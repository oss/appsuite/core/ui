include:
  - project: 'sre/ci-building-blocks'
    file: '/nodejs.yml'

variables:
  OX_COMPONENT: "core-ui"
  ALLURE_PUBLISH_REPORT: "true"
  REGISTRY: "registry.dev.oxui.de"
  KUBE_INGRESS_BASE_DOMAIN: "dev.oxui.de"
  E2E_TEST_METRICS_URL: http://e2e-test-metrics.e2e-test-metrics-main.svc.cluster.local
  PROVISIONING_URL: https://appsuite-main.dev.oxui.de/

audit npm modules:
  extends: .audit npm modules
  timeout: 5 minutes
  tags:
    - build-hetzner

unit tests:
  extends: .unit tests
  timeout: 10 minutes
  tags:
    - build-hetzner

eslint:
  extends: .eslint
  tags:
    - build-hetzner
  timeout: 10 minutes

allure report:
  extends: .allure report
  tags:
    - tiny-hetzner
  timeout: 5 minutes

remove allure report:
  extends: .remove allure report
  tags:
    - tiny-hetzner
  timeout: 5 minutes

build image:
  extends: .build image
  tags:
    - build-hetzner

build e2e image:
  extends: .build e2e image
  tags:
    - build-hetzner

.customize-k8s:
  script:
    - kubectl rollout restart statefulset redis-master || true
    - |
      if [[ "$CI_COMMIT_REF_NAME" =~ ^stable- ]]; then
        kubectl annotate namespace $NAMESPACE janitor/expires=`date --date='28 days' +%FT%H:%M` --overwrite
      fi

deploy preview chart:
  extends: .auto-deploy-preview-chart
  tags:
    - tiny-hetzner
  before_script:
    - export STACK_CHART_VERSION_MAIN=$(curl --header PRIVATE-TOKEN:$STACK_API_ACCESS_TOKEN https://gitlab.open-xchange.com/api/v4/projects/1494/variables/STACK_CHART_VERSION_MAIN | jq -r .value)
    - envsubst < .gitlab/preview/Chart_template.yaml > .gitlab/preview/Chart.yaml

tear down preview:
  extends: .auto-deploy-preview-chart-teardown
  tags:
    - tiny-hetzner
  timeout: 5 minutes

e2e codeceptjs preview:
  image: registry.gitlab.open-xchange.com/appsuite/web-apps/ui/e2e:$TAG_NAME
  extends:
    - .e2e-codeceptjs-preview
  tags:
    - e2e-hetzner
  parallel: 60
  timeout: 20 minutes
  before_script:
    - |
      if [ "${#CI_COMMIT_REF_SLUG}" -ge $(expr 62 - ${#OX_COMPONENT}) ];then export PREVIEW_APP_NAME=${OX_COMPONENT}-${CI_COMMIT_REF_SLUG:$(expr ${#OX_COMPONENT} + ${#CI_COMMIT_REF_SLUG} - 62):$(expr 62 - ${#OX_COMPONENT})};
      else export PREVIEW_APP_NAME=${OX_COMPONENT}-${CI_COMMIT_REF_SLUG}
      fi
    - export LAUNCH_URL=https://$PREVIEW_APP_NAME.k3s.os2.oxui.de/
    - for file in /e2e/* /e2e/.[!.]* /e2e/..?*; do if [ -e "$file" ]; then mv "$file" $CI_PROJECT_DIR; fi; done
    - mkdir -p ./output/
    - export E2E_ADMIN_PW=$ADMIN_PW
    - export LAUNCH_URL=https://$PREVIEW_APP_NAME.dev.oxui.de/
    - export MAX_RERUNS=5

e2e codeceptjs grep:
  image: registry.gitlab.open-xchange.com/appsuite/web-apps/ui/e2e:$CI_COMMIT_REF_SLUG
  extends:
    - .e2e-codeceptjs-grep
  tags:
    - e2e-hetzner
  variables:
    E2E_GREP_RUNS: 10
  before_script:
    - !reference [.e2e-codeceptjs-preview, before_script]
    - export E2E_ADMIN_PW=$ADMIN_PW
    - export LAUNCH_URL=https://$PREVIEW_APP_NAME.dev.oxui.de/
    - export MAX_RERUNS=${E2E_GREP_RUNS}
    - export MIN_SUCCESS=${E2E_GREP_RUNS}

transition deployment smoketests:
  stage: e2e
  trigger:
    project: frontend/transition-smoke-tests
    branch: main
    strategy: depend
    forward:
      yaml_variables: false
      pipeline_variables: false
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: on_success
    - when: never

build documentation:
  image: registry.gitlab.open-xchange.com/engineering/documentation:latest
  stage: build
  script:
    - VERSION=8
    - if [ "$CI_COMMIT_REF_NAME" != "main" ]; then
        VERSION=`grep '"version":' package.json | cut -d\" -f4 | cut -d- -f1`;
      fi
    - echo $VERSION
    - export VERSION=$VERSION
    - ln -s $CI_PROJECT_DIR/docs /documentation/jekyll/_ui
    - cd /documentation
    - bundle exec jekyll b --baseurl /$VERSION --config _config.yml
    - cd $CI_PROJECT_DIR
    - mkdir -p doku/$VERSION
    - cp -r /documentation/dist/* doku/$VERSION
  dependencies: []
  tags:
    - tiny-hetzner
  artifacts:
    paths:
      - doku/
    expire_in: 2 hours
  retry: 2
  rules:
    - if: '$CI_COMMIT_REF_NAME =~ /^(main|stable-.*)$/'
      changes:
        - docs/**/*
        - package.json
      when: always

deploy documentation:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - export VERSION=`ls doku --sort=time | cut -f1 | head -n1`
    - echo $VERSION
    - mkdir -p /var/www/documentation/$VERSION/ui
    - rsync -aHAX --delete doku/$VERSION/ui/ /var/www/documentation/$VERSION/ui
    - mkdir -p /var/www/documentation/$VERSION/assets /var/www/documentation/$VERSION/docs-general
    - rsync -aHAX --delete doku/$VERSION/assets/ /var/www/documentation/$VERSION/assets
    - rsync -aHAX --delete doku/$VERSION/docs-general/ /var/www/documentation/$VERSION/docs-general
    - rsync -aHAX --delete doku/$VERSION/index.html /var/www/documentation/$VERSION/
    - find /var/www/documentation.open-xchange.com -user gitlab-runner -exec chmod g+w '{}' \;
    - find /var/www/documentation.open-xchange.com -user gitlab-runner -exec chgrp www-data '{}' \;
  dependencies:
    - build documentation
  needs:
    - build documentation
  tags:
    - shell
    - documentation
  allow_failure: true
  retry: 2
  rules:
    - if: '$CI_COMMIT_REF_NAME =~ /^(main|stable-.*)$/'
      changes:
        - docs/**/*
        - package.json
      when: on_success

disable interruptible:
  rules:
    - if: '$DISABLE_INTERRUPTIBLE == "true"'
      when: always
