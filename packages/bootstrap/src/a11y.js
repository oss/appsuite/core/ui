/* global jQuery: true */
/* global underscore: true */

const $ = jQuery
const _ = underscore
//
// Copied from bootstrap accessibility plugin
// https://github.com/Open-Xchange-Frontend/bootstrap-accessibility-plugin
//

// Modal Extension
// ===============================
$.fn.modal.Constructor.prototype._hide = $.fn.modal.Constructor.prototype.hide
$.fn.modal.Constructor.prototype._show = $.fn.modal.Constructor.prototype.show

$.fn.modal.Constructor.prototype.hide = function () {
  const modalOpener = this.$element.parent().find('[data-target="#' + this.$element.attr('id') + '"]')
  $.fn.modal.Constructor.prototype._hide.apply(this, arguments)
  modalOpener.focus()
}

$.fn.modal.Constructor.prototype.show = function () {
  $('.modal-dialog', this).attr({ role: 'document' })
  $.fn.modal.Constructor.prototype._show.apply(this, arguments)
}

// GENERAL UTILITY FUNCTIONS
// ===============================
const removeMultiValAttributes = function (el, attr, val) {
  let describedby = (el.attr(attr) || '').split(/\s+/)
  const index = $.inArray(val, describedby)
  if (index !== -1) {
    describedby.splice(index, 1)
  }
  describedby = $.trim(describedby.join(' '))
  if (describedby) {
    el.attr(attr, describedby)
  } else {
    el.removeAttr(attr)
  }
}

// Popover Extension
// ===============================
const showPopover = $.fn.popover.Constructor.prototype.setContent
const hidePopover = $.fn.popover.Constructor.prototype.hide

$.fn.popover.Constructor.prototype.setContent = function () {
  showPopover.apply(this, arguments)
  const $tip = this.tip()
  const tooltipID = $tip.attr('id') || _.uniqueId('ui-tooltip')
  $tip.attr({ role: 'tooltip', id: tooltipID })
  this.$element.attr('aria-describedby', tooltipID)
}

$.fn.popover.Constructor.prototype.hide = function () {
  hidePopover.apply(this, arguments)
  removeMultiValAttributes(this.$element, 'aria-describedby', this.tip().attr('id'))
  return this
}

// TOOLTIP Extension
// ===============================
const showTooltip = $.fn.tooltip.Constructor.prototype.show
const hideTooltip = $.fn.tooltip.Constructor.prototype.hide
const tooltipHasContent = $.fn.tooltip.Constructor.prototype.hasContent

$.fn.tooltip.Constructor.prototype.show = function () {
  showTooltip.apply(this, arguments)
  const $tip = this.tip()
  const tooltipID = $tip.attr('id') || _.uniqueId('ui-tooltip')
  $tip.attr({ role: 'tooltip', id: tooltipID })
  if (this.$element) this.$element.attr({ 'aria-describedby': tooltipID })
}

$.fn.tooltip.Constructor.prototype.hide = function () {
  hideTooltip.apply(this, arguments)
  if (this.$element) removeMultiValAttributes(this.$element, 'aria-describedby', this.tip().attr('id'))
  return this
}

$.fn.tooltip.Constructor.prototype.hasContent = function () {
  if (!this.$element) return false
  return tooltipHasContent.apply(this, arguments)
}
