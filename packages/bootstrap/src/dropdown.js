/* global jQuery: true */
/* global underscore: true */

const $ = jQuery
const _ = underscore
let activeElement

// jquery extension for dropdown
function Plugin (options) {
  return this.each(function () {
    const $this = $(this)
    $this.attr('data-toggle', 'dropdown')
    if (options === 'toggle') $this.trigger('click')
  })
}

$.fn.dropdown = Plugin

// get parent function from bootstrap
function getParent ($this) {
  const selector = $this.attr('data-target')
  const $parent = selector && $(selector)

  return $parent && $parent.length ? $parent : $this.parent()
}

// clear menus function from bootstrap
function clearMenus (e) {
  if (e && e.which === 3) return
  $('[data-toggle="dropdown"]').each(function () {
    const $this = $(this)
    const $parent = getParent($this)
    const relatedTarget = { relatedTarget: this }

    if (!$parent.hasClass('open')) return

    const originalEventType = e ? e.type : null
    if (originalEventType === 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

    $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

    // dropdowns can be manually prevented from closing. exception: direct click on the toggle button
    // used by special dropdowns, like notification area (should not close when a sidepopup is opened or the user clicks within it)
    // also used by tours
    // prevent closing of currently opening dropdowns, produces weird glitches when content is loaded asynchronously (foldertree context menu)
    if (e.isDefaultPrevented() || $parent.attr('forceOpen') === 'true' || $this.hasClass('opening')) return

    // if the user clicked on a focusable inputfield we focus that instead of the dropdown root element
    const focusableElement = $(document.activeElement).filter('.editable, input[type="text"], input[type="textarea"], input[type="email"]')
    if (activeElement) {
      if (document.body.classList.contains('smartphone') && originalEventType === 'click' && focusableElement.length) {
        focusableElement.focus()
      } else {
        activeElement.focus()
      }
    }

    $this.attr('aria-expanded', false)
    $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
  })
}

function toggle (e) {
  const $this = $(this)
  const $parent = getParent($this)
  const isActive = $parent.hasClass('open')
  // direct click on the toggle button removes the force open state
  if (e && ($parent[0] !== e.target || this === e.target)) {
    $parent.attr('forceOpen', false)
  }
  clearMenus()

  if (!isActive) {
    const relatedTarget = { relatedTarget: this }
    $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))
    activeElement = $this
    // if (e.isDefaultPrevented()) return

    $this.attr('aria-expanded', true)
    $parent
      .toggleClass('open')
      .trigger($.Event('shown.bs.dropdown', relatedTarget))
    $this.focus()
  }
  return false
}

function keydown (e) {
  // close on ESC
  if (e.which === 27) return clearMenus()

  if (!/(13|32|38|40)/.test(e.which)) return

  const $target = $(e.target)
  const $parent = getParent($target)
  const $menu = $target.siblings('ul')
  const isActive = $parent.hasClass('open')

  if (!isActive) {
    // do not explecitely open on enter or space
    if (!/(13|32)/.test(e.which)) $target.trigger('click')
    import('@/io.ox/core/a11y').then(function ({ default: a11y }) {
      _.defer(function () {
        const items = a11y.getTabbable($menu)
        if (/(13|32|40)/.test(e.which)) {
          items.first(':visible').focus()
        }
        if (e.which === 38) items.last(':visible').focus()
      })
    })
  }
}

function keydownMenu (e) {
  console.log('ox bootstrap keydownMenu')
  // close on ESC
  if (e.which === 27) return clearMenus()

  if (!/(38|40)/.test(e.which)) return
  // Needs preventDefault(), otherwise scrolling occurs on pages that exceed view port.
  e.preventDefault()
  const $target = $(e.target)
  const $menu = $target.closest('ul')
  const $list = $menu.find('a:visible[role^="menuitem"]')
  let index = $list.index($target)

  if (e.which === 38) index--
  if (e.which === 40) index++

  if (index < 0) index += $list.length
  if (index >= $list.length) index -= $list.length

  _.defer(function () {
    $list.eq(index).get(0).focus()
  })
}

function onClickListItem (e) {
  const $target = $(e.target)
  if (e.which === 32 && $target.is('a')) $target.trigger('click')
}

function onFocusOut () {
  const self = this
  _.defer(function () {
    if (!$.contains(self, document.activeElement)) clearMenus()
  })
}

function onResize (e) {
  // do not clear menus when the resize event has been triggered or on android
  if (!e.originalEvent) return
  clearMenus(e)
}

// global listener for dropdown handling
$(document)
  .on('click.bs.dropdown.data-api', clearMenus)
  .on('click.bs.dropdown.data-api', '[data-toggle="dropdown"]', toggle)
  .on('keydown.bs.dropdown.data-api', '[data-toggle="dropdown"]', keydown)
  .on('keydown.bs.dropdown.data-api', '.dropdown-menu', keydownMenu)
  .on('keydown.bs.dropdown.data-api', '[role=menu]', onClickListItem)
  .on('focusout.dropdown.data-api', '.dropdown-menu', onFocusOut)
$(window).on('resize', _.throttle(onResize, 200))
