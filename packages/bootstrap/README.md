# @open-xchange/bootstrap

- A bundle which provides bootstrap 3.0 including datepicker, typeahead and tokenfield as a module

## Prerequisites

This module has several dependencies (see externals in the vite config), which are usually only there in an open-xchange Appsuite UI environment. In other environments, this module might be useless.

## How to use

### Simple usage

```js
// main.js
import '@open-xchange/bootstrap'
import '@open-xchange/bootstrap/dist/style.css'
```

### Style with less-variables

```js
// main.js
import '@open-xchange/bootstrap'
import './style.less'
```

```less
// style.less
// bootstrap overwrite
@brand-primary: #fff;
// use bootstrap with variables
@import '@open-xchange/bootstrap/less/bootstrap.less';
```
