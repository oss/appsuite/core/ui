import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    rollupOptions: {
      external: ['@/ox', '@/io.ox/core/a11y'],
      input: 'index.js',
      output: [
        {
          dir: 'dist',
          entryFileNames: '[name].mjs',
          banner: "import jQuery from 'jquery'\nimport underscore from 'underscore'",
          format: 'module'
        }
      ]
    },
    lib: {
      entry: 'index.js'
    }
  }
})
