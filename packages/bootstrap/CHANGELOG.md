# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.4.0] - 2024-05-31

### Removed

- CJS support

## [0.3.1] - 2023-10-19

### Removed

- Glyphicons [`0fca971`](https://gitlab.open-xchange.com/frontend/open-xchange-bootstrap/commit/0fca971a04fd99f8bed130f6254b2a0ac1160222)

## [0.3.0] - 2023-09-26

### Changed

- [`OXUI-1224`](https://jira.open-xchange.com/browse/OXUI-1224): New styling for mobile dropdowns [`1cc728e`](https://gitlab.open-xchange.com/frontend/open-xchange-bootstrap/commit/1cc728ebf81fcd5e4b5c30b6b63de88ad169622b)

## [0.2.6] - 2023-09-26

### Added

- Started changelog

[unreleased]: https://gitlab.open-xchange.com/frontend/open-xchange-bootstrap/compare/0.3.1...main
[0.3.1]: https://gitlab.open-xchange.com/frontend/open-xchange-bootstrap/compare/0.3.0...0.3.1
[0.3.0]: https://gitlab.open-xchange.com/frontend/open-xchange-bootstrap/releases/tag/0.3.0
