/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import 'vitest-canvas-mock'
import $ from '@/jquery'
import { vi } from 'vitest'

// the unit test environment doesn't know CSS (so far)
window.CSS = { escape: $.escapeSelector }
// window.URL.createObjectURL = function () { return 'asd' }
window.fetch = async () => ({ ok: true, text: () => '' })
window.$ = window.jQuery = $

vi.mock('@/browser', () => ({
  default: {},
  us: { browser: {} },
  underscoreExtends: { device: () => false },
  device: {}
}))
vi.mock('@/io.ox/core/tk/image-util', () => ({ default: { PromiseWorker: vi.fn() } }))
vi.mock('@/io.ox/core/tk/tokenfield', () => ({}))
vi.mock('@/io.ox/core/viewer/util', () => ({}))
vi.mock('@/io.ox/core/folder/actions/properties', () => ({}))
vi.mock('@/io.ox/core/api/filestorage', () => ({ default: { on: vi.fn(), rampup: vi.fn() } }))
vi.mock('@/io.ox/core/cache/indexeddb', () => ({ default: { open: vi.fn() } }))

vi.mock('@/io.ox/core/http', async () => {
  const actual = await vi.importActual('@/io.ox/core/http')
  return {
    ...actual,
    default: { ...actual.default, GET: vi.fn(() => { return $.Deferred() }), POST: vi.fn(() => { return $.Deferred() }) }
  }
})

vi.mock('@io.ox/mail/sanitizer', () => {
  return {
    default: {
      sanitize: vi.fn(), simpleSanitize: vi.fn(), isEnabled: vi.fn()
    }
  }
})

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn()
  }))
})
