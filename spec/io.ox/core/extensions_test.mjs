/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect } from 'vitest'

import ext from '@/io.ox/core/extensions'
import $ from '@/jquery'

describe('Core extensions', () => {
  describe('baton clone()', () => {
    it('keeps state of isDefaultPrevented', () => {
      // prevent before clone
      const baton = ext.Baton()
      baton.preventDefault()
      const clone = baton.clone()

      expect(typeof baton.isDefaultPrevented).toBe('function')
      expect(typeof clone.isDefaultPrevented).toBe('function')
      expect(baton.isDefaultPrevented()).toBe(true)
      expect(clone.isDefaultPrevented()).toBe(true)

      // prevent after clone
      const anotherBaton = ext.Baton()
      const anotherClone = anotherBaton.clone()
      anotherBaton.preventDefault()

      expect(typeof anotherBaton.isDefaultPrevented).toBe('function')
      expect(typeof anotherClone.isDefaultPrevented).toBe('function')
      expect(anotherBaton.isDefaultPrevented()).toBe(true)
      expect(anotherClone.isDefaultPrevented()).toBe(false)
    })

    it('keeps state of stopPropagation', () => {
      // stop before clone
      const baton = ext.Baton()
      baton.stopPropagation()
      const clone = baton.clone()

      expect(typeof baton.isPropagationStopped).toBe('function')
      expect(typeof clone.isPropagationStopped).toBe('function')
      expect(baton.isPropagationStopped()).toBe(true)
      expect(clone.isPropagationStopped()).toBe(true)

      // stop after clone
      const anotherBaton = ext.Baton()
      const anotherClone = anotherBaton.clone()
      anotherBaton.stopPropagation()

      expect(typeof anotherBaton.isPropagationStopped).toBe('function')
      expect(typeof anotherClone.isPropagationStopped).toBe('function')
      expect(anotherBaton.isPropagationStopped()).toBe(true)
      expect(anotherClone.isPropagationStopped()).toBe(false)
    })

    it('supports different data types', () => {
      // prevent before clone
      const baton = ext.Baton()
      baton.$el = $('<div>')
      baton.array = ['value', {}]
      baton.object = { key: 'value', obj: {} }
      baton.string = 'value'
      baton.number = 1
      baton.boolean = true
      baton.null = null
      baton.unset = undefined
      baton.fn = () => { return 'value' }

      const clone = baton.clone({ string: 'overwritten-value' })

      baton.$el.append('<anotherdiv>')
      baton.array.push('anothervalue')
      baton.array[1].someKey = 'value'
      baton.object.anotherKey = 'anothervalue'
      baton.object.obj.someKey = 'value'
      baton.string = 'anothervalue'
      baton.number = 2
      baton.boolean = false
      baton.null = 'wasnull'
      baton.unset = 'wasundefined'
      baton.fn = () => { return 'anothervalue' }

      // shallow
      expect(clone.$el.get(0).outerHTML).toBe('<div><anotherdiv></anotherdiv></div>')
      // shallow for complex datatype properties/items
      expect(clone.array).not.toContain('anothervalue')
      expect(clone.array[1]).not.toBe('anothervalue')
      expect(Object.keys(clone.object)).not.toContain('anotherKey')
      expect(Object.keys(clone.object.obj)).toContain('someKey')

      // primitives
      expect(clone.string).toBe('overwritten-value')
      expect(clone.number).toBe(1)
      expect(clone.boolean).toBe(true)
      expect(clone.null).toBeNull()
      expect(clone.unset).toBeUndefined()
      expect(clone.fn()).toBe('value')
    })
  })

  describe('extension point "has"', () => {
    it('prevents double ids', () => {
      const point = ext.point('myAwesomePoint')

      expect(point.all().length).toBe(0)

      expect(point.has('superExtension')).toBe(false)
      point.extend({ id: 'superExtension' })
      expect(point.all().length).toBe(1)
      expect(point.has('superExtension')).toBe(true)

      point.extend({ id: 'orphanedExtension', after: 'anchor' })
      expect(point.all().length).toBe(1)
      expect(point.has('orphanedExtension')).toBe(true)

      // try adding the same id
      point.extend({ id: 'orphanedExtension', after: 'anchor' })
      expect(point.all().length).toBe(1)
      expect(point.has('orphanedExtension')).toBe(true)

      // try adding the same id
      point.extend({ id: 'orphanedExtension', before: 'anchor' })
      expect(point.all().length).toBe(1)
      expect(point.has('orphanedExtension')).toBe(true)

      // only one orphaned extension should be added when the anchor extension is added
      point.extend({ id: 'anchor' })
      expect(point.all().length).toBe(3)
    })
  })
})
