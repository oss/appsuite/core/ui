/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect } from 'vitest'

import { categoriesCollection, getMailCategories } from '@/io.ox/core/categories/api'
import { CategoryModel } from '@/io.ox/core/categories/model'
import { settings as coreSettings } from '@/io.ox/core/settings'

describe('Categories', () => {
  describe('Model', () => {
    describe('applies/generates id for', () => {
      it('predefined categories', () => {
        let model = categoriesCollection.findWhere({ name: 'Important' })
        expect(model.get('id')).toEqual('$ct_predefined_0001')
        model = categoriesCollection.findWhere({ name: 'Business' })
        expect(model.get('id')).toEqual('$ct_predefined_0002')
        model = categoriesCollection.findWhere({ name: 'Private' })
        expect(model.get('id')).toEqual('$ct_predefined_0003')
        model = categoriesCollection.findWhere({ name: 'Meeting' })
        expect(model.get('id')).toEqual('$ct_predefined_0004')
        expect(coreSettings.get('categories/serialNumber')).toBe(undefined)
      })

      it('type user', () => {
        const model = new CategoryModel({ name: 'My category #1', type: 'user' })
        const [prefix, type, number, user] = model.get('id').split('_')
        expect(prefix).toEqual('$ct')
        expect(type).toEqual('user')
        expect(number).toEqual('0001')
        expect(user).toEqual('0')

        expect(coreSettings.get('categories/serialNumber')).toEqual(1)
      })

      it('type predefined', () => {
        const model = new CategoryModel({ name: 'Priority', type: 'predefined' })
        const [prefix, type, number, user] = model.get('id').split('_')
        expect(prefix).toEqual('$ct')
        expect(type).toEqual('predefined')
        expect(number).toEqual('0002')
        expect(user).toBeUndefined()

        expect(coreSettings.get('categories/serialNumber')).toEqual(2)
      })

      it('type user with given id', () => {
        const model = new CategoryModel({ id: '$ct_user_9999_0', name: 'My category #2', type: 'user' })
        const [prefix, type, number, user] = model.get('id').split('_')
        expect(prefix).toEqual('$ct')
        expect(type).toEqual('user')
        expect(number).toEqual('9999')
        expect(user).toEqual('0')

        expect(coreSettings.get('categories/serialNumber')).toEqual(2)
      })
    })

    describe('stores references uniquely', () => {
      it('type user', () => {
        const model = new CategoryModel({ name: 'My category #1', type: 'user' })
        model.addReference('mail:4711')
        model.addReference('mail:4711')
        expect(model.get('references').join(',')).toBe('mail:4711')
      })
    })
  })

  describe('getMailCategories', () => {
    it('returns collection with valid category models', () => {
      const model = new CategoryModel({ name: 'My category #1', type: 'user' })
      categoriesCollection.add(model)
      const ref = 'mail:4711'
      const list = `invalid,${model.get('id')},$ct_predefined_0001`
      const collection = getMailCategories(list, ref)
      expect(collection.map(model => model.get('id')).join(',')).toEqual(`${model.get('id')},$ct_predefined_0001`)
      expect(collection.map(model => model.get('references'))).toEqual([[ref], [ref]])
    })
  })
})
