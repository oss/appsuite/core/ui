/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect, vi, afterAll, beforeAll } from 'vitest'

import $ from 'jquery'
import ox from '@/ox'
import { createIcon, createIllustration } from '@/io.ox/core/components'
import { settings as coreSettings } from '@/io.ox/core/settings'

/* global jQuery */

const icons = {
  './themes/default/icons/bi/chevron-right.svg': `<svg aria-hidden="true" width="16" height="16" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
<path xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708"></path>
</svg>`,
  './themes/default/icons/bi/broken-icon.svg': `<svg width="200" height="96" viewBox="0 0 200 96" xmlns="http://www.w3.org/2000/svg" fill-rule="nonzero">
<path d="M173.151 43.1c-.9-1.2-2.3-2.1-3.8-2.3l-34.8-4.7c-.3-.6-.6-1.1`,
  './themes/default/icons/bi/chevron-top.svg': `<svg aria-hidden="true" width="16" height="16" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
<script>document.write('XSS')</script>
</svg>`,
  './themes/default/illustrations/empty-selection.svg': `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 96" width="200" height="96" fill-rule="nonzero">
<path d="M173.151 43.1c-.9-1.2-2.3-2.1-3.8-2.3l-34.8-4.7c-.3-.6-.6-1.1-1.1-1.6-1.2-1.3-2.9-2-4.7-2h-59.4c-1.9 0-3.6.8-4.9 2.3-.4.5-.7 1-1 1.6l-33.6 4.5c-1.5.2-2.9 1-3.8 2.3-.8 1.1-1.2 2.5-1 3.9l4.2 31.1a5.18 5.18 0 0 0 2.1 3.6c1 .7 2.1 1.1 3.3 1.1h.7l28-3.7c1 2.2 3.2 3.8 5.8 3.8h59.4c2.7 0 4.9-1.6 5.9-3.9l29.1 3.9h.7c1.2 0 2.4-.4 3.3-1.1 1.2-.9 1.9-2.2 2.1-3.6l4.2-31.1c.5-1.6.1-3-.7-4.1z" fill="var(--background)"/>
<path d="M169.351 40.8l-34.8-4.7c-.3-.6-.6-1.1-1.1-1.6-1.2-1.3-2.9-2-4.7-2h-59.4c-1.9 0-3.6.8-4.9 2.3-.4.5-.7 1-1 1.6l-33.6 4.5c-1.5.2-2.9 1-3.8 2.3-.8 1.1-1.2 2.5-1 3.9l4.2 31.1a5.18 5.18 0 0 0 2.1 3.6c1 .7 2.1 1.1 3.3 1.1h.7l28-3.7c1 2.2 3.2 3.8 5.8 3.8h59.4c2.7 0 4.9-1.6 5.9-3.9l29.1 3.9h.7c1.2 0 2.4-.4 3.3-1.1 1.2-.9 1.9-2.2 2.1-3.6l4.2-31.1c.2-1.4-.2-2.8-1-3.9-.6-1.5-2-2.3-3.5-2.5zm-139.2 1.8l32.9-4.4c0 .3-.1.5-.1.8v19.1l-2 2.4c-1.6 1.9-4.4 2.3-6.5.9l-26.2-18c.6-.5 1.2-.8 1.9-.8zm5.1 38.5c-1 .1-2-.1-2.8-.7s-1.3-1.5-1.4-2.5l-4.2-31.1c-.1-.7 0-1.4.3-2l26.3 18c1.2.8 2.5 1.2 3.8 1.2 1.9 0 3.8-.8 5.1-2.4l.7-.8v15.6c0 .3 0 .6.1.9l-27.9 3.8zm34.1-46.8h9.5 2.2 37.1 2.2 8.4c1.2 0 2.4.5 3.2 1.3l-.2.2-1.7 1.5-6.5 5.8-1.3 1.2-8.5 7.6-2 1.8-8.7 7.8c-2.3 2.1-5.7 2.1-8.1 0l-7.3-6.6-2-1.8-9.1-8.2-1.3-1.2-7-6.3-1.7-1.5-.3-.3c.7-.8 1.9-1.3 3.1-1.3zm64.1 20.4v2.7 19 .5c-.1.6-.3 1.2-.5 1.7-.8 1.4-2.3 2.4-4.1 2.4h-59.5a4.58 4.58 0 0 1-4-2.3c-.3-.5-.5-1.1-.6-1.7 0-.2-.1-.4-.1-.7V58.7 56 38.9c0-.3 0-.7.1-1s.1-.5.3-.8l.8.7 8.1 7.3 1.3 1.2 10.6 9.5 2 1.8 5.9 5.3c1.5 1.4 3.4 2.1 5.2 2.1 1.9 0 3.7-.7 5.2-2.1l7.1-6.4 2-1.8 10-9 1.3-1.2 7.6-6.9.6-.6c.1.2.2.4.2.7.1.4.1.8.1 1.2v15.8h.4zm35.7-12.1c.7.1 1.3.4 1.9.8l-26.2 17.9c-2.1 1.4-4.9 1-6.5-.9l-3.1-3.7V38.9c0-.3 0-.6-.1-.9l34 4.6zm3.2 4.2l-4.2 31.1c-.1 1-.6 1.9-1.4 2.5s-1.8.9-2.8.7l-28.8-3.9v-.8-17l1.8 2.1c1.3 1.5 3.2 2.4 5.1 2.4 1.3 0 2.6-.4 3.8-1.2l26.3-18c.2.7.3 1.4.2 2.1z" fill="currentColor"/>
</svg>
`,
  './themes/default/illustrations/broken-illustration.svg': `<svg width="200" height="96" viewBox="0 0 200 96" xmlns="http://www.w3.org/2000/svg" fill-rule="nonzero">
  <path d="M173.151 43.1c-.9-1.2-2.3-2.1-3.8-2.3l-34.8-4.7c-.3-.6-.6-1.1`
}

describe('createIcon pre settings', () => {
  beforeAll(() => {
    global.fetch = vi.fn(url => Promise.resolve({
      ok: true,
      text: () => Promise.resolve(icons[url])
    }))
  })

  it('should return the original icon when settings are not yet available (eg. login page)', async () => {
    const icon = createIcon('bi/chevron-right.svg')
    await new Promise(resolve => icon.on('load', resolve))
    expect(icon).toBeDefined()
    expect(icon.attr('width')).toEqual('16')
    expect(icon.attr('height')).toEqual('16')
    expect(icon.attr('aria-hidden')).toEqual('true')
    expect(icon[0].outerHTML).toEqual(icons['./themes/default/icons/bi/chevron-right.svg'])
  })
})

describe('createIcon', () => {
  beforeAll(() => {
    ox.session = true
    coreSettings.set('theming/customIcons', {
      'bi/chevron-left.svg': './themes/default/icons/bi/chevron-right.svg'
    })
    coreSettings.trigger('load')

    global.fetch = vi.fn(url => Promise.resolve({
      ok: true,
      text: (a) => Promise.resolve(icons[url])
    }))
  })

  afterAll(() => {
    global.fetch.mockClear()
  })

  it('should return a jQuery object', () => {
    const icon = createIcon('bi/question-octagon.svg')
    expect(icon).toBeDefined()
    // check for well-known jQuery method
    expect(icon.one).toBeInstanceOf(Function)
  })

  it('should lazily inject svg data into the icon', async () => {
    const icon = createIcon('bi/chevron-left.svg')

    expect(icon.attr('width')).toEqual('16')
    expect(icon.attr('height')).toEqual('16')

    await new Promise(resolve => icon.on('load', resolve))
    expect(icon.attr('aria-hidden')).toEqual('true')
    expect(icon.find('path')).toHaveLength(1)
  })

  it('fetches alternative icons when overridden in settings (io.ox/core//theming/customIcons)', async () => {
    // standard icon
    const $div1 = $('<div>').append(createIcon('bi/chevron-right.svg'))
    await new Promise(resolve => setTimeout(resolve))
    expect($div1[0].innerHTML).toEqual(icons['./themes/default/icons/bi/chevron-right.svg'])

    // icon overridden in settings
    const $div2 = $('<div>').append(createIcon('bi/chevron-left.svg'))
    await new Promise(resolve => setTimeout(resolve))
    expect($div2[0].innerHTML).toEqual(icons['./themes/default/icons/bi/chevron-right.svg'])

    // prevents XSS from settings
    const $div3 = $('<div>').append(createIcon('bi/ chevron-top.svg'))
    await new Promise(resolve => setTimeout(resolve))
    expect($div3[0].innerHTML).to.not.contain('XSS')
  })

  describe('events', () => {
    it('should fire a load event when the icon is loaded', async () => {
      const spy = vi.fn()
      const icon = createIcon('bi/chevron-right.svg')
      icon.on('load', spy)

      expect(icon.attr('width')).toEqual('16')
      expect(icon.attr('height')).toEqual('16')
      expect(icon.attr('aria-hidden')).toEqual('true')

      // expect(icon.find('path')).toHaveLength(0)
      expect(spy).toHaveBeenCalledTimes(0)
      await new Promise(resolve => setTimeout(resolve, 1000))
      expect(icon.find('path')).toHaveLength(1)
      // expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should fire for icons defined synchronously', async () => {
      // calling createIcon with an icon source is deprecated but used in legacy code
      const spy = vi.fn()
      const icon = createIcon('<svg><circle r="50" /></svg>')
      icon.on('load', spy)

      expect(icon.attr('aria-hidden')).toEqual('true')
      expect(icon.find('circle')).toHaveLength(1)
      expect(icon.find('circle').attr('r')).toEqual('50')
      expect(spy).toHaveBeenCalledTimes(0)
      await new Promise(resolve => setTimeout(resolve, 0))
      expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should have a fallback for broken icons', async () => {
      const icon = createIcon('bi/broken-icon.svg')

      expect(icon.attr('width')).toEqual('16')
      expect(icon.attr('height')).toEqual('16')

      await new Promise(resolve => icon.on('load', resolve))
      expect(icon.attr('aria-hidden')).toEqual('true')
      expect(icon.hasClass('bi-question-octagon')).toBeTruthy()
    })

    it('should have a fallback for missing icons', async () => {
      const icon = createIcon('bi/this-does-not-exist.svg')

      expect(icon.attr('width')).toEqual('16')
      expect(icon.attr('height')).toEqual('16')

      await new Promise(resolve => icon.on('load', resolve))
      expect(icon.attr('aria-hidden')).toEqual('true')
      expect(icon.hasClass('bi-question-octagon')).toBeTruthy()
    })
  })
})

describe('createIllustration', () => {
  beforeAll(() => {
    global.fetch = vi.fn(url => Promise.resolve({
      ok: true,
      text: () => Promise.resolve(icons[url])
    }))
  })

  afterAll(() => {
    global.fetch.mockClear()
  })

  it('should return a jQuery object', () => {
    const illustration = createIllustration('illustrations/empty-selection.svg')
    expect(illustration instanceof jQuery).toBeTruthy()
  })

  it('should keep original width and height when parameters are not set', async () => {
    const illustration = createIllustration('illustrations/empty-selection.svg')

    // added
    expect(illustration.attr('aria-hidden')).toEqual('true')
    expect(illustration.attr('role')).toEqual('presentation')
    // overwrite width and height as attributes, not via CSS
    expect(illustration.attr('width')).toEqual('200')
    expect(illustration.attr('height')).toEqual('96')
    expect(illustration[0].style.width).toEqual('')
    expect(illustration[0].style.height).toEqual('')
    // keep original file attributes
    expect(illustration.attr('viewBox')).toEqual('0 0 200 96')
  })

  it('should keep original ratio when only width is set', async () => {
    const illustration = createIllustration('illustrations/empty-selection.svg', { width: 400 })

    // added
    expect(illustration.attr('aria-hidden')).toEqual('true')
    expect(illustration.attr('role')).toEqual('presentation')
    // overwrite width and height as attributes, not via CSS
    expect(illustration.attr('width')).toEqual('400')
    expect(illustration.attr('height')).toEqual('auto')
    expect(illustration[0].style.width).toEqual('')
    expect(illustration[0].style.height).toEqual('')
    // keep original file attributes
    expect(illustration.attr('viewBox')).toEqual('0 0 200 96')
  })

  it('should keep original ratio when only height is set', async () => {
    const illustration = createIllustration('illustrations/empty-selection.svg', { height: 192 })

    // added
    expect(illustration.attr('aria-hidden')).toEqual('true')
    expect(illustration.attr('role')).toEqual('presentation')
    // overwrite width and height as attributes, not via CSS
    expect(illustration.attr('width')).toEqual('auto')
    expect(illustration.attr('height')).toEqual('192')
    expect(illustration[0].style.width).toEqual('')
    expect(illustration[0].style.height).toEqual('')
    // keep original file attributes
    expect(illustration.attr('viewBox')).toEqual('0 0 200 96')
  })

  it('should apply optional width and height parameters', async () => {
    const illustration = createIllustration('illustrations/empty-selection.svg', { width: 400, height: 192 })

    // added
    expect(illustration.attr('aria-hidden')).toEqual('true')
    expect(illustration.attr('role')).toEqual('presentation')
    // overwrite width and height as attributes, not via CSS
    expect(illustration.attr('width')).toEqual('400')
    expect(illustration.attr('height')).toEqual('192')
    expect(illustration[0].style.width).toEqual('')
    expect(illustration[0].style.height).toEqual('')
    // keep original file attributes
    expect(illustration.attr('viewBox')).toEqual('0 0 200 96')
  })

  it('should render an empty svg for broken illustrations', async () => {
    const illustration = createIllustration('illustrations/broken-illustration.svg', { width: 400, height: 192 })
    await new Promise(resolve => illustration[0].addEventListener('load', resolve))

    // added
    expect(illustration.attr('aria-hidden')).toEqual('true')
    expect(illustration.attr('role')).toEqual('presentation')
    // overwrite width and height as attributes, not via CSS
    expect(illustration.attr('width')).toEqual('400')
    expect(illustration.attr('height')).toEqual('192')
    expect(illustration[0].style.width).toEqual('')
    expect(illustration[0].style.height).toEqual('')
    // original file attributes
    expect(illustration.attr('viewBox')).toEqual(undefined)
  })

  it('should render an empty svg for missing illustrations', async () => {
    global.fetch.mockClear()
    global.fetch = vi.fn(url => Promise.resolve({ ok: false }))

    const illustration = createIllustration('illustrations/this-does-not-exist.svg', { width: 400, height: 192 })
    await new Promise(resolve => illustration[0].addEventListener('load', resolve))

    // add
    expect(illustration.attr('aria-hidden')).toEqual('true')
    // overwrite width and height as attributes, not via CSS
    expect(illustration.attr('width')).toEqual('400')
    expect(illustration.attr('height')).toEqual('192')
    expect(illustration[0].style.width).toEqual('')
    expect(illustration[0].style.height).toEqual('')
    // original file attributes
    expect(illustration.attr('viewBox')).toEqual(undefined)
    expect(illustration.children()).toHaveLength(0)
  })
})
