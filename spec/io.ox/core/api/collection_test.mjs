/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect, beforeEach, afterEach } from 'vitest'
import $ from '@/jquery'
import _ from '@/underscore'
import Backbone from '@/backbone'

import CollectionLoader from '@/io.ox/core/api/collection-loader'
import Pool from '@/io.ox/core/api/collection-pool'

function fetch (params) {
  const result = [{ id: 10 }, { id: 20 }, { id: 30 }, { id: 40 }, { id: 50 }, { id: 60 }]
  return $.Deferred().resolve(
    result.slice.apply(result, params.limit.split(','))
  )
}

function fetchAlternative () {
  return $.Deferred().resolve(
    [{ id: 70 }, { id: 20 }, { id: 40 }, { id: 50 }, { id: 80 }]
  )
}
describe('Core Collection', () => {
  describe('loader', () => {
    let loader

    beforeEach(() => {
      loader = new CollectionLoader({
        PRIMARY_PAGE_SIZE: 3,
        SECONDARY_PAGE_SIZE: 3,
        getQueryParams () {
          return {
            folder: 'default0/INBOX'
          }
        }
      })
      loader.fetch = fetch
    })

    describe('cid()', () => {
      it('is a function', () => {
        expect(loader.cid).toBeInstanceOf(Function)
      })

      it('handles missing parameters correctly', () => {
        expect(loader.cid()).toEqual('default')
      })

      it('handles empty object correctly', () => {
        expect(loader.cid({})).toEqual('default')
      })

      it('returns correct composite ID', () => {
        expect(loader.cid({ a: 1, b: 2 })).toEqual('a=1&b=2')
      })
    })

    describe('addIndex()', () => {
      it('is a function', () => {
        expect(loader.addIndex).toBeInstanceOf(Function)
      })

      it('injects index property', () => {
        const data = [{ a: 10 }]
        loader.addIndex(0, {}, data)
        expect(data).toStrictEqual([{ a: 10, index: 0 }])
      })

      it('calls each()', () => {
        const data = [{ a: 10 }]
        loader.each = function (obj) { obj.test = true }
        loader.addIndex(0, {}, data)
        expect(data).toStrictEqual([{ a: 10, index: 0, test: true }])
      })
    })

    describe('Instance', () => {
      it('returns a collection', () => {
        expect(loader.getDefaultCollection()).toBeInstanceOf(Backbone.Collection)
        expect(loader.getCollection()).toBeInstanceOf(Backbone.Collection)
      })

      it('has a load method that returns a collection', () => {
        const collection = loader.load()
        expect(collection).toBeInstanceOf(Backbone.Collection)
      })

      it('has a load method that loads initial data', async function () {
        const collection = loader.load()
        await new Promise(resolve => collection.once('load', resolve))
        expect(collection.pluck('id')).toStrictEqual([10, 20, 30])
        expect(collection.pluck('index')).toStrictEqual([0, 1, 2])
      })

      it('has a paginate method that loads more data', async function () {
        const collection = loader.load()
        await new Promise(resolve => collection.once('load', resolve))
        expect(collection.pluck('id')).toStrictEqual([10, 20, 30])
        expect(collection.pluck('index')).toStrictEqual([0, 1, 2])
        await new Promise(resolve => loader.paginate().once('paginate', resolve))
        expect(collection.pluck('id')).toStrictEqual([10, 20, 30, 40, 50, 60])
        expect(collection.pluck('index')).toStrictEqual([0, 1, 2, 3, 4, 5])
      })

      it('has a reload method that reloads data', async () => {
        const collection = loader.load()
        await new Promise(resolve => collection.once('load', resolve))
        loader.fetch = fetchAlternative
        await new Promise(resolve => loader.reload().once('reload', resolve))
        expect(collection.pluck('id')).toStrictEqual([70, 20, 40, 50, 80])
        expect(collection.pluck('index')).toStrictEqual([0, 1, 2, 3, 4])
      })
    })
  })
  describe('Pool', () => {
    let pool
    beforeEach(() => {
      pool = Pool.create('collection_spec')
    })
    afterEach(function () {
      pool.get('collection_spec').reset()
    })

    describe('add()', () => {
      it('should add a new element to the pool', () => {
        const obj = {
          id: '1337',
          folder_id: '1338'
        }
        const collection = pool.get('detail')
        expect(collection.get(_.cid(obj))).toBeUndefined()
        const c = pool.add('detail', obj)
        expect(c).toBeInstanceOf(Object)
        expect(collection.get(_.cid(obj))).toBeInstanceOf(Object)
      })
    })
  })
})
