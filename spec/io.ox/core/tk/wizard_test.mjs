/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest'
import $ from '@/jquery'

import Wizard from '@/io.ox/core/tk/wizard'

describe('The Wizard.', () => {
  let wizard
  beforeEach(() => {
    wizard = new Wizard()
  })

  afterEach(function () {
    wizard.close()
  })

  describe('Adding steps.', () => {
    it('has no steps', () => {
      expect(wizard.steps).toHaveLength(0)
    })

    it('starts with first step', () => {
      expect(wizard.currentStep).toEqual(0)
    })

    it('allows adding steps', () => {
      wizard.step()
      expect(wizard.steps.length).toEqual(1)
    })

    it('supports long chains', () => {
      wizard.step().end().step().end().step()
      expect(wizard.steps.length).toEqual(3)
    })
  })

  describe('Navigation.', () => {
    beforeEach(() => {
      wizard.step().end().step().end()
    })

    it('offers "next"', () => {
      expect(wizard.hasNext()).toEqual(true)
    })

    it('moves to next step', () => {
      wizard.next()
      expect(wizard.currentStep).toEqual(1)
    })

    it('does not offer "back" if at start', () => {
      expect(wizard.hasBack()).toEqual(false)
    })

    it('does offer "back" if at second step', () => {
      wizard.next()
      expect(wizard.hasBack()).toEqual(true)
    })

    it('does not move to invalid position', () => {
      wizard.setCurrentStep(-1)
      expect(wizard.currentStep).toEqual(0)
      wizard.setCurrentStep(+1)
      expect(wizard.currentStep).toEqual(1)
      wizard.setCurrentStep(+2)
      expect(wizard.currentStep).toEqual(1)
    })
  })

  describe('Events.', () => {
    it('forwards step-related events', () => {
      const spyNext = vi.fn()
      const spyBack = vi.fn()
      const spyClose = vi.fn()

      wizard
        .step()
        .end()
        .on({ 'step:next': spyNext, 'step:back': spyBack, 'step:close': spyClose })
        .withCurrentStep(function (step) {
          step.trigger('next')
          step.trigger('back')
          step.trigger('close')
        })

      expect(spyNext.mock.calls).toHaveLength(1)
      expect(spyBack.mock.calls).toHaveLength(1)
      expect(spyClose.mock.calls).toHaveLength(1)
    })
  })

  describe('Execution.', () => {
    beforeEach(() => {
      wizard.step().end().step().end()
    })

    it('shows up in the DOM', () => {
      wizard.start()
      expect($('.wizard-step').length).toEqual(1)
    })

    it('get removed from the DOM', () => {
      wizard.start().close()
      expect($('.wizard-step').length).toEqual(0)
    })

    it('shows proper content', () => {
      wizard.steps[0].content('Lorem ipsum')
      wizard.start()
      expect($('.wizard-step .wizard-content').text()).toEqual('Lorem ipsum')
    })

    it('has proper "start" button', () => {
      wizard.start()
      expect($('.wizard-step .btn[data-action="next"]').text()).toEqual('Start tour')
    })

    it('has proper "back" button', () => {
      wizard.start().next()
      expect($('.wizard-step .btn[data-action="back"]').text()).toEqual('Back')
    })
  })
})
