/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect } from 'vitest'
import { unitTesting } from '@/io.ox/core/http'

describe('http.js request handler ', () => {
  it('must check if a request is retryable or not', () => {
    const retryableUrls = [
      'appsuite/api/folders?action=get&altNames=true&id=default0%2FINBOX&timezone=UTC&tree=0&session=1234567890',
      'appsuite/api/mail?action=get&timezone=utc&embedded=false&sanitize=false&folder=default0%2FINBOX&id=120976&view=noimg&max_size=10240000&process_plain_text=false&pregenerate_previews=true&unseen=false&session=1234567890'
    ]
    const notRetryableUrls = [
      'appsuite/api/system?action=ping',
      'appsuite/api/mail?action=get&timezone=utc&embedded=false&sanitize=false&folder=default0%2FINBOX&id=120976&view=noimg&max_size=10240000&process_plain_text=false&pregenerate_previews=true&unseen=false&session=unset'
    ]

    for (const url of retryableUrls) {
      expect(unitTesting.isRetryable(url)).toBe(true)
    }
    for (const url of notRetryableUrls) {
      expect(unitTesting.isRetryable(url)).toBe(false)
    }
  })
})
