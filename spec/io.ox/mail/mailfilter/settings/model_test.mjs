/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect } from 'vitest'
import { gt } from 'gettext'

import mailfilterModel from '@/io.ox/mail/mailfilter/settings/model'

const emptyModel = {
  rulename: gt('New rule'),
  test: {
    id: 'true'
  },
  actioncmds: [],
  flags: [],
  active: true
}
const returnedModel = mailfilterModel.protectedMethods.provideEmptyModel()

describe('Mailfilter model', () => {
  describe('should provide empty model', () => {
    it('should return a object', () => {
      expect(returnedModel).toBeInstanceOf(Object)
    })

    it('should have a property rulename', () => {
      expect(returnedModel).toHaveProperty('rulename')
    })

    it('should be a string', () => {
      expect(typeof returnedModel.rulename).toEqual('string')
    })

    it('should be equal', () => {
      expect(returnedModel.rulename).toEqual(emptyModel.rulename)
    })

    it('should be a object', () => {
      expect(returnedModel.test).toBeInstanceOf(Object)
    })

    it('should have a property id', () => {
      expect(returnedModel.test).toHaveProperty('id', 'true')
    })

    it('should have a property actioncmds', () => {
      expect(returnedModel).toHaveProperty('actioncmds')
    })

    it('should be a array', () => {
      expect(returnedModel.actioncmds).toBeInstanceOf(Array)
    })

    it('should be empty', () => {
      expect(returnedModel.actioncmds).toHaveLength(0)
    })

    it('should have a property flags', () => {
      expect(returnedModel).toHaveProperty('flags')
    })

    it('should be a array', () => {
      expect(returnedModel.flags).toBeInstanceOf(Array)
    })

    it('should be empty', () => {
      expect(returnedModel.flags).toHaveLength(0)
    })

    it('should have a property active', () => {
      expect(returnedModel).toHaveProperty('active', true)
    })
  })
})
