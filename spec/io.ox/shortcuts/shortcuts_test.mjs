/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { transformProfile, normalizeModifiers, isObject, mergeObjects, arrayToTree, parseShortcut, resolveCommandOrControl, keyEventToStrings } from '@/io.ox/shortcuts/util'
import { describe, expect, it, vi } from 'vitest'

describe('Shortcut Utilities', () => {
  it('normalizeModifiers', () => {
    expect(normalizeModifiers('Control+Alt+a')).to.equal('Alt+Control+a')
  })

  it('transformProfile', () => {
    const tree = {
      default: {
        'io.ox/mail': {
          bold: ['CommandOrControl+b'],
          underline: ['CommandOrControl+u'],
          'New mail': ['g c a'],
          'Archive mail': ['e', 'f']
        }
      }
    }

    const result = {
      default: {
        'io.ox/mail': {
          'Control+b': 'bold',
          'Control+u': 'underline',
          g: {
            c: {
              a: 'New mail'
            }
          },
          e: 'Archive mail',
          f: 'Archive mail'
        }
      }
    }

    expect(transformProfile(tree)).to.deep.equal(result)
  })

  it('isObject', () => {
    expect(isObject({})).to.equal(true)
    expect(isObject([])).to.equal(false)
    expect(isObject('')).to.equal(false)
    expect(isObject(1)).to.equal(false)
    expect(isObject(null)).to.equal(false)
    expect(isObject(undefined)).to.equal(false)
  })

  it('mergeObjects', () => {
    const consoleMock = vi.spyOn(console, 'warn').mockImplementation(() => undefined)
    expect(mergeObjects({ a: 1 }, { b: 2 })).to.deep.equal({ a: 1, b: 2 })
    expect(mergeObjects({ a: 1 }, { a: 2 })).to.deep.equal({ a: 2 })
    expect(consoleMock).toHaveBeenCalledOnce()
    expect(mergeObjects({ a: 1 }, { b: { a: 3 } })).to.deep.equal({ a: 1, b: { a: 3 } })
  })

  it('arrayToTree', () => {
    expect(arrayToTree(['a', 'b', 'c'])).to.deep.equal({ a: { b: 'c' } })
    expect(arrayToTree(['a', 'b', 'c', 'd'])).to.deep.equal({ a: { b: { c: 'd' } } })
    expect(arrayToTree(['a', 'b', 'c', 'd', 'e'])).to.deep.equal({ a: { b: { c: { d: 'e' } } } })
  })

  it('parseShortcut', () => {
    expect(parseShortcut('Control+Alt+a')).to.deep.equal({ key: 'a', modifiers: ['Alt', 'Control'] })
  })

  it('resolveCommandOrControl', () => {
    expect(resolveCommandOrControl('CommandOrControl+a')).to.equal('Control+a')
  })

  it('keyEventToStrings', () => {
    const event = new KeyboardEvent('keydown', { key: 'a', code: 'KeyA', altKey: true, ctrlKey: true, metaKey: true, shiftKey: true })
    const result = {
      key: 'Alt+Control+Meta+Shift+a',
      shiftlessKey: 'Alt+Control+Meta+a',
      code: 'Alt+Control+Meta+Shift+KeyA'
    }
    expect(keyEventToStrings(event)).to.deep.equal(result)
  })
})
