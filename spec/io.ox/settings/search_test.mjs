/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

// cSpell:ignore España

import { describe, beforeEach, it, expect } from 'vitest'
import { reset, add, setConfigurable } from '@/io.ox/settings/index'

describe('Settings search', function () {
  let index
  describe('Simple search', function () {
    beforeEach(function () {
      index = reset()
      index.add('lorem ipsum', '#1')
      index.add('dolor sit', '#2')
      index.add('lorem amet', '#3')
      index.add('Bär', '#4')
      index.add('Très Bon', '#5')
      index.add('España', '#6')
      index.add('E-Mail', '#7')
      index.add('Work-week', '#8')
    })

    it('should return empty array for empty queries', function () {
      expect(index.searchForIds(' ')).toEqual([])
    })

    it('should search for simple words', function () {
      expect(index.searchForIds('lorem')).toEqual(['#1', '#3'])
    })

    it('should search for two words', function () {
      expect(index.searchForIds('lorem ip')).toEqual(['#1'])
    })

    it('should search case-insensitive', function () {
      expect(index.searchForIds('DoLOr')).toEqual(['#2'])
    })

    it('should ignore white-space', function () {
      expect(index.searchForIds('  amet ')).toEqual(['#3'])
    })

    it('should handle accents', function () {
      expect(index.searchForIds('bar')).toEqual(['#4'])
      expect(index.searchForIds('tres')).toEqual(['#5'])
      expect(index.searchForIds('espana')).toEqual(['#6'])
    })

    it('should handle words with dashes', function () {
      expect(index.searchForIds('email')).toEqual(['#7'])
      expect(index.searchForIds('e-mail')).toEqual(['#7'])
      expect(index.searchForIds('workweek')).toEqual(['#8'])
      expect(index.searchForIds('work-week')).toEqual(['#8'])
    })
  })

  describe('complex examples', function () {
    beforeEach(function () {
      index = reset()
      add({ id: 'TEST1', text: 'This is an example text', page: 'Page 1', section: 'Section A', selector: '.selector-a', priority: 3 })
      add({ id: 'TEST2', text: 'This is an another text', page: 'Page 2', section: 'Section B', selector: '.selector-b', priority: 2 })
    })

    it('should handle settings sections', function () {
      expect(index.searchForIds('text')).toEqual(['TEST1', 'TEST2'])
      expect(index.searchForIds('example')).toEqual(['TEST1'])
      expect(index.searchForIds('ANOTHER')).toEqual(['TEST2'])
    })

    it('should find result in order', function () {
      expect(index.search('text')).toEqual([
        { text: 'This is an another text', page: 'Page 2', section: 'Section B', selector: '.selector-b', priority: 2 },
        { text: 'This is an example text', page: 'Page 1', section: 'Section A', selector: '.selector-a', priority: 3 }
      ])
    })

    it('should ignore non-configurable', function () {
      index.search('text')
      index.resetCache()
      setConfigurable({ TEST1: false })

      expect(index.search('text')).toEqual([
        { text: 'This is an another text', page: 'Page 2', section: 'Section B', selector: '.selector-b', priority: 2 }
      ])
    })
  })
})
