/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect, vi, afterAll, beforeAll } from 'vitest'
import { mount, flushPromises } from '@vue/test-utils'
import BaseIcon from '@/io.ox/vueComponents/base-icon.vue'
import { settings as coreSettings } from '@/io.ox/core/settings'

const icons = {
  './themes/default/icons/bi/chevron-right.svg': `<svg class="bi bi-chevron-right" width="16" height="16" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 16">
  <path xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708"></path>
</svg>`,
  './themes/default/icons/bi/chevron-top.svg': `<svg class="bi bi-chevron-right" width="16" height="16" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 16">
  <script>document.write('XSS')</script>
</svg>`
}

describe('BaseIcon', () => {
  beforeAll(() => {
    global.fetch = vi.fn(url => Promise.resolve({
      ok: true,
      text: (a) => {
        return Promise.resolve(icons[url])
      }
    }))
  })

  afterAll(() => {
    global.fetch.mockClear()
  })

  it('fetches alternative icons when overridden in settings (io.ox/core//theming/customIcons)', async () => {
    coreSettings.set('theming/customIcons', {
      'bi/chevron-left.svg': './themes/default/icons/bi/chevron-right.svg'
    })
    coreSettings.trigger('load')

    // standard icon
    const rightIcon = mount(BaseIcon, { props: { iconName: 'bi/chevron-right.svg' } })
    await flushPromises()
    const svg = rightIcon.find('svg')
    expect(svg.element.getAttribute('aria-hidden')).toEqual('true')
    expect(svg.element.getAttribute('width')).toEqual('16')
    expect(svg.element.getAttribute('height')).toEqual('16')
    expect(svg.element.querySelector('path')).toBeDefined()

    expect(rightIcon.html()).toEqual(icons['./themes/default/icons/bi/chevron-right.svg'])

    // icon overridden in settings
    const leftIcon = mount(BaseIcon, { props: { iconName: 'bi/chevron-left.svg' } })
    await new Promise(resolve => setTimeout(resolve, 100))
    expect(leftIcon.html()).toEqual(icons['./themes/default/icons/bi/chevron-right.svg'])

    // try to inject malicious code
    const topIcon = mount(BaseIcon, { props: { iconName: 'bi/chevron-top.svg' } })
    expect(topIcon.html()).to.not.contain('XSS')
  })
})
