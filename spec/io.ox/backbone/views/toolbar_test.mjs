/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, it, expect, beforeEach, vi } from 'vitest'

import $ from '@/jquery'
import _ from '@/underscore'

import ToolbarView from '@/io.ox/backbone/views/toolbar'
import ext from '@/io.ox/core/extensions'

$.fn.tooltip = vi.fn(() => {
  return {
    tooltip () { vi.fn() },
    replaceWith () { vi.fn() }
  }
})

vi.mock('@/io.ox/core/folder/api', () => {
  return {
    getFolder (id) {
      return $.when({ id })
    }
  }
})

vi.mock('@/io.ox/core/cache/indexeddb', () => {
  return {
    open (key) {
      return false
    }
  }
})

const folderId = 'toolbar/test'
const data = [{ id: 1, folder_id: folderId }, { id: 2, folder_id: folderId }]
let enableEight = false

// define links
ext.point('io.ox/test/toolbar/links').extend(
  { id: 'one', title: 'One', prio: 'hi', ref: 'io.ox/test/toolbar/actions/one', section: 'a' },
  { id: 'two', title: 'Two', prio: 'hi', ref: 'io.ox/test/toolbar/actions/two', icon: 'bi/trash.svg', section: 'a' },
  { id: 'three', label: 'Three', prio: 'hi', ref: 'io.ox/test/toolbar/actions/three', drawDisabled: true, section: 'a' },
  { id: 'four', title: 'Four', prio: 'lo', ref: 'io.ox/test/toolbar/actions/four', section: 'b' },
  { id: 'five', title: 'Five', prio: 'lo', ref: 'io.ox/test/toolbar/actions/five', section: 'b' },
  { id: 'six', title: 'Six', prio: 'lo', ref: 'io.ox/test/toolbar/actions/six', section: 'c' },
  { id: 'seven', title: 'Seven', prio: 'lo', ref: 'io.ox/test/toolbar/actions/seven', section: 'c' },
  { id: 'eight', title: 'Eight', prio: 'lo', ref: 'io.ox/test/toolbar/actions/eight', section: 'd', sectionTitle: 'foo' }
)

// define actions
action('io.ox/test/toolbar/actions/one', {
  device: 'chrome',
  collection: 'some'
})

action('io.ox/test/toolbar/actions/two', {
  // backwards compatible
  requires (e) {
    if (!e.collection.has('some')) return
    return $.when(true)
  }
})

action('io.ox/test/toolbar/actions/three', {
  // drawDisabled
  matches: _.constant(false)
})

action('io.ox/test/toolbar/actions/four', {
  matches: _.constant(true)
})

action('io.ox/test/toolbar/actions/five')

action('io.ox/test/toolbar/actions/six', {
  toggle: false
})

action('io.ox/test/toolbar/actions/seven', {
  device: 'firefox'
})

action('io.ox/test/toolbar/actions/eight', {
  matches: function () {
    if (!enableEight) return false
    return _.wait(1).then(_.constant(true))
  }
})

function action (id, options) {
  ext.point(id).extend(_.extend({ id: 'default', index: 100 }, options))
}

describe('Actions', () => {
  //
  // Action Toolbar
  //

  describe('Toolbar view', () => {
    let toolbar

    beforeEach(function () {
      toolbar = new ToolbarView({ point: 'io.ox/test/toolbar/links', simple: true })
    })

    it('trigger ready event synchronously', () => {
      const spy = vi.fn()
      toolbar.on('ready', spy)
      toolbar.setSelection(data)
      expect(spy.mock.calls).toHaveLength(1)
    })

    it('waits for an async action', async () => {
      enableEight = true
      const spy = vi.fn()
      toolbar.on('ready', spy)
      const promise = new Promise((resolve) => {
        toolbar.on('ready:toolbar/test.1,toolbar/test.2', function (selection) {
          // use try/catch to see the error instead of timeout
          try {
            expect(selection).toEqual('toolbar/test.1,toolbar/test.2')
            expect(spy.mock.calls).toHaveLength(1)
            spy.mockClear()
          } catch (e) {
            console.error(e)
          } finally {
            resolve()
          }
        })
      })
      toolbar.setSelection(data)
      expect(spy.mock.calls).toHaveLength(0)
      enableEight = false
      await promise
    })
  })
})
