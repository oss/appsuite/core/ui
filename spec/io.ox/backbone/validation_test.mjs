/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, expect, it } from 'vitest'

import validation, { validateName } from '@/io.ox/backbone/validation'

describe('Core Backbone validations', () => {
  describe('validating the "any float" format', () => {
    it('should accept 123,45', () => {
      expect(validation.formats.anyFloat('123.45')).toEqual(true)
      expect(validation.formats.anyFloat('123,45')).toEqual(true)
    })
    it('should accept 100,00 as well as 100.00', () => {
      expect(validation.formats.anyFloat('100.00')).toEqual(true)
      expect(validation.formats.anyFloat('100,00')).toEqual(true)
    })
    it('should reject no-number,3', () => {
      expect(validation.formats.anyFloat('no-number,3')).toEqual('Please enter a valid number')
    })
  })

  describe('provided validateName helper', () => {
    it('should only check for emptiness by default', () => {
      expect(validateName('filename:/', 'file')).toHaveLength(0)
      expect(validateName(undefined, 'file')).toHaveLength(1)
      expect(validateName(null, 'file')).toHaveLength(1)
      expect(validateName(' ', 'file')).toHaveLength(1)
    })
    it('should detect slashes', () => {
      expect(validateName('filename:/', 'file', { noSlashes: true })).toHaveLength(1)
    })
    it('should detect colons', () => {
      expect(validateName('filename:/', 'file', { noColons: true })).toHaveLength(1)
    })
    it('should detect slashes and colons', () => {
      expect(validateName('filename:/', 'file', { noSlashes: true, noColons: true })).toHaveLength(2)
    })
  })
})
