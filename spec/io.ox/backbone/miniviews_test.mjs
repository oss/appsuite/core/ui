/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { describe, expect, it, beforeEach, vi } from 'vitest'
import $ from '@/jquery'
import Backbone from '@/backbone'

import common from '@/io.ox/backbone/mini-views/common'
import date from '@/io.ox/backbone/mini-views/date'
import moment from 'moment-timezone'

describe('Core Backbone mini-views.', () => {
  describe('AbstractView view', () => {
    let view

    beforeEach(() => {
      view = new common.AbstractView({ name: 'test', model: new Backbone.Model() })
    })

    it('has an "initialize" function', () => {
      expect(view.initialize).toBeInstanceOf(Function)
    })

    it('has a "dispose" function', () => {
      expect(view.dispose).toBeInstanceOf(Function)
    })

    it('has a "valid" function', () => {
      expect(view.valid).toBeInstanceOf(Function)
    })

    it('has an "invalid" function', () => {
      expect(view.invalid).toBeInstanceOf(Function)
    })

    it('has a model', () => {
      expect(view.model).toBeDefined()
    })

    it('references itself via data("view")', () => {
      expect(view.$el.data('view')).toEqual(view)
    })
  })

  describe('InputView', () => {
    let view, model
    beforeEach(() => {
      model = new Backbone.Model({ test: '' })
      view = new common.InputView({ name: 'test', model })
    })

    it('is an input field', () => {
      expect(view.$el.prop('tagName')).toEqual('INPUT')
      expect(view.$el.attr('type')).toEqual('text')
    })

    it('has a setup function', () => {
      expect(view.setup).toBeInstanceOf(Function)
    })

    it('has an update function', () => {
      expect(view.update).toBeInstanceOf(Function)
    })

    it('has a render function', () => {
      expect(view.render).toBeInstanceOf(Function)
    })

    it('has a render function that returns "this"', () => {
      const result = view.render()
      expect(result).toEqual(view)
    })

    it('has a render function that calls update', () => {
      const spy = vi.spyOn(view, 'update')
      view.render()
      expect(spy.mock.calls).toHaveLength(1)
    })

    it('should render a name attribute', () => {
      view.render()
      expect(view.$el.attr('name')).toEqual('test')
    })

    it('should be empty', () => {
      expect(view.$el.val()).toHaveLength(0)
      expect(model.get('test')).toHaveLength(0)
    })

    it('reflects model changes', () => {
      model.set('test', '1337')
      expect(view.$el.val()).toEqual('1337')
    })

    it('updates the model', () => {
      view.$el.val('Hello World').trigger('change')
      expect(model.get('test')).toEqual('Hello World')
    })
  })

  describe('TextView', () => {
    let model, view

    beforeEach(() => {
      model = new Backbone.Model({ test: '' })
      view = new common.TextView({ name: 'test', model })
    })

    it('is an input field', () => {
      expect(view.$el.prop('tagName')).toEqual('TEXTAREA')
    })

    it('reflects model changes', () => {
      model.set('test', 'Lorem Ipsum')
      expect(view.$el.val()).toEqual('Lorem Ipsum')
    })

    it('updates the model', () => {
      view.$el.val('Lorem Ipsum').trigger('change')
      expect(model.get('test')).toEqual('Lorem Ipsum')
    })
  })

  describe('CheckboxView', () => {
    let model, view

    beforeEach(() => {
      model = new Backbone.Model({ test: '' })
      view = new common.CheckboxView({ name: 'test', model })
    })

    it('is an input field', () => {
      expect(view.$el.prop('tagName')).toEqual('INPUT')
      expect(view.$el.attr('type')).toEqual('checkbox')
    })

    it('reflects model changes', () => {
      model.set('test', true)
      expect(view.$el.prop('checked')).toEqual(true)
    })

    it('updates the model', () => {
      view.$el.prop('checked', true).trigger('change')
      expect(model.get('test')).toEqual(true)
    })
  })

  describe('PasswordView', () => {
    let model, view
    beforeEach(() => {
      model = new Backbone.Model({ test: '' })
      view = new common.PasswordView({ name: 'test', model })
    })

    it('is an input field', () => {
      expect(view.$el.prop('tagName')).toEqual('INPUT')
      expect(view.$el.attr('type')).toEqual('password')
    })

    it('has a setup function', () => {
      expect(view.setup).toBeInstanceOf(Function)
    })

    it('has an update function', () => {
      expect(view.update).toBeInstanceOf(Function)
    })

    it('has a render function', () => {
      expect(view.render).toBeInstanceOf(Function)
    })

    it('has a render function that returns "this"', () => {
      const result = view.render()
      expect(result).toEqual(view)
    })

    it('has a render function that calls update', () => {
      const spy = vi.spyOn(view, 'update')
      view.render()
      expect(spy.mock.calls).toHaveLength(1)
    })

    it('should render a name attribute', () => {
      view.render()
      expect(view.$el.attr('name')).toEqual('test')
    })

    it('should have a autocomplete attribute set to off', () => {
      view.render()
      expect(view.$el.attr('autocomplete')).toEqual('off')
    })

    it('should have a autocorrect attribute set to off', () => {
      view.render()
      expect(view.$el.attr('autocorrect')).toEqual('off')
    })

    it('should be empty', () => {
      expect(view.$el.val()).toHaveLength(0)
      expect(model.get('test')).toHaveLength(0)
    })

    it('should show stars if no value is set', () => {
      model.set('test', null)
      expect(view.$el.val()).toEqual('********')
    })

    it('reflects model changes', () => {
      model.set('test', '1337')
      expect(view.$el.val()).toEqual('1337')
    })

    it('updates the model', () => {
      view.$el.val('new password').trigger('change')
      expect(model.get('test')).toEqual('new password')
    })
  })

  describe('DateView', () => {
    let modelDate, model, view

    beforeEach(() => {
      modelDate = moment.utc({ year: 2012, month: 1, date: 5 })
      model = new Backbone.Model({ test: modelDate.valueOf() })
      view = new date.DateSelectView({ name: 'test', model, label: $('<label>').text('label') })
      view.render()
    })

    it('is a <div> tag with three <select> controls', () => {
      expect(view.$el.prop('tagName')).toEqual('DIV')
      expect(view.$el.children().length).toEqual(3)
      expect(view.$el.find('div > select').length).toEqual(3)
    })

    it('contains 1604 as fallback year', () => {
      expect(view.$el.find('.year').children().first().attr('value')).toEqual('1604')
    })

    it('contains an empty option for month', () => {
      expect(view.$el.find('.month').children().eq(0).attr('value')).toHaveLength(0)
    })

    it('lists month as one-digit numbers starting with 0', () => {
      expect(view.$el.find('.month').children().eq(1).attr('value')).toEqual('0')
    })

    it('contains an empty option for dates', () => {
      expect(view.$el.find('.date').children().eq(0).attr('value')).toHaveLength(0)
    })

    it('lists dates as one-digit numbers starting with 1', () => {
      expect(view.$el.find('.date').children().eq(1).attr('value')).toEqual('1')
    })

    it('reflects model state', () => {
      expect(view.$el.find('.date').val()).toEqual(String(modelDate.date()))
      expect(view.$el.find('.month').val()).toEqual(String(modelDate.month()))
      expect(view.$el.find('.year').val()).toEqual(String(modelDate.year()))
    })

    it('updates the model', () => {
      view.$el.find('.year').val('1978').trigger('change')
      view.$el.find('.month').val('0').trigger('change')
      view.$el.find('.date').val('29').trigger('change')
      expect(model.get('test')).toEqual(Date.UTC(1978, 0, 29))
    })
  })

  describe('ErrorView', () => {
    let model, container, containerDefault, inputView, view, viewSecond

    beforeEach(() => {
      model = new Backbone.Model({ test: '' })
      container = $('<div class="row">')
      containerDefault = $('<div class="form-group">')
      inputView = new common.InputView({ name: 'test', model })
      view = new common.ErrorView({ selector: '.row' })
      viewSecond = new common.ErrorView()
    })

    it('is an span container', () => {
      expect(view.$el.prop('tagName')).toEqual('SPAN')
      expect(view.$el.attr('class')).toEqual('help-block')
    })

    it('has a invalid function', () => {
      expect(view.invalid).toBeInstanceOf(Function)
    })

    it('has an valid function', () => {
      expect(view.valid).toBeInstanceOf(Function)
    })

    it('should render a aria-live attribute', () => {
      view.render()
      expect(view.$el.attr('aria-live')).toEqual('assertive')
    })

    it('has a render function', () => {
      expect(view.render).toBeInstanceOf(Function)
    })

    it('has a render function that returns "this"', () => {
      const result = view.render()
      expect(result).toEqual(view)
    })

    it('has a getContainer function ', () => {
      expect(view.getContainer).toBeInstanceOf(Function)
    })

    it('should listen to the custom container', () => {
      container.append(
        inputView.render().$el,
        view.render().$el
      )
      expect(view.getContainer().empty()[0]).toEqual(container[0])
    })

    it('should listen to the default container', () => {
      containerDefault.append(
        inputView.render().$el,
        viewSecond.render().$el
      )
      expect(viewSecond.getContainer().empty()[0]).toEqual(containerDefault[0])
    })
  })
})
