## Summary

A brief summary of the bug.

## Basic Information

- **Affected versions**: [e.g. 7.10.6, 8.20]
- **Environment**: [e.g oxcloud easy]
- **Reproducibility**: [e.g always/sometimes/unable]
- **OTRS Ticket Link**: https://otrs.open-xchange.com/otrs/index.pl?Action=AgentTicketZoom&TicketNumber=INSERT_OTRS_TICKET_ID_HERE

## Steps to Reproduce

1. Step 1
2. Step 2
3. ...

### Expected Behavior

A clear and concise description of what you expected to happen.

### Actual Behavior

A clear and concise description of what actually happened.

## Screenshots

If applicable, add screenshots or error messages to help explain the issue.

## Misc

Any other information that might be helpful

/label ~"Bug"
