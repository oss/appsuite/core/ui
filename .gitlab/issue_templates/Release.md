
# Release

If its a patch release please additonally follow https://confluence.open-xchange.com/pages/viewpage.action?spaceKey=PL&title=App+Suite+8+Patch+Process

JIRA Crosslink: https://jira.open-xchange.com/browse/CTR-???

## What's New

- [ ] illustration (whatsnew/meta.js)
- [ ] text (whatsnew/meta.js)
- [ ] bump latest version (whatsnew/util.js)

## Components


### Guided Tours

[Instructions](https://gitlab.open-xchange.com/appsuite/support/-/blob/main/docs/release-workflow/core-guidedtours.md)

- [ ] release
- [ ] helm
- [ ] stack

### User Guide

[Instructions](https://gitlab.open-xchange.com/appsuite/support/-/blob/main/docs/release-workflow/core-user-guide.md)

- [ ] stable-branch
- [ ] release
- [ ] helm
- [ ] stack

### UI

[Instructions](https://gitlab.open-xchange.com/appsuite/support/-/blob/main/docs/release-workflow/core-ui.md)

- [ ] What's new entry (like [@7200fdee](https://gitlab.open-xchange.com/appsuite/web-apps/ui/-/commit/7200fdee9cc9dc7c22b8443cbc5ae9e0edb33461))
- [ ] stable-branch
- [ ] release
- [ ] helm
- [ ] stack

## Release notes

**Instructions**

- switch to https://gitlab.open-xchange.com/appsuite/releases at branch `stable-8.xx`
- run `npx update-release 8.xx` to fetch latest changelogs, commit and push

**Tasks**

- [ ] release notes
- [ ] illustration
- [ ] breaking changes
- [ ] noteworthy changes
