/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import moment from '@/moment'

import ext from '@/io.ox/core/extensions'
import Backbone from '@/backbone'
import ExtensibleView from '@/io.ox/backbone/views/extensible'
import DisposableView from '@/io.ox/backbone/views/disposable'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import Colorpicker from '@/io.ox/backbone/mini-views/colorpicker'
import capabilities from '@/io.ox/core/capabilities'
import * as util from '@/io.ox/core/settings/util'
import * as mailUtil from '@/io.ox/mail/util'
import * as contactsUtil from '@/io.ox/contacts/util'
import yell from '@/io.ox/core/yell'
import VacationNoticeModel from '@/io.ox/mail/mailfilter/vacationnotice/model'
import AutoforwardModel from '@/io.ox/mail/mailfilter/autoforward/model'
import mailfilter from '@/io.ox/core/api/mailfilter'
import { createIcon, createIllustration } from '@/io.ox/core/components'
import mini from '@/io.ox/backbone/mini-views'
import api from '@/io.ox/core/api/account'
import { st, isConfigurable, setConfigurable, index } from '@/io.ox/settings/index'

import { settings } from '@/io.ox/mail/settings'
import gt from 'gettext'

// we need the existing extension for signatures
import '@/io.ox/mail/settings/signatures/settings/pane'

// not possible to set nested defaults, so do it here
if (settings.get('features/registerProtocolHandler') === undefined) {
  settings.set('features/registerProtocolHandler', true)
}

ext.point('io.ox/mail/settings/detail').extend({
  index: 100,
  id: 'view',
  draw () {
    this.append(
      util.header(
        st.MAIL,
        'ox.appsuite.user.sect.email.settings.html'
      ),
      new ExtensibleView({ point: 'io.ox/mail/settings/detail/view', model: settings })
        .build(function () {
          this.$el.addClass('settings-body io-ox-mail-settings')
          this.listenTo(settings, 'change', function () {
            settings.saveAndYell().then(
              function ok () {
                // update mail API
                import('@/io.ox/mail/api').then(function ({ default: mailAPI }) {
                  mailAPI.updateViewSettings()
                })
              },
              function fail (error) {
                console.error(error)
                yell('error', gt('Could not save settings'))
              }
            )
          })
        })
        .render().$el
    )
  }
})

ext.point('io.ox/mail/settings/detail/view').extend(
  {
    id: 'layout',
    index: 100,
    render (baton) {
      if (!isConfigurable.LAYOUT_MAIL) return
      util.renderExpandableSection(st.LAYOUT_MAIL, st.LAYOUT_MAIL_EXPLANATION, 'io.ox/mail/settings/layout', true).call(this, baton)
    }
  },
  {
    id: 'reading',
    index: 200,
    render: util.renderExpandableSection(st.READING, st.READING_EXPLANATION, 'io.ox/mail/settings/reading')
  },
  {
    id: 'inbox-categories',
    index: 300,
    render (baton) {
      if (!isConfigurable.INBOX_CATEGORIES) return
      util.renderExpandableSection(st.INBOX_CATEGORIES, st.INBOX_CATEGORIES_EXPLANATION, 'io.ox/mail/settings/inbox-categories').call(this, baton)
    }
  },
  {
    id: 'signatures',
    index: 400,
    render: util.renderExpandableSection(st.SIGNATURES, st.SIGNATURES_EXPLANATION, 'io.ox/mail/settings/signatures')
  },
  {
    id: 'compose',
    index: 500,
    render: util.renderExpandableSection(st.COMPOSE_REPLY, st.COMPOSE_REPLY_EXPLANATION, 'io.ox/mail/settings/compose')
  },
  {
    id: 'aliases',
    index: 600,
    render: util.renderExpandableSection(st.ALIASES, st.ALIASES_EXPLANATION, 'io.ox/mail/settings/aliases')
  },
  {
    id: 'templates',
    index: 700,
    render (baton) {
      if (!isConfigurable.TEMPLATES) return
      util.renderExpandableSection(st.TEMPLATES, st.TEMPLATES_EXPLANATION, 'io.ox/mail/settings/templates').call(this, baton)
    }
  },
  {
    id: 'Rules',
    index: 800,
    render: util.renderExpandableSection(st.RULES, st.RULES_EXPLANATION, 'io.ox/mail/settings/rules')
  },
  {
    id: 'folders',
    index: 900,
    render (baton) {
      if (!isConfigurable.FOLDERS) return
      util.renderExpandableSection(st.FOLDERS, st.FOLDERS_EXPLANATION, 'io.ox/mail/settings/folders').call(this, baton)
    }
  },
  {
    id: 'advanced',
    index: 10000,
    render: util.renderExpandableSection(st.MAIL_ADVANCED, '', 'io.ox/mail/settings/advanced')
  }
)

ext.point('io.ox/mail/settings/layout').extend(
  {
    id: 'layout',
    index: 100,
    render ({ model }) {
      if (!isConfigurable.LAYOUT_MAIL) return
      const list = [
        { id: 'vertical', title: gt('Vertical') },
        { id: 'horizontal', title: gt('Horizontal') },
        { id: 'list', title: gt('List') }
      ]
      const radioCardView = new util.RadioCardView({ name: 'layout', model, list })
      radioCardView.renderCard = (option) => {
        return createIllustration(`illustrations/layout-${option.id}.svg`, { width: 80, height: 60 })
          .css({ color: 'var(--accent)', width: 120, height: 'auto', padding: '4px 2px' })
          .addClass('card-rect')
      }
      this.append(
        $('<div class="pseudo-fieldset">').append(
          radioCardView.render().$el
        )
      )
    }
  }
)

function getFontSizeBlock (model) {
  if (!isConfigurable.FONT_SIZE) return $()
  const list = [
    { label: gt('Very small'), value: 'text-very-small' },
    { label: gt('Small'), value: 'text-small' },
    { label: gt('Medium'), value: 'text-medium' },
    { label: gt('Large'), value: 'text-large' },
    { label: gt('Very large'), value: 'text-very-large' }
  ]
  return util.fieldset(
    st.FONT_SIZE,
    new mini.SelectView({ name: 'fontSize', id: 'fontSize', list, model }).render().$el.addClass('mb-16').css('max-width', '50%'),
    util.explanation(gt('HTML formatted emails, like newsletters, will keep their original font size.'))
  )
}

ext.point('io.ox/mail/settings/reading').extend(
  {
    id: 'message-list',
    index: 100,
    render ({ model }) {
      const list = [
        { value: 'simple', label: gt('Simple') },
        { value: 'avatars', label: gt('Contact pictures') }
      ]
      if (model.get('selectionMode') !== 'alternative') {
        list.push({ value: 'checkboxes', label: gt('Checkboxes') })
      }

      this.append(
        util.fieldset(
          st.MESSAGE_LIST,
          $('<div class="flex-row flex-wrap">').append(
            $('<div class="flex-grow">').append(
              _.device('smartphone') ? '' : new mini.CustomRadioView({ name: 'listViewLayout', model, list }).render().$el.addClass('mb-24'),
              isConfigurable.SHOW_TEXT_PREVIEW
                ? util.checkbox('showTextPreview', st.SHOW_TEXT_PREVIEW, settings)
                : [],
              isConfigurable.SHOW_CATEGORIES
                ? util.checkbox('showCategories', st.SHOW_CATEGORIES, settings)
                : [],
              util.checkbox('exactDates', st.EXACT_DATES, settings),
              util.checkbox('alwaysShowSize', st.SHOW_SIZE, settings)
            ),
            $('<div style="width: 360px" aria-hidden="true">').append(
              new MessageListPreview({ model: settings }).render().$el
            )
          )
        )
      )
    }
  },
  {
    id: 'reading-pane',
    index: 200,
    render ({ model }) {
      this.append(
        util.fieldset(
          st.READING_PANE,
          util.explanation(gt('Set your preferred setting for reading e-mails.')),
          $('<div class="flex-row flex-wrap">').append(
            $('<div class="flex-grow">').append(
              util.checkbox('isColorQuoted', st.COLOR_QUOTES, model),
              util.checkbox('useFixedWidthFont', st.FIXED_WIDTH_FONT, model),
              // font size
              getFontSizeBlock(model)
            ),
            $('<div style="width: 360px" aria-hidden="true">').append(
              new ReadingPanePreview({ model }).render().$el
            )
          )
        )
      )
    }
  },
  {
    id: 'mark-as-read',
    index: 300,
    render () {
      const list = [
        // #. Mark email as read
        { label: gt('Immediately'), value: 'instant' },
        // #. Mark email as read; %1$d = seconds
        { label: gt('After %1$d seconds', 5), value: 'fast' },
        // #. Mark email as read
        { label: gt('After %1$d seconds', 20), value: 'slow' },
        // #. Mark email as read
        { label: gt('Never'), value: 'never' }
      ]
      this.append(
        util.fieldset(
          st.MARK_READ,
          util.explanation(gt('Determine when and if an email is marked as read upon opening.')),
          new mini.CustomRadioView({ name: 'markAsRead', model: settings, list }).render().$el
        )
      )
    }
  }
)

ext.point('io.ox/mail/settings/folders').extend(
  {
    id: 'folders',
    index: 100,
    render () {
      if (!isConfigurable.UNSEEN_FOLDER && !isConfigurable.FLAGGED_FOLDER) return
      this.append(
        util.fieldset(
          st.SPECIAL_FOLDERS,
          $(),
          // unseen folder
          isConfigurable.UNSEEN_FOLDER
            ? util.checkbox('unseenMessagesFolder', st.UNSEEN_FOLDER, settings)
            : [],
          // flagged folder
          isConfigurable.FLAGGED_FOLDER
            ? util.checkbox('flaggedMessagesFolder', st.FLAGGED_FOLDER, settings)
            : []
        )
      )
    }
  },
  {
    id: 'imap-subscriptions',
    index: 200,
    render () {
      if (!isConfigurable.IMAP_SUBSCRIPTIONS) return

      this.append(
        util.fieldset(
          gt('Subscribe to IMAP folders'),
          $(),
          $('<button type="button" class="btn btn-default mt-8" data-action="change-image-supscriptions">')
            .text(st.IMAP_SUBSCRIPTIONS + ' \u2026')
            .on('click', () => {
              ox.load(() => import('@/io.ox/core/folder/actions/imap-subscription'))
                .then(({ default: subscribe }) => { subscribe() })
            })
        )
      )
    }
  }
)

ext.point('io.ox/mail/settings/inbox-categories').extend(
  {
    id: 'inbox-categories',
    index: 100,
    render () {
      // aka tabbed inbox
      this.append(
        $('<div class="pseudo-fieldset">').append(
          $('<div class="row">').append(
            $('<div class="col-md-6">').append(
              $('<div class="">').append(
                // $('<label for="regional-settings" class="block">').text(gt('Regional settings')),
                $('<button type="button" class="btn btn-default" data-action="edit-inbox-categories">')
                  .text(st.INBOX_CATEGORIES_BUTTON)
                  .on('click', () => {
                    import('@/io.ox/mail/categories/edit').then(({ default: dialog }) => dialog.open())
                  })
              )
            )
          )
        )
      )
    }
  }
)

const formatOptions = [
  { label: gt('HTML'), value: 'html' },
  { label: gt('Plain text'), value: 'text' },
  { label: gt('HTML and plain text'), value: 'alternative' }
]

const fontNameOptions = [{ label: gt('Use browser default'), value: 'browser-default' }].concat(
  mailUtil.getFontFormats().split(';')
    .filter(function (str) {
      return !/^(Web|Wing)dings/.test(str)
    })
    .map(function (pair) {
      pair = pair.split('=')
      return { label: pair[0], value: pair[1] }
    })
)

const fontSizeOptions = [
  { label: gt('Use browser default'), value: 'browser-default' },
  { label: '8pt', value: '8pt' },
  { label: '10pt', value: '10pt' },
  { label: '11pt', value: '11pt' },
  { label: '12pt', value: '12pt' },
  { label: '13pt', value: '13pt' },
  { label: '14pt', value: '14pt' },
  { label: '16pt', value: '16pt' },
  { label: '18pt', value: '18pt' },
  { label: '24pt', value: '24pt' },
  { label: '36pt', value: '36pt' }
]

function toggleDropdownStates (dropdownViews = []) {
  const isPlainText = settings.get('messageFormat') === 'text'
  dropdownViews.forEach(dropdownView => dropdownView.$toggle.prop('disabled', isPlainText).toggleClass('disabled', isPlainText))
}

function getExampleTextCSS () {
  const css = {
    'font-size': settings.get('defaultFontStyle/size', 'browser-default'),
    'font-family': settings.get('defaultFontStyle/family', 'browser-default'),
    color: settings.get('defaultFontStyle/color', 'transparent')
  }
  // using '' as a value removes the attribute and thus any previous styling
  if (css['font-size'] === 'browser-default') css['font-size'] = ''
  if (css['font-family'] === 'browser-default') css['font-family'] = ''
  if (css.color === 'transparent') css.color = ''
  return css
}

ext.point('io.ox/mail/settings/compose').extend(
  {
    id: 'compose-format',
    index: 100,
    async render () {
      if (capabilities.has('guest')) return
      if (!isConfigurable.MESSAGE_FORMAT) return

      this.append(
        util.fieldset(st.MESSAGE_FORMAT,
          $(),
          util.compactSelect('messageFormat', '', settings, formatOptions)
            .find('label').addClass('sr-only').end()
            .addClass('mt-8')
        )
      )
    }
  },
  {
    id: 'defaultStyle',
    index: 200,
    render () {
      if (!isConfigurable.DEFAULT_STYLE) return

      let exampleText, defaultStyleSection

      const model = new Backbone.Model({
        family: settings.get('defaultFontStyle/family', 'browser-default'),
        size: settings.get('defaultFontStyle/size', 'browser-default'),
        color: settings.get('defaultFontStyle/color', 'transparent')
      })
      const fontFamilySelect = new Dropdown({ caret: true, model, label: gt('Font'), tagName: 'div', className: 'dropdown fontnameSelectbox', name: 'family' })
      const fontSizeSelect = new Dropdown({ caret: true, model, label: gt('Size'), tagName: 'div', className: 'dropdown fontsizeSelectbox', name: 'size' })

      _(fontNameOptions).each(function (item, index) {
        if (index === 1) fontFamilySelect.divider()
        fontFamilySelect.option('family', item.value, item.label, { radio: true })
      })

      _(fontSizeOptions).each(function (item, index) {
        if (index === 1) fontSizeSelect.divider()
        fontSizeSelect.option('size', item.value, item.label, { radio: true })
      })

      model.on('change', function () {
        settings.set('defaultFontStyle', model.toJSON()).save()
        exampleText.css(getExampleTextCSS())
      })

      _(fontFamilySelect.$ul.find('a')).each(function (item, index) {
        // index 0 is browser default
        if (index === 0) return
        $(item).css('font-family', $(item).data('value'))
      })

      _(defaultStyleSection).each(function (obj) {
        obj.toggle(settings.get('messageFormat') !== 'text')
      })

      settings.on('change:messageFormat', function (value) {
        _(defaultStyleSection).each(function (obj) {
          obj.toggle(value !== 'text')
        })
      })

      const colorPicker = new Colorpicker({ name: 'color', model, className: 'dropdown', label: gt('Color'), caret: true })
      this.append(
        util.fieldset(st.DEFAULT_STYLE,
          util.explanation(
            gt('Customize the font, size and color styles of emails you compose.')
          ),
          $('<div class="my-8">').append(
            fontFamilySelect.render().$el,
            fontSizeSelect.render().$el,
            $('<div class="fontcolorButton">').append(
              colorPicker.render().$el
            )
          ),
          exampleText = $('<div class="example-text">')
            .text(gt('This is how your message text will look like.'))
            .css(getExampleTextCSS())
        ).addClass('default-text-style')
      )

      const views = [fontFamilySelect, fontSizeSelect, colorPicker]
      settings.on('change:messageFormat', toggleDropdownStates.bind(undefined, views))
      toggleDropdownStates(views)
    }
  },
  {
    id: 'undo-send',
    index: 300,
    render () {
      if (!isConfigurable.UNDO_SEND) return
      const list = [
        { label: gt('Without delay'), value: '0' },
        { label: gt('5 seconds'), value: '5' },
        { label: gt('10 seconds'), value: '10' }
      ]
      this.append(
        util.fieldset(
          st.UNDO_SEND,
          util.explanation(gt('Sending email can be delayed so that you can cancel sending if it happens by mistake or you forgot something.')),
          new mini.CustomRadioView({ name: 'undoSendDelay', model: settings, list }).render().$el
        ).attr('id', 'undoSendDelay').addClass('last')
      )
    }
  }
)

const moduleReady = mailfilter.getConfig()
const isActionInConfig = (config, action) => config.actioncmds.map(i => i.id).includes(action)

async function fetchAccounts () {
  /**
   * TODO: only the default account (id: 0) can have multiple aliases for now
   * all other accounts can only have one address (the primary address)
   * So the option is only for the default account, for now. This should
   * be changed in the future. If more (e.g. external) addresses are shown
   * here, server _will_ respond with an error, when these are selected.
   *
   * THIS COMMENT IS IMPORTANT, DON’T REMOVE
   */
  return api.getSenderAddresses(0).then(function (addresses) {
    return _(addresses).map(function (address) {
      // use value also as label
      return { value: address[1], label: address[1] }
    })
  })
}

function hideAliasSection (node) {
  node.closest('.settings-section').addClass('hidden')
  setConfigurable({
    ALIASES: false,
    SENDER_DEFAULT: false
  })
  index.resetCache()
}

ext.point('io.ox/mail/settings/aliases').extend(
  {
    id: 'sender',
    index: 100,
    async render () {
      if (capabilities.has('guest')) return hideAliasSection(this)

      const $fieldset = $('<fieldset>')

      this.append($fieldset)

      const senderOptions = await fetchAccounts()
      $fieldset.append(
        $('<legend class="sectiontitle">').append(
          $('<h3>').text(st.SENDER_DEFAULT),
          util.compactSelect('defaultSendAddress', '', settings, senderOptions)
            .find('label').addClass('sr-only').end()
            .addClass('mt-8')
        )
      )
      // toggle hidden state of whole section
      if (senderOptions.length > 1) return

      // hide whole section
      hideAliasSection(this)
    }
  }
)

ext.point('io.ox/mail/settings/rules/buttons').extend(
  {
    id: 'vacation-notice',
    index: 100,
    async render () {
      const config = await moduleReady
      if (!capabilities.has('mailfilter_v2') || !isActionInConfig(config, 'vacation')) return

      const toggle = createIcon('bi/toggle-on.svg').addClass('me-4 mini-toggle')
      this.append(
        $('<button type="button" class="btn btn-default me-16" data-action="edit-vacation-notice">')
          .append(
            toggle.hide(),
            $.txt(st.VACATION_NOTICE + ' \u2026')
          )
          .on('click', openDialog)
      )

      // check whether it's active
      const model = new VacationNoticeModel()
      model.fetch().done(updateToggle.bind(this, model))
      ox.on('mail:change:vacation-notice', updateToggle.bind(this))

      function updateToggle (model) {
        toggle.toggle(model.get('active'))
      }

      function openDialog () {
        ox.load(() => import('@/io.ox/mail/mailfilter/vacationnotice/view')).then(function ({ default: view }) {
          view.open()
        })
      }
    }
  },
  {
    id: 'auto-forward',
    index: 200,
    async render () {
      const config = await moduleReady
      if (!capabilities.has('mailfilter_v2') || !isActionInConfig(config, 'redirect')) return

      const toggle = createIcon('bi/toggle-on.svg').addClass('me-4 mini-toggle')
      this.append(
        $('<button type="button" class="btn btn-default me-16" data-action="edit-auto-forward">')
          .append(
            toggle.hide(),
            $.txt(st.AUTO_FORWARD + ' \u2026')
          )
          .on('click', openDialog)
      )

      // check whether it's active
      const model = new AutoforwardModel()
      model.fetch().done(updateToggle.bind(this, model))
      ox.on('mail:change:auto-forward', updateToggle.bind(this))

      function updateToggle (model) {
        toggle.toggle(model.isActive())
      }

      function openDialog () {
        ox.load(() => import('@/io.ox/mail/mailfilter/autoforward/view')).then(function ({ default: view }) {
          view.open()
        })
      }
    }
  }
)

ext.point('io.ox/mail/settings/advanced').extend(
  {
    id: 'behavior',
    index: 100,
    render () {
      if (capabilities.has('guest')) return

      this.append(
        $('<fieldset>').append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(gt('Behavior'))
          ),
          util.checkbox('sendDispositionNotification', st.READ_RECEIPTS, settings),
          util.checkbox('removeDeletedPermanently', st.REMOVE_PERMANENTLY, settings),
          util.checkbox('autoSelectNewestSeenMessage', st.AUTO_SELECT_NEWEST, settings),
          // mailto handler registration
          util.checkbox('features/registerProtocolHandler', st.MAILTO_HANDLER, settings)
            .find('label').css('margin-right', '8px').end()
            .append(
            // if supported add register now link
              navigator.registerProtocolHandler
                ? $('<a href="#" role="button">').text(gt('Register now')).on('click', function (e) {
                  e.preventDefault()
                  const l = location; const $l = l.href.indexOf('#'); const url = l.href.substr(0, $l)
                  navigator.registerProtocolHandler(
                    'mailto', url + '#app=io.ox/mail&mailto=%s', ox.serverConfig.productNameMail
                  )
                })
                : []
            )
        )
      )
    }
  },
  {
    id: 'allow-html-messages',
    index: 200,
    render () {
      if (capabilities.has('guest')) return

      this.append(
        $('<fieldset>').append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(gt('Reading'))
          ),
          util.checkbox('allowHtmlMessages', st.DISPLAY_HTML, settings)
        )
      )
    }
  },
  {
    id: 'address-collection',
    index: 300,
    render () {
      if (capabilities.has('guest')) return

      this.append(
        // Address collection
        $('<fieldset>').toggleClass('hidden', !capabilities.has('collect_email_addresses')).append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(gt('Automatic email address collection'))
          ),
          util.explanation(gt('Automatically creates a "Collected addresses" folder for email addresses you interact with.')),
          util.checkbox('contactCollectOnMailTransport', st.COLLECT_ON_SEND, settings),
          util.checkbox('contactCollectOnMailAccess', st.COLLECT_ON_READ, settings)
        )
      )
    }
  },
  {
    id: 'forwarding',
    index: 400,
    render () {
      if (capabilities.has('guest')) return

      this.append(
        $('<fieldset>').append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(st.FORWARD_AS)
          ),
          $('<div role="group" class="form-group mb-24" aria-labelledby="forwardMessageAsDescription">').append(
            new mini.CustomRadioView({
              list: [
                { label: gt('Inline'), value: 'Inline' },
                { label: gt('Attachment'), value: 'Attachment' }
              ],
              name: 'forwardMessageAs',
              model: settings
            }).render().$el
          )
        )
      )
    }
  },
  {
    id: 'compose-and-reply',
    index: 500,
    render () {
      if (capabilities.has('guest')) return

      this.append(
        $('<fieldset>').append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(gt('Compose & reply'))
          ),
          util.checkbox('appendMailTextOnReply', st.APPEND_TEXT_ON_REPLY, settings),
          util.checkbox('confirmReplyToMailingLists', st.ASK_BEFORE_MAILING_LIST, settings),
          util.checkbox('appendVcard', st.ATTACH_VCARD, settings)
        )
      )
    }
  },
  {
    id: 'auto-bcc',
    index: 600,
    render () {
      if (capabilities.has('guest')) return

      this.append(
        $('<fieldset>').append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(st.AUTO_BCC)
          ),
          util.explanation(gt('The given address is always added to BCC when replying or composing new emails.')),
          $('<div class="form-group row mb-24">').append(
            $('<div class="col-md-9 col-xs-12">').append(
              $('<label for="autobcc">').text(gt('Recipient')),
              new mini.InputView({ name: 'autobcc', model: settings, className: 'form-control', id: 'autobcc' }).render().$el
            )
          )
        )
      )
    }
  }
)

ext.point('io.ox/mail/settings/signatures').extend({
  id: 'view',
  index: 100,
  render () {
    this.append(
      new ExtensibleView({ point: 'io.ox/mail/settings/signatures/detail/view', model: settings })
        .render().$el.addClass('io-ox-signature-settings')

    )
  }
})

ext.point('io.ox/mail/settings/templates').extend({
  id: 'view',
  index: 100,
  render (baton) {
    this.parent().one('open', () => {
      import('@/io.ox/mail/settings/templates/settings/pane.js').then(() => {
        ext.point('io.ox/mail/settings/templates/view').invoke('draw', this, baton)
      })
    })
  }
})

const MessageListPreview = DisposableView.extend({
  className: 'flex-row border border-bright rounded p-16 text-xs',
  initialize () {
    this.listenTo(this.model, 'change:showTextPreview change:showCategories change:exactDates change:alwaysShowSize change:listViewLayout', this.render)
  },
  render () {
    const now = moment()
    const layout = this.model.get('listViewLayout')
    const senderName = gt('Name Lastname')
    this.$el.empty().append(
      // avatar or checkbox
      $('<div class="flex-col">').append(
        layout === 'avatars' && !_.device('smartphone')
          ? $('<div class="avatar initials me-8" style="width: 32px; height: 32px">').text(contactsUtil.getInitials({ display_name: senderName }))
          : $(),
        layout === 'checkboxes' && !_.device('smartphone')
          ? $('<div class="text-gray me-8 pt-8">').append(createIcon('bi/square.svg').addClass('bi-14'))
          : $()
      ),
      // content
      $('<div class="flex-col flex-grow zero-min-width">').append(
        // row 1
        $('<div class="flex-row">').append(
          // sender
          $('<div class="flex-grow text-bold truncate">').text(senderName),
          // date
          $('<div class="text-gray ms-8">').text(
            this.model.get('exactDates')
              ? now.format('l') + ' ' + now.format('LT')
              : now.format('LT')
          )
        ),
        // row 2
        $('<div class="flex-row">').append(
          // subject
          $('<div class="flex-grow truncate">').text(gt('Email subject')),
          // size
          this.model.get('alwaysShowSize')
            ? $('<div class="text-gray ms-8">').text('1.2 MB')
            : $()
        ),
        // row 3
        isConfigurable.SHOW_TEXT_PREVIEW && this.model.get('showTextPreview')
          ? $('<div class="flex-row">').append(
            // text preview (no translation here!)
            $('<div class="text-gray truncate">')
              .addClass(isConfigurable.SHOW_CATEGORIES && this.model.get('showCategories') ? '' : 'multiline')
              .text(gt('This is the email preview. You can also turn it off.'))
          )
          : $(),
        // row 4
        isConfigurable.SHOW_CATEGORIES && this.model.get('showCategories')
          ? $('<div class="flex-row">').append(
            $('<ul class="categories-badges list-unstyled flex-wrap flex-nowrap mb-0">').append(
              $('<li class="zero-min-width">').append(
                $('<span class="category-view ellipsis" style="background-color: rgb(255, 204, 219); border-color: rgb(255, 204, 219); color: rgb(133, 71, 90);">').append(
                  // #. This is part of an email preview with activated categories (option: show categories)
                  $('<span class="ellipsis">').text(gt('some category'))
                )
              )
            )
          )
          : $()
      )
    )
    return this
  }
})

const ReadingPanePreview = DisposableView.extend({
  id: 'reading-pane-preview',
  className: 'border border-bright rounded p-16',
  initialize () {
    this.listenTo(this.model, 'change:isColorQuoted change:useFixedWidthFont change:fontSize', this.render)
  },
  render () {
    const useColor = this.model.get('isColorQuoted')
    const fontSizeClass = _.device('smartphone') ? '' : this.model.get('fontSize', 'text-very-small')
    this.$el.empty().toggleClass('font-mono', this.model.get('useFixedWidthFont')).append(
      // cSpell:disable
      $('<p class="m-0">').addClass(fontSizeClass).text('Lorem ipsum dolor sit amet').append(
        $('<blockquote class="text-xs text-gray m-0 p-0 pl-8 border-left">').addClass(fontSizeClass).toggleClass('text', useColor).text('sed diam nonumy eirmod tempor').append(
          $('<blockquote class="text-xs m-0 p-0 pl-8 border-left">').addClass(fontSizeClass).css(useColor ? { borderColor: '#283f73', color: '#283f73' } : {}).text('sed diam voluptua. At vero eos et').append(
            $('<blockquote class="text-xs m-0 p-0 pl-8 border-left">').addClass(fontSizeClass).css(useColor ? { borderColor: '#C2185B', color: '#C2185B' } : {}).text('accusam et justo duo dolores')
          )
        )
      )
      // cSpell:enable
    )
    return this
  }
})

export default moduleReady
