/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import ext from '@/io.ox/core/extensions'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import { buttonWithIcon, createButton, createIcon } from '@/io.ox/core/components'
import gt from 'gettext'
import * as extensions from '@/io.ox/mail/view-options-extensions'

// rename extension point
ext.point('io.ox/mail/list-view/actions').extend({
  id: 'dropdown',
  index: 1000,
  draw () {
    const { listView } = this
    const sortTitle = gt('Sort options')
    const sortDropdown = new Dropdown({
      $toggle: buttonWithIcon({
        className: 'btn btn-link',
        icon: createIcon('bi/sort-down.svg').addClass('bi-18'),
        title: sortTitle,
        ariaLabel: sortTitle
      }),
      dataAction: 'sort',
      model: this.props,
      title: sortTitle
    })
    ext.point('io.ox/mail/view-options').invoke('draw', sortDropdown.$el, ext.Baton({ app: this }))

    const $selectButton = createButton({
      text: gt('Select'),
      className: 'btn btn-unstyled btn-link edit'
    })

    listView.on('collection:load collection:reload', () => {
      $selectButton.toggleClass('hidden', !listView.collection.length)
    })

    this.pages.getAppHeader('listView').setButtons(
      $('<div class="flex items-center">').append(
        sortDropdown.render().$el,
        $selectButton
      )
    )
  }
})

ext.point('io.ox/mail/list-view/toolbar/bottom').extend({
  id: 'status',
  index: 100,
  draw: extensions.updateStatus
}, {
  id: 'folder-actions',
  index: 200,
  draw (baton) {
    const $toggle = buttonWithIcon({
      className: 'btn btn-toolbar',
      icon: createIcon('bi/three-dots.svg'),
      title: gt('Email actions'),
      ariaLabel: gt('Email actions')
    })

    const actions = extensions.getActions(baton.app)

    const actionDropdown = new Dropdown({
      tagName: 'li',
      attributes: { role: 'presentation' },
      className: 'dropdown folder-actions p-8',
      title: gt('Actions'),
      $toggle
    })
    extensions.folderActions.call(actionDropdown.render().$el, ext.Baton({ actions, ...baton }))
    this.append(actionDropdown.$el)

    function updateStatusBar () {
      const actionsEmpty = !extensions.getActions(baton.app).length
      this.find('.status-bar').toggleClass('empty-actions', actionsEmpty)
      actionDropdown.$el.toggle(!actionsEmpty)
    }

    updateStatusBar.call(this)
    baton.app.listView.on('collection:loading', updateStatusBar.bind(this))
  },
  redraw (baton) {
    const actions = extensions.getActions(baton.app)
    if (!actions.length) return this.$el.hide()

    this.$el.data('view', this)
    extensions.folderActions.call(this.$el, ext.Baton({ actions, ...baton }))
    ext.point('io.ox/core/dropdown').invoke('redraw', this, ext.Baton({ dropdownTitle: gt('Actions') }))
    this.$el.show()
  }
})

ext.point('io.ox/mail/view-options').extend({
  id: 'sort',
  index: 200,
  draw: extensions.sortOptions
}, {
  id: 'order',
  index: 300,
  draw: extensions.sortOrder
}, {
  id: 'thread',
  index: 400,
  draw: extensions.groupThreads
})
