/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import $ from '@/jquery'
import moment from '@/moment'
import ext from '@/io.ox/core/extensions'
import SearchView, { appendSearchView, parseDate } from '@/io.ox/backbone/views/search'
import CollectionLoader from '@/io.ox/core/api/collection-loader'
import api from '@/io.ox/mail/api'
import folderAPI from '@/io.ox/core/folder/api'
import http from '@/io.ox/core/http'
import { hasFeature } from '@/io.ox/core/feature'
import { isMiddlewareMinVersion } from '@/io.ox/core/util'
import gt from 'gettext'
import { addReadyListener } from '@/io.ox/core/events'
import { settings as mailSettings } from '@/io.ox/mail/settings'
import { settings as contactsSettings } from '@/io.ox/contacts/settings'
import { categoriesCollection, userFlagPrefix } from '@/io.ox/core/categories/api'

const folders = {}
const contactsQueryMinLength = Math.max(1, contactsSettings.get('search/minimumQueryLength', 2))

if (api.allMessagesFolder) folders.all = gt('All folders')
if (api.allUnseenMessagesFolder) folders.unseen = gt('All unseen messages')
folders.inbox = gt('Inbox')
folders.current = gt('Current folder')

const i18n = {
  // #. search terms must be one word (without spaces), lowercase, and must be unique per app
  attachments: gt.pgettext('search', 'attachments'),
  // #. search terms must be one word (without spaces), lowercase, and must be unique per app
  flagged: gt.pgettext('search', 'flagged'),
  // #. search terms must be one word (without spaces), lowercase, and must be unique per app
  file: gt.pgettext('search', 'file'),
  // #. search terms must be one word (without spaces), lowercase, and must be unique per app
  year: gt.pgettext('search', 'year'),
  // #. "Search in" like "Search in folder"; appears as label above folder selection
  searchIn: gt.pgettext('search', 'Search in')
}

const searchIn = {
  current: gt('Current folder'),
  all: gt('All folders'),
  inbox: gt('Inbox')
}

const filters = [
  // id, label, prefix (fallback is label lowercase), type (optional), unique, visible
  ['to', gt('To'), '', 'string', false],
  ['from', gt('From'), '', 'string', true],
  ['subject', gt('Subject'), '', 'string', true],
  ['content', gt('Content', '', 'string', true)],
  ['attachment', gt('Has attachments'), i18n.attachments, 'checkbox', true],
  ['flagged', gt('Has color flag'), i18n.flagged, 'checkbox', true],
  ['year', gt('Year'), i18n.year, 'string', true],
  ['folder', i18n.searchIn, '', 'list', true, true, searchIn]
]

let supportsAttachmentSearch = false
let defaultFields = []
let separateContentSeach = false

addReadyListener('rampup', async () => {
  // use inbox to check whether mail server supports FILENAME_SEARCH aka SEARCH=X-MIMEPART
  const inbox = await folderAPI.get('default0/INBOX')
  supportsAttachmentSearch =
    inbox?.supported_capabilities?.indexOf('FILENAME_SEARCH') > -1 ||
    mailSettings.get('search/supports/mimepart', false)
  if (supportsAttachmentSearch) filters.push(['file', gt('Attached file'), i18n.file, 'string', true])
  defaultFields = String(mailSettings.get('search/default/fields', 'from,to,cc,bcc,subject,content')).split(',')
  separateContentSeach = defaultFields.indexOf('content') === -1
})

function getDayString (value) {
  // Note: mailserver truncates the time part and does not respecting users timezone
  const date = moment(value).startOf('day')
  const offset = date._offset
  return String(date.add(offset, 'minutes').valueOf())
}

ext.point('io.ox/mail/mediator').extend({
  id: 'top-search',
  index: 10000,
  setup (app) {
    const listView = app.listView
    const collectionLoader = new CollectionLoader({
      module: 'mail',
      mode: 'search',
      getQueryParams (params) {
        const filters = []
        const criteria = params.criteria
        const { from, to, subject, year, content } = criteria
        if (from) filters.push(['=', { field: 'from' }, from])
        if (to) {
          const words = to
            .split(/("[^"]+"|\S+)/)
            .map(word => word.replace(/^"(.*)"$/, '$1').trim())
            .filter(Boolean)
          words.forEach(word => {
            filters.push(['or'].concat(['to', 'cc', 'bcc'].map(field => ['=', { field }, word])))
          })
        }
        if (subject) filters.push(['=', { field: 'subject' }, subject])
        if (content) filters.push(['=', { field: 'content' }, content])
        if (year) {
          const start = Date.UTC(year, 0, 1)
          const end = Date.UTC(year, 11, 31)
          filters.push(['and', ['>', { field: 'received_date' }, String(start)], ['<', { field: 'received_date' }, String(end)]])
        }
        if (criteria.words) {
          const includeAttachments = supportsAttachmentSearch && isMiddlewareMinVersion(8, 7) && mailSettings.get('search/default/includeAttachments', true)
          const words = criteria.words
            .split(/("[^"]+"|\S+)/)
            .map(word => word.replace(/^"(.*)"$/, '$1').trim())
            .filter(Boolean)
          words.forEach(word => {
            const fields = ['or'].concat(defaultFields.map(field => ['=', { field }, word]))
            if (includeAttachments) {
              // support for 'text' so that we can search for attachments in one go
              // has been added with v8.7 (https://jira.open-xchange.com/browse/OXUIB-2025)
              fields.push(['=', { attachment: 'name' }, word])
              filters.push(['and', ['=', { field: 'text' }, word], fields])
            } else {
              filters.push(fields)
            }
          })
        }
        if (criteria.file && supportsAttachmentSearch) filters.push(['=', { attachment: 'name' }, criteria.file])
        if (criteria.attachment === 'true') filters.push(['=', { field: 'content_type' }, 'multipart/mixed'])
        if (criteria.after) filters.push(['>', { field: 'received_date' }, getDayString(criteria.after)])
        if (criteria.before) filters.push(['<', { field: 'received_date' }, getDayString(criteria.before)])
        if (criteria.flagged) filters.push(getUserFlags())
        if (criteria.user_flags) filters.push(getCategoryFlags(criteria.user_flags))
        // 32 = seen, so not 32
        if (isUnseen(criteria.folder)) filters.push(['not', ['=', { field: 'flags' }, 32]])

        function isUnseen (folder) {
          if (folder === 'unseen') return true
          return folder === 'current' && app.folder.get() === 'virtual/all-unseen'
        }

        function getFolder (folder) {
          const type = isUnseen(folder) ? 'unseen' : folder
          switch (type) {
            case 'unseen':
            case 'all': return api.allMessagesFolder
            case 'inbox': return 'default0/INBOX'
            // case 'trash':
            // case 'sent': return accountAPI.getFoldersByType(folder)[0] || app.folder.get()
            default: return app.folder.get()
          }
        }

        function getUserFlags () {
          return ['or', ['=', { field: 'flags' }, 8]]
            .concat(Array(10).fill(1).map((c, i) => ['=', { field: 'user_flags' }, '$cl_' + (i + 1)]))
        }

        function getCategoryFlags (criteria) {
          const categoryFilters = []
          const reMailCategoryPrefix = new RegExp(`^"?\\${userFlagPrefix}_`);
          // support multiple categories. Pattern is '"category1" "category2"'. Also support simple 'category1' pattern
          (criteria.match(/".*?"/g) || [criteria]).forEach(category => {
            // get rid of leading and ending "
            category = category.replaceAll('"', '')
            if (!reMailCategoryPrefix.test(category)) {
              const model = categoriesCollection.findWhere({ name: category })
              category = model ? model.get('id') : category
            }
            categoryFilters.push(['=', { field: 'user_flags' }, category])
          })
          return ['and'].concat(categoryFilters)
        }

        return {
          action: 'search',
          folder: getFolder(criteria.folder),
          columns: http.defaultColumns.mail.search,
          sort: params.sort || '661',
          order: params.order || 'desc',
          timezone: 'utc',
          data: { filter: filters.length === 1 ? filters[0] : ['and'].concat(filters) }
        }
      },
      fetch (obj) {
        return http.wait().then(function () {
          const { data, ...params } = obj
          return http.PUT({
            module: 'mail',
            params,
            data
          })
        })
      },
      each (obj) {
        api.pool.add('detail', obj)
      },
      // Reduce performance impact, see (https://jira.open-xchange.com/browse/DOP-2955)
      PRIMARY_PAGE_SIZE: 20,
      SECONDARY_PAGE_SIZE: 100
    })

    let previousCollectionLoader
    let previousAttributes = {}
    const view = new SearchView({
      app,
      placeholder: gt('Search email'),
      point: 'io.ox/mail/search/dropdown',
      defaults: {
        folder: api.allMessagesFolder ? 'all' : 'current',
        addresses: '',
        after: '',
        attachment: false,
        before: '',
        file: '',
        flagged: false,
        from: '',
        subject: '',
        to: '',
        words: ''
      },
      filters,
      autoSuggest ({ prefix, word, unquoted }) {
        if (prefix || word.length < contactsQueryMinLength) return []
        const list = []
        // use the unquoted version if there is one (also helps to remove strange half open quotes with a missing quote at the end)
        word = unquoted || word
        if (/^((19|20)\d\d)$/.test(word)) {
          // year
          list.push({ filter: 'year', value: word })
        } else if (/^(\d{1,2}\.\d{1,2}\.\d{2,4}|\d{1,2}\/\d{1,2}\/\d{2,4}|\d{4}-\d{1,2}-\d{1,2})$/.test(word)) {
          // dates
          const date = parseDate(word)
          if (date.isValid()) list.push({ filter: 'after', value: word }, { filter: 'before', value: word })
        } else {
          list.push({ filter: 'from', value: word }, { filter: 'to', value: word })
        }
        list.push({ filter: 'subject', value: word })
        if (separateContentSeach) list.push({ filter: 'content', value: word })
        list.push({ filter: 'file', value: word })
        // flags
        if (i18n.attachments.startsWith(word)) {
          list.push({ filter: 'attachments', value: gt.pgettext('search', 'yes') })
        }
        if (i18n.flagged.startsWith(word)) {
          list.push({ filter: 'flagged', value: gt.pgettext('search', 'yes') })
        }
        return list
      }
    })
      .on('before:show', () => $('#io-ox-topsearch .search-container.search-view').hide())
      .on('search', async function (criteria) {
        if (_.device('smartphone')) await app.pages.changePage('listView')
        if (!previousCollectionLoader) {
          previousCollectionLoader = listView.loader
          previousAttributes = listView.model.pick('thread', 'sort', 'order')
        }
        listView.connect(collectionLoader)
        listView.model.set({ criteria, thread: false, sort: 661, order: 'desc' })
      })
      .on('cancel', function () {
        listView.connect(previousCollectionLoader)
        listView.model.set(previousAttributes, { silent: true }).unset('criteria')
        previousCollectionLoader = null
      })

    appendSearchView({ app, view })
  }
})

ext.point('io.ox/mail/search/dropdown').extend({
  id: 'default',
  index: 100,
  render () {
    this.$dropdown.append(
      this.select('folder', gt('Search in'), SearchView.convertOptions(folders)),
      this.address('from', gt('From')),
      this.address('to', gt('To')),
      this.input('subject', gt('Subject')),
      separateContentSeach
        // #. Context: mail search. Label for <input>.
        ? this.input('content', gt('Content'))
        // #. Context: mail search. Label for <input>.
        : this.input('words', gt('Contains words')),
      supportsAttachmentSearch
        // #. Context: mail search. Label for <input>.
        ? this.input('file', gt('Attachment name'))
        : $(),
      this.dateRange({ mandatoryAfter: false, mandatoryBefore: false }),
      hasFeature('categories') && this.categoryInput('user_flags', gt('Categories'), {
        mapping: value => {
          const reMailCategoryPrefix = new RegExp(`^"?\\${userFlagPrefix}_`)
          if (reMailCategoryPrefix.test(value)) {
            const model = categoriesCollection.findWhere({ id: value }) || categoriesCollection.findWhere({ id: value.replaceAll('"', '') })
            value = model ? model.get('name') : value
          }
          return value
        },
        flags: ['no-split']
      }),
      // #. Context: mail search. Label for checkbox.
      this.checkbox('attachment', gt('Has attachments')),
      this.checkbox('flagged', mailSettings.flagByColor ? gt('Has color flag') : gt('Flagged messages')),
      this.button()
    )
  }
})
