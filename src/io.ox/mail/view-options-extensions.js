/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import ext from '@/io.ox/core/extensions'
import account from '@/io.ox/core/api/account'
import folderAPI from '@/io.ox/core/folder/api'
import contextmenu from '@/io.ox/core/folder/contextmenu'
import { settings } from '@/io.ox/mail/settings'
import moment from '@/moment'
import api from '@/io.ox/mail/api'

import gt from 'gettext'

export function getActions (app) {
  // no folder actions available for unseen and flagged messages folder or
  // while doing a search or using categories or empty folder
  if (app.folder.get() === 'virtual/all-unseen') return []
  if (app.folder.get() === api.allFlaggedMessagesFolder) return []
  if (app.props.get('searching')) return []
  if (app.props.get('categories')) return []
  if (!app.folder.getModel().get('total')) return []

  return ['headerAll', 'markFolderSeen', 'moveAllMessages', 'archive', 'empty']
}

export function folderActions (baton) {
  const view = this.data('view')
  const app = baton.app
  const actions = baton.actions
  const extensions = contextmenu.extensions

  app.folder.getData().done(function (data) {
    const baton = new ext.Baton({ data, module: 'mail', listView: app.listView })
    actions.forEach(id => extensions[id].call(view.$ul, baton))
  })

  if (!view.alreadyListening) {
    view.listenTo(app, 'folder:change', redraw)
    view.listenTo(app.props, 'change:categories change:searching', redraw)
    view.alreadyListening = true
  }

  function redraw () {
    // we need to postpone this because folder:change happens before model updates
    setTimeout(() => {
      view.$ul.empty()
      if (_.device('smartphone')) {
        return ext.point('io.ox/mail/list-view/toolbar/bottom')
          .invoke('redraw', view, baton)
      }
      ext.point('io.ox/mail/view-options').invoke('draw', view.$el, baton)
    }, 0)
  }
}

export function sortOptions (baton) {
  if (baton.app.props.get('searching')) return
  const view = this.data('view')
  const folder = baton.app.folder.get()
  if (folder === 'virtual/all-unseen') return

  view
    .divider()
    .header(gt('Sort by'))
    .option('sort', 661, gt('Date'), { radio: true })
    .option('sort', 'from-to', account.is('sent|drafts', folder) ? gt('To') : gt('From'), { radio: true })
    .option('sort', 651, gt('Unread'), { radio: true })
    .option('sort', 608, gt('Size'), { radio: true })
    .option('sort', 607, gt('Subject'), { radio: true })

  // #. Sort by messages that have attachments
  if (folderAPI.pool.getModel(folder).supports('ATTACHMENT_MARKER')) view.option('sort', 602, gt('Attachments'), { radio: true })
  // color flags
  if (settings.flagByColor) this.data('view').option('sort', 102, gt('Color'), { radio: true })
  // sort by /flagged messages, internal naming is "star"
  // #. Sort by messages which are flagged, "Flag" is used in dropdown
  if (settings.flagByStar) this.data('view').option('sort', 660, gt('Flag'), { radio: true })
  const defaultFolders = settings.get('folder', {})
  if (folder === defaultFolders.scheduled) this.data('view').option('sort', 669, gt('Scheduled date'), { radio: true })
}

export function sortOrder (baton) {
  if (baton.app.props.get('searching')) return
  if (baton.app.folder.get() === 'virtual/all-unseen') return
  this.data('view')
    .divider()
    .header(gt('Sort order'))
    .option('order', 'asc', gt('Ascending'), { radio: true })
    .option('order', 'desc', gt('Descending'), { radio: true })
}

export function groupThreads (baton) {
  if (baton.app.props.get('searching')) return
  if (baton.app.folder.get() === 'virtual/all-unseen') return
  // don't add if thread view is disabled server-side
  if (baton.app.settings.get('threadSupport', true) === false) return
  // no thread support in drafts/sent folders. This breaks caching (Sent folders get incomplete threads). See OXUIB-853
  if (account.is('sent|drafts', baton.app.folder.get())) return
  this.data('view')
    .divider()
    .header(gt('Group by'))
    .option('thread', true, gt('Conversations'))
}

export function updateStatus (baton) {
  const $el = $('<li role="presentation" class="status-bar flex-center">')
  this.append($el)
  baton.app.listView.on('collection:loading', updateStatus)
  setInterval(updateStatus.bind(baton.app.listView), 10000)

  function updateStatus () {
    $el
      .text(getStatus(this.collection))
      .toggleClass('empty-actions', !getActions(baton.app).length)
  }

  function getStatus (collection) {
    // #. Present progressive, indicating an ongoing or current action.
    if (collection.loading) return gt('Loading…')
    const age = Math.ceil((_.now() - collection.timestamp) / 60000) || 0
    if (age < 2) return gt('Updated just now')
    if (age < 60) return gt('Updated %1$d minutes ago', age)
    // #. %1$d is a time
    return gt('Updated at %1$d', moment(collection.timestamp).format('LT'))
  }
}
