/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import mailAPI from '@/io.ox/mail/api'
import { userFlagPrefix } from '@/io.ox/core/categories/api'
import { CategoryDropdownMulti } from '@/io.ox/core/categories/view'
import gt from 'gettext'

export function onChange (list, data) {
  const changed = []
  const { add, remove } = data
  list.forEach(mail => {
    const changes = { add: [], remove: [] }
    mail.user = mail.user || []

    add.forEach(category => {
      if (!mail.user.includes(category)) changes.add.push(category)
    })

    remove.forEach(category => {
      if (mail.user.includes(category)) changes.remove.push(category)
    })

    // check
    if (changes.add.length || changes.remove.length) changed.push({ mail, changes })
  })

  if (changed.length === 0) return
  mailAPI.setCategories(changed)
}

export function appendDropdown ({ baton, el, $toggle }) {
  const list = [].concat(baton.data)

  const categoriesDropdownMulti = new CategoryDropdownMulti({
    el: el.addClass('dropdown'),
    $toggle,
    data: list,
    prefix: `${userFlagPrefix}_`,
    appPrefix: 'mail',
    caret: true,
    label: gt('Add category'),
    useToggleWidth: true
  }).render()

  categoriesDropdownMulti.on('categories:change', data => onChange(list, data))
}

export default { appendDropdown, onChange }
