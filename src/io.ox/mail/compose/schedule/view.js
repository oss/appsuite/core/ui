/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import moment from '@/moment'

import ext from '@/io.ox/core/extensions'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import ModalDialog from '@/io.ox/backbone/views/modal'
import Timepicker from '@/io.ox/backbone/views/timepicker'
import ScheduleModel from '@/io.ox/mail/compose/schedule/model'
import { DateView, ErrorView } from '@/io.ox/backbone/mini-views/common'
import { createButton } from '@/io.ox/core/components'
import { hasFeature } from '@/io.ox/core/feature'
import { settings as mailSettings } from '@/io.ox/mail/settings'

import gt from 'gettext'

const TimeDropdown = Dropdown.extend({
  onShow () {
    this.renderOptions()
    this.trigger('open')
  },
  renderOptions () {
    const { baton } = this.options
    this.$ul.empty()
    ext.point('io.ox/core/dropdown').invoke('redraw', this, baton)
    ext.point('io.ox/mail/compose/scheduled/dropdown').invoke('render', this, baton)
  },
  scheduleMailAt (date) {
    const { model, view } = this.options.baton
    // Do not use moment's toISOString as it adds milliseconds in Firefox, causing middleware errors
    model.set('dateToSend', `${date.clone().utc().format('YYYYMMDDTHHmmss')}Z`)
    view.send()
  }
})

ext.point('io.ox/mail/compose/header').extend({
  id: 'scheduled-send',
  index: 50,
  draw (baton) {
    if (!hasFeature('scheduleSend')) return
    if (_.device('smartphone')) return

    const { config } = baton
    const { buttonsView } = baton

    const timeDropdown = new TimeDropdown({
      label: gt('Schedule send'),
      $toggle: createButton({
        variant: 'primary',
        className: 'dropdown-toggle scheduled-send',
        icon: { name: 'bi/chevron-down.svg', className: 'bi-12', title: gt('Schedule email') },
        disabled: !config.get('scheduled_mail')
      }),
      baton,
      smart: true,
      dropup: true
    }).render()

    // initialized disabled
    config.on('change:scheduled_mail', () => {
      timeDropdown.$toggle.prop('disabled', !config.get('scheduled_mail'))
    })

    // use `send` and `timeDropdown` as split button and render again
    buttonsView.$save = $('<div class="btn-group" role="toolbar">').append(
      buttonsView.$save,
      timeDropdown.$el
    )
    buttonsView.render()
  }
})

ext.point('io.ox/mail/compose/menuoptions').extend({
  id: 'scheduled-send-mobile',
  index: 800,
  draw (baton) {
    if (!hasFeature('scheduleSend')) return
    if (!_.device('smartphone')) return

    const { config } = baton

    const optionsDropdown = this.data('view')
    optionsDropdown.link('scheduleSend', gt('Schedule send'), e => {
      if ($(e.target).hasClass('disabled')) return

      baton.dropdownTitle = gt('Schedule send')
      const timeDropdown = new TimeDropdown({ baton }).render()

      $('body').append(timeDropdown.$el)
      _.delay(() => { timeDropdown.open() }, 200)
      timeDropdown.$ul.on('click', () => timeDropdown.$el.remove())
    })

    const link = optionsDropdown.$ul.find('li:last-child > a')
    link.addClass('scheduled-send').toggleClass('disabled', !config.get('scheduled_mail'))

    // initialized disabled
    config.on('change:scheduled_mail', () => {
      link.toggleClass('disabled', !config.get('scheduled_mail'))
    })
  }
})

ext.point('io.ox/mail/compose/scheduled/dropdown').extend({
  id: 'header',
  index: 100,
  render () {
    if (_.device('smartphone')) return
    this.header(gt('Schedule send'))
  }
}, {
  id: 'last-date-time',
  index: 200,
  render () {
    const lastScheduledDate = mailSettings.get('lastScheduledDate')
    if (!lastScheduledDate) return
    const momentObj = moment(lastScheduledDate)
    if (momentObj.valueOf() < moment().valueOf()) {
      mailSettings.set('lastScheduledDate', undefined)
      return
    }
    const year = moment().year() === momentObj.year() ? '' : 'YYYY'
    this.link('lastPickedTime', gt('Last chosen date'), () => this.scheduleMailAt(momentObj), { detailText: momentObj.format(`Do MMM ${year}, LT`) })
  }
}, {
  id: 'tomorrow',
  index: 300,
  render () {
    const tomorrow = moment().hour() < 8
      ? {
          date: moment().hour(8).startOf('hour'),
          text: gt('Send this morning')
        }
      : {
          date: moment().hour(32).startOf('hour'),
          text: gt('Send tomorrow morning')
        }

    this.link('tomorrowMorning', tomorrow.text, () => this.scheduleMailAt(tomorrow.date), { detailText: tomorrow.date.format('Do MMM, LT') })
  }
}, {
  id: 'monday',
  index: 400,
  render () {
    const mondayMorning = moment().day(8).hour(8).startOf('hour')
    this.link('mondayMorning', gt('Send Monday morning'), () => this.scheduleMailAt(mondayMorning), { detailText: mondayMorning.format('Do MMM, LT') })
  }
}, {
  id: 'date-time-picker',
  index: 500,
  render (baton) {
    this.divider()
    this.link('pickTime', gt('Pick a date and time') + ' …', () => {
      const timeDropdown = this
      openDateTimePicker(baton, timeDropdown)
      ext.point('io.ox/core/dropdown').invoke('hide', this)
    })
  }
}
)

function openDateTimePicker (baton, timeDropdown) {
  return new ModalDialog({
    // #. Dialog header for choosing date and time
    title: gt('Schedule send'),
    backdrop: true,
    width: 420,
    autoClose: false,
    model: new ScheduleModel({
      date: baton.model.get('scheduledTime')?.date,
      time: baton.model.get('scheduledTime')?.time
    }),
    previousFocus: timeDropdown.$toggle
  })
    .build(function () {
      const model = this.model
      const dateID = _.uniqueId('dp_')
      const timeID = _.uniqueId('tp_')

      this.$el.addClass('schedule-modal')
      this.$body.append(
        $('<span>').text(gt('Choose a sending date and time for your email.')),
        $('<div class="picker-group">').append(
          $('<div>').addClass('picker').append(
            $('<label>')
              .text(gt('Date'))
              .addClass('control-label')
              .attr('for', dateID),
            new DateView({ model, name: 'date', id: dateID }).render().$el
          ),
          $('<div>').addClass('picker').append(
            $('<label>')
              .text(gt('Time'))
              .addClass('control-label')
              .attr('for', timeID),
            new Timepicker({ model, name: 'time', id: timeID }).render().$el
          ),
          new ErrorView({
            name: 'datetime',
            model,
            selector: '.picker-group',
            focusSelector: ''
          }).render().$el
        )
      )
    })
    .addCancelButton()
    .addButton({ label: gt('Schedule email'), action: 'schedule' })
    .on('schedule', function () {
      const model = this.model
      const pickerGroupNode = this.$('.picker-group')
      if (!model.isValid()) {
        return pickerGroupNode.trigger('invalid', gt('Date must be in the future.'))
      }
      pickerGroupNode.trigger('valid')
      mailSettings.set('lastScheduledDate', model.getTimestamp().valueOf()).save()
      timeDropdown.scheduleMailAt(model.getTimestamp())
      this.close()
    })
    .open()
}
