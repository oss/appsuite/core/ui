/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import * as extensions from '@/io.ox/mail/view-options-extensions'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import { createButton } from '@/io.ox/core/components'
import FolderInfoView from '@/io.ox/core/tk/folder-info'
import api from '@/io.ox/mail/api'
import folderAPI from '@/io.ox/core/folder/api'

import gt from 'gettext'

ext.point('io.ox/mail/list-view/toolbar/top').extend({
  id: 'all',
  index: 200,
  draw (baton) {
    this.addClass('items-center').append(
      new FolderInfoView({ app: baton.app }).render().$el
    )
  }
})

ext.point('io.ox/mail/list-view/toolbar/top').extend({
  id: 'dropdown',
  index: 1000,
  draw (baton) {
    const dropdown = new Dropdown({
      tagName: 'li',
      className: 'dropdown grid-options toolbar-item margin-left-auto',
      attributes: { role: 'presentation' },
      $toggle: createButton({ variant: 'btn btn-toolbar', icon: { name: 'bi/three-dots.svg', title: gt('More message options') } }),
      dataAction: 'sort',
      model: baton.app.props
    })
    ext.point('io.ox/mail/view-options').invoke('draw', dropdown.$el, baton)
    dropdown.render().$ul.addClass('dropdown-menu-right')
    this.append(dropdown.$el)
  }
})

ext.point('io.ox/mail/list-view/toolbar/bottom').extend({
  id: 'status',
  index: 100,
  draw: extensions.updateStatus
})

ext.point('io.ox/mail/view-options').extend({
  id: 'all',
  index: 100,
  draw (baton) {
    // show a generic select all action for all-unseen, search results and when using categories
    const isUnseenFolder = baton.app.folder.get() === 'virtual/all-unseen'
    const isFlaggedFolder = baton.app.folder.get() === api.allFlaggedMessagesFolder
    const isActiveSearch = baton.app.props.get('searching')
    const isUsingCategories = baton.app.props.get('categories') && !folderAPI.is('trash', baton.app.folder.getModel().toJSON())

    const actions = isUnseenFolder || isFlaggedFolder || isActiveSearch || isUsingCategories
      ? ['selectAll']
      : ['headerAll', 'selectAll', 'markFolderSeen', 'moveAllMessages', 'archive', 'empty']
    extensions.folderActions.call(this, (ext.Baton({ ...baton, actions })))
  }
}, {
  id: 'sort',
  index: 200,
  draw: extensions.sortOptions
}, {
  id: 'order',
  index: 300,
  draw: extensions.sortOrder
}, {
  id: 'thread',
  index: 400,
  draw: extensions.groupThreads
})
