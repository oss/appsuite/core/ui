/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import DetailView from '@/io.ox/mail/detail/view'
import extensions from '@/io.ox/mail/common-extensions'
import ext from '@/io.ox/core/extensions'
import { hasFeature } from '@/io.ox/core/feature'
import * as util from '@/io.ox/mail/util'
import flagPicker from '@/io.ox/core/tk/flag-picker'
import '@/io.ox/mail/style.scss'

import gt from 'gettext'
import { settings } from '@/io.ox/mail/settings'
import { createButton, createIcon } from '@/io.ox/core/components'

ext.point('io.ox/mail/mobile/detail').extend({
  id: 'flagged-class',
  index: 100,
  draw: extensions.flaggedClass
})

ext.point('io.ox/mail/mobile/detail').extend({
  id: 'header',
  index: 200,
  draw (baton) {
    const $header = $('<header class="mobile-detail-view-mail detail-view-header flex">').append(
      $('<div class="left-side flex">'),
      $('<div class="right-side flex">')
    )
    ext.point('io.ox/mail/mobile/detail/header/indicators').invoke('draw', $header, baton)
    ext.point('io.ox/mail/mobile/detail/header').invoke('draw', $header, baton)
    this.append($header)
  }
})

ext.point('io.ox/mail/mobile/detail/header/categories-picker').extend({
  index: 100,
  draw (baton) {
    if (!hasFeature('categories')) return
    const button = createButton({ type: 'button', variant: 'toolbar' })
      .attr({ 'aria-label': gt('Set categories') })
      .append(createIcon('bi/tags.svg').addClass('bi-18'))
    this.append(button)
    import('@/io.ox/mail/actions/categories').then(({ appendDropdown }) => appendDropdown({ baton, el: this, $toggle: button }))
  }
})

ext.point('io.ox/mail/mobile/detail').extend({
  id: 'notifications',
  index: 300,
  draw (baton) {
    const section = $('<section class="notifications">')
    ext.point('io.ox/mail/detail/notifications').invoke('draw', section, baton)
    this.append(section)
  }
})

ext.point('io.ox/mail/mobile/detail').extend({
  id: 'attachments',
  index: 400,
  draw () {
    this.append($('<section class="attachments">'))
  }
})

ext.point('io.ox/mail/mobile/detail').extend({
  id: 'subject',
  index: 500,
  draw: function (baton) {
    const subject = util.getSubject(baton.data, true)
    this.append($('<div class="subject">').text(subject))
  }
})

ext.point('io.ox/mail/mobile/detail').extend({
  id: 'body',
  index: 600,
  draw: function () {
    this.append($('<section class="body user-select-text">'))
  }
})

ext.point('io.ox/mail/mobile/detail/header/indicators').extend({
  id: 'indicators',
  index: 100,
  draw (baton) {
    const $wrapper = $('<div class="wrapper flex justify-center items-center">')
    this.find('.left-side').append($('<div class="indicators">').append($wrapper))

    ext.point('io.ox/mail/mobile/detail/header/base-indicators').invoke('draw', $wrapper, baton)

    const $attachmentClass = $('<div class="attachment-indicator">').attr('aria-label', gt('Email has an attachment'))
    extensions.paperClip.call($attachmentClass, baton)
    $wrapper.append($attachmentClass)
  }
})

ext.point('io.ox/mail/mobile/detail/header/base-indicators').extend({
  id: 'base-indicators',
  index: 100,
  draw (baton) {
    const isUnseen = util.isUnseen(baton.data)
    if (isUnseen) baton.view?.$el.addClass('unread')
    const labelText = isUnseen ? gt('Marked as unread') : gt('Marked as read')
    const $unseenClass = $('<div>').attr('aria-label', labelText)
    extensions.unseenIndicator.call($unseenClass, baton)

    const $replyClass = $('<div>').attr('aria-label', gt('Email has been replied'))
    extensions.answered.call($replyClass, baton)

    const $forwardClass = $('<div>').attr('aria-label', gt('Email has been forwarded'))
    extensions.forwarded.call($forwardClass, baton)

    this.append($unseenClass, $replyClass, $forwardClass)
  }
})

ext.point('io.ox/mail/mobile/detail/header').extend({
  id: 'participants',
  index: 100,
  draw (baton) {
    const email = baton.data.from[0][1]
    const $avatar = $('<div class="avatar" data-detail-popup="halo">')
      .data({ email, email1: email })

    extensions.senderPicture.call($avatar, baton)
    this.find('.left-side').append($avatar)

    const node = $('<div class="participants ellipsis w-full">')
    ext.point('io.ox/mail/mobile/detail/header/participants').invoke('draw', node, baton, this)
    this.find('.right-side').append(node)
  }
}, {
  id: 'info-container',
  index: 200,
  draw: function (baton) {
    const node = $('<div class="info-container flex">')
    ext.point('io.ox/mail/mobile/detail/header/info-container').invoke('draw', node, baton)
    this.find('.right-side').append(node)
  }
}, {
  id: 'date-container',
  index: 300,
  draw: function (baton) {
    const node = $('<div class="date-container flex gap-8">')
    ext.point('io.ox/mail/mobile/detail/header/date-container').invoke('draw', node, baton)
    this.find('.info-container').append(node)
  }
}, {
  id: 'detail-container',
  index: 400,
  draw: function (baton) {
    const node = $('<div class="header-actions flex gap-8">')
    ext.point('io.ox/mail/mobile/detail/header/actions').invoke('draw', node, baton)
    this.find('.info-container').append(node)
  }
}, {
  id: 'categories-list',
  index: 500,
  draw (baton) {
    if (!hasFeature('categories')) return
    const $el = $('<div class="categories-badges-container">')
    ext.point('io.ox/mail/mobile/detail/header/categories-list').invoke('draw', $el, baton)
    this.find('.participants').append($el)
  }
})

ext.point('io.ox/mail/mobile/detail/header/categories-list').extend({
  index: 400,
  draw (baton) {
    const container = this.empty()
    extensions.category.call(container, baton, { searchable: true })
    this.find('.categories-badges').removeClass('mt-12 pt-8').addClass('py-4 m-0')
  }
})

ext.point('io.ox/mail/mobile/detail/header/participants').extend({
  id: 'from',
  index: 100,
  draw (baton) {
    extensions.fromDetail(baton, this)
  }
}, {
  id: 'recipients',
  index: 200,
  draw (baton, $header) {
    const $participants = this
    extensions.mobileRecipients(baton, $header, $participants)
  }
})

ext.point('io.ox/mail/mobile/detail/header/date-container').extend({
  id: 'date',
  index: 100,
  draw (baton) {
    if (util.isScheduledMail(baton.data)) {
      return extensions.scheduledDate.call(this, baton.data.date_to_send)
    }
    extensions.date.call(this, baton)
    extensions.fulldate.call(this, baton)
  }
})

ext.point('io.ox/mail/mobile/detail/header/date-container').extend({
  id: 'priority',
  index: 200,
  draw: extensions.priority

})

ext.point('io.ox/mail/mobile/detail/header/info-container').extend({
  id: 'security',
  index: 100,
  draw: extensions.security
})

ext.point('io.ox/mail/mobile/detail/header/actions').extend({
  id: 'flags',
  index: 200,
  draw (baton) {
    const node = $('<span class="flag-container p-0">').appendTo(this)
    ext.point('io.ox/mail/mobile/detail/header/flags').invoke('draw', node, baton)
  }
}, {
  id: 'categories',
  index: 300,
  draw (baton) {
    const node = $('<span class="categories">').appendTo(this)
    ext.point('io.ox/mail/mobile/detail/header/categories-picker').invoke('draw', node, baton)
  }
}, {
  id: 'mail-actions',
  index: 400,
  draw: extensions.mailActionsMenu
})

ext.point('io.ox/mail/mobile/detail/header/flags').extend({
  id: 'flag-toggle',
  index: 100,
  draw: extensions.flagToggle
})

ext.point('io.ox/mail/mobile/detail/header/flags').extend({
  id: 'color-picker',
  index: 200,
  draw (baton) {
    if (!settings.flagByColor) return
    const node = createButton({ type: 'button', variant: 'toolbar' }).attr({ 'aria-label': gt('Set color') })

    if (!baton.model.get('color_label')) node.hide()
    baton.model.on('change:color_label', (model, color) => node.toggle(!!color))

    this.append(node)
    flagPicker.attach(node, { data: baton.data, view: baton.view })
  }
})

ext.point('io.ox/mail/mobile/detail/attachments').extend({
  id: 'attachment-list',
  index: 100,
  draw (baton) {
    if (baton.attachments.length === 0) return
    extensions.attachmentList.call(this, baton)
  }
})

ext.point('io.ox/mail/mobile/detail/body').extend({
  id: 'iframe+content',
  index: 100,
  draw (baton) {
    const self = this
    ext.point('io.ox/mail/detail/body').get('iframe', function (extension) {
      extension.invoke('draw', self, baton)
    })
    ext.point('io.ox/mail/detail/body').get('content', function (extension) {
      extension.invoke('draw', self, baton)
    })
  }
})

ext.point('io.ox/mail/mobile/detail/body').extend({
  id: 'max-size',
  index: 200,
  draw (baton) {
    const isTruncated = _(baton.data.attachments).some(function (attachment) { return attachment.truncated })
    if (!isTruncated) return

    const url = 'api/mail?' + $.param({
      action: 'get',
      view: 'document',
      folder: baton.data.folder_id,
      id: baton.data.id,
      session: ox.session
    })

    this.append(
      $('<div class="max-size-warning">').append(
        $.txt(gt('This message has been truncated due to size limitations.')), $.txt(' '),
        $('<a role="button" target="_blank">').attr('href', url).text(gt('Show entire message'))
      )
    )
  }
})

// Used for header information in threads on mobile (threadView page)
// Uses all extension points from desktop view
const MobileHeaderView = DetailView.View.extend({

  events: {
    'click .detail-view-header': 'onClick'
  },
  onClick (e) {
    // trigger bubbling event
    if ($(e.target).hasClass('show-all-recipients')) return
    this.$el.trigger('showmail')
  },
  toggle () {
    // overwrite default toggle of super view
    return this
  }
})

// DetailView for mobile use
// uses extension point defined in this file
const MobileDetailView = DetailView.View.extend({

  onChangeAttachments () {
    const data = this.model.toJSON()
    const baton = ext.Baton({ data, attachments: util.getAttachments(data), view: this })
    const node = this.$el.find('section.attachments').empty()

    ext.point('io.ox/mail/mobile/detail/attachments').invoke('draw', node, baton)

    if (this.model.previous('attachments') &&
      this.model.get('attachments') &&
      this.model.previous('attachments')[0].content !== this.model.get('attachments')[0].content) this.onChangeContent()
  },

  onChangeCategories () {
    if (this.model.changed.user && _.isEqual(this.model.previous('user'), this.model.get('user'))) return
    const baton = ext.Baton({ view: this, data: this.model.toJSON() })
    const node = this.$el.find('.categories-badges-container')
    ext.point('io.ox/mail/mobile/detail/header/categories-list').invoke('draw', node, baton)
  },

  onChangeContent () {
    const data = this.model.toJSON()
    const baton = ext.Baton({
      view: this,
      model: this.model,
      data,
      attachments: util.getAttachments(data)
    })
    const node = this.getEmptyBodyNode()
    baton.disable(this.options.disable)
    // draw mail body
    ext.point('io.ox/mail/mobile/detail/body').invoke('draw', node, baton)
  },

  render () {
    const data = this.model.toJSON()
    const baton = ext.Baton({ data, model: this.model, view: this, options: this.options })
    const isThread = this.options.isThread
    const isPopup = this.options.popup
    const subject = util.getSubject(data)
    const title = util.hasFrom(data)
    // #. %1$s: Mail sender
    // #. %2$s: Mail subject
      ? gt('Email from %1$s: %2$s', util.getDisplayName(data.from[0]), subject)
      : subject

    // disable extensions?
    _(this.options.disable).each(function (extension, point) {
      if (_.isArray(extension)) {
        _(extension).each(function (ext) {
          baton.disable(point, ext)
        })
      } else {
        baton.disable(point, extension)
      }
    })

    this.$el.attr({
      'aria-label': title,
      'data-cid': this.cid,
      'data-loaded': 'false'
    })
    this.$el.data({ view: this, model: this.model })
    this.baton = baton
    ext.point('io.ox/mail/mobile/detail').invoke('draw', this.$el, baton)

    // do not change page to detail page since we show whole mails in the thread view itself
    if (!isThread && !isPopup) $('[data-page-id="io.ox/mail/detailView"]').trigger('header_ready')

    this.model.on('change:flags', model => {
      if (!this.$el) return
      const $indicators = this.$el.find('.indicators .wrapper')

      function toggleIndicator (flagName, indicatorText, extensionCallback) {
        const $indicator = $indicators.find(`[aria-label="${indicatorText}"]`)
        if (!util[flagName](model.toJSON()) || $indicator.children().length) return
        extensionCallback.call($indicator, { data: model.toJSON() })
        $indicator.show()
      }

      toggleIndicator('isAnswered', gt('Email has been replied'), extensions.answered)
      toggleIndicator('isForwarded', gt('Email has been forwarded'), extensions.forwarded)
    })

    return this
  }
})

export default {
  HeaderView: MobileHeaderView,
  DetailView: MobileDetailView
}
