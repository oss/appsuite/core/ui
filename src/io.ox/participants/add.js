/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import Backbone from '@/backbone'
import ext from '@/io.ox/core/extensions'
import pModel from '@/io.ox/participants/model'
import pViews from '@/io.ox/participants/views'
import Typeahead from '@/io.ox/core/tk/typeahead'
import * as util from '@/io.ox/mail/util'
import * as contactsUtil from '@/io.ox/contacts/util'
import * as coreUtil from '@/io.ox/core/util'
import * as calendarUtil from '@/io.ox/calendar/util'
import yell from '@/io.ox/core/yell'
import capabilities from '@/io.ox/core/capabilities'
import AddressPickerView from '@/io.ox/backbone/mini-views/addresspicker'

import { settings as settingsContacts } from '@/io.ox/contacts/settings'
import gt from 'gettext'

/**
 * extension point for autocomplete item
 */
ext.point('io.ox/participants/add/autoCompleteItem').extend({
  id: 'view',
  index: 100,
  draw (participant, options) {
    const pView = new pViews.ParticipantEntryView({
      model: participant,
      closeButton: false,
      halo: false,
      field: true,
      isMail: options.isMail
    })
    this.append(pView.render().$el)
  }
})

const validation = {

  validate (list, options) {
    if (!this.options.blocklist) return
    const opt = _.extend({ yell: true }, options)
    const invalid = this.getInvalid(list)
    // process
    if (invalid.length === 0) return
    // yell warning
    if (opt.yell) this.yell(list, invalid)
    return invalid
  },

  getInvalid (list) {
    const blocklist = this.options.blocklist
    return _(getAddresses(list)).filter(function (address) {
      return !!blocklist[address]
    })
  },

  yell (list, invalid) {
    // do not use "gt.ngettext" for plural without count
    yell('warning', (list.length === 1)
      ? gt('This email address cannot be used')
    // #. %1$s a list of email addresses (always more than one)
    // #, c-format
      : gt('The following email addresses cannot be used: %1$s', invalid.join(', '))
    )
  }
}

function getAddresses (list) {
  return _(list).map(getAddress)
}

function getAddress (item) {
  // string, data or model
  if (_.isString(item)) item = { email1: item }
  else if (item instanceof Backbone.Model) item = item.toJSON()
  return contactsUtil.getMail(item)
}

export function stringToHash (str) {
  const hash = {}
  str.split(',').forEach(address => { hash[address.trim().toLowerCase()] = true })
  return hash
}

const AddParticipantView = Backbone.View.extend({

  tagName: 'div',

  className: 'add-participants-view',

  events: {
    'keydown input': 'resolve',
    'blur input': 'resolve'
  },

  typeahead: null,

  options: {
    placeholder: gt('Add contact/resource'),
    label: gt('Add contact/resource'),
    extPoint: 'io.ox/participants/add',
    blocklist: false,
    autoselect: false
  },

  initialize (o) {
    this.options = $.extend({}, this.options, o || {})
    const { collection } = this
    if (_.isString(this.options.blocklist)) {
      this.options.blocklist = stringToHash(this.options.blocklist)
      _.extend(this, validation)
    }
    this.options.click = _.bind(this.addParticipant, this)

    const { blocklist } = this.options
    this.options.harmonize = function (data) {
      const list = data.map(item => new pModel.Participant(item))
        .filter(model => !collection.get(model))
        .filter(model => { return blocklist ? !blocklist[model.get(model.get('field'))] : true })
      // wait for participant models to be fully loaded (autocomplete suggestions might have missing values otherwise)
      const deferreds = list.map(model => model.loading)
      return $.when.apply($, deferreds).then(() => list)
    }

    // ensure a fixed scroll position when adding participants/members after the initial rendering
    this.initialRendering = true
    const scrollIntoView = _.debounce(function () {
      if (this.initialRendering) {
        this.initialRendering = false
        return
      }
      this.typeahead.el.scrollIntoView()
    }.bind(this), 0)

    this.collection.on('render', scrollIntoView)
  },

  resolve (e) {
    if (e && e.type === 'keydown' && e.which !== 13) return

    const val = this.typeahead.$el.typeahead('val')
    const list = coreUtil.getAddresses(val)
    const participants = []
    // split based on comma or semi-colon as delimiter
    _.each(list, function (value) {
      if (_.isEmpty(value)) return
      participants.push({
        display_name: util.parseRecipient(value)[0],
        email1: util.parseRecipient(value)[1],
        field: 'email1',
        type: 5
      })
    })
    return this.addParticipant(e, participants, val)
  },

  setFocus () {
    if (this.typeahead) this.typeahead.$el.focus()
  },

  addParticipant (e, data, value) {
    let list = [].concat(data)
    const distlists = []
    // validate is just used to check against blocklist
    let error = this.validate ? this.validate(list) : false
    const self = this
    // abort when blocklisted where found
    if (error) return
    // now really validate address
    list = this.getValidAddresses(list)

    // check for error but don't block (there might be other valid addresses)
    if (list.length !== [].concat(data).length) error = 'invalid addresses'

    if (this.options.convertToAttendee) {
      list = _(list).chain().map(function (item) {
        if (!(item.attributes && item.attributes.mark_as_distributionlist)) {
          return calendarUtil.createAttendee(item)
        }
        distlists.push(item)
        return undefined
      }).flatten().compact().value()
    }

    // clean typeahead input
    if (value || this.typeahead.$el.typeahead('val')) this.typeahead.$el.typeahead('val', '')

    if (!_.isEmpty(list)) this.collection.add(contactsUtil.checkDuplicateMails(list, this.collection))

    if (!_.isEmpty(distlists)) {
      _.each(distlists, function (item) {
        self.collection.resolveDistList(item.attributes.distribution_list).then(function (list) {
          _.each(contactsUtil.validateDistributionList(list), function (item) {
            // hint: async custom wrapper
            self.collection.add(calendarUtil.createAttendee(item))
          })
        })
      })
    }

    // return possible errors, so views can react
    return error
  },

  getValidAddresses (list) {
    return _(list).filter(function (item) {
      const address = getAddress(item)
      if (coreUtil.isValidMailAddress(address)) return true
      // #. %1$s is an email address
      yell('error', gt('Cannot add contact with an invalid email address: %1$s', address))//
      return false
    })
  },

  render () {
    const guid = _.uniqueId('form-control-label-')
    const self = this

    this.typeahead = new Typeahead(this.options)
    this.$el.append(
      this.typeahead.$el.attr({ id: guid }).addClass('add-participant')
    )
    this.typeahead.render()
    if (this.options.scrollIntoView) {
      const $el = this.$el
      this.typeahead.on('typeahead-custom:dropdown-rendered', function () {
        const target = $el.find('.tt-dropdown-menu')
        const container = target.scrollParent()
        const pos = target.offset().top - container.offset().top
        if (!target.is(':visible')) return
        if ((pos < 0) || (pos + target.height() > container.height())) {
          // scroll to Node, leave 16px offset
          container.scrollTop(container.scrollTop() + pos - 16)
        }
      })
    }
    // add data-1p-ignore to prevent 1Password or e.g. Lastpass from filling the input
    this.$el.find('.tt-input').attr('data-1p-ignore', true)
    this.options.usePicker = !_.device('smartphone') && capabilities.has('contacts') && settingsContacts.get('picker/enabled', true)
    if (this.options.usePicker) {
      this.addresspicker = new AddressPickerView({
        process (e, member) {
          // validate is just used to check against blocklist
          const error = self.validate ? self.validate([member]) : false
          // abort when blocklisted where found
          if (error) return
          if (self.options.convertToAttendee) {
            if (!self.options.processRaw && member.type !== 3) {
              // fix type 5 that are actually type 1
              member.magic()
            } else if (!member.type && (!member.folder_id || !member.user_id || member.field !== 'email1')) {
              member.type = 5
            }
            self.options.collection.add(contactsUtil.checkDuplicateMails(calendarUtil.createAttendee(member), self.options.collection))
            return
          }
          self.options.collection.add(contactsUtil.checkDuplicateMails(member, self.options.collection))
        },
        processRaw: self.options.processRaw,
        useGABOnly: self.options.useGABOnly,
        selection: self.options.selection,
        hideResources: !self.options?.apiOptions?.resources,
        blocklist: self.options.blocklist
      })
      this.$el.append(
        this.addresspicker.render().$el
      )
      this.$el.wrapInner($('<div class="input-group has-picker">'))
    }
    // move visible label to proper position
    const $label = $('<label class="sr-only m-0 p-0">').attr({ for: guid }).text(this.options.label)
    this.$el.prepend(
      $('<div class="flex-row pt-8">').append(
        $label.toggleClass('sr-only', !this.options.labelVisible)
      )
    )
    return this
  }
})

export default AddParticipantView
