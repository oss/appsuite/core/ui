/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import mobile from '@/io.ox/backbone/views/actions/mobile'
import * as util from '@/io.ox/calendar/util'
import _ from '@/underscore'
import '@/io.ox/calendar/actions'

import gt from 'gettext'

const meta = {
  create: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Create appointment'),
    icon: 'bi/plus-lg.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/detail/actions/create',
    isPrimary: true
  },
  listView: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('List view'),
    icon: 'bi/card-list.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/switch-to-list-view'
  },
  calendarView: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Calendar view'),
    icon: 'bi/table.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/switch-to-month-view'
  },
  dayView: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('List view'),
    icon: 'bi/card-list.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/switch-to-day-view'
  },
  nextDay: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Next day'),
    icon: 'bi/chevron-right.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showNext'
  },
  prevDay: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Previous day'),
    icon: 'bi/chevron-left.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showPrevious'
  },
  nextMonth: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Next month'),
    icon: 'bi/chevron-right.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showNext'
  },
  prevMonth: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Previous month'),
    icon: 'bi/chevron-left.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showPrevious'
  },
  nextYear: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Next year'),
    icon: 'bi/chevron-right.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showNext'
  },
  prevYear: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Previous year'),
    icon: 'bi/chevron-left.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showPrevious'
  },
  today: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Today'),
    drawDisabled: true,
    ref: 'io.ox/calendar/actions/showToday'
  },
  calendars: {
    mobile: 'hi',
    title: gt('Calendars'),
    ref: 'io.ox/calendar/actions/calendars'
  },
  move: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Move'),
    icon: 'bi/folder-symlink.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/detail/actions/move'
  },
  delete: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Delete'),
    icon: 'bi/trash.svg',
    drawDisabled: true,
    ref: 'io.ox/calendar/detail/actions/delete'
  },
  export: {
    prio: 'hi',
    mobile: 'hi',
    title: gt('Export'),
    drawDisabled: true,
    ref: 'io.ox/calendar/detail/actions/export'
  }
}

const points = {
  folderTree: 'io.ox/calendar/mobile/toolbar/folderTree',
  yearView: 'io.ox/calendar/mobile/toolbar/year',
  monthView: 'io.ox/calendar/mobile/toolbar/month',
  weekView: 'io.ox/calendar/mobile/toolbar/week',
  listView: 'io.ox/calendar/mobile/toolbar/list',
  detailView: 'io.ox/calendar/mobile/toolbar/detailView',
  weekDayHeader: 'io.ox/calendar/mobile/toolbar/weekDayHeader',
  listHeader: 'io.ox/calendar/mobile/toolbar/listHeader',
  yearViewbottomToolbar: 'io.ox/calendar/mobile/bottom-toolbar/year',
  monthViewbottomToolbar: 'io.ox/calendar/mobile/bottom-toolbar/month',
  weekViewbottomToolbar: 'io.ox/calendar/mobile/bottom-toolbar/week'
}

// clone all available links from inline links (larger set)
ext.point(points.detailView + '/links').extend(
  ext.point('io.ox/calendar/links/inline').list().map(function (item) {
    item = _(item).pick('id', 'index', 'prio', 'mobile', 'icon', 'title', 'ref', 'section', 'sectionTitle')
    return item
  })
)

mobile.addAction(points.folderTree, meta, ['create'])
mobile.addAction(points.yearView, meta, ['create'])
mobile.addAction(points.monthView, meta, ['create'])
mobile.addAction(points.weekView, meta, ['create'])
mobile.addAction(points.listView, meta, ['create'])
mobile.addAction(points.weekDayHeader, meta, ['listView'])
mobile.addAction(points.listHeader, meta, ['dayView'])
mobile.addAction(points.yearViewbottomToolbar, meta, ['prevYear', 'today', 'nextYear', 'calendars'])
mobile.addAction(points.monthViewbottomToolbar, meta, ['prevMonth', 'today', 'nextMonth', 'calendars'])
mobile.addAction(points.weekViewbottomToolbar, meta, ['prevDay', 'today', 'nextDay', 'calendars'])

const updateToolbar = _.debounce(function (list) {
  if (!list) return
  // extract single object if length === 1
  list = list.length === 1 ? list[0] : list
  // draw toolbar
  const baton = ext.Baton({ data: list, app: this })

  this.pages.getToolbar('month').setBaton(baton)
  this.pages.getToolbar('list').setBaton(baton)
}, 10)

function prepareUpdateToolbar (app) {
  let list = app.pages.getCurrentPage().name === 'list' ? app.listView.selection.get() : []
  list = _(list).map(function (item) {
    if (_.isString(item)) item = _.extend(util.cid(item), { flags: app.listView.selection.getNode(item).attr('data-flags') || '' })
    return item
  })
  app.updateToolbar(list)
}

// some mediator extensions
// register update function and introduce toolbar updating
ext.point('io.ox/calendar/mediator').extend({
  id: 'toolbar-mobile',
  index: 10100,
  setup (app) {
    if (!_.device('smartphone')) return
    app.updateToolbar = updateToolbar
  }
})

ext.point('io.ox/calendar/mediator').extend({
  id: 'update-toolbar-mobile',
  index: 10300,
  setup (app) {
    if (!_.device('smartphone')) return
    app.updateToolbar([])
    // update toolbar on selection change
    app.listView.on('selection:change', function () {
      prepareUpdateToolbar(app)
    })
    // folder change
    app.on('folder:change', function () {
      prepareUpdateToolbar(app)
    })
    app.getWindow().on('change:perspective change:initialPerspective', function () {
      _.defer(prepareUpdateToolbar, app)
    })
  }
})

ext.point('io.ox/calendar/mediator').extend({
  id: 'change-mode-toolbar-mobile',
  index: 10400,
  setup (app) {
    if (!_.device('smartphone')) return
    // if multiselect is triggered, show secondary toolbar with other options based on selection
    app.props.on('change:checkboxes', function (model, state) {
      app.pages.toggleSecondaryToolbar('list', state)
    })
  }
})
