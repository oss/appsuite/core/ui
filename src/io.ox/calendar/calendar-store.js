/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { defineStore } from 'pinia'
import { computed, ref, shallowRef, triggerRef, watch } from 'vue'
import { DateTime, Interval } from 'luxon'

import api from '@/io.ox/calendar/api'
import ox from '@/ox'

// list of error codes where a folder should be removed from the selection
const removeList = [
  'FLD-1004', // folder storage service no longer available
  'FLD-0008', // folder not found
  'FLD-0003', // permission denied
  'CAL-4060', // folder is not supported
  'CAL-4030', // permission denied
  'CAL-4044' // account not found
]

const defaultFields = [
  'lastModified',
  'color',
  'createdBy',
  'endDate',
  'flags',
  'folder',
  'id',
  'location',
  'recurrenceId',
  'rrule',
  'seriesId',
  'startDate',
  'summary',
  'timestamp',
  'transp',
  'attendeePrivileges',
  'categories'
].join(',')

export const useCalendarStore = defineStore('calendar', () => {
  const activeLayout = ref('week:day')
  const selectedDate = ref(DateTime.now())
  const appointments = shallowRef({})
  const requests = ref([])
  const selectedFolders = ref([])
  const selectedYear = computed(() => selectedDate.value.year)
  const headerTitle = ref('')
  const today = ref(DateTime.now().startOf('day'))

  updateToday()

  function updateToday () {
    setTimeout(() => {
      const newDate = DateTime.now().startOf('day')
      if (today.value.valueOf() !== newDate.valueOf()) {
        today.value = newDate
      }
      updateToday()
    }, msLeftOfToday() + 100)
  }

  const rangeAlreadyRequested = ({ rangeStart, rangeEnd, folder }) => {
    const requestedRange = Interval.fromDateTimes(rangeStart, rangeEnd)
    return requests.value
      .find(request => {
        let [requestFolder, requestStart, requestEnd] = request.split('.')
        if (requestFolder !== folder) return false
        requestEnd = DateTime.fromISO(requestEnd).endOf('day')
        const interval = Interval.fromISO(`${requestStart}/${requestEnd}`)
        return interval.engulfs(requestedRange) || interval.equals(requestedRange)
      })
  }

  const appointmentsByDay = computed(() => {
    const results = {}
    const days = Object.keys(appointments.value)

    days.forEach(day => {
      const dayAppointments = Object.values(appointments.value[day] || {})
        .filter(appointment => selectedFolders.value?.includes(appointment.folder))
      results[day] = dayAppointments
    })
    return results
  })

  /**
   * Sets the year of the selected date.
   * @param {number} year - The year to set.
   */
  function setYear (year) {
    selectedDate.value = DateTime.fromObject({ year, month: 1, day: 1 })
  }

  /**
   * Fetches appointments for a given date range.
   * @param {Object} newParams - The parameters for the calendar request (action=all).
   * @returns {Promise<Array<Object>>} - Promise that resolves with the fetched appointments.
   */
  function fetchAppointments (newParams) {
    const params = Object.assign({
      action: 'all',
      fields: defaultFields,
      order: 'asc',
      sort: 'startDate',
      expand: true
    }, newParams)

    const folders = params.folders || selectedFolders.value

    return api.request({
      module: 'chronos',
      params,
      data: { folders }
    }, selectedFolders.value ? 'PUT' : 'GET').then(function success (data) {
      return data
        // Appointments from standard folders should appear before resource calendar appointments
        .sort((a, b) => a.folder.indexOf('cal://0/resource') - b.folder.indexOf('cal://0/resource'))
        .map(function (data) {
          // no folders defaults to all folder
          if (!selectedFolders.value) return data
          if (data.events) return data.events
          if (!removeList.includes(data.error.code)) return undefined
          api.trigger('all:fail', data.folder)
          return undefined
        })
        .filter(Boolean)
        .flat()
    }, function fail (err) {
      console.log('fail', err) // TODO handle error
    })
  }

  /**
   * Returns appointments for a given date range from cache. If appointments are not present in the cache, they are fetched from the server.
   * @param {Object} options - The options for retrieving appointments.
   * @param {DateTime} options.start - The start date of the time range.
   * @param {DateTime} [options.end] - The end date of the time range. If not provided, defaults to the same as the start date.
   * @param {Array} [options.folder] - The folders to retrieve appointments for. If not provided, defaults to all selected folders.
   * @returns {Promise<Object>} - Promise that resolves with the fetched appointments.
   */
  async function getAppointments ({ start, end, folder }) {
    start = start.startOf('day')
    end = end ? end.endOf('day') : start.endOf('day')

    const folders = (folder ? [folder] : selectedFolders.value)
      .filter(folder => !rangeAlreadyRequested({ rangeStart: start, rangeEnd: end, folder }))

    if (!folders.length) return

    // if not present in cache, fetch missing appointments
    await updateAppointments({ start, end, folders, reset: false })
  }

  /**
   * Updates appointments within a specified range. If reset is true, drops all existing appointments before updating.
   * @param {Object} options - The options for setting appointments.
   * @param {DateTime} options.start - The start date and time of the range.
   * @param {DateTime} options.end - The end date and time of the range.
   * @param {Array} options.folders - The folders to retrieve appointments for.
   * @param {boolean} [options.reset=true] - Whether to delete existing appointments before updating.
   * @returns {Promise<void>} - A promise that resolves when the appointments are set.
   */
  async function updateAppointments ({ start, end, folders = selectedFolders.value, reset = true }) {
    start = start.startOf('day')
    end = end.endOf('day')

    const params = {
      rangeStart: start.toFormat("yyyyMMdd'T'HHmmss'Z'"),
      rangeEnd: end.toFormat("yyyyMMdd'T'HHmmss'Z'"),
      folders
    }
    try {
      const fetched = await fetchAppointments(params)

      if (reset) {
        requests.value = []
        appointments.value = {}
      }

      folders.forEach(folder => {
        requests.value.push(`${folder}.${start.toISODate()}.${end.toISODate()}`)
      })

      fetched.forEach(appointment => addAppointment(appointment))

      // add days without appointments as empty objects
      let iterator = start
      while (iterator < end) {
        const key = iterator.toISODate()
        if (!appointments.value[key]) appointments.value[key] = {}
        iterator = iterator.plus({ day: 1 })
      }
      triggerRef(appointments)
    } catch (error) {
      console.log(error)
    }
  }

  /**
   * Adds an appointment to the calendar store.
   * @param {Object} appointment - The appointment object to be added.
   */
  function addAppointment (appointment) {
    const start = DateTime.fromISO(appointment.startDate.value, { zone: appointment.startDate.tzid })
    const end = DateTime.fromISO(appointment.endDate.value, { zone: appointment.endDate.tzid })
    const id = `${appointment.folder}.${appointment.id}` +
      (appointment.recurrenceId ? `.${appointment.recurrenceId}` : '')

    let iterator = start.startOf('day')
    while (iterator < end) {
      const key = iterator.toISODate()
      if (!appointments.value[key]) appointments.value[key] = {}
      appointments.value[key][id] = appointment
      iterator = iterator.plus({ days: 1 })
    }
  }

  /**
   * Removes an appointment from the calendar store.
   * @param {Object} appointment - The appointment to be removed.
   */
  function removeAppointment (appointment) {
    const dateKey = DateTime.fromISO(appointment.startDate.value, { zone: appointment.startDate.tzid }).toISODate()
    const cid = appointment.cid
    delete appointments.value[dateKey][cid]
  }

  /**
   * Updates an appointment from the calendar store.
   * @param {Object} appointment - The appointment to be updated.
   */
  function updateAppointment (appointment) {
    const cid = appointment.cid
    const newStartdate = DateTime.fromISO(appointment.startDate.value, { zone: appointment.startDate.tzid })

    for (const [key, value] of Object.entries(appointments.value)) {
      if (appointments.value[key][cid]) {
        const oldAppointment = value[cid]
        const oldStartdate = DateTime.fromISO(oldAppointment.startDate.value, { zone: oldAppointment.startDate.tzid })
        if (!oldStartdate.hasSame(newStartdate, 'day')) {
          delete appointments.value[key][cid]
          appointments.value[newStartdate.toISODate()][cid] = appointment
        } else {
          appointments.value[key][cid] = appointment
        }
        return
      }
    }
  }

  // TODO: missing behavior for refetching, when switching to month view
  api.on('delete', async (appointmentAttributes) => {
    if (appointmentAttributes.recurrenceId) {
      const { start, end } = getDefaultRange()
      await updateAppointments({ start, end, reset: true })
      return
    }
    removeAppointment(appointmentAttributes)
    triggerRef(appointments)
  })

  // process:create gets triggered when an appointment gets changed from a simple to a recurring appointment
  api.on('create process:create', async (appointmentAttributes) => {
    if (appointmentAttributes.recurrenceId) {
      const { start, end } = getDefaultRange()
      await updateAppointments({ start, end, reset: true })
      return
    }
    addAppointment(appointmentAttributes)
    triggerRef(appointments)
  })

  api.on('update', (appointmentAttributes) => {
    updateAppointment(appointmentAttributes)
    triggerRef(appointments)
  })

  ox.on('socket:calendar:updates', async function fetchAppointmentUpdates () {
    const { start, end } = getDefaultRange()
    await updateAppointments({ start, end, reset: true })
  })

  function getDefaultRange () {
    let start, end

    switch (activeLayout.value) {
      case 'week:day':
        start = selectedDate.value.minus({ days: 7 }).startOf('day')
        end = selectedDate.value.plus({ days: 8 }).endOf('day')
        break
      default:
        start = selectedDate.value.minus({ months: 1 }).startOf('month').startOf('week', { useLocaleWeeks: true }).startOf('day')
        end = selectedDate.value.plus({ months: 1 }).endOf('month').endOf('week', { useLocaleWeeks: true }).endOf('day')
    }

    return { start, end }
  }

  watch(selectedFolders, async (newFolders, oldFolders) => {
    const folders = newFolders.filter(folder => !oldFolders.includes(folder))
    const { start, end } = getDefaultRange()
    await getAppointments({ start, end, folders, reset: false })
  })

  return {
    activeLayout,
    selectedYear,
    selectedDate,
    appointments,
    today,
    setYear,
    getAppointments,
    updateAppointments,
    addAppointment, // TODO export can be removed after refactoring
    removeAppointment,
    updateAppointment,
    selectedFolders,
    appointmentsByDay,
    headerTitle
  }
})

function msLeftOfToday () {
  const startNextDay = DateTime.now().plus({ days: 1 }).startOf('day')
  return startNextDay - DateTime.now()
}
