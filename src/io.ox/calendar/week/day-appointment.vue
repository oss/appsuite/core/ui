<!--

  @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
  @license AGPL-3.0

  This code is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.

  Any use of the work other than as authorized under this license or copyright law is prohibited.

-->

<template>
  <div
    role="button"
    class="appointment"
    :class="classes"
    :style="{ ...layout, ...colors }"
    @click.stop="openAppointment"
    :aria-label="ariaLabel"
  >
    <div
      class="appointment-content"
      aria-hidden="true"
      :title="title"
    >
      <div v-if="typeFlags.length" class="title-container">
        <BaseIcon
          v-for="flag in typeFlags"
          :key="flag.className"
          class="bi-14 my-auto"
          :class="flag.className"
          :iconName="flag.icon"
          :title="flag.title"
          :aria-label="flag.title"
        />
        <div class="title">{{ appointment.summary }}</div>
      </div>
      <div v-else class="title">{{ appointment.summary }}</div>
      <div v-if="appointment.location" class="location">{{ appointment.location }}</div>
      <div
        v-if="propertyFlags.length"
        class="flags shrink-0"
        :class="{'bottom-right': !isFullDay}"
      >
        <BaseIcon
          v-for="flag in propertyFlags"
          :key="flag.className"
          class="bi-14"
          :class="flag.className"
          :iconName="flag.icon"
          :title="flag.title"
          :aria-label="flag.title"
        />
      </div>
    </div>
  </div>
</template>

<script setup>
import ox from '@/ox'
import { computed, ref, toRefs } from 'vue'
import { DateTime } from 'luxon'
import BaseIcon from '@/io.ox/vueComponents/base-icon.vue'
import gt from 'gettext'
import ext from '@/io.ox/core/extensions'
import apps from '@/io.ox/core/api/apps'
import api from '@/io.ox/calendar/api'
import folderAPI from '@/io.ox/core/folder/api'
import * as util from '@/io.ox/calendar/util'
import detailView from '@/io.ox/calendar/view-detail'

const props = defineProps({
  appointment: { type: Object, required: true },
  date: Object,
  columnLayout: Object
})
const isFullDay = !props.date
const { appointment } = toRefs(props)

const folder = computed(() => folderAPI.pool.getModel(appointment.value.folder).toJSON())

const layout = computed(() => {
  const start = DateTime.fromISO(appointment.value.startDate.value, { zone: appointment.value.startDate.tzid })
  const end = DateTime.fromISO(appointment.value.endDate.value, { zone: appointment.value.endDate.tzid })
  let left, width
  if (props.columnLayout) {
    const topPadding = '1px'
    const bottomPadding = '4px'
    const { firstColumn, lastColumn, totalColumns } = props.columnLayout
    const columnWidth = 100 / totalColumns
    left = `calc(${firstColumn * columnWidth}% - ${topPadding})`
    width = `calc(${(lastColumn - firstColumn + 1) * columnWidth}% - ${bottomPadding})`
  }

  return {
    left,
    width,
    top: getTopPropertyFromTime(start),
    height: getHeightProperty(start, end),
    'align-items': isFullDay ? 'center' : 'unset'
  }
})

const darkTheme = ref(document.querySelector('html').classList.contains('dark'))
ox.on('themeChange', () => {
  darkTheme.value = document.querySelector('html').classList.contains('dark')
})

const derivedColors = computed(() => {
  const baseColor = util.getAppointmentColor(folder.value, appointment.value)
  return util.deriveAppointmentColors(baseColor, darkTheme.value)
})
const colors = computed(() => {
  return {
    color: derivedColors.value.foreground,
    'background-color': derivedColors.value.background,
    'border-inline-start-color': derivedColors.value.border
  }
})

const propertyFlags = computed(() => util.returnIconsByType(appointment.value, true).property)
const typeFlags = computed(() => util.returnIconsByType(appointment.value, true).type)
const title = computed(() => {
  const locationString = appointment.value.location ? `, ${appointment.value.location}` : ''
  return appointment.value.summary + locationString
})
const ariaLabel = computed(() => `${title.value}, ${gt('Category')}: ${derivedColors.value.name}`)

function getTopPropertyFromTime (start) {
  if (isFullDay) return
  return start < props.date.startOf('day') ? 0 : `${getPositionFromTime(start) * 100}%`
}

function getHeightProperty (start, end) {
  if (isFullDay) return

  const bottomPadding = '2px'
  const top = start < props.date.startOf('day') ? 0 : getPositionFromTime(start)
  const bottom = end >= props.date.endOf('day') ? 1 : getPositionFromTime(end)

  return `calc(${(bottom - top) * 100}% - ${bottomPadding})`
}

function getPositionFromTime (time) {
  const minutes = time.diff(props.date.startOf('day')).as('minutes')
  return minutes / 24 / 60
}

const classes = computed(() => {
  const lightTheme = colors.value.background === 'white'
  const isPrivate = util.isPrivate(appointment.value) && !folderAPI.is('private', folder)
  const modify = folderAPI.can('write', folder, appointment.value) && util.allowedToEdit(appointment.value, folder)
  const confirmation = util.hasParticipationStatus(appointment.value)
    ? { [util.getConfirmationStatusFromAppointment(appointment.value)]: true }
    : {}

  return {
    white: lightTheme,
    black: !lightTheme,
    private: isPrivate,
    [util.getShownAsClass(appointment.value)]: true,
    [util.getStatusClass(appointment.value)]: true,
    modify,
    ...confirmation
  }
})

function openAppointment () {
  const app = apps.get('io.ox/calendar')
  app.pages.changePage('detailView')
  app.pages.getPage('detailView').busy()

  const page = app.pages.getPage('detailView')
  api.get({ folder: appointment.value.folder, id: appointment.value.id, recurrenceId: appointment.value.recurrenceId })
    .then(function (model) {
      const baton = new ext.Baton({ data: model.toJSON(), model })
      page.idle().empty().append(detailView.draw(baton))
      app.pages.getToolbar('detailView').setBaton(baton)
    })
}
</script>

<style lang="scss" scoped>
.appointment {
  height: calc(4.16667% - 1px);
  inset-inline: -1px 4px;
  width: unset;
}
</style>
