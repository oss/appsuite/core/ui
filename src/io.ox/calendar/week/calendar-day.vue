<!--

  @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
  @license AGPL-3.0

  This code is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.

  Any use of the work other than as authorized under this license or copyright law is prohibited.

-->

<template>
  <div class="scrollpane" :date="date.toISODate()">
    <div class="week-container-label" aria-hidden="true">
      <div class="time"
        v-for="(timeLabel, i) in timeLabels"
        :key="timeLabel"
        :class="{ in: isIn(i) }"
      >
        <div class="number">{{ timeLabel }}</div>
      </div>
    </div>
    <div class="day" ref="day">
      <div class="timeslot"
        v-for="(timeslot, i) in timeslots"
        :key="timeslot"
        :class="{
          out: isOut(i),
          'working-time-border': isWorkingTimeBorder(i)
        }"
        :data-timeslot="i"
      ></div>
      <Appointment
        v-for="appointment in appointments"
        :key="appointment.id"
        :appointment="appointment"
        :date="date"
        :column-layout="layout[appointment.id]"
      />
    </div>
    <CurrentTimeIndicator
      :date="date"
      v-if="isToday"
    />
  </div>
</template>

<script setup>
import { DateTime, Interval } from 'luxon'
import { computed, ref, watch } from 'vue'
import { onLongPress } from '@vueuse/core'
import { useCalendarStore } from '@/io.ox/calendar/calendar-store'
import CurrentTimeIndicator from '@/io.ox/calendar/week/current-time-indicator.vue'
import Appointment from '@/io.ox/calendar/week/day-appointment.vue'
import ox from '@/ox'
import apps from '@/io.ox/core/api/apps'
import { settings } from '@/io.ox/calendar/settings'
import calendarModel from '@/io.ox/calendar/model'

const store = useCalendarStore()
const { date } = defineProps({ date: { type: Object, reqired: true } })
const day = ref(null)
const isToday = computed(() => store.today.hasSame(date, 'day'))
const layout = ref({})
const gridSize = 60 / settings.get('interval', 30)
const workingHours = { start: Number(settings.get('startTime', 8)), end: Number(settings.get('endTime', 18)) }
const timeslots = Array(24 * gridSize)
const timeLabels = Array(24).fill().map((_, i) => `${i}:00`)
const isWorkingTimeBorder = i => ++i / gridSize === workingHours.start || i / gridSize === workingHours.end
const appointments = computed(() => {
  const key = date.toISODate()
  return Object.values(store.appointmentsByDay[key] || {})
    // remove full day appointments
    .filter(appointment => appointment.startDate.value.includes('T'))
    // set luxon start time, end time and interval to save cpu cycles
    .map(appointment => {
      const start = DateTime.fromISO(appointment.startDate.value, { zone: appointment.startDate.tzid })
      // to avoid false conflicts when using luxons `overlaps()` function we need to offset the
      // end by one millisecond. For example an appointment should end at 08:59:59:999 instead
      // of 09:00 or it would overlap with another event starting at 09:00
      const end = DateTime
        .fromISO(appointment.endDate.value, { zone: appointment.endDate.tzid })
        .minus({ Milliseconds: 1 })
      appointment.startDate.dateTime = start
      appointment.endDate.dateTime = end
      appointment.interval = Interval.fromDateTimes(start, end)
      return appointment
    })
})

/**
 * Checks wether a timeslot is outside the working hours.
 * @param {number} timeslot
 * @returns {bool}
 */
function isOut (timeslot) {
  return timeslot / gridSize < workingHours.start ||
    timeslot / gridSize >= workingHours.end
}

/**
 * Checks wether an hour is within the working hours.
 * @param {number} hour - Hour of the day.
 * @returns {bool}
 */
function isIn (hour) { return hour >= workingHours.start && hour <= workingHours.end }

onLongPress(day, (event) => {
  const timeslot = event.target.dataset.timeslot
  if (timeslot) openEditDialog(timeslot)
}, { delay: 500 })

/**
 * Opens edit dialog to create a new appointment in a given timeslot.
 * @param {number} timeslot
 */
function openEditDialog (timeslot) {
  const app = apps.get('io.ox/calendar')
  const folder = app.folder.get()
  const minutes = 60 / gridSize * timeslot

  const startDate = DateTime.fromObject({ day: store.selectedDate.day, hour: Math.floor(minutes / 60), minute: minutes % 60 })
  const endDate = startDate.plus({ hour: 1 })

  const params = {
    startDate: { value: startDate.toFormat('yyyyMMdd\'T\'HHmmss'), tzid: startDate.zoneName },
    endDate: { value: endDate.toFormat('yyyyMMdd\'T\'HHmmss'), tzid: endDate.zoneName },
    folder
  }

  ox.load(() => Promise.all([import('@/io.ox/calendar/edit/main')]))
    .then(async function ([{ edit }]) {
      const app = edit.getApp()
      await app.launch()
      app.create(new calendarModel.Model(params))
    })
}

watch(appointments, () => {
  let lastEnd = date.startOf('day') // latest end time in the placed appointments
  const cluster = [] // cluster of overlapping appointments

  appointments.value
    // sort by starting time and by length for appointments with the same starting time
    .sort((a, b) => {
      return a.startDate.dateTime - b.startDate.dateTime || // 0 (falsy) if same start
        b.endDate.dateTime - a.endDate.dateTime
    })
    // cluster overlapping appointments and sort them into columns
    .forEach(appointment => {
      if (appointment.startDate.dateTime >= lastEnd) {
        Object.assign(layout.value, getLayout(cluster))
        cluster.length = 0
      }
      const isPlaced = cluster.some((column, index) => {
        if (isOverlapping(appointment, column)) return false
        cluster[index].push(appointment)
        return true
      })
      if (!isPlaced) cluster.push([appointment])
      if (lastEnd < appointment.endDate.dateTime) lastEnd = appointment.endDate.dateTime
    })

  Object.assign(layout.value, getLayout(cluster)) // last cluster
}, { immediate: true })

/**
 * Calculates an optimal column layout for a cluster of conflicting appointments
 * @typedef {{firstColumn: number, lastColumn: number, totalColumns: number}} Columns
 * @typedef {string} appointmentID - Unique appointment identifier.
 * @param {Array<Object[]>} cluster - Cluster of columns of otherwise conflicting appointments.
 * @returns {Object<appointmentID, Columns>} - Object containing the column layout for the cluster's appointments.
 */
function getLayout (cluster) {
  const result = {}
  const totalColumns = cluster.length
  cluster.forEach((column, columnIndex) => {
    const remainingColumns = cluster.slice(columnIndex + 1)
    column.forEach(appointment => {
      const unusedColumns = getEmptyNeighboringColumnsCount(appointment, remainingColumns)
      result[appointment.id] = {
        firstColumn: columnIndex,
        lastColumn: columnIndex + unusedColumns,
        totalColumns
      }
    })
  })
  return result
}

/**
 * Counts the empty columns to the right of an appointment in a cluster of conflicting appointments.
 * @param {Object} appointment - An appointment to the right of the columns.
 * @param {Array<Object>} columns - Columns containing the appointments left of the appointment.
 * @returns {number} - Count of empty columns left of the appointment.
 */
function getEmptyNeighboringColumnsCount (appointment, columns) {
  let index = 0
  while (index < columns.length) {
    if (isOverlapping(appointment, columns[index])) return index
    index++
  }
  return index
}

/**
 * Check wether an appointment conflicts with other appointments.
 * @param {Object} appointment - Appointment that gets checked for conflict.
 * @param {Array<Object>} otherAppointments - Potentially conflicting appointments.
 * @returns {bool}
 */
function isOverlapping (appointment, otherAppointments) {
  return otherAppointments.some(otherAppointment => {
    return appointment.interval.overlaps(otherAppointment.interval)
  })
}
</script>

<style lang="scss" scoped>
  .timeslot { min-height: 28px; }
</style>

<style lang="scss">
  swiper-slide { overflow-y: scroll; }
</style>
