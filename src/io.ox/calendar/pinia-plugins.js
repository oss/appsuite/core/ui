/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { watch } from 'vue'
import { storeToRefs } from 'pinia'
import { DateTime } from 'luxon'

export function getCalendarModelAdapter (app) {
  return context => {
    if (context.store.$id !== 'calendar') return

    const { activeLayout, selectedDate } = storeToRefs(context.store)

    context.store.selectedDate = DateTime.fromMillis(app.props.get('date'))

    app.props.on('change:date', (model, value) => {
      if (DateTime.fromMillis(value).equals(context.store.selectedDate.valueOf())) return
      context.store.selectedDate = DateTime.fromMillis(value)
    })

    app.props.on('change:layout', (model, value) => {
      if (context.store.activeLayout === value) return
      context.store.activeLayout = value
    })

    context.store.selectedFolders = app.folders.folders
    app.on('folders:change', (value) => {
      context.store.selectedFolders = value
    })

    watch(activeLayout, newLayout => {
      // FIX: this code does not work due to non-synchronized settings between desktop / mobile
      // const modelLayout = app.settings.get('layout')
      // if (newLayout === modelLayout) return << breaks here
      // if (!_.device('smartphone')) return app.settings.set('layout', newLayout)

      // // use animation, if we change to the next page on the left
      // // later this should be solved by the component resembling the page controller
      // const dayToMonth = modelLayout === 'week:day' && newLayout === 'month'
      // const monthToYear = modelLayout === 'month' && newLayout === 'year'
      // const yearToTree = modelLayout === 'year' && newLayout === 'folderTree'
      // const dayToList = modelLayout === 'list' && newLayout === 'week:day'
      // let animation = { disableAnimations: true }

      // if (dayToMonth || monthToYear || yearToTree) animation = { animation: 'slideright' }
      // else if (dayToList) animation = { animation: 'slideup' }

      app.pages.changePage(newLayout, { animation: 'slideleft' })
    })

    watch(selectedDate, newDate => {
      app.props.set('date', newDate.valueOf())
    })
  }
}
