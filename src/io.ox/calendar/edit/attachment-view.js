/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import Backbone from 'backbone'
import $ from '@/jquery'
import _ from '@/underscore'
import { AttachmentListView } from '@/io.ox/backbone/mini-views/attachments'

export const AttachmentCollection = Backbone.Collection.extend({
  parse (file) {
    return {
      file,
      filename: file.name,
      fmtType: file.type,
      size: file.size,
      cid: _.uniqueId('unique-')
    }
  }
})

// Chronos specific attachment list view.
// Chronos does not use the attachment api and doesn't need most of the event listeners and requests.
export const ChronosAttachmentListView = AttachmentListView.extend({

  setup () {
    this.listenTo(this.collection, 'add remove reset', this.render)
  },

  onDelete (e) {
    e.preventDefault()
    const node = $(e.currentTarget).closest('.attachment')
    const model = this.collection.get(node.attr('data-cid'))
    return this.collection.remove(model)
  }
})
