/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import moment from '@/moment'
import Backbone from '@/backbone'
import { TextView } from '@/io.ox/backbone/mini-views/common'
import calendarAPI from '@/io.ox/calendar/api'
import ModalDialog from '@/io.ox/backbone/views/modal'
import { getDateTimeIntervalMarkup } from '@/io.ox/calendar/util'
import { AttendeeCollection } from '@/io.ox/calendar/model'
import AddParticipantsView from '@/io.ox/participants/add'
import { AttendeeContainer } from '@/io.ox/participants/chronos-views'
import yell from '@/io.ox/core/yell'

import '@/io.ox/calendar/style.scss'

import gt from 'gettext'
import { getRecurrenceString } from '@/io.ox/calendar/recurrence-rule-map-model'

export async function openDialog (appointmentData) {
  if (!appointmentData) return
  const { recurrenceId, flags, attendees, summary, id, folder } = appointmentData

  let action = 'appointment'
  // no need for series change on last occurrence
  if (recurrenceId || flags?.includes('last_occurrence')) {
    // no series splits allowed
    const dialog = new ModalDialog({
      width: 620,
      title: gt('Invite to series?')
    })
      .addCancelButton({ left: true })
      .addButton({ label: gt('Edit this appointment'), action: 'appointment', className: 'btn-default' })
      .addButton({ label: gt('Edit series'), action: 'series' })
      .build(function () {
        this.$body.append(gt('This appointment is part of a series.'), '\u00a0', gt('Do you want to forward for the whole series or just this appointment within the series?'))
      }).open()
    action = await new Promise(resolve => dialog.on('action', resolve))
  }

  if (action === 'cancel') return

  new ModalDialog({
    title: gt('Forward appointment'),
    focus: '.add-participant.tt-input',
    async: true
  })
    .build(function () {
      this.model = new Backbone.Model({
        comment: '',
        collection: new AttendeeCollection(null, { resolveGroups: true })
      })
      const { dateStr, timeStr } = getDateTimeIntervalMarkup(appointmentData, { output: 'strings', zone: moment().tz() })
      const recurrenceString = getRecurrenceString(appointmentData)
      const guid = _.uniqueId('label-')

      const addView = new AddParticipantsView({
        apiOptions: {
          contacts: true,
          users: true,
          groups: true,
          resources: false,
          distributionlists: true
        },
        convertToAttendee: true,
        processRaw: true,
        placeholder: gt('Invite participant'),
        collection: this.model.get('collection'),
        // filter out participants that are already there
        blocklist: (attendees || []).map(attendee => attendee.email).join(',')
      })

      const attendeeContainerView = new AttendeeContainer({
        collection: this.model.get('collection'),
        baton: { model: this.model },
        entryClass: 'col-xs-12 col-sm-12',
        labelClass: 'sr-only',
        asHtml: true,
        noEmptyLabel: true,
        showOptionalButton: false,
        showHeadlines: false,
        showAvatar: true,
        showParticipationStatus: false
      })

      this.$body.addClass('forward-invitation-dialog').append(
        $('<div class="mb-16">').text(gt("Use the 'Forward Appointment' feature to share appointment invitations with others. Please note that the meeting organizer must approve any new attendees you invite.")),
        $('<div>').append(
          $('<div class="text-bold">').text(summary),
          $('<div>').text(`${dateStr}${recurrenceString !== '' ? ' \u2013 ' + recurrenceString : ''} ${timeStr}`)
        ),
        addView.render().$el,
        attendeeContainerView.render().$el.addClass('overflow-hidden'),
        $(`<label class="mt-16" for="${guid}">`).text(gt('Add a message to the invitation email for the new participants.')),
        new TextView({ name: 'comment', model: this.model, placeholder: '', autocomplete: false }).render().$el.addClass('comment').attr('id', guid)
      )
    })
    .addCancelButton()
    .addButton({ action: 'ok', label: gt('Send invitations'), className: 'btn-primary' })
    .on('ok', function () {
      if (this.model.get('collection').length < 1) {
        yell('info', gt('You need to invite at least one participant.'))
        return this.idle()
      }

      calendarAPI.forward({
        id,
        folder,
        recurrenceId: action === 'series' ? false : recurrenceId,
        comment: this.model.get('comment'),
        recipients: this.model.get('collection').map(attendee => attendee.pick('uri', 'cn', 'email', 'entity'))
      }).then(function () {
        yell('success', gt('Invitations sent.'))
      }, yell)
      this.close()
    })
    .open()
}
