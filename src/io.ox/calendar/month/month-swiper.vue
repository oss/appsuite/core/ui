<!--

  @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
  @license AGPL-3.0

  This code is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.

  Any use of the work other than as authorized under this license or copyright law is prohibited.

-->

<template>
  <SwiperElement
    v-slot="slotProps"
    :slides="months"
    :initial-slide="initialSlide"
    @swiperbeforeinit="onBeforeInit"
    @swipertransitionend="onTransitionEnd"
    @swiperactiveindexchange="onActiveIndexChange"
    ref="container"
    spaceBetween="30"
    :a11y="false"
  >
    <table class="month vue">
      <thead>
        <tr>
          <th
            v-for="day in days"
            :key="day"
            :class="{ weekend: weekendWeekdays.includes(day+1) }"
          >
            {{ weekdays[day] }}
          </th>
        </tr>
      </thead>
      <tbody>
        <tr
          class="week"
          v-for="(week, rowIndex) in slotProps.data.weeks"
          :key="rowIndex"
          :style="{ height: (100 / slotProps.data.weeks.length) + '%' }"
        >
          <td
            v-for="(day, dateIndex) in week"
            :key="dateIndex"
            :class="{
              out: day.month !== slotProps.data.startOfMonth.month,
              today: today.hasSame(day, 'day') && day.month === slotProps.data.startOfMonth.month
            }"
            class="day"
            :id="cellId(slotProps.data.startOfMonth, day)"
          >
            <MonthCell
              :day="day"
              :height="height"
              :tabindex="slotProps.data.slideTitle === currentSlide ? 0 : -1"
              :numberOfWeeks="slotProps.data.weeks.length"
              :class="{
                today: today.hasSame(day, 'day') && day.month === slotProps.data.startOfMonth.month,
                out: day.month !== slotProps.data.startOfMonth.month
              }"
            />
          </td>
        </tr>
      </tbody>
    </table>
  </SwiperElement>
</template>

<script setup>
import { ref, watch } from 'vue'
import { useElementSize } from '@vueuse/core'
import { Info } from 'luxon'

import { storeToRefs } from 'pinia'
import { useCalendarStore } from '@/io.ox/calendar/calendar-store'

import SwiperElement from '@/io.ox/vueComponents/swiper-element.vue'
import MonthCell from '@/io.ox/calendar/month/month-cell.vue'

const store = useCalendarStore()
const { selectedDate, activeLayout, headerTitle, today } = storeToRefs(store)

const months = generateSlides({ startDate: selectedDate.value.startOf('month').minus({ months: 1 }), numMonths: 3 })
const initialSlide = Math.floor(months.length / 2)

watch(selectedDate, newDate => slideToSelectedMonth(newDate))

const cellId = (startOfMonth, day) => {
  return startOfMonth.toFormat("'slide-month-'yyyy-MM'-") + day.toFormat("'day-'yyyy-MM-dd")
}

// slide generation

let swiper
let currentMonthStart
const currentSlide = ref(null)

function onBeforeInit (e) {
  swiper = e.detail[0]
}

/**
 * Generates data for the swiper.js slides based on the given start date and following number of months.
 * Retrieves all appointments for all visible days in those month slides.
 * @param {Object} options - The options object.
 * @param {Luxon.DateTime} options.startDate - The starting date from which slides are generated from.
 * @param {number} options.numMonths - The number of months following the start date for which to generate slides.
 * @returns {Array<Object>} An array of objects representing each slide and its data.
 */
function generateSlides ({ startDate, numMonths }) {
  return Array(numMonths).fill().map((el, months) => {
    const startOfMonth = startDate.plus({ months }).startOf('month')
    const slideTitle = startOfMonth.toFormat('MMMM yyyy')

    const start = startDate.plus({ months }).startOf('month').startOf('week', { useLocaleWeeks: true })
    const end = startDate.plus({ months }).endOf('month').endOf('week', { useLocaleWeeks: true })

    store.getAppointments({ start, end })
    return {
      data: {
        startOfMonth,
        slideTitle,
        weeks: weeksOfMonth(startOfMonth)
      },
      id: `slide-month-${startOfMonth.toFormat('yyyy-MM')}`
    }
  })
}

const container = ref(null) // SwiperElement
const { height } = useElementSize(container)

// table generation

const weekdays = Info.weekdays('short')
const weekendWeekdays = Info.getWeekendWeekdays()
const firstDayOfWeek = today.value.loc.weekSettings.firstDay - 1
const days = Array.from({ length: 7 }, (_, i) => (i + firstDayOfWeek) % 7)

const onTransitionEnd = () => {
  const slides = swiper.virtual.slides
  currentMonthStart = slides[swiper.activeIndex].data.startOfMonth
  let newSlides = []

  if (swiper.isEnd) {
    newSlides = generateSlides({ startDate: currentMonthStart.plus({ months: 1 }), numMonths: 1 })
    swiper.virtual.appendSlide(newSlides)
    swiper.virtual.removeSlide(0)
  }

  if (swiper.isBeginning) {
    newSlides = generateSlides({ startDate: currentMonthStart.minus({ months: 1 }), numMonths: 1 }).reverse()
    swiper.virtual.prependSlide(newSlides)
    swiper.virtual.removeSlide(swiper.virtual.slides.length - 1)
  }

  currentSlide.value = currentMonthStart.toFormat('MMMM yyyy')
  selectedDate.value = currentMonthStart
}

const onActiveIndexChange = () => {
  const slides = swiper.virtual.slides
  headerTitle.value = slides[swiper.activeIndex].data.slideTitle
}

function slideToSelectedMonth (date) {
  let index = swiper.virtual.slides.findIndex(slide => slide.data.startOfMonth.hasSame(date, 'month'))

  if (index !== 1) {
    swiper.virtual.slides = generateSlides({ startDate: date.startOf('month').minus({ days: 1 }), numMonths: 3 })
    index = swiper.virtual.slides.findIndex(slide => slide.data.startOfMonth.hasSame(date, 'month'))
  }

  // wait for page animation of 150ms when changing views, to prevent flickering
  setTimeout(() => {
    swiper.virtual.update(true)
    swiper.slideTo(index)
    headerTitle.value = swiper.virtual.slides[swiper.activeIndex].data.slideTitle
  }, activeLayout.value === 'month' ? 0 : 150)
}

/**
 * Calculates the weeks and associated days of a given month.
 * @param {Luxon.DateTime} startOfMonth - The month for which to calculate weeks.
 * @returns {Array<Array<Luxon.DateTime>>} An array of arrays representing weeks, each containing the associated days as DateTime objects.
 */
function weeksOfMonth (startOfMonth) {
  const firstDay = startOfMonth.startOf('week', { useLocaleWeeks: true })
  const lastDay = startOfMonth.endOf('month').endOf('week', { useLocaleWeeks: true })

  const weeks = []
  let currentDay = firstDay

  while (currentDay <= lastDay) {
    const week = Array.from({ length: 7 }, (_, days) => currentDay.plus({ days }))
    weeks.push(week)

    currentDay = currentDay.plus({ week: 1 })
  }

  return weeks
}

</script>

<style lang="scss" scoped>
.month {

  thead {
    display: table-header-group;

    tr {
      height: 36px;

      th {
        text-align: center;
        color: var(--text-gray);
        font-size: 14px;
        font-weight: normal;

        &.weekend {
          color: var(--text);
          display: table-cell;
        }
      }
    }
  }

  .week {
    border-width: 0;

    .day {
      border-top: solid lightgray 1px;
      border-right: none;
      text-align: center;

      &:active { background-color: var(--selected-background); }

      .day-button {
        display: flex;
        flex-direction: column;
        align-items: center;
        height: 100%;
        width: 100%;
      }

      .more {
        font-size: 12px;
        text-align: center;
        color: var(--text);
        height: 16px;
        margin-bottom: 1px;
      }

      &.out {
        background: unset;
        color: var(--text);
      }

      .number {
        position: static;
        color: var(--text-dark);
        font-size: 14px;
        font-weight: 400;
        align-items: center;
        margin: 4px 0;
        padding: 4px;
      }

      &.today .number {
        color: var(--focus);
        background-color: var(--today);
        margin: 4px 12px;
        border-radius: 8px;
      }

      .appointment-list {
        width: 100%;

        .list {
          display: flex;
          flex-wrap: wrap;
          overflow: hidden;
          margin: 0 2px;
          font-weight: initial;
        }
      }

      .appointment-indicator {
        display: none;
        color: var(--unseen);
        font-weight: initial;
        flex-grow: 1;
        justify-content: center;
        align-items: center;
      }
    }
  }

  // reduce margins for very small screens
  @media (max-height: 600px)  {
    .week .day {
      .appointment-list { display: none; }
      .appointment-indicator {
        display: flex;
      }

      &.today .number { margin-block: 0px; }

      .number { margin: 0; }
    }
  }
}
</style>

<style lang="scss">
.month-container .day-button .appointment {
  display: inline-block;
  text-align: center;
  border-left: unset;
  width: 100%;
  border-radius: 4px;
  line-height: 16px;

  .appointment-content {
    justify-content: center;
    padding: 0;

    .title {
      margin-right: 0;
      text-overflow: unset;
      font-weight: unset;
    }
  }
}
</style>
