/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import ext from '@/io.ox/core/extensions'
import ox from '@/ox'
import gt from 'gettext'
import Stage from '@/io.ox/core/extPatterns/stage'
import { Action } from '@/io.ox/backbone/views/actions/util'
import { openDialog } from '@/io.ox/wizards/pwa-install-instructions/dialog'
import { ensureInitialConfig, isActive, setLastAutoopen, disable, mergeConfig } from '@/io.ox/core/autoopen'

// `manifest.json` loads this for browsers that support our PWA. On these browsers
// a settings or launcher option is created, for entering the PWA installation
// wizard.  Note that `openDialog` is imported here already. Browsers that don't
// support our PWA still need the `openDialog` function for example when App Suite
// is opened by scanning a qr code that starts the wizard.

ext.point('io.ox/topbar/settings-dropdown').extend({
  id: 'pwa-install-instructions',
  index: 5010,
  render () {
    this.link(
      'pwa-install-instructions',
      // #. %1$s is the product name
      gt('Install %1$s like an app', ox.serverConfig.productName) + ' \u2026',
      () => openDialog({ showRemindMe: false })
    )
  }
})

ext.point('io.ox/core/appcontrol/sideview').extend({
  id: 'pwa-install-instructions',
  index: 70,
  draw: function () {
    this.$el.append(this.createMenuButton({
      device: 'smartphone',
      name: 'pwa-instructions-wizard',
      // #. %1$s is the product name
      text: gt('Install %1$s like an app', ox.serverConfig.productName),
      icon: 'phone',
      action: () => openDialog({ showRemindMe: false })
    }))
  }
})

// eslint-disable-next-line no-new
new Stage('io.ox/core/stages',
  {
    id: 'session-handover',
    index: 1100,
    run (baton) {
      if (!_.url.hash('pwalogin')) return
      _.url.hash('pwalogin', null)
      if (_.device('desktop')) return

      const { id } = baton.extension
      openDialog({ showRemindMe: false })
      baton.data.popups.push({ name: id })
    }
  }, {
    id: 'pwa-install-instructions',
    index: 1200,
    run (baton) {
      if (_.device('smartphone || !supportsStandalone')) return
      if (baton.data.popups.length > 0) return

      const { id } = baton.extension
      ensureInitialConfig(id, { cooldown: 1, remaining: 1 })
      if (!isActive(id)) return

      baton.data.popups.push({ name: id })
      openDialog({ showRemindMe: false }).on('open', () => {
        setLastAutoopen(id)
      }).on('dismiss', () => {
        disable(id)
      }).on('reminder', () => {
        mergeConfig(id, { remaining: 1 })
      })
    }
  }, {
    id: 'pwa-install-instructions-mobile',
    index: 1200,
    run (baton) {
      if (_.device('!smartphone || !supportsStandalone || standalone')) return

      // do not show mobile banner when session-handover was used
      if (baton.data.popups.find(item => item.name === 'session-handover')) return

      const { id } = baton.extension
      ensureInitialConfig(id, { cooldown: 1, remaining: 3 })
      if (!isActive(id)) return

      import('@/io.ox/wizards/pwa-install-instructions/banner').then(({ showPWABanner }) => {
        const node = showPWABanner()
        node.on('remove', () => setLastAutoopen(id))
      })
    }
  }
)

Action('io.ox/wizards/pwa-install-instructions', {
  action () { openDialog() }
})
