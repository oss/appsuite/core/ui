/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import gt from 'gettext'
import $ from '@/jquery'
import ox from '@/ox'
import { buttonWithIcon, createButton, createIcon, showBanner } from '@/io.ox/core/components'

export function showPWABanner () {
  // If the reload banner is pressent, keep it!
  if ($('#io-ox-banner').find('button.reload').length) return

  const closeButton = buttonWithIcon({
    icon: createIcon('bi/x-lg.svg'),
    title: gt('Close'),
    ariaLabel: gt('Close'),
    className: 'btn btn-unstyled py-auto px-16'
  }).on('click', removeBanner)

  const installButton = createButton({ text: gt('Install') })
    .addClass('my-auto text-start mx-16')
    .on('click', async () => {
      if (ox.deferredInstallPrompt?.prompt) return ox.deferredInstallPrompt.prompt()

      const { openDialog } = await import('@/io.ox/wizards/pwa-install-instructions/dialog')
      return openDialog()
    })

  const bannerContainer = showBanner({
    className: 'flex flex-row',
    content: [
      closeButton,
      $('<div class="text-start flex items-center flex-grow">').append(
        $('<p class="m-0">').text(gt('Install %1$s like an app', ox.serverConfig.productName))
      ),
      installButton
    ]
  })

  function removeBanner () {
    bannerContainer.slideUp().empty().removeClass().trigger('remove')
  }

  return bannerContainer
}
