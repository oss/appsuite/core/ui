/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import { createApp, defineAsyncComponent } from 'vue'
import gt from 'gettext'
import ox from '@/ox'
import ModalDialog from '@/io.ox/backbone/views/modal'

export function openDialog ({ showRemindMe = true } = {}) {
  return new ModalDialog({
    className: 'pwa-install-instructions-modal modal flex',
    // #. %1$s is the product name
    title: gt('Instructions for installing %1$s like an app', ox.serverConfig.productName)
  })
    .build(function () {
      this.$header.hide()
      this.$footer.hide()

      const modules = [
        // Order is important here
        { device: 'iOS && safari', module () { return import('@/io.ox/wizards/pwa-install-instructions/instructions/instructions-ios.vue') } },
        { device: 'safari', module () { return import('@/io.ox/wizards/pwa-install-instructions/instructions/instructions-safari-desktop.vue') } },
        { device: 'desktop && edge', module () { return import('@/io.ox/wizards/pwa-install-instructions/instructions/instructions-edge.vue') } },
        { device: 'chrome', module () { return import('@/io.ox/wizards/pwa-install-instructions/instructions/instructions-chrome.vue') } },
        // Fallback:
        { device: '', module () { return import('@/io.ox/wizards/pwa-install-instructions/instructions/instructions-fallback.vue') } }
      ]

      const module = modules.find(({ device }) => _.device(device)).module
      const component = defineAsyncComponent(module)

      const app = createApp(component, {
        close: () => {
          this.trigger('dismiss')
          this.close()
        },
        remindMe: () => {
          this.trigger('reminder')
          this.close()
        },
        showRemindMe
      })
      app.mount(this.el.querySelector('.modal-body'))
      // wait a bit for vue to render and update dom to avoid flickering
      setTimeout(() => this.open(), 100)
    })
}
