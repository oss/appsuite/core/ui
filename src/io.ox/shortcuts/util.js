/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'

export function isObject (value) {
  return typeof value === 'object' && value?.constructor === Object
}

// deep merge two objects
// Object.assign only does a shallow merge
export function mergeObjects (obj1, obj2) {
  const merged = structuredClone(obj1)
  Object.entries(obj2).forEach(([key, value]) => {
    if (merged[key] && !isObject(value)) console.warn('Key collision detected, overwriting', key)
    merged[key] = merged[key] || {}
    if (isObject(value)) merged[key] = mergeObjects(merged[key], value)
    else merged[key] = value
  })
  return merged
}

export function arrayToTree (array) {
  let tree = array.pop()
  let subtree
  array.reverse().forEach(item => {
    subtree = tree
    tree = {}
    tree[item] = subtree
  })
  return tree
}

export function parseShortcut (shortcut) {
  const keys = shortcut.split('+')
  const key = keys.pop()
  const modifiers = keys.filter(key => ['Meta', 'Alt', 'Control', 'Shift'].includes(key)).sort()
  return { key, modifiers }
}

export function resolveCommandOrControl (keyCombo) {
  if (_.device('macos')) return keyCombo.replace('CommandOrControl', 'Meta')
  else return keyCombo.replace('CommandOrControl', 'Control')
}

export function normalizeModifiers (keyCombo) {
  keyCombo = resolveCommandOrControl(keyCombo)
  const { key, modifiers } = parseShortcut(keyCombo)
  return [...modifiers, key].join('+')
}

// create all possible key combos from event
// format is [Meta+][Alt+][Control+][Shift+]key
// this gets a bit tricky for shift and alt keys,
// eg. Shift+1 is ! (on a German keyboard), and Alt+1 is ¹
// so Shift+1 and ! should map to the same shortcut, eg Alt+Shift+1 is not the same as Alt+!
export function keyEventToStrings (e) {
  const key = e.key
  const code = e.code
  const modifiers = {
    Alt: e.altKey,
    Control: e.ctrlKey,
    Meta: e.metaKey,
    Shift: e.shiftKey
  }
  const modifiersA = Object.entries(modifiers)
    .filter(([modifier, pressed]) => pressed)
    .map(([modifier, pressed]) => modifier)
    .sort()

  return {
    key: [...modifiersA, key].join('+'),
    shiftlessKey: [...modifiersA.filter(mod => mod !== 'Shift'), key].join('+'),
    code: [...modifiersA, code].join('+')
  }
}

//
// Transform shortcuts into tree format:
//  {
// gmail:
//    'io.ox/mail': {
//      'New mail': ['a c']
//    }
//  }
//
//  =>
//
//  {
//    'io.ox/mail': {
//      Shift+a : {
//        c: 'New mail'
//      }
//    }
//  }
//
// also:
// - Resolve CommandOrControl => Meta if Macos, Control otherwise
// - Normalize modifier format: [Alt+][Control+][Meta+][Shift+]key
//
export function transformProfile (profiles) {
  const tree = {}
  Object.entries(profiles).forEach(([profileName, profile]) => {
    tree[profileName] = {}
    Object.entries(profile).forEach(([app, shortcuts]) => {
      tree[profileName][app] = {}
      Object.entries(shortcuts).forEach(([action, sequences]) => {
        sequences.forEach(sequence => {
          const keycombos = sequence.split(' ').map(normalizeModifiers)

          keycombos.push(action)
          const subtree = arrayToTree(keycombos)
          tree[profileName][app] = mergeObjects(tree[profileName][app], subtree)
        })
      })
    })
  })
  return tree
}
