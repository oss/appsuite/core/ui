/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ox from '@/ox'
import $ from 'jquery'
import '@/io.ox/shortcuts/helpModal'
import ext from '@/io.ox/core/extensions'
import { settings as coreSettings } from '@/io.ox/core/settings'
import '@/io.ox/shortcuts/settings/pane'
import '@/io.ox/shortcuts/navigationActions'
import { getUnifiedProfile, validateProfileName } from '@/io.ox/shortcuts/profile'
import { transformProfile } from '@/io.ox/shortcuts/util'
import { openModals } from '@/io.ox/backbone/views/modal'

let currentProfile = coreSettings.get('shortcutsProfile', 'default')
coreSettings.on('change:shortcutsProfile', newProfile => {
  currentProfile = validateProfileName(newProfile)
  resetPointer()
})

const profile = getUnifiedProfile()
const tree = transformProfile(profile)

let pointer
let appId = ''
ox.on('app:ready app:resume', () => {
  if (!ox.ui.App.getCurrentApp()) return
  appId = ox.ui.App.getCurrentApp().get('id')
  resetPointer()
})

function resetPointer () {
  // if `tree[currentProfile][appId] === undefined` we're in an unrecognized app (eg. documents), best to just ignore keyboard inputs
  // this also unfortunately includes legitimate apps, eg. mobile email compose
  pointer = tree[currentProfile][appId] || {}
  clearTimeout(timeoutId)
}

const allowedInSettings = [
  'Open Mail',
  'Open Calendar',
  'Open Address Book',
  'Open Portal',
  'Open Tasks',
  'Open Drive',
  'Show shortcut help',
  'Focus search'
]

function runAction (action, e) {
  // disable shortcuts when modals are open, except settings which has some allowed shortcuts
  const topModal = openModals.queue.slice(-1)[0]
  if (topModal && !(topModal.point.id === 'io.ox/core/settings/dialog' && allowedInSettings.includes(action))) return

  ext.point('io.ox/shortcuts')
    .filter(ext => ext.action === action)
    .forEach(ext => ext.perform({ e }))
}

export function handleKeydown (e) {
  // ignore if we're in an input
  if (!e.ctrlKey && !e.metaKey) {
    const target = e.originalEvent.composedPath()[0]
    if (/^(INPUT|TEXTAREA|SELECT)$/.test(target.tagName)) return
    if (target.isContentEditable) return
  }

  // clear sequence after a short delay so old sequences don't hang around
  clearTimeout(timeoutId)
  timeoutId = setTimeout(resetPointer, 500)

  // if key is a modifier, ignore
  if (['Shift', 'Control', 'Alt', 'Meta'].includes(e.key)) return

  // there are a number of ways to describe a key
  const { key, shiftlessKey, code } = keyEventToStrings(e)

  // move down the tree until we find a string value
  // if we find a string value, we have a shortcut
  // if we find an object, we have to go deeper (wait for next keypress)
  // if we find undefined, reset
  pointer = pointer[key] || pointer[shiftlessKey] || pointer[code]
  if (!pointer) resetPointer() // reset pointer if we don't find a match
  if (typeof pointer === 'string') {
    // execute shortcut action
    runAction(pointer, e)

    // sequence completed, reset pointer
    resetPointer()
  }
}

let timeoutId
$(document).on('keydown', e => handleKeydown(e))

// create all possible key combos from event
// format is [Meta+][Alt+][Control+][Shift+]key
// this gets a bit tricky for shift and alt keys,
// eg. Shift+1 is ! (on a German keyboard), and Alt+1 is ¹
// so Shift+1 and ! should map to the same shortcut, eg Alt+Shift+1 is not the same as Alt+!
export function keyEventToStrings (e) {
  const key = e.key
  const code = e.code
  const modifiers = {
    Alt: e.altKey,
    Control: e.ctrlKey,
    Meta: e.metaKey,
    Shift: e.shiftKey
  }
  const modifiersA = Object.entries(modifiers)
    .filter(([modifier, pressed]) => pressed)
    .map(([modifier, pressed]) => modifier)
    .sort()

  return {
    key: [...modifiersA, key].join('+'),
    shiftlessKey: [...modifiersA.filter(mod => mod !== 'Shift'), key].join('+'),
    code: [...modifiersA, code].join('+')
  }
}
