/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import Backbone from '@/backbone'

import { settings } from '@/io.ox/onboarding/settings'
import gt from 'gettext'

const appNames = {
  mail: gt.pgettext('app', 'Mail'),
  calendar: gt.pgettext('app', 'Calendar (CalDav)'),
  contacts: gt.pgettext('app', 'Address Book (CardDav)')
}

export const productNames = {
  mail: settings.get('productNames/mail', gt.pgettext('native app', 'OX Mail')),
  drive: settings.get('productNames/drive', gt.pgettext('native app', 'OX Drive'))
}

export const titles = {
  windows: {
    title: gt('Windows'),
    mailsync: appNames.mail,
    addressbook: appNames.contacts,
    calendar: appNames.calendar,
    drive: productNames.drive
  },
  android: {
    title: gt('Android'),
    mailsync: appNames.mail,
    mailapp: productNames.mail,
    addressbook: appNames.contacts,
    calendar: appNames.calendar,
    driveapp: productNames.drive,
    eassync: gt('EAS'),
    syncapp: gt('OX Sync App')
  },
  macos: {
    title: gt('macOS'),
    // #. will be shown in the onboarding wizard e.g. "Mail, Contacts & Calendar" referencing the app names
    mailCalendarAddressbook: gt('%1$s, %2$s & %3$s', appNames.mail, appNames.contacts, appNames.calendar),
    mailsync: gt('Apple Mail'),
    addressbook: appNames.contacts,
    calendar: appNames.calendar,
    drive: productNames.drive
  },
  ios: {
    title: gt('iOS'),
    // #. will be shown in the onboarding wizard e.g. "Mail, Contacts & Calendar" referencing the app names
    mailCalendarAddressbook: gt('%1$s, %2$s & %3$s', appNames.mail, appNames.contacts, appNames.calendar),
    mailsync: gt('iOS Mail'),
    mailapp: productNames.mail,
    addressbook: appNames.contacts,
    calendar: appNames.calendar,
    driveapp: productNames.drive,
    eassync: gt('EAS')
  }
}

export const platformList = new Backbone.Collection([
  {
    title: gt('Windows PC'),
    icon: 'bi/windows.svg',
    platform: 'windows'
  },
  {
    title: gt('Android phone or tablet'),
    icon: 'bi/android2.svg',
    platform: 'android'
  },
  {
    title: gt('macOS'),
    icon: 'bi/apple.svg',
    platform: 'macos'
  },
  {
    title: gt('iPhone or iPad'),
    icon: 'bi/apple.svg',
    platform: 'ios'
  }
])

export const appList = new Backbone.Collection([
  // Windows
  {
    title: appNames.mail,
    icon: 'bi/ox-mail.svg',
    app: 'mailsync',
    platform: 'windows',
    cap: 'webmail'
  },
  {
    title: appNames.contacts,
    icon: 'bi/ox-address-book.svg',
    app: 'addressbook',
    platform: 'windows',
    cap: 'carddav'
  }, {
    title: appNames.calendar,
    icon: 'bi/ox-calendar.svg',
    app: 'calendar',
    platform: 'windows',
    cap: 'caldav'
  },
  {
    title: productNames.drive,
    icon: 'bi/cloud.svg',
    app: 'drive',
    platform: 'windows',
    cap: 'drive'
  },
  // Android
  {
    title: gt('Email with Android Mail'),
    icon: 'bi/ox-mail.svg',
    app: 'mailsync',
    platform: 'android',
    cap: 'webmail'
  },
  {
    // #. 1$s product name of mail application, e.g. OX Mail
    title: gt('Email with %1$s', productNames.mail),
    icon: 'bi/ox-mail.svg',
    app: 'mailapp',
    platform: 'android',
    cap: 'mobile_mail_app'
  },
  {
    title: appNames.contacts,
    icon: 'bi/ox-address-book.svg',
    app: 'addressbook',
    platform: 'android',
    cap: 'carddav'
  },
  {
    title: appNames.calendar,
    icon: 'bi/ox-calendar.svg',
    app: 'calendar',
    platform: 'android',
    cap: 'caldav'
  },
  {
    title: productNames.drive,
    icon: 'bi/cloud.svg',
    app: 'driveapp',
    platform: 'android',
    cap: 'drive'
  },
  {
    title: gt('Exchange Active Sync'),
    icon: 'bi/ox-address-book.svg',
    app: 'eassync',
    platform: 'android',
    cap: 'active_sync'
  },
  {
    title: gt('OX Sync App'),
    icon: 'bi/ox-address-book.svg',
    app: 'syncapp',
    platform: 'android',
    cap: 'carddav caldav'
  },
  // macOS
  {
    // #. will be shown in the onboarding wizard e.g. "Mail, Contacts & Calendar" referencing the app names
    title: gt('%1$s, %2$s & %3$s', appNames.mail, appNames.contacts, appNames.calendar),
    icon: 'bi/star.svg',
    app: 'mailCalendarAddressbook',
    platform: 'macos',
    cap: 'webmail caldav carddav'
  },
  {
    title: appNames.mail,
    icon: 'bi/ox-mail.svg',
    app: 'mailsync',
    platform: 'macos',
    cap: 'webmail'
  },
  {
    // #.
    title: appNames.contacts,
    icon: 'bi/ox-address-book.svg',
    app: 'addressbook',
    platform: 'macos',
    cap: 'carddav'
  },
  {
    title: appNames.calendar,
    icon: 'bi/ox-calendar.svg',
    app: 'calendar',
    platform: 'macos',
    cap: 'caldav'
  },
  {
    title: productNames.drive,
    icon: 'bi/cloud.svg',
    app: 'drive',
    platform: 'macos',
    cap: 'drive'
  },
  // iOS
  {
    // #. will be shown in the onboarding wizard e.g. "Mail, Contacts & Calendar" referencing the app names
    title: gt('%1$s, %2$s & %3$s', appNames.mail, appNames.contacts, appNames.calendar),
    icon: 'bi/star.svg',
    app: 'mailCalendarAddressbook',
    platform: 'ios',
    cap: 'webmail caldav carddav'
  },
  {
    title: appNames.mail,
    icon: 'bi/ox-mail.svg',
    app: 'mailsync',
    platform: 'ios',
    cap: 'webmail'
  },
  {
    // #. 1$s product name of mail application, e.g. OX Mail
    title: gt('Email with %1$s', productNames.mail),
    icon: 'bi/ox-mail.svg',
    app: 'mailapp',
    platform: 'ios',
    cap: 'mobile_mail_app'
  },
  {
    title: appNames.contacts,
    icon: 'bi/ox-address-book.svg',
    app: 'addressbook',
    platform: 'ios',
    cap: 'carddav'
  },
  {
    title: appNames.calendar,
    icon: 'bi/ox-calendar.svg',
    app: 'calendar',
    platform: 'ios',
    cap: 'caldav'
  },
  {
    title: productNames.drive,
    icon: 'bi/cloud.svg',
    app: 'driveapp',
    platform: 'ios',
    cap: 'drive'
  },
  {
    title: gt('Exchange Active Sync'),
    icon: 'bi/ox-address-book.svg',
    app: 'eassync',
    platform: 'ios',
    cap: 'active_sync'
  }
])

// hide specific apps by jslob setting
const appListHidden = settings.get('hidden/apps', ['syncapp'])
appList.remove(
  appList.filter(function (model) {
    return appListHidden.indexOf(model.get('app')) > -1
  })
)
