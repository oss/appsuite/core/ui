/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import jobsAPI from '@/io.ox/core/api/jobs'
import $ from '@/jquery'
import gt from 'gettext'

let deleteJobs = []
const minMessageDuration = 3000

// #. Present progressive, indicating an ongoing or current action.
const deleteMessage = $('<div class="bottom-message delete-notification text-gray">').text(gt('Deletion in progress…'))

function jobFinished (id) {
  deleteJobs = deleteJobs.filter(jobId => jobId !== id)
  removeMessage()
}

let timer = false

function removeMessage () {
  if (deleteJobs.length > 0 || timer === true) return
  deleteMessage.detach()
}

export function showMessage () {
  if (timer) return
  timer = true
  $('#io-ox-message-container').prepend(deleteMessage)
  setTimeout(() => {
    timer = false
    removeMessage()
  }, minMessageDuration)
}

export function addDeleteJob (id) {
  if (id === undefined || deleteJobs.includes(id)) return
  deleteJobs.push(id)
  jobsAPI.once(`finished:${id}`, () => jobFinished(id))
  showMessage()
}
