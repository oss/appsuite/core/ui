/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import * as extensions from '@/io.ox/core/folder/contextmenu-extensions'
import contextUtils from '@/io.ox/backbone/mini-views/contextmenu-utils'

ext.point('io.ox/core/foldertree/contextmenu/default').extend({
  id: 'move',
  index: 1200,
  draw: extensions.move
}, {
  id: 'moveDrive',
  index: 1250,
  draw: extensions.moveDrive
}, {
  id: 'shares',
  index: 2000,
  draw: extensions.shares
}, {
  id: 'import',
  index: 3100,
  draw: extensions.importData
}, {
  id: 'export',
  index: 3200,
  draw: extensions.exportData
}, {
  id: 'zip',
  index: 3300,
  draw: extensions.zip
}, {
  // -----------------------------------------------
  id: 'divider-4',
  index: 3400,
  draw: contextUtils.divider
}, {
  id: 'refresh-calendar',
  index: 6100,
  draw: extensions.refreshCalendar
}, {
  id: 'select-only',
  index: 6200,
  draw: extensions.selectOnly
}, {
  id: 'toggle',
  index: 6300,
  draw: extensions.toggle
}, {
  id: 'showInDrive',
  index: 6400,
  draw: extensions.showInDrive
}, {
  // -----------------------------------------------
  id: 'divider-6',
  index: 6600,
  draw: contextUtils.divider
}, {
  id: 'properties',
  index: 6700,
  draw: extensions.properties
})
