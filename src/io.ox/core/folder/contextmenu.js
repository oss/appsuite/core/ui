/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import * as extensions from '@/io.ox/core/folder/contextmenu-extensions'
import contextUtils from '@/io.ox/backbone/mini-views/contextmenu-utils'

ext.point('io.ox/core/foldertree/contextmenu/myfolders').extend({
  id: 'add-folder',
  index: 100,
  render: extensions.add
}, {
  // -----------------------------------------------
  id: 'divider-1',
  index: 200,
  render: contextUtils.divider
}, {
  id: 'add-account',
  index: 300,
  render: extensions.addAccount
})

ext.point('io.ox/core/foldertree/contextmenu/default').extend({
  id: 'add-folder',
  index: 1000,
  draw: extensions.add
}, {
  id: 'rename',
  index: 1100,
  draw: extensions.rename
}, {
  id: 'change-alarms',
  index: 1100,
  draw: extensions.alarms
}, {
  // -----------------------------------------------
  id: 'divider-1',
  index: 1450,
  draw: contextUtils.divider
}, {
  id: 'customColor',
  index: 1500,
  draw: extensions.customColor
}, {
  // -----------------------------------------------
  id: 'divider-2',
  index: 1600,
  draw: contextUtils.divider
}, {
  id: 'deputies',
  index: 2050,
  draw: extensions.deputies
}, {
  // -----------------------------------------------
  id: 'divider-3',
  index: 2100,
  draw: contextUtils.divider
}, {
  id: 'mark-folder-read',
  index: 4100,
  draw: extensions.markFolderSeen
}, {
  id: 'move-all-messages',
  index: 4200,
  draw: extensions.moveAllMessages
}, {
  id: 'expunge',
  index: 4300,
  draw: extensions.expunge
}, {
  id: 'archive',
  index: 4400,
  draw: extensions.archive
}, {
  // -----------------------------------------------
  id: 'divider-5',
  index: 4500,
  draw: contextUtils.divider
}, {
  id: 'refresh-calendar',
  index: 6100,
  draw: extensions.refreshCalendar
}, {
  id: 'select-only',
  index: 6200,
  draw: extensions.selectOnly
}, {
  id: 'empty',
  index: 6500,
  draw: extensions.empty
}, {
  id: 'delete',
  index: 6600,
  draw: extensions.removeFolder
}, {
  id: 'restore',
  index: 6600,
  draw: extensions.restoreFolder
})

export default {
  extensions,
  addLink: contextUtils.addLink,
  disable: contextUtils.disable,
  divider: contextUtils.divider
}
