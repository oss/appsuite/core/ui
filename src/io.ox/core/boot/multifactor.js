/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import session from '@/io.ox/core/session'
import util from '@/io.ox/core/boot/util'

// Do multifactor authentication.  If successful, load full rampup data
export default async function doMultifactor () {
  const { default: auth } = await import('@/io.ox/multifactor/auth')

  try {
    await auth.doAuthentication()
    $('#background-loader').fadeIn(util.DURATION, function () {
      $('#io-ox-login-screen').hide().empty()
    })
  } catch (error) {
    console.error(error)
    console.error('Failed multifactor login. Reloading')
    session.logout().always(function () {
      window.location.reload(true) // Hard fail here. Reload
      throw error
    })
  }
}
