/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import moment from '@/moment'

import ox from '@/ox'
import http from '@/io.ox/core/http'
import util from '@/io.ox/core/boot/util'
import { deepExtend } from '@/io.ox/core/util'
import locale from '@/io.ox/core/boot/locale'
import { gt } from 'gettext'
import support from '@/io.ox/core/boot/support'
import login from '@/io.ox/core/boot/login/standard'
import manifests from '@/io.ox/core/manifests'
import { getSVGLogo } from '@/io.ox/core/theming/util'

/**
 * url params/values (all optional)
 * ================================
 *
 * login_type:      [ 'guest' | 'guest_password' | 'anonymous_password' | 'multifactor' | select_device | 'lost_device' ]
 * login_name:      [ something ]
 *
 * status:          [ 'reset_password' | 'invalid_request' ]
 *
 * message_type:    [ 'INFO' | 'ERROR' ]
 * message:         [ something ]
 *
 * forgot-password: [ something ]
 * share:           [ something ]
 * confirm:         [ something ]
 * autologout:      [ something ]
 */

export default async function (formType) {
  let bindLogin = true

  util.debug('Show form ...')

  function displayMessageContinue () {
    loadLoginLayout({ hideTitle: true, addClass: 'login-type-message' })
    hideFormElements('.username, .password, .options')
  }

  function displayContinue (data) {
    $('#io-ox-login-button').attr('data-i18n', 'Continue').text(gt('Continue'))
    $('#io-ox-login-restoremail, #io-ox-login-username').val(data.login_name || '').prop('readonly', true)
    $('#io-ox-login-password').val('')
  }

  function displayMessageOnly () {
    loadLoginLayout({ hideTitle: true, addClass: 'login-type-message' })
    hideFormElements()
  }

  function hideFormElements (elements) {
    // hide all other inputs
    $('#io-ox-login-form div.row')
      .filter(elements || '.username, .password, .options, .button')
      .hide()
  }

  function resetPassword () {
    loadLoginLayout({ altTitle: gt('Reset password'), newPassword: true, showAlert: true })

    $('#io-ox-login-form').attr({
      action: '/appsuite/api/share/reset/password',
      method: 'post',
      target: '_self'
    }).append(
      $('<input type="hidden" name="share">').val(_.url.hash('share')),
      $('<input type="hidden" name="confirm">').val(_.url.hash('confirm'))
    ).submit(function (e) {
      const pass1 = $.trim($('#io-ox-login-password').val())
      const pass2 = $.trim($('#io-ox-retype-password').val())
      if (pass1.length === 0 || pass2.length === 0) {
        e.preventDefault()
        return util.fail({ error: gt('Please enter your new password.'), code: 'UI-0003' }, 'password')
      }
      if (pass1 !== pass2) {
        e.preventDefault()
        return util.fail({ error: gt('Please enter the same password.'), code: 'UI-0004' }, 'password')
      }
    })
    // remove unused fields
    $('#io-ox-login-form div.row.username').remove()
    $('#io-ox-login-store').remove()
    $('#io-ox-forgot-password').remove()
    // show retype
    $('#io-ox-login-form div.row.password-retype').show()
    // i18n
    $('#io-ox-login-button')
      .attr('data-i18n', 'Set password')
      .text(gt('Set password'))
    bindLogin = false
  }

  function guestLogin () {
    loadLoginLayout({ showAlert: true })

    const loginName = _.url.hash('login_name')
    $('.row.username').hide()
    if (!_.isEmpty(loginName)) {
      $('#io-ox-login-restoremail, #io-ox-login-username').val(loginName).prop('readonly', true)
    }
    $('#io-ox-forgot-password, #io-ox-backtosignin').find('a').click(function (e) {
      e.preventDefault()
      $('#io-ox-resetpassword-button').attr({ 'data-i18n': 'Next' }).text(gt('Next'))
      // If restore email is already populated and readOnly, submit the form
      if ($('#io-ox-login-restoremail, #io-ox-login-username').prop('readOnly')) {
        $('#io-ox-password-forget-form').submit()
      } else {
        $('#io-ox-password-forget-form, #io-ox-login-form').toggle()
      }
    })
    $('#io-ox-password-forget-form').append(
      $('<input type="hidden" name="share">').val(_.url.hash('share'))
    )
  }

  function anonymousLogin () {
    loadLoginLayout({ showAlert: true })

    $('.row.username').hide()
    $('#io-ox-forgot-password').remove()
  }

  function defaultLogin () {
    loadLoginLayout({ showAlert: true })

    // remove form for sharing
    $('#io-ox-password-forget-form').remove()

    if (!ox.serverConfig.forgotPassword) {
      // either not configured or guest user
      $('#io-ox-forgot-password').remove()
      $('#io-ox-login-store').toggleClass('col-sm-6 col-sm-12')
    } else {
      $('#io-ox-forgot-password').find('a').attr('href', ox.serverConfig.forgotPassword)
    }
  }

  function removeUnusedLines (strings, ...args) {
    const replacement = 'uniqueReplacementForUndefinedValues'
    while (args.length < strings.length) args.push('')
    return strings
      .map((part, i) => part + (args[i] === undefined ? replacement : args[i]))
      .join('')
      .split('\n')
      .filter(line => !line.includes(replacement))
      .map(line => line.trim())
      .join('\n')
  }

  function generateCss (loginConfiguration) {
    const { backgroundColor, form, footer, header, topVignette } = loginConfiguration
    let { loginBox, backgroundImage } = loginConfiguration
    loginBox = !['left', 'right'].includes(loginBox) ? 'center' : loginBox
    backgroundImage = !_.device('smartphone') && backgroundImage

    return removeUnusedLines`
      #io-ox-login-container { background: ${backgroundColor}; }
      #io-ox-login-background-image { background: ${backgroundImage}; }
      #io-ox-login-header { background: linear-gradient(rgba(0,0,0,${topVignette?.transparency}),rgba(0,0,0,0)); }
      #io-ox-login-header, #io-ox-login-header #io-ox-languages #io-ox-languages-label { color: ${header?.textColor}; }
      #io-ox-login-header a, #io-ox-login-header .toggle-text, #io-ox-login-header .caret { color: ${header?.linkColor}; }
      #login-title-mobile { color: ${header?.textColor} !important; }
      #box-form-header { color: ${form?.header?.textColor}; }
      #box-form-header { background: ${form?.header?.bgColor}; }
      #box-form-body *:not(button, button>span, .toggle, svg, svg>path) { color: ${form?.textColor}; }
      #box-form-body .checkbox.custom input:checked + .toggle { background-color: ${form?.button?.bgColor}; color: ${form?.button?.textColor}; }
      #box-form-body .checkbox.custom input:focus + .toggle { border-color: ${form?.button?.bgColor}; box-shadow: 0 0 0 0.25rem ${form?.button?.bgColor}40; }
      #box-form-body { color: ${form?.textColor}; }
      #box-form #box-form-body a { color: ${form?.linkColor}; }
      #box-form button, #io-ox-login-button { background-color: ${form?.button?.bgColor}; border-color: ${form?.button?.bgColor}; }
      #box-form button, #io-ox-login-button { border-color: ${form?.button?.borderColor}; }
      #box-form button, #io-ox-login-button { color: ${form?.button?.textColor}; }
      #box-form button svg path, #io-ox-login-button svg path { fill: ${form?.button?.textColor}; }
      #io-ox-login-footer { background: ${footer?.bgColor}; }
      #io-ox-login-footer, #io-ox-login-footer #io-ox-languages .lang-label { color: ${footer?.textColor}; }
      #io-ox-login-footer a, #io-ox-login-footer .toggle-text, #io-ox-login-footer #language-select, #io-ox-login-footer .caret { color: ${footer?.linkColor}; }
      #io-ox-login-content { justify-content: ${loginBox}; }`
  }

  function loadLoginLayout (options) {
    const loginConfiguration = getLoginConfiguration(options)

    // apply login screen specific classes
    if (_.device('smartphone')) {
      $('#io-ox-login-username, #io-ox-login-password, #io-ox-retype-password, #io-ox-login-restoremail')
        .each(function () {
          const text = $(`label[for="${this.id}"]`).attr('data-i18n')
          $(this).attr({
            'data-i18n-attr': 'placeholder',
            'data-i18n': text,
            placeholder: gt(text)
          })
        })
    }
    $('#io-ox-login-screen')
      .addClass(loginConfiguration.addClass)
      .addClass('initialized')

    const toolbar = $('#io-ox-login-toolbar')
    const content = $('#io-ox-login-content')
    const footer = $('#io-ox-login-footer')

    const standardNodes = {
      $logo: $('<img class="login-logo" alt="Logo">').attr('src', loginConfiguration.logo),
      $language: $('<span id="io-ox-languages" class="mx-16">'),
      $spacer: $('<div class="composition-element login-spacer">'),
      $privacy: $('<span>').append(
        $('<a>')
          .attr({
            target: '_blank',
            href: loginConfiguration.footer.privacy,
            'data-i18n': 'Privacy Policy'
          })
          .data('href-translations', getTranslations(loginConfiguration.footer.$privacy))
          .text(gt('Privacy Policy'))),
      $imprint: $('<span>').append(
        $('<a>')
          .attr({
            target: '_blank',
            href: loginConfiguration.footer.imprint,
            'data-i18n': 'Imprint'
          })
          .data('href-translations', getTranslations(loginConfiguration.footer.$imprint))
          .text(gt('Imprint'))),
      $copyright: $('<span>')
        .text((loginConfiguration.footer.copyright || ox.serverConfig.copyright)
          .replace(/\(c\)/g, '\u00A9')
          .replace(/\$year/g, moment().year())),
      $version: $('<span>')
        .text(ox.serverConfig.version || ox.version)
    }

    if (!loginConfiguration.logo) {
      const filename = ox.serverConfig.useOXLogo ? 'ox_logo.svg' : 'logo-dynamic.svg'
      getSVGLogo('./themes/default/' + filename).then(logo => {
        $('.login-logo').replaceWith($(logo).addClass('login-logo default'))
      })
    }

    function getNodes (bucket) {
      return bucket.sorting.split(',').map(function (str) {
        if (!str.length) return undefined
        if (standardNodes[str]) return standardNodes[str].clone(true, true)
        return $('<div class="composition-element">').append(
          str.match(/(\$[a-zA-Z]+|[^$]+)/g).map(function (match) {
            if (standardNodes[match]) return standardNodes[match].clone(true, true)
            if (bucket[match]) return $('<span data-i18n>').data('translations', getTranslations(bucket[match]))
            return $('<span>').text(match)
          })
        )
      })
    }

    function getTranslations (o) {
      return _.isObject(o) ? o : { en_US: o }
    }

    // header and toolbar
    toolbar.append(getNodes(loginConfiguration.header))
    if (_.device('smartphone')) {
      toolbar.append(
        $('<div id="login-title-mobile">')
          .text(loginConfiguration.header.title)
      )
    }

    // teaser and boxposition
    const teaser = $('<div id="io-ox-login-teaser" class="col-sm-6" data-i18n-attr="html" data-i18n>')
      .data('translations', getTranslations(loginConfiguration.teaser))

    if (loginConfiguration.loginBox === 'left' && !_.device('smartphone')) {
      content.append(teaser)
    } else if (loginConfiguration.loginBox === 'right' && !_.device('smartphone')) {
      content.prepend(teaser)
    }

    // form
    $('#box-form-header')
      .text(loginConfiguration.header.title)
      .attr({ 'data-i18n': '', 'data-i18n-attr': 'text' })
      .data('translations', getTranslations(loginConfiguration.header.title))

    if (loginConfiguration.altTitle) {
      $('#login-title')
        .attr({ 'data-i18n': loginConfiguration.altTitle })
        .text(loginConfiguration.altTitle)
    } else if (loginConfiguration.hideTitle) {
      $('#login-title').remove()
    } else {
      $('#login-title')
        .attr({ 'data-i18n': 'Sign in' })
        .text(gt('Sign in'))
    }

    $('#io-ox-login-button')
      .attr({ 'data-i18n': 'Sign in' })
      .text(gt('Sign in'))

    if (loginConfiguration.newPassword) $('#io-ox-login-password').val('')
    if (loginConfiguration.informationMessage) {
      $('#io-ox-information-message')
        .attr({ 'data-i18n': '', 'data-i18n-attr': 'html' })
        .data('translations', getTranslations(loginConfiguration.informationMessage))
    }

    // alert info
    if (options.showAlert) $('#io-ox-login-feedback').addClass('alert-highlight')

    // footer
    footer.append(getNodes(loginConfiguration.footer))
    if (_.device('smartphone')) {
      toolbar.find('#io-ox-languages').remove()
      footer.prepend(standardNodes.$language)
    }

    const localeIsDropUp = loginConfiguration.header.sorting.indexOf('$language') === -1
    locale.render(localeIsDropUp)

    $('head').append(
      // apply styles from server configuration (login page)
      $('<style data-src="login-page-configuration" type="text/css">')
        .text(util.scopeCustomCss(generateCss(loginConfiguration), '#io-ox-login-screen')),
      // apply custom css
      $('<style data-src="login-page-configuration-custom" type="text/css">')
        .text(util.scopeCustomCss(loginConfiguration.customCss, '#io-ox-login-screen'))
    )
  }

  function loadMfaLayout (target) {
    const { multifactorBackground: background } = ox.serverConfig
    $('#io-ox-login-form, #io-ox-password-forget-form').remove()
    $('#io-ox-select-device-form, #io-ox-multifactor-form, #io-ox-lost-device-form')
      .parent().hide()
    if (!$('#io-ox-login-screen').hasClass('initialized')) loadLoginLayout({})
    if (background && !_.device('smartphone')) {
      const el = document.getElementById('multifactor-background')
      el.src = background
      el.style.display = 'block'
    }
    $(target).parent().show()
    util.restore()
  }

  function getLoginConfiguration (options) {
    const configurations = [
      getDefaultConfiguration(),
      ox.serverConfig.loginPage,
      _.device('smartphone') && ox.serverConfig.loginPage?.mobile
    ]
    const loginConfiguration = deepExtend({}, ...configurations, options)
    loginConfiguration.header.title = loginConfiguration.form?.header?.title || ox.serverConfig.productName
    return loginConfiguration
  }

  function getDefaultConfiguration () {
    return {
      header: {
        sorting: '$logo,$spacer,$language'
      },
      footer: {
        sorting: '$spacer,$copyright,Version $version,$privacy,$imprint,$spacer',
        $privacy: 'https://www.open-xchange.com/privacy/',
        $imprint: 'https://www.open-xchange.com/legal/',
        copyright: '(c) $year Open-Xchange GmbH'
      }
    }
  }

  const loginLocation = ox.serverConfig.loginLocation
  let showContinue = false

  switch (formType || _.url.hash('login_type')) {
    case 'guest':
      // fallthrough
    case 'message_continue':
      displayMessageContinue()
      showContinue = true
      break
    // show guest login
    case 'guest_password':
      guestLogin()
      break
    // show anonymous login
    case 'anonymous_password':
      anonymousLogin()
      break
    case 'reset_password':
      resetPassword()
      break
    case 'message':
      displayMessageOnly()
      break
    case 'multifactor':
      loadMfaLayout('#io-ox-multifactor-form')
      break
    case 'select_device':
      loadMfaLayout('#io-ox-select-device-form')
      break
    case 'lost_device':
      loadMfaLayout('#io-ox-lost-device-form')
      break
    default:
      // at this point we know that a "normal" (i.e. non-guest) login is required
      // therefore we finally check if a custom login location is set
      if (loginLocation && loginLocation !== '') return util.gotoSignin()
      defaultLogin()
      break
  }

  const redeem = function (lang) {
    http.GET({
      module: 'share/redeem/token',
      params: { token: _.url.hash('token'), language: lang },
      appendSession: false,
      processResponse: false
    }).done(function (data) {
      if (data.message_type === 'ERROR') {
        util.feedback('error', data.message)
      } else {
        $('#io-ox-login-help').text(data.message)
      }
      if (showContinue) displayContinue(data)
    })
      .fail(function (e) {
        util.feedback('error', e.error)
        if (showContinue) hideFormElements()
      })
  }

  // handle message params
  if (_.url.hash('token')) {
    ox.on('language', redeem)
  }

  // set language select to link color defined by the given configuration
  const loginConfiguration = getLoginConfiguration()

  // update header
  $('#io-ox-login-header-prefix')
    .text(`${ox.serverConfig.pageHeaderPrefix || '\u00A0'} `)
    .removeAttr('aria-hidden')
  $('#io-ox-login-header-label')
    .text(ox.serverConfig.pageHeader || '\u00A0')
    .removeAttr('aria-hidden')

  // update footer
  const revision = 'revision' in ox.serverConfig ? ox.serverConfig.revision : `Rev${ox.revision}`
  const footer = [ox.serverConfig.copyright, ox.serverConfig.version && `Version: ${ox.serverConfig.version}`, revision, ox.serverConfig.buildDate].filter(Boolean).join(' ')
  $('#io-ox-copyright').text(footer.replace(/\(c\)/i, '\u00A9'))

  // check/uncheck?
  const box = $('#io-ox-login-store-box')
  const cookie = _.getCookie('staySignedIn')

  if (cookie !== undefined) {
    box.prop('checked', cookie === 'true')
  } else if ('staySignedIn' in ox.serverConfig) {
    box.prop('checked', !!ox.serverConfig.staySignedIn)
  }
  box.on('change', function () {
    _.setCookie('staySignedIn', $(this).prop('checked'))
  })

  // update productname in password reset dialog
  $('#io-ox-password-forget-form .help-block').text(
    // #. %1$s is the product name, e.g. OX App Suite
    gt('Please enter your email address associated with %1$s. You will receive an email that contains a link to reset your password.', loginConfiguration.header.title)
  )

  util.debug('Set default locale')

  return Promise.all([
    manifests.manager.loadPluginsFor('signin'),
    locale.setDefaultLocale()
  ])
    .finally(function () {
      // autologout message
      if (_.url.hash('autologout')) {
        util.feedback('info', function () {
          return $.txt(gt('You have been automatically signed out'))
        })
      }

      // handle browser support
      support()

      util.debug('Fade in ...')

      if ($('#showstopper').is(':visible')) return

      $('#background-loader').fadeOut(util.DURATION, function () {
        // show login dialog
        $('#io-ox-login-blocker').on('mousedown', false)
        if (bindLogin) $('#io-ox-login-form').on('submit', login)
        $('#io-ox-login-username').prop('disabled', false)
        // focus password or username
        $($('#io-ox-login-username').is(':hidden') ? '#io-ox-login-password' : '#io-ox-login-username').focus().select()
      })
    })
};
