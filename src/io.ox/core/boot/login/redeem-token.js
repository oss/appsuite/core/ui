/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import ox from '@/ox'
import _ from '@/underscore'
import http from '@/io.ox/core/http'
import session from '@/io.ox/core/session'
import util from '@/io.ox/core/boot/util'

import gt from 'gettext'

ext.point('io.ox/core/boot/login').extend({
  id: 'redeemToken',
  index: 410,
  login (baton) {
    const { appId, token } = baton.hash
    if (!appId || !token) return

    _.url.hash({ appId: null, token: null })

    if (!appId) return util.fail({ error: gt('Something went wrong. Maybe the session expired or the token is invalid.'), code: 'UI-0005' })

    return http.POST({
      module: 'login',
      appendColumns: false,
      appendSession: false,
      processResponse: false,
      params: { action: 'redeemToken' },
      data: {
        token,
        client: session.client(),
        appId,
        authId: +new Date()
      }
    }).done(data => {
      session.set(data)
      ox.trigger('login:success', data)
    }).fail(error => {
      if (ox.debug) console.warn('PWA login has failed with error', error)
      util.fail({ error: gt('Something went wrong. Maybe the session expired or the token is invalid.'), code: 'UI-0005' })
      _.url.hash({ pwalogin: null, login_type: null })
    })
  }
})
