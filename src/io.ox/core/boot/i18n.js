/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import gt from 'gettext'

// A list of all strings to be included in the POT file.
// eslint-disable-next-line no-unused-vars
function list () {
  // #. the noun not the verb
  gt.pgettext('word', 'Sign in')
  // #. While or after requesting a new password you can go back to the initial login page
  gt('Back to sign in')
  gt('Confirm new password')
  gt('Connection error')
  gt('Connection timed out. Please try reloading the page.')
  gt('Email address')
  gt('Error')
  // #. 'Google Chrome' is a brand and should not be translated
  gt('For best results we recommend using Google Chrome for Android.')
  gt('Forgot your password?')
  // #. The missing word at the end of the sentence ('Play Store') will be injected later by script
  gt('Get the latest version from the ')
  gt('Imprint')
  gt('Language:')
  gt('Language')
  gt('Languages')
  gt('Next')
  gt('No connection to server. Please check your internet connection ' + 'and retry.')
  gt('Password')
  gt('Please enter the same password.')
  gt('Please enter your credentials.')
  // #. While requesting a new password on the login page; %1$s will be replaced by the product name e.g. OX App Suite
  gt('Please enter your email address associated with %1$s. You will receive an email that contains a link to reset your password.')
  gt('Please enter your new password.')
  gt('Please enter your password.')
  gt('Privacy Policy')
  gt('Reload')
  gt('Reset password')
  gt('Retry')
  gt('Set password')
  gt('Sign in')
  gt('Some mandatory configuration could not be loaded.')
  gt('Some mandatory configuration could not be loaded.')
  gt('Something went wrong. Please close this browser tab and try again.')
  gt('Stay signed in')
  // #. all variables are version strings of the browsers, like 52 in Chrome 52
  gt('Support starts with Chrome %1$d, Firefox %2$d, Edge %3$d, and Safari %4$d.')
  gt('The service is not available right now.')
  gt('This browser is not supported on your current platform.')
  gt('This platform is currently not supported.')
  gt('Username')
  gt('You have been automatically signed out')
  gt('Your browser is not officially supported on your current platform.')
  // #. %n in the lowest version of Android
  gt('You need to use Android %n or higher.')
  // #. %n is the lowest version of iOS
  gt('You need to use iOS %n or higher.')
  gt('Your browser is not supported')
  gt('While the site may work as expected, we recommend using Safari on iOS.')
  gt('Your browser\'s cookie functionality is disabled. Please turn it on.')
  // #. browser recommendation: sentence ends with 'Google Chrome' (wrappend in a clickable link)
  gt('Your password is expired. Please change your password to continue.')
  gt('Your platform is not supported')
}
