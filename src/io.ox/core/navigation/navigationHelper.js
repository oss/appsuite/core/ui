/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import apps from '@/io.ox/core/api/apps'
import _ from '@/underscore'
import $ from '@/jquery'
import ox from '@/ox'
import { controller as notificationAreaController } from '@/io.ox/core/notifications/main'
import { openModals } from '@/io.ox/backbone/views/modal'
import { open as openSettingsDialog, close as closeSettingsDialog, getModal, setCurrentSettings } from '@/io.ox/settings/main'

let currentStateId
// use timestamp as baseId to prevent page reloads changing our order
const idBase = _.now()
let nextStateId = 0
function getID () { return `${idBase}.${nextStateId}` }

// cache for relevant actions for back/forward navigations (not every popstate event is relevant)
let actionHistory = []
let currentActionIndex = -1
let navigationInProgress = false
// ignores action response to popstate event. Used if you want to manipulate the history stack "silently"
let ignoreNextAction = false

// add an initial state
history.replaceState({ ...history.state, id: getID() }, '')
currentStateId = history.state.id
nextStateId++

// general navigation tracking
window.addEventListener('popstate', async event => {
  if (!event?.state?.id) {
    // add id to state. This way we can determine the navigation direction (note: second parameter is always '', must be there but is ignored by browser)
    history.replaceState({ id: getID(), ...event.state }, '')
    nextStateId++
  } else if (ignoreNextAction) {
    ignoreNextAction = false
    // make sure history state is from this session and not from before a page reload, to prevent errors
  } else if (history.state.id.startsWith(idBase)) {
    navigationInProgress = true
    if (event.state.id < currentStateId) await goBack()
    if (event.state.id > currentStateId) await goForward()
    navigationInProgress = false
  }
  currentStateId = history.state.id
})

// restore previous state, as if no navigation happened. Used if current navigation action cannot be done at the moment.
// for example: close settings while there is a modal dialog open on top of it
function restoreState (direction) {
  // careful here, we don't want to create a navigation loop
  ignoreNextAction = true
  if (direction === 'forward') {
    currentActionIndex--
    return history.back()
  }
  // back
  currentActionIndex++
  history.forward()
}

function closeModal () {
  // Try to close the currently open dialog
  const currentModal = openModals.queue.find(dialog => !dialog.$el.hasClass('modal-paused') && !dialog.$el.hasClass('hidden'))
  // check if we have a cancel button. Trigger cancel if this is the case
  if (currentModal?.$el.find('[data-action="cancel"]').length > 0) currentModal.invokeAction('cancel')
}

async function goBack () {
  if (currentActionIndex === -1) return
  await performAction(actionHistory[currentActionIndex], 'back')
  currentActionIndex--
}

async function goForward () {
  if (currentActionIndex === actionHistory.length - 1) return
  currentActionIndex++
  await performAction(actionHistory[currentActionIndex], 'forward')
}

async function performAction (action, direction) {
  const options = {
    direction,
    ...action,
    // add some direction dependant attributes
    ...(direction === 'forward'
      ? {
          targetStatus: action.status,
          targetVisibility: action.visible,
          targetApp: action.newApp,
          targetPage: action.newPage,
          targetValue: action.currentValue
        }
      : {
          targetStatus: !action.status,
          targetVisibility: !action.visible,
          targetApp: action.previousApp,
          targetPage: action.previousPage,
          targetValue: action.previousValue
        })
  }
  switch (action.type) {
    case 'changeApp':
      changeApp(options)
      break
    case 'changePage':
      await changePage(options)
      break
    case 'notificationAreaToggle':
      notificationAreaController.mainView.toggle(options.targetVisibility)
      break
    case 'toggleDropdown':
      if (options.toggle?.length) options.toggle.trigger('click')
      break
    case 'toggleFolderSelectionMode':
      toggleFolderSelectionMode(options)
      break
    case 'toggleCheckboxes':
      toggleCheckboxes(options)
      break
    case 'toggleSearchMode':
      $('#io-ox-topbar-mobile-search-container button').trigger('click')
      if (options.targetStatus) redoSearch(options)
      if (!_.device('smartphone') && !options.targetStatus) $('#io-ox-topsearch .cancel-button').trigger('click')
      break
    case 'openSettings':
      if (direction === 'forward') return openSettingsDialog()
      // direction is needed, in case we need to restore the old navigation state
      closeSettings(direction)
      break
    case 'closeSettings':
      // direction is needed, in case we need to restore the old navigation state
      if (direction === 'forward') return closeSettings(direction)
      openSettingsDialog()
      break
    // no default
  }
}

// ----------------- action helpers -----------------

function changeApp ({ targetApp }) {
  const app = apps.get(targetApp)
  if (!app) return
  app.launch()
}

async function changePage ({ appId, targetPage, settingsPage, direction }) {
  if (!appId || !targetPage) return
  // settings pagecontroller doesn't have an app
  const pages = appId === 'settings' ? getModal()?.options?.pages : apps.get(appId)?.pages
  if (!pages) return
  // actually only relevant on mobile
  if (settingsPage && targetPage === 'detailView') return await setCurrentSettings(settingsPage)
  if (settingsPage && targetPage === 'folderTree' && getModal().$el.hasClass('modal-paused')) {
    // settings dialog is paused. Try to close the currently open dialog
    closeModal()
    // undo navigation and restore previous state (settings are still open after all)
    return restoreState(direction)
  }
  pages.changePage(targetPage)
}

function toggleFolderSelectionMode ({ appId }) {
  const app = apps.get(appId)
  if (!app || !app.toggleFolders) return
  app.toggleFolders()
}

function redoSearch ({ appId }) {
  const app = apps.get(appId)
  if (!app || !app.searchView) return
  app.searchView.redoLastSearch()
}

function toggleCheckboxes ({ appId, targetValue }) {
  const app = apps.get(appId)
  if (!app) return
  // calendar is special
  if (appId === 'io.ox/calendar') return app.pages.getNavbar('list').trigger('rightAction')
  app.props.set('checkboxes', targetValue)
}

function addStateToHistory () {
  history.pushState({ id: getID() }, '')
  currentStateId = history.state.id
  nextStateId++
}

function addActionToHistory (data) {
  if (!data) return
  // do not track actions that were caused by navigating back or forwards (we don't want to pollute our own history)
  if (navigationInProgress) return
  // slice of all "newer" actions that might be there. User navigated back and did something new -> old forward states are no longer valid
  actionHistory = actionHistory.slice(0, currentActionIndex + 1)
  actionHistory.push(data)
  currentActionIndex = actionHistory.length - 1
  // add state to history (so the backbutton has something to go back to (not all actions create a state in the history))
  addStateToHistory()
}

// called to remove actions associated with apps that are no longer there
function removeAppActionsFromHistory (appId) {
  if (!appId) return
  // slice of all "newer" actions that might be there. App remove may cause broken states otherwise
  actionHistory = actionHistory.slice(0, currentActionIndex + 1)
  let followUpAction = false
  actionHistory = actionHistory.filter(action => {
    // remove actions between every switch to removed app and switch from removed app
    if (action.type === 'changeApp') {
      if (action.newApp === appId) {
        followUpAction = true
        return false
      }
      if (action.previousApp === appId) {
        followUpAction = false
        return false
      }
    } else if (followUpAction) return false
    return true
  })
  currentActionIndex = actionHistory.length - 1
  // add state to history (so the backbutton has something to go back to (not all actions create a state in the history))
  addStateToHistory()
}

function closeSettings (direction) {
  // check if settings dialog is paused
  if (!getModal().$el.hasClass('modal-paused')) return closeSettingsDialog()
  // settings dialog is paused. Try to close the currently open dialog
  closeModal()
  // undo navigation and restore previous state (settings are still open after all)
  restoreState(direction)
}

// ----------------- action event listeners -----------------

apps.on('navigation:change:app', ({ previousApp, newApp }) => {
  addActionToHistory({ type: 'changeApp', previousApp, newApp })
})

apps.on('remove', app => {
  removeAppActionsFromHistory(app.id)
})

apps.on('navigation:change:page', ({ appId, previousPage, newPage }) => {
  // special case for calendar, first pagechange event is not a real pagechange event. Ignore that.
  if (previousPage === 'start') return
  const options = { type: 'changePage', appId, previousPage, newPage }
  // settings is a bit special, detail page is reused, but we need to keep track of the currently shown settings (technically it's the current folder)
  if (appId === 'settings') options.settingsPage = getModal()?.options?.tree.selection.get()
  addActionToHistory(options)
})

apps.on('navigation:app:property:change', ({ appId, property, previousValue, currentValue }) => {
  if (property === 'mobileFolderSelectMode') addActionToHistory({ type: 'toggleFolderSelectionMode', appId })
  if (property === 'checkboxes') addActionToHistory({ type: 'toggleCheckboxes', appId, previousValue, currentValue })
  // desktop search misses toggleview, so just work with the propertychange
  if (!_.device('smartphone') && property === 'searching') addActionToHistory({ type: 'toggleSearchMode', appId, status: currentValue })
})

ox.on('navigation:open:settings', () => addActionToHistory({ type: 'openSettings' }))

ox.on('navigation:close:settings', () => addActionToHistory({ type: 'closeSettings' }))

ox.on('navigation:search:toggled', ({ appId, status }) => addActionToHistory({ type: 'toggleSearchMode', appId, status }))

// only track on smartphones since dropdowns are fullscreen there
if (_.device('smartphone')) {
  $(document).on('shown.bs.dropdown hidden.bs.dropdown', event => {
    if (!event?.relatedTarget) return
    addActionToHistory({ type: 'toggleDropdown', toggle: $(event.relatedTarget) })
  })
}

function addNotificationListener () {
  notificationAreaController.on('main:visibility-change', visible => addActionToHistory({ type: 'notificationAreaToggle', visible }))
}

if (notificationAreaController.mainView) {
  addNotificationListener()
} else {
  notificationAreaController.on('initialized', addNotificationListener)
}
