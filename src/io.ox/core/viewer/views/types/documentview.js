/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ox from '@/ox'
import $ from '@/jquery'
import _ from '@/underscore'

import BaseView from '@/io.ox/core/viewer/views/types/baseview'
import PDFDocumentManager from '@/io.ox/core/pdf/pdfdocumentmanager'
import PDFViewerApplication from '@/io.ox/core/pdf/pdfviewerapplication'
import { pdfloader } from '@/io.ox/core/pdf/pdfloader'
import { getPDFloadErrorDescription } from '@/io.ox/core/pdf/util'
import DocConverterUtils from '@/io.ox/core/tk/doc-converter-utils'
import Util from '@/io.ox/core/viewer/util'

import gt from 'gettext'

/**
 * The document file type. Implements the ViewerType interface.
 *
 * @interface ViewerType (render, prefetch, show, unload)
 *
 */
const DocumentView = BaseView.extend({

  initialize (options) {
    Util.logPerformanceTimer('DocumentView:initialize')
    _.extend(this, options)
    // predefined zoom factors.
    this.ZOOM_FACTORS = [25, 35, 50, 75, 100, 125, 150, 200, 300, 400]
    this.pdfViewerApplication = null
    this.pdfDocumentManager = null
    // a Deferred object indicating the load process of this document view.
    this.documentLoad = $.Deferred()
    this.disposed = null
    // bind resize, zoom and close handler
    this.listenTo(this.viewerEvents, 'viewer:resize', this.onResize)
    this.listenTo(this.viewerEvents, 'viewer:zoom:in', () => this.changeZoomLevel('increase'))
    this.listenTo(this.viewerEvents, 'viewer:zoom:out', () => this.changeZoomLevel('decrease'))
    this.listenTo(this.viewerEvents, 'viewer:zoom:fitwidth', () => this.changeZoomLevel('fitwidth'))
    this.listenTo(this.viewerEvents, 'viewer:zoom:fitheight', () => this.changeZoomLevel('fitheight'))
    this.listenTo(this.viewerEvents, 'viewer:beforeclose', this.onBeforeClose)
    this.listenTo(this.viewerEvents, 'viewer:document:scrolltopage', this.onScrollToPage)
    this.listenTo(this.viewerEvents, 'viewer:document:next', this.onNextPage)
    this.listenTo(this.viewerEvents, 'viewer:document:previous', this.onPreviousPage)
    this.listenTo(this.viewerEvents, 'viewer:document:first', this.onFirstPage)
    this.listenTo(this.viewerEvents, 'viewer:document:last', this.onLastPage)
    this.listenTo(this.viewerEvents, 'viewer:document:print', this.onPrint)
    this.listenTo(this.viewerEvents, 'viewer:document:scrolldown', event => this.scroll('down', event))
    this.listenTo(this.viewerEvents, 'viewer:document:scrollup', event => this.scroll('up', event))
    this.showCurrentZoomAsCaptionDebounced = _.debounce(this.showCurrentZoomAsCaption.bind(this), 10)
    // disable display flex styles, for pinch to zoom
    this.$el.addClass('swiper-slide-document')
    // swiping is not supportet currently on pdf pages
    this.$el.addClass('swiper-no-swiping')
    // pdfs scroll in their own container and not the whole slide
    this.$el.removeClass('scrollable')
    // whether double tap zoom is already triggered
    this.doubleTapZoomed = false
    // indicates if the document was prefetched
    this.isPrefetched = false
    // needed to decide which caption should be shown
    this.lastZoomTimestamp = Date.now()
    // required at load for user notifications
    this.$waitForGeneratedNotification = null
    this.$generatedReadyNotification = null
    this.pdfDocumentWaitTimer = null
    // DOM references
    this.$loadingbar = null
    this.$documentContainer = null
    this.$scaler = null
  },

  /**
   * Tap event handler.
   * - zooms documents a step in and out in case of a double tap.
   *
   * @param {jQuery.Event} event The jQuery event object.
   * @param {number}       tapCount  The count of taps, indicating a single or double tap.
   */
  onTap (event, tapCount) {
    if (tapCount === 2) {
      if (this.doubleTapZoomed) {
        this.changeZoomLevel('fitwidth')
      } else {
        this.changeZoomLevel('original')
      }
      this.doubleTapZoomed = !this.doubleTapZoomed
    }
  },

  /**
   * Handles pinch events.
   *
   * @param {string}       phase    The current pinch phase ('start', 'move', 'end' or 'cancel')
   * @param {jQuery.Event} event    The jQuery tracking event.
   * @param {number}       distance The current distance in px between the two fingers
   * @param {Point}        midPoint The current center position between the two fingers
   */
  onPinch: (function () {
    let startDistance,
      nextTransformScale,
      nextZoomFactor,
      startZoomFactor,
      pinchFactor,

      rectAfterScale,
      rectAfterZoom,
      deltaTop,
      deltaLeft,

      touchPosInScaleContainerX,
      touchPosInScaleContainerY,
      currentPage

    return function pinchHandler (phase, event, distance, midPoint) {
      window.requestAnimationFrame(() => {
        switch (phase) {
          case 'start':
            touchPosInScaleContainerX = midPoint.x - this.$scaler[0].getBoundingClientRect().x
            touchPosInScaleContainerY = midPoint.y - this.$scaler[0].getBoundingClientRect().y
            startDistance = distance
            startZoomFactor = this.pdfViewerApplication.getZoom() / 100
            break
          case 'move':
            pinchFactor = distance / startDistance
            nextTransformScale = Math.round(1000 * pinchFactor) / 1000
            nextZoomFactor = nextTransformScale * startZoomFactor
            if (nextZoomFactor > this.getMaxZoomFactor() / 100) {
              nextZoomFactor = this.getMaxZoomFactor() / 100
              this.viewerEvents.trigger('viewer:blendcaption', Math.floor(100 * nextZoomFactor) + ' %')
              return
            }
            if (nextZoomFactor < this.getMinZoomFactor() / 100) {
              nextZoomFactor = this.getMinZoomFactor() / 100
              this.viewerEvents.trigger('viewer:blendcaption', Math.floor(100 * nextZoomFactor) + ' %')
              return
            }
            this.$scaler.css({
              'transform-origin': (touchPosInScaleContainerX + this.$el.scrollLeft()) + 'px ' + (touchPosInScaleContainerY + this.$el.scrollTop()) + 'px',
              transform: 'scale(' + nextTransformScale + ')'
            })
            this.viewerEvents.trigger('viewer:blendcaption', Math.floor(100 * nextZoomFactor) + ' %')
            break
          case 'end': case 'cancel':
            if (!nextZoomFactor) return
            currentPage = this.pdfViewerApplication.page
            rectAfterScale = this.$documentContainer.find(`.page[data-page-number="${currentPage}"]`)[0].getBoundingClientRect()
            this.changeZoomLevel(nextZoomFactor, { showCaption: false })
            this.$scaler.removeAttr('style')
            rectAfterZoom = this.$documentContainer.find(`.page[data-page-number="${currentPage}"]`)[0].getBoundingClientRect()
            deltaTop = rectAfterZoom.top - rectAfterScale.top
            deltaLeft = rectAfterZoom.left - rectAfterScale.left
            this.$documentContainer[0].scrollTop = this.$documentContainer[0].scrollTop + deltaTop
            this.$documentContainer[0].scrollLeft = this.$documentContainer[0].scrollLeft + deltaLeft

            nextZoomFactor = null
            break
        }
      })
    }
  })(),

  /**
   * Scroll-to-page handler:
   * - scrolls to the desired page number.
   *
   * @param {number} pageNumber The 1-based page number.
   */
  onScrollToPage (pageNumber) {
    if (this.isVisible()) { this.pdfViewerApplication.page = pageNumber }
  },

  /**
   * Next page handler:
   * - scrolls to the next page
   */
  onNextPage () {
    if (this.isVisible()) { this.pdfViewerApplication.page++ }
  },

  /**
   * Previous page handler:
   * - scrolls to the previous page
   */
  onPreviousPage () {
    if (this.isVisible()) { this.pdfViewerApplication.page-- }
  },

  /**
   * First page handler:
   * - scrolls to the first page
   */
  onFirstPage () {
    if (this.isVisible()) { this.onScrollToPage(1) }
  },

  /**
   * Last page handler:
   * - scrolls to the last page
   */
  onLastPage () {
    if (this.isVisible() && this.pdfDocumentManager?.pdfDocument.numPages > 1) {
      this.onScrollToPage(this.pdfDocumentManager.pdfDocument.numPages)
    }
  },

  scroll (direction, event) {
    if (this.isVisible() && this.$documentContainer) {
      // when the focus is inside the pdf container with overflow: scroll,
      // native scrolling would be added in addition, prevent that
      event.preventDefault()

      this.$documentContainer[0].scrollBy({
        top: direction === 'up' ? -40 : 40,
        left: 0
      })
    }
  },

  /**
   * Print handler:
   * - opens the document in a new browser tab
   */
  onPrint () {
    // can't use print action because it needs the ToolbarVIew as context to function
    const documentUrl = DocConverterUtils.getEncodedConverterUrl(this.model)
    window.open(documentUrl, '_blank')
  },

  /**
   * Viewer before close handler:
   * - saves the scroll position of the document.
   */
  onBeforeClose () {
    // for now this is not used, but keep the structure
    // if (this.isVisible()) {
    //   const fileId = this.model.get('id')
    //   const fileScrollPosition = this.$container.scrollTop()
    //   this.setInitialScrollPosition(fileId, fileScrollPosition)
    // }
  },

  /**
   * - shows the current page on a page change that was NOT triggerd by zoom changes.
   * - blends in navigation controls.
   * - selects the corresponding thumbnail in the thumbnail pane.
   */
  onPageChangeHandler (pdfJsEvent, options = { showCaption: true }) {
    const currentPageIndex = this.pdfViewerApplication.page.toString()
    const pageCount = this.pdfDocumentManager.pdfDocument.numPages.toString()
    // use-case: zoom a page can cause a page change
    const pageChangePropablyCausedByZoomChange = !(Date.now() - this.lastZoomTimestamp > 200)

    // note: we want an typeError in 'options' to notice when the argument order changes!
    if (options.showCaption && !pageChangePropablyCausedByZoomChange) {
    // #. text of a viewer document page caption
    // #. Example result: "Page 5 of 10"
    // #. %1$d is the current page index
    // #. %2$d is the total number of pages
      this.viewerEvents.trigger('viewer:blendcaption', gt('Page %1$d of %2$d', currentPageIndex, pageCount))
    }
    // select/highlight the corresponding thumbnail according to displayed document page
    this.viewerEvents.trigger('viewer:document:selectthumbnail', currentPageIndex)
      .trigger('viewer:document:pagechange', currentPageIndex, pageCount)
  },

  /**
   * Creates and renders an empty document template slide.
   *
   * @returns {DocumentView} the DocumentView instance.
   */
  render () {
    this.$el.empty()
    return this
  },

  /**
   * "Prefetches" triggers a PDF conversion for the given document on the server.
   *  The idea is to lower the request time for files than needs pdf conversion.
   *
   *  In order to save memory and network bandwidth only documents with highest prefetch priority are prefetched.
   *
   * @param   {object}        options Optional parameters (options.priority)
   * @returns {Backbone.View}         A reference to this DocumentView instance.
   */
  prefetch (options) {
    // check for highest priority
    if (!this.model.isPDF() && options?.priority === 1) {
      $.ajax({
        url: DocConverterUtils.getEncodedConverterUrl(this.model, { async: true }),
        dataType: 'text'
      })
      this.isPrefetched = true
    }

    return this
  },

  /**
   * "Shows" and renders the a given PDF document and attaches it to the slide.
   *
   * @returns {Backbone.View} the DocumentView instance.
   */
  show () {
    // do nothing and quit if a document is already disposed.
    if (this.disposed) { return }

    // already loaded slides must be skipped or multiple instances are created:
    // the check below works fine, but in case workers are not removed after closing
    // the viewer (multiple instances created) this is a place to check
    if (this.$el.children().length > 0) { return }

    this.$loadingbar = $('<div class="progress-bar hide" role="progressbar" aria-valuenow="0" style="height: 1px; transition: width 200ms; position: absolute;">')
    this.$el.empty().append(this.$loadingbar)

    // load pdf file
    const documentUrl = DocConverterUtils.getEncodedConverterUrl(this.model)
    this.pdfloader = pdfloader()
    this.pdfloader.fetchWithProgress(documentUrl, (status, progress, estimatedLoadTime) => {
      // waiting for response, the DC propably needs time
      if (status === 'waiting') this.startPdfGeneratingWaitMessage()

      if (status === 'downloading') this.clearPdfGeneratingWaitMessageTimer()

      // we have an estimate of the download duration now
      if (status === 'downloading' && typeof estimatedLoadTime === 'number') {
        // visible 'wait' notification has  a 'ready' follow-up notification, show it only when the user has a chance to read it
        if (this.$waitForGeneratedNotification) {
          this.clearAllPdfGeneratingMessages()
          if (estimatedLoadTime > 2000) this.startPdfGeneratingReadyMessage()
        }

        // show loadingbar only for reasonable load times to prevent flicker
        if (estimatedLoadTime > 1000) {
          this.$loadingbar?.attr('aria-valuenow', progress).css('width', progress + '%').removeClass('hide')
          // remove when done
          if (progress >= 100) {
            this.$loadingbar.one('transitionend', () => {
              this.$loadingbar.addClass('hide')
            })
          }
        } else {
          this.$loadingbar.addClass('hide')
        }
      }

      // final clean up
      if (progress >= 100) this.clearAllPdfGeneratingMessages()
    })
    // pdf file downloaded
      .then((pdfFile) => {
        this.pdfDocumentManager = new PDFDocumentManager(pdfFile)
        return this.pdfDocumentManager.open()
      })
      .then((pdfDocument) => {
        if (!pdfDocument) { return }
        this.pdfViewerApplication = new PDFViewerApplication(pdfDocument, { initialZoomLevel: 'auto', initialScrollPosition: 0 })
        this.$documentContainer = this.pdfViewerApplication.$container
        this.$scaler = this.pdfViewerApplication.$scaler
        this.$el.append(this.$documentContainer)
        // hide, but keep dimensions to prevent visible flicker at first load,
        // don't use 'visibility: hidden' to prevent focus loss
        this.$documentContainer.addClass('first-load')
        return this.pdfViewerApplication.initUI()
      })
      // pdf viewer ready
      .then(() => {
        this.$documentContainer.enableTouch({
          tapHandler: this.onTap.bind(this),
          pinchHandler: this.onPinch.bind(this),
          touchEndHandler: () => { this.viewerEvents.trigger('viewer:blendnavigation') }
        })

        // prevent visible flicker during the very first frames:
        // - limit waiting time, complex pages might render longer but the the user should not wait too
        //   long for a sign of progress, even when this means seeing visible flicker before rendering has been finished
        // - do that only at fist load of the whole pdf, because re-loading pages at pagination must be visible
        window.setTimeout(() => { this.$documentContainer.removeClass('first-load') }, 64)

        this.viewerEvents.trigger('viewer:document:loaded')

        this.onPageChangeHandler(null, { showCaption: false })

        this.pdfViewerApplication.eventBus.on('pagechanging', this.onPageChangeHandler.bind(this))
      })
      .catch((error) => {
        if (ox.debug) { console.warn('Core.Viewer.DocumentView.show(): failed loading PDF document - Error:', error?.origin, error?.cause, error?.message) }

        // clean up load progress feedback
        this.$loadingbar?.addClass('hide')
        this.clearAllPdfGeneratingMessages()

        // handle and show errors
        if (error?.cause !== 'abortedByUser') {
          const { notificationText, notificationIcon, details } = getPDFloadErrorDescription(error)
          const $notification = this.displayDownloadNotification(notificationText, notificationIcon)
          if (details) {
            $notification.append($('<div>').css({ position: 'absolute', bottom: 5, left: 'auto', right: 'auto', opacity: 0.7 }).text(_.noI18n(details)))
          }
        }
      })

    return this
  },

  /**
   * Changes the zoom level of a document.
   *
   * @param {string || number} level Supported values: 'increase', 'decrease', 'fitheight','fitwidth',  'original' or number.
   */
  changeZoomLevel (level, options = { showCaption: true }) {
    if (!this.isVisible() || !this.pdfViewerApplication) { return }

    const currentZoomFactor = this.pdfViewerApplication.getZoom()
    let nextZoomFactor

    if (_.isNumber(level)) {
      nextZoomFactor = level
      level = 'number'
    }
    switch (level) {
      case 'number':
        // keep number the first entry to be faster for direct manipulation
        break
      case 'increase':
        nextZoomFactor = _.find(this.ZOOM_FACTORS, function (factor) {
          return factor > currentZoomFactor
        }) || this.getMaxZoomFactor()
        nextZoomFactor = nextZoomFactor / 100
        break
      case 'decrease': {
        const lastIndex = _.findLastIndex(this.ZOOM_FACTORS, function (factor) {
          return factor < currentZoomFactor
        })
        nextZoomFactor = this.ZOOM_FACTORS[lastIndex] || this.getMinZoomFactor()
        nextZoomFactor = nextZoomFactor / 100
        break
      }
      case 'fitwidth':
        nextZoomFactor = 'page-width'
        break
      case 'fitheight':
        nextZoomFactor = 'page-height'
        break
      case 'auto':
        nextZoomFactor = 'auto'
        break
      case 'original':
        nextZoomFactor = 1
        break
      default:
        return
    }

    // limit to min/max
    nextZoomFactor = nextZoomFactor > this.getMaxZoomFactor() / 100 ? this.getMaxZoomFactor() / 100 : nextZoomFactor
    nextZoomFactor = nextZoomFactor < this.getMinZoomFactor() / 100 ? this.getMinZoomFactor() / 100 : nextZoomFactor

    this.lastZoomTimestamp = Date.now()
    this.pdfViewerApplication.setZoom(nextZoomFactor)
    if (options.showCaption) this.showCurrentZoomAsCaptionDebounced()
  },

  showCurrentZoomAsCaption () {
    this.viewerEvents.trigger('viewer:blendcaption', Math.round(this.pdfViewerApplication.getZoom()) + ' %')
  },

  /**
   * Gets the maximum zoom factor of a document.
   */
  getMaxZoomFactor () {
    return _.last(this.ZOOM_FACTORS)
  },

  /**
   * Gets the minimum zoom factor of a document.
   */
  getMinZoomFactor () {
    return _.first(this.ZOOM_FACTORS)
  },

  /**
   * Clears the timer and all possible notifications to be always in a clean state after calling.
   */
  clearAllPdfGeneratingMessages () {
    this.$waitForGeneratedNotification?.remove()
    // acts also as a flag, hence remove is not enough
    this.$waitForGeneratedNotification = null

    this.$generatedReadyNotification?.remove()
    this.clearPdfGeneratingWaitMessageTimer()
  },

  /**
   * Clears the timer to show a waiting notification.
   */
  clearPdfGeneratingWaitMessageTimer () {
    clearTimeout(this.pdfDocumentWaitTimer)
    this.pdfDocumentWaitTimer = null
  },

  /**
   * Creates a self-removing generating success notification.
   */
  startPdfGeneratingReadyMessage () {
    this.$generatedReadyNotification = this.displayNotification(gt('Preview generated. Downloading the file.'), 'io-ox-busy', false)
    this.$generatedReadyNotification.delay(5000).fadeOut(5000, () => { this.$generatedReadyNotification?.remove() })
  },

  /**
   * Creates a timer to show a message in case it takes very long to show the pdf.
   */
  startPdfGeneratingWaitMessage () {
    if (this.pdfDocumentWaitTimer) { return }
    this.pdfDocumentWaitTimer = window.setTimeout(() => {
      this.$waitForGeneratedNotification = this.displayDownloadNotification(gt('Your preview is being generated.'), 'io-ox-busy', gt('Alternatively you can download the file.'), false)
    }, 5000)
  },

  /**
   * Resize handler of the document view. Calculates and sets a new initial zoom factor
   */
  onResize () {
    if (!this.isVisible()) { return }
    // it's tempting to use requestAnimationFrame, but it creates visual flicker at annotations
    // how to reproduce:
    // -  load pdf with comment
    // -  pin a comment
    // -  resize browser
    // -  visible flicker
    this.changeZoomLevel('auto', { showCaption: false })
  },

  /**
   * Unloads the document slide by destroying the pdf view and model instances
   */
  unload () {
    // keep this destructoring order
    this.pdfloader?.abort()
    this.pdfDocumentManager?.close()
    this.pdfViewerApplication?.close()
    this.pdfViewerApplication = null
    this.pdfDocumentManager = null
    // clear document container content
    this.$el.empty()
    this.isPrefetched = false

    this.clearAllPdfGeneratingMessages()

    return this
  },

  /**
   * Destructor function of this view.
   */
  onDispose () {
    this.unload()
    // save disposed status
    this.disposed = true
    this.$el.off()
    this.$el.removeClass('swiper-slide-document')
    if (this.thumbnailsView) {
      this.thumbnailsView.onDispose()
    }
  }

})

// returns an object which inherits BaseView
export default DocumentView
