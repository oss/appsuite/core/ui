/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { DateTime } from 'luxon'
import { settings as coreSettings } from '@/io.ox/core/settings'
import { addReadyListener } from '@/io.ox/core/events'
import debug from '@/io.ox/core/main/debug'

export function updateConfig (id, data) {
  return coreSettings.set(`autoopen/${id}`, data).save()
}

export function mergeConfig (id, data) {
  const newConfig = { ...coreSettings.get(`autoopen/${id}`, {}), ...data }
  return coreSettings.set(`autoopen/${id}`, newConfig).save()
}

export function ensureInitialConfig (id, { cooldown = 7, remaining = 3, lastAutoopen = '1970-01-01' }) {
  if (Object.keys(coreSettings.get(`autoopen/${id}`, {})).length > 0) return
  updateConfig(id, { cooldown, remaining, lastAutoopen })
}

export function isActive (id) {
  const { remaining = 0, cooldown = 0, lastAutoopen = '1970-01-01' } = coreSettings.get(`autoopen/${id}`, {})
  // check that remaining is still positive and cooldown period has been reached
  if (remaining < 1) return false
  const lastTime = DateTime.fromISO(lastAutoopen)
  const daysSinceLastActivation = Math.abs(lastTime.diffNow('days').days)
  return daysSinceLastActivation >= cooldown
}

export function setLastAutoopen (id) {
  const { cooldown, remaining } = coreSettings.get(`autoopen/${id}`, { })
  const today = DateTime.now().toISODate()
  updateConfig(id, { cooldown, remaining: remaining - 1, lastAutoopen: today })
}

export function disable (id) {
  updateConfig(id, { remaining: 0 })
}

addReadyListener('settings', () => {
  Object.keys(coreSettings.get('autoopen', {})).sort().forEach(key => {
    const data = coreSettings.get(`autoopen/${key}`, {})
    debug(`Autoopen for "${key}": ${JSON.stringify(data)}`)
  })
})
