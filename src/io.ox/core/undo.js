/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ox from '@/ox'
import { createApp } from 'vue'
import PrimeVue from 'primevue/config'
import { ToastService } from 'primevue'
import UndoToast from '@/io.ox/core/toasts/undo-toast.vue'
import { settings as coreSettings } from '@/io.ox/core/settings'
import { hasFeature } from '@/io.ox/core/feature'
import mailAPI from '@/io.ox/mail/api'

let UndoToastService = null

function getUndoToastService () {
  UndoToastService = UndoToastService ||
    createApp(UndoToast)
      .use(PrimeVue)
      .use(ToastService)
      .mount('#io-ox-toast-container')
      .toast
  return UndoToastService
}

export function undoAction ({ token, text, icon = 'bi/folder-symlink.svg', life = 5000 }) {
  if (!token || !hasFeature('undo') || !(life > 0)) return
  const toastService = getUndoToastService()
  toastService.removeGroup('undo')
  toastService.add({
    detail: {
      text,
      token,
      icon
    },
    closable: true,
    life,
    group: 'undo'
  })
}

mailAPI.on('undo', (event, action, data) => {
  undoAction({ ...data, action, life: coreSettings.get('undoPeriod', 5000) })
})

// DEPRECATED: `undoMailAction`, pending removal with 8.37.
if (ox.debug) console.warn('`undoMailAction` is deprecated, pending removal with 8.37. Please use `undoAction`')
export const undoMailAction = undoAction
// DEPRECATED: `undoPeriod`, pending removal with 8.37.
if (ox.debug) console.warn('`undoPeriod` is deprecated, pending removal with 8.37. Please use `coreSettings.get(\'undoPeriod\')`')
export const undoPeriod = coreSettings.get('undoPeriod', 5000)
// DEPRECATED: default export of `undo.js`, pending removal with 8.37.
if (ox.debug) console.warn('default export of `undo.js`is deprecated, pending removal with 8.37. Please use named exports')
export default {
  undoMailAction
}
