/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import moment from '@/moment'
import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import ExtensibleView from '@/io.ox/backbone/views/extensible'
import * as util from '@/io.ox/core/settings/util'
import apps from '@/io.ox/core/api/apps'
import locale from '@/io.ox/core/locale'
import TimezonePicker from '@/io.ox/backbone/mini-views/timezonepicker'
import { changePassword } from '@/io.ox/settings/security/change-password'
import { CustomRadioView } from '@/io.ox/backbone/mini-views/common'
import { st, isConfigurable } from '@/io.ox/settings/index'
import { getCurrent, setTheme, saveCurrent } from '@/io.ox/core/theming/main'

import { settings as coreSettings } from '@/io.ox/core/settings'
import { settings as mailSettings } from '@/io.ox/mail/settings'
import gt from 'gettext'
import Backbone from 'backbone'

const MINUTES = 60000

// this is the official point for settings
ext.point('io.ox/core/settings/detail').extend({
  index: 100,
  id: 'view',
  draw () {
    this.append(
      util.header(
        st.GENERAL,
        'ox.appsuite.user.sect.settings.commonsettings.html'
      ),
      new ExtensibleView({ point: 'io.ox/core/settings/detail/view', model: coreSettings })
        .inject({

          showNoticeFields: ['language', 'timezone'],

          showNotice (attr) {
            return _(this.showNoticeFields).some(function (id) {
              return id === attr
            })
          },

          reloadHint: gt('Changing language or timezone requires a page reload or relogin to take effect.'),

          getLanguageOptions () {
            const isCustomized = !_.isEmpty(coreSettings.get('localeData'))
            const current = locale.current()
            return _(locale.getSupportedLocales())
              .map(function (locale) {
                locale.name = isCustomized && locale.id === current ? locale.name + ' / ' + gt('Customized') : locale.name
                return { label: locale.name, value: locale.id }
              })
          },

          getRefreshOptions () {
            return [
              { label: gt('5 minutes'), value: 5 * MINUTES },
              { label: gt('10 minutes'), value: 10 * MINUTES },
              { label: gt('15 minutes'), value: 15 * MINUTES },
              { label: gt('30 minutes'), value: 30 * MINUTES }
            ]
          },

          propagateSettingsLanguage (val) {
            import('@/io.ox/core/api/tab').then(function ({ default: tabApi }) {
              const newSettings = {
                language: locale.deriveSupportedLanguageFromLocale(val),
                locale: val
              }
              tabApi.propagate('update-ox-object', _.extend(newSettings, { exceptWindow: tabApi.getWindowName() }))
              tabApi.updateOxObject(newSettings)
            })
          },

          propagateSettingsTheme (val) {
            import('@/io.ox/core/api/tab').then(function ({ default: tabApi }) {
              tabApi.propagate('update-ox-object', { theme: val, exceptWindow: tabApi.getWindowName() })
              tabApi.updateOxObject({ theme: val })
            })
          }

        })
        .build(function () {
          this.$el.addClass('settings-body')
        })
        .render().$el
    )
  }
})

ext.point('io.ox/core/settings/detail/view').extend(
  //
  // Events handlers
  //
  {
    id: 'onchange',
    index: 100,
    render () {
      this.listenTo(coreSettings, 'change', function (attr, value) {
        if (ox.tabHandlingEnabled && attr === 'theme') this.propagateSettingsTheme(value)
        if (ox.tabHandlingEnabled && attr === 'language') this.propagateSettingsLanguage(value)
        const showNotice = this.showNotice(attr)
        coreSettings.saveAndYell(undefined, { force: !!showNotice }).then(
          async () => {
            // reload mail settings to avoid an error with MW translated mail category names. See OXUIB-2410
            await mailSettings.reload()
            if (!showNotice) return
            this.$('.reload-page').show()
          }
        )
      })
    }
  },
  {
    id: 'theme',
    index: 200,
    render (baton) {
      if (!isConfigurable.THEME) return
      return util.renderExpandableSection(st.THEME, st.THEME_EXPLANATION, 'io.ox/settings/general/theme', true).call(this, baton)
    }
  },
  {
    id: 'language',
    index: 300,
    render (baton) {
      if (!isConfigurable.LANGUAGE_TIMEZONE) return
      return util.renderExpandableSection(st.LANGUAGE_TIMEZONE, st.LANGUAGE_TIMEZONE_EXPLANATION, 'io.ox/settings/general/language').call(this, baton)
    }
  },
  {
    id: 'apps',
    index: 400,
    render (baton) {
      if (!isConfigurable.APPS) return
      return util.renderExpandableSection(st.APPS, st.APPS_EXPLANATION, 'io.ox/settings/general/apps').call(this, baton)
    }
  },
  {
    id: 'shortcuts',
    index: 500,
    render (baton) {
      if (!isConfigurable.SHORTCUTS) return
      return util.renderExpandableSection(st.SHORTCUTS, st.SHORTCUTS_EXPLANATION, 'io.ox/settings/general/shortcuts').call(this, baton)
    }
  },
  {
    id: 'advanced',
    index: 10000,
    render: util.renderExpandableSection(st.GENERAL_ADVANCED, '', 'io.ox/settings/general/advanced')
  }
)

ext.point('io.ox/settings/general/theme').extend(
  //
  // Theme desktop
  //
  {
    id: 'theme',
    index: 100,
    async render () {
      if (!isConfigurable.ACCENT_COLORS || !isConfigurable.BACKGROUNDS) return
      const $el = $('<div>')
      this.append($el)
      const { getInplaceDialog } = await import('@/io.ox/core/theming/dialog')
      getInplaceDialog($el)
    }
  },
  {
    id: 'theme-mobile',
    index: 200,
    async render (baton) {
      if (!_.device('smartphone')) return
      const { view } = baton
      const { themeMobile } = getCurrent()

      const model = new Backbone.Model({ themeMobile })
      view.listenTo(model, 'change', model => {
        setTheme(model.get('themeMobile'))
        saveCurrent()
      })

      this.append(
        util.compactSelect('themeMobile', gt('Theme settings'), model, [
          { label: gt('Light theme'), value: 'white' },
          { label: gt('Dark theme'), value: 'dark' },
          { label: gt('Use system setting'), value: 'system' }
        ])
      )
    }
  }
)

function getLocalePreviewBlock () {
  const preview = {
    time: moment().month(0).date(29).hour(9).minute(0).format('LT'),
    date: moment().month(0).date(29).hour(9).minute(0).format('L'),
    number: locale.getDefaultNumberFormat(),
    first: locale.getFirstDayOfWeek()
  }

  return $('<div class="code-container locale-example text-xs mr-16">').append(
    $('<div class="code p-16 flex-col">').append(
      $('<div class="flex-row">').append(
        $('<div class="mr-8 text-gray">').text(gt('Time format') + ':'),
        $('<div>').text(preview.time)
      ),
      $('<div class="flex-row">').append(
        $('<div class="mr-8 text-gray">').text(gt('Date format') + ':'),
        $('<div>').text(preview.date)
      ),
      $('<div class="flex-row">').append(
        $('<div class="mr-8 text-gray">').text(gt('Number format') + ':'),
        $('<div>').text(preview.number)
      ),
      $('<div class="flex-row">').append(
        $('<div class="mr-8 text-gray">').text(gt('First day of the week') + ':'),
        $('<div>').text(preview.first)
      )
    )
  )
}

ext.point('io.ox/settings/general/language').extend(
  //
  // Hint on reload
  //
  {
    id: 'hint',
    index: 100,
    render ({ view }) {
      this.append(
        $('<div class="settings-hint reload-page">').hide().append(
          $('<div class="mb-16">').text(view.reloadHint),
          $('<div>').append(
            $('<button type="button" class="btn btn-primary" data-action="reload">')
              .text(gt('Reload page'))
              .on('click', () => { location.reload() })
          )
        )
      )
    }
  },
  {
    id: 'language',
    index: 200,
    render ({ view, model }) {
      if (!isConfigurable.LANGUAGE) return
      const $select = util.compactSelect('language', st.LANGUAGE, model, view.getLanguageOptions())
      const selectView = $select.find('select').data('view')

      selectView.listenTo(model, 'change:language', function (language) {
        _.setCookie('locale', language)
      })

      selectView.listenTo(ox, 'change:locale:data', function () {
        this.setOptions(view.getLanguageOptions())
      })

      this.append(
        $('<div class="pseudo-fieldset">').append(
          $select.addClass('legendary-label')
        )
      )
    }
  },
  {
    id: 'regional-settings',
    index: 300,
    render ({ view, model }) {
      if (!isConfigurable.LANGUAGE) return

      view.listenTo(ox, 'change:locale:data', function () {
        this.$el.find('.locale-example').replaceWith(getLocalePreviewBlock())
      })

      this.append(
        $('<div class="pseudo-fieldset">').append(
          $('<div class="form-group row legendary-label mt-24">').append(
            $('<div class="col-md-6">').append(
              $('<div class="">').append(
                $('<label for="regional-settings" class="block">').text(gt('Regional settings')),
                $('<button role="button" class="btn btn-default" id="regional-settings">')
                  .text(st.CUSTOM_LOCALE + ' \u2026')
                  .on('click', e => {
                    e.preventDefault()
                    import('@/io.ox/core/settings/editLocale').then(({ default: dialog }) => {
                      dialog.open()
                    })
                  })
              )
            ),
            $('<div class="col-md-6">').append(
              $('<div class="">').append(
                $('<label class="block">').text(gt('Preview')),
                getLocalePreviewBlock()
              )
            )
          )
        )
      )
    }
  },
  {
    id: 'timezone',
    index: 400,
    render ({ model }) {
      if (!isConfigurable.TIMEZONE) return
      this.append(
        $('<div class="pseudo-fieldset">').append(
          $('<div class="row mt-24">').append(
            $('<div class="col-md-6 legendary-label">').append(
              $('<label for="settings-timezone">').text(st.TIMEZONE),
              new TimezonePicker({
                name: 'timezone',
                model,
                id: 'settings-timezone',
                showFavorites: true
              }).render().$el
            )
          )
        )
      )
    }
  }
)

ext.point('io.ox/settings/general/apps').extend(
  {
    id: 'autoStart',
    index: 100,
    render ({ model }) {
      const value = model.get('autoStart', '')

      // autofix old school format (see ui@f10b1c98)
      if (/\/main$/.test(value)) model.set('autoStart', value.replace(/\/main$/, ''), { silent: true })

      this.append(
        $('<div class="pseudo-fieldset">').append(
          // #. Start with (application)
          util.compactSelect('autoStart', st.START_WITH, model, apps.getAvailableApps())
            .addClass('legendary-label')
        )
      )
    }
  },
  {
    id: 'quicklaunch',
    index: 200,
    render ({ model }) {
      this.append(
        $('<div class="pseudo-fieldset">').append(
          $('<div class="form-group row legendary-label mt-24">').append(
            $('<div class="col-md-6">').append(
              $('<label for="set-quicklauncher">').text(gt('Quick launch bar')),
              util.explanation(gt('You can edit your favorite apps as a shortcut.')),
              $('<button type="button" class="btn btn-default" id="set-quicklauncher" data-action="configure-quick-launchers">')
                .text(st.QUICK_LAUNCH_BAR + ' \u2026')
                .on('click', async () => {
                  const quickLauncherDialog = (await import('@/io.ox/core/settings/dialogs/quickLauncherDialog')).default
                  quickLauncherDialog.openDialog()
                })
            )
          )
        )
      )
    }
  }
)

ext.point('io.ox/settings/general/advanced').extend(
  {
    id: 'refreshInterval',
    index: 100,
    render ({ view, model }) {
      if (!isConfigurable.REFRESH) return
      this.append(
        // #. Reload data every (x minutes)
        util.compactSelect('refreshInterval', st.REFRESH, model, view.getRefreshOptions())
      )
    }
  },
  {
    id: 'undo',
    index: 150,
    render () {
      if (!isConfigurable.UNDO) return

      this.append(
        $('<fieldset name="undo">').append(
          $('<legend class="sectiontitle">').append(
            $('<h3>').text(st.UNDO)
          ),
          util.explanation(
            gt('Set how long the "Undo" notification stays visible after an action, such as deleting an email. Only the most recent action can be undone.')
          ),
          new CustomRadioView({
            name: 'undoPeriod',
            id: 'undoPeriod',
            cast: parseInt,
            list: [
              { value: 0, label: gt('Do not show') },
              { value: 5000, label: gt('5 seconds') },
              { value: 10000, label: gt('10 seconds') },
              { value: 60000, label: gt('60 seconds') },
            ],
            model: coreSettings
          }).render().$el.addClass('mb-16')
        )
      )
    }
  },
  {
    id: 'buttons',
    index: 200,
    render (baton) {
      const $group = $('<fieldset>')
      ext.point('io.ox/settings/general/advanced/buttons').invoke('render', $group, baton)
      if ($group.children().length > 0) this.append($group)
    }
  }
)

ext.point('io.ox/settings/general/advanced/buttons').extend(
  {
    id: 'password',
    index: 100,
    render () {
      if (!isConfigurable.CHANGE_PASSWORD) return
      this.append(
        $('<button type="button" class="btn btn-default me-16" data-action="edit-password">')
          .text(st.CHANGE_PASSWORD + ' \u2026')
          .on('click', async () => {
            await changePassword()
          })
      )
    }
  },
  {
    id: 'categories',
    index: 200,
    render () {
      if (!isConfigurable.MANAGE_CATEGORIES) return
      this.append(
        $('<button type="button" class="btn btn-default me-16" data-name="categories">')
          .text(st.MANAGE_CATEGORIES + ' \u2026')
          .on('click', async () => {
            const categories = await import('@/io.ox/core/categories/view')
            categories.openManageCategoryModal({ previousFocus: this.$toggle })
          })
      )
    }
  },
  {
    id: 'deputy',
    index: 300,
    render () {
      if (!isConfigurable.MANAGE_DEPUTIES) return
      this.append(
        $('<button type="button" class="btn btn-default me-16" data-action="manage-deputies">')
          .text(st.MANAGE_DEPUTIES + ' \u2026')
          .on('click', async () => {
            const dialog = (await import('@/io.ox/core/deputy/dialog')).default
            dialog.open()
          })
      )
    }
  }
)
