<!--

  @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
  @license AGPL-3.0

  This code is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.

  Any use of the work other than as authorized under this license or copyright law is prohibited.

-->

<template>
  <div id="category-browser-main">
    <div class="dropdown-wrapper">
      <label>{{ categoriesString }} </label>
      <div ref="dropdownPlaceholder"></div>
    </div>
    <div v-show="status!=='default'" class="categories-container selected-categories">
      <BadgeList :collection="selectedCategoriesCollection" :removable="true" />
    </div>
    <div v-show="status==='default'" class="categories-container recent-categories">
      <label :title="recentlyUsedAriaLabel">
        {{
          // #. label for a list of recently used categories
          recentlyUsedString
        }}
      </label>
      <BadgeList :collection="recentCategoriesCollection" :searchable="true" />
    </div>
    <div class="item-list-container">
      <ItemListComponent v-if="activeModules.includes('mail') && listComponents.mail.store.items.length > 0" :props="listComponents.mail.store" />
      <ItemListComponent v-if="activeModules.includes('calendar') && listComponents.calendar.store.items.length > 0" :props="listComponents.calendar.store" />
      <ItemListComponent v-if="activeModules.includes('contacts') && listComponents.contacts.store.items.length > 0" :props="listComponents.contacts.store" />
      <ItemListComponent v-if="activeModules.includes('tasks') && listComponents.tasks.store.items.length > 0" :props="listComponents.tasks.store" />
      <div class="io-ox-busy" v-else-if="
        (activeModules.includes('mail') && listComponents.mail.store.status.value === 'searching' && listComponents.mail.store.items.length === 0) ||
        (activeModules.includes('calendar') && listComponents.calendar.store.status.value === 'searching' && listComponents.calendar.store.items.length === 0) ||
        (activeModules.includes('contacts') && listComponents.contacts.store.status.value === 'searching' && listComponents.contacts.store.items.length === 0) ||
        (activeModules.includes('tasks') && listComponents.tasks.store.status.value === 'searching' && listComponents.tasks.store.items.length === 0)"
      ></div>
    </div>
    <div v-if="infoMessage" class="infobox">
      <tagsIllustration/>
    <div>{{ infoMessage }}</div>
    </div>
  </div>
</template>

<script setup>
import folderAPI from '@/io.ox/core/folder/api'
import gt from 'gettext'
import _ from '@/underscore'
import { DateTime } from 'luxon'
import { LUXON_ZULU_FORMAT } from '@/io.ox/calendar/util'
import http from '@/io.ox/core/http'
import mailAPI from '@/io.ox/mail/api'
import MailItemComponent from '@/io.ox/core/categories/browser/mail-item.vue'
import CalendarItemComponent from '@/io.ox/core/categories/browser/calendar-item.vue'
import ContactItemComponent from '@/io.ox/core/categories/browser/contact-item.vue'
import TaskItemComponent from '@/io.ox/core/categories/browser/task-item.vue'
import ItemListComponent from '@/io.ox/core/categories/browser/item-list.vue'
import tagsIllustration from '@/io.ox/core/categories/browser/illustrations/illustration-ox-category.vue'
import { onMounted, reactive, ref, markRaw, onUnmounted } from 'vue'
import capabilities from '@/io.ox/core/capabilities'
import { CategoryDropdown } from '@/io.ox/core/categories/view'
import Backbone from '@/backbone'
import { categoriesCollection } from '@/io.ox/core/categories/api'
import { settings as coreSettings } from '@/io.ox/core/settings'
import BadgeList from '@/io.ox/core/categories/browser/badge-list.vue'
import { useDebounceFn } from '@vueuse/core'

const defaultMailFolderID = folderAPI.getDefaultFolder('mail')
const recentlyUsedAriaLabel = gt('Recently used')
const infoMessage = ref('')
const status = ref('default')
const unmountCallbacks = []

const { selectedCategoriesCollection } = defineProps(['selectedCategoriesCollection'])

// static data for item-lists
const storeTemplates = {
  mail: {
    // may be updated to specific inbox name if allMessagesFolder is unavailable.
    labelText: gt('Emails'),
    appId: 'io.ox/mail',
    type: 'mail',
    applySearchFilters (searchView, categories) {
      categories.forEach(category => { searchView.filters.add('user_flags', `"${category.name}"`) })
      searchView.filters.add('folder', mailAPI.allMessagesFolder || defaultMailFolderID)
    },
    ItemComponent: markRaw(MailItemComponent)
  },
  calendar: {
    labelText: gt('Appointments'),
    appId: 'io.ox/calendar',
    type: 'calendar',
    applySearchFilters (searchView, categories) {
      categories.forEach(category => { searchView.filters.add('categories', `"${category.name}"`) })
    },
    ItemComponent: markRaw(CalendarItemComponent)
  },
  contacts: {
    labelText: gt('Contacts'),
    appId: 'io.ox/contacts',
    type: 'contacts',
    applySearchFilters (searchView, categories) {
      categories.forEach(category => { searchView.filters.add('categories', `"${category.name}"`) })
    },
    ItemComponent: markRaw(ContactItemComponent)
  },
  tasks: {
    labelText: gt('Tasks'),
    appId: 'io.ox/tasks',
    type: 'tasks',
    applySearchFilters (searchView, categories) {
      const words = `*${categories.map(category => category.name).join('*')}*`
      searchView.filters.add('words', words)
      // needed because the "words" filter is tied to the main input field
      searchView.$input.val(words)
    },
    ItemComponent: markRaw(TaskItemComponent)
  }
}

// search api connections
const apis = {
  mail (categories = []) {
    return http.PUT({
      module: 'mail',
      params: {
        action: 'search',
        folder: mailAPI.allMessagesFolder || defaultMailFolderID,
        columns: http.defaultColumns.mail.search,
        sort: '661',
        order: 'desc',
        limit: 6
      },
      data: { filter: ['and'].concat(categories.map(category => ['=', { field: 'user_flags' }, category.id])) }
    })
  },
  calendar (categories = []) {
    const rangeStart = DateTime.now().minus({ months: 3 }).startOf('month').toFormat(LUXON_ZULU_FORMAT)
    const rangeEnd = DateTime.now().plus({ months: 3 }).endOf('month').toFormat(LUXON_ZULU_FORMAT)
    return http.PUT({
      module: 'chronos',
      params: {
        action: 'advancedSearch',
        expand: true,
        fields: 'lastModified,color,categories,createdBy,endDate,flags,folder,id,location,recurrenceId,rrule,seriesId,startDate,summary,timestamp,transp',
        rangeStart,
        rangeEnd
      },
      data: { filter: ['and'].concat(categories.map(category => ['=', { field: 'categories' }, `*${category.name}*`])) }
    }).then(response => {
      return response.map(folder => folder.events).flat()
    })
  },
  contacts (categories = []) {
    return http.PUT({
      module: 'addressbooks',
      params: {
        action: 'advancedSearch',
        columns: '20,1,100,101,500,501,502,505,508,510,519,520,524,526,528,555,556,557,569,592,597,602,606,607,616,617,5,2',
        sort: '607',
        timezone: 'utc',
        right_hand_limit: 6
      },
      data: { filter: ['and'].concat(categories.map(category => ['=', { field: 'categories' }, `*${category.name}*`])) }
    })
  },
  tasks (categories = []) {
    return http.PUT({
      module: 'tasks',
      params: {
        action: 'search',
        columns: '1,2,5,20,100,101,200,203,220,221,300,301,309,316,317,401',
        sort: '317',
        order: 'asc',
        timezone: 'utc',
        right_hand_limit: 6
      },
      data: { pattern: `*${categories.map(category => category.name).join('*')}*` }
    })
  }
}

function performSearch (module) {
  const searchCategories = selectedCategoriesCollection.toJSON()
  const store = listComponents[module].store
  store.status.value = 'searching'
  return apis[module](searchCategories).then(result => {
    // check if the current categories still match the searched categories.
    // If a request is really slow the user might have changed the selected categories already.
    const currentCategorySelection = searchCategories.map(category => category.id).join()
    const updatedCategorySelection = selectedCategoriesCollection
      .toJSON()
      .map(category => category.id)
      .join()
    if (currentCategorySelection !== updatedCategorySelection) return
    (result || []).forEach(item => {
      // add cids needed for detail views
      item.cid = _.cid(item)
      // use uniqueKey with added last modified data to make vue aware of changed items and trigger a redraw
      // some model changes or properties drawn via backbone views are not reactive yet.
      item.uniqueKey = `${item.cid}${item.last_modified ? '.' + item.last_modified : ''}`
      // mail is special and needs more attributes in the key to notice it has been updated
      // todo: improve this and make more extension points and backbone views etc reactive.
      if (module === 'mail') item.uniqueKey = `${item.uniqueKey}.${item.received_date}.${item.flags}.${item.user.join('.')}`
    })
    store.items.length = 0
    store.items.push(...result)
  }).always(() => { store.status.value = 'default' })
}

const searchAndUpdate = function (module) {
  performSearch(module)
  status.value = isEmpty() ? 'noResults' : 'results'
  updateInfoMessage()
}

// add active modules and lifecycle hooks
const activeModules = []
if (capabilities.has('webmail')) {
  activeModules.push('mail')
  import('@/io.ox/mail/api').then(({ default: mailAPI }) => {
    const callback = useDebounceFn(e => searchAndUpdate('mail'), 100)
    mailAPI.on('delete update:after archive after:all-seen', callback)
    unmountCallbacks.push(() => mailAPI.off('delete update:after archive after:all-seen', callback))
  })
}
if (capabilities.has('calendar')) {
  activeModules.push('calendar')
  import('@/io.ox/calendar/api').then(({ default: calendarAPI }) => {
    const callback = useDebounceFn(() => searchAndUpdate('calendar'), 100)
    calendarAPI.on('delete move update create process:create', callback)
    unmountCallbacks.push(() => calendarAPI.off('delete move update create process:create', callback))
  })
}
if (capabilities.has('contacts')) {
  activeModules.push('contacts')
  import('@/io.ox/contacts/api').then(({ default: contactsAPI }) => {
    const callback = useDebounceFn(() => searchAndUpdate('contacts'), 100)
    contactsAPI.on('delete move update create update:image', callback)
    unmountCallbacks.push(() => contactsAPI.off('delete move update create update:image', callback))
  })
}
if (capabilities.has('tasks')) {
  activeModules.push('tasks')
  import('@/io.ox/tasks/api').then(({ default: tasksAPI }) => {
    const callback = useDebounceFn(() => searchAndUpdate('tasks'), 100)
    tasksAPI.on('delete move update create', callback)
    unmountCallbacks.push(() => tasksAPI.off('delete move update create', callback))
  })
}
const listComponents = {}

activeModules.forEach(module => { listComponents[module] = { store: { ...storeTemplates[module], items: reactive([]), categories: reactive([]), status: ref('default') } } })
const recentCategoriesCollection = new Backbone.Collection(categoriesCollection.filter(category => coreSettings.get('categories/recentlyUsed', []).includes(category.id)))

const makeAPICalls = useDebounceFn(() => {
  if (selectedCategoriesCollection.length === 0) {
    status.value = 'default'
    updateInfoMessage()
    return activeModules.forEach(module => { listComponents[module].store.items.length = 0 })
  }

  // remove duplicates, make sure newest models are first then cut down to 3 models
  recentCategoriesCollection.remove(selectedCategoriesCollection.models)
  recentCategoriesCollection.unshift(selectedCategoriesCollection.models)
  recentCategoriesCollection.reset(recentCategoriesCollection.slice(0, 3))
  coreSettings.set('categories/recentlyUsed', recentCategoriesCollection.pluck('id')).save()

  status.value = 'searching'
  updateInfoMessage()

  const promises = []
  activeModules.forEach(module => {
    const store = listComponents[module].store
    store.categories.length = 0
    store.categories.push(...selectedCategoriesCollection.toJSON())
    promises.push(performSearch(module))
  })

  Promise.allSettled(promises).then(() => {
    status.value = isEmpty() ? 'noResults' : 'results'
    updateInfoMessage()
  })
}, 100)

selectedCategoriesCollection.on('add remove reset', makeAPICalls)

const categoriesDropdown = new CategoryDropdown({
  itemCategories: selectedCategoriesCollection,
  caret: true,
  buttonToggle: true,
  label: gt('Add category'),
  useToggleWidth: true
})

const dropdownPlaceholder = ref()
// update store label with inbox name if allMessagesFolder is unavailable.
// render elements that are not part of the template. Backbone views etc
onMounted(() => {
  dropdownPlaceholder.value.replaceWith(categoriesDropdown.render().el)
  // #. Title of section in category browser
  // #. %1$s: name of the mail inbox folder in case server does not support a all-messages-folder
  if (!mailAPI.allMessagesFolder) folderAPI.get(defaultMailFolderID).then(({ title }) => { listComponents.mail.store.labelText = gt('Emails (%1$s)', title) })
})

// trigger initial api calls
selectedCategoriesCollection.trigger('reset')

function isEmpty () {
  return activeModules.every(module => listComponents[module].store.items.length === 0)
}
function updateInfoMessage () {
  if (status.value === 'searching' || status.value === 'results') {
    infoMessage.value = ''
    return
  }

  if (status.value === 'noResults') {
    infoMessage.value = gt.ngettext('No items with this category found', 'No items with these categories found', selectedCategoriesCollection.length)
    return
  }
  infoMessage.value = gt('No category selected')
}

onUnmounted(() => {
  unmountCallbacks.forEach(callback => callback.call(this))
  selectedCategoriesCollection.off('add remove reset', makeAPICalls)
})

const categoriesString = gt('Categories')
const recentlyUsedString = gt('Recently used')
</script>

<style lang="scss">
@import '@/themes/imports';
#category-browser-main {
  position: absolute;
  width: 100%;
  height: 100%;
  padding-bottom: 16px;

  .categories-container {
    padding: 0px 16px 16px;
    .categories-badges { margin: 0; }
  }

  .dropdown-wrapper {
    // needed or dropdown opens at wrong position
    position: relative;
    margin: 8px 16px;
    height: 60px;
  }

  .item-list-container {
    overflow-y: auto;
    padding: 0 var(--gap);
    // 100% - header - category selector - selected categories list
    height: calc(100% - $appcontrol-height - 76px - 40px);
    display: flex;
    flex-direction: column;
    gap: 16px;
    .io-ox-busy {
      padding: 8px 16px;
      height: 48px;
    }
  }

  .infobox {
    // try to align this with the mail empty message and icon (no mail selected)
    top: calc(50% - 26px);
    width: 100%;
    position: absolute;
    display: flex;
    flex-direction: column;
    align-items: center;

    svg {
      width: 100px;
      height: 100px;
      color: var(--accent-300);
      // try to align this with the mail empty message and icon (no mail selected)
      margin-bottom: 12px;
    }
    div {
      color: var(--text-gray);
      font-size: 14px;
      text-align: center;
    }
  }
}
</style>
