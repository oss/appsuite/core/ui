/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import Backbone from '@/backbone'
import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import gt from 'gettext'
import ext from '@/io.ox/core/extensions'
import ListView from '@/io.ox/core/tk/list'
import DisposableView from '@/io.ox/backbone/views/disposable'
import { open as openCategoryBrowser } from '@/io.ox/core/categories/browser'

import { ErrorView, InputView } from '@/io.ox/backbone/mini-views/common'
import DropdownView from '@/io.ox/backbone/mini-views/dropdown'
import ModalDialog from '@/io.ox/backbone/views/modal'
import { createIcon, createButton } from '@/io.ox/core/components'
import { categoriesCollection, BaseCollection, getMailCategories } from '@/io.ox/core/categories/api'
import { CategoryModel } from '@/io.ox/core/categories/model'
import { ItemColorView, ItemIconView, ItemPicker } from '@/io.ox/core/categories/item-picker'
import { deriveCategoryColors, icons, colors } from '@/io.ox/core/categories/util'

import '@/io.ox/contacts/addressbook/style.scss'

const CategoryView = DisposableView.extend({

  className: 'category-view ellipsis',

  tagName: 'span',

  initialize (options = {}) {
    this.options = options
    this.$el
      .toggleClass('search', !!this.options.searchable)
      .attr('data-searchable', !!this.options.searchable)
      .attr('data-removable', !!this.options.removable)

    this.listenTo(this.model, 'change:icon change:name change:color', this.render)
    this.listenTo(ox, 'themeChange', this.render)
  },

  render () {
    if (this.disposed === true) return

    const icon = this.model.get('icon') !== 'none' && this.model.get('icon')
    const baseColor = this.model.get('color')
    const colors = deriveCategoryColors(baseColor)

    this.$el.empty()
      .append(
        icon ? createIcon(icon).addClass('bi-14 shrink-0') : $(),
        $('<span class="ellipsis">').text(this.model.get('name')),
        this.getControls()
      )
      .attr({
        'data-name': this.model.get('name'),
        'data-type': this.model.get('type'),
        'data-id': this.model.get('id')
      })
      .css({
        backgroundColor: colors.background,
        borderColor: baseColor === 'transparent' ? colors.border : colors.background,
        color: colors.foreground
      })

    return this
  },

  getControls () {
    if (!this.options.removable) return $()

    return createButton({
      variant: 'none',
      className: 'badge-remove-button flex-none p-0 border-0',
      icon: {
        name: 'bi/x-lg.svg',
        className: 'bi-14',
        // #. Used for removing a category with name from a pim object
        title: gt('Remove category %s', this.model.get('name'))
      }
    })
  }
})

export const CategoryBadgesView = DisposableView.extend({
  tagName: 'ul',
  className: 'categories-badges list-unstyled flex',

  events: {
    'click button.badge-remove-button': 'onChildDelete',
    'click button.search': 'onChildSearch'
  },

  initialize (options = {}) {
    this.collection = options.collection
    this.limit = options.limit

    this.childViewOptions = {}
    if (options.searchable) this.childViewOptions = { ...this.childViewOptions, tagName: 'button', searchable: true }
    if (options.removable) this.childViewOptions = { ...this.childViewOptions, removable: true }

    // reflect global category removal
    this.listenTo(categoriesCollection, 'remove', model => { this.collection.remove(model) })
    this.listenTo(this.collection, 'add remove reset', this.render)
  },

  onChildSearch (e) {
    const category = e.target.closest('.category-view').dataset
    // category browser is only available on desktop (for now)
    if (_.device('smartphone')) return ox.trigger('search:category', category)
    openCategoryBrowser([category.id])
  },

  onChildDelete (e) {
    const { name } = e.target.closest('.category-view').dataset
    const model = this.collection.findWhere({ name })
    this.collection.remove(model)
  },

  render () {
    this.$el.empty().append(
      this.collection
        .slice(0, this.limit ? this.limit : undefined)
        .map(model => $('<li class="zero-min-width">').append(
          new CategoryView({ model, ...this.childViewOptions }).render().$el
        )),
      this.renderCounter()
    )
    return this
  },

  renderCounter () {
    if (!this.limit || this.collection.length <= this.limit) return $()
    return $('<li class="categories-counter">').append(
      $('<span class="category-view ellipsis">').append(
        $(`<span class="ellipsis counter-text shrink-0">+${this.collection.length - this.limit}</span>`)
      )
    )
  }
})

const CategoryListItem = DisposableView.extend({
  className: 'category-list-item flex justify-between items-center pb-8',
  tagName: 'li',

  events: {
    'click .delete': 'onClickDelete',
    'click .edit': 'onClickEdit'
  },

  initialize () {
    this.categoryBadge = new CategoryView({ model: this.model })
  },

  onClickEdit (e) {
    e.preventDefault()
    openUpdateCategoryModal({
      title: gt('Edit category'),
      label: gt('Save'),
      model: this.model
    })
  },

  onClickDelete (e) {
    e.preventDefault()
    openDeleteCategoryModal({ model: this.model })
  },

  render () {
    this.$el.append(this.categoryBadge.render().el)

    if (this.model.get('immutable')) return this

    this.$el.append(
      $('<div class="list-item-controls flex pr-8">').append(
        createButton({
          className: 'edit border-none ml-4',
          variant: 'link',
          icon: {
            name: 'bi/pencil.svg',
            className: 'bi-15',
            // #. Tooltip text of icon-based button to edit a category
            // #. %1$s: category name
            title: gt('Edit category %1$s', this.model.get('name'))
          }
        }),
        createButton({
          className: 'delete border-none ml-4',
          variant: 'link',
          icon: {
            name: 'bi/trash.svg',
            className: 'bi-15',
            // #. Tooltip text of icon-based button to delete a category
            // #. %1$s: category name
            title: gt('Delete category %1$s', this.model.get('name'))
          }
        })
      )
    )

    return this
  }
})

const CategoryListView = ListView.extend({
  className: 'category-list-view list-unstyled mb-0',
  tagName: 'ul',

  initialize () {
    this.listenTo(categoriesCollection, 'update', this.render)
  },

  render () {
    this.$el.empty().append(
      categoriesCollection
        .filter(model => model.get('type') !== 'shared-item' && !model.get('ai'))
        .map(model => { return new CategoryListItem({ model }).render().el })
    )
    return this
  }
})

export const CategoryDropdownMulti = DropdownView.extend({

  className: 'category-dropdown multi',

  initialize (options) {
    DropdownView.prototype.initialize.apply(this, arguments)

    this.prefix = options.prefix || ''
    this.appPrefix = options.appPrefix || 'unknown'
    this.list = options.data || []
    this.itemCategories = new BaseCollection()
    this.references = this.list.map(mail => `${this.appPrefix}:${mail.cid}`)

    const models = this.list.map(mail => {
      const ref = `${this.appPrefix}:${mail.cid}`
      return getMailCategories(mail.user, ref).models
    }).flat()

    this.itemCategories.add(models)
    this.model = new Backbone.Model()

    // initial value for dropdown models
    this.itemCategories.forEach(categoryModel => this.model.set(categoryModel.get('name'), true))

    this.listenTo(this.model, 'change', this.onChange)
    this.listenTo(this.itemCategories, 'remove', this.removeCategory)

    // categories created, removed or changed
    this.listenTo(categoriesCollection, 'add', () => this.render())
    this.listenTo(categoriesCollection, 'remove', this.removeCategory)
    this.listenTo(categoriesCollection, 'change:name change:color change:icon', this.updateCategory)
  },

  updateCategory (model) {
    const nameOld = model.previous('name')
    const name = model.get('name')
    if (name === nameOld) return this.render()
    // update dropdown state (checked) for renamed category
    const checked = this.model.get(nameOld)
    this.model.set(name, checked, { silent: true })
    this.model.unset(nameOld, { silent: true })
    this.render()
  },

  removeCategory (categoryModel) {
    this.model.unset(categoryModel.get('name'), { silent: true })
    this.render()
  },

  updateItemCategories () {
    Object.entries(this.model.attributes).forEach(([name, isChecked]) => {
      const categoryModel = categoriesCollection.findWhere({ name })
      return isChecked
        ? this.itemCategories.add(categoryModel)
        : this.itemCategories.remove(categoryModel)
    })
  },

  onChange (model, options = { _origin: 'dropdown' }) {
    const source = options._origin || 'dropdown'
    // model is lastest => update itemModel categories
    if (source === 'dropdown') this.updateItemCategories()

    const add = []
    const remove = []
    Object.entries(this.model.changed).forEach(([name, isChecked]) => {
      const categoryModel = categoriesCollection.findWhere({ name })
      const list = isChecked ? add : remove
      list.push(categoryModel.get('id'))
    })
    this.trigger('categories:change', { add, remove })
    this.render()
  },

  render () {
    this.$el.addClass(this.className)
    this.$ul.empty()

    // If not shared item or an unchecked AI category, filter out
    const isCheckmarked = name => this.model.get(name) !== undefined
    const userCategories = categoriesCollection.filter(model => model.get('type') !== 'shared-item' && (!model.get('ai') || isCheckmarked(model.get('name'))))
    const sharedCategories = categoriesCollection.filterByRef(this.references)

    // #. Categories available to the user
    if (sharedCategories.length !== 0) this.header(gt('User Categories'))
    userCategories.forEach(model => {
      this.option(
        model.get('name'), true, () => new CategoryView({ model, tagName: 'div' }).render().el
      )
    })

    if (sharedCategories.length !== 0) {
      // #. Categories available to the user through sharing
      this.header(gt('Shared Categories'))
      sharedCategories.forEach((model) => {
        this.option(
          model.get('name'), true, () => new CategoryView({ model, tagName: 'div' }).render().el
        )
      })
    }

    this.divider()
    this.link(
      'dropdown-min-width',
      // #. Opens the Manage categories dialog
      `${gt('Manage categories')} …`,
      () => openManageCategoryModal({ previousFocus: this.$toggle }),
      { data: 'dropdown-min-width' }
    )

    DropdownView.prototype.render.apply(this)
    ext.point('io.ox/core/dropdown').invoke('redraw', this)
    return this
  }
})

export const CategoryDropdown = DropdownView.extend({
  className: 'category-dropdown',

  initialize (options) {
    options.title = options.title || gt('Categories')
    DropdownView.prototype.initialize.apply(this, arguments)
    this.ref = options.ref
    this.itemModel = options.itemModel
    this.itemCategories = options.itemCategories
    this.model = new Backbone.Model()

    // initial value for dropdown models
    this.itemCategories.forEach(categoryModel => this.model.set(categoryModel.get('name'), true))
    this.render()

    // ensure (dropdown) model AND itemCategories AND itemModel value are in sync
    this.listenTo(this.model, 'change', this.onChange)
    this.listenTo(this.itemCategories, 'remove', this.removeCategory)
    this.listenTo(categoriesCollection, 'remove', this.removeCategory)
    this.listenTo(categoriesCollection, 'update', this.render)
  },

  removeCategory (categoryModel) {
    this.model.unset(categoryModel.get('name'), { _origin: 'badge' })
  },

  updateItemCategories () {
    Object.entries(this.model.attributes).forEach(([name, isChecked]) => {
      const categoryModel = categoriesCollection.findWhere({ name })
      return isChecked
        ? this.itemCategories.add(categoryModel)
        : this.itemCategories.remove(categoryModel)
    })
  },

  onChange (model, options = { _origin: 'dropdown' }) {
    const source = options._origin || 'dropdown'
    // model is lastest => update itemModel categories
    if (source === 'dropdown') this.updateItemCategories()

    if (this.itemModel) {
      // update itemModel value
      this.itemModel.set('categories', Object.entries(this.model.attributes)
        .map(([name, isChecked]) => isChecked && categoriesCollection.findWhere({ name })?.get('name'))
        .filter(Boolean)
        .join(',')
      )
    }
    this.render()
  },

  render () {
    this.$ul.empty()

    const userCategories = categoriesCollection.filter(model => model.get('type') !== 'shared-item' && !model.get('ai'))
    const sharedCategories = this.ref ? categoriesCollection.filterByRef(this.ref) : []

    // #. Categories available to the user
    if (sharedCategories.length !== 0) this.header(gt('User Categories'))
    this.renderCategories(userCategories)

    // #. Categories available to the user through sharing
    if (sharedCategories.length !== 0) this.header(gt('Shared Categories'))
    this.renderCategories(sharedCategories)

    this.divider()
    this.link(
      'dropdown-min-width',
      // #. Opens the Manage categories dialog
      `${gt('Manage categories')} …`,
      () => openManageCategoryModal({ previousFocus: this.$toggle }),
      { data: 'dropdown-min-width' }
    )

    DropdownView.prototype.render.apply(this)
    return this
  },

  renderCategories (models) {
    models.forEach(model => {
      this.option(
        model.get('name'), true, () => new CategoryView({ model, tagName: 'div' }).render().el
      )
      this.stopListening(model, 'change:icon change:name change:color', this.render)
      this.listenTo(model, 'change:icon change:name change:color', this.render)
    })
  }
})

// Create/Edit Modal
function openUpdateCategoryModal (options) {
  const { model, title, label } = options
  const isCreate = !model.get('name')
  const isSmallDevice = _.device('smartphone') && window.innerWidth <= 450

  const modal = new ModalDialog({
    title,
    backdrop: true,
    width: 426,
    point: 'io.ox/core/category/edit',
    autoClose: false
  })
    .build(function () {
      this.modifiedModel = new CategoryModel({ ...model.toJSON(), oldcid: model.cid })
    })
    .extend({
      body () {
        this.$el.addClass('category-modal category-modal-update')
      },
      form () {
        this.$body.append(
          $('<form class="edit_category">').append(
            $('<fieldset class="main form-horizontal">')
          ).on('submit', e => { e.preventDefault() })
        )
      },
      name () {
        const { modifiedModel } = this
        this.$('fieldset.main').append(
          $('<div class="category-form-group name-group">').append(
            $('<label class="control-label" for=name>').text(gt('Name')),
            new InputView({
              model: modifiedModel,
              id: 'name',
              trim: true,
              maxlength: 50,
              attributes: {
                disabled: modifiedModel.get('immutable')
              }
            }).render().$el,
            new ErrorView({
              name: 'name',
              model,
              selector: '.category-form-group'
            }).render().$el
          )
        )
      },
      color () {
        const { modifiedModel } = this
        this.$('fieldset.main').append(
          $('<fieldset class="category-form-group color-group">').append(
            $('<legend id="categories-colorselection">').text(gt('Color')),
            new ItemPicker({
              model: modifiedModel,
              attribute: 'color',
              items: colors(),
              ItemView: ItemColorView
            }).render().$el
          ).toggleClass('scrollable', isSmallDevice)
        )
      },
      icon () {
        const { modifiedModel } = this
        this.$('fieldset.main').append(
          $('<fieldset class="category-form-group icon-group" role="radiogroup" aria-labelledby="categories-iconselection">').append(
            $('<legend id="categories-iconselection">').text(gt('Icon')),
            new ItemPicker({
              model: modifiedModel,
              attribute: 'icon',
              items: icons,
              ItemView: ItemIconView
            }).render().$el
          ).toggleClass('scrollable', isSmallDevice)
        )
      },
      states () {
        const { modifiedModel } = this
        // toggle save button state
        this.listenTo(modifiedModel, 'invalid', () => {
          this.$('button[data-action=save]')[0].disabled = true
        })
        this.listenTo(modifiedModel, 'valid', () => {
          this.$('button[data-action=save]')[0].disabled = false
        })
        // reset model to 'valid' when error was displayed and input value is changed by user after that
        this.$('#name').on('input', () => {
          if (modifiedModel.isValid()) return
          modifiedModel.trigger('valid')
        })
      }
    })
    .addCancelButton()
    .addButton({ label, action: 'save' })
    .on('save', function () {
      let { modifiedModel } = this
      if (!modifiedModel.isValid({ isSave: true })) return
      // If hidden AI generated category already exist with same name, modify instead of creating
      const aiModel = categoriesCollection.models.find(model =>
        model.get('name').toLowerCase() === modifiedModel.get('name').toLowerCase() &&
        model.get('ai')
      )
      if (isCreate && aiModel) {
        modifiedModel.set('id', aiModel.get('id'))
        modifiedModel.set('ai', false)
        aiModel.set(modifiedModel.toJSON())
        modifiedModel = aiModel
      }

      // if this is a shared category, reset `references` to add this as user category which can be saved
      modifiedModel.set({ references: [], type: 'user' })
      model.set(modifiedModel.toJSON())

      if (isCreate && !aiModel) {
        categoriesCollection.add(model)
      } else {
        categoriesCollection.saveUserCategories()
      }

      categoriesCollection.trigger('update')
      this.close()
    })

  return modal.open()
}

// Delete confirmation
function openDeleteCategoryModal (options) {
  const modal = new ModalDialog(
    {
      // #. Dialog for deleting a category
      title: gt('Delete category'),
      backdrop: true,
      // #. Dialog prompt for deleting a category with name
      description: gt('Do you really want to delete the category "%s"?', options.model.get('name'))
    }
  )
    .inject(
      {
        getModel () {
          return options.model
        }
      }
    )
    .addCancelButton()
    .addButton({ label: gt('Delete'), action: 'delete' })
    .on('delete', function () {
      categoriesCollection.remove(this.getModel())
    })

  return modal.open()
}

export function openManageCategoryModal ({ previousFocus }) {
  const modal = new ModalDialog({
    // #. Dialog header for managing categories
    title: gt('Manage categories'),
    backdrop: true,
    width: 420,
    autoClose: false,
    help: 'ox.appsuite.user.sect.dataorganisation.categories.manage.html',
    previousFocus
  })
    .build(function () {
      this.$el.addClass('category-modal')
      this.$body.append(
        new CategoryListView().render().$el
      )
    })
    .addButton({
      placement: 'left',
      className: 'btn-default',
      // #. Button to create a new category
      label: gt('New category'),
      action: 'open-new-category'
    })
    .addCloseButton()
    .on('open-new-category', () =>
      openUpdateCategoryModal({
        title: gt('Create category'),
        label: gt('Create'),
        model: new CategoryModel({})
      })
    )
    .on('save', () => {
      categoriesCollection.saveUserCategories()
      modal.close()
    })

  modal.$el.addClass('category-modal')
  return modal.open()
}

export const AutoCompleteCategoriesItems = {
  render ($el, list, offset = 0, limit = 20) {
    const el = $el[0]
    const subset = list.slice(offset, limit).map(item => categoriesCollection.get(item.cid))
    if (offset === 0) el.innerHTML = ''
    const template = subset.map(model => {
      return $(`<li class="list-item selectable" aria-selected="false" role="option" tabindex="-1" data-cid="${model.cid}">`).append(
        $('<div class="list-item-checkmark">'),
        $('<div class="list-item-content">').append(
          new CategoryView({ model, tagName: 'div' }).render().el
        )
      )
    })
    $el.append(template)
    if (offset === 0) el.scrollTop = 0
  }
}
