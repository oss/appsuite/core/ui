/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import categoryBrowser from '@/io.ox/core/categories/browser/category-browser.vue'
import { createApp } from 'vue'
import Backbone from '@/backbone'
import { categoriesCollection } from '@/io.ox/core/categories/api'
import { sidepanelView } from '@/io.ox/core/sidepanel'
import gt from 'gettext'

let firstRun = true
const selectedCategoriesCollection = new Backbone.Collection()

export function open (categories = []) {
  if (categories.length) selectedCategoriesCollection.reset(categoriesCollection.filter(category => categories.includes(category.id)))
  if (firstRun) {
    const widget = sidepanelView.add('category-browser', gt('Browse categories'), {
      helpUrl: 'ox.appsuite.user.sect.dataorganisation.categories.browse.html'
    })
    widget.$content.attr('id', 'io-ox-category-browser')
    const categoryBrowserApp = createApp(categoryBrowser, { selectedCategoriesCollection })
    categoryBrowserApp.mount(widget.$content[0])
    firstRun = false
  }
  sidepanelView.makeActive('category-browser')
}
