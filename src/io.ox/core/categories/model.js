/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import BasicModel from '@/io.ox/backbone/basicModel'
import { categoriesCollection, userFlagPrefix } from '@/io.ox/core/categories/api'
import { settings as coreSettings } from '@/io.ox/core/settings'

import gt from 'gettext'

ext.point('io.ox/core/categories/model/validation/save').extend([{
  id: 'name-uniqueness',
  validate ({ name, oldcid }, err, model) {
    // only search in user categories
    const isTaken = categoriesCollection.where({ name })
      .filter(model => model.type !== 'shared-item')
      .filter(model => !model.get('ai'))
      .some(c => c.cid !== oldcid)
    if (!isTaken) return
    // #. Used for validation when creating categories
    this.add('name', gt('Name is already taken.'))
  }
}, {
  id: 'name-empty',
  validate (attr, err, model) {
    if (attr.name) return
    // #. Used for validation when creating categories
    this.add('name', gt('Please enter a name.'))
  }
}, {
  id: 'name-contains-separator',
  validate (attr, err, model) {
    if (!attr.name.includes(',')) return
    // #. Used for validation when creating categories
    this.add('name', gt('Comma is not allowed.'))
  }
}])

function generateUniqueID (type = 'user') {
  // ensure uniqueness user-category ids (including potentially deleted ones)
  let number = coreSettings.get('categories/serialNumber', 0)
  coreSettings.set('categories/serialNumber', ++number).save()
  const paddedNumber = String(number).padStart(4, '0')
  let id = `${userFlagPrefix}_${type}_${paddedNumber}`
  if (type === 'user') id += `_${ox.user_id}`
  return id
}

export const CategoryModel = BasicModel.extend({
  ref: 'io.ox/core/categories/model/',
  defaults: {
    name: '',
    color: 'transparent',
    icon: 'none',
    immutable: false,
    type: 'user',
    // used to add references to shared items (shared categories)
    references: []
  },
  initialize (options = {}) {
    BasicModel.prototype.initialize.call(this, options)
    if (this.has('id')) return
    this.set('id', generateUniqueID(this.get('type')), { silent: true })
  },
  addReference (reference) {
    const references = this.get('references').concat(reference)
    // unique
    this.set('references', [...new Set(references)])
  },
  removeReference (reference) {
    this.get('references').splice(this.get('references').indexOf(reference), 1)
  }
})
