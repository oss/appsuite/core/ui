/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import ox from '@/ox'
import Backbone from '@/backbone'
import { settings as coreSettings } from '@/io.ox/core/settings'
import { parseStringifiedList } from '@/io.ox/core/util'
import { CategoryModel } from '@/io.ox/core/categories/model'
import apps from '@/io.ox/core/api/apps'
import { hasFeature } from '@/io.ox/core/feature'
import { settings } from '@/io.ox/mail/settings'

export const userFlagPrefix = '$ct'

const predefinedCategories = [{
  id: '$ct_predefined_0001',
  name: 'Important',
  color: '#ff2968',
  icon: 'bi/exclamation-circle.svg'
}, {
  id: '$ct_predefined_0002',
  name: 'Business',
  color: '#16adf8',
  icon: 'bi/briefcase.svg'
}, {
  id: '$ct_predefined_0003',
  name: 'Private',
  color: '#707070',
  icon: 'bi/house-door.svg'
}, {
  id: '$ct_predefined_0004',
  name: 'Meeting',
  color: '#63da38',
  icon: 'bi/people.svg'
}]

export const BaseCollection = Backbone.Collection.extend({
  model: CategoryModel
})

const CategoriesCollection = BaseCollection.extend({
  initialize () {
    const groups = { default: [], predefined: [], user: [] }

    // if user deleted all categories, ensure at least default categories are available
    groups.default = Array(coreSettings.get('categories/default', [])).flat()
      .map(category => ({ ...category, type: 'default', immutable: true }))

    // admin categories are not user-changable
    groups.predefined = Array(coreSettings.get('categories/predefined', predefinedCategories)).flat()
      .map(category => ({ ...category, type: 'predefined', immutable: true }))

    groups.user = Array(coreSettings.get('categories/userCategories', [])).flat()
      .map(category => ({ ...category, type: 'user' }))

    // prefer predefined categories over user categories (same name)
    const initialSet = []
    const categories = [...groups.predefined, ...groups.user.length ? groups.user : groups.default]
    categories.forEach(category => {
      if (initialSet.find(({ name }) => name === category.name)) return
      initialSet.push(category)
    })

    this.reset(initialSet)
    this.saveUserCategories()

    // store user categories after any change
    this.on('add remove', _.debounce(this.saveUserCategories, 300))
  },

  add (data, options) {
    const list = [].concat(data)
      .filter(Boolean)
      .map(data => {
        // object categories handling
        const name = data.get?.('name') || data.name
        const ref = data.get?.('ref') || data.ref
        const model = this.findWhere({ name, type: 'shared-item' })
        if (!model) {
          data.references = []
          if (ref) data.references.push(ref)
          delete data.ref
          return data
        }
        model.addReference(ref)
        return null
      })
      .filter(Boolean)
    BaseCollection.prototype.add.call(this, list, options)
  },

  findByName (name, ref) {
    // user category
    // mail adds references to user categories, do not count those or we create category duplicates for other apps.
    // (open mail with category important => then open appointment with category important => broken)
    const userCategory = this.where({ name }).filter(model => model.get('references').filter(ref => !ref.startsWith('mail:')).length === 0).shift()
    if (userCategory) return userCategory
    // shared category as fallback
    return this.find(model => {
      return model.get('name') === name &&
      model.get('references').includes(ref) &&
      model.get('type') === 'shared-item'
    })
  },

  filterByRef (ref) {
    const references = [].concat(ref)
    // get (shared) categories or specified item (identified by ref)
    return this
      .filter(model => model.get('type') === 'shared-item')
      .filter(model => references.every(
        ref => model.get('references').includes(ref))
      )
  },

  addObjectCategories (list, ref) {
    // app items (mails, contacts, tasks, appointments) can be shared, thus they might not be known in userCategories
    // These categories are added to categories containing it's object reference (`references`)
    // Users can import shared categories (implicitely) by editing them, which removes all references
    list
      .filter(name => !this.findByName(name, ref))
      .map(name => { return this.add({ name, ref, type: 'shared-item', immutable: true }) })
      .filter(Boolean)
  },

  saveUserCategories () {
    const list = this.filter(model => {
      if (model.get('type') === 'shared-item') return false
      if (model.get('type') === 'predefined') return false
      return true
    }).map(model => {
      const { id, name, color, icon, ai } = model.toJSON()
      return { id, name, color, icon, ai }
    })
    coreSettings.set('categories/userCategories', list).save()
  }
})

export const categoriesCollection = new CategoriesCollection()
const useAICategory = () => apps.get('io.ox/mail')?.useCategories() && hasFeature('ai') && settings.get('ai/useAICategories', false)

export function getMailCategories (list = [], reference, includeAI = false) {
  // Defined within AI project / plugin
  list = _.isArray(list) ? list : parseStringifiedList(list)
  const models = list
    .filter(id => id.startsWith(`${userFlagPrefix}_`))
    .filter(id => includeAI || useAICategory() || !(categoriesCollection?.get(id))?.get('ai'))
    .map(id => {
      if (!categoriesCollection.get(id) && ox.debug) console.log(`Missing category ${id} of mail ${reference}`)
      return categoriesCollection.get(id)
    })
    .filter(Boolean)
  models.forEach(model => model.addReference(reference))
  return new BaseCollection(models)
}

// map category names (that serve as IDs) to known categories, or to new blank categories if not yet known
export function getCategoriesFromModel (propertyValue = [], reference, prefix = '') {
  const categoryNames = parseStringifiedList(propertyValue).map(name => name.replace(prefix, ''))
  // side effect: import shared categories into the user categories to make them editable (except for mail)
  if (!prefix) categoriesCollection.addObjectCategories(categoryNames, reference)

  return new BaseCollection(
    categoryNames.map(name => categoriesCollection.findByName(name, reference)).filter(Boolean)
  )
}
