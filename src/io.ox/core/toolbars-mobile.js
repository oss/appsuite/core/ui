/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import Backbone from '@/backbone'
import $ from '@/jquery'
import ToolbarView from '@/io.ox/backbone/views/toolbar'

/**
 * Abstract Barview
 * Just a superclass for toolbar and navbar
 * Holds some shared
 */
const BarView = Backbone.View.extend({
  show () {
    this.$el.show()
    return this
  },
  hide () {
    this.$el.hide()
    return this
  }
})

const NavbarView = BarView.extend({

  tagName: 'div',

  className: 'actions flex items-center py-6 ps-12 pe-16 justify-between',

  /**
   * only buttons which do NOT include the .custom class
   * will trigger the navbars onLeft and onRight events
   * For custom actions and links in navbar one must include
   * the .custom class to prevent the view to kill the clickevent
   * early and spawn a onLeft or onRight action
   */
  events: {
    'click .left-action:not(.custom) :not(.edit, span)': 'onLeftAction',
    'click .right-action:not(.custom) :not(.edit, span)': 'onRightAction',
    'click button.edit:not(.custom)': 'toggleEditMode'
  },

  initialize (options) {
    this.left = options.left || $()
    this.right = options.right || $()
    this.baton = options.baton
    this.hiddenElements = []
  },

  render () {
    this.$el.empty()

    this.$el.append(
      $('<div class="left-action">').append(this.left),
      $('<div class="right-action">').append(this.right)
    )

    // hide all hidden elements
    this.$el.find(this.hiddenElements.join()).hide()

    const justify = this.right && !this.left
    this.$el.toggleClass('justify-end', justify)
    this.$el.toggleClass('justify-between', !justify)

    return this
  },

  renderLeft () {
    this.$el.find('.left-action').empty().append(this.left)
  },

  renderRight () {
    this.$el.find('.right-action').empty().append(this.right)
  },

  setLeft ($node) {
    this.left = $node
    this.renderLeft()
    return this
  },

  setRight ($node) {
    this.right = $node
    this.renderRight()
    return this
  },

  toggleEditMode (e) {
    e.preventDefault()
    this.trigger('editMode')
  },

  onLeftAction (e) {
    e.preventDefault()
    this.trigger('leftAction')
  },

  onRightAction (e) {
    e.preventDefault()
    this.trigger('rightAction')
  },

  hide (elem) {
    this.hiddenElements.push(elem)
    this.hiddenElements = _.uniq(this.hiddenElements)
    this.render()
    return this
  },

  show (elem) {
    this.hiddenElements = _.without(this.hiddenElements, elem)
    this.render()
    return this
  },

  setBaton (baton) {
    this.baton = baton
    this.render()
    return this
  },

  toggle (state) {
    this.$el.toggle(state)
  }
})

const AppHeaderView = BarView.extend({

  tagName: 'div',

  className: 'app-header-container flex-row justify-between items-start',

  events: {
    'click .edit:not(.custom)': 'toggleEditMode'
  },

  initialize (options) {
    this.$title = options.$title || $('<span class="title-lg font-bold">')
    this.$subTitle = options.$subTitle || $('<span class="title-sm mt-4">')
    this.$leftside = options.$leftside || $('<div class="context flex-col">')
    this.$rightside = options.$rightside || $('<div class="right-side">')
    this.buttons = options.buttons || $()
  },

  render () {
    this.$el.empty()

    this.$el.append(
      this.$leftside.append(
        this.$title,
        this.$subTitle
      ),
      this.$rightside.append(this.buttons)
    )

    return this
  },

  setTitle (title) {
    this.$title.text(title)
    return this
  },

  setSubtitle (subtitle) {
    this.$subTitle.text(subtitle)
    return this
  },

  setButtons (buttons) {
    this.buttons = buttons
    this.renderButtons()
    return this
  },

  renderButtons () {
    this.$rightside.empty().append(this.buttons)
  },

  toggleEditMode (e) {
    e.preventDefault()
    this.trigger('editMode')
  }
})

const MobileToolbarView = BarView.extend({

  initialize (options) {
    this.page = options.page
    this.baton = options.baton
    this.extension = options.extension
  },
  render () {
    const baton = this.baton
    const point = `${this.extension}/${this.page}/${baton.isDraft ? 'draft/' : ''}links`
    this.$el
      .empty()
      .append(new ToolbarView({ point, title: baton?.app?.getTitle(), inline: true })
        .setSelection(baton.array(), function () {
          const options = _(baton).pick('models', 'collection', 'allIds')
          options.data = baton.data
          options.isThread = baton.isThread
          options.folder_id = null
          if (baton.app) {
            options.app = baton.app
            options.folder_id = baton.app.folder.get()
          }
          // some detail view inline toolbars have a model attribute
          if (baton.model) {
            options.model = baton.model
          }
          return options
        }).$el)

    // Disable "More Actions" dropdown button if no mail is selected
    this.$el.find('.more-dropdown button').prop('disabled', baton.selection && !baton.selection.length)

    return this
  },
  setBaton (baton) {
    this.baton = baton
    this.render()
    return this
  }
})

export default {
  BarView,
  NavbarView,
  MobileToolbarView,
  AppHeaderView
}
