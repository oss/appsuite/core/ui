/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import { pinia } from '@/pinia'

/**
 * This plugin invokes the `io.ox/store` extension point to allow extensions
 * to provide their own stores to vue components.
 *
 * In vue components all registered stores will be injected with the corresponding useStore function.
 * So it is possible to do this:
 *
 * ```js
 * import { inject } from 'vue'
 *
 * // this can be done for all stores registered at the extension point
 * const useExampleStore = inject('useExampleStore', () => ({ some: 'defaults' }))
 * ```
 *
 * When implementing an extension on the store, the extension is expected to have a function
 * `provide` that takes a Baton object as argument.
 * The Baton object has the `app` and `options` as properties and the implementation should
 * look something like:
 *
 * ```js
 * ext.point('io.ox/store').extend({
 *   id: 'useExampleStore',
 *   provide (baton) {
 *     const { app } = baton.data
 *     // baton.extension.id is useExampleStore in our case.
 *     app.provide(baton.extension.id, useExampleStore)
 *   },
 *   instance () { return useExampleStore() }
 * })
 * ```
 *
 * The `instance` function can be used to create a new instance of the store, just like calling the useStore function
 * of the store.
 * When consuming the store outside a component this is useful to get the store instance:
 *
 * ```js
 * const exampleStore = ext.point('io.ox/store').get('useExampleStore').instance()
 * ```
 *
 * @type {import('vue').Plugin}
 */
export const provideStoresPlugin = {
  install (app, options) {
    app.use(pinia)
    // configure the app
    ext.point('io.ox/store').invoke('provide', null, ext.Baton.ensure({ app, options }))
  }
}
