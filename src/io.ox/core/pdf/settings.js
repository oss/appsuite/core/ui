/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'

// Safari iOS has a lazy gfx cache, using large canvas sizes can quickly result in out of memory exceptions
export const MAX_CANVAS_PIXELS = _.device('ios || android') ? 5242880 : 16777216

// Fixed factor used by pdfjs for rendering canvas.
export const PDF_TO_CSS_UNITS = 96.0 / 72.0

// The url to the predefined Adobe CMaps.
export const CMAP_URL = './pdfjs/cmaps/'

// The url to the pdfjs web worker.
export const WORKER_PATH = './pdfjs/pdf.worker.min.mjs'

// Path for image resources, mainly for annotation icons. Include trailing slash.
export const IMAGE_PATH = './pdfjs/web/images/'
