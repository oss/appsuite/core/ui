/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'

import * as pdfjsViewer from 'pdfjs-dist/web/pdf_viewer'
import { MAX_CANVAS_PIXELS, IMAGE_PATH } from '@/io.ox/core/pdf/settings'
import { clearCanvas } from '@/io.ox/core/pdf/util'

import 'pdfjs-dist/web/pdf_viewer.css'

// overwrites pdfjs styles
import '@/io.ox/core/pdf/pdfstyle.scss'

/**
 * The pdf viewer application. Creates the HTML representation of the document pages.
 *
 *  Notes: Some function names can be discussed, but the idea is to kept function names
 *        and constructions similar to pdfjs for easier reference.
 *
 * @constructor
 *
 * @param {pdfDocument} pdfDocument   The pdfDocument model created by the PDFDocumentManager.
 *
 * @param {Object} options
 *    @param {Number} [options.initialZoomLevel]
 *    @param {Number} [options.initialScrollPosition]
 *
 */
export default class PDFViewerApplication {
  #pdfDocument = null
  #pdfViewer = null
  #pdfReadyPromise = $.Deferred()
  #pdfLinkService = null
  #initialZoomLevel = null
  #initialScrollPosition = null

  eventBus = null
  $viewer = $('<div class="pdfViewer">')
  $scaler = $('<div class="io-ox-pdf-scaler">')
  // FF needs tabindex -1 or it will be a focus target
  $container = $('<div class="io-ox-pdf-container viewerContainer scrollable" tabindex="-1">').append(this.$scaler.append(this.$viewer))

  constructor (pdfDocument, options) {
    this.#pdfDocument = pdfDocument
    this.#initialZoomLevel = options?.initialZoomLevel || 1
    this.#initialScrollPosition = options?.initialScrollPosition || 0
  }

  /**
   * Closes PDFViewerApplication instance.
   *  Note: Should be destroyed after the 'pdfDocumentManager'.
   */
  close () {
    this.#pdfReadyPromise.reject('close')

    // iOS has a small canvas cache and lazy garbage collection, clean up
    // existing canvas objects to prevent out of memory problems over time.
    // Can be monitored at iOS with Safari debugger open, watching the Graphics" section.
    const canvases = this.$viewer?.find('canvas')
    if (canvases?.length) {
      canvases.each((i, canvas) => {
        clearCanvas(canvas)
      })
    }

    this.#pdfViewer?.setDocument(null)
    this.#pdfLinkService?.setDocument(null, null)
  }

  get page () {
    return this.#pdfViewer.currentPageNumber
  }

  set page (val) {
    let existingPageNumber = val < 1 ? 1 : val
    existingPageNumber = val > this.#pdfDocument.numPages ? this.#pdfDocument.numPages : existingPageNumber
    this.#pdfViewer.currentPageNumber = existingPageNumber
  }

  setZoom (zoom) {
    this.#pdfViewer.currentScaleValue = zoom
    // use-case: 'auto' should not create too big scales on a large screen
    if (zoom === 'auto' && this.#pdfViewer.currentScale > 1) {
      this.#pdfViewer.currentScaleValue = 1
    }
  }

  getZoom () {
    return this.#pdfViewer.currentScale * 100
  }

  /**
   * Initialize the pdfViewerApplication, which contains the canvas, textLayer and annotationLayer.
   *
   * @returns {Promise}   Returns a promise, which is resolved when
   *                      the pdf is ready to view.
   */
  initUI () {
    const eventBus = new pdfjsViewer.EventBus()
    this.eventBus = eventBus

    const linkService = new pdfjsViewer.PDFLinkService({
      eventBus,
      externalLinkTarget: 2 // open external links in a new window
    })
    this.#pdfLinkService = linkService

    const container = this.$container[0]
    const viewer = this.$viewer[0]
    const pdfViewer = new pdfjsViewer.PDFViewer({
      container,
      viewer,
      eventBus,
      linkService,
      maxCanvasPixels: MAX_CANVAS_PIXELS,
      textLayerMode: 1,
      annotationEditorMode: 0,
      // Workaround: provide a Noop to fix sporadic typeErrors in pdfjs when destroying pdfs fast:
      // - altTextManager.destroy() is called but not altTextManager is not defined in the pdfjs destructor
      // - use-case: have many long pdfs in a folder an switch fast between them in the viewer
      altTextManager: { destroy () {} },
      // Path for image resources, mainly for annotation icons. Include trailing slash.
      imageResourcesPath: IMAGE_PATH
    })
    this.#pdfViewer = pdfViewer
    linkService.setViewer(pdfViewer)

    // Workaround: disable all inputs in pdf annotations, they can't be saved in the ox viewer, hence it's confusing when the user can edit the content
    eventBus.on('annotationlayerrendered', (e) => {
      const annotationLayerDOMRef = e?.source?.annotationLayer?.div
      if (annotationLayerDOMRef instanceof HTMLElement && annotationLayerDOMRef.classList.contains('annotationLayer')) {
        annotationLayerDOMRef.querySelectorAll('input, textarea').forEach((n) => { n.setAttribute('disabled', true) })
      } else {
        console.error('AnnotationLayer not found, propably due to changes in the API')
      }
    })

    eventBus.on('pagesinit', () => {
      // set initial zoom and scroll position
      if (this.#initialZoomLevel) { this.setZoom(this.#initialZoomLevel) }
      if (typeof this.#initialScrollPosition === 'number') { this.$container.scrollTop(this.#initialScrollPosition) }
      // pdf viewer is ready to view
      this.#pdfReadyPromise.resolve()
    })

    this.#pdfViewer.setDocument(this.#pdfDocument)
    this.#pdfLinkService.setDocument(this.#pdfDocument)

    return this.#pdfReadyPromise
  }
}
