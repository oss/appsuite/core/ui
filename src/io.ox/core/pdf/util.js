/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ox from '@/ox'
import _ from '@/underscore'

import DocConverterUtils from '@/io.ox/core/tk/doc-converter-utils'
import { PDF_TO_CSS_UNITS } from '@/io.ox/core/pdf/settings'
import gt from 'gettext'

export function getPDFloadErrorDescription (error) {
  let notificationText
  let notificationIcon
  let details

  // DocumentConverter errors
  if (error?.origin === 'DocumentConverter' && error.cause !== 'abortedByUser') {
    notificationText = DocConverterUtils.getErrorTextFromResponse(error) || DocConverterUtils.getErrorText('importError')
    notificationIcon = (error.cause === 'passwordProtected') ? 'bi/lock-fill.svg' : null

    // DOCS-4805: show error details in addition to the normal error message
    if (error.cause !== 'passwordProtected') {
      details = `cause="${error.cause}"`
      const statusErrorCache = error.errorData?.statusErrorCache
      if (_.isString(statusErrorCache) && (statusErrorCache !== 'new')) details += `-${statusErrorCache}`
      if (_.isNumber(error.status) && (error.status >= 400)) details += `-${error.status}`
    }
  }

  // PDFjs parse errors
  if (error?.origin === 'pdfjs') {
    notificationText = gt('An unexpected error occurred while rendering the PDF file.')
    notificationIcon = 'bi/exclamation-triangle.svg'
    if (error.cause === 'password_error') {
      notificationText = gt('The PDF file is password protected and cannot be displayed')
      notificationIcon = 'bi/lock-fill.svg'
    }
    if (error.cause === 'invalid_file_error') { notificationText = gt('The PDF file structure is invalid.') }
  }

  return { notificationText, notificationIcon, details }
}

export async function getPdfPageDimension (pdfDocument, pageNumber) {
  const firstPage = await pdfDocument.getPage(pageNumber)
  const viewPort = await firstPage.getViewport({ scale: 1 * PDF_TO_CSS_UNITS })
  return { width: Math.ceil(viewPort.width), height: Math.ceil(viewPort.height) }
}

export function clearCanvas (canvas) {
  // detect typical errors like passing jquery nodes
  if (canvas instanceof HTMLCanvasElement) {
    // Setting the canvas to 1px resets the size and thus the canvas memory consumption,
    // this seems to be the most reliable way at the moment, just removing the element from DOM does not help.
    // Can be monitored at iOS with Safari debugger open, watching the Graphics" section.
    canvas.height = 1
    canvas.width = 1
  } else {
    if (ox.debug) { console.error('Canvas not cleared') }
  }
}
