/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'

export function pdfloader () {
  let loadProgress = 0
  let reader
  const controller = new AbortController()
  const signal = controller.signal

  /**
   * Abort the fetch request.
   */
  function abort () {
    // to abort this fetch request, which has an  additional ReadableStream, a special handling/order is required to prevent errors
    reader?.cancel() // without this line: DOMException: BodyStreamBuffer was aborted
    controller?.abort()
  }

  /**
   *  Fetch  a pdf file from the document converter.
   *
   *  @param {String} url                 The url with the document converter request to fetch the pdf file.
   *  @param {Function} progressCallback  A callback function that is invoked at every received chunk.
   *
   *  @returns {Promise<Uint8Array>}      Returns a promise, which is resolved with the fetched pdf file.
   *
   */
  async function fetchWithProgress (url, progressCallback) {
    progressCallback('waiting')

    try {
      const response = await fetch(url, { signal })

      // HTTP error
      if (!response.ok) {
        throw new Error('Response error', { cause: { origin: 'DocumentConverter', cause: 'importError', status: response.status, errorData: response.statusText } })
      }

      // Document conversion error, detect JSON response and create an error descriptor object
      if (response.headers.get('Content-Type')?.startsWith('application/json')) {
        const data = { status: response.status }
        const json = await response.json()
        if (_.isObject(json) && _.isString(json.cause)) {
          Object.assign(data, json)
        } else {
          Object.assign(data, { cause: 'filterError', errorData: data })
        }
        throw new Error('DocumentConverter error', { cause: data })
      }

      const contentLength = response.headers.get('content-length')

      // handle progress information of the documentConverter fetch request
      reader = response.body.getReader({ signal })
      let receivedLength = 0
      let estimatedLoadTime = null
      const startTime = Date.now()
      const speedMeasureWindow = 200
      const progressStream = new ReadableStream({
        start (controller) {
          // process each data chunk
          function push () {
            reader.read().then(({ done, value }) => {
              // no more data to read
              if (done) {
                controller.close()
                return done
              }

              receivedLength += value.length
              controller.enqueue(value)
              // estimate the time to load of the file very quick and roughly in a specific timeWindow,
              // the estimate must not be updated during download
              if (estimatedLoadTime === null && Date.now() - startTime > speedMeasureWindow) {
                estimatedLoadTime = 1 / (receivedLength / contentLength) * speedMeasureWindow
              }
              // update the progress
              const currentProgress = Math.round((receivedLength / contentLength) * 100)
              if (currentProgress !== loadProgress) { progressCallback('downloading', currentProgress, estimatedLoadTime) }
              loadProgress = currentProgress

              push()
            })
          }

          push()
        }
      })

      const finalResponse = await new Response(progressStream).arrayBuffer()
      if (signal?.aborted) { throw new Error('Aborted by user', { cause: { origin: 'DocumentConverter', cause: 'abortedByUser' } }) }

      return finalResponse
    } catch (error) {
      // how to reproduce this error in dev mode: be in a folder, close vite server, and open the viewer
      if (!error?.cause && error.message === 'Failed to fetch') { error.cause = { origin: 'DocumentConverter', cause: 'failedToFetch' } }
      return Promise.reject(error.cause)
    }
  }

  return {
    fetchWithProgress,
    abort
  }
}
