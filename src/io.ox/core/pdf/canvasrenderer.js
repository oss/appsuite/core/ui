/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { MAX_CANVAS_PIXELS, PDF_TO_CSS_UNITS } from '@/io.ox/core/pdf/settings'

/**
 * Renders the canvas representation of a single pdf page to a provided <canvas> element.
 *
 * @param {pdfDocument} pdfDocument     The model of the pdfDocument.
 * @param {HTMLCanvasElement} canvas    A HTML canvas node.
 * @param {Number} pageNumber           The page of the document to render.
 * @param {Number} pageZoom             The wanted zoom factor of the pdf page.
 *
 * @returns {Promise}                   Returns a promise, which is resolved when the
 *                                      the canvas has been rendered.
 *
 * Notes:
 *
 * - pdfjs buffers "render" calls in a queue. I found no problem not doing it in ox uses-cases.
 *   Just be aware of this if problems arise in other use cases with many fast calls of 'renderSinglePage'
 *
 * - iOS has a lazy & small canvas cache which can result in "Total canvas memory use exceeds the maximum limity" errors.
 *   It's best to 'clear' the canvas (see function in pdf/utils.js) or re-use the canvas to keep memory consumption low.
 */
export async function renderSinglePage (pdfDocument, canvas, pageNumber, pageZoom) {
  const page = await pdfDocument.getPage(pageNumber)

  // the factor PDF_TO_CSS_UNITS is required to keep equal size to the PDFViewerApplication
  let viewport = page.getViewport({ scale: pageZoom * PDF_TO_CSS_UNITS })
  // support HiDPI
  const outputScale = window.devicePixelRatio || 1

  const context = canvas.getContext('2d')

  canvas.width = Math.ceil(Math.ceil(viewport.width) * outputScale)
  canvas.height = Math.ceil(Math.ceil(viewport.height) * outputScale)
  canvas.style.width = Math.ceil(viewport.width) + 'px'
  canvas.style.height = Math.ceil(viewport.height) + 'px'

  // limit max canvas size
  if (canvas.width * canvas.height > MAX_CANVAS_PIXELS) {
    const reduceFactor = Math.sqrt(canvas.width * canvas.height / MAX_CANVAS_PIXELS)
    viewport = page.getViewport({ scale: pageZoom / reduceFactor })
    canvas.width = Math.ceil(Math.ceil(viewport.width) * outputScale)
    canvas.height = Math.ceil(Math.ceil(viewport.height) * outputScale)
  }

  const transform = outputScale !== 1
    ? [outputScale, 0, 0, outputScale, 0, 0]
    : null

  // render the pdf page to the given canvas
  const renderContext = {
    canvasContext: context,
    transform,
    viewport
  }
  await page.render(renderContext)
}
