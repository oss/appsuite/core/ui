/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import * as pdfjsLib from 'pdfjs-dist/build/pdf'
import { CMAP_URL, WORKER_PATH } from '@/io.ox/core/pdf/settings'

/**
 * Create, store and manage the pdfDocument model and it's life-cycle.
 *
 *  Notes: Some function names can be discussed, but the idea is to kept function names
 *        and constructions similar to pdfjs for easier reference.
 *
 * @constructor
 *
 * @param {Uint8Array} pdfFile The PDF file.
 */
export default class PDFDocumentManager {
  #pdfLoadingTask
  #pdfFile
  constructor (pdfFile) {
    this.#pdfFile = pdfFile
    this.pdfDocument = null
  }

  /**
   * Opens the pdf file to create the 'pdfDocument'.
   *
   *  @returns {Promise<pdfDocument>}   Returns a promise, which is resolved with the
   *                                    pdfDocument when pdf file is parsed.
   */
  async open () {
    if (this.#pdfLoadingTask) {
      return this.close().then(
        () => {
          return this.open()
        }
      )
    }

    // this is how pdfjs defines the worker path
    pdfjsLib.GlobalWorkerOptions.workerSrc = WORKER_PATH

    // parse the pdf file
    this.#pdfLoadingTask = pdfjsLib.getDocument({
      data: this.#pdfFile,
      maxImageSize: -1, // no limit
      cMapUrl: CMAP_URL,
      cMapPacked: true,
      disableAutoFetch: true, // required to disable chunk loading
      disableStream: true, // required to disable chunk loading
      disableRange: true, // required to disable chunk loading
      verbosity: 0
    })

    try {
      this.pdfDocument = await this.#pdfLoadingTask.promise
      // the pdf file is parsed and the 'pdfDocument' is ready to use
      return this.pdfDocument
    } catch (exception) {
      let pdfjsErrorType = ''
      if (exception instanceof pdfjsLib.InvalidPDFException) {
        // Invalid or corrupted PDF file
        pdfjsErrorType = 'invalid_file_error'
      } else if (exception instanceof pdfjsLib.MissingPDFException) {
        // 'Missing PDF file.'
        pdfjsErrorType = 'missing_file_error'
      } else if (exception instanceof pdfjsLib.UnexpectedResponseException) {
        // Unexpected server response
        pdfjsErrorType = 'unexpected_response_error'
      } else if (exception.name === 'PasswordException') {
        // Need and password to be opened
        pdfjsErrorType = 'password_error'
      } else {
        // An error occurred while loading the PDF.
        pdfjsErrorType = 'loading_error'
      }

      const errorObj = { origin: 'pdfjs', cause: pdfjsErrorType }
      return Promise.reject(errorObj)
    }
  }

  /**
   * Closes the pdfDocument to destroy the worker and further clean up.
   *
   * @returns {Promise}  Returns a promise, which is resolved when the
   *                     destruction is completed.
   */
  close () {
    if (!this.#pdfLoadingTask) {
      // it's already closed
      return Promise.resolve()
    }

    if (this.#pdfLoadingTask) {
      // Fix for "pdf_viewer.js: Unable to get page <n> to initialize viewer Error: Transport destroyed" errors
      // - load a pdf viewer app with many pages (100+) and close it directly when the download has been finished and rendering starts
      this.#pdfLoadingTask._worker.destroy()

      this.#pdfLoadingTask.destroy()
    }
    const promise = this.#pdfLoadingTask.destroy()
    this.#pdfLoadingTask = null

    if (this.pdfDocument) {
      this.pdfDocument = null
    }

    return promise
  }
}
