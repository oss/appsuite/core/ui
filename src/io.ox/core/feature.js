/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import { addReadyListener } from '@/io.ox/core/events'
import { isMiddlewareMinVersion, isPWAEnabled } from '@/io.ox/core/util'
import capabilities from '@/io.ox/core/capabilities'
import manifests from '@/io.ox/core/manifests'
import upsell from '@/io.ox/core/upsell'
import { settings as coreSettings } from '@/io.ox/core/settings'
import ext from '@/io.ox/core/extensions'
import { settings as zoomSettings } from '@/io.ox/conference/zoom-settings'
import { settings as jitsiSettings } from '@/io.ox/jitsiReservationManager/settings'
import ox from '@/ox'

// Let's consolidate feature toggles
// Currently they're spread across all settings
// sometimes features/something, sometimes somename/enabled
// sometimes they're mixing up with user settings
// idea is to centrally have feature toggle in io.ox/core
// each toggle has a name, the value is bool, the user cannot change them

// lets keep this list in alphabetical order
const defaults = {
  ai: false,
  attachFromDrive: true,
  categories: false,
  clientOnboardingHint: true,
  connectYourDevice: true,
  countdown: true,
  dragDropICAL: true,
  folderIcons: true,
  firstStartWizard: true,
  forwardInvitation: true,
  freeBusyVisibility: true,
  implicitCancel: true,
  jitsi: true,
  logoutButtonHint: true,
  managedResources: true,
  navigation: true,
  notes: false,
  pe: true,
  pns: false,
  presence: false,
  pwa: true,
  pwaInstructionsWizard: true,
  qrSessionHandover: true,
  reloginPopup: true,
  resourceCalendars: false,
  scheduleSend: false,
  shortcuts: true,
  storeSavePoints: true,
  switchboard: true,
  templates: false,
  undo: true,
  undoSend: true,
  validateMailAddresses: true,
  validatePhoneNumbers: false,
  zoom: true
}

const userDefaults = {
  countdown: false
}

const basePath = ox.root.replace(/\/$/, '')

// lets keep this list in alphabetical order
const requirements = {
  attachFromDrive: () => capabilities.has('infostore') && isMiddlewareMinVersion(8, 8),
  categories: () => isMiddlewareMinVersion(8, 19),
  clientOnboardingHint: () => coreSettings.get('features/clientOnboardingHint/enabled', false),
  connectYourDevice: () => capabilities.has('client-onboarding') && coreSettings.get('onboardingWizard', true),
  countdown: () => capabilities.has('calendar') && !_.device('smartphone'),
  drive: () => capabilities.has('filestore'),
  // disable if old first start wizard is active
  firstStartWizard: () => manifests.manager.pluginsFor('io.ox/wizards/firstStart').length === 0,
  forwardInvitation: () => isMiddlewareMinVersion(8, 20),
  freeBusyVisibility: () => isMiddlewareMinVersion(8, 14),
  implicitCancel: () => capabilities.has('calendar'),
  jitsi: () => jitsiSettings.get('enabled', false) && !!jitsiSettings.get('host'),
  logoutButtonHint: () => coreSettings.get('features/logoutButtonHint/enabled', false),
  managedResources: () => isMiddlewareMinVersion(8, 13),
  // old AI integration in core UI (pre 8.28)
  openai: () => hasFeature('pe') && upsell.visible('openai') && requirements.switchboard(),
  // pns needs presence since presence has the websocket connection from switchboard
  pns: () => isMiddlewareMinVersion(8, 18) && requirements.switchboard(),
  presence: () => requirements.switchboard(),
  pwa: isPWAEnabled,
  pwaInstructionsWizard: isPWAEnabled,
  qrSessionHandover: () => isMiddlewareMinVersion(8, 29) && hasFeature('pwaInstructionsWizard') && coreSettings.get('tokens/applications', []).includes('qrlogin'),
  reloginPopup: () => !ox.serverConfig.oidcLogin && !ox.serverConfig.samlLogin,
  resourceCalendars: () => capabilities.has('calendar'),
  scheduleSend: () => capabilities.has('scheduled_mail'),
  // switchboard is just listed as a requirement (e.g. used by presence and AI); it's not a feature toggle
  switchboard: () => capabilities.has('switchboard') && ext.point('io.ox/switchboard/state')?.get('settings')?.get('host'),
  undo: () => isMiddlewareMinVersion(8, 33) && coreSettings.get('undoPeriod', 5000) > 0,
  zoom: () => hasFeature('pe') && zoomSettings.get('enabled', false) && requirements.switchboard()
}

addReadyListener('capabilities:server', async () => {
  const config = (await import('@/io.ox/core/boot/config')).default
  const { edition } = await config.server()
  defaults.pe = typeof edition === 'undefined' || edition === 'pe'
})

const features = {}
addReadyListener('settings', () => {
  Object.assign(features, defaults, coreSettings.get('features', {}))
})

export function getRequirement (feature) {
  return requirements[feature]
}

export function meetsRequirements (feature) {
  return requirements[feature] ? requirements[feature]() : true
}

export function addFeature (feature = '', defaultValue = true, requirement) {
  if (feature in defaults) return
  features[feature] = !!coreSettings.get(['features', feature], defaultValue)
  // reset memoize cache for this feature toggle
  delete hasFeature.cache[feature]
  if (requirement) addRequirement(feature, requirement)
}

export function addRequirement (feature, requirement) {
  if (requirements[feature]) {
    return console.warn(`Core: Feature "${feature}" has already a requirement defined and can not be overwritten`)
  }
  requirements[feature] = requirement
  // reset memoize cache
  delete hasFeature.cache[feature]
  return requirements
}

export const hasFeature = _.memoize(function hasFeature (feature) {
  if (!feature) return false
  if (!features) {
    if (ox.debug) console.error('io.ox/core/feature.js: feature toggle checked before settings are loaded')
    return false
  }
  if (!features[feature]) return false
  if (requirements[feature]) return requirements[feature]()
  return true
})

export function getTogglePath (feature) {
  // `.user` is a special namespace just for temporary internal use (feature testing)
  return 'features/.user/' + feature
}

export async function loadFeature ({ feature, path, delay = 0 }) {
  if (!hasFeature(feature)) return
  const toggle = getTogglePath(feature)
  const enabled = coreSettings.get(toggle, userDefaults[feature] ?? true)
  if (enabled) {
    setTimeout(() => import(/* @vite-ignore */`${basePath}/${path}.js`), delay)
  } else {
    coreSettings.once('change:' + toggle, () => import(/* @vite-ignore */`${basePath}/${path}.js`))
  }
  // make sure user changes are saved
  coreSettings.on('change:' + toggle, () => { coreSettings.save() })
}

export function userCanToggleFeature (feature) {
  return hasFeature(feature) && coreSettings.isConfigurable(getTogglePath(feature))
}

export function isEnabledByUser (feature) {
  return hasFeature(feature) && coreSettings.get(getTogglePath(feature), false)
}

export function onChangeUserToggle (feature, callback) {
  coreSettings.on('change:' + getTogglePath(feature), callback)
}

export function getFeedbackUrl (feature) {
  // `.feedback` is a special namespace just for temporary internal use (feature testing)
  return coreSettings.get(['features', '.feedback', feature, 'url'])
}

export function getEnabled () {
  if (!features) {
    if (ox.debug) console.error('io.ox/core/feature.js: feature toggle checked before settings are loaded')
    return []
  }
  // look for our default feature toggles (booleans) and return only those whose requirements are met.
  return Object.keys(features).filter(key => hasFeature(key)).sort()
}

export const toggleSettings = coreSettings
