/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import yell from '@/io.ox/core/yell'

import gt from 'gettext'

// handle online/offline mode
//
let visible = false
let indicator = '#io-ox-offline'
const options = {
  show: { bottom: '0px' },
  hide: { bottom: '-41px' }
}
if (_.device('smartphone')) {
  indicator = '#io-ox-offline-smartphone'
  options.show = { height: '24px', 'min-height': '24px' }
  options.hide = { height: '0px', 'min-height': '0px' }
}

function showIndicator (text) {
  if (visible) return
  $(indicator).text(text).show().animate(options.show, 200, function () { visible = true })
  yell('screenreader', text)
}

function hideIndicator () {
  if (!visible) return
  $(indicator).stop().animate(options.hide, 200, function () { $(this).hide(); visible = false })
}

ox.on({
  'connection:online' () {
    hideIndicator()
    ox.online = true
  },
  'connection:offline' () {
    showIndicator(gt('Offline'))
    ox.online = false
  },
  'connection:up' () {
    if (ox.online) hideIndicator()
  },
  'connection:down' () {
    if (ox.online) showIndicator(gt('Server unreachable'))
  }
})

if (!ox.online) {
  $(window).trigger('offline')
}
