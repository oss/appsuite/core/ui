/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ui from '@/io.ox/core/desktop'
import manifests from '@/io.ox/core/manifests'

const appSuiteVuePlugins = await manifests.manager.loadPluginsFor('vue-apps')

appSuiteVuePlugins.forEach((plugin, pluginIndex) => {
  const app = ui.createApp({
    ...plugin.meta,
    async load () {
      return {
        default: {
          getApp () {
            return app
          }
        }
      }
    }
  }).setLauncher(async () => {
    const win = ui.createWindow({
      name: plugin.meta.name,
      chromeless: true
    })
    app.setWindow(win)

    const appContainer = app.getWindowNode()[0]
    const shadowRoot = appContainer.attachShadow({ mode: 'open' })

    document.addEventListener('update-hmr-css', function updateCss ({ detail }) {
      let styleElement = shadowRoot.querySelector(`style[data-vite-id="${detail.id}"]`)
      if (!styleElement) {
        styleElement = document.createElement('style')
        styleElement.setAttribute('data-vite-id', detail.id)
        shadowRoot.appendChild(styleElement)
      }
      styleElement.textContent = detail.css
    })

    app.vueApp = await plugin.createVueApp({ shadowRoot })

    manifests.manager.pluginPoints['vue-apps'][pluginIndex].css?.forEach((cssRef) => {
      const cssLinkElement = document.querySelector(`link[href*="/${cssRef}"`)
      if (cssLinkElement) shadowRoot.appendChild(cssLinkElement)
    })

    app.vueApp.mount(shadowRoot)
    app.vueApp.provide('core/app', app)
    win.show()
    document.getElementsByTagName('html')[0].classList.add('complete')
  })
})
