/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import { createIcon } from '@/io.ox/core/components'
import gt from 'gettext'
import HelpApp from '@/io.ox/help/main'
import Backbone from '@/backbone'
import ox from '@/ox'

const SidepanelView = Backbone.View.extend({
  el: '#io-ox-sidepanel',
  events: {
    'click .back': 'navigateBack',
    'click .help-button': 'openHelp',
    'click .close-button': 'hide'
  },
  initialize () {
    this.widgets = {}
  },
  isVisible () {
    return $('#io-ox-core').hasClass('sidepanel-visible')
  },
  toggle (state) {
    $('#io-ox-core').toggleClass('sidepanel-visible', state ?? !this.isVisible())
  },
  hide () {
    this.toggle(false)
  },
  show () {
    this.toggle(true)
  },
  openHelp () {
    if (HelpApp.reuse({ base: 'help', href: this.activeWidget.helpUrl })) return
    HelpApp.getApp({ base: 'help', href: this.activeWidget.helpUrl }).launch()
  },
  navigateBack () {
    const back = this.activeWidget.back
    if (back) this.makeActive(back)
  },
  getActive () {
    return this.activeWidget?.id
  },
  makeActive (id) {
    const widget = this.widgets[id]
    if (!widget) return
    Object.values(this.widgets).forEach(widget => widget.$el.removeClass('active'))
    widget.$el.addClass('active')
    this.activeWidget = widget
    this.show()
  },
  add (id, title = '', { back, $controls, helpUrl, elementId = '' } = {}) {
    if (!id) return
    // avoid duplicates
    if (this.widgets[id]) return
    const $content = $('<div class="sidepanel-content">')
      // add ID to support e2e tests especially when dealing with shadow-DOM
      .attr('id', elementId || (id.replace(/([A-Z])/g, '-$1').toLowerCase() + '-sidepanel'))
    const $el = $('<div class="sidepanel" class="region">')
      .toggleClass('slidein', !!back)
      .append(
        $('<div class="sidepanel-header">').append(
          // back button
          back
            ? $('<button type="button" class="btn back">')
              .attr('title', gt('Back'))
              .append(createIcon('bi/chevron-left.svg'))
            : $(),
          // title
          $('<h2 class="title truncate">').text(title),
          // optional controls
          $controls || $(),
          // optional help button
          helpUrl
            ? $('<button type="button" class="btn help-button">')
              .attr('title', gt('Online help'))
              .append(createIcon('bi/question-circle.svg'))
            : $(),
          // close button
          $('<button type="button" class="btn close-button">')
            // #. used as a verb
            .attr('title', gt('Close'))
            .append(createIcon('bi/x-lg.svg'))
        ),
        $content
      )
    const widget = { id, $el, $content, title, back, $controls, helpUrl }
    this.widgets[id] = widget
    this.$el.append($el)
    return widget
  },
  remove (id) {
    const widget = this.widgets[id]
    if (!widget) return
    widget.$el.remove()
    if (widget === this.activeWidget) this.activeWidget = null
    delete this.widgets[id]
    this.hide()
  }
})

// strange duplication behavior otherwise if imported in browser console
if (!ox.sidepanelView) ox.sidepanelView = new SidepanelView()

export const sidepanelView = ox.sidepanelView
