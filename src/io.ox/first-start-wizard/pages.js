/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import WelcomePage from '@/io.ox/first-start-wizard/pages/welcome-page.vue'
import LanguageRegionPage from '@/io.ox/first-start-wizard/pages/language-region.vue'
import PersonalInfoPage from '@/io.ox/first-start-wizard/pages/personal-info.vue'
import ThemePreferencePage from '@/io.ox/first-start-wizard/pages/theme-preference.vue'
import MailAppLayoutPage from '@/io.ox/first-start-wizard/pages/mail-layout.vue'
import CalendarWorkingTimePage from '@/io.ox/first-start-wizard/pages/calendar-working-time.vue'
import InstallAppSuitePage from '@/io.ox/first-start-wizard/pages/install-app-suite.vue'

import _ from '@/underscore'
import { settings as coreSettings } from '@/io.ox/core/settings'
import theming from '@/io.ox/core/theming/main'
import capabilities from '@/io.ox/core/capabilities'
import { hasFeature } from '@/io.ox/core/feature'

const isLanguageConfigurable = coreSettings.isConfigurable('language')
const isTimezoneConfigurable = coreSettings.isConfigurable('timezone')
const isPersonalInfoConfigurable = coreSettings.get('user/internalUserEdit', true)
const isThemeConfigurable = theming.supportsPicker()
const isMailAppLayoutConfigurable = capabilities.has('webmail') && !_.device('smartphone')
const isCalendarWorkingTimeConfigurable = capabilities.has('calendar')
const hasQrSessionHandoverFeature = hasFeature('qrSessionHandover')

ext.point('io.ox/first-start-widget').extend([
  {
    id: 'welcome-page',
    index: 100,
    component: WelcomePage
  },
  {
    id: 'language-region',
    index: 200,
    component: LanguageRegionPage,
    disabled: !(isLanguageConfigurable && isTimezoneConfigurable)
  },
  {
    id: 'personal-info',
    index: 300,
    component: PersonalInfoPage,
    disabled: !isPersonalInfoConfigurable
  },
  {
    id: 'theme-preference',
    index: 400,
    component: ThemePreferencePage,
    disabled: !isThemeConfigurable
  },
  {
    id: 'mail-layout',
    index: 500,
    component: MailAppLayoutPage,
    disabled: !isMailAppLayoutConfigurable
  },
  {
    id: 'calendar-working-time',
    index: 600,
    component: CalendarWorkingTimePage,
    disabled: !isCalendarWorkingTimeConfigurable
  },
  {
    id: 'install-app-suite',
    index: 700,
    component: InstallAppSuitePage,
    disabled: !hasQrSessionHandoverFeature
  }
])
