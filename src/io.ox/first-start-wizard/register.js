/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import Stage from '@/io.ox/core/extPatterns/stage'
import userAPI from '@/io.ox/core/api/user'
import ModalDialog from '@/io.ox/backbone/views/modal'
import FirstStartWizard from '@/io.ox/first-start-wizard/first-start-wizard.vue'
import { createApp } from 'vue'
import { DateTime } from 'luxon'
import { ensureInitialConfig, isActive, setLastAutoopen } from '@/io.ox/core/autoopen'
import { settings as coreSettings } from '@/io.ox/core/settings'
import * as actionsUtil from '@/io.ox/backbone/views/actions/util'

let modal

function isNew (user) {
  const daysSinceCreation = DateTime.fromMillis(user.creation_date).diffNow('days').days
  return Math.abs(daysSinceCreation) < 30
}

export function openDialog () {
  // Prevent opening the dialog multiple times via hotkey
  if (modal?.el) return modal

  modal = new ModalDialog({
    width: 765,
    className: 'first-start-wizard-modal modal flex'
  })
    .build(async function () {
      this.$header.hide()
      this.$footer.hide()
      const app = createApp(FirstStartWizard, { close: () => modal.close() })
      app.mount(this.el.querySelector('.modal-body'))
    })

  // wait a bit for vue to render and update dom to avoid flickering
  setTimeout(() => {
    modal.open()
  }, 100)

  return modal
}

actionsUtil.Action('io.ox/first-start-wizard/actions/showFirstStartWizard', {
  action: openDialog
})

// eslint-disable-next-line no-new
new Stage('io.ox/core/stages', {
  id: 'first-start-wizard',
  index: 800,
  run (baton) {
    if (baton.data.popups.length > 0) return
    if (coreSettings.get('whatsNew/lastSeenVersion', false)) return

    const { id } = baton.extension
    ensureInitialConfig(id, { remaining: 1 })
    if (!isActive(id)) return

    return userAPI.get().then(user => {
      if (!isNew(user)) return

      baton.data.popups.push({ name: id })
      openDialog().on('open', () => {
        setLastAutoopen(id)
      })
    })
  }
})
