/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import Backbone from '@/backbone'

import ext from '@/io.ox/core/extensions'
import deviceAuthenticator from '@/io.ox/multifactor/deviceAuthenticator'
import { createCancelButton, createLostDeviceButton } from '@/io.ox/multifactor/views/base-dialog'
import ModalView from '@/io.ox/backbone/views/modal'
import form from '@/io.ox/core/boot/form'
import { buttonWithIconAndText, createIcon } from '@/io.ox/core/components'
import cpuIcon from 'bootstrap-icons/icons/cpu.svg?raw'
import phoneIcon from 'bootstrap-icons/icons/phone.svg?raw'
import keyIcon from 'bootstrap-icons/icons/key.svg?raw'
import fileTextIcon from 'bootstrap-icons/icons/file-text.svg?raw'
import personBadgeIcon from 'bootstrap-icons/icons/person-badge.svg?raw'

import gt from 'gettext'

// FIXME: Test language switch...

const ICONS = {
  U2F: cpuIcon,
  SMS: phoneIcon,
  TOTP: keyIcon,
  BACKUP: fileTextIcon,
  YUBIKEY: personBadgeIcon
}

function open (devices, authInfo) {
  // Some devices don't need to be individually selected, like U2f
  devices = groupDevices(devices)
  if (devices.length === 1) {
    // If only one after grouping, proceed to auth
    _.extend(authInfo, { device: devices[0], providerName: devices[0].providerName })
    return deviceAuthenticator.getAuth(authInfo)
  }

  let dialog = {
    $body: $('#io-ox-select-device-body').empty(),
    $footer: $('#io-ox-select-device-footer').empty()
  }

  const model = new Backbone.Model({ devices, authInfo })
  const baton = new ext.Baton({ model })

  if (authInfo.reAuth) { // Different presentation if already logged in, or this is the initial login form
    dialog = new ModalView({
      async: true,
      title: gt('Reauthentication required for this action')
    })
      .open()
    authInfo.def.always(dialog.close)
  } else {
    form('select_device', { model, deviceAuthenticator })

    $('#io-ox-select-device-title')
      .text(gt('2-step verification'))
      .attr('data-i18n', '2-step verification')
  }

  dialog.$footer.append(
    createLostDeviceButton(authInfo),
    createCancelButton(authInfo.def.reject)
  )

  ext.point('multifactor/views/selectDeviceView')
    .invoke('render', dialog, baton)

  return dialog
}

ext.point('multifactor/views/selectDeviceView').extend(
  {
    index: 100,
    id: 'header',
    render () {
      const text = gt('Please select a device to use for additional authentication')
      this.$body.append(
        $('<p class="text-start" data-i18n="Please select a device to use for additional authentication">')
          .text(text)
      )
    }
  },
  {
    index: 200,
    id: 'selection',
    render (baton) {
      // FIXME: Make scrollable and fix whitespace on top/bottom of login-box
      const options = baton.model.get('devices').map(device => {
        const button = buttonWithIconAndText({
          className: 'btn form-control my-8 flex justify-start',
          icon: createIcon(ICONS[device.providerName]),
          text: device.name
        }).on('click', function (event) {
          event.preventDefault()
          const authInfo = baton.model.get('authInfo')
          authInfo.providerName = device.providerName
          authInfo.device = device
          deviceAuthenticator.getAuth(authInfo)
        })
        return button
      })
      this.$body.append(options)
    }
  },
  {
    index: 300,
    id: 'error',
    render (baton) {
      const error = baton.model.get('authInfo').error
      if (error && error.text) {
        this.$body.append(
          $('<div class="multifactorError text-start text-attention">').append(error.text)
        )
      }
    }
  }
)

function groupDevices (devices) {
  const grouped = {}
  const newList = []
  devices.forEach(function (device) {
    if (['U2F', 'WEB-AUTH'].includes(device.providerName)) {
      if (!grouped[device.providerName]) {
        newList.push(device)
        grouped[device.providerName] = true
      } else {
        newList.forEach(function (dev) {
          if (dev.providerName === device.providerName) dev.name = '' // Wipe grouped names
        })
      }
    } else {
      newList.push(device)
    }
  })
  return newList
}

export default {
  open
}
