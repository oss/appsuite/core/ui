/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import Backbone from '@/backbone'

import ext from '@/io.ox/core/extensions'
import ModalView from '@/io.ox/backbone/views/modal'
import mini from '@/io.ox/backbone/mini-views/common'
import gt from 'gettext'
import form from '@/io.ox/core/boot/form'
import { createButton } from '@/io.ox/core/components'
import { createCancelButton, createNextButton, getSubmitFunction } from '@/io.ox/multifactor/views/base-dialog'

function open (challenge, authInfo) {
  const model = new Backbone.Model({
    provider: authInfo.providerName,
    deviceId: authInfo.device.id,
    challenge,
    error: authInfo.error
  })

  if (authInfo.reAuth === true) return openModalDialog(challenge, authInfo)

  const dialog = {
    $body: $('#io-ox-lost-device-body').empty(),
    $footer: $('#io-ox-lost-device-footer').empty()
  }
  const baton = new ext.Baton({ model })

  const uploadButton = createButton({ text: gt('Upload Recovery File'), className: 'form-control my-4' })
    .attr('id', 'io-ox-lost-device-upload-button')
    .on('click', () => startUpload(authInfo))
  uploadButton.find('span').attr('data-i18n', 'Upload Recovery File')

  form('lost_device')

  $('#io-ox-lost-device-title')
    .text(gt('2-step verification'))
    .attr('data-i18n', '2-step verification')
  $('#io-ox-lost-device-input-label')
    .text(gt('Recovery code'))
    .attr('data-i18n', 'Recovery code')
  dialog.$footer.append(
    createNextButton(getSubmitFunction('#recovery', authInfo)),
    uploadButton,
    createCancelButton(authInfo.def.reject)
  )

  ext.point('multifactor/views/backupProvider')
    .invoke('render', dialog, baton)

  return dialog
}

function openModalDialog (model, authInfo) {
  return new ModalView({
    async: true,
    point: 'multifactor/views/backupProvider',
    title: gt('2-step verification'),
    width: 640,
    enter: 'OK',
    model
  })
    .on('open', $('#recovery').focus)
    .addCancelButton()
    .addButton({ label: gt('Next'), action: 'OK' })
    .addAlternativeButton({ label: gt('Upload Recovery File'), action: 'Upload' })
    .on('cancel', authInfo.def.reject)
    .on('OK', function () {
      const submit = getSubmitFunction('#recovery', authInfo)
      submit()
      this.close()
    })
    .on('Upload', function () {
      startUpload(authInfo)
      this.close()
    })
    .open()
}

function startUpload (authInfo) {
  uploadRecovery().then(data => {
    const resp = {
      response: data,
      id: authInfo.device.id,
      provider: authInfo.providerName
    }
    authInfo.def.resolve(resp)
  })
}

// Get recovery string from file
function uploadRecovery () {
  const def = $.Deferred()
  const fileInput = $('<input type="file" name="file" class="file">')
    .css('display', 'none')
    .on('change', function () {
      if (!this.files || !this.files[0]) return def.reject()
      const reader = new FileReader()
      reader.addEventListener('load', event => def.resolve(event.target.result))
      reader.readAsBinaryString(this.files[0])
    })
  $('.recoveryDiv').append(fileInput)
  fileInput.click()
  return def
}

ext.point('multifactor/views/backupProvider').extend(
  {
    index: 100,
    id: 'header',
    render () {
      const text = gt('Please enter the recovery code')
      this.$body.append(
        $('<p class="text-start" data-i18n="Please enter the recovery code">')
          .text(text)
      )
    }
  },
  {
    index: 200,
    id: 'selection',
    render () {
      const model = new Backbone.Model({
        initialize () {
          this.on('change:recovery', this.validateVerification)
        },
        validateVerification () {
          const valid = this.get('recovery').replaceAll(' ', '').length === 32
          if (valid) this.trigger('valid:recovery')
          else this.trigger('invalid:recovery', gt('Recovery code must be 32 characters long.'))
          return valid
        }
      })

      const text = gt('Recovery code')
      const label = $('<label id="io-ox-lost-device-input-label" for="recovery" data-i18n="Recovery code">')
        .text(text)
      const inputView = new mini.InputView({
        name: 'recovery',
        model,
        placeholder: '',
        autocomplete: false,
        validate: true
      }).render()

      inputView.$el.attr('id', 'recovery')
        .prop('autofocus')

      const errorView = new mini.ErrorView({ name: 'recovery', model }).render()

      this.$body.append(
        $('<div class="form-group">').append(
          label,
          inputView.$el,
          errorView.$el
        )
      )
    }
  },
  {
    index: 300,
    id: 'error',
    render (baton) {
      const error = baton.model.get('error')
      if (error && error.text) {
        const label = $('<label class="multifactorError">').text(error.text)
        this.$body.append(label)
      }
    }
  }
)

export default {
  open
}
