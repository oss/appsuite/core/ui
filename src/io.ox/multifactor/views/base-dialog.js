/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import gt from 'gettext'
import $ from '@/jquery'
import form from '@/io.ox/core/boot/form'
import lost from '@/io.ox/multifactor/lost'
import ModalView from '@/io.ox/backbone/views/modal'
import '@/io.ox/multifactor/views/style.scss'
import { createButton } from '@/io.ox/core/components'

let action // Holder for the login action

export function getMfaDialog (authInfo) {
  let dialog = {
    $header: $('#io-ox-multifactor-header'),
    $body: $('#io-ox-multifactor-body').empty(),
    $footer: $('#io-ox-multifactor-footer').empty()
  }

  if (authInfo.reAuth) { // Different presentation if already logged in, or this is the initial login form
    dialog = new ModalView({
      async: true,
      title: gt('Reauthentication required for this action')
    })
      .open()
    authInfo.def.always(dialog.close)
  } else { // Login form
    form('multifactor')

    $('#multifactor-title')
      .empty()
      .append(
        $('<span>')
          .attr('data-i18n', '2-step verification')
          .text(gt('2-step verification'))
      )
  }

  dialog.$footer.append(
    createNextButton('#authentication, #verification', authInfo),
    createLostDeviceButton(authInfo),
    createCancelButton(authInfo.def.reject)
  )

  return dialog
}

export function createNextButton (target, authInfo) {
  const button = createButton({
    text: gt('Next'),
    variant: 'primary',
    className: 'col-xs-12 my-4'
  })
  action = getSubmitFunction(target, authInfo)
  button
    .on('click', action)
    .find('span').attr('data-i18n', 'Next')
  return button
}

export function createLostDeviceButton (authInfo) {
  const button = createButton({ text: gt('I lost my device'), className: 'col-xs-12 my-4 mfbutton' })
    .on('click', () => lost(authInfo))
  button.find('span').attr('data-i18n', 'I lost my device')
  return button
}

export function createCancelButton (action) {
  const button = createButton({ text: gt('Cancel'), className: 'form-control my-4 mfbutton' })
    .attr('id', 'io-ox-lost-device-cancel-button')
    .on('click', action)
  button.find('span').attr('data-i18n', 'Cancel')
  return button
}

export function getSubmitFunction (target, authInfo) {
  return () => {
    const input = $(target).val().replace(/\s/g, '')
    if (!input) return authInfo.def.reject()

    authInfo.def.resolve({
      response: input,
      id: authInfo.device.id,
      provider: authInfo.providerName
    })
  }
}

export function getInputValidation () {
  return (event) => {
    if (event.which === 13) {
      action()
      return
    }
    const value = event.target.value
    const isValid = value === value.match(/[0-9\s]*/)[0]
    event.target.classList.toggle('mfInputError', !isValid)
  }
}
