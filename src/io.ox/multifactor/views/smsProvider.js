/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import Backbone from '@/backbone'
import gt from 'gettext'
import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import HelpLink from '@/io.ox/backbone/mini-views/helplink'
import { getInputValidation, getMfaDialog } from '@/io.ox/multifactor/views/base-dialog'
import helpIcon from 'bootstrap-icons/icons/question-circle.svg?raw'

function open (challenge, authInfo) {
  const dialog = getMfaDialog(authInfo)

  const model = new Backbone.Model({
    provider: authInfo.providerName,
    deviceId: authInfo.device.id,
    challenge,
    error: authInfo.error
  })
  const baton = new ext.Baton({ model })

  ext.point('multifactor/views/smsProvider')
    .invoke('render', dialog, baton)

  return dialog
}

ext.point('multifactor/views/smsProvider').extend(
  {
    index: 100,
    id: 'help',
    render () {
      const options = {
        base: 'help',
        href: 'ox.appsuite.user.sect.security.multifactor.sms.html',
        tabindex: '-1',
        simple: !ox.ui.createApp
      }
      if (!ox.ui.createApp) options.icon = helpIcon
      const helpLink = new HelpLink(options).render()
      this.$header.find('#multifactor-title, .modal-title').append(helpLink.$el.addClass('ms-8 my-auto').css('float', 'right'))
    }
  },
  {
    index: 200,
    id: 'header',
    render (baton) {
      const label = $('<label for="verification" class="text-start">')
      if (baton.model.get('error') && !baton.model.get('error').repeat) {
        this.$body.append(
          $('<div class="mfLoginMessage">').append(label
            .text(gt('A new code was sent to your SMS device. Please enter the code.'))
            .attr('data-i18n', 'A new code was sent to your SMS device. Please enter the code.'))
        )
        return
      }

      const { phoneNumberTail } = baton.model.get('challenge')
      // XXX: Where to put these new strings?
      this.$body.append($('<div>').append(
        label
          .append(
            $('<div>')
              .text(gt('You secured your account with 2-step verification. Please enter the verification code we sent to your phone.'))
              .attr('data-i18n', 'You secured your account with 2-step verification. Please enter the verification code we sent to your phone.'),
            phoneNumberTail && $('<div class="mfHint">').append(
              $('<span>')
                .text(gt('Registered phone number:'))
                .attr('data-i18n', 'Registered phone number:'),
              $('<span>').text(' *****' + phoneNumberTail)
            )
          )
      ))
    }
  },
  {
    index: 300,
    id: 'selection',
    render () {
      this.$body.append(
        $('<div class="multifactorAuthDiv">').append(
          $('<input type="text" class="form-control mfInput" id="verification">')
            .on('keyup', getInputValidation())
        )
      )
    }
  },
  {
    index: 400,
    id: 'error',
    render (baton) {
      const error = baton.model.get('error')
      if (error && error.text) {
        this.$body.append(
          $('<div class="multifactorError">')
            .append(error.text)
        )
      }
    }
  }

)

export default {
  open
}
