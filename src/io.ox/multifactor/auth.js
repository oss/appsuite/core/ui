/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import ox from '@/ox'

import api from '@/io.ox/multifactor/api'
import selectDeviceView from '@/io.ox/multifactor/views/selectDeviceView'
import deviceAuthenticator from '@/io.ox/multifactor/deviceAuthenticator'
import yell from '@/io.ox/core/yell'
import lost from '@/io.ox/multifactor/lost'

let authenticating = false
let authProcess

const auth = {

  getAuthentication (authInfo) {
    if (authenticating) {
      return new Promise((resolve, reject) => authProcess.then(resolve, reject))
    }
    authenticating = true
    const def = authProcess = $.Deferred()
    authInfo.def = def
    if (authInfo.error && authInfo.error.backup) {
      lost(authInfo)
      return new Promise((resolve, reject) => def.then(resolve, reject))
    }
    api.getDevices().then(function (list) {
      if (list && list.length > 0) {
        if (list && list.length > 1) {
          selectDeviceView.open(list, authInfo)
        } else {
          const device = list[0]
          Object.assign(authInfo, { providerName: device.providerName, device })
          deviceAuthenticator.getAuth(authInfo)
        }
      } else {
        // No primary methods. Check if some backup devices exist
        api.getDevices('BACKUP').then(function (list) {
          if (list && list.length > 0) {
            lost(authInfo)
            return def
          }
          def.reject()
        })
      }
    }, function (fail) {
      def.reject(fail)
    })
    return new Promise((resolve, reject) => def.then(resolve, reject))
  },

  doAuthentication: authenticate,

  reAuthenticate () {
    return authenticate({ reAuth: true })
  }

}

function notifyFailure (message) {
  yell('error', message)
  $('#io-ox-core').show() // May be hidden in login
  $('.multifactorBackground').hide() // If covered with background, hide
}

function authenticate (authInfo = {}) {
  ox.idle()
  return auth.getAuthentication(authInfo)
    .then(data => {
      authenticating = false

      if (!data) return Promise.reject(new Error('data is undefined'))

      return api.doAuth(data.provider, data.id, data.response, data.parameters)
        .catch(rejection => {
          authInfo.error = {
            backup: data.backup === true
          }
          if (rejection.code && (/MFA-002[1-3]/).test(rejection.code)) {
            authInfo.error.text = rejection.error
          } else {
            if (rejection) notifyFailure(rejection.error)
            return new Promise((resolve, reject) => window.setTimeout(reject, 3000))
          }
          return authenticate(authInfo)
        })
    }).catch(data => {
      authenticating = false
      if (data && data.error) {
        notifyFailure(data.error)
        return new Promise((resolve, reject) => window.setTimeout(reject, 3000))
      }
      return Promise.reject(new Error(data))
    })
}

export default auth
