/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '@/io.ox/core/extensions'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import '@/io.ox/contacts/actions'
import '@/io.ox/contacts/style.scss'
import gt from 'gettext'
import { buttonWithIcon, createIcon, createButton } from '../core/components'

ext.point('io.ox/contacts/vgrid/toolbar').extend({
  id: 'dropdown',
  index: 200,
  draw (baton) {
    const sortTitle = gt('Sort options')
    const dropdown = new Dropdown({
      className: 'dropdown grid-options toolbar-item margin-left-auto',
      attributes: { role: 'presentation' },
      $toggle: buttonWithIcon({
        className: 'btn btn-link',
        icon: createIcon('bi/sort-down.svg').addClass('bi-18'),
        title: sortTitle,
        ariaLabel: sortTitle
      }),
      title: sortTitle,
      dataAction: 'sort',
      model: baton.app.props,
      tabindex: -1
    })
    ext.point('io.ox/contacts/list-options').invoke('draw', dropdown.$el, baton)
    this.append(
      dropdown.render().$el
        .find('.dropdown-menu')
        .addClass('dropdown-menu-right')
        .end()
        .on('dblclick', function (e) { e.stopPropagation() }),
      createButton({
        text: gt('Select'),
        className: 'btn btn-unstyled btn-link edit'
      })
    )
  }
})
