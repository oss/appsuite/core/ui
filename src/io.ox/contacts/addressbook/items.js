/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'
import $ from '@/jquery'
import '@/io.ox/contacts/addressbook/style.scss'

import biList from 'bootstrap-icons/icons/list.svg'
import biPeople from 'bootstrap-icons/icons/people.svg'
import biGearWide from 'bootstrap-icons/icons/gear-wide.svg'

// use a template for maximum performance
// yep, no extensions here; too slow for find-as-you-type
const template = _.template(
  '<% _(list).each(function (item) { %>' +
  '<li class="list-item selectable" aria-selected="false" role="option" tabindex="-1" data-cid="<%- item.cid %>">' +
  '  <div class="list-item-checkmark"></div>' +
  '  <div class="list-item-content">' +
  '    <% if (item.list) { %>' +
  '      <div class="avatar distribution-list" aria-hidden="true">' + biList + '</div>' +
  '    <% } else if (item.type && item.type === 3) { %>' +
  '      <div class="avatar generic" aria-hidden="true">' + biGearWide + '</div>' +
  '    <% } else if (item.label) { %>' +
  '      <div class="avatar label" aria-hidden="true">' + biPeople + '</div>' +
  '    <% } else if (item.image && appeared[item.image]) { %>' +
  '      <div class="avatar image" style="background-image: url(<%- item.image %>)" aria-hidden="true"></div>' +
  '    <% } else if (item.image) { %>' +
  '      <div class="avatar image" data-original="<%- item.image %>" aria-hidden="true"></div>' +
  '    <% } else { %>' +
  '      <div class="avatar initials <%- item.initials_color %>" aria-hidden="true"><%- item.initials %></div>' +
  '    <% } %>' +
  '    <div class="name">' +
  '      <% if (item.full_name_html) { %>' +
  '        <%= item.full_name_html %>' +
  '      <% } else { %>' +
  '        <%- item.email || "\u00A0" %>' +
  '      <% } %>' +
  '      <% if (item.department) { %><span class="gray">(<%- item.department %>)</span><% } %>' +
  '    </div>' +
  '    <div class="email gray"><%- item.caption || "\u00A0" %></div>' +
  '  </div>' +
  '</li>' +
  '<% }); %>'
)

function render ($el, list, offset = 0, limit = 20) {
  const el = $el[0]
  // get subset; don't draw more than n items by default
  const subset = list.slice(offset, limit)
  if (offset === 0) el.innerHTML = ''
  el.innerHTML += template({ list: subset, appeared })
  if (offset === 0) el.scrollTop = 0
  $el.find('.avatar[data-original]').lazyload()
}

function initialize ($el) {
  $el.on('appear', onAppear)
}

function onAppear (e) {
  appeared[$(e.target).attr('data-original')] = true
}

// track avatar URLs
const appeared = {}

export default {
  render,
  initialize,
  onAppear
}
