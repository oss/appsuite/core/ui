/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'

import ox from '@/ox'
import contactsAPI from '@/io.ox/contacts/api'
import yell from '@/io.ox/core/yell'
import detailView from '@/io.ox/contacts/view-detail'

import ext from '@/io.ox/core/extensions'
import { getFullName } from '@/io.ox/contacts/util'

import gt from 'gettext'
import Backbone from 'backbone'

const NAME = 'io.ox/contacts/detail'

ox.ui.App.mediator(NAME, {

  'events' (app) {
    app.model = new Backbone.Model()

    // draw/redraw (other changes handled by detailView itself)
    app.listenTo(app.model, 'change:folder', model => {
      const { id, folder } = model.toJSON()
      app.showContact({ id, folder })
    })

    // update folder in model
    app.listenTo(contactsAPI, 'move', onMove)
    function onMove (e, ecid, data) {
      if (ecid !== _.ecid(app.model.toJSON())) return
      app.model.set('folder', data.folder_id)
    }

    app.listenTo(contactsAPI, 'delete', onDelete)
    function onDelete (e, data) {
      if (_.ecid(data) !== _.ecid(app.model.toJSON())) return
      app.stopListening(contactsAPI, 'delete', onDelete)
      app.stopListening(contactsAPI, 'move', onMove)
      app.quit()
    }
  },

  'show-contact' (app) {
    app.showContact = function (contact) {
      return contactsAPI.get(contact).done(function (data) {
        const baton = ext.Baton({ data, app })
        const label = data.mark_as_distributionlist ? gt('Distribution List Details') : gt('Contact Details')

        const containerNode = $('<div class="f6-target detail-view-container" role="region" tabindex="-1">').attr('aria-label', label)
        app.setTitle(getFullName(data))
        app.right = containerNode

        app.getWindowNode().addClass('detail-view-app').empty().append(
          containerNode.append(
            detailView.draw(baton)
          )
        )
      }).fail(yell)
    }
  }
})

// multi instance pattern
function createInstance () {
  // application object
  const app = ox.ui.createApp({
    closable: true,
    name: NAME,
    title: '',
    floating: true
  })

  // launcher
  return app.setLauncher(({ cid }) => {
    const win = ox.ui.createWindow({
      chromeless: true,
      name: NAME,
      toolbar: false,
      floating: true,
      closable: true
    })

    app.setWindow(win)
    // add overflow hidden class on mobile to help custom code show ads
    win.floating.$el.toggleClass('overflow-hidden', _.device('smartphone'))
    app.mediate()
    win.show()

    const { id, folder } = _.cid(cid || app.getState().id)

    if (!id || !folder) return
    app.setState({ id, folder })
    app.model.set({ id, folder })
  })
}

export default {
  getApp: createInstance
}
