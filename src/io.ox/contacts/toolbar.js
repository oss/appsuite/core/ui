/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import ext from '@/io.ox/core/extensions'
import { subscribe, subscribeShared } from '@/io.ox/contacts/extensions'
import ToolbarView from '@/io.ox/backbone/views/toolbar'
import api from '@/io.ox/contacts/api'
import { settings as contactsSettings } from '@/io.ox/contacts/settings'
import '@/io.ox/contacts/actions'
import '@/io.ox/contacts/style.scss'
import gt from 'gettext'

// define links for classic toolbar
ext.point('io.ox/contacts/toolbar/links').extend({
  //
  // --- HI ----
  //
  id: 'edit',
  index: 100,
  prio: 'hi',
  mobile: 'hi',
  title: gt('Edit'),
  icon: 'bi/pencil.svg',
  drawDisabled: true,
  ref: 'io.ox/contacts/actions/update'
}, {
  id: 'send',
  index: 200,
  prio: 'hi',
  mobile: 'hi',
  title: gt('Send email'),
  icon: 'bi/envelope.svg',
  ref: 'io.ox/contacts/actions/send'
}, {
  id: 'invite',
  index: 300,
  prio: 'hi',
  mobile: 'hi',
  title: gt('Invite to appointment'),
  icon: 'bi/calendar-plus.svg',
  ref: 'io.ox/contacts/actions/invite'
}, {
  id: 'delete',
  index: 400,
  prio: 'hi',
  mobile: 'hi',
  title: gt('Delete'),
  tooltip: gt('Delete contact'),
  icon: 'bi/trash.svg',
  drawDisabled: true,
  ref: 'io.ox/contacts/actions/delete'
}, {
//
// --- LO ----
// :
  id: 'vcard',
  index: 500,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Send as vCard'),
  drawDisabled: true,
  ref: 'io.ox/contacts/actions/vcard'
}, {
  id: 'move',
  index: 600,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Move'),
  ref: 'io.ox/contacts/actions/move',
  drawDisabled: true,
  section: 'file-op'
}, {
  id: 'copy',
  index: 700,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Copy'),
  ref: 'io.ox/contacts/actions/copy',
  drawDisabled: true,
  section: 'file-op'
}, {
  id: 'print',
  index: 800,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Print'),
  drawDisabled: true,
  ref: 'io.ox/contacts/actions/print',
  section: 'export'
}, {
  id: 'export',
  index: 900,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Export'),
  drawDisabled: true,
  ref: 'io.ox/contacts/actions/export',
  section: 'export'
}, {
  id: 'add-to-portal',
  index: 1000,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Add to portal'),
  ref: 'io.ox/contacts/actions/add-to-portal',
  section: 'export'
})

ext.point('io.ox/contacts/mediator').extend({
  id: 'toolbar',
  index: 10000,
  setup (app) {
    // yep strict false (otherwise toolbar does not redraw when changing between empty folders => contacts are created in the wrong folder)
    const toolbarView = new ToolbarView({ point: 'io.ox/contacts/toolbar/links', title: app.getTitle(), strict: false })
    const limit = contactsSettings.get('toolbar/limits/fetch', 100)

    app.getWindow().nodes.body.find('.rightside').addClass('classic-toolbar-visible').prepend(
      toolbarView.$el
    )

    // list is array of object (with id and folder_id)
    app.updateToolbar = function (list) {
      const options = { data: [], folder_id: this.folder.get(), app: this }
      toolbarView.setSelection(list, function () {
        if (!list.length) return options
        return (list.length <= limit ? api.getList(list) : $.when(list)).then(function (data) {
          options.data = data
          return options
        })
      })
    }
  }
})

ext.point('io.ox/contacts/mediator').extend({
  id: 'update-toolbar',
  index: 10200,
  setup (app) {
    app.updateToolbar([])
    // update toolbar on selection change as well as any model change (seen/unseen flag)
    app.getGrid().selection.on('change', function (e, list) {
      app.updateToolbar(list)
    })
    api.on('update', function () {
      app.updateToolbar(app.getGrid().selection.get())
    })
  }
})

ext.point('io.ox/contacts/list-options').extend({
  id: 'sort',
  index: 200,
  draw () {
    this.data('view')
      .divider()
      .header(gt('Sort by'))
      .option('sort', 607, gt('Last name'), { radio: true })
      .option('sort', 501, gt('First name'), { radio: true })
      .divider()
      .header(gt('Sort order'))
      .option('order', 'asc', gt('Ascending'), { radio: true })
      .option('order', 'desc', gt('Descending'), { radio: true })
  }
})

ext.point('io.ox/topbar/settings-dropdown').extend(
  {
    id: 'contacts-list-options',
    index: 200,
    render (baton) {
      if (baton.appId !== 'io.ox/contacts') return
      this
        .group(gt('List options'))
        .option('listViewLayout', 'avatars', gt('Contact pictures'), { radio: true, group: true })
      if (contactsSettings.get('selectionMode') !== 'alternative') {
        this.option('listViewLayout', 'checkboxes', gt('Checkboxes'), { radio: true, group: true })
      }
      this.option('listViewLayout', 'simple', gt('Simple'), { radio: true, group: true })
        .divider()
    }
  },
  {
    id: 'subscribe',
    index: 300,
    render: subscribe
  },
  {
    id: 'subscribe-shared-contacts',
    index: 400,
    render: subscribeShared
  }
)
