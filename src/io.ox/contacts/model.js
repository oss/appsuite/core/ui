/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'

import ModelFactory from '@/io.ox/backbone/modelFactory'
import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/contacts/api'
import * as settingsUtil from '@/io.ox/settings/util'
import strings from '@/io.ox/core/strings'

import gt from 'gettext'

function buildFactory (ref, api) {
  const isMyContactData = ref === 'io.ox/core/user/model'
  const factory = new ModelFactory({
    api,
    ref,
    updateEvents: ['edit'],

    create (model) {
      let data = model.toJSON()
      const file = data.pictureFileEdited || data.pictureFile
      // clean up data
      data = _.omit(data, ['crop', 'pictureFile', 'pictureFileEdited', 'image1'])
      return api.create(data, file)
    },

    update (model) {
      // Some special handling for profile pictures
      let data = model.changedSinceLoading()
      const file = data.pictureFileEdited || data.pictureFile
      const yell = !isMyContactData ? _.identity : settingsUtil.yellOnReject
      // clean up data
      data = _.omit(data, ['crop', 'pictureFile', 'pictureFileEdited'])
      // update
      if (file) return api.editNewImage({ id: model.id, folder_id: model.get('folder_id') }, _.omit(data, 'image1'), file)
      return yell(
        api.update({ id: model.id, folder: model.get('folder_id'), last_modified: model.get('last_modified'), data })
      )
    },

    destroy (model) {
      return api.remove({ id: model.id, folder_id: model.get('folder_id') })
    }
  })

  ext.point(ref + '/validation').extend({
    id: 'upload_quota',
    validate (attributes) {
      if (attributes.quotaExceeded) {
        this.add('attachments_list', gt('Files can not be uploaded, because upload limit of %1$s is exceeded.', strings.fileSize(attributes.quotaExceeded.attachmentMaxUploadSize, 2)))
      }
    }
  })

  ext.point(ref + '/validation').extend({
    id: 'birthday',
    validate (attributes) {
      // null is valid because this is the value to remove a birthday
      // undefined means the user has a month but no day selected or the other way round.
      // This way we can see if the input is incomplete or the birthday should be removed
      if ('birthday' in attributes && attributes.birthday !== null && !_.isNumber(attributes.birthday)) {
        this.add('birthday', gt('Please set day and month properly'))
      }
    }
  })

  ext.point(ref + '/validation').extend({
    id: 'first_name',
    validate (attributes) {
      // if this contact is based on a user, first_name and last_name must not be empty (dipslay name is generated from them)
      if (attributes.user_id && (!attributes.first_name || _.isEmpty(String(attributes.first_name).trim()))) {
        this.add('first_name', gt('First name must not be empty for internal users'))
      }
    }
  })

  ext.point(ref + '/validation').extend({
    id: 'last_name',
    validate (attributes) {
      // if this contact is based on a user, first_name and last_name must not be empty (dipslay name is generated from them)
      if (attributes.user_id && (!attributes.last_name || _.isEmpty(String(attributes.last_name).trim()))) {
        this.add('last_name', gt('Last name must not be empty for internal users'))
      }
    }
  })

  return factory
}

export const fields = {
  // noun
  display_name: gt('Display name'),
  first_name: gt('First name'),
  last_name: gt('Last name'),
  second_name: gt('Middle name'),
  suffix: gt('Suffix'),
  title: gt.pgettext('salutation', 'Title'),
  street_home: gt('Street'),
  postal_code_home: gt('Postcode'),
  city_home: gt('City'),
  state_home: gt('State'),
  country_home: gt('Country'),
  birthday: gt('Date of birth'),
  marital_status: gt('Marital status'),
  number_of_children: gt('Children'),
  profession: gt('Profession'),
  nickname: gt('Nickname'),
  spouse_name: gt('Spouse\'s name'),
  anniversary: gt('Anniversary'),
  note: gt.pgettext('contact', 'Note'),
  department: gt('Department'),
  position: gt('Position'),
  employee_type: gt('Employee type'),
  room_number: gt('Room number'),
  street_business: gt('Street'),
  postal_code_business: gt('Postcode'),
  city_business: gt('City'),
  state_business: gt('State'),
  country_business: gt('Country'),
  number_of_employees: gt('Employee ID'),
  sales_volume: gt('Sales Volume'),
  tax_id: gt('TAX ID'),
  commercial_register: gt('Commercial Register'),
  branches: gt('Branches'),
  business_category: gt('Business category'),
  info: gt('Info'),
  manager_name: gt('Manager'),
  assistant_name: gt('Assistant'),
  street_other: gt('Street'),
  city_other: gt('City'),
  postal_code_other: gt('Postcode'),
  country_other: gt('Country'),
  telephone_business1: gt('Phone (business)'),
  telephone_business2: gt('Phone (business alt)'),
  fax_business: gt('Fax'),
  telephone_callback: gt('Telephone callback'),
  telephone_car: gt('Phone (car)'),
  telephone_company: gt('Phone (company)'),
  telephone_home1: gt('Phone (home)'),
  telephone_home2: gt('Phone (home alt)'),
  fax_home: gt('Fax (Home)'),
  cellular_telephone1: gt('Cell phone'),
  cellular_telephone2: gt('Cell phone (alt)'),
  telephone_other: gt('Phone (other)'),
  fax_other: gt('Fax (alt)'),
  email1: gt('Email 1'),
  email2: gt('Email 2'),
  email3: gt('Email 3'),
  url: gt('URL'),
  telephone_isdn: gt('Telephone (ISDN)'),
  telephone_pager: gt('Pager'),
  telephone_primary: gt('Telephone primary'),
  telephone_radio: gt('Telephone radio'),
  telephone_telex: gt('Telex'),
  telephone_ttytdd: gt('TTY/TDD'),
  instant_messenger1: gt('Instant Messenger 1'),
  instant_messenger2: gt('Instant Messenger 2'),
  telephone_ip: gt('IP phone'),
  telephone_assistant: gt('Phone (assistant)'),
  company: gt('Company'),
  image1: gt('Image 1'),
  userfield01: gt('Optional 01'),
  userfield02: gt('Optional 02'),
  userfield03: gt('Optional 03'),
  userfield04: gt('Optional 04'),
  userfield05: gt('Optional 05'),
  userfield06: gt('Optional 06'),
  userfield07: gt('Optional 07'),
  userfield08: gt('Optional 08'),
  userfield09: gt('Optional 09'),
  userfield10: gt('Optional 10'),
  userfield11: gt('Optional 11'),
  userfield12: gt('Optional 12'),
  userfield13: gt('Optional 13'),
  userfield14: gt('Optional 14'),
  userfield15: gt('Optional 15'),
  userfield16: gt('Optional 16'),
  userfield17: gt('Optional 17'),
  userfield18: gt('Optional 18'),
  userfield19: gt('Optional 19'),
  userfield20: gt('Optional 20'),
  links: gt('Links'),
  distribution_list: gt('Distribution list'),
  state_other: gt('State'),
  mark_as_distributionlist: gt('Mark as distribution list'),
  default_address: gt('Default address'),
  addressHome: gt('Address Home'),
  addressBusiness: gt('Address Business'),
  addressOther: gt('Address Other'),
  private_flag: gt('This contact is private and cannot be shared'),
  // don't think we show this anywhere so we don't use gt here not to annoy translators
  number_of_links: 'Number of links',
  number_of_images: 'Number of images',
  image_last_modified: 'Image last modified',
  file_as: 'File as',
  image1_content_type: 'Image1 content type',
  image1_url: 'Image1 url',
  internal_userid: 'Internal userid',
  useCount: 'use Count',
  yomiFirstName: 'yomi First Name',
  yomiLastName: 'yomi Last Name',
  yomiCompany: 'yomi Company'
}

export const factory = buildFactory('io.ox/contacts/model', api)
export const protectedMethods = { buildFactory }

export default {
  Contact: factory.model,
  Contacts: factory.collection,
  factory,
  api,
  fields,
  protectedMethods
}
