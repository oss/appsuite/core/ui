/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import gt from 'gettext'
import Backbone from '@/backbone'
import { createButton } from '@/io.ox/core/components'

const SelectAllButtonView = Backbone.View.extend({
  events: {
    'click .select-all': 'selectAll',
    'click .deselect-all': 'deselectAll'
  },
  initialize (options) {
    this.$selectAll = createButton({ text: gt('Select all'), className: 'btn btn-unstyled btn-link select-all' })
    this.$deselectAll = createButton({ text: gt('Deselect all'), className: 'btn btn-unstyled btn-link deselect-all' })

    if (options.listView) this.setupList(options)
    else if (options.grid) this.setupGrid(options)
  },
  render () {
    this.$el.append(this.$selectAll)
    return this
  },
  setupList (options) {
    this.selection = options.listView.selection
    options.listView.on('selection:change', selection => {
      // only trigger if there was a selection before, otherwise we create a circular path
      if (selection.length && !options.listView.collection.length) options.navbar.trigger('editMode')
      const allSelected = this.selection.get().length === this.selection.getItems().length
      this.$el.empty().append(allSelected ? this.$deselectAll : this.$selectAll)
    })
  },
  setupGrid (options) {
    this.selection = options.grid.selection
    this.selection.on('change', () => {
      const allSelected = this.selection.get().length === options.grid.getIds().length
      this.$el.empty().append(allSelected ? this.$deselectAll : this.$selectAll)
    })
    if (!options.grid.getIds().length) this.$selectAll.prop('disabled', true)
  },
  selectAll () {
    this.selection.selectAll()
  },
  deselectAll () {
    this.selection.clear()
  }
})

export default SelectAllButtonView
