/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import ext from '@/io.ox/core/extensions'
import { buttonWithIcon, createIcon } from '@/io.ox/core/components'
import gt from 'gettext'
import '@/themes/components/dropdown-mobile.scss'
import ox from '@/ox'

const topObserver = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    const ul = entry.target.parentElement
    if (ul.classList.contains('launcher-dropdown')) return
    if (entry.isIntersecting) {
      ul.classList.remove('pinned')
      document.dispatchEvent(new Event('update:theme-color'))
    } else if (ul.offsetParent !== null) {
      if (entry.boundingClientRect.y > 0) {
        // don't add pinned class if scrolling out the bottom
        return
      }
      ul.classList.add('pinned')
      setThemeColor(ul)
    }
  })
})

const resizeObserver = new ResizeObserver(entries => {
  // in case the dropdown shrinks while beeing displayed, it might float above
  // the bottom of the display
  entries.forEach(entry => {
    const { height, bottom } = entry.target.getBoundingClientRect()
    if (!height || bottom <= 0 || bottom > innerHeight) {
      // !height: dropdown is not displayed at all
      // bottom < 0: dropdown correctly positioned
      // bottom > innerHeight: dropdown outside viewport
      return
    }
    // dropdown floats above the bottom of the screen
    entry.target.style.top = innerHeight - height + 'px'
  })
})

function setThemeColor (ul) {
  const event = new Event('update:theme-color')
  event.color = document.defaultView.getComputedStyle(ul).backgroundColor
  event.ignoreOverlay = true
  document.dispatchEvent(event)
}

function drawTopHeader (baton = ext.Baton()) {
  if (this.$ul.find('.top-header').length) {
    this.$title.text(baton.dropdownTitle)
    return
  }
  this.$title = $('<h2 class="text-lg font-bold ellipsis m-0">').text(baton.dropdownTitle)

  this.$observable = $('<li class="observable" role="none" style="min-height: 0; display: block;">')
    .on('dispose', () => {
      if (this.disposed) return
      topObserver.unobserve(this.$observable.get(0))
    })
  topObserver.observe(this.$observable.get(0))

  this.$ul
    .addClass('custom-dropdown')
    .prepend(
      this.$observable,
      $('<li class="top-header" role="separator">').append(this.$title),
      $('<li class="close-button" role="menuitem">')
        .append(
          buttonWithIcon({
            ariaLabel: gt('Close menu'),
            className: 'btn btn-unstyled btn-close p-4',
            icon: createIcon('bi/x.svg').addClass('bi-24'),
            title: gt('Close menu')
          }).attr('tabindex', '-1')
            .on('click', event => { this.close(event) })
        )
        .on('click', (event) => {
          event.stopImmediatePropagation()

          const duration = Math.min(400, 400 * (innerHeight - this.$ul.position().top) / innerHeight)
          this.$ul.stop(true).animate({ top: '100%' }, duration)
          setTimeout(() => {
            if (this.$el.hasClass('open')) this.$toggle.trigger('click')
          }, duration)
        })
    )
}

function addToResizeObserver () {
  const dropdownList = this.$ul[0]
  this.on('open', () => { resizeObserver.observe(dropdownList) })
  this.on('close', () => { resizeObserver.unobserve(dropdownList) })
}

ext.point('io.ox/core/dropdown').extend({
  id: 'top-header',
  draw: drawTopHeader,
  redraw: drawTopHeader
}, {
  id: 'height-observer',
  draw: addToResizeObserver
}, {
  id: 'touch-events',
  draw () {
    // drag dropdown downwards an close it eventually
    const dropdown = this
    let position
    let closing

    this.$ul
      .on('touchstart', onTouchStart)
      .on('touchmove', onTouchMove)
      .on('touchend', onTouchEnd)

    function onTouchStart () {
      if (position !== undefined) return
      position = event.changedTouches[0].clientY
    }

    function onTouchMove (event) {
      const maxTop = innerHeight - event.currentTarget.getBoundingClientRect().height
      const newPosition = event.changedTouches[0].clientY
      const scroll = newPosition - position
      position = newPosition
      this.style.top = Math.max(maxTop, this.offsetTop + scroll) + 'px'
    }

    function onTouchEnd (event) {
      if (closing || event.changedTouches.length > 1) return
      const top = parseFloat(this.style.top)
      position = undefined

      if (top > 0.85 * innerHeight) {
        $(this).animate({ top: '100%' }, 200)
        closing = !!setTimeout(() => {
          dropdown.close()
          closing = false
        }, 200)
      } else if (top > 0.5 * innerHeight) {
        const newTop = this.offsetHeight < 0.5 * innerHeight ? `${innerHeight - this.offsetHeight}px` : '50%'
        $(this).animate({ top: newTop }, 200)
      }
    }
  }
}, {
  id: 'show',
  show () {
    const maxTop = Math.max(innerHeight - this.$ul.height(), innerHeight / 2)
    const duration = 400 * (this.$ul.position().top - maxTop) / innerHeight
    ox.disable()
    this.$ul
      .find('.divider').remove()
    this.$ul.find('.btn-close').attr('tabindex', '0')
    if (!this.$ul.hasClass('scrolled')) this.$ul.addClass('scrolled').css('top', '100%')
    this.$ul.stop(true).animate({ top: `${maxTop - 16}px` }, duration)
  }
}, {
  id: 'hide',
  hide () {
    this.$ul.removeClass('scrolled complete')
    this.$ul.find('[tabindex="0"').attr('tabindex', '-1')
    ox.idle()
  }
})
