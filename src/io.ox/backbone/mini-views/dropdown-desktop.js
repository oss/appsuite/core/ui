/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import ext from '@/io.ox/core/extensions'

ext.point('io.ox/core/dropdown').extend({
  id: 'default',
  adjustBounds () {
    if (this.$toggle.hasClass('more-dropdown')) {
      // more drop downs have their own way to adjust bounds
      // see extension `bounds-more`
      return
    }
    const isDropUp = !!this.options.dropup
    const bounds = this.$ul.get(0).getBoundingClientRect()
    if (this.useToggleWidth) bounds.width = this.$toggle.outerWidth()
    const margins = {
      top: parseInt(this.$ul.css('margin-top') || 0, 10),
      left: parseInt(this.$ul.css('margin-left') || 0, 10)
    }
    let top = bounds.top
    let left = bounds.left
    // prefer data over bounds (to avoid misalignment)
    const minWidth = this.getMinWidth()

    const data = this.$ul.data()
    if (data.top) top = data.top
    if (data.left) left = data.left
    const positions = {
      top: top - margins.top,
      left: left - margins.left,
      right: 'auto', /* rtl */
      width: bounds.width < minWidth ? minWidth : bounds.width,
      height: 'auto'
    }
    const offset = this.$toggle.offset()
    const width = this.$toggle.outerWidth()
    const availableWidth = $(window).width()
    const availableHeight = $(window).height()
    const topbar = $('#io-ox-appcontrol')

    if (isDropUp) {
      const top = this.$el.get(0).getBoundingClientRect().top
      positions.top = top - bounds.height
      // adjust height
      positions.height = bounds.height
      positions.height = Math.min(availableHeight - this.margin - positions.top, positions.height)

      // outside viewport?
      positions.left = Math.max(this.margin, positions.left)
      positions.left = Math.min(availableWidth - positions.width - this.margin, positions.left)
    } else if ((top + bounds.height > availableHeight - this.margin)) {
      // hits bottom

      // left or right?
      if ((offset.left + width + bounds.width + this.margin) < availableWidth) {
        // enough room on right side
        positions.left = offset.left + width + this.margin
      } else {
        // position of left side
        positions.left = offset.left - bounds.width - this.margin
      }

      // move dropdown up
      positions.top = availableHeight - this.margin - bounds.height
      // don't overlap topbar or banner
      positions.top = Math.max(positions.top, topbar.offset().top + topbar.height())
      // adjust height
      positions.height = bounds.height
      positions.height = Math.min(availableHeight - this.margin - positions.top, positions.height)
    } else {
      // outside viewport?
      positions.left = Math.max(this.margin, positions.left)
      positions.left = Math.min(availableWidth - positions.width - this.margin, positions.left)
    }

    // overflows top
    if (positions.top < 0) {
      positions.height = positions.height + positions.top
      positions.top = 0
      this.$overlay.addClass('scrollable')
    }

    if (this.$toggle.data('fixed')) positions.left = bounds.left
    this.$ul.css(positions)
  }
}, {
  id: 'bounds-more',
  adjustBounds (baton) {
    if (!this.$toggle.hasClass('more-dropdown')) return
    baton.preventDefault() // prevents `adjustBounds` from `default` beeing executed

    const data = this.$ul.data()
    const pos = { right: 'auto', bottom: 'auto' }
    const menu = this.$ul.get(0).getBoundingClientRect()
    if (data.top !== undefined) {
      // use predefined position, e.g. originating from a right click
      pos.top = data.top
      pos.left = data.left
    } else {
      const box = this.$toggle.get(0).getBoundingClientRect()
      pos.top = box.bottom
      pos.left = this.$ul.hasClass('dropdown-menu-right') ? box.right - this.$ul.width() : box.left
    }
    // ensure proper position inside viewport
    pos.top = Math.max(0, Math.min(pos.top, innerHeight - menu.height))
    pos.left = Math.max(0, Math.min(pos.left, innerWidth - menu.width))
    this.$ul.css(pos)
  }
})
