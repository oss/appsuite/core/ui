/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import _ from '@/underscore'

import * as util from '@/io.ox/core/util'
import { hasFeature } from '@/io.ox/core/feature'
import gt from 'gettext'

// Validates a name against conditions, returning warnings as an array
export function validateName (name = '', type = 'file', { noSlashes = false, noColons = false } = {}) {
  const warnings = []
  const trimmedName = name?.trim()

  if (!trimmedName) {
    return [type === 'file'
      ? gt('File names must not be empty')
      : gt('Folder names must not be empty')
    ]
  }

  if (noSlashes && /\//.test(trimmedName)) {
    warnings.push(type === 'file'
      ? gt('File names must not contain slashes')
      : gt('Folder names must not contain slashes')
    )
  }

  if (noColons && /[:]/.test(trimmedName)) {
    warnings.push(type === 'file'
      ? gt('File names must not contain colons')
      : gt('Folder names must not contain colons')
    )
  }

  return warnings
}

function isEmpty (value) {
  return (_.isUndefined(value) || value === null || value === '')
}

export const formats = {
  // numbers with "." or "," as a separator are valid 1.23 or 1,23 for example
  anyFloat (val) {
    // empty value is valid (if not, add the mandatory flag)
    if (isEmpty(val)) return true

    val = String(val)
      .replace(/,/g, '.')
    // remove zero only decimal places
      .replace(/\.0*$/, '')
    // check if its a number
    const isValid = (!isNaN(parseFloat(val, 10)) &&
                          // check if parseFloat did not cut the value (1ad2 would be made to 1 without error)
                          (parseFloat(val, 10).toString().length === val.toString().length))
    return isValid ||
              gt('Please enter a valid number')
  },
  number (val) {
    // empty value is valid (if not, add the mandatory flag)
    const isValid = (isEmpty(val)) ||
                          // check if its a number
                          (!isNaN(parseFloat(val, 10)) &&
                          // check if parseFloat did not cut the value (1ad2 would be made to 1 without error)
                          (parseFloat(val, 10).toString().length === val.toString().length))
    return isValid || gt('Please enter a valid number')
  },
  array (val) {
    return _.isArray(val) || 'Please enter a valid array'
  },
  boolean (val) {
    return _.isBoolean(val) || 'Please enter a bool'
  },
  date (val) {
    // val: timestamp
    // tasks allows null values to remove a date. Calendar must have start and end date
    // calendar fields use val = undefined if they are empty so this should work correctly for both systems
    if ((val !== null && !_.isNumber(val)) || val > 253402214400008) {
      return gt('Please enter a valid date')
    }
    return true
  },
  pastDate (val) {
    if (_.isString(val)) {
      if (val !== '') return gt('Please enter a valid date')
    }
    return _.now() > val || gt('Please enter a date in the past')
  },
  email (val) {
    // enabled by default
    return hasFeature('validateMailAddresses') ||
                util.isValidMailAddress(val) ||
                gt('Please enter a valid email address')
  },
  phone (val) {
    // disabled by default
    return hasFeature('validatePhoneNumbers') ||
                util.isValidPhoneNumber(val) ||
                gt('Please enter a valid phone number. Allowed characters are: %1$s', '0-9 , . - ( ) # + ; /')
  },
  object (val) {
    return _.isObject(val) ||
                gt('Please enter a valid object')
  }
}

// DEPRECATED: `validationFor`, pending full removal with 8.36
export function validationFor () {
  console.error('Function `validationFor` from `io.ox/backbone/validation.js` is disabled, pending full removal with 8.36.')
}

export default {
  formats,
  validationFor
}
