/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import _ from '@/underscore'
import gt from 'gettext'
import yell from '@/io.ox/core/yell'
import tinymce from 'tinymce'
const cache = {}

tinymce.PluginManager.add('oximage', editor => {
  editor.getPendingDeferreds = function (ids) {
    return _(ids)
      .chain()
      .map(id => cache[id])
      .compact()
      .value()
  }

  /**
   * Uploads a blob image file and inserts it at the cursor position.
   */
  editor.uploadBlob = function (blob, opt) {
    opt = opt || {}

    // store in tinymce blobcache
    const reader = new window.FileReader()
    reader.readAsDataURL(blob)
    reader.onloadend = function () {
      const id = _.uniqueId('upload-image-')
      const blobcache = editor.editorUpload.blobCache
      const blobInfo = blobcache.create(id, blob, reader.result)
      blobcache.add(blobInfo)

      editor.addImage({ src: blobInfo.blobUri(), id, from_clipboard: opt.from_clipboard })
    }
    reader.onerror = function () {
      console.error(reader.error)
    }
  }

  editor.addImage = function (opt) {
    editor.undoManager.transact(function () {
      editor.focus()
      // tinymce pasteplugin seems to strip ids so provide as class for now
      const attr = { src: opt.src, 'data-mce-src': opt.src, style: 'max-width: 100%;', class: 'aspect-ratio', alt: '', 'data-pending': 'true', 'data-id': opt.id }
      if (opt.from_clipboard) attr.from_clipboard = true
      editor.selection.setContent(editor.dom.createHTML('img', attr))
    })
  }

  editor.ui.registry.addButton('image', {
    icon: 'image',
    title: gt('Insert image'),
    tooltip: gt('Insert image'),
    onAction () {
      const fileInput = $('<input type="file" name="file" accept="image/*" capture="camera" style="display: none;">')
      $('body').append(fileInput)

      function change () {
        const file = $(this)

        if (!(/\.(gif|bmp|tiff|jpe?g|gmp|png)$/i).test(file.val())) {
          yell('error', gt('Please select a valid image file to insert'))
        } else if (file[0].files && editor) {
          editor.uploadBlob(file[0].files[0])
        }
        fileInput.remove()
      }

      fileInput.on({
        change,
        cancel () { fileInput.remove() }
      })

      fileInput.trigger('click')
    }
  })
})
