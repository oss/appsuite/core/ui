/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import dropzone from '@/io.ox/core/dropzone'
import yell from '@/io.ox/core/yell'
import gt from 'gettext'
import tinymce from 'tinymce'

tinymce.PluginManager.add('oxdrop', function (editor) {
  editor.on('PreInit', function () {
    function onDrop (e) {
      const dataTransfer = e.originalEvent.dataTransfer
      if (dataTransfer && dataTransfer.types.includes('text/uri-list')) {
        const text = decodeURIComponent(dataTransfer.getData('text/uri-list'))
        const list = text.match(/([^,]("[^"]*")?)+/g)

        list.forEach(item => {
          // only use image tags
          if (!/^<img /.test(item)) return
          const $elem = $(item)
          if ($elem.attr('data-pending') !== 'true') return
          const id = $elem.data('id')
          const img = $(editor.getElement()).find(`img[data-id="${id}"]`)
          if (!img.length) return
          // remove data from dataTransfer instead of stop propagation
          // therefore, tinymce executes drag and drop without content (especially sets the cursor)
          dataTransfer.setData('text/html', '')
          const html = img.prop('outerHTML')
          setTimeout(() => editor.insertContent(html), 100)
        })
      }
    }

    // capture drop event to prevent bug in tinymce paste plugin when a html image is dragged
    $(editor.contentDocument)
      .on('dragover', false)
      .on('drop', onDrop)

    const target = $(editor.getContainer())
    const zone = new dropzone.Inplace({
      caption: gt('Drop inline images here'),
      eventTarget: $(editor.contentDocument)
    })

    zone.on({
      show () {
        const compose = $(editor.getContentAreaContainer()).closest('.window-body').find('.mail-compose-fields')
        // don't adjust styles when not in mail compose, e.g. signature editor
        if (!compose.length) return
        // reserved height for for attachmentdropzone (might still be hidden so we have to check)
        const attachmentzoneheight = Math.max(132 - (compose.find('.attachments').height() || 0), 0)
        zone.$el.css({ top: compose.height() + attachmentzoneheight + 'px', height: '40%', margin: 0 })
      },
      drop (files) {
        const hasNonImages = files.reduce((memo, file) => memo || !/^image/.test(file.type), false)
        if (hasNonImages) {
          yell('error', gt('Please only drop images here. If you want to send other files, you can send them as attachments.'))
        } else {
          files.forEach(editor.uploadBlob)
        }
      }
    })

    target.after(zone.render().$el.addClass('abs'))
  })
})
