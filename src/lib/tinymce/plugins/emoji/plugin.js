/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '@/jquery'
import gt from 'gettext'
import EmojiView from '@/io.ox/core/emoji/view.js'
import tinymce from 'tinymce'

tinymce.PluginManager.add('emoji', editor => {
  const view = new EmojiView({ editor })
  const panelId = 'emoji-' + editor.id

  const dialogConfig = {
    title: 'Emojicon',
    body: {
      type: 'panel',
      items: [{
        type: 'htmlpanel',
        html: `<div id="${panelId}">`
      }]
    },
    buttons: [],
    onClose () {
      if (view) view.toggle()
    }
  }

  editor.ui.registry.addButton('emoji', {
    tooltip: gt('Emoticons'),
    icon: 'emoji',
    onAction () {
      editor.windowManager.open(dialogConfig)
      if ($('#' + panelId).is(':empty') && view) {
        $('#' + panelId).append(view.$el)
      }
      if (view) {
        view.toggle()
        view.$el.closest('.tox-dialog').addClass('emoji')
      }
    }
  })
})
