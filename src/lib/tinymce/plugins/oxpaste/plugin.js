/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from 'jquery'
import _ from '@/underscore'
import yell from '@/io.ox/core/yell.js'
import gt from 'gettext'
import tinymce from 'tinymce'

tinymce.PluginManager.add('oxpaste', editor => {
  /**
   * React on pasted images. This is mainly for chrome support
   */
  editor.on('paste', function (e) {
    const clipboard = (e.clipboardData || (e.originalEvent && e.originalEvent.clipboardData) || window.clipboardData)
    if (clipboard) {
      const items = clipboard.items
      _(items).each(function (item) {
        // skip non-images
        if (!/^image/.test(item.type)) return
        const file = item.getAsFile()
        _.defer(function () { editor.uploadBlob(file, { from_clipboard: true }) })
        // prevent default when an image is pasted to prevent duplication
        // when using the copy picture function the picture is added to the clipboard object as html and image. We only need the image part.
        e.preventDefault()
      })
    }
  })

  /**
   * Process pasted images with encoded url.
   * Those images are inserted with local object url url and uploaded meanwhile. After upload, the url is replaced.
   */
  editor.on('PastePostProcess', _.debounce(function (e) {
    $('img', $(e.node)).each(function (index, elem) {
      const $elem = $(elem)
      const src = $elem.attr('src')

      if (/^data:/.test(src)) {
        // Some browsers directly inserts base64 encoded images.
        const byteString = atob(src.split(',')[1])
        const mimeString = src.split(',')[0].split(':')[1].split(';')[0]
        const buffer = new window.ArrayBuffer(byteString.length)
        const intArray = new window.Uint8Array(buffer)

        for (let i = 0; i < byteString.length; i++) {
          intArray[i] = byteString.charCodeAt(i)
        }

        // uploads and inserts the image
        if (intArray.length) editor.uploadBlob(new Blob([intArray], { type: mimeString }))
      } else if (src.indexOf('webkit-fake-url') !== -1) {
        // Safari uses webkit-fake-urls instead of base64 encoded urls, which disallows image data access
        yell('warning', gt('Pasting images is not supported by your browser!'))
      } else if ($elem.attr('data-pending') && !$elem.attr('from_clipboard')) {
        // this image is already uploading and needs to be previewed
        // if inserted from clipboard the uploadblob function handles this already
        editor.addImage({ src, id: $elem.attr('id') })
      }
    })
  }, 0))
})
