import _ from '@/underscore'
import $ from '@/jquery'
import Backbone from '@/backbone'

import ox from '@/ox'
import upsell from '@/io.ox/core/upsell'
import { settings as coreSettings } from '@/io.ox/core/settings'
import { createIcon, createIllustration } from '@/io.ox/core/components'

export default Backbone.View.extend({

  tagName: 'div',

  events: {
    'click a': 'onClick'
  },

  onClick (e) {
    e.preventDefault()

    upsell.trigger({
      type: 'custom',
      id: this.opt.id,
      missing: upsell.missing(this.opt.requires)
    })
  },

  initialize (opt) {
    this.opt = _.extend({
      color: 'var(--upsell)',
      icon: coreSettings.get('upsell/defaultIcon', 'bi/stars.svg'),
      linkClass: '',
      enabled: true
    }, opt, coreSettings.get('features/upsell/' + opt.id), coreSettings.get('features/upsell/' + opt.id + '/i18n/' + ox.language))

    // A11y: Links in folder tree need to be nested in li with role presentation
    if (/^folderview\//.test(opt.id)) {
      // this.setElement('<li role="presentation">')
      this.$el.addClass(this.className)
    }

    // ensure io-ox-upsell-link class
    this.$el.addClass('io-ox-upsell-link')

    this.customize = this.opt.customize
    this.icon = this.opt.icon
    this.visible = this.opt.enabled && !upsell.has(this.opt.requires) && upsell.enabled(this.opt.requires)
  },

  render () {
    if (this.visible) {
      const node = $('<a href="#">').addClass(this.opt.linkClass)
      if (this.opt.illustration) {
        node.addClass('flex-row flex-center').append(
          createIllustration('illustrations/rocket.svg', { width: 40 }).addClass('me-16 shrink-0')
        )
      } else if (this.icon) {
        node.append(createIcon(this.icon).addClass('me-8').css('color', this.opt.color))
      }
      node.append($.txt(this.opt.title))
      // A11y: Links in foldertree need to be role treeitem
      // if (/^folderview\//.test(this.opt.id)) node.attr('role', 'treeitem')
      this.$el.append(node)
      if (this.customize && _.isFunction(this.customize)) this.customize()
    } else {
      this.$el.hide()
    }
    return this
  }
})
