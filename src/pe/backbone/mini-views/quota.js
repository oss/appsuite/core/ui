import Backbone from '@/backbone'
import $ from '@/jquery'

import quotaAPI from '@/io.ox/core/api/quota'
import strings from '@/io.ox/core/strings'
import UpsellView from '@/pe/backbone/mini-views/upsell'
import capabilities from '@/io.ox/core/capabilities'

import gt from 'gettext'
import { settings } from '@/io.ox/mail/settings'

const QuotaView = Backbone.View.extend({

  tagName: 'div',

  initialize (options) {
    this.options = {
      module: 'mail',
      quotaField: 'quota',
      usageField: 'use',
      renderUnlimited: true,
      renderThreshold: 0,
      sizeFunction: size => strings.fileSize(size, 'smart'),
      upsellLimit: -1,
      ...options
    }

    this.$el.addClass('io-ox-quota-view')

    this.listenTo(quotaAPI.mailQuota, 'change', this.updateQuota)
    this.listenTo(quotaAPI.fileQuota, 'change', this.updateQuota)

    this.$el.hide()
  },

  getQuota (forceReload) {
    const { quota, usage, quotaField, usageField, module } = this.options

    // use forceReload to prevent using the cashed data
    // quotaAPI.get will trigger an automated redraw
    if (!forceReload && quota && usage) return $.when({ quota, usage })

    if (forceReload && module === 'file') quotaAPI.requestFileQuotaUpdates()
    const request = forceReload ? quotaAPI.reload() : quotaAPI.load()

    return request.then(result => {
      return {
        quota: result[module][quotaField],
        usage: result[module][usageField]
      }
    })
  },

  updateQuota () {
    const { quotaField, usageField, module } = this.options
    const data = quotaAPI.getModel(module).toJSON()
    if (data[quotaField] === undefined || data[usageField] === undefined) return
    this.options.quota = data[quotaField]
    this.options.usage = data[usageField]
    this.render()
  },

  renderTitle ({ quota, usage }) {
    const sizeFunction = this.options.sizeFunction
    let label
    this.$el.append(
      $('<div class="quota-description">').append(
        $('<div class="title">').text(this.options.title),
        label = $('<div class="numbers">')
      )
    )

    if (quota < 0) {
      label.text(
        // -1 means unlimited; if mail server is down (no inbox) we say unknown
        this.options.module !== 'mail' || settings.get('folder/inbox') ? gt('unlimited') : gt('unknown')
      )
    } else if (sizeFunction) {
      label.text(
        // #. %1$d is the storagespace in use
        // #. %2$d is the max storagespace
        // #, c-format
        gt('%1$d of %2$d used', sizeFunction(Math.min(usage, quota)), sizeFunction(quota))
      )
    } else {
      label.text(
        // #. %1$d is the used mail count in percent (hence the appended %-sign)
        gt('%1$d%% of mail count remaining', Math.round(100 - usage * 100 / quota))
      )
    }
  },

  renderBar ({ quota, usage }) {
    // do not render the bar with unlimited quota
    if (quota <= 0) return
    // #. %1$s is the quota in use
    // #. %2$s is the max quota
    // #, c-format
    const title = quota && usage && !this.options.sizeFunction ? gt('%1$s of %2$s used', usage, quota) : ''
    // max is 100%, of course; visual minimum is 5%
    let width = Math.max(5, Math.min(100, Math.round(usage / quota * 100)))

    if (!quota) width = 100
    if (!usage) width = 0

    this.$el.append(
      $('<div class="progress">')
        .append(
          $('<div class="progress-bar">')
            .css('width', width + '%')
            .addClass(width < 90 ? 'default' : 'bar-danger')
            .attr('title', title)
        )
    )
  },

  renderUpsell ({ quota }) {
    // only draw when upsell is activated
    if (!this.options.upsell) return
    const view = new UpsellView(this.options.upsell)
    const upsellLimit = view.opt.upsellLimit || this.options.upsellLimit

    // unlimited quota?
    if (upsellLimit <= 0) return
    // check if demomode or hitting upsell limit
    if (!view.opt.demo && quota >= upsellLimit) return

    this.$el.append(view.render().$el)
  },

  render () {
    // do not render for guests
    if (capabilities.has('guest')) return this

    this.getQuota().done(result => {
      const { quota, usage } = result
      const renderThreshold = this.options.renderThreshold
      // do not render if quota fields are undefined
      if (quota === undefined || usage === undefined) return
      // do not render when quota is unlimited
      if (!this.options.renderUnlimited && quota <= 0) return

      // do not render if render threshold is not reached
      if ((renderThreshold > 0 && quota < 0) || renderThreshold > usage / quota) {
        this.$el.remove()
        return
      }

      this.$el.empty()
      this.renderTitle(result)
      this.renderBar(result)
      this.renderUpsell(result)
      // show element after adding data
      this.$el.show()
    })

    return this
  }
})

export default QuotaView
