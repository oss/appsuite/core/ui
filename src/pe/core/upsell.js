import _ from '@/underscore'
import $ from '@/jquery'
import ox from '@/ox'
import capabilities from '@/io.ox/core/capabilities'

import { settings as coreSettings } from '@/io.ox/core/settings'
import { addReadyListener, triggerReady } from '@/io.ox/core/events'

function upgrade (options) {
  console.debug('upsell:upgrade', options)
  window.open(ox.root + '/pe/upsell/checkout.html?' + $.param(options.plan), 'checkout')
}

let enabled = {}

// local copy for speed
let capabilityCache = {}
const enabledCache = {}

// enabled = { infostore: true, calendar: true } // uncomment for debugging

const that = {
  // convenience functions
  trigger (options) {
    ox.trigger('upsell:requires-upgrade', options || {})
  },

  // simple click handler
  click (e) {
    e.preventDefault()
    that.trigger()
  },

  // find one set of capabilities that matches
  // returns true or false
  any (array) {
    if (!array) return true
    return _([].concat(array)).reduce(function (memo, c) {
      return memo || c === undefined || that.has(c)
    }, false)
  },

  // returns missing capabilities (<string>)
  missing (condition) {
    if (!condition) return ''

    condition = [].concat(condition).join(' ')

    return _(condition.match(/!?[a-z_:-]+/ig))
      .filter(function (c) {
        return !that.has(c)
      })
      .join(' ')
  },

  // bypass for convenience
  has (string) {
    if (!string) return true
    // check cache
    if (string in capabilityCache) return capabilityCache[string]
    // lookup
    return (capabilityCache[string] = capabilities.has(string))
  },

  // checks if something should be visible depending on required capabilities
  // true if any item matches requires capabilities
  // true if any item does not match its requirements but is enabled for upsell
  // this function is used for any inline link, for example, to decide whether or not showing it
  visible: (function () {
    function isEnabled (cap) {
      if (!_.isString(cap) || capabilities.has('guest')) return false

      return !!enabled[cap]
    }

    function isEnabledOrHas (cap) {
      let condition = cap.replace(/([^&|]) ([^&|])/gi, '$1 && $2').replace(capabilities.allowlistRegex, '')
      if (ox.debug && /,/.test(condition)) return !!console.error('You can\'t use a comma in a condition use space instead.')
      condition = condition.replace(/[\w:-]+/ig, function (match) {
        match = match.toLowerCase()
        return isEnabled(match) || that.has(match)
      })
      try {
        /* eslint no-new-func: 0 */
        return new Function('return !!(' + condition + ')')()
      } catch (e) {
        console.error('upsell.visible()', cap, e)
        return false
      }
    }

    return function (array) {
      const list = _.compact([].concat(array))
      return !list.length || _.reduce(list, function (memo, capability) {
        // consider edge cases here
        // for example 'active_sync clientonboarding' with cap['active_sync'] = false and cap['client-onboarding'] = true
        // and upsell['active_sync'] = true but upsell['client-onboarding'] = false should return true
        return memo || isEnabledOrHas(capability)
      }, false)
    }
  })(),

  // checks if upsell is enabled for a set of capabilities
  // true if at least one set matches
  enabled: (function () {
    // checks if upsell is enabled for a single capability
    function isEnabled (capability) {
      if (!_.isString(capability)) return false

      return !!enabled[capability]
    }

    return function () {
      // example: 'a', 'b || c' -> 'a || b || c'
      // example: 'a b && c' -> 'a && b && c'
      // example: 'a,b' -> invalid
      let condition = _(arguments).flatten().join(' || ').replace(/([^&|]) ([^&|])/gi, '$1 && $2').replace(capabilities.allowlistRegex, '')
      if (ox.debug && /,/.test(condition)) return !!console.error('You can\'t use a comma in a condition use space instead.')
      condition = condition.replace(/[a-z0-9_:-]+/ig, function (match) {
        match = match.toLowerCase()
        return isEnabled(match)
      })
      try {
        return new Function('return !!(' + condition + ')')()
      } catch (e) {
        console.error('upsell.enabled()', condition, e)
        return false
      }
    }
  }()),

  captureRequiresUpgrade () {
    ox.on('upsell:requires-upgrade', async (options) => {
      const { showPlans } = await import('@/pe/upsell/plans')
      showPlans(options)
    })
    that.captureRequiresUpgrade = $.noop
  },

  captureUpgrade () {
    ox.on('upsell:upgrade', upgrade)
    that.captureUpgrade = $.noop
  },

  useDefaults () {
    that.captureRequiresUpgrade()
    that.captureUpgrade()
  },

  // helpful if something goes wrong
  debug () {
    console.debug('enabled', enabled, 'capabilityCache', capabilityCache, 'enabledCache', enabledCache)
  },

  // just for demo purposes
  // flag helps during development of custom upsell wizard; just disables some capabilities but
  // neither registers events nor adds portal plugin
  demo (debugCustomWizard) {
    // all features enabled for upsell
    enabled = {
      portal: true,
      calendar: true,
      webmail: true,
      contacts: true,
      infostore: true,
      carddav: true,
      guard: true,
      'ai-service': true
    }
    // disable some capabilities to show triggers
    capabilityCache = _.extend({
      infostore: false,
      calendar: false,
      active_sync: false,
      caldav: false,
      'active_sync || caldav || carddav': false,
      'active_sync || caldav || carddav || extended_quota': false,
      'guard infostore': false,
      'ai-service': false
    }, capabilityCache)

    coreSettings.set('upsell/activated', true)
    // settings.set('features/upsell/secondary-launcher', { icon: 'bi/stars.svg', color: 'var(--upsell)' }) // upgrade button
    coreSettings.set('features/upsell/portal-widget', { enabled: true, requires: 'infostore' })
    coreSettings.set('features/upsell/upgrade-button', { enabled: true, requires: 'infostore' })
    coreSettings.set('features/upsell/folderview/mail', { enabled: true })
    coreSettings.set('features/upsell/topbar-dropdown', { color: '#ff0000' })
    coreSettings.set('features/upsell/mail-folderview-quota', { demo: true, upsellLimit: 10 * 1024 * 1024 })
    coreSettings.set('features/upsell/drive-folderview-quota', { demo: true, upsellLimit: 10 * 1024 * 1024 })

    coreSettings.set('features/upsell/client.onboarding', { enabled: true })
    coreSettings.set('enabled/ai', true)
    console.debug('Running Upsell Demo')
    if (!debugCustomWizard) {
      that.useDefaults()
    }
  },

  // for development & debugging
  setEnabled (capability) {
    enabled[capability] = true
  }
}

addReadyListener('settings', () => {
  // added to enable upsell demo also via setting instead of hash "demo=upsell"
  const demoMode = coreSettings.get('upsell/demo', false)
  enabled = coreSettings.get('upsell/enabled') || {}
  // killswitch for upsell, needed to enable/disable the feature without chaning the whole config
  // needed a different term than "enabled" as it's already taken
  if (!coreSettings.get('upsell/activated', false)) enabled = {}
  triggerReady('upsell')
  // demo mode
  const hash = _.url.hash('demo') || ''
  if (hash.indexOf('upsell') > -1 || demoMode) that.demo()
})

export default that
