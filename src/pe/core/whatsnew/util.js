import ox from '@/ox'
import capabilities from '@/io.ox/core/capabilities'
import { settings as coreSettings } from '@/io.ox/core/settings'

// this must be maintained manually
// otherwise we always need to load (ever growing) "meta" although we known the latest version
// version MUST BE string because IT IS NOT A NUMBER! (8.10 becomes 8.1 otherwise)
const LATEST_VERSION = '8.34'
const isActive = !capabilities.has('guest')
const autoStart = coreSettings.get('whatsNew/autoStart', true)
const mightHaveUnseenFeatures = compareVersion(getLastSeenVersion(), LATEST_VERSION) > 0

function getLastSeenVersion (fallback) {
  if (!fallback) {
    fallback = coreSettings.get('whatsNew/baselineVersion', '') || LATEST_VERSION
  }
  const version = coreSettings.get('whatsNew/lastSeenVersion', fallback)
  // remove zeros right after the dot (to fix older 8.0011 notation für 8.11)
  return /\.0/.test(version)
    ? Number(version).toFixed(4).replace(/\.0+/, '.')
    : String(version)
}

function compareVersion (a, b) {
  const [aMajor, aMinor] = String(a).split('.', 2)
  const [bMajor, bMinor] = String(b).split('.', 2)
  if (aMajor !== bMajor) return parseInt(bMajor) - parseInt(aMajor)
  return parseInt(bMinor) - parseInt(aMinor)
}

async function getAllFeatures () {
  const { getAllFeatures } = await import('@/pe/core/whatsnew/meta')
  return getAllFeatures()
}

async function getFeaturesForAutoStart () {
  const { getNewFeatures } = await import('@/pe/core/whatsnew/meta')
  return getNewFeatures()
}

// support customized help links
const helpLinks = coreSettings.get('whatsNew/helpLinks', false)
// no custom fallback link? use en_US
if (helpLinks) helpLinks.fallback = helpLinks.fallback || helpLinks.en_US

// returns language specific help url or fallback if configured
function getLink () {
  if (!helpLinks) return false
  return helpLinks[ox.language] || helpLinks.fallback
}

function updateLastSeenVersion () {
  coreSettings.set('whatsNew/lastSeenVersion', LATEST_VERSION).save()
}

export default {
  latestVersion: LATEST_VERSION,
  isActive,
  autoStart: isActive && autoStart && mightHaveUnseenFeatures,
  showMenuItem: isActive && coreSettings.get('whatsNew/menuEntry', true),
  mightHaveFeatures: isActive && mightHaveUnseenFeatures,
  compareVersion,
  getLastSeenVersion,
  getAllFeatures,
  getFeaturesForAutoStart,
  getLink,
  updateLastSeenVersion
}
