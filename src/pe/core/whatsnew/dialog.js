import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import util from '@/pe/core/whatsnew/util'
import ModalDialog from '@/io.ox/backbone/views/modal'
import { userCanToggleFeature, getTogglePath, toggleSettings } from '@/io.ox/core/feature'
import * as settingsUtil from '@/io.ox/core/settings/util'
import gt from 'gettext'
import openSettings from '@/io.ox/settings/util'
import { invoke } from '@/io.ox/backbone/views/actions/util'
import { getSetting } from '@/io.ox/settings/index'
import { createButton } from '@/io.ox/core/components'

// important to load this before using gt
import '@/io.ox/settings/strings'
import '@/pe/core/whatsnew/style.scss'

export async function showDialog ({ features, autoStart = false }) {
  return new Promise((resolve) => {
    const dialog = new ModalDialog({
      autoClose: false,
      // focus "Close" button to avoid scrolling to feature checkboxes
      focus: '[data-action="cancel"]',
      point: 'tours/whatsnew/dialog',
      width: '40rem'
    })
      .inject({
        renderIllustration (illustration) {
          if (!illustration) return $()
          // support custom illustrations
          if (typeof illustration === 'function') return illustration()
          return $('<img alt="" loading="lazy">')
            .attr('src', illustration)
        },
        renderFeatureToggle (feature) {
          if (!feature) return $()
          if (!userCanToggleFeature(feature)) return $()
          return settingsUtil.checkbox(getTogglePath(feature), gt('Enable this feature'), toggleSettings).addClass('my-8 select-none')
        },
        renderFeaturelist () {
          const groupByVersion = !autoStart
          let lastReleaseMonth
          this.$ul.empty().append(
            features.map((feature, index) => {
              let $el = $()
              if (groupByVersion && feature.month !== lastReleaseMonth) {
                $el = $el.add(
                  $(`<li class="pt-${index > 0 ? 32 : 16} pb-32">`).append(
                    $('<h2 class="m-0 text-xl font-bold border-bottom feature-padding-start feature-padding-end">')
                      .text(feature.month)
                  )
                )
                lastReleaseMonth = feature.month
              }
              return $el.add(this.renderFeature(feature))
            })
          )
        },
        renderSettingsPath ({ text, id }) {
          const { page, selector = ',', section } = getSetting(id)
          const [folder, cssSelector] = selector.split(' ')
          let settingsPath = joinSettingsPath([gt('Settings'), page, section])
          // make settingsPath clickable if not autostarted
          if (!autoStart) settingsPath = $('<a href="#">').text(settingsPath)[0].outerHTML

          const $el = $('<p class="mb-8 italic">')
            .append(_.noI18n.format(text, settingsPath))
            .on('click', 'a', e => {
              e.preventDefault()
              openSettings(`virtual/settings/${folder}`, null, cssSelector)
            })
          return $el
        },

        renderActionLink ({ text, id }) {
          return $('<p class="mb-8 italic">')
            .append(
              createButton({ text, className: 'btn btn-unstyled btn-link', variant: 'none' })
                .on('click', e => {
                  e.preventDefault()
                  dialog.close()
                  invoke(id)
                })
            )
        },
        renderFeature ({ title, description, illustration, annotation, setting, feature, showFeatureToggle, tag, action } = {}) {
          return $('<li class="mb-32 flex-row">').append(
            $('<div class="feature-illustration select-none">').append(
              this.renderIllustration(illustration)
            ),
            $('<div class="flex-grow text-sm feature-padding-end">').append(
              $('<div class="mb-4 text-base">').append(
                $('<h3 class="m-0 text-base font-bold inline me-8">').text(title),
                $.txt(' '),
                tag ? $('<span class="label label-subtle subtle-accent">').text(tag) : $()
              ),
              description ? this.renderDescription(description) : $(),
              annotation ? $('<p class="mb-8 italic">').text(annotation) : $(),
              setting ? this.renderSettingsPath(setting) : $(),
              action ? this.renderActionLink(action) : $(),
              showFeatureToggle ? this.renderFeatureToggle(feature) : $()
            )
          )
        },
        renderDescription (description) {
          if (_.isArray(description)) return $('<ul class="list-disc p-0 pl-16 pr-32">').append(description.filter(Boolean).map(el => $('<li>').text(el)))
          return $('<p class="mb-8 leading-5">').text(description)
        }
      })
      .extend({
        scaffold () {
          this.$header.css({
            backgroundImage: 'url("./whatsnew/8.0/dialog_banner@2x.png")'
          })
          const titleId = _.uniqueId('title')
          this.$el.addClass('whats-new-dialog').attr('aria-labelledby', titleId)
          const $h1 = $('<h1 class="m-0 font-bold text-2xl feature-padding-start feature-padding-end">').attr('id', titleId)
          if (autoStart) {
            this.$body.append(
              // #. %1%s is the product name, e.g. App Suite
              $h1.addClass('mb-32').text(gt('What\'s new in %1$s', ox.serverConfig.productName))
            )
          } else {
            this.$body.append(
              // #. updates of software. the term "Updates" might not need a translation in some languages, e.g. German
              $h1.addClass('mb-8').text(gt.pgettext('software', 'Updates')),
              $('<h2 class="m-0 mb-32 font-normal text-sm feature-padding-start feature-padding-end">')
                // #. %1%s is the product name, e.g. App Suite
                .text(gt('Stay informed about what\'s new and discover the latest features and enhancements in our latest updates of %1$s.', ox.serverConfig.productName))
            )
          }
          this.$ul = $('<ul class="list-unstyled">')
          this.$body.addClass('select-text').append(this.$ul)
          this.renderFeaturelist()
        }
      })
      .addButton()
      .on('close', function () {
        if (autoStart) util.updateLastSeenVersion()
        resolve()
      })

    if (util.getLink()) {
      dialog
        .addButton({ className: 'btn-default', placement: 'left', action: 'info', label: gt('Learn more') })
        .on('info', function () {
          window.open(util.getLink())
        })
    }

    dialog.open()
  })
}

// create rtl save path string from path array
export function joinSettingsPath (path) {
  if (path.length < 2) return path
  // #. Generate translated representation of a path, e.g. "Settings > Mail > Mark as read"
  // #. %1$s - first component of the path, e.g. "Settings"
  // #. %2$s - constructed rest of the path, e.g. "Mail > Mark as read" or just a single component
  return gt('%1$s > %2$s', path.shift(), joinSettingsPath(path))
}
