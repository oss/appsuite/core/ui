import $ from '@/jquery'
import _ from '@/underscore'
import ext from '@/io.ox/core/extensions'
import Stage from '@/io.ox/core/extPatterns/stage'
import util from '@/pe/core/whatsnew/util'
import gt from 'gettext'

// dropdown link
if (util.showMenuItem) {
  // #. updates of software. the term "Updates" might not need a translation in some languages, e.g. German
  const label = gt.pgettext('software', 'Updates')
  const onClick = (e) => {
    e.preventDefault()
    import('@/pe/core/whatsnew/dialog').then(({ showDialog }) => {
      util.getAllFeatures().then(features => showDialog({ features }))
    })
  }
  // launcher on smartphones help menu on desktop
  if (_.device('smartphone')) {
    ext.point('io.ox/core/appcontrol/sideview').extend({
      id: 'updates',
      index: 200,
      draw () {
        this.$el.append(this.createMenuButton({
          device: 'smartphone',
          name: 'updates',
          text: gt('Updates'),
          icon: 'megaphone',
          action: onClick
        }))
      }
    })
  } else {
    ext.point('io.ox/core/appcontrol/right/help').extend({
      id: 'updates',
      after: 'divider-first',
      extend () {
        this.append(
          $('<a href="#" data-action="updates" role="menuitem">').text(label).on('click', onClick)
        )
      }
    })
  }
}

// autostart if user has not seen the features of this version yet
if (util.autoStart) {
  // eslint-disable-next-line no-new
  new Stage('io.ox/core/stages', {
    id: 'whatsnewdialog',
    // just keep the index before the tours, see OXUIB-665
    index: 950,
    run (baton) {
      return util.getFeaturesForAutoStart().then(features => {
        if (!features.length) return
        baton.data.popups.push({ name: 'whats-new-dialog' })
        return import('@/pe/core/whatsnew/dialog').then(({ showDialog }) => {
          return showDialog({ features, autoStart: true })
        })
      })
    }
  })
}
