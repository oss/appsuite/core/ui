// cSpell:ignore Regelmäßige
import _ from '@/underscore'
import moment from '@/moment'
import ox from '@/ox'
import util from '@/pe/core/whatsnew/util'
import capabilities from '@/io.ox/core/capabilities'
import ext from '@/io.ox/core/extensions'
import { hasFeature } from '@/io.ox/core/feature'

import gt from 'gettext'

const dates = {

  8.34: '2025-01-17',
  8.33: '2024-12-19',
  8.32: '2024-11-22',
  8.31: '2024-10-25',
  '8.30': '2024-09-27',
  8.29: '2024-09-06',
  8.28: '2024-08-02',
  8.27: '2024-07-04',
  8.26: '2024-06-06',
  8.25: '2024-05-03',
  8.24: '2024-04-05',
  8.23: '2024-03-08',
  8.22: '2024-02-08',
  '8.20': '2023-12-01',
  8.19: '2023-10-27',
  8.18: '2023-09-29',
  8.16: '2023-08-04',
  8.15: '2023-07-07',
  8.14: '2023-06-09',
  8.13: '2023-05-05',
  8.12: '2023-04-04',
  8.11: '2023-03-10',
  '8.10': '2023-02-08',
  8.8: '2023-01-11',
  8.6: '2022-11-09',
  8.4: '2022-09-01',
  8.3: '2022-07-20',
  '8.0': '2022-03-25'

}

// consistent default title and tag
// #. Appears in "Whats New" or "Updates" dialogs
// #. A generic term for regular updates (like minor or regular improvements)
// #. should have a positive spin in other languages (e.g. "Regelmäßige Verbesserungen" in German)
const title = gt('Continuous improvements')
const tag = gt('Usability')
const illustration = 'generic/lightbulb.svg'

let features = [
  // 8.33
  {
    version: '8.33',
    priority: 1,
    check: () => hasFeature('undo'),
    feature: 'undo',
    title: gt('Undo mail operations'),
    description: gt('Easily undo actions like delete, archive, move or copy with a single click – no stress, no hassle, just instant control.'),
    illustration: './whatsnew/8.33/ic_undo_mail.svg',
    // added with 8.34
    setting: { id: 'UNDO', text: gt('Set your preferred duration here %1$s') },
    tag
  },
  // 8.30
  {
    version: '8.30',
    priority: 1,
    check: () => hasFeature('connectYourDevice'),
    feature: 'qrSessionHandover',
    // #. %1$s is the product name
    title: gt('Connect your device to your account in seconds'),
    // #. %1$s is the product name
    description: gt('Simply scan the QR code and your device will connect seamlessly - no need to manually enter login credentials or go through lengthy setup processes.'),
    illustration: './whatsnew/8.30/ic_qr_code.svg',
    action: { text: gt('Connect your device'), id: 'io.ox/onboarding/whatsnew' },
    tag
  },
  // 8.29
  {
    version: '8.29',
    priority: 1,
    feature: 'pwaInstructionsWizard',
    // #. %1$s is the product name
    title: gt('Install %1$s like an app', ox.serverConfig.productName),
    // #. %1$s is the product name
    description: gt('You can now install %1$s like an app for quick and easy access! A new wizard will walk you through all the necessary steps on your device. Please note that this feature is only available if your browser supports Progressive Web Apps (PWAs) - currently some browsers do not support PWA installation.', ox.serverConfig.productName),
    illustration: './whatsnew/8.29/ic_pwa.svg',
    action: _.device('!standalone') ? { text: gt('Open installation instructions'), id: 'io.ox/wizards/pwa-install-instructions' } : undefined,
    tag
  },
  // 8.28
  {
    version: '8.28',
    priority: 1,
    device: '!smartphone',
    feature: 'firstStartWizard',
    title: gt('First start wizard'),
    description: gt('Guides new users through the initial setup, ensuring a seamless onboarding experience.'),
    illustration: './whatsnew/8.28/ic_start_wizard.svg',
    tag
  },
  // 8.27
  {
    version: '8.27',
    priority: 1,
    device: '!smartphone',
    title: gt('Custom font size when reading unformatted mails'),
    // #. Product name will be "OX App Suite" or similar.
    description: gt('Enhance your email reading experience. %1$s now allows you to set custom font sizes for most emails, giving you more control and comfort.', ox.serverConfig.productName),
    illustration: './whatsnew/8.27/ic_font_size.svg',
    setting: { id: 'FONT_SIZE', text: gt('Font size can be configured in %1$s') },
    tag
  },
  // 8.24
  {
    version: '8.24',
    priority: 1,
    feature: 'categories',
    title: gt('Mobile calendar redesign'),
    // #. Product name will be "OX App Suite" or similar.
    description: gt('This version of %1$s introduces a fresh calendar layout and enhanced usability for mobile users. Navigate your week with ease through our new, dynamic week view. The updated month view displays additional details, optimizing your daily routine.', ox.serverConfig.productName),
    illustration: './whatsnew/8.11/ic_mobile_device.svg',
    tag: gt('Mobile')
  },
  {
    version: '8.24',
    priority: 1,
    feature: 'categories',
    title: gt('Category browser'),
    description: gt("Browse in categories: In our latest update, we're excited to unveil the new category browser. This innovative feature allows users to effortlessly search and navigate through items tagged under any category in a single place, regardless of their type - be it emails, contacts, appointments, or tasks."),
    illustration: './whatsnew/8.24/ic_browse_categories.svg',
    tag
  },
  // 8.23
  {
    version: '8.23',
    priority: 2,
    capabilities: 'calendar',
    title: gt('Year view in mobile calendar'),
    description: gt('Previously exclusive to desktop, the year view is now also available when using your mobile device!'),
    illustration: './whatsnew/8.11/ic_mobile_device.svg',
    tag: gt('Mobile')
  },
  // 8.22
  {
    version: '8.22',
    priority: 1,
    device: '!smartphone',
    capabilities: 'webmail',
    feature: 'ai',
    title: gt('GPT/AI integration'),
    description: gt('Have you heard about ChatGPT? With our brand-new AI integration, you can now take advantage of its exceptional abilities to read and respond to emails faster, more efficiently, and with more finesse. Say goodbye to the hassle of drafting emails, and let GPT make your life easier.'),
    illustration: './whatsnew/8.12/ic_ai.svg',
    tag
  },
  {
    version: '8.22',
    priority: 1,
    capabilities: 'calendar',
    feature: 'forwardInvitation',
    title: gt('Forward meeting invitations'),
    description: gt('You can now easily forward meeting invitations to others, facilitating seamless sharing and coordination of appointments.'),
    tag: gt('Calendar'),
    illustration: './whatsnew/8.22/ic_forward_meeting.svg'
  },
  // 8.20
  {
    version: '8.20',
    priority: 1,
    title: gt('Big mobile UI makeover'),
    description: !hasFeature('pwa')
      ? gt('This release brings a completely new look and feel to your mobile device, including a faster navigation for switching apps, a redesigned mail app, updated compose windows, a new launcher design, and many more.')
      : gt('This release brings a completely new look and feel to your mobile device, including a faster navigation for switching apps, a redesigned mail app, updated compose windows, a new launcher design, and many more. You can now also install %1$s on your device like an App. On Android using Chrome, tap the three dots in the top right, and select "Install app". On iOS, tap the share icon at the bottom, and choose "Add to Home Screen" to install.', ox.serverConfig.productName),
    illustration: './whatsnew/8.11/ic_mobile_device.svg',
    tag: gt('Mobile')
  },
  {
    version: '8.20',
    priority: 2,
    title: gt('Scheduled email sending'),
    description: gt("We're excited to announce that you can now schedule emails to be sent on a specific future date. This feature makes planning and sending emails at your convenience incredibly easy."),
    illustration: './whatsnew/8.20/ic_scheduled_mail.svg',
    feature: 'scheduleSend',
    tag: gt('Mail')
  },
  {
    version: '8.20',
    priority: 3,
    title: gt('Categories for emails'),
    description: gt("We've expanded category support! While previously limited to appointments, contacts, and tasks, you can now assign categories to your emails as well."),
    illustration: './whatsnew/8.20/ic_mail_category.svg',
    feature: 'categories',
    tag: gt('Mail')
  },
  // 8.19
  {
    version: '8.19',
    priority: 2,
    title: gt('Browser back/forward navigation'),
    description: gt("With this feature, you can now seamlessly switch between apps using your browser's back and forward functions, providing a level of convenience and flexibility that was not previously available."),
    illustration: './whatsnew/8.19/ic_browser_back_forward.svg',
    feature: 'navigation',
    tag
  },

  // 8.18
  {
    version: '8.18',
    priority: 2,
    title: gt('Keyboard Shortcuts'),
    description: gt('Introducing keyboard shortcuts for a more efficient experience. With selectable presets, easily switch between Gmail and Outlook styles, streamlining your navigation and actions.'),
    illustration: './whatsnew/8.18/ic_keyboard_shortcuts.svg',
    feature: 'shortcuts',
    setting: { id: 'SHORTCUT_PROFILE', text: gt('Shortcuts can be configured in %1$s') },
    tag
  },

  // 8.16
  {
    version: '8.16',
    priority: 2,
    title: gt('Big settings makeover'),
    description: gt('The search section has been cleaned up and made easier to navigate, with the most important settings now prioritized and placed upfront. We\'ve also improved the wording throughout the interface to ensure better understandability.'),
    illustration: './whatsnew/8.16/ic_settings.svg',
    tag
  },
  // 8.15
  {
    version: '8.15',
    priority: 2,
    title: gt('Add attachments from another email'),
    description: gt('You can now easily reuse attachments from your previous emails! %s automatically remembers your recent attachments and makes them easily accessible when you\'re composing a new message. Say goodbye to the hassle of downloading and reuploading files.', ox.serverConfig.productName),
    illustration: './whatsnew/8.15/ic_attach_from_email.svg',
    tag: gt('Mail')
  },
  {
    version: '8.15',
    priority: 3,
    title: gt('Text templates'),
    description: gt('Text templates: A powerful tool for creating and editing pre-defined snippets that enable quick responses and visually captivating content. Seamlessly insert plain or formatted text, including images, into new emails with ease, boosting productivity and efficiency.'),
    setting: { id: 'TEMPLATES', text: gt('You can manage the templates in %1$s') },
    illustration: './whatsnew/8.15/ic_text_snippets.svg',
    feature: 'templates',
    tag: gt('Mail')
  },
  // 8.14
  {
    version: '8.14',
    priority: 3,
    title: gt('Reply to'),
    description: gt('When you respond to an email, you typically reply to the person who sent it. However, there are situations where it\'s preferred that the recipients of the response are different, such as a whole team or group. In these cases, the original sender can specify one or even multiple email addresses in the "reply-to" field, indicating who the response should be directed to. Additionally, if the same group of people often needs to receive the responses, default "reply-to" addresses can be set to simplify the process.'),
    setting: { id: 'YOUR_ACCOUNTS', text: gt('You can set the default addresses in %1$s') },
    illustration: './whatsnew/8.14/ic_reply_to.svg',
    tag: gt('Mail')
  },
  // 8.13
  {
    version: '8.13',
    priority: 2,
    title: gt('Undo Send'),
    description: gt('„Undo send“ is a new feature that gives you the ability to cancel sending an email shortly after you\'ve clicked the "send" button. This feature is super handy if you\'ve made a mistake, such as forgetting to attach a file or sending an email to the wrong recipient or simply hit „send“ by accident.'),
    setting: { id: 'UNDO_SEND', text: gt('You can adjust the time to undo sending an email in %1$s') },
    illustration: './whatsnew/8.13/ic_undo_send.svg',
    feature: 'undoSend',
    tag: gt('Mail')
  },
  {
    version: '8.13',
    priority: 2,
    title: gt('Managed Resources'),
    description: gt('Managed resources is a feature that allows organizations to efficiently manage and schedule shared resources such as conference rooms and equipment. This feature helps ensure that resources are utilized effectively and efficiently, improving productivity and reducing scheduling conflicts.'),
    illustration: './whatsnew/8.13/ic_managed_resources.svg',
    tag: gt('Calendar')
  },
  // 8.11
  {
    version: '8.11',
    priority: 2,
    capabilities: 'webmail',
    title: gt('Mark as read'),
    description: gt('It is now possible to set how quickly an email will be marked as read. You can choose to have it marked as read immediately, after 5 seconds, after 20 seconds, or never.'),
    setting: { id: 'MARK_READ', text: gt('You can find these options in %1$s') },
    illustration: './whatsnew/8.11/ic_mark_read.svg',
    tag: gt('Mail')
  },
  {
    version: '8.11',
    priority: 2,
    device: '!smartphone',
    title: gt('Never miss a meeting'),
    description: gt('If you have a busy day with many meetings, it is easy to overlook a meeting. To help you stay on top of your schedule, we have added a countdown that reminds you of your upcoming meetings with color, sound, and desktop notifications.'),
    setting: { id: 'COUNTDOWN', text: gt('You can adjust the countdown in %1$s') },
    illustration: './whatsnew/8.11/ic_countdown.svg',
    feature: 'countdown',
    showFeatureToggle: true,
    tag: gt('Calendar')
  },
  {
    version: '8.11',
    priority: 2,
    capabilities: 'calendar',
    title: gt('Mark participants as optional'),
    description: gt('Especially when organizing larger meetings, it is helpful to be able to designate certain participants as optional, as not everyone\'s attendance might be required.'),
    illustration: './whatsnew/8.11/ic_optional_participants.svg',
    tag: gt('Calendar')
  },
  {
    version: '8.11',
    priority: 3,
    capabilities: 'calendar',
    title: gt('Smarter participant sorting'),
    description: gt('The order of participants has been adjusted to better suit different use cases: When viewing appointment details, participants are now ordered by their participation status, prioritizing those who have accepted or declined. In contrast, when creating or editing an appointment, participants are sorted alphabetically by name for easier reference.'),
    illustration: './whatsnew/8.11/ic_sort_participants.svg',
    tag: gt('Calendar')
  },
  {
    version: '8.11',
    priority: _.device('smartphone') ? 1 : 3,
    capabilities: 'webmail',
    device: undefined, // desktop users are informed about the mobile development as well
    title: gt('Expanded mobile feature set'),
    description: gt('Our mobile feature set just got an upgrade! Now, you can enjoy even more features on your mobile device that were once exclusive to desktops. Say hello to customizable mail signatures, email formatting, reminders, move and archive functions, and even printing - all available right at your fingertips.'),
    illustration: './whatsnew/8.11/ic_mobile_device.svg',
    tag: gt('Mobile')
  },
  // 8.10
  {
    version: '8.10',
    priority: 4,
    title,
    description: [
      gt('The user interface is now more resilient in handling unstable internet connections'),
      capabilities.has('calendar') && gt('Appointment details now have an indicator when the appointment is about to begin'),
      capabilities.has('calendar') && gt('Appointment participants are now sorted by their confirmation status'),
      hasFeature('connectYourDevice') && gt('The "Connect your device" wizard now allows you to setup email, contacts, and calendar synchronization with your iPhone, iPad, or Macbook in one step'),
      capabilities.has('mail_categories') && gt('Inbox categories now have a cleaner appearance')
    ],
    illustration,
    tag
  },
  // 8.8
  {
    version: '8.8',
    priority: 4,
    title,
    description: [
      // #. %1$s: name of 'drive' app
      hasFeature('attachFromDrive') && gt('You can now attach files from %1$s directly to appointments, contacts, or tasks', gt.pgettext('app', 'Drive')),
      gt('The loading behavior has been improved to achieve faster loading times')
    ],
    illustration,
    tag
  },
  // 8.6
  {
    version: '8.6',
    priority: 4,
    title,
    description: [
      capabilities.has('webmail') && gt('You can now set a default signature for alias addresses as well'),
      gt('The majority of app icons have been visually updated')
    ],
    illustration,
    tag
  },
  // 8.4
  {
    version: '8.4',
    priority: 1,
    title: gt('New notification center'),
    // #. The following sentences appear under the title "New notification center" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('Our newly designed notification center keeps you always up to date. Find all your appointments, reminders and birthdays at a glance.'),
    illustration: './whatsnew/8.4/ic_notifications.svg',
    tag
  },
  {
    version: '8.4',
    priority: 2,
    title: gt('Settings got a new search'),
    // #. The following sentences appear under the title "Settings got a new search" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('We have given the settings a new look and added a search function. Now you can find your way around the settings even faster.'),
    illustration: './whatsnew/8.4/ic_search_settings.svg',
    tag
  },
  {
    version: '8.4',
    priority: 3,
    device: '!smartphone',
    title: gt('Signatures for all mail accounts'),
    // #. The following sentences appear under the title "Signatures for all mail accounts" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('If you use multiple email accounts, you can now assign individual signatures to each account.'),
    illustration: './whatsnew/8.4/ic_signature.svg',
    tag: gt('Mail')
  },
  // 8.3
  {
    version: '8.3',
    priority: 4,
    title,
    description: [
      gt('Dark mode is now also available on mobile devices')
    ],
    illustration
  },
  // 8.0
  {
    version: '8.0',
    priority: 1,
    title: gt('New design'),
    // #. The following sentences appear under the title "New design" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('We are very happy to introduce you to our brand-new user interface. We changed countless little details to offer you a great and contemporary user experience.'),
    illustration: './whatsnew/8.0/ic_ui_refresh.svg',
    tag
  },
  {
    version: '8.0',
    priority: 2,
    device: '!smartphone',
    title: gt('Dark mode'),
    // #. The following sentences appear under the title "Dark mode" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('Let there be darkness! You can now select Dark Mode which uses bright text on a dark background as opposed to the usual black text on a bright white screen. Apart from personal preference, Dark Mode has significant benefits. A device or application using Dark Mode emits less light which improves readability in low light environments thus reducing eye strain.'),
    illustration: './whatsnew/8.0/ic_darkmode.svg',
    tag
  },
  {
    version: '8.0',
    priority: 2,
    device: '!smartphone',
    title: gt('Themes'),
    // #. The following sentences appear under the title "Themes" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('More colors! You like purple? Prefer orange? You now have the choice! We added a set of themes that covers the most common color preferences. And you can combine different background and accent colors.'),
    illustration: './whatsnew/8.0/ic_theme.svg',
    tag
  },
  {
    version: '8.0',
    priority: 2,
    title: gt('New search'),
    // #. The following sentences appear under the title "New search" in the "Whats New" dialog.
    // #. It is a continuous text with illustrations aside.
    description: gt('Searching is one of the most used functions. We have simplified and improved the search feature. It is now located in the most prominent position: upper, top central navigation bar. It\'s a simple and easy-to-use text field and more sophisticated filters are available in a separate dropdown.'),
    illustration: './whatsnew/8.0/ic_search.svg',
    tag
  }
]

// example for configuration
/* var links = {
        fallback: 'https://www.open-xchange.com',
        ca_ES: 'https://www.open-xchange.com',
        cs_CZ: 'https://www.open-xchange.com',
        da_DK: 'https://www.open-xchange.com',
        de_DE: 'https://www.open-xchange.com',
        en_GB: 'https://www.open-xchange.com',
        en_US: 'https://www.open-xchange.com',
        es_ES: 'https://www.open-xchange.com',
        es_MX: 'https://www.open-xchange.com',
        fi_FI: 'https://www.open-xchange.com',
        fr_CA: 'https://www.open-xchange.com',
        fr_FR: 'https://www.open-xchange.com',
        hu_HU: 'https://www.open-xchange.com',
        it_IT: 'https://www.open-xchange.com',
        ja_JP: 'https://www.open-xchange.com',
        lv_LV: 'https://www.open-xchange.com',
        nb_NO: 'https://www.open-xchange.com',
        nl_NL: 'https://www.open-xchange.com',
        pl_PL: 'https://www.open-xchange.com',
        pt_BR: 'https://www.open-xchange.com',
        ro_RO: 'https://www.open-xchange.com',
        ru_RU: 'https://www.open-xchange.com',
        sk_SK: 'https://www.open-xchange.com',
        sv_SE: 'https://www.open-xchange.com',
        tr_TR: 'https://www.open-xchange.com',
        zh_CN: 'https://www.open-xchange.com',
        zh_TW: 'https://www.open-xchange.com'
    };
    */

// allow customization of the feature list, be careful to use the same versioning as the original list or we will get bugs
const customfeatures = ext.point('pe/core/whatsnew/dialog/featurelist').invoke('customize', null, features).valueOf()

if (customfeatures.length > 0) {
  features = _(customfeatures).flatten()
}

// get features based on the last seen version of the dialog
// list can be provided just for testing purposes
export function getNewFeatures ({ list = features, limit = 7, sinceVersion } = {}) {
  const lastSeenVersion = sinceVersion ?? util.getLastSeenVersion()
  return Object.values(list)
    .map(injectReleaseInformation)
    .filter(({ capabilities: cap, check, device, feature, version, priority }) => {
      // high priority only
      if (priority > 3) return false
      // new features only
      if (util.compareVersion(version, lastSeenVersion) >= 0) return false
      return filter({ cap, check, device, feature })
    })
    .sort((a, b) => {
      // sort by PRIORITY, then VERSION
      if (a.priority !== b.priority) return a.priority - b.priority
      return util.compareVersion(a.version, b.version)
    })
    .slice(0, limit)
}

// list can be provided just for testing purposes
export function getAllFeatures (list = features) {
  return Object.values(list)
    .map(injectReleaseInformation)
    .filter(({ capabilities: cap, check, device, feature }) => {
      return filter({ cap, check, device, feature })
    })
    .sort((a, b) => {
      // sort by RELEASE DATE, then PRIORITY
      if (a.date === b.date) return a.priority - b.priority
      return b.date.localeCompare(a.date)
    })
}

function filter ({ cap, device, feature, check }) {
  if (cap && !capabilities.has(cap)) return false
  if (device && !_.device(device)) return false
  if (feature && !hasFeature(feature)) return false
  if (check && !check()) return false
  return true
}

function injectReleaseInformation (feature) {
  feature.priority = feature.priority ?? 3
  if (feature.date) return feature
  // missing date defaults to 8.0
  const date = dates[feature.version] || '2022-03-25'
  if (date) {
    feature.date = moment(date).startOf('month').format('YYYY-MM-DD')
    feature.month = moment(date).format('MMMM YYYY')
  } else if (ox.debug) {
    console.debug('whatsnew/meta.js: Every release MUST refer to an existing release or define "date" and "month".')
  }
  return feature
}
