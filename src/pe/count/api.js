import _ from '$/underscore'
import Backbone from '$/backbone'

// expose dummy api
const api = _.extend({ queue: [], add: _.noop }, Backbone.Events)

export default api
