import $ from '@/jquery'
import Backbone from '@/backbone'
import moment from '@/moment'

import DisposableView from '@/io.ox/backbone/views/disposable'

import ModalDialog from '@/io.ox/backbone/views/modal'
import ext from '@/io.ox/core/extensions'
import mini from '@/io.ox/backbone/mini-views/common'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import api from '@/pe/settings/personalData/api'
import yell from '@/io.ox/core/yell'
import strings from '@/io.ox/core/strings'
import '@/pe/settings/personalData/settings/style.scss'
import { createIcon } from '@/io.ox/core/components'
import { addReadyListener } from '@/io.ox/core/events'
import * as util from '@/io.ox/core/settings/util'
import { st } from '@/io.ox/settings/index'

import gt from 'gettext'

// same structure as api response
const modules = {
  mail: {
    label: gt('Email'),
    description: gt('Includes all emails from your primary mail account as eml files.'),
    includeTrash: {
      // #. header for a dropdown
      header: gt('Included folders'),
      // #. shown when a download of mail data is requested (has header "Included folders ...")
      label: gt('Trash folder')
    },
    includePublic: {
      // #. shown when a download of mail data is requested (has header "Included folders ...")
      label: gt('Public folders')
    },
    includeShared: {
      // #. shown when a download of mail data is requested (has header "Included folders ...")
      label: gt('Shared folders')
    },
    includeUnsubscribed: {
      // #. shown when a download of mail data is requested (has header "Included folders ...")
      label: gt('Unsubscribed folders')
    }
  },
  calendar: {
    label: gt('Calendar'),
    description: gt('Includes all appointments from your calendars as ical files.'),
    includePublic: {
      // #. header for a dropdown
      header: gt('Included calendars'),
      // #. shown when a download of calendar data is requested (has header "Included calendars ...")
      label: gt('Public calendars')
    },
    includeShared: {
      // #. shown when a download of calendar data is requested (has header "Included calendars ...")
      label: gt('Shared calendars')
    },
    includeUnsubscribed: {
      // #. shown when a download of calendar data is requested (has header "Included calendars ...")
      label: gt('Unsubscribed calendars')
    }
  },
  contacts: {
    label: gt('Address book'),
    description: gt('Includes all contact data from your address books as vcard files.'),
    includePublic: {
      // #. header for a dropdown
      header: gt('Included address books'),
      // #. shown when a download of contact data is requested (has header "Included address books ...")
      label: gt('Public address books')
    },
    includeShared: {
      // #. shown when a download of contact data is requested (has header "Included address books ...")
      label: gt('Shared address books')
    }
  },
  infostore: {
    label: gt.pgettext('app', 'Drive'),
    // #. %1$s is usually "Drive" (product name; might be customized)
    description: gt('Includes all files from %1$s.', gt.pgettext('app', 'Drive')),
    includeTrash: {
      // #. header for a dropdown
      header: gt('Included folders'),
      // #. shown when a download of (cloud) drive files is requested (has header "Included folders ...")
      label: gt('Trash folder')
    },
    includePublic: {
      // #. shown when a download of (cloud) drive files is requested (has header "Included folders ...")
      label: gt('Public folders')
    },
    includeShared: {
      // #. shown when a download of (cloud) drive files is requested (has header "Included folders ...")
      label: gt('Shared folders')
    },
    includeAllVersions: {
      // #. header for a dropdown
      header: gt('Additional options'),
      divider: true,
      // #. shown when a download of (cloud) drive files is requested
      label: gt('Include all file versions')
    }
  },
  tasks: {
    label: gt('Tasks'),
    description: gt('Includes all tasks as ical files.'),
    includePublic: {
      // #. header for a dropdown
      header: gt('Included folders'),
      // #. shown when a download of task data is requested (has header "Included folders ...")
      label: gt('Public folders')
    },
    includeShared: {
      // #. shown when a download of task data is requested (has header "Included folders ...")
      label: gt('Shared folders')
    }
  }
}

const filesizelimits = [
  // 512mb (is hardcoded backend minimum)
  536870912,
  // 1gb
  1073741824,
  // 2gb
  2147483648
]

const ignoredErrors = [
  // data export canceled by user. (why does this error message exist? That's no error)
  'GDPR-EXPORT-0013',
  // no data export or it has already been completed (why does this error message exist? That's also no error)
  'GDPR-EXPORT-0009'
]

function handleApiResult (apiResponse) {
  // check if this is [data, timestamp]
  apiResponse = Array.isArray(apiResponse) ? apiResponse[0] : apiResponse

  // error, failed, aborted. Behavior is the same in all cases, just display view, so user can try again.
  if (apiResponse.error || apiResponse.status === 'FAILED' || apiResponse.status === 'ABORTED') {
    if (!ignoredErrors.includes(apiResponse.code)) {
      yell(apiResponse)
    }
    // in case of error set status to NONE, so user can retry
    apiResponse = { status: 'NONE' }
  }

  return apiResponse
}

function deleteDialog (options) {
  return new Promise((resolve, reject) => {
    new ModalDialog({ title: options.title })
      .build(function () {
        this.$body.append(`<div>${options.text}</div>`)
      })
      .addCancelButton()
      .addButton({ className: 'btn-primary', action: options.action, label: options.label })
      .on('action', resolve)
      .on('cancel', reject)
      .open()
  })
}

// displays a lot of checkboxes to select the data to download
const SelectDataView = DisposableView.extend({
  className: 'personal-data-view settings-section',
  initialize (options) {
    const self = this
    this.status = options.status
    this.status.on('change:status', this.render.bind(this))
    // save this so it isn't overwritten when the selectbox is used
    this.maxSupportedFileSize = self.model.get('maxFileSize')
    // create one model for each submodule
    // makes it easier to use checkbox miniviews later on since data is not nested anymore
    this.models = {}
    Object.keys(modules).forEach(moduleName => {
      if (!self.model.get(moduleName)) return
      self.models[moduleName] = new Backbone.Model(self.model.get(moduleName))
      self.models[moduleName].on('change:enabled', function toggleCheckboxDisabledState (model) {
        self.$el.find(`.${moduleName}-sub-option`).toggleClass('disabled', !model.get('enabled')).find('input').attr('aria-disabled', true).prop('disabled', model.get('enabled') ? '' : 'disabled')
      })
    })
  },
  render () {
    const self = this
    const supportedFilesizes = filesizelimits.filter(value => value <= self.maxSupportedFileSize)

    // data selection
    this.$el.removeClass('disabled').empty().append(
      $('<h2>').text(gt('Request download')),
      $('<p>').text(gt('You can download a copy of your personal data from your account, if you want to save it or transfer it to another provider.'))
    )

    // build Checkboxes
    Object.keys(modules).forEach(moduleName => {
      const data = modules[moduleName]
      if (!self.model.get(moduleName)) return
      const dropdownView = new Dropdown({ caret: true, model: self.models[moduleName], label: gt('Options') })
      self.models[moduleName].on('change:enabled', () => {
        dropdownView.$toggle.attr('aria-disabled', !self.models[moduleName].get('enabled')).toggleClass('disabled', !self.models[moduleName].get('enabled'))
      })

      // sub options as dropdown (include trash folder etc)
      Object.keys(data).forEach(subOption => {
        if (subOption === 'label' || subOption === 'description') return
        if (modules[moduleName][subOption].divider) dropdownView.divider()
        if (modules[moduleName][subOption].header) dropdownView.header(modules[moduleName][subOption].header)
        // yes, include headers and dividers even if the first option is missing
        if (self.model.get(moduleName)[subOption] === undefined) return
        dropdownView.option(subOption, true, modules[moduleName][subOption].label)
      })

      // main checkbox for the module
      // a11y does not like it when multiple nodes have the same name attribute, so despite all having the attribute name in the model ("enabled") we use different name attributes for the nodes
      this.$el.append(
        $('<div>').append(
          new mini.CustomCheckboxView({
            name: 'enabled',
            nodeName: moduleName,
            label: modules[moduleName].label,
            model: self.models[moduleName]
          }).render().$el.addClass('main-option'),
          $('<div class="description">').text(modules[moduleName].description),
          dropdownView.$ul.find('a').length > 0 ? dropdownView.render().$el : ''
        )
      )
    })

    if (supportedFilesizes.length) {
      this.$el.append(
        $('<div class="form-group row mt-24">').append(
          `<div class="col-xs-12">
            <label class="mb-4" for="personaldata-filesizepicker">${gt('Maximum file size')}</label>
            <div class="filepicker-description">${gt('Archives larger than the selected size will be split into multiple files.')}</div>
          </div>`,
          $('<div class="col-xs-12 col-md-6">').append(
            new mini.SelectView({
              name: 'maxFileSize',
              id: 'personaldata-filesizepicker',
              model: self.model,
              list: supportedFilesizes.map(function (fileSize) {
                return { label: strings.fileSize(fileSize), value: fileSize }
              })
            }).render().$el.val(supportedFilesizes[supportedFilesizes.length - 1])
          )
        )
      )
    }

    // display the correct buttons depending on the current download state
    switch (this.status.get('status')) {
      case 'NONE':
        this.$el.append($(`<button type="button" class="btn btn-primary">${gt('Request download')}</button>`)
          .on('click', () => {
            api.requestDownload(self.getDownloadConfig(), true).done(() => yell('success', gt('Download requested'))).fail(yell)
          }))
        break
      case 'PENDING':
      case 'RUNNING':
      case 'PAUSED':
        this.$el.addClass('disabled').find('.checkbox,a').addClass('disabled')
        this.$el.find('input,select').attr('aria-disabled', true).prop('disabled', 'disabled')
        this.$el.append(`<button type="button" class="btn btn-primary" disabled>${gt('Request download')}</button>`)
        break
      case 'DONE':
        this.$el.append($(`<button type="button" class="btn btn-primary request-download">${gt('Request new download')}</button>`)
          .on('click', () => {
            deleteDialog({
              title: gt('Request new download'),
              text: gt('There is currently an archive download available. By requesting a new download the current archive will be deleted and is no longer available.'),
              action: 'delete',
              label: gt('Request new download')
            }).then(action => {
              if (action === 'delete') api.requestDownload(self.getDownloadConfig(), true).done(() => yell('success', gt('Download requested'))).fail(yell)
            })
          }))
        break
      // no default
    }

    return this
  },
  getDownloadConfig () {
    const self = this
    Object.keys(this.models).forEach(moduleName => {
      self.model.set(moduleName, self.models[moduleName].toJSON())
    })

    return this.model.toJSON()
  }
})
// used to display the current state if a download was requested
const DownloadView = DisposableView.extend({
  className: 'personal-data-download-view settings-section',
  initialize () {
    const self = this
    this.listenTo(api, 'updateStatus', () => {
      api.getAvailableDownloads().always(downloadStatus => {
        downloadStatus = handleApiResult(downloadStatus)
        // update attributes
        self.model.set(downloadStatus)
        // remove attributes that are no longer there
        Object.keys(self.model.attributes).forEach(key => {
          if (!Object.keys(downloadStatus).includes(key)) self.model.unset(key)
        })
        self.render()
      })
    })
  },
  render () {
    this.$el.empty().toggle(this.model.get('status') === 'DONE' ? this.model.get('results')?.length > 0 : this.model.get('status') !== 'NONE')

    if (this.model.get('status') === 'PENDING' || this.model.get('status') === 'RUNNING' || this.model.get('status') === 'PAUSED') {
      this.$el.append(
        // #. header for zip archive download list
        $('<h2>').text(gt('Your archive')),
        $('<p class="status">').text(gt('Your requested archive is currently being created. Depending on the size of the requested data this may take hours or days. You will be informed via email when your download is ready.')),
        $(`<button type="button" class="cancel-button btn btn-primary">${gt('Cancel download request')}</button>`)
          .on('click', () => {
            deleteDialog({
              title: gt('Cancel download request'),
              text: gt('Do you really want to cancel the current download request?'),
              action: 'delete',
              label: gt('Cancel download request')
            }).then(action => {
              if (action === 'delete') api.cancelDownloadRequest().fail(yell)
            })
          })
      )
    }

    if (this.model.get('status') === 'DONE' && this.model.get('results')?.length > 0) {
      this.$el.append(
        // #. header for zip archive download list
        $('<h2>').text(gt('Your archive')),
        $('<p class="status">').text(
          // #. %1$s: date when the download was requested
          // #. %2$s: date and time when the download expires
          gt('Your data archive from %1$s is ready for download. The download is available until %2$s.', moment(this.model.get('creationTime')).format('L'), moment(this.model.get('availableUntil')).format('L'))
        ),
        $('<ul class="list-unstyled downloads">').append(
          this.model.get('results').map(file => {
            return $('<li class="file">')
              .append(
                $('<span>').text(file.fileInfo),
                // #. %1$s: filename
                $('<button type="button" class="btn">').attr('title', gt('Download %1$s.', file.fileInfo))
                  .append(createIcon('bi/download.svg'))
                  .on('click', () => {
                    api.downloadFile(file.taskId, file.number)
                  })
              )
          })
        ))
    }

    return this
  }
})

addReadyListener('capabilities:user', (capabilities) => {
  if (!capabilities.has('dataexport')) return

  ext.point('io.ox/settings/personalData/settings/detail').extend({
    id: 'select-view',
    index: 100,
    draw () {
      const $body = $('<div class="settings-body">')
      this.addClass('io-ox-personal-data-settings').append(
        util.header(
          st.GDPR,
          'ox.appsuite.user.sect.dataorganisation.downloadpersonaldata.html'
        ),
        $body
      )
      // no when here, behavior in always callback would not work correctly.
      api.getAvailableModules().then(availableModules => {
        api.getAvailableDownloads().always(downloadStatus => {
          downloadStatus = handleApiResult(downloadStatus)

          if (!(downloadStatus.status === 'NONE' || downloadStatus.status === 'DONE') && downloadStatus.workItems?.length) {
            // there is a download pending or currently worked on. Enable the modules that were selected for that download, instead of the default set
            const selectedModules = downloadStatus.workItems.map(item => item.module)
            Object.keys(availableModules).forEach(name => {
              const module = availableModules[name]
              if (module.enabled === undefined) return
              module.enabled = selectedModules.includes(name)
            })
          }

          const availableModulesModel = new Backbone.Model(availableModules)
          const status = new Backbone.Model(downloadStatus)
          const sdView = new SelectDataView({ model: availableModulesModel, status })
          const dlView = new DownloadView({ model: status })

          $body.append(
            dlView.render().$el,
            sdView.render().$el
          )
        })
      }, yell)
    }
  })
})

// not actually used elsewhere, just return them to make unit tests easier
export default {
  handleApiResult,
  DownloadView,
  SelectDataView
}
