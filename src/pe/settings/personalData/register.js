import $ from '@/jquery'
import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import openSettings from '@/io.ox/settings/util'

import { gt } from 'gettext'

ext.point('io.ox/settings/pane').extend({
  id: 'personalData',
  index: 600,
  subgroup: 'io.ox/settings/pane/personalData'
})

ext.point('io.ox/settings/pane/personalData').extend({
  id: 'personaldata',
  title: gt('Download personal data'),
  ref: 'io.ox/settings/personalData',
  index: 100,
  icon: 'bi/download.svg',
  load: () => import('@/pe/settings/personalData/settings/pane.js')
})

//
// GDPR direct link to settings page
//
const gdprHandler = function (e) {
  e.preventDefault()
  const data = $(this).data()
  import('@/pe/settings/personalData/api').then(function ({ default: gdprAPI }) {
    // this triggers a redraw of the view if it was drawn before (usually this is only done on 'refresh^')
    gdprAPI.trigger('updateStatus')
  })
  openSettings(data.folder)
}

$(document).on('click', '.deep-link-gdpr', gdprHandler)

ox.on('click:deep-link-mail', function (e, scope) {
  const types = e.currentTarget.className.split(' ')

  if (types.indexOf('deep-link-gdpr') >= 0) gdprHandler.call(scope, e)
})
