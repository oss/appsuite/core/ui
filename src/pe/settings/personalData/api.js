import $ from '@/jquery'
import ox from '@/ox'
import Events from '@/io.ox/core/event'
import http from '@/io.ox/core/http'
import download from '@/io.ox/core/download'

// super simple cache to save available modules config, you really only need to get this data once. Not likely to ever change
const cache = {}

function triggerUpdate (result) {
  api.trigger('updateStatus')
  return result
}

const api = {
  downloadFile (id, packageNumber) {
    const params = $.param({ id, number: packageNumber, session: ox.session })
    download.url(
      `${ox.apiRoot}/gdpr/dataexport/${id}?${params}`
    )
  },

  getAvailableDownloads () {
    return http.GET({
      url: 'api/gdpr/dataexport'
    })
  },

  cancelDownloadRequest () {
    return http.DELETE({
      url: 'api/gdpr/dataexport'
    }).then(triggerUpdate, error => {
      // no export there or already finished. Update views, accordingly
      if (error.code === 'GDPR-EXPORT-0009') api.trigger('updateStatus')
      return error
    })
  },

  // deleteOldDataExport removes files from previously requested downloads. Careful here. Can cause data loss
  requestDownload (data, deleteOldDataExport) {
    return http.POST({
      url: 'api/gdpr/dataexport',
      data: JSON.stringify(data),
      contentType: 'application/json',
      params: { deleteOldDataExport }
    }).then(triggerUpdate)
  },

  getAvailableModules () {
    if (cache.availableModules) $.Deferred().resolve(cache.availableModules)
    return http.GET({
      url: 'api/gdpr/dataexport/availableModules'
    }).then(data => {
      cache.availableModules = data
      return data
    })
  },

  deleteAllFiles () {
    return http.DELETE({
      url: 'api/gdpr/dataexport/delete'
    }).then(triggerUpdate)
  }
}
Events.extend(api)

// just trigger updatestatus on refresh, the views will update themselves then if needed
ox.on('refresh^', triggerUpdate)

export default api
