import $ from '@/jquery'
import _ from '@/underscore'
import Backbone from '@/backbone'
import ext from '@/io.ox/core/extensions'
import manifests, { manifestManager } from '@/io.ox/core/manifests'
import upsell from '@/io.ox/core/upsell'
import yell from '@/io.ox/core/yell'
import { settings as coreSettings } from '@/io.ox/core/settings'
import { settings } from '@/pe/portal/settings'
import { addReadyListener } from '@/io.ox/core/events'
import gt from 'gettext'

// for temporary hacks use: ['pe/portal/plugins/helloworld/register']
const DEV_PLUGINS = []

// application object
let availablePlugins = _(manifests.manager.pluginsFor('portal')).uniq().concat(DEV_PLUGINS)
const collection = new Backbone.Collection([])
const widgetSet = settings.get('widgetSet', '')
const generation = Number(settings.get('generation', 0))

collection.comparator = function (a, b) {
  return ext.indexSorter({ index: a.get('index') }, { index: b.get('index') })
}

const widgets = {}
const loadWidgets = _.once(async function () {
  await manifests.manager.loadPluginsFor('portal')
  const userValues = settings.get('settings' + widgetSet, {})
  const userWidgets = settings.get('widgets/user')
  // remove possible upsell widgets from the user's jslob
  // upsell widget will be handled separate from all other widgets
  // it's protected and will be injected at runtime without a settings
  // entry in the jslob. It will stay as long as upsell is enabled
  if (userWidgets?.upsell_0) {
    delete userWidgets.upsell_0
    await settings.set('widgets/user', userWidgets).save()
  }
  // Load the users widgets
  _(userWidgets || {}).each(function (widgetDef, id) {
    widgets[id] = _.extend({}, widgetDef, { userWidget: true })
  })

  // http://oxpedia.org/wiki/index.php?title=AppSuite:Configuring_portal_plugins
  //                      +--------------+------------+----------------+--------+
  //                      | show on      | user can...                          |
  //                      | first start  | move       | enable/disable | delete |
  // +--------------------+--------------+------------+----------------+--------+
  // | default            | x            | x          | x              | x      |
  // | eager              | x            | x          | x              |        |
  // | protected          | x            | (property) |                |        |
  // | protected(disabled) |              |            |                |        |
  // +--------------------+--------------+------------+----------------+--------+

  // Ensure all eager widgets of all generations that weren't removed in their corresponding generation
  function processEager (gen) {
    const deleted = {}
    _(settings.get('widgets/deleted' + widgetSet + '/gen_' + gen, [])).each(function (id) {
      deleted[id] = true
    })
    return function process (widgetDef, id) {
      if (!deleted[id]) {
        widgets[id] = _.extend({}, widgets[id], widgetDef, userValues[id], { eagerWidget: true })
      }
    }
  }
  // process all eager generations
  for (let gen = 0; gen <= generation; gen++) {
    _(settings.get('widgets/eager' + widgetSet + '/gen_' + gen)).each(processEager(gen))
  }

  // Ensure all protected widgets
  _(settings.get('widgets/protected' + widgetSet)).each(function (widgetDef, id) {
    widgetDef.protectedWidget = true
    widgets[id] = _.extend({}, widgets[id], widgetDef, { protectedWidget: true })
    widgets[id].userWidget = false
    widgetDef.userWidget = false

    let draggable = false
    if (widgetDef.changeable) {
      const updates = userValues[id] || {}
      _(widgetDef.changeable).each(function (enabled, attr) {
        if (enabled) {
          widgets[id][attr] = updates[attr] || widgets[id][attr]
          if (attr === 'index') {
            draggable = true
          }
        }
      })
    }
    widgetDef.draggable = draggable
    widgets[id].draggable = draggable
  })

  // Upgrade or downgrade widgets from previous/later versions
  // this is needed, as we don't have migrations for the settings
  let needsSave = false
  for (const widget in widgets) {
    const { plugin } = widgets[widget]
    if (!plugin || manifestManager.plugins[plugin]) continue
    if (manifestManager.plugins[plugin.replace('plugins/portal/', 'pe/portal/plugins/')]) {
      widgets[widget].plugin = plugin.replace('plugins/portal/', 'pe/portal/plugins/')
      needsSave = true
    } else if (manifestManager.plugins[plugin.replace('pe/portal/plugins/', 'plugins/portal/')]) {
      widgets[widget].plugin = plugin.replace('pe/portal/plugins/', 'plugins/portal/')
      needsSave = true
    } else if (!manifestManager.plugins[plugin]) {
      console.warn(`Plugin '${plugin}' not found for widget with id '${widget}'.`)
    }
  }
  if (needsSave) await settings.set('widgets/user', widgets).save()

  // no widgets configured (first start)
  if (_.isEmpty(widgets) && !userWidgets) {
    Object.assign(widgets, {
      mail_0: {
        plugin: 'pe/portal/plugins/mail/register',
        userWidget: true,
        index: 1,
        props: {
          name: gt('Inbox')
        }
      },
      birthdays_0: {
        plugin: 'pe/portal/plugins/birthdays/register',
        userWidget: true,
        index: 4
      },
      calendar_0: {
        plugin: 'pe/portal/plugins/calendar/register',
        userWidget: true,
        index: 2
      },
      tasks_0: {
        plugin: 'pe/portal/plugins/tasks/register',
        userWidget: true,
        index: 3
      },
      myfiles_0: {
        plugin: 'pe/portal/plugins/recentfiles/register',
        userWidget: true,
        index: 4
      }
    })

    settings.set('widgets/user', widgets).save()
  }
  // Note: Instead of returning this, collection.reset() now returns the changed (added, removed or updated) model or list of models.
  collection
    .reset(api.getSettingsSorted())
})

const api = {

  // for demo/debugging
  addPlugin (plugin) {
    availablePlugins = availablePlugins.concat(plugin)
  },

  getAvailablePlugins () {
    return _(availablePlugins).uniq()
  },

  getCollection () {
    return collection
  },

  getEnabled () {
    return collection.filter(function (model) {
      return !model.has('candidate') && (!model.has('enabled') || model.get('enabled') === true)
    })
  },

  loadWidgets,

  getSettings () {
    const allTypes = ext.point('io.ox/portal/widget').pluck('id')

    return _(widgets)
      .chain()
    // map first since we need the object keys
      .map(function (obj, id) {
        obj.id = id
        obj.type = id.substr(0, id.lastIndexOf('_'))
        obj.props = obj.props || {}
        obj.inverse = _.isUndefined(obj.inverse) ? false : obj.inverse
        obj.enabled = _.isUndefined(obj.enabled) ? true : obj.enabled

        return obj
      })
      .filter(function (obj) {
        return _(api.getAvailablePlugins()).contains(obj.plugin) || _(allTypes).contains(obj.type)
      })
      .value()
  },

  getSettingsSorted () {
    return _(api.getSettings())
      .chain()
      .sortBy(function (obj) {
        return obj.index
      })
      .map(function (obj) {
        delete obj.candidate
        return obj
      })
      .value()
  },

  getOptions (type) {
    return ext.point('io.ox/portal/widget/' + type + '/settings').options()
  },

  getAllTypes () {
    const allTypes = ext.point('io.ox/portal/widget').pluck('id')

    return _.chain(api.getAvailablePlugins().concat(allTypes))
      .map(function (id) {
        return id
          .replace(/^portal\/plugins\/(\w+)\/register$/, '$1')
          .replace(/^pe\/portal\/plugins\/(\w+)\/register$/, '$1')
      })
      .uniq()
      .map(function (type) {
        const options = api.getOptions(type)
        // inject "requires" for upsell
        options.requires = api.requires(type)
        return options
      })
      .filter(function (obj) {
        return obj.type !== undefined
      })
      .value()
      .sort(function (a, b) {
        return a.title < b.title ? -1 : +1
      })
  },

  getUsedTypes () {
    return _(collection.pluck('type')).uniq().sort()
  },

  containsType (type) {
    return _(this.getUsedTypes()).contains(type)
  },

  getTitle (data, title) {
    // precedence if title is a function
    if (_.isFunction(title)) return title(data)
    // try custom title, then fallback
    return data.title || (data.props ? (data.props.description || data.props.title) : '') || title || ''
  },

  getPluginByType (type) {
    // look for full plugin path
    const plugin = ext.point('io.ox/portal/widget/' + type).prop('plugin')
    if (plugin) return plugin
    // look for type
    const prop = ext.point('io.ox/portal/widget/' + type).prop('type')
    return 'plugins/portal/' + (prop || type) + '/register'
  },

  add (type, options) {
    // find free id
    const defaults = settings.get('widgets/defaults', {})
    let i = 0; let id = type + '_0'

    options = _.extend({
      enabled: true,
      inverse: false,
      plugin: type,
      props: {}
    }, defaults[type] || {}, options || {})

    while (id in widgets) {
      id = type + '_' + (++i)
    }

    const widget = {
      enabled: options.enabled,
      inverse: options.inverse,
      id,
      // otherwise not visible
      index: 0,
      plugin: this.getPluginByType(options.plugin),
      props: options.props,
      type,
      userWidget: true
    }

    settings.set('widgets/user/' + id, widget).saveAndYell()

    // add to widget hash and collection
    widgets[id] = widget
    collection.unshift(widget)
  },

  remove (model) {
    collection.remove(model)
  },

  removeDisabled () {
    collection.remove(
      collection.filter(function (model) {
        return ext.point('io.ox/portal/widget/' + model.get('type'))
          .invoke('isEnabled')
          .reduce(function (memo, bool) {
            return memo && bool
          }, true)
          .value() === false
      })
    )
    return api.getEnabled()
  },

  toJSON () {
    // get latest values
    const w = {}
    collection.each(function (model) {
      if (!model.get('userWidget')) {
        return
      }
      const id = model.get('id')
      w[id] = model.toJSON()
      delete w[id].baton
    })
    return w
  },

  extraSettingsToJSON () {
    const extraSettings = {}
    collection.each(function (model) {
      if (model.get('userWidget')) {
        return
      }
      const id = model.get('id')
      extraSettings[id] = {
        index: model.get('index'),
        enabled: model.get('protectedWidget') ? true : model.get('enabled')
      }
    })
    return extraSettings
  },

  update (obj) {
    collection.each(function (model) {
      const id = model.get('id')
      if (id in obj) {
        model.set(obj[id], { silent: true, validate: true })
      }
    })
  },

  /**
   * Save a list of widgets into the database. The parameter
   * @param   widgetList - the sortable list of widgets
   * @return             - a deferred object with the save request
   */
  save (widgetList) {
    const self = this

    // update collection

    const obj = _.extend({}, widgets)
    const oldState = obj
    let index = 0

    function getIndex (i) {
      return i
    }

    // update all indices
    widgetList.children().each(function () {
      const node = $(this); const id = node.attr('data-widget-id')
      if (id in obj) {
        if (obj[id].draggable || _.isUndefined(obj[id].draggable) || _.isNull(obj[id].draggable)) {
          index++
          obj[id].index = getIndex(index)
        }
      }
    })

    self.update(obj)
    collection.sort()
    return settings.set('widgets/user', self.toJSON()).set('settings' + widgetSet, self.extraSettingsToJSON()).save().catch(
      // don't say anything if successful
      function () {
        // reset old state
        self.update(oldState)
        collection.trigger('sort')
        yell('error', gt('Could not save settings.'))
      }
    )
  },

  requires (type) {
    const plugin = this.getPluginByType(type)
    return manifests.manager.getRequirements(plugin)
  },

  // convenience function for upsell
  visible (type) {
    const requires = this.requires(type)
    return upsell.has(requires)
  },

  // get model by widget id, e.g. mail_0
  getModel (id) {
    return collection.get(id)
  },

  // open settings by widget id
  // returns true if edit dialog can be opened, false otherwise
  edit (id) {
    const model = this.getModel(id); let options
    if (model) {
      options = this.getOptions(model.get('type'))
      if (_.isFunction(options.edit)) {
        options.edit(model)
        return true
      }
    }
    return false
  }
}

collection
  .on('change', _.debounce(function (model) {
    // update widgets object
    widgets[model.get('id')] = model.attributes
    settings.set('widgets/user', api.toJSON()).set('settings' + widgetSet, api.extraSettingsToJSON()).saveAndYell()
    // don’t handle positive case here, since this is called quite often
  }, 100))
  .on('remove', function (model) {
    if (model.get('protectedWidget')) {
      // Don't you dare
    } else if (model.get('eagerWidget')) {
      const blocklist = settings.get('widgets/deleted' + widgetSet + '/gen_' + generation, [])
      blocklist.push(model.get('id'))
      if (!settings.get('widgets/deleted')) {
        settings.set('widgets/deleted', {})
      }

      if (!settings.get('widgets/deleted' + widgetSet)) {
        settings.set('widgets/deleted' + widgetSet, {})
      }

      settings.set('widgets/deleted' + widgetSet + '/gen_' + generation, blocklist).saveAndYell()
    } else if (model.get('userWidget')) {
      settings.remove('widgets/user/' + model.get('id')).saveAndYell()
    }
  })

addReadyListener('settings', async () => {
  // add or remove upsell widget to portal
  const options = _.extend({
    enabled: false,
    requires: 'active_sync || caldav || carddav'
  }, coreSettings.get('features/upsell/portal-widget'))
  const showWidget = options.enabled && !upsell.has(options.requires) && upsell.enabled(options.requires)
  if (showWidget) {
    // pre-load the module to have meta data available
    await import('@/pe/portal/plugins/upsell/register')
    api.addPlugin('pe/portal/plugins/upsell/register')
    api.add('upsell')
  }
})

export default api
