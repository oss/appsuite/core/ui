import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/mail/api'
import * as util from '@/io.ox/mail/util'
import accountAPI from '@/io.ox/core/api/account'
import portalWidgets from '@/pe/portal/widgets'
import ModalDialog from '@/io.ox/backbone/views/modal'
import DisposableView from '@/io.ox/backbone/views/disposable'
import CollectionLoader from '@/io.ox/core/api/collection-loader'
import capabilities from '@/io.ox/core/capabilities'
import ox from '@/ox'
import folderAPI from '@/io.ox/core/folder/api'
import http from '@/io.ox/core/http'
import _ from '@/underscore'
import $ from '@/jquery'
import moment from '@/moment'
import '@/pe/portal/plugins/mail/style.scss'

import { settings as mailSettings } from '@/io.ox/mail/settings'
import gt from 'gettext'
import { createIcon } from '@/io.ox/core/components'

async function draw (baton) {
  const popup = this.busy()
  const detail = (await import('@/io.ox/mail/detail/view')).default
  const obj = api.reduce(baton.item)
  api.get(obj).then(function (data) {
    const view = new detail.View({
      data,
      // no threads - no different subject
      disable: { 'io.ox/mail/detail/header/row3': 'different-subject' }
    })
    popup.idle().empty().append(view.render().expand().$el.addClass('no-padding'))
    data = null
    // response to "remove" event
    view.listenTo(view.model, 'remove', function () {
      popup.trigger('close')
    })
  })
}

const MailView = DisposableView.extend({

  tagName: 'li',
  className: 'item',

  initialize () {
    this.listenTo(this.model, 'change', this.render)
  },

  render (baton) {
    const self = this
    const subject = this.model.get('subject') ? _.ellipsis(this.model.get('subject'), { max: 50 }) : gt('No subject')
    const received = moment(this.model.get('date')).format('l')
    const sender = this.model.get('from')[0] || ['']
    const senderName = util.getDisplayName(sender) || gt('Unknown Sender')

    this.$el.empty()
      .attr({ 'data-detail-popup': 'mail', 'data-cid': this.model.cid })
      .data('item', this.model.attributes)
      .append(
        $('<div class="row1">').append(
          (function () {
            if ((self.model.get('flags') & 32) === 0) {
              return createIcon('bi/circle-fill.svg').addClass('new-item accent')
            }
          })(),
          // same markup as mail's common-extensions.from
          $('<div class="from">').attr('title', sender[1]).append(
            $('<span class="flags">'),
            $('<div class="person">').text(_.noI18n(senderName)), $.txt(' ')
          ),
          $('<div class="date accent">').text(_.noI18n(received))
        ),
        $('<div class="row2">').append(
          $('<div class="subject ellipsis">').text(subject),
          $.txt(' ')
        )
      )

    // Give plugins a chance to customize mail display
    ext.point('io.ox/mail/portal/list/item').invoke('customize', this.$el, this.model.toJSON(), baton, this.$el)
    return this
  }
})

const MailListView = DisposableView.extend({

  tagName: 'ul',

  className: 'mailwidget content list-unstyled',

  initialize () {
    // reset is only triggered by garbage collection and causes wrong "empty inbox" messages under certain conditions
    this.listenTo(this.collection, 'add remove set', this.render)
    this.listenTo(this.collection, 'expire', this.onExpire)
  },

  onExpire () {
    // revert flag since this is an active collection (see bug 54111)
    this.collection.expired = false
  },

  render (baton) {
    if (this.collection.length > 0) {
      this.$el.empty().append(
        _(this.collection.first(10)).map(function (mailModel) {
          return new MailView({
            model: mailModel
          }).render(baton).$el
        })
      )
    } else {
      this.$el.empty().append($('<li>').text(gt('No mails in your inbox')))
    }
    return this
  }
})

function getFolderName (baton) {
  const props = baton.model.get('props', {})
  if (props.id) return $.when('default' + props.id + '/INBOX')
  return accountAPI.getUnifiedMailboxName().then(function (mb) {
    return mb ? mb + '/INBOX' : api.getDefaultFolder()
  })
}

async function reload (baton) {
  // force refresh
  const portal = (await ox.load(() => import('@/pe/portal/main'))).default
  baton.collection.expired = true
  portal.getApp().refreshWidget(baton.model, 0)
}

ext.point('io.ox/portal/widget/mail').extend({

  title: gt('Inbox'),
  plugin: 'pe/portal/plugins/mail/register',

  initialize (baton) {
    // create new collection loader instance
    baton.collectionLoader = new CollectionLoader({
      module: 'mail',
      getQueryParams (params) {
        return {
          action: 'all',
          folder: params.folder,
          columns: http.defaultColumns.mail.all,
          sort: params.sort || '610',
          order: params.order || 'desc',
          timezone: 'utc',
          deleted: !mailSettings.get('features/ignoreDeleted', false)
        }
      }
    })

    baton.collectionLoader.each = function (obj) {
      obj.from = Array.isArray(obj.from[0]) ? obj.from : [[gt('Unknown sender')]]
      api.pool.add('detail', obj)
    }

    baton.loadCollection = function (folder) {
      const def = new $.Deferred()

      this.collectionLoader
        .load({ folder })
        .once('load', function () {
          def.resolve()
          this.off('load:fail')
        })
        .once('load:fail', function (error) {
          def.reject(error)
          this.off('load')
        })

      return def
    }

    return $.when(getFolderName(baton)).done(function (folderName) {
      api.on('refresh.all', _.partial(reload, baton))
      api.on('update', function (event, list, target) {
        if (target === folderName) reload(baton)
      })
    })
  },

  load (baton) {
    return $.when(getFolderName(baton)).then(function (folder) {
      const loader = baton.collectionLoader
      const params = loader.getQueryParams({ folder })
      baton.folder = folder
      baton.collection = loader.getCollection(params)
      if (!folder) {
        return $.Deferred().reject({ error: api.mailServerDownMessage, retry: false })
      }
      return folderAPI.get(folder)
    }).then(() => {
      if (baton.collection.length === 0 || baton.collection.expired) return baton.loadCollection(baton.folder)
    })
  },

  summary (baton) {
    if (this.children('.summary').length) return

    const node = $('<div class="summary">')

    // get folder model
    const model = folderAPI.pool.getModel(baton.folder)
    setSummary(model)
    this.append(node).addClass('with-summary show-summary')
    model.on('change:unread', setSummary)

    function setSummary (model) {
      const unread = model.get('unread')
      if (!unread) return node.text(gt('You have no unread messages'))
      node.text(
        // #. %1$d is the number of mails
        // #, c-format
        gt.ngettext('You have %1$d unread message', 'You have %1$d unread messages', unread, unread)
      )
    }
  },

  preview (baton) {
    this.append(new MailListView({
      collection: baton.collection
    }).render(baton).$el)
  },

  draw
})

function edit (model, view) {
  // disable widget till data is set by user
  model.set('candidate', true, { silent: true, validate: true })

  const dialog = new ModalDialog({ title: gt('Inbox'), async: true })
  const props = model.get('props') || {}

  accountAPI.all().then(function (accounts) {
    let accSelect; let nameInput; const options = _(accounts).map(function (acc) {
      return $('<option>').val(acc.id).text(acc.name).prop('selected', props.id && (props.id === String(acc.id)))
    })

    dialog.build(function () {
      const accId = _.uniqueId('form-control-label-')
      const nameId = _.uniqueId('form-control-label-')
      this.$body.append(
        capabilities.has('multiple_mail_accounts')
          ? $('<div class="form-group">').append(
            $('<label>').attr('for', accId).text(gt('Account')),
            accSelect = $('<select class="form-control">').attr('id', accId).prop('disabled', options.length <= 1).append(options)
          )
          : $(),
        $('<div class="form-group">').append(
          $('<label>').attr('for', nameId).text(gt('Description')),
          nameInput = $('<input type="text" class="form-control">').attr('id', nameId).val(props.name || gt('Inbox')),
          $('<div class="alert alert-danger">').css('margin-top', '15px').hide()
        )
      )
    })
      .addCancelButton()
      .addButton({ label: gt('Save'), action: 'save' })
      .on('show', function () {
        if (options.length > 1) {
          if (!props.name) {
            accSelect.on('change', function () {
              nameInput.val(gt('Inbox') + ' (' + $('option:selected', this).text() + ')')
            }).change()
          }
          // set focus
          accSelect.focus()
        } else {
          nameInput.focus()
        }
      }).open()

    dialog.on('save', function () {
      const title = $.trim(nameInput.val())
      const widgetProps = { name: title }
      if (options.length > 1) widgetProps.id = accSelect.val()
      model.set({ title, props: widgetProps }).unset('candidate')
      dialog.close()
    }).on('cancel', function () {
      if (model.has('candidate') && _.isEmpty(model.attributes.props)) view.removeWidget()
    })
  })
}

ext.point('io.ox/portal/widget/mail/settings').extend({
  title: gt('Inbox'),
  type: 'mail',
  editable: true,
  edit,
  unique: !capabilities.has('multiple_mail_accounts')
})

ext.point('io.ox/portal/widget/stickymail').extend({

  // helps at reverse lookup
  type: 'mail',

  // called right after initialize. Should return a deferred object when done
  load (baton) {
    const props = baton.model.get('props') || {}
    const remove = function (event, list) {
      const removed = _(list).chain().map(_.cid).contains(_.cid(props)).value()
      if (!removed) return
      api.off('deleted-mails', remove)
      portalWidgets.getCollection().remove(baton.model)
    }
    return api.get({ folder: props.folder_id, id: props.id, view: 'text', unseen: true }, { cache: false }).then(
      function success (data) {
        baton.data = data
        // remove widget when mail is deleted
        api.on('deleted-mails', remove)
      },
      function fail (e) {
        throw e.code === 'MSG-0032' ? 'remove' : e
      }
    )
  },

  preview (baton) {
    const data = baton.data
    const received = moment(data.date).format('l')
    const content = _(data.attachments).reduce(function (memo, a) {
      return memo + (a.content_type === 'text/plain' ? a.content : '')
    }, '')

    this.append(
      $('<div class="content">').append(
        $('<div class="item">')
          .attr({ 'data-detail-popup': 'mail', 'data-cid': _.cid(data) })
          .data('item', data)
          .append(
            $('<span class="bold">').text(util.getDisplayName(data.from[0])), $.txt(' '),
            $('<span class="normal">').text(_.ellipsis(data.subject, { max: 100 })), $.txt(' '),
            $('<span class="accent">').text(received), $.txt(' '),
            $('<span class="gray">').text(_.ellipsis(content, { max: 600 }))
          )
      )
    )
  },

  draw
})
