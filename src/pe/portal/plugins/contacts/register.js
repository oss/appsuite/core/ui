import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/contacts/api'
import _ from '@/underscore'
import $ from '@/jquery'
import portalWidgets from '@/pe/portal/widgets'
ext.point('io.ox/portal/widget/stickycontact').extend({

  // helps at reverse lookup
  type: 'contacts',
  plugin: 'pe/portal/plugins/contacts/register',

  load (baton) {
    const props = baton.model.get('props') || {}
    return api.get({ folder: props.folder_id, id: props.id }).then(function (data) {
      baton.data = data
    }, function (e) {
      throw e.code === 'CON-0125' ? 'remove' : e
    })
  },

  preview (baton) {
    api.on('delete', function (event, element) {
      if (element.id === baton.data.id && element.folder_id === baton.data.folder_id) {
        const widgetCol = portalWidgets.getCollection()
        widgetCol.remove(baton.model)
      }
    })

    const list = baton.data.distribution_list || []; const content = $('<ul class="content pointer list-unstyled">')

    _(list).each(function (obj) {
      content.append(
        $('<li class="paragraph">').append(
          $('<div class="bold">').text(obj.display_name),
          $('<div class="accent">').text(obj.mail)
        )
      )
    })

    this.append(content)
  },

  async draw (baton) {
    const popup = this.busy()
    const obj = api.reduce(baton.data)
    const view = (await import('@/io.ox/contacts/view-detail')).default
    api.get(obj).done(function (data) {
      popup.idle().append(view.draw(data))
    })
  }
})
