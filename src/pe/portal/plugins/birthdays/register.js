import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/contacts/api'
import * as util from '@/io.ox/contacts/util'
import _ from '@/underscore'
import $ from '@/jquery'
import moment from '@/moment'
import ox from '@/ox'
import '@/pe/portal/plugins/birthdays/style.scss'
import { createIcon } from '@/io.ox/core/components'

import gt from 'gettext'

const WEEKS = 12

function unifySpelling (name) {
  // lowercase & transform umlauts
  return String(name).toLowerCase().replace(/ä/g, 'ae').replace(/ö/g, 'oe').replace(/ü/g, 'ue').replace(/ß/g, 'ss')
}

function cid (name, birthday) {
  return unifySpelling(name) + ':' + birthday.format('YYYYMMDD')
}

function markDuplicate (name, birthday, hash) {
  hash[cid(name, birthday)] = true
}

function isDuplicate (name, birthday, hash) {
  return cid(name, birthday) in hash
}

// use same code to get the date (see bug 50414)
function getBirthday (contact) {
  // birthday comes along in utc, so moment.utc(t); not moment(t).utc(true)
  return moment.utc(contact.birthday).startOf('day')
}

ext.point('io.ox/portal/widget/birthdays').extend({

  title: gt('Birthdays'),
  plugin: 'pe/portal/plugins/birthdays/register',

  initialize (baton) {
    api.on('update create delete', async function () {
      // refresh portal
      const portal = (await ox.load(() => import('@/pe/portal/main'))).default
      const portalApp = portal.getApp()
      const portalModel = portalApp.getWidgetCollection()._byId[baton.model.id]
      if (portalModel) {
        portalApp.refreshWidget(portalModel, 0)
      }
    })
  },

  load (baton) {
    const start = moment().utc(true).startOf('day').subtract(1, 'day')
    return api.birthdays({
      start: start.valueOf(),
      end: start.add(WEEKS, 'weeks').valueOf(),
      right_hand_limit: 25
    }).done(function (data) {
      baton.data = data
    })
  },

  preview (baton) {
    const $list = $('<ul class="content list-unstyled io-ox-portal-birthdays">')
    const hash = {}
    let contacts = baton.data
    const numOfItems = _.device('smartphone') ? 5 : 8

    // ignore broken birthdays
    contacts = _(contacts).filter(function (contact) {
      // null, undefined, empty string
      // 0 is valid (1.1.1970)
      return _.isNumber(contact.birthday)
    })

    if (contacts.length === 0) {
      $list.append(
        _.device('smartphone') ? '' : $('<li class="line no-padding flex-grow flex-center">').append(createIcon('bi/gift.svg').addClass('ornament')),
        $('<li class="line no-padding text-center">').text(gt('No birthdays within the next %1$d weeks', WEEKS))
      )
    } else {
      _(contacts.slice(0, numOfItems)).each(function (contact) {
        // just to be sure hours are the same
        const birthday = getBirthday(contact)
        let birthdayText = ''
        // it's not really today, its today in the year of the birthday
        const today = moment().utc(true).startOf('day').year(birthday.year())
        const name = util.getFullName(contact)
        if (birthday.isSame(today, 'day')) {
          // today
          birthdayText = gt('Today')
        } else if (birthday.diff(today, 'day') === 1) {
          // tomorrow
          birthdayText = gt('Tomorrow')
        } else if (birthday.diff(today, 'day') === -1) {
          // yesterday
          birthdayText = gt('Yesterday')
        } else {
          birthdayText = util.getBirthday(birthday)
        }

        if (!isDuplicate(name, birthday, hash)) {
          $list.append(
            $('<li class="item" tabindex="0">')
              .attr({ 'data-detail-popup': 'contact', 'data-cid': _.cid(contact) })
              .append(
                api.pictureHalo(
                  $('<div class="picture">').text(util.getInitials(contact)),
                  contact,
                  { width: 32, height: 32, fallback: false }
                ),
                $('<div class="bold ellipsis">').text(name).prepend(
                  birthday.isSame(today, 'day') ? $('<div class="cake">').append(createIcon('bi/star.svg')) : $()
                ),
                $('<div class="accent">').text(birthdayText)
              )
          )
          markDuplicate(name, birthday, hash)
        }
      })
    }

    this.append($list)
  },

  draw () {
  }
})

ext.point('io.ox/portal/widget/birthdays/settings').extend({
  title: gt('Birthdays'),
  type: 'birthdays',
  editable: false,
  unique: true
})
