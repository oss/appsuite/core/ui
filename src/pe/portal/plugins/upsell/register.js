import ext from '@/io.ox/core/extensions'
import upsell from '@/io.ox/core/upsell'
import _ from '@/underscore'
import ox from '@/ox'
import $ from '@/jquery'
import { createIllustration } from '@/io.ox/core/components'

import { settings as coreSettings } from '@/io.ox/core/settings'
import '@/pe/portal/plugins/upsell/style.scss'

import gt from 'gettext'

const id = 'portal-widget'
const options = _.extend({
  title: gt('Upgrade your account'),
  requires: 'active_sync || caldav || carddav',
  removable: false
}, coreSettings.get('features/upsell/' + id), coreSettings.get('features/upsell/' + id + '/i18n/' + ox.language))

function trigger (e) {
  // do not trigger when clicked on close
  if ($(e.target).closest('.disable-widget').length > 0) return
  upsell.trigger({ type: 'custom', id, missing: upsell.missing(options.requires) })
}

ext.point('io.ox/portal/widget/upsell').extend({

  title: options.title,
  plugin: 'pe/portal/plugins/upsell/register',

  preview () {
    if (options.imageURL) {
      this.addClass('image-upsell-widget').append(
        $('<button type="button" class="btn btn-link content">')
          .attr('title', gt('Upgrade your account'))
          .css('backgroundImage', 'url(' + options.imageURL + ')').on('click', trigger)
      )
    } else {
      this.append(
        $('<div class="content flex-center">').append(
          createIllustration('illustrations/rocket-with-stars.svg'),
          $('<h4 class="my-16">').text(gt('Upgrade your account')),
          $('<div class="text-center">').append(
            $('<button type="button" class="btn btn-default">')
              // #. Text for an upsell button (Upgrade your account)
              .text(gt('Upgrade'))
          ).on('click', trigger)
        )
      )
    }
    $('.disable-widget', this).remove()
  }
})
