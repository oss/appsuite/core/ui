import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/core/api/quota'
import capabilities from '@/io.ox/core/capabilities'
import QuotaView from '@/pe/backbone/mini-views/quota'
import $ from '@/jquery'
import '@/pe/portal/plugins/quota/style.scss'

import { settings as coreSettings } from '@/io.ox/core/settings'

import gt from 'gettext'
import { createIcon } from '@/io.ox/core/components'

function loadTile () {
  api.requestFileQuotaUpdates()
  return api.load()
}

function getFieldOptions (quota) {
  const options = []
  if (coreSettings.get('quotaMode', 'default') === 'unified' || capabilities.has('infostore')) {
    options.push({
      module: 'file',
      quota: quota.file.quota,
      usage: quota.file.use,
      title: coreSettings.get('quotaMode', 'default') === 'unified' ? gt('Storage') : gt('File storage')
    })
  }

  if (coreSettings.get('quotaMode', 'default') === 'default' && capabilities.has('webmail')) {
    options.push({
      module: 'mail',
      quota: quota.mail.quota,
      usage: quota.mail.use,
      title: gt('Mail storage')
    })
    options.push({
      module: 'mail',
      quota: quota.mail.countquota,
      usage: quota.mail.countuse,
      quotaField: 'countquota',
      usageField: 'countuse',
      renderUnlimited: false,
      title: gt('Mail count quota'),
      sizeFunction (name) {
        return name
      }
    })
  }

  // must check against undefined otherwise 0 values would lead to not displaying the quota. See Bug 25110
  return options.filter(option => option.quota !== undefined && option.usage !== undefined)
}

function drawTile (quota) {
  this.append(
    $('<ul class="content no-pointer list-unstyled">').append(
      $('<li class="flex-grow flex-center">').append(createIcon('bi/speedometer2.svg').addClass('ornament')),
      getFieldOptions(quota).map(function (options) {
        return new QuotaView({ tagName: 'li', ...options }).render().$el
      })
    )
  )
}

ext.point('io.ox/portal/widget/quota').extend({
  title: gt('Storage'),
  plugin: 'pe/portal/plugins/quota/register',
  load () { return $.Deferred().resolve() },
  draw () { return $.Deferred().resolve() },
  preview () {
    const self = this
    return loadTile().done(function (quota) {
      drawTile.call(self, quota)
    })
  }
})

ext.point('io.ox/portal/widget/quota/settings').extend({
  title: gt('Storage'),
  type: 'quota',
  editable: false,
  unique: true
})
