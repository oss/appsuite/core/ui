import ext from '@/io.ox/core/extensions'
import $ from '@/jquery'

import gt from 'gettext'

const title = gt('PowerDNS parental control')

ext.point('io.ox/portal/widget').extend({ id: 'powerdns-parental-control' })

ext.point('io.ox/portal/widget/powerdns-parental-control').extend({

  title,
  plugin: 'pe/portal/plugins/powerdns-parental-control/register',

  preview () {
    this.append(
      $('<div class="content">').append(
        $('<div class="paragraph">').append(
          $('<a role="button" class="action">')
          // #. button label within the client-onboarding widget
          // #. button opens the wizard to configure your device
            .text(gt('Open parental control settings'))
        )
      )
    )
  }

})

ext.point('io.ox/portal/widget/powerdns-parental-control/settings').extend({
  title,
  type: 'powerdns-parental-control',
  unique: true
})
