import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import taskAPI from '@/io.ox/tasks/api'
import * as util from '@/io.ox/tasks/util'
import _ from '@/underscore'
import $ from '@/jquery'

import gt from 'gettext'

ext.point('io.ox/portal/widget/tasks').extend({

  title: gt('My tasks'),
  plugin: 'pe/portal/plugins/tasks/register',

  initialize (baton) {
    taskAPI.on('update create delete', async function () {
      // refresh portal
      const portal = (await ox.load(() => import('@/pe/portal/main'))).default
      const portalApp = portal.getApp()
      const portalModel = portalApp.getWidgetCollection()._byId[baton.model.id]
      if (portalModel) {
        portalApp.refreshWidget(portalModel, 0)
      }
    })
  },

  load (baton) {
    // super special getAll method
    return taskAPI.getAllMyTasks().done(function (data) {
      baton.data = data
    })
  },

  summary (baton) {
    if (this.children('.summary').length) return

    this.addClass('with-summary show-summary')

    const tasks = _(baton.data).filter(function (task) {
      return task.end_time !== null && task.status !== 3
    })
    const sum = $('<div class="summary">')

    if (tasks.length === 0) {
      sum.text(gt('You don\'t have any tasks that are either due soon or overdue.'))
    } else {
      const task = util.interpretTask(_(tasks).first())

      sum.append(
        $('<li class="item flex-row" tabindex="0">').data('item', task).append(
          $('<div class="truncate flex-grow me-8">').append(
            $('<span class="bold">').text(_.ellipsis(task.title, { max: 50 })), $.txt(' '),
            task.end_time === ''
              ? ''
              : $('<span class="accent">').text(
                // #. Due on date
                gt('Due on %1$s', task.end_time)
              ),
            $.txt(' ')
          ),
          $('<span class="status">').text(task.status).addClass(task.badge)
        )
      )
    }

    this.append(sum)
  },

  preview (baton) {
    const content = $('<ul class="content list-unstyled">')

    const tasks = _(baton.data).filter(function (task) {
      return task.end_time !== null && task.status !== 3
    })

    if (tasks.length === 0) {
      this.append(
        content.append(
          $('<li>').text(gt('You don\'t have any tasks that are either due soon or overdue.'))
        )

      )
      return
    }

    _(tasks.slice(0, 10)).each(function (task) {
      task = util.interpretTask(task)
      content.append(
        $('<li class="flex-item text-base gap-8 flex-row" tabindex="0">')
          .attr({ 'data-detail-popup': 'task', 'data-cid': _.cid(task) })
          .data('item', task)
          .append(
            $('<div class="bold truncate grow-0">').text(task.title),
            task.end_time === ''
              ? ''
              : $('<div class="accent grow shrink-0">').text(
                // #. Due on date
                gt('Due on %1$s', task.end_time)
              ),
            $('<div class="self-center shrink-0">').text(task.status).addClass(task.badge)
          )
      )
    })

    this.append(content)
  },

  async draw (baton) {
    const popup = this.busy()
    const view = (await import('@/io.ox/tasks/view-detail')).default
    const obj = taskAPI.reduce(baton.item)
    taskAPI.get(obj).done(function (data) {
      popup.idle().append(view.draw(data))
    })
  }
})

ext.point('io.ox/portal/widget/tasks/settings').extend({
  title: gt('My tasks'),
  type: 'tasks',
  editable: false,
  unique: true
})
