import ext from '@/io.ox/core/extensions'
import ox from '@/ox'
import $ from '@/jquery'

import { settings as coreSettings } from '@/io.ox/core/settings'

import gt from 'gettext'
import { createIcon } from '@/io.ox/core/components'

let type
const wizard = coreSettings.get('onboardingWizard', true)
const id = type = 'client-onboarding'
const title = gt('Connect your device')

ext.point('io.ox/portal/widget').extend({ id })

ext.point('io.ox/portal/widget/client-onboarding').extend({

  title,

  type,
  plugin: 'pe/portal/plugins/client-onboarding/register',

  load (baton) {
    // define inline styles within baton
    // to allow simple customization
    baton.style = {
      content: {
        cursor: 'pointer'
      },
      devices: {
        'font-size': '4em',
        'max-width': '360px',
        width: '100%'
      }
    }
  },

  preview (baton) {
    const style = $.extend({ content: {}, devices: {} }, baton.style)
    this.append(
      $('<div class="content flex-center">')
        .css(style.content)
        .append(
          $('<div class="paragraph flex-center devices">')
            .css(style.devices)
            .append(
              createIcon('bi/laptop.svg').addClass('ornament mx-8'),
              createIcon('bi/phone.svg').addClass('ornament mx-8'),
              createIcon('bi/tablet-landscape.svg').addClass('ornament mx-8')
            ),
          $('<h3 class="text-center">').text(
            // #. title for 1st and snd step of the client onboarding wizard
            // #. users can configure their devices to access/sync appsuites data (f.e. install ox mail app)
            // #. %1$s the product name
            // #, c-format
            gt('Take %1$s with you!', ox.serverConfig.productName)
          ),
          $('<div class="paragraph text-center">').text(
            gt('Stay up-to-date on your favorite devices.')
          ),
          $('<div class="paragraph">').append(
            $('<button type="button" class="btn action">')
            // #. button label within the client-onboarding widget
            // #. button opens the wizard to configure your device
              .text(gt('Connect'))
          )
        )
      // listener
        .on('click', async function () {
          if (wizard) {
            const onboardingWizard = (await import('@/io.ox/onboarding/main')).default
            onboardingWizard.load()
          } else {
            const clientOnboardingWizard = (await import('@/io.ox/onboarding/clients/wizard')).default
            clientOnboardingWizard.run()
          }
        })
    )
  }
})

ext.point('io.ox/portal/widget/client-onboarding/settings').extend({
  title,
  type,
  unique: true,
  editable: false
})
