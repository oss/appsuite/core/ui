import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/calendar/api'
import * as util from '@/io.ox/calendar/util'
import yell from '@/io.ox/core/yell'
import ox from '@/ox'
import _ from '@/underscore'
import $ from '@/jquery'
import moment from '@/moment'
import Backbone from '@/backbone'
import '@/io.ox/calendar/style.scss'

import { settings } from '@/io.ox/calendar/settings'
import gt from 'gettext'

let listenerAdded = false

// hide day label (and following nodes) if they are the last visible item in the list.
function hideOverflowingItems (baton) {
  // add on show listener if widget is currently hidden
  if (!this.is(':visible')) {
    if (listenerAdded) return
    listenerAdded = true
    const widgetNode = this
    return baton.app.getWindow().one('show', function () {
      listenerAdded = false
      hideOverflowingItems.call(widgetNode, baton)
    })
  }
  let hide = false
  const children = this.find('.content').children().show()
  children.each(function (index, item) {
    if (hide) return $(item).hide()
    if (!$(item).hasClass('paragraph')) return
    if (children[index + 1] && item.offsetLeft !== children[index + 1].offsetLeft) {
      $(item).hide()
      hide = true
    }
  })
}

const EventsView = Backbone.View.extend({

  tagName: 'ul',

  className: 'content list-unstyled',

  initialize () {
    this.listenTo(this.collection, 'add remove change', this.render)
  },

  render () {
    const numOfItems = _.device('smartphone') ? 5 : 10
    let lastDate = ''
    this.$el.empty()
    this.collection
      .chain()
      .filter(function (model) {
        // use endDate instead of startDate so current appointments are shown too
        return model.getTimestamp('endDate') > _.now()
      })
      .first(numOfItems)
      .each(function (model) {
        const declined = util.getConfirmationStatus(model) === 'DECLINED'
        if (declined && !settings.get('showDeclinedAppointments', false)) return
        // show in user's timezone
        const start = model.getMoment('startDate').tz(moment().tz())
        const date = start.calendar(null, { sameDay: '[' + gt('Today') + ']', nextDay: '[' + gt('Tomorrow') + ']', nextWeek: 'dddd', sameElse: 'L' })
        const startTime = start.format('LT')
        const isAllday = util.isAllday(model)
        if (date !== lastDate) {
          this.$el.append(
            $('<li class="paragraph bold">').text(date).data('item', model)
          )
          lastDate = date
        }
        this.$el.append(
          $('<li class="item" tabindex="0">')
            .attr({ 'data-detail-popup': 'appointment', 'data-cid': model.cid })
            .addClass(declined ? 'declined' : '')
            .data('item', model)
            .append(
              $('<div class="flex-row">').append(
                $('<div class="time accent text-end me-8">').text(isAllday ? gt('All day') : startTime),
                $('<div class="summary truncate flex-grow">').append(
                  $('<span class="bold me-8 inline-block">').text(model.get('summary') || ''),
                  $('<span class="gray location">').text(model.get('location') || '')
                )
              )
            )
        )
      }, this)
      .value()

    return this
  }

})

function getRequestParams () {
  return {
    start: moment().startOf('day').valueOf(),
    end: moment().startOf('day').add(1, 'month').valueOf()
  }
}

async function reload (baton) {
  // force refresh
  const portal = (await ox.load(() => import('@/pe/portal/main'))).default
  baton.collection.expired = true
  portal.getApp().refreshWidget(baton.model, 0)
}

ext.point('io.ox/portal/widget/calendar').extend({

  title: gt('Appointments'),
  plugin: 'pe/portal/plugins/calendar/register',

  initialize (baton) {
    baton.collection = api.getCollection(getRequestParams())
    api.on('refresh.all update create delete move', function () { reload(baton) })
  },

  load (baton) {
    return new Promise((resolve, reject) => {
      baton.collection.sync()
      baton.collection
        .once('load', function () {
          resolve()
          this.off('load:fail')
        })
        .once('load:fail', function (error) {
          reject(error)
          this.off('load')
        })
    })
  },

  summary (baton) {
    if (this.children('.summary').length) return

    this.addClass('with-summary show-summary')
    const sum = $('<div class="summary">')
    const model = baton.collection && baton.collection.first()

    if (!model) {
      sum.text(gt('You don\'t have any appointments in the near future.'))
    } else {
      util.getEvenSmarterDate(model, true)
      sum.append(
        $('<span class="normal accent">').text(util.getEvenSmarterDate(model, true)), $.txt('\u00A0'),
        $('<span class="bold">').text(model.get('summary') || ''), $.txt('\u00A0'),
        $('<span class="gray">').text(model.get('location') || '').toggleClass('hidden', model.get('location')?.startsWith('https'))
      )
    }

    this.append(sum)
  },

  preview (baton) {
    // use endDate instead of startDate so current appointments are shown too
    const collection = baton.collection.filter(function (model) { return model.getTimestamp('endDate') > _.now() })

    if (collection.length === 0) {
      this.append(
        $('<ul class="content list-unstyled">').append(
          $('<li class="line">')
            .text(gt('You don\'t have any appointments in the near future.'))
        )
      )
    } else {
      this.append(new EventsView({
        collection: baton.collection
      }).render().$el)
    }

    hideOverflowingItems.call(this, baton)
  },

  async draw (baton) {
    if ('foo'.length > 1) return
    const popup = this.busy()
    const view = (await import('@/io.ox/calendar/view-detail')).default
    // model might contain only list request data yet. So get full appointment data
    api.get(baton.item).then(function (model) {
      popup.idle().append(view.draw(model.toJSON(), { deeplink: true }))
    }, function (error) {
      popup.close()
      yell(error)
    })
  },

  post (ext) {
    const self = this
    api.on('refresh.all', function () {
      ext.load().done(_.bind(ext.draw, self))
    })
  }
})

ext.point('io.ox/portal/widget/calendar/settings').extend({
  title: gt('Appointments'),
  type: 'calendar',
  editable: false,
  unique: true
})
