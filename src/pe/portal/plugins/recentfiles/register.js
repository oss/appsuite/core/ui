import ext from '@/io.ox/core/extensions'
import filesAPI from '@/io.ox/files/api'
import userAPI from '@/io.ox/core/api/user'
import strings from '@/io.ox/core/strings'
import _ from '@/underscore'
import $ from '@/jquery'
import moment from '@/moment'
import Backbone from '@/backbone'
import '@/pe/portal/plugins/recentfiles/style.scss'

import { settings as coreSettings } from '@/io.ox/core/settings'
import { settings as filesSettings } from '@/io.ox/files/settings'
import gt from 'gettext'

_(['recentfiles', 'myfiles']).each(function (type) {
  const searchOptions = { includeSubfolders: true, limit: _.device('smartphone') ? 5 : 10, order: 'desc', sort: 5 }

  if (type === 'myfiles') {
    searchOptions.folder = coreSettings.get('folder/infostore')
  }

  const title = type === 'recentfiles' ? gt('Recently changed files') : gt('My latest files')

  ext.point('io.ox/portal/widget/' + type).extend({

    // helps at reverse lookup
    type: 'recentfiles',
    plugin: 'pe/portal/plugins/recentfiles/register',

    title,

    load (baton) {
      return filesAPI.search('', searchOptions).then(function (files) {
        // don't show hidden files if disabled in settings
        if (filesSettings.get('showHidden') === false) {
          files = _(files).filter(function (file) {
            const title = (file ? file['com.openexchange.file.sanitizedFilename'] : '')
            return title.indexOf('.') !== 0
          })
        }
        // update baton
        baton.data = files
        // get user ids
        const userIds = _(files).chain().pluck('modified_by').uniq().value()
        // map userids back to each file
        return userAPI.getList(userIds)
          .then(function (users) {
            _(files).each(function (file) {
              file.modified_by = _(users).find(function (user) { return user.id === file.modified_by })
            })
          })
          .then(function () {
            return files
          })
      })
    },

    summary (baton) {
      if (this.children('.summary').length) return

      let message = ''
      const count = baton.data.length

      if (count === 0) {
        message = gt('No files have been changed recently')
      } else if (count === 1) {
        message = gt('1 file has been changed recently')
      } else {
        message = gt('%1$d files have been changed recently', count)
      }

      this.addClass('with-summary show-summary').append(
        $('<div class="summary">').text(message)
      )
    },

    preview (baton) {
      const content = $('<ul class="content recentfiles list-unstyled">').appendTo(this)
      const data = baton.data

      if (!data || data.length === 0) {
        content.append($('<li>').text(gt('No files have been changed recently')))
        return
      }

      content.append(
        _(data).map(function (file) {
          let filename = String(file['com.openexchange.file.sanitizedFilename'] || file.filename || file.title || '')
          const size = strings.fileSize(file.file_size, 1)
          const ago = moment.duration(file.last_modified - _.now()).humanize(true)
          // create nice filename for long names
          if (filename.length > 25) {
            // remove leading & tailing date stuff
            filename = filename
              .replace(/^[0-9_\-.]{5,}(\D)/i, '\u2026$1')
              .replace(/[0-9_\-.]{5,}(\.\w+)?$/, '\u2026$1')
          }
          return $('<li class="item file" tabindex="0">')
            .data('item', file)
            .append(
              $('<div class="title">').text(_.noI18n(_.ellipsis(filename, { max: 25 }))), $.txt(' '),
              $('<div class="flex-row">').append(
                // show WHO changed it OR file size
                $('<div class="inline-block flex-grow truncate me-8">').text(
                  type === 'recentfiles' ? file.modified_by.display_name : size
                ),
                // show WHEN it was changed
                $('<div class="inline-block accent">').text(ago)
              )
            )
        })
      )

      // store a copy of all items
      content.data('items', _(data).map(_.cid))

      content.on('click', 'li.item', async function (e) {
        e.stopPropagation()
        const items = $(e.delegateTarget).data('items')
        const item = $(e.currentTarget).data('item')
        const Viewer = (await import('@/io.ox/core/viewer/main')).default
        const models = filesAPI.resolve(items, false)
        const collection = new Backbone.Collection(models)
        const viewer = new Viewer()
        baton = new ext.Baton({ data: await filesAPI.get(item), collection })
        viewer.launch({ selection: baton.data, files: baton.collection.models })
      })
    },

    draw () {
    }
  })

  // publish
  ext.point('io.ox/portal/widget').extend({ id: type })

  ext.point('io.ox/portal/widget/' + type + '/settings').extend({
    title,
    type,
    unique: true,
    editable: false
  })
})
