import ext from '@/io.ox/core/extensions'
import rss from '@/io.ox/rss/api'
import ModalDialog from '@/io.ox/backbone/views/modal'
import { simpleSanitize } from '@/io.ox/mail/sanitizer'
import _ from '@/underscore'
import $ from '@/jquery'
import moment from '@/moment'
import { device } from '@/browser'
import apps from '@/io.ox/core/api/apps'
import accountAPI from '@/io.ox/core/api/account'

import '@/io.ox/keychain/api'

import gt from 'gettext'

const portalApp = apps.get('io.ox/portal')

ext.point('io.ox/portal/widget/rss').extend({

  title: gt('RSS Feed'),
  plugin: 'pe/portal/plugins/rss/register',

  // prevent loading on refresh when error occurs to not bloat logs (see Bug 41740)
  stopLoadingOnError: true,

  load (baton) {
    let urls = baton.model.get('props').url || []
    const title = baton.model.attributes.title
    // remove empty urls (causes backend error)
    // needs type check since the url that is set by config can be a string, array or key-value object
    urls = typeof urls === 'string'
      ? [urls.trim()]
      : _(urls).filter(url => url.trim() !== '')

    accountAPI.on('refresh:ssl', () => {
      portalApp.refreshWidget(baton.model)
    })

    return rss.getMany(urls).done(function (data) {
      // limit data manually till api call can be limited
      data = data.slice(0, 100)

      // fix some special characters
      data.forEach(function (item) {
        item.subject = item.subject.replace(/&nbsp;/g, ' ').replace('&amp;', '&').replace(/&#034;/g, '"')
      })

      baton.data = {
        items: data,
        title: '',
        link: '',
        urls
      }

      // use existing title or the title of the first feed
      if (title !== undefined && title !== '') {
        baton.data.title = title
      } else {
        const elem = data[0]

        if (elem !== undefined && elem.feedTitle !== undefined) {
          baton.data.title = elem.feedTitle
        } else {
          baton.data.title = gt('RSS Feed')
        }
      }
    })
  },

  preview (baton) {
    const data = baton.data
    const count = device('smartphone') ? 5 : 10
    const $content = $('<ul class="content pointer list-unstyled" tabindex="0" role="button" aria-label="' + gt('Press [enter] to jump to the rss stream.') + '">')

    if (data.items.length === 0) {
      $('<li class="item">').text(gt('No RSS feeds found.')).appendTo($content)
    } else {
      $(data.items).slice(0, count).each(function (index, entry) {
        $content.append(
          $('<li class="paragraph">').append(
            function () {
              if (data.urls.length > 1) {
                return $('<span class="gray">').text(entry.feedTitle + ' ')
              }
              return ''
            },
            $('<span>').text(entry.subject), $.txt('')
          )
        )
      })
    }

    this.append($content)
  },

  draw: (function () {
    function drawItem (item) {
      const $body = $('<div class="text-body noI18n">').html(simpleSanitize(item.body, { ALLOW_DATA_ATTR: false }))

      // add target to a tags
      $body.find('a').attr({ target: '_blank', rel: 'noopener' })
      // allow images from https sources
      $body.find('img[src=""][data-original-src^="https://"]').each(function (index, img) {
        $(img).attr('src', $(img).attr('data-original-src'))
          .css({ 'max-width': '300px', 'max-height': '300px' })
      })

      this.append(
        $('<div class="text">').append(
          $('<h4>').text(item.subject),
          $body,
          $('<div class="rss-url">').append(
            $('<a target="_blank" rel="noopener">').attr('href', item.url).text((item.feedTitle ? item.feedTitle + ' - ' : '') + moment(item.date).format('l'))
          )
        )
      )
    }

    return function (baton) {
      const data = baton.data
      const node = $('<div class="portal-feed">')

      if (data.title) {
        node.append($('<h2>').text(data.title))
      }

      data.items.forEach(drawItem, node)

      this.append(node)
    }
  }())
})

function edit (model, view) {
  // disable widget till data is set by user
  model.set('candidate', true, { silent: true, validate: true })

  const dialog = new ModalDialog({ title: gt('RSS Feeds'), async: true })
  const $url = $('<textarea id="rss_url" class="form-control" rows="5" placeholder="http://">')
  const $description = $('<input id="rss_desc" type="text" class="form-control">')
  const $error = $('<div class="alert alert-danger">').css('margin-top', '15px').hide()
  const props = model.get('props') || {}

  // needs type check since the url that is set by config is always a string
  if (props.url !== undefined) props.url = [].concat(props.url)

  dialog.build(function () {
    this.$body.append(
      $('<div class="form-group">').append(
        $('<label for="rss_url">').text(gt('URL')),
        $url.val((props.url || []).join('\n'))
      ),
      $('<div class="form-group">').append(
        $('<label for="rss_desc">').text(gt('Description')),
        $description.val(props.description),
        $error
      )
    )
  })
    .addCancelButton()
    .addButton({ label: gt('Save'), action: 'save' })
    .on('cancel', function () {
      if (model.has('candidate') && _.isEmpty(model.get('props')?.url)) view.removeWidget()
    })
    .on('save', function () {
      let url = $.trim($url.val())
      const description = $.trim($description.val())

      $error.hide()

      if (url.length === 0) {
        $error.text(gt('Please enter a feed URL.')).show()
        return dialog.idle()
      }

      // split and remove empty urls (causes backend error)
      url = url.split(/\n/).filter(feed => !!feed.trim())
      dialog.close()
      model.set(
        { title: description, props: { url, description } },
        { validate: true }
      ).unset('candidate')
    })
    .open()
}

ext.point('io.ox/portal/widget/rss/settings').extend({
  title: gt('RSS Feed'),
  type: 'rss',
  editable: true,
  edit
})
