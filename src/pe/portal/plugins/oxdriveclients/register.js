import ext from '@/io.ox/core/extensions'
import '@/pe/portal/plugins/oxdriveclients/style.scss'
import _ from '@/underscore'
import ox from '@/ox'
import $ from '@/jquery'
import { createIcon } from '@/io.ox/core/components'

import { settings } from '@/pe/portal/plugins/oxdriveclients/settings'
import gt from 'gettext'

function getPlatform () {
  const isAndroid = _.device('android')
  const isIOS = _.device('ios')
  const isMac = _.device('macos')
  const isWindows = _.device('windows')

  if (isWindows) return 'Windows'
  if (isAndroid) return 'Android'
  if (isIOS) return 'iOS'
  if (isMac) return 'Mac OS'
  // #. Will be used as part of another string, forming a sentence like "Download OX Drive for your platform". Platform is meant as operating system like Windows or Mac OS
  return gt('your platform')
}

function getAllOtherPlatforms () {
  const isAndroid = _.device('android')
  const isIOS = _.device('ios')
  const isMac = _.device('macos')
  const isWindows = _.device('windows')

  if (isWindows) return ['Android', 'iOS', 'Mac OS']
  if (isAndroid) return ['Windows', 'iOS', 'Mac OS']
  if (isIOS) return ['Mac OS', 'Windows', 'Android']
  if (isMac) return ['iOS', 'Android', 'Windows']
  // fallback for others
  return ['Android', 'iOS', 'Mac OS', 'Windows']
}

function getShopLinkWithImage (_platform, url) {
  let lang = ox.language.split('_')[0]
  // languages we have custom shop icons for
  let langs = settings.get('l10nImages')
  const imagePath = './pe/portal/plugins/oxdriveclients/img/'
  let platform = _platform.toLowerCase()

  // convenience: support string of comma separated values
  langs = _.isString(langs) ? langs.split(',') : langs
  // fallback
  if (_.indexOf(langs, lang) === -1) lang = 'en'

  if (platform.match(/android|ios|mac os/)) {
    if (platform === 'mac os') platform = 'mac_os'
    const type = lang === 'en' && platform === 'mac_os' ? 'svg' : 'png'
    const $img = $('<div class="oxdrive-shop-image ' + platform + '">')
      .css('background-image', `url(${imagePath}${lang}_${platform}.${type}`)

    return $('<a class="shoplink" target="_blank" rel="noopener">').attr('href', url)
      .on('click', e => e.stopPropagation())
      .append($img, $('<span class="sr-only">').text(
        // #. Label of download button
        // #. %1$s is the product name
        // #. %2$s is the platform
        // #, c-format
        gt('Download the %1$s client for %2$s', settings.get('productName'), platform))
      )
  } else if (platform === 'windows') {
    return [
      createIcon('bi/download.svg'),
      $.txt(' '),
      $('<a class="shoplink" target="_blank" rel="noopener">').attr('href', ox.apiRoot + url + '?session=' + ox.session).text(
        // #. Label of download button
        // #. %s is the product name
        // #, c-format
        gt('Download %s', settings.get('productName'))
      )
    ]
  }
  return $()
}

function getText (baton) {
  const platform = getPlatform()
  let link = getShopLinkWithImage(platform, settings.get('linkTo/' + platform))

  // simple fallback for typical wrong configured customer systems (OX Drive for windows)
  if (_.isEmpty(settings.get('linkTo/' + platform))) {
    console.warn('OX Drive for windows settings URL not present! Please configure "io.ox/portal/plugins/oxdriveclients/linkTo/Windows" correctly')
    // #. Points the user to a another place in the UI to download an program file. Variable will be the product name, ie. "OX Drive"
    link = $('<p style="font-weight: bold">').text(gt('Please use the "Connect your device" wizard to download %s', settings.get('productName')))
  }

  const ul = $('<ul class="oxdrive content pointer list-unstyled">').append(
    $('<li class="first flex-center flex-grow">').append(createIcon('bi/cloud.svg').addClass('ornament')),
    $('<li class="message text-center">').append($('<h3>').text(baton.message)),
    $('<li class="teaser text-center">').text(baton.teaser),
    $('<li class="link">').append(link)
  )

  return ul
}

ext.point('io.ox/portal/widget/oxdriveclients').extend({

  // #. Product name will be inserted to advertise the product. I.e. "Get OX Drive" but meant in terms of gettings a piece of software from a online store
  title: () => gt('Get %s', settings.get('productName')),
  plugin: 'pe/portal/plugins/oxdriveclients/register',

  initialize: () => settings.ensureData(),

  load (baton) {
    // #. String will include a product name and a platform forming a sentence like "Download OX Drive for Windows now."
    // #. %1$s is the product name
    // #. %2$s is the platform
    // #, c-format
    baton.message = gt('Download %1$s for %2$s now', settings.get('productName'), getPlatform())
    // #. %1$s is the product name of the client app
    // #. %2$s is the AppSuite product name
    // #, c-format
    baton.teaser = gt('The %1$s client lets you store and share your photos, files, documents and videos, anytime, ' +
                'anywhere. Access any file you save to %1$s from all your computers, iPhone, iPad or from within %2$s itself.', settings.get('productName'), ox.serverConfig.productName)
    baton.link = settings.get('linkTo/' + getPlatform())
  },

  preview (baton) {
    this.append(getText(baton))
  },

  draw (baton) {
    const ul = getText(baton)
    this.append(ul)
    // all other platforms
    // #. Product name will be inserted, i.E. "Ox Drive is also available for other platforms". Platforms is meant as operating system like Windows
    ul.append($('<li>').append($('<h3>').text(gt('%s is also available for other platforms:', settings.get('productName')))))

    _.each(getAllOtherPlatforms(), function (os) {
      // use isEmpty here to detect missing settings, null, undefined and empty string
      if (!_.isEmpty(settings.get('linkTo/' + os))) ul.append($('<li class="link">').append(getShopLinkWithImage(os, settings.get('linkTo/' + os))))
    })
  }
})

ext.point('io.ox/portal/widget/oxdriveclients/settings').extend({
  // #. Product name will be inserted to advertise the product. I.e. "Get OX Drive" but meant in terms of gettings a piece of software from a online store
  title: () => gt('Get %s', settings.get('productName')),
  initialize: () => settings.ensureData(),
  type: 'oxdriveclients',
  editable: false,
  unique: true
})
