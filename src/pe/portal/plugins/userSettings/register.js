import $ from '@/jquery'
import _ from '@/underscore'
import ext from '@/io.ox/core/extensions'
import { changePassword } from '@/io.ox/settings/security/change-password'
import '@/pe/portal/plugins/userSettings/style.scss'
import { addReadyListener } from '@/io.ox/core/events'

import { settings as coreSettings } from '@/io.ox/core/settings'

import gt from 'gettext'
import { createIcon } from '@/io.ox/core/components'

const internalUserEdit = coreSettings.get('user/internalUserEdit', true)

addReadyListener('capabilities:user', (capabilities) => {
  const passwordEdit = capabilities.has('edit_password')
  if (!internalUserEdit && !passwordEdit) return

  function keyClickFilter (e) {
    if (e.which === 13 || e.type === 'click') {
      if (_.isFunction(e.data.fn)) {
        e.data.fn()
      }
    }
  }

  async function changeUserData () {
    const users = (await import('@/io.ox/core/settings/user')).default
    users.openModalDialog()
  }

  ext.point('io.ox/portal/widget/userSettings').extend({

    title: gt('User data'),
    plugin: 'pe/portal/plugins/userSettings/register',

    preview () {
      const paragraph = $('<div class="paragraph mt-auto">')
      if (internalUserEdit) {
        paragraph.append(
          // user data
          $('<button class="btn action" type="button">').text(gt('My contact data'))
            .on('click keypress', { fn: changeUserData }, keyClickFilter)

        )
      }
      // password
      // check for capability
      if (passwordEdit) {
        paragraph.append(
          $('<button class="btn action" type="button">').text(gt('My password'))
            .on('click keypress', { fn: changePassword }, keyClickFilter)
        )
      }
      this.append(
        $('<div class="content mt-auto">').append(
          $('<div class="paragraph flex-center flex-grow m-0">').append(
            createIcon('bi/person-badge.svg').addClass('ornament')
          ),
          paragraph
        ))
    }
  })

  ext.point('io.ox/portal/widget/userSettings/settings').extend({
    title: gt('User data'),
    type: 'userSettings',
    editable: false,
    unique: true
  })
})
