import ext from '@/io.ox/core/extensions'
import $ from '@/jquery'

import gt from 'gettext'

// most common trap is that you plugin is not loaded at all.
// there are different ways to enabled a plugin. simple hack is to
// register it manually in pe/portal/main.js by adding it to the
// DEV_PLUGINS array.

console.info('Loaded portal plugin: pe/portal/plugins/helloworld/register')

// this line allows putting this file to other folders than "apps/plugins/portal"
ext.point('io.ox/portal/widget').extend({ id: 'helloworld' })

ext.point('io.ox/portal/widget/helloworld').extend({

  // widget title (fills <h2> title node)
  title: 'Hello World',
  plugin: 'pe/portal/plugins/helloworld/register',

  // if 'action' is implemented the widget title becomes clickable
  action (baton) {
    /* eslint no-alert: 0 */
    alert(baton.hello)
  },

  // called first. Optional. You can create instances here and put them into the baton
  initialize (baton) {
    baton.hello = String('Hello World')
  },

  // called right after initialize. Should return a deferred object when done
  load () {
    return $.when()
  },

  // called to draw the preview stuff on the portal. waits for load().
  // usually a widget creates a "content" div
  preview (baton) {
    const content = $('<div class="content pointer">').text(baton.hello)
    // you could do something great here
    this.append(content)
  },

  // 'draw' is called to put content into the popup
  // the popup uses the following delegate: '.item, .content.pointer'
  // so you have to you proper CSS classes in preview
  draw (baton) {
    this.append(
      $('<h1>').text(baton.hello)
    )
  }
})

ext.point('io.ox/portal/widget/helloworld/settings').extend({
  title: gt('Hello World'),
  type: 'helloworld',
  editable: false
})
