import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/xing/api'
import yell from '@/io.ox/core/yell'
import '@/pe/portal/plugins/xing/xing.scss'
import ox from '@/ox'
import $ from '@/jquery'
import { createIcon } from '@/io.ox/core/components'

import gt from 'gettext'

// DEPRECATED: Support for `xing` is deprecated, pending removal with 8.36
console.warn('Support for `xing` is deprecated, pending removal with 8.36')

ext.point('io.ox/portal/widget/xing/reaction').extend({
  id: 'comment',

  accepts (activityName) {
    return activityName.toUpperCase() === 'COMMENT'
  },

  handle (activity) {
    // tres fragile
    const id = activity.ids[0]

    const commentToggle = function () {
      container.toggle()
    }

    const formSubmission = function () {
      console.log('Commenting:', textarea.val())

      api.addComment({
        activity_id: id,
        text: textarea.val()
      })
        .fail(function (response) {
          yell('error', gt('There was a problem with XING, the error message was: "%s"', response.error))
        })
        .done(function () {
          container.toggle()
          textarea.val('')
          yell('success', gt('Comment has been successfully posted on XING'))
          ox.trigger('refresh^')
        })
    }

    const container = $('<div>').addClass('comment-form')
    const textarea = $('<textarea>').attr({ rows: 3, cols: 40 })

    return $('<div class="xing possible-action comment">').append(
      $('<span class="comment-toggle">').append(
        createIcon('bi/chat-left-text.svg'),
        $.txt(gt('Comment'))
      ).on('click', commentToggle),
      container.append(
        textarea,
        $('<button type="button" class="btn btn-primary">').text(gt('Submit comment'))
      ).on('click', '.btn', formSubmission).hide()
    )
  }
})

ext.point('io.ox/portal/widget/xing/reaction').extend({
  id: 'delete',

  accepts (activityName) {
    return activityName.toUpperCase() === 'DELETE'
  },

  handle (activity) {
    // tres fragile
    const id = activity.ids[0]

    const handler = function () {
      const container = $(`.activity[data-activity-id="${CSS.escape(id)}"]`)
      const type = activity.type
      let successMessage
      let def = $.Deferred()

      if (type === 'activity') {
        def = api.deleteActivity({ activity_id: id })
        successMessage = gt('The activity has been deleted successfully')
      } else if (type === 'comment') {
        def = api.deleteComment({ comment_id: id })
        successMessage = gt('The comment has been deleted successfully')
      } else {
        console.log('We currently do not know how to handle deleting data of type="' + type + '". Please let us know about it. Here is more data:', JSON.stringify(activity))
      }

      def.fail(function (response) {
        yell('error', gt('There was a problem with XING. The error message was: "%s"', response.error))
      })
        .done(function () {
          container.remove()
          // #. %s may be either 'comment' or 'activity'
          yell('success', successMessage)
          ox.trigger('refresh^')
        })
    }

    return $('<div class="xing possible-action delete">').append(
      createIcon('bi/trash.svg'),
      $.txt(gt('Delete'))
    ).on('click', handler)
  }
})

ext.point('io.ox/portal/widget/xing/reaction').extend({
  id: 'like',

  accepts (activityName) {
    return activityName.toUpperCase() === 'LIKE'
  },

  handle (activity) {
    const actId = activity.ids[0]
    // tres fragile
    const comId = activity.objects[0].id
    const isAlreadyLiked = activity && activity.likes && activity.likes.current_user_liked

    const handler = function () {
      let def, message

      if (isAlreadyLiked) {
        def = api.unlikeActivity({
          activity_id: actId,
          comment_id: comId
        })
        // #. As on Facebook, XING allows a stop pointing out they liked a comment. An 'undo' for the like action, if you will.
        message = gt('Un-liked comment')
      } else {
        def = api.likeActivity({
          activity_id: actId,
          comment_id: comId
        })
        // #. As on Facebook, XING allows a user to point out that they like a comment
        message = gt('Liked comment')
      }

      def.fail(function (response) {
        yell('error', gt('There was a problem with XING, the error message was: "%s"', response.error))
      })
        .done(function () {
          yell('success', message)
          ox.trigger('refresh^')
        })
    }

    if (isAlreadyLiked) {
      return $('<div class="xing possible-action unlike">').append(
        createIcon('bi/hand-thumbs-down.svg'),
        $.txt(gt('Un-like'))
      ).on('click', handler)
    }
    return $('<div class="xing possible-action like">').append(
      createIcon('bi/hand-thumbs-up.svg'),
      $.txt(gt('Like'))
    ).on('click', handler)
  }
})

ext.point('io.ox/portal/widget/xing/reaction').extend({
  id: 'share',

  accepts (activityName) {
    return activityName.toUpperCase() === 'SHARE'
    /* assuming the experimental "share link" function will be different */
  },

  handle (activity) {
    // tres fragile
    const id = activity.ids[0]

    const handler = function () {
      api.shareActivity({
        activity_id: id
      })
        .fail(function (response) {
          yell('error', gt('There was a problem with XING, the error message was: "%s"', response.error))
        })
        .done(function () {
          yell('success', gt('Shared activity'))
          ox.trigger('refresh^')
        })
    }

    return $('<div class="xing possible-action share">').append(
      createIcon('bi/share.svg'),
      $.txt(gt('Share'))
    ).on('click', handler)
  }
})
