import ext from '@/io.ox/core/extensions'
import _ from '@/underscore'
import $ from '@/jquery'
import '@/pe/portal/plugins/xing/xing.scss'

import gt from 'gettext'

// DEPRECATED: Support for `xing` is deprecated, pending removal with 8.36
console.warn('Support for `xing` is deprecated, pending removal with 8.36')

const linkXingContact = function (contact) {
  const contactNode = $('<a class="external xing" target="_blank" rel="noopener">')
    .attr('href', 'https://www.xing.com/profile/' + contact.page_name)
  if (contact.photo_urls) {
    const ps = contact.photo_urls
    const photoUrl = ps.maxi_thumb || ps.medium_thumb || ps.mini_thumb || ps.thumb || ps.large

    $('<img>').attr({ href: photoUrl, class: 'photo' }).appendTo(contactNode)
  }
  contactNode.append($.txt(makeName(contact)))
  return contactNode
}

const makeName = function (actor) {
  return actor.type === 'user' ? actor.display_name : actor.name
}

const veryShortMiddleEllipsis = function (text, options) {
  if (!options || !options.limitLength) {
    return text
  }
  return _.ellipsis(text, { max: 30, charpos: 'middle' })
}

const veryShort = function (text, options) {
  if (!options || !options.limitLength) {
    return text
  }
  return _.ellipsis(text, { max: 30 })
}

const shorter = function (text, options) {
  if (!options || !options.limitLength) {
    return text
  }
  return _.ellipsis(text, { max: 50 })
}

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'link',
  accepts (activity) {
    return activity.verb === 'bookmark'
  },
  handle (activityObj, options) {
    return $('<div class="xing activityObj">').append(
      $('<div class="actionDesc">').text(gt('%1$s recommends this link:', makeName(activityObj.creator))),
      $('<div class="actionContent">').append(
        $('<a>').attr({ href: activityObj.url }).text(veryShortMiddleEllipsis(activityObj.url, options)).addClass('external xing'),
        $('<div class="title">').text(veryShort(activityObj.title, options)),
        $('<div class="description">').text(shorter(activityObj.description, options))
      )
    )
  }
})

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'singleBookmarkPost',
  accepts (activity) {
    return activity.verb === 'post' &&
                activity.objects.length === 1 &&
                activity.objects[0].type === 'bookmark'
  },
  handle (activity, options) {
    const linkActivity = activity.objects[0]
    return $('<div class="xing activityObj">').append(
      $('<div class="actionDesc">').text(gt('%1$s posted a link:', makeName(linkActivity.creator))),
      $('<div class="actionContent">').append(
        $('<a class="external xing" target="_blank" rel="noopener">').attr('href', linkActivity.url)
          .text(shorter(linkActivity.description, options) || veryShortMiddleEllipsis(linkActivity.url, options))
      )
    )
  }
})

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'singleStatusPost',
  accepts (activity) {
    return activity.verb === 'post' &&
                activity.objects.length === 1 &&
                activity.objects[0].type === 'status'
  },
  handle (activity, options) {
    const statusActivity = activity.objects[0]

    return $('<div class="xing activityObj">').append(
      // #. We do not know the gender of the user and therefore, it is impossible to write e.g. '%1$s changed her status'.
      // #. But you could use '%1$s changes his/her status' depending on the language.
      // #. %1$s the name of the user which changed his/her status
      $('<div class="actionDesc">').text(gt('%1$s changed the status:', makeName(statusActivity.creator))),
      $('<div class="actionContent">').text(shorter(statusActivity.content, options))
    )
  }
})

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'friend',
  accepts (activity) {
    return activity.verb === 'make-friend'
  },
  handle (activity) {
    const newContacts = []
    const actor = activity.actors[0]
    const objs = activity.objects

    if (objs.length === 1) {
      return $('<div class="xing activityObj">').append(
        $('<div class="actionDesc">').text(gt('%1$s has a new contact:', makeName(actor))),
        $('<div class="actionContent">').append(
          linkXingContact(objs[0])
        )
      )
    }

    _(objs).each(function (contact) {
      newContacts.push(linkXingContact(contact))
      newContacts.push($.txt(', '))
    })
    newContacts.pop()
    return $('<div class="xing activityObj">').append(
      $('<div class="actionDesc">').text(gt('%1$s has new contacts:', makeName(actor))),
      $('<div class="actionContent">').append(newContacts)
    )
  }
})

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'singleActivityPost',
  accepts (activity) {
    return activity.verb === 'post' &&
                activity.objects.length === 1 &&
                activity.objects[0].type === 'activity'
  },
  handle (activity, options) {
    const statusActivity = activity.objects[0]

    return $('<div class="xing activityObj">').append(
      $('<div class="actionDesc">').text(gt('%1$s posted a new activity:', makeName(statusActivity.creator))),
      $('<div class="actionContent">').text(shorter(statusActivity.content, options))
    )
  }
})

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'singleCompanyProfileUpdate',
  accepts (activity) {
    return (activity.verb === 'share' || activity.verb === 'post') &&
                activity.objects.length === 1 &&
                activity.objects[0].type === 'company_profile_update'
  },
  handle (activity, options) {
    const profileUpdate = activity.objects[0]

    return $('<div class="xing activityObj">').append(
      // #. We do not know the gender of the user and therefore, it is impossible to write e.g. '%1$s changed her profile:'.
      // #. But you could use '%1$s changes his/her profile:' depending on the language.
      // #. %1$s the name of the user which changed his/her profile
      $('<div class="actionDesc">').text(gt('%1$s updated the profile:', profileUpdate.description)),
      $('<div class="actionContent">').text(shorter(profileUpdate.name, options))
    )
  }
})

ext.point('io.ox/portal/widget/xing/activityhandler').extend({
  id: 'singleUpdate',
  accepts (activity) {
    return activity.verb === 'update' &&
                activity.objects.length === 1
  },
  handle (activity) {
    const profile = activity.objects[0]

    return $('<div class="xing activityObj">').append(
      // #. We do not know the gender of the user and therefore, it is impossible to write e.g. '%1$s changed her profile:'.
      // #. But you could use '%1$s changes his/her profile:' depending on the language.
      // #. %1$s the name of the user which changed his/her profile
      $('<div class="actionDesc">').text(gt('%1$s updated the profile:', makeName(profile))),
      $('<div class="actionContent">').append(
        linkXingContact(profile)
      )
    )
  }
})
