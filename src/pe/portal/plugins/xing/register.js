import ext from '@/io.ox/core/extensions'
import api from '@/io.ox/xing/api'
import userApi from '@/io.ox/core/api/user'
import yell from '@/io.ox/core/yell'
import ModalDialog from '@/io.ox/backbone/views/modal'
import * as util from '@/io.ox/core/settings/util'
import keychain from '@/io.ox/keychain/api'
import _ from '@/underscore'
import ox from '@/ox'
import $ from '@/jquery'
import moment from '@/moment'
import Backbone from '@/backbone'
import '@/pe/portal/plugins/xing/xing.scss'

import gt from 'gettext'

// DEPRECATED: Support for `xing` is deprecated, pending removal with 8.36
console.warn('Support for `xing` is deprecated, pending removal with 8.36')

const title = gt('XING')
const MAX_ITEMS_PREVIEW = 6
const XING_NAME = gt('XING')
const point = ext.point('io.ox/portal/widget/xing')

const reauthorizeAccount = function () {
  const account = keychain.getStandardAccount('xing')

  keychain.submodules.xing.reauthorize(account).then(function () {
    yell('success', gt('Successfully reauthorized your %s account', XING_NAME))
    ox.trigger('refresh^')
  }).catch(function (response) {
    yell('error', gt('There was a problem with %s. The error message was: "%s"', XING_NAME, response.error))
  })
}

const getLanguages = function () {
  return [
    { label: 'de', value: 'de' },
    { label: 'en', value: 'en' },
    { label: 'es', value: 'es' },
    { label: 'fr', value: 'fr' },
    { label: 'it', value: 'it' },
    { label: 'nl', value: 'nl' },
    { label: 'pl', value: 'pl' },
    { label: 'pt', value: 'pt' },
    { label: 'ru', value: 'ru' },
    { label: 'tr', value: 'tr' },
    { label: 'zh', value: 'zh' }
  ]
}

const createXingAccount = function (e) {
  e.preventDefault()

  // #. 'Create a XING Account' as a header of a modal dialog to create a XING account.
  new ModalDialog({ title: gt('Create a XING Account') })
    .build(function () {
      const self = this; let guid
      const language = 'language'
      const model = new Backbone.Model()
      model.isConfigurable = true
      model.isConfigurable = function () { return true }
      model.set(language, getLanguages()[0].value)

      this.$body.append(
        $('<p>').text(
          gt('Please select which of the following data we may use to create your %s account:', XING_NAME)
        ),
        $('<div class="row">').append(
          $('<div class="col-sm-12">').append(
            $('<label for="mail_address">').text(gt('Mail address')),
            this.$email = $('<input id="mail_address" type="text" name="email" class="form-control">').attr('id', guid)
          )
        ),
        $('<div class="row">').append(
          $('<div class="col-sm-12">').append(
            $('<label for="first_name">').text(gt('First name')),
            this.$firstname = $('<input id="first_name" type="text" class="form-control" placeholder="">')
          )
        ),
        $('<div class="row">').append(
          $('<div class="col-sm-12">').append(
            $('<label for="last_name">').text(gt('Last name')),
            this.$lastname = $('<input id="last_name" type="text" class="form-control" placeholder="">')
          )
        ),
        util.compactSelect(language, gt('Language'), model, getLanguages())
      )
      userApi.getCurrentUser().then(function (userData) {
        const locale = userData.attributes.locale
        self.$email.val(userData.attributes.email1 || userData.attributes.email2 || userData.attributes.email3)
        self.$firstname.val(userData.attributes.first_name)
        self.$lastname.val(userData.attributes.last_name)
        self.$language.val(locale.indexOf('_') > -1 ? locale.split('_')[0] : locale)
      })
    })
    .addCancelButton()
    // #. 'Create' as button text of a modal dialog to confirm to create a new XING-Account
    .addButton({ label: gt('Create'), action: 'accepted' })
    .on('accepted', function () {
      api.createProfile({
        tandc_check: true, // cSpell:disable-line
        email: this.$email.val(),
        first_name: this.$firstname.val(),
        last_name: this.$lastname.val(),
        language: this.$language.val()
      })
        .fail(function (response) {
          yell('error', gt('There was a problem with %s. The error message was: "%s"', XING_NAME, response.error))
        })
        .done(function () {
          yell({
            type: 'success',
            duration: 60000,
            message: gt('Please check your inbox for a confirmation email.\n\nFollow the instructions in the email and then return to the widget to complete account setup.')
          })
        })
    })
    .open()
}

const statusUpdateForm = function () {
  const form = $('<div class="xing comment">').append(
    $('<textarea rows="3" cols "40">'),
    $('<button type="button" class="btn btn-primary">').text(gt('Post a status update'))
  )

  form.on('click', '.btn', function (clickEvent) {
    const container = $(clickEvent.target).parent()
    const input = container.children('textarea')

    api.changeStatus({
      message: input.val()

    }).fail(function (response) {
      yell('error', gt('Your status update could not be posted on %s. The error message was: "%s"', XING_NAME, response.error))
    }).done(function () {
      container.remove()
      yell('success', gt('Your status update has been successfully posted on %s', XING_NAME))
    })
  })

  return form
}

const makeNewsfeed = function (networkActivities, options) {
  options = options || {}

  const maxCount = options.maxCount
  const node = $('<div class="networkActivities">')
  let newsItemCount = 0

  if (networkActivities.length === 0) {
    node.text(gt('There is no recent activity in your Xing network.'))
  }

  _(networkActivities).each(function (activity) {
    if (activity.type !== 'activity' || (maxCount && newsItemCount >= maxCount)) return

    const activityNode = $('<div class="activity">').appendTo(node)
    const reactionNode = $('<div class="reactions">')
    const dateNode = $('<div class="date">')
    let foundHandler = false
    const id = activity.ids[0]

    if (id) activityNode.attr('data-activity-id', id)

    ext.point('io.ox/portal/widget/xing/activityhandler').each(function (handler) {
      if (handler.accepts(activity, options)) {
        foundHandler = true
        handler.handle(activity, options).appendTo(activityNode)
      }
    })

    if (foundHandler) {
      // Date
      if (activity.created_at) {
        const creationDate = moment(activity.created_at)
        dateNode.text(creationDate.format('l LT')).appendTo(activityNode)
      }

      // reactions like comment, share, like
      _(activity.possible_actions).each(function (reaction) {
        ext.point('io.ox/portal/widget/xing/reaction').each(function (handler) {
          if (handler.accepts(reaction)) {
            reactionNode.append(
              handler.handle(activity)
            )
          }
        })
      })
      reactionNode.appendTo(activityNode)
      newsItemCount++
    } else {
      console.log('Could not find a handler for the following activity: ' + activity.verb + '. Please let us know about this. Here is some data that would help us: ', JSON.stringify(activity))
    }
  })

  if (newsItemCount.length === 0) {
    node.text(gt('There is no recent activity in your Xing network.'))
  }
  return node
}

const refreshWidget = async function () {
  const portal = (await ox.load(() => import('@/pe/portal/main'))).default
  const portalApp = portal.getApp()
  const portalModels = portalApp.getWidgetCollection().filter(function (model) { return /^xing_\d*/.test(model.id) })

  if (portalModels.length > 0) {
    portalApp.refreshWidget(portalModels[0], 0)
  }
}

/**
 * Portal extension points: Here's where it all starts
 */
point.extend({

  title,
  plugin: 'pe/portal/plugins/xing/register',

  // prevent loading on refresh when error occurs to not bloat logs (see Bug 41740)
  stopLoadingOnError: true,

  isEnabled () {
    return keychain.isEnabled('xing')
  },

  requiresSetUp () {
    return keychain.isEnabled('xing') && !keychain.hasStandardAccount('xing')
  },

  drawDefaultSetup (baton) {
    keychain.submodules.xing.off('create', null, this)
    keychain.submodules.xing.on('create', function () {
      api.createSubscription()
      baton.model.node.removeClass('requires-setup widget-color-custom color-xing')
      refreshWidget()
    }, this)
    const content = this.find('.content')
    this.toggleClass('widget-color-custom color-xing', true)
    content.find('.paragraph').empty().text(gt('Get news from your XING network delivered to you. Stay in touch and find out about new business opportunities.'))
    content.append(
      $('<a href="#" class="action" role="button">').text(
        // #. %1$s is social media name, e.g. Facebook
        gt('Create new %1$s account', XING_NAME)
      )
        .on('click', { baton }, createXingAccount)
    )
  },

  performSetUp () {
    const win = window.open('busy.html', '_blank', 'height=400, width=600')
    return keychain.createInteractively('xing', win)
  },

  load (baton) {
    return new Promise((resolve, reject) => {
      api.getUserfeed({
        // name variations, page_name, and picture
        user_fields: '0,1,2,3,4,8,23'
      })
        .then(xingResponse => {
          baton.data = xingResponse
          resolve(xingResponse)
        })
        .catch(error => {
          if (error.params && error.error_params[0] === 'Invalid OAuth token') {
            if (keychain.getStandardAccount('xing')) {
              baton.reauthorize = true
              return resolve()
            }
          }
          reject(error)
        })
    })
  },

  preview (baton) {
    // remove setup that may not have been cleared correctly (may happen if an account was created successfully but callback function wasn't called)
    this.find('.setup-questions').remove()
    if (baton.reauthorize) {
      this.append($('<div class="content">').append(
        $('<a href="#">').text(gt('Click here to reconnect to your xing account to see activities.')).on('click', function () {
          reauthorizeAccount()
          return false
        })
      ))
    } else {
      this.append(
        $('<div class="content preview io-ox-xing pointer">').append(
          makeNewsfeed(baton.data.network_activities, { maxCount: MAX_ITEMS_PREVIEW, limitLength: true })
        ).on('click', 'a.external.xing', function (e) { e.stopPropagation() })
      )
    }
  },

  draw (baton) {
    this.append(
      $('<div class="content io-ox-xing fullview">').append(
        statusUpdateForm(),
        $('<h2>').text(gt('Your %s newsfeed', XING_NAME)),
        makeNewsfeed(baton.data.network_activities)
      )
    )
  }
})

ext.point('io.ox/portal/widget/xing/settings').extend({
  title,
  type: 'xing',
  editable: false,
  unique: true
})
