import _ from '@/underscore'
import $ from '@/jquery'
import { Action } from '@/io.ox/backbone/views/actions/util'
import { getWidgetsByType } from '@/pe/portal/util'
import { isTrash } from '@/io.ox/files/actions'
import * as util from '@/io.ox/files/util'

import widgets from '@/pe/portal/widgets'
import api from '@/io.ox/contacts/api'
import yell from '@/io.ox/core/yell'

import gt from 'gettext'

function addedToPortal (data) {
  const cid = _.cid(data)
  return _(getWidgetsByType('stickycontact')).any(function (widget) {
    return _.cid(widget.props) === cid
  })
}

Action('io.ox/contacts/actions/add-to-portal', {
  capabilities: 'portal',
  collection: 'one',
  every: 'id && folder_id && mark_as_distributionlist',
  matches (baton) {
    const data = baton.first()
    return data.mark_as_distributionlist && !addedToPortal(data)
  },
  async action (baton) {
    await widgets.loadWidgets()

    const data = baton.first()
    widgets.add('stickycontact', {
      plugin: 'contacts',
      props: { id: data.id, folder_id: data.folder_id, title: data.display_name }
    })
    // trigger update event to get redraw of detail views
    api.trigger('update:' + _.ecid(data), data)
    yell('success', gt('This distribution list has been added to the portal'))
  }
})

Action('io.ox/files/actions/add-to-portal', {
  capabilities: 'portal',
  collection: 'one && items',
  matches (baton) {
    if (_.isEmpty(baton.data)) return false
    if (isTrash(baton)) return false
    if (util.isContact(baton)) return false
    if (baton.isViewer && !util.isCurrentVersion(baton)) return false
    return true
  },
  async action (baton) {
    await widgets.loadWidgets()

    const data = baton.first()
    widgets.add('stickyfile', {
      plugin: 'files',
      props: {
        id: data.id,
        folder_id: data.folder_id,
        title: data['com.openexchange.file.sanitizedFilename'] || data.filename || data.title
      }
    })
    yell('success', gt('This file has been added to the portal'))
  }
})

Action('io.ox/mail/actions/add-to-portal', {
  capabilities: 'portal',
  collection: 'one && toplevel',
  async action (baton) {
    await widgets.loadWidgets()

    const data = baton.first()
    // using baton.data.parent if previewing during compose (forward mail as attachment)
    widgets.add('stickymail', {
      plugin: 'mail',
      props: $.extend({
        id: data.id,
        folder_id: data.folder_id,
        title: data.subject ? data.subject : gt('No subject')
      }, data.parent || {})
    })
    yell('success', gt('This email has been added to the portal'))
  }
})
