import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import moment from '@/moment'
import ext from '@/io.ox/core/extensions'
import userAPI from '@/io.ox/core/api/user'
import contactAPI from '@/io.ox/contacts/api'
import DetailPopup from '@/io.ox/backbone/views/popup'
import ModalDialog from '@/io.ox/backbone/views/modal'
import widgets from '@/pe/portal/widgets'
import '@/pe/portal/settings/pane'
import WidgetSettingsView from '@/pe/portal/settings/widgetview'
import yell from '@/io.ox/core/yell'

import { settings as coreSettings } from '@/io.ox/core/settings'
import { settings } from '@/pe/portal/settings'
import '@/pe/portal/style.scss'
import gt from 'gettext'
import { createButton } from '@/io.ox/core/components'

const widgetsWithFirstVisit = ['mail', 'calendar', 'tasks', 'myfiles']
const firstVisitGreeting = {
  // #. Default greeting for portal widget
  mail: gt('Welcome to your inbox'),
  // #. Default greeting for portal widget
  calendar: gt('Welcome to your calendar'),
  // #. Default greeting for portal widget
  tasks: gt('Welcome to your tasks'),
  // #. Default greeting for portal widget
  myfiles: gt('Welcome to your files')
}
const hadData = settings.get('settings/hadData') || []

function isFirstVisitWidget (type, baton) {
  function hasDataShown (type) {
    return _.contains(hadData, type)
  }

  function containsData (type) {
    let tasks
    if (type === 'tasks') {
      tasks = _(baton.data).filter(function (task) {
        return task.end_time !== null && task.status !== 3
      })
      if (_.isEmpty(tasks)) return false
    }
    if (!_.isEmpty(baton.data)) return true
  }

  // URL is mandatory; return false if it's missing
  if (!settings.get('settings/getStartedLink')) return false

  if (_.contains(widgetsWithFirstVisit, type)) {
    if (containsData(type)) {
      if (!hasDataShown(type)) hadData.push(type)
      settings.set('settings/hadData', hadData).save()
      return false
    } else if (!containsData(type)) {
      if (hasDataShown(type)) {
        // reset for debugging
        // settings.set('settings/hadData', []).save();
        return false
      }
      return true
    }
  } else {
    return false
  }
}

// time-based greeting phrase
function getGreetingPhrase (name) {
  const hour = moment().hour()
  // find proper phrase
  if (hour >= 4 && hour <= 11) {
    return gt('Good morning, %s', name)
  } else if (hour >= 18 && hour <= 23) {
    return gt('Good evening, %s', name)
  }
  return gt('Hello %s', name)
}

// portal header
ext.point('io.ox/portal/sections').extend({
  id: 'header',
  index: 100,
  draw (baton) {
    let $header
    this.append($header = $('<div class="header flex-row">'))
    // greeting
    const $greeting = $('<h1 class="greeting flex-grow">').append(
      baton.$.greeting = $('<span class="greeting-phrase">'),
      $('<span class="signin ellipsis">').append(
        // #. Portal. Logged in as user
        $('<span>').text(gt('Signed in as')),
        $.txt(' '), userAPI.getTextNode(ox.user_id, { type: 'email' })
      )
    )
    // if (!_.device('smartphone')) {
    // add widgets
    ext.point('io.ox/portal/settings/widgets')
      .filter(point => point && point.id === 'add')
      .map(point => point.invoke('render', $header))
    $header.find('[role="log"]').remove()
    // }
    $header.prepend($greeting)
  }
})

// widget container
ext.point('io.ox/portal/sections').extend({
  id: 'widgets',
  index: 200,
  draw (baton) {
    this.append(
      baton.$.widgets = $('<ol class="widgets">')
    )
  }
})

// widget scaffold
ext.point('io.ox/portal/widget-scaffold').extend({
  id: 'attributes',
  index: 100,
  draw (baton) {
    this.attr({
      'data-widget-cid': baton.model.cid,
      'data-widget-id': baton.model.get('id'),
      'data-widget-type': baton.model.get('type')
    })
  }
})

ext.point('io.ox/portal/widget-scaffold').extend({
  id: 'classes',
  index: 200,
  draw (baton) {
    const isDraggable = (baton.model.get('draggable') ?? true) && !_.device('touch')
    const isProtected = baton.model.get('draggable') === false
    this.addClass('widget')
      .addClass(baton.model.get('inverse') ? 'inverse' : '')
      .addClass(isProtected ? 'protected' : '')
      .addClass(isDraggable ? 'draggable' : '')
      .attr({
        draggable: isDraggable,
        'aria-grabbed': isDraggable || undefined
      })
  }
})

ext.point('io.ox/portal/widget-scaffold').extend({
  id: 'default',
  index: 300,
  draw (baton) {
    this.append(
      // border decoration
      $('<div class="decoration pending">').append(
        $('<h2 class="flex-row">').append(
          // title span
          $('<div class="title truncate flex-grow">').text('\u00A0'),
          // add remove icon
          baton.model.get('protectedWidget')
            ? []
            : createButton({
              href: '#',
              variant: 'none',
              icon: {
                name: 'bi/x-lg.svg',
                title: gt('Disable widget')
              }
            }).addClass('disable-widget')
        )
      )
    )
  }
})

// application object
const app = ox.ui.createApp({ name: 'io.ox/portal', id: 'io.ox/portal', title: 'Portal' })
let win
const appBaton = ext.Baton({ app })
const collection = widgets.getCollection()

app.settings = settings

collection
  .on('remove', function (model) {
    // remove DOM node
    appBaton.$.widgets.find(`[data-widget-cid="${CSS.escape(model.cid)}"]`).remove()
    // clean up
    if (model.has('baton')) {
      delete model.get('baton').model
      model.set('baton', null, { validate: true })
      model.isDeleted = true
    }
    widgets.save(appBaton.$.widgets)
  })
  .on('add', function (model) {
    app.drawScaffold(model, true)
    // See Bugs: 47816 / 47230
    if (ox.ui.App.getCurrentApp().get('name') === 'io.ox/portal') {
      const widgetSettingsView = new WidgetSettingsView({ model, collection: model.collection })
      ext.point('io.ox/portal/widget/' + model.get('type') + '/settings')
        .invoke('edit', this, model, widgetSettingsView)
    }
    if (model.has('candidate') !== true) {
      app.drawWidget(model)
      widgets.save(appBaton.$.widgets)
    }
  })
  .on('change', function (model, e) {
    if ('enabled' in model.changed) {
      if (model.get('enabled')) {
        app.getWidgetNode(model).show()
        app.drawWidget(model)
      } else {
        app.getWidgetNode(model).hide()
      }
    } else if ('title' in model.changed) {
      // change name
      app.getWidgetNode(model).find('.title').text(model.get('title'))
    } else if (this.wasElementDeleted(model)) {
      // element was removed, no need to refresh it.

    } else if ('unset' in e && 'candidate' in model.changed && model.drawn) {
      // redraw fresh widget
      app.refreshWidget(model)
    } else if ('props' in model.changed && model.drawn) {
      // redraw existing widget due to config change
      app.refreshWidget(model)
    } else {
      app.drawWidget(model)
    }
  })
  .on('sort', function () {
    // loop over collection for resorting DOM tree
    this.each(function (model) {
      // just re-append all in proper order
      appBaton.$.widgets.append(app.getWidgetNode(model))
    })
  })

collection.wasElementDeleted = function (model) {
  const needle = model.cid
  const haystack = this.models
  return !_(haystack).some(function (suspiciousHay) { return suspiciousHay.cid === needle })
}

if (_.device('smartphone')) app.on('launch resume', model => $('#io-ox-appcontrol').hide())

app.getWidgetCollection = () => collection

app.updateTitle = function () {
  userAPI.getGreeting(ox.user_id).then(function (name) {
    appBaton.$.greeting.text(getGreetingPhrase(name))
  })
}

function openDetailPopup (e) {
  // get widget node
  const node = $(e.currentTarget).closest('.widget')
  // get widget cid
  const cid = node.attr('data-widget-cid')
  // get model
  const model = collection.get(cid)
  if (!model) return
  const baton = model.get('baton')
  baton.item = $(e.currentTarget).data('item')

  // defer to get visual feedback first (e.g. script errors)
  _.defer(function () {
    const popup = new DetailPopup()
    popup.snap(e)
    ext.point('io.ox/portal/widget/' + model.get('type')).invoke('draw', popup.$body, model.get('baton'))
    popup.show()
  })
}

app.drawScaffold = function (model, add) {
  add = add || false
  const baton = ext.Baton({ model, app, add })
  const node = $('<li tabindex="0">')

  model.node = node
  ext.point('io.ox/portal/widget-scaffold').invoke('draw', node, baton)

  if (model.get('enabled') === false) {
    node.hide()
  } else if (!widgets.visible(model.get('type'))) {
    // hide due to missing capabilities
    node.hide()
    return
  }
  if (add) {
    const lastProtected = appBaton.$.widgets.find('li.protected').last()
    if (lastProtected.length) {
      lastProtected.after(node)
    } else {
      appBaton.$.widgets.prepend(node)
    }
  } else {
    appBaton.$.widgets.append(node)
  }
}

app.getWidgetNode = function (model) {
  return appBaton.$.widgets.find(`[data-widget-cid="${CSS.escape(model.cid)}"]`)
}

function setup (e) {
  e.preventDefault()
  const baton = e.data.baton
  ext.point(baton.point).invoke('performSetUp', null, baton)
}

app.drawFirstVisitPane = function (type, baton) {
  const greeting = firstVisitGreeting[type] || ''
  const link = settings.get('settings/getStartedLink') || '#'

  baton.model.node.append(
    $('<div class="content">').append(
      // text
      $('<div>').append(
        $('<span class="bold">').text(ox.serverConfig.productName + '. '),
        $.txt(greeting + '.')
      ),
      // link
      $('<a href="#" target="_blank" style="white-space: nowrap;" role="button">')
        .attr('href', link)
        .text(gt('Get started here') + '!')
    )
  )
}

app.drawDefaultSetup = function (baton) {
  const data = baton.model.toJSON()
  const point = ext.point(baton.point)
  const title = widgets.getTitle(data, point.prop('title'))
  const node = baton.model.node

  node.append(
    $('<div class="content">').append(
      // text
      $('<div class="paragraph" style="min-height: 40px">').append(
        $.txt(
          // #. %1$s is social media name, e.g. Facebook
          gt('Welcome to %1$s', title) + '!'
        ),
        $('<br>'),
        $.txt(
          gt('Get started here') + ':'
        )
      ),
      // button
      $('<a href="#" class="action" role="button">').text(
        // #. %1$s is social media name, e.g. Facebook
        gt('Add your %1$s account', title)
      )
        .on('click', { baton }, setup)
    )
  )

  point.invoke('drawDefaultSetup', node, baton)
}

async function ensureDeferreds (ret) {
  return ret
}

function reduceBool (memo, bool) {
  return memo || bool
}

function runAction (e) {
  ext.point(e.data.baton.point).invoke('action', $(this).closest('.widget'), e.data.baton)
}

async function loadAndPreview (point, node, baton) {
  const decoration = node.find('.decoration')
  try {
    await Promise.all(point.invoke('load', node, baton).map(ensureDeferreds).value())
  } catch (e) {
    function getTrustOption () {
      const solutionContainer = $('<div>').append(
        $('<a class="solution">').text(gt('Inspect certificate')).on('click', async function () {
          const certUtils = await import('@/io.ox/settings/security/certificates/settings/utils')
          certUtils.openExamineDialog(e)
        })
      )

      setTimeout(function () {
        solutionContainer.find('.solution').off('click')
          .empty()
          .append(
            $('<a class="solution">').text(gt('Try again.')).on('click', function () {
              node.find('.decoration').addClass('pending')
              loadAndPreview(point, node, baton)
            })
          )
      }, 60000)

      return solutionContainer
    }
    const showCertManager = !coreSettings.get('security/acceptUntrustedCertificates') && coreSettings.get('security/manageCertificates')
    // special return value?
    if (e === 'remove') {
      widgets.remove(baton.model)
      node.remove()
      throw e
    }
    // clean up
    node.find('.content').remove()
    decoration.removeClass('pending')
    if (e.code === 'ACC-0002') {
      // if an account was removed, disable widget (do not delete it)
      // to avoid further requests
      baton.model.set('enabled', false, { validate: false })
      yell('info', gt('The widget "%s" was disabled as the account might have been removed or is no longer available.', baton.model.get('title')))
    } else if (e.code !== 'OAUTH-0006') {
      // show error message unless it's just missing oauth account
      node.append(
        $('<div class="content error">').append(
          $('<div>').text(gt('An error occurred.')),
          // message
          $('<div class="italic">').text(_.isString(e.error) ? e.error : ''),
          $('<br>'),
          // retry
          e.retry !== false
            ? [
                (!/SSL/.test(e.code))
                  ? $('<a class="solution">').text(gt('Try again.')).on('click', function () {
                    node.find('.decoration').addClass('pending')
                    loadAndPreview(point, node, baton)
                  })
                  : $(), (/SSL/.test(e.code) && showCertManager) ? getTrustOption() : $()]
            : $()

        )
      )
      if (point.prop('stopLoadingOnError')) {
        baton.options.loadingError = true
      }
      point.invoke('error', node, e, baton)
    } else {
      // missing oAuth account
      app.drawDefaultSetup(baton)
    }
    throw e
  }

  baton.options.loadingError = false
  const widgetType = _.last(baton.point.split('/'))
  node.find('.content').remove()

  if (isFirstVisitWidget(widgetType, baton)) {
    app.drawFirstVisitPane(widgetType, baton)
  } else {
    point.invoke('preview', node, baton)
  }
  node.removeClass('error-occurred')
  decoration.removeClass('pending error-occurred')

  // draw summary only on smartphones (please adjust settings pane when adjusting this check)
  function drawSummary () {
    if (settings.get('mobile/summaryView')) {
      node.addClass('with-summary')
      if (point.all()[0].summary) {
        // invoke special summary if there is one
        point.invoke('summary', node, baton)
        node.on('click', 'h2', function (e) {
          $(e.delegateTarget).toggleClass('show-summary')
        })
      } else if (!node.hasClass('generic-summary')) {
        // add generic open close if it's not added yet
        node.addClass('show-summary generic-summary')
        node.on('click', 'h2', function (e) {
          $(e.delegateTarget).toggleClass('show-summary generic-summary')
        })
      }
    } else {
      node.off('click', 'h2')
      node.removeClass('with-summary generic-summary')
    }
  }
  if (_.device('smartphone')) {
    settings.on('change:mobile/summaryView', drawSummary)
    drawSummary()
  }
}

app.drawWidget = async function (model, index) {
  const node = model.node

  if (!model.drawn) {
    if (model.get('enabled') === true && !widgets.visible(model.get('type'))) {
      // hide due to missing capabilities
      node.hide()
      return
    }

    model.drawn = true
    index = index || 0

    // set/update title
    const baton = ext.Baton({ app, model, point: 'io.ox/portal/widget/' + model.get('type') })
    const point = ext.point(baton.point)
    // ensure that plugin settings are loaded
    await Promise.all(point.invoke('initialize', undefined, baton).value())
    const title = widgets.getTitle(model.toJSON(), point.prop('title'))
    const $title = node.find('h2 .title').text(title)
    const requiresSetUp = point.invoke('requiresSetUp').reduce(reduceBool, false).value()
    // remember
    model.set('baton', baton, { validate: true, silent: true })
    node.attr('aria-label', title)
    node.find('a.disable-widget').attr({
      'aria-label': title + ', ' + gt('Disable widget')
    })
    // setup?
    if (requiresSetUp) {
      node.find('.decoration').removeClass('pending')
      app.drawDefaultSetup(baton)
    } else {
      // add link?
      if (point.prop('action') !== undefined) {
        $title.addClass('action-link').css('cursor', 'pointer').on('click', { baton }, runAction)
      }
      // simple delay approach
      _.delay(function () {
        // initialize first
        point.invoke('initialize', node, baton)
        // load & preview
        node.busy()
        loadAndPreview(point, node, baton).then(function () {
          node.removeAttr('style')
        }).finally(function () {
          node.idle()
        })
      }, (index / 2 >> 0) * 100)
    }
  }
}

app.refreshWidget = function (model, index) {
  if (!model.drawn) return

  index = index || 0

  let node = model.node
  const delay = (index / 2 >> 0) * 1000
  let baton = model.get('baton')
  let point = ext.point(baton.point)

  _.defer(function () {
    _.delay(function () {
      node.find('.decoration').addClass('pending')
      // CSS Transition delay 0.3s
      _.delay(function () {
        loadAndPreview(point, node, baton)
        node = baton = point = null
      }, 300)
    }, delay)
  })
}

// can be called every 30 seconds
app.refresh = _.throttle(function () {
  _(widgets.getEnabled()).chain().filter(function (model) {
    // don't refresh widgets with loading errors automatically so logs don't get spammed (see bug 41740)
    // also handle ignoreGlobalRefresh option (See Bug 49562)
    const options = model.attributes.baton && model.attributes.baton.options
    return (!options && !model.get('ignoreGlobalRefresh')) || (!options.loadingError && !model.get('ignoreGlobalRefresh'))
  }).each(app.refreshWidget)
}, 30000)

// mail push, needs extra handling
app.mailWidgetRefresh = function () {
  _(widgets.getEnabled()).chain()
    .filter(function (model) {
      return model.get('type') === 'mail'
    })
    .each(function (model, index) {
      if (model.get('baton')) {
        model.get('baton').collection.expire()
      }
      app.refreshWidget(model, index)
    })
}

ox.on('refresh^', function () {
  app.refresh()
})

ox.on('socket:mail:new', app.mailWidgetRefresh)

app.getContextualHelp = function () {
  return 'ox.appsuite.user.sect.portal.gui.html'
}

// launcher
app.setLauncher(async function () {
  // get window
  app.setWindow(win = ox.ui.createWindow({
    name: 'io.ox/portal',
    chromeless: true
  }))

  win.nodes.main.addClass('io-ox-portal f6-target').attr({
    tabindex: -1,
    'aria-label': gt('Portal widgets')
  })
  await widgets.loadWidgets()
  win.setTitle(gt('Portal'))

  ext.point('io.ox/portal/sections').invoke('draw', win.nodes.main, appBaton)

  app.updateTitle()
  setInterval(app.updateTitle, 360000)
  // change name if username changes
  userAPI.get(ox.user_id).then(function (userData) {
    contactAPI.on('update:' + _.ecid({ folder_id: userData.folder_id, id: userData.contact_id }), app.updateTitle)
  })

  win.show(function () {
    // draw scaffolds now for responsiveness
    collection.each(function (model) {
      app.drawScaffold(model, false)
    })

    widgets.removeDisabled().forEach(app.drawWidget)
    ox.trigger('portal:items:render')

    // add detail popup
    win.nodes.main.on('click', '.item:not([data-detail-popup]), .content.pointer, .action.pointer', _.debounce(openDetailPopup, 100))

    // react on 'remove'
    win.nodes.main.on('click', '.disable-widget', function (e) {
      e.preventDefault()
      const id = $(this).closest('.widget').attr('data-widget-id')
      const model = widgets.getModel(id)
      if (model) {
        // do we have custom data that might be lost?
        if (!_.isEmpty(model.get('props'))) {
          const dialog = new ModalDialog({ title: gt('Delete widget'), description: gt('Do you really want to delete this widget?') })
            .addCancelButton()
            // #. Really delete portal widget - in contrast to "just disable"
            .on('delete', function () { model.collection.remove(model) })
          if (model.get('enabled')) {
            // #. Just disable portal widget - in contrast to delete
            dialog.addButton({ label: gt('Just disable widget'), action: 'disable', placement: 'left', className: 'btn-default' })
              .on('disable', function () { model.set('enabled', false, { validate: true }) })
          }
          dialog.addButton({ label: gt('Delete'), action: 'delete' })
          dialog.open()
        } else {
          model.collection.remove(model)
        }
      }
    })

    let draggedItem
    appBaton.$.widgets.delegate('li', {
      dragstart: function dragstart () {
        draggedItem = $(this).css('opacity', '0.999')
        _.defer(function () {
          if (!draggedItem) return
          draggedItem
            .css('visibility', 'hidden')
            .attr('aria-grabbed', true)
        })

        // find possible drop targets
        draggedItem
          .nextUntil('.protected')
          .attr('aria-dropeffect', 'move')
        draggedItem
          .prevUntil('.protected')
          .attr('aria-dropeffect', 'move')
      },
      dragenter: function dragenter () {
        const target = $(this)
        if (!target.is('[aria-dropeffect="move"]')) return
        if (draggedItem.index() < target.index()) target.after(draggedItem)
        else target.before(draggedItem)
      },
      'dragend drop': function dragend () {
        if (!draggedItem) return
        draggedItem
          .css('visibility', '')
          .attr('aria-grabbed', false)
        appBaton.$.widgets.children().removeAttr('aria-dropeffect')
        draggedItem = null
        widgets.getCollection().trigger('order-changed', 'portal')
        widgets.save(appBaton.$.widgets)
      }
    })
    app.getWindowNode().on('dragover', function (e) {
      if (!draggedItem) return
      e.preventDefault()
      e.originalEvent.dataTransfer.dropEffect = 'move'
    })
  })

  $('html').addClass('complete')
})

export default {
  getApp: app.getInstance
}
