import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import ExtensibleView from '@/io.ox/backbone/views/extensible'
import manifests from '@/io.ox/core/manifests'
import WidgetSettingsView from '@/pe/portal/settings/widgetview'
import upsell from '@/io.ox/core/upsell'
import widgets from '@/pe/portal/widgets'

import { settings } from '@/pe/portal/settings'
import listUtils from '@/io.ox/backbone/mini-views/listutils'
import ListView from '@/io.ox/backbone/mini-views/settings-list-view'
import Dropdown from '@/io.ox/backbone/mini-views/dropdown'
import * as util from '@/io.ox/core/settings/util'
import { createIcon } from '@/io.ox/core/components'
import { st } from '@/io.ox/settings/index'

import '@/pe/portal/style.scss'
import gt from 'gettext'

// we need to import this otherwise portal settings lack strings
// reason is portal/main sharing the "Add widget" button with settings
// ideally this becomes a util function somewhere else
// see ext.point('io.ox/portal/settings/widgets') -> add
import '@/io.ox/settings/strings'

ext.point('io.ox/portal/settings/detail').extend({
  index: 100,
  id: 'view',
  async draw () {
    const view = new ExtensibleView({ point: 'io.ox/portal/settings/detail/view', model: settings })
      .build(function () {
        this.$el.addClass('settings-body io-ox-portal-settings')
        this.listenTo(settings, 'change:mobile/summaryView', function () {
          settings.saveAndYell()
        })
      })

    this.append(
      util.header(
        st.PORTAL,
        'ox.appsuite.user.sect.portal.settings.html'
      ),
      view.$el.busy()
    )

    await widgets.loadWidgets()
    if (!view.disposed) view.render().$el.idle()
  }
})

const collection = widgets.getCollection()
const notificationId = _.uniqueId('notification_')

collection.on('remove', function () {
  repopulateAddButton()
})

ext.point('io.ox/portal/settings/detail/view').extend(
  {
    id: 'widgets',
    index: 100,
    render: util.renderExpandableSection(st.PORTAL_WIDGETS, st.PORTAL_WIDGETS_EXPLANATION, 'io.ox/portal/settings/widgets', true)
  },
  {
    id: 'advanced',
    index: 10000,
    render: util.renderExpandableSection(st.PORTAL_ADVANCED, '', 'io.ox/portal/settings/advanced')
  }
)

ext.point('io.ox/portal/settings/widgets').extend({
  index: 100,
  id: 'add',
  render () {
    const $ul = $('<ul class="dropdown-menu io-ox-portal-settings-dropdown" role="menu">').on('click', 'a:not(.io-ox-action-link)', addWidget)
    const $button = !_.device('smartphone')
      ? [
          // there is is fallback for st.ADD_WIDGET since this particular extension
          // might be called from portal/main directly
          // (so settings/strings not necessarily loaded by intention)
          $('<span>').text(st.ADD_WIDGET || gt('Add widget')),
          createIcon('bi/chevron-down.svg').addClass('bi-12 ms-8')
        ]
      : [
          $('<span class="sr-only">').text(st.ADD_WIDGET || gt('Add widget')),
          createIcon('bi/plus-lg.svg').addClass('bi-18')
        ]
    const $toggle = $('<button type="button" class="btn btn-primary dropdown-toggle add-widget" data-toggle="dropdown" aria-haspopup="true">').append(
      $button
    )

    this.append(
      $('<div class="form-group mb-24">').append(
        new Dropdown({
          className: 'dropdown btn-group-portal',
          $ul,
          $toggle
        }).render().$el,
        $('<div class="sr-only" role="log" aria-live="assertive" aria-relevant="additions text">').attr('id', notificationId)
      )
    )

    repopulateAddButton()
  }
})

function addWidget (e) {
  e.preventDefault()

  const type = $(this).attr('data-type')
  const requires = manifests.manager.getRequirements('pe/portal/plugins/' + type + '/register')

  // upsell check
  if (![].concat(requires).some(upsell.has)) {
    // trigger global upsell event
    upsell.trigger({
      type: 'portal-widget',
      id: type,
      missing: upsell.missing(requires)
    })
  } else {
    // add widget
    widgets.add(type)
    repopulateAddButton()
  }
}

function repopulateAddButton () {
  widgets.loadWidgets().then(async function () {
    $('.io-ox-portal-settings-dropdown').children('[role=presentation]').remove().end().append(
      await Promise.all(_(widgets.getAllTypes()).map(async function (options) {
        const baton = ext.Baton({ point: `io.ox/portal/widget/${options.type}/settings` })
        const point = ext.point(baton.point)
        // ensure that plugin settings are loaded
        await Promise.all(point.invoke('initialize', undefined, baton).value())
        const isUnique = options.unique && widgets.containsType(options.type)
        const isVisible = upsell.visible(options.requires)
        const title = _.isFunction(options.title) ? options.title() : options.title
        if (isUnique || !isVisible) {
          return $()
        }
        return $('<li role="presentation">')
        // add disabled class if requires upsell
          .addClass(!upsell.has(options.requires) ? 'requires-upsell' : undefined)
          .append(
            $('<a href="#" role="menuitem">').attr('data-type', options.type).text(title)
          )
      })
      )
    )
  })
}

ext.point('io.ox/portal/settings/widgets').extend({
  index: 200,
  id: 'list',
  render () {
    this.append(
      new ListView({
        collection,
        sortable: true,
        containment: this,
        notification: this.find('#' + notificationId),
        ChildView: WidgetSettingsView,
        dataIdAttribute: 'data-widget-id',
        childOptions (model) {
          return {
            point: 'pe/portal/widget/' + model.get('type')
          }
        },
        filter (model) {
          const enabledIsChangeable = _.isObject(model.get('changeable')) && model.get('changeable').enabled === true
          const anyChangeable = _.any(model.get('changeable'), function (val) {
            return val === true
          })
          // do not show protected widgets which are disabled and the disabled state can not be changed
          if (model.get('protectedWidget') === true && (model.get('enabled') !== true || enabledIsChangeable)) return false
          // do not show protected widgets which are enabled but don't have any attribute changeable
          if (model.get('protectedWidget') === true && model.get('enabled') === true && !anyChangeable) return false
          return true
        }
      })
        .on('add', function (view) {
          // See Bugs: 47816 / 47230
          if (ox.ui.App.getCurrentApp().get('name') === 'io.ox/portal') return
          view.edit()
        })
        .on('order:changed', function () {
          widgets.getCollection().trigger('order-changed', 'settings')
          widgets.save(this.$el)
        })
        .render().$el
    )
  }
})

ext.point('io.ox/portal/settings/detail/list-item').extend({
  id: 'state',
  index: 100,
  draw (baton) {
    const data = baton.model.toJSON()
    this.toggleClass('disabled', !data.enabled)
  }
})

ext.point('io.ox/portal/settings/detail/list-item').extend({
  id: 'drag-handle',
  index: 200,
  draw (baton) {
    if (_.device('smartphone')) return
    const data = baton.model.toJSON()
    // seems to be added in MW silently..
    // a protectedWidget might be editable. If it is the "index" allow d&d for reorder
    const protectedButDraggable = data.protectedWidget && (data.changeable && data.changeable.index)
    this.addClass(data.protectedWidget && !protectedButDraggable ? 'protected' : ' draggable')
      .append(
        data.protectedWidget && !protectedButDraggable
          ? $('<div class="spacer">')
          : listUtils.dragHandle(gt('Drag to reorder widget'), baton.model.collection.length <= 1 ? 'hidden' : '')
      )
  }
})

ext.point('io.ox/portal/settings/detail/list-item').extend({
  id: 'title',
  index: 400,
  draw (baton) {
    const data = baton.model.toJSON()
    const point = ext.point(baton.view.point)
    const title = widgets.getTitle(data, point.prop('title'))
    this.append(
      listUtils.makeTitle(title)
        .addClass('widget-' + data.type)
        .removeClass('pull-left')
    )
  }
})

ext.point('io.ox/portal/settings/detail/list-item').extend({
  id: 'controls',
  index: 500,
  draw (baton) {
    const data = baton.model.toJSON()
    const point = ext.point(baton.view.point)
    const title = widgets.getTitle(data, point.prop('title'))

    if (data.protectedWidget || data.type === 'upsell') {
      // early exit if protected Widget
      this.append('&nbsp;')
      return
    }

    const $controls = listUtils.makeControls()
    const $link = listUtils.controlsToggle()

    if (data.enabled) {
      // editable?
      if (baton.view.options.editable) {
        $controls.append(
          listUtils.appendIconText(
            listUtils.controlsEdit({ ariaLabel: gt('Edit %1$s', title) }),
            gt('Edit'),
            'edit'
          )
        )
      }
      if (!_.device('smartphone')) {
        $controls.append(
          listUtils.appendIconText($link.attr({
            'aria-label': gt('Disable %1$s', title)
          }), gt('Disable'), 'disable')
        )
      }
    } else {
      $controls.append(
        listUtils.appendIconText($link.attr({
          'aria-label': gt('Enable %1$s', title)
        }), gt('Enable'), 'enable')
      )
    }

    $controls.append(
      listUtils.controlsDelete({ ariaLabel: gt('Delete %1$s', title) })
    )

    this.append($controls)
  }
})

ext.point('io.ox/portal/settings/advanced').extend({
  index: 100,
  id: 'smartphone',
  render () {
    this.append(
      $('<div class="form-group">').append(
        util.checkbox('mobile/summaryView', st.REDUCE_TO_SUMMARY, settings)
      )
    )
  }
})

export default {}
