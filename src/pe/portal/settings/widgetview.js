import _ from '@/underscore'
import ext from '@/io.ox/core/extensions'
import ModalDialog from '@/io.ox/backbone/views/modal'
import manifests from '@/io.ox/core/manifests'
import upsell from '@/io.ox/core/upsell'

import DisposableView from '@/io.ox/backbone/views/disposable'
import '@/pe/portal/style.scss'
import gt from 'gettext'

const WidgetSettingsView = DisposableView.extend({

  tagName: 'li',

  className: 'settings-list-item items-center',

  events: {
    'click [data-action="edit"]': 'onEdit',
    'click [data-action="toggle"]': 'onToggle',
    'click [data-action="delete"]': 'onDelete'
  },

  initialize () {
    this.$el.attr('data-widget-id', this.model.get('id'))
    // get explicit state
    const enabled = this.model.get('enabled')
    this.model.set('enabled', !!(enabled === undefined || enabled === true), { validate: true })
    // get widget options
    this.point = 'io.ox/portal/widget/' + this.model.get('type')
    this.options = ext.point(this.point + '/settings').options()
  },

  render () {
    const baton = ext.Baton({ model: this.model, view: this })

    if (this.disposed) {
      return this
    }
    ext.point('io.ox/portal/settings/detail/list-item').invoke('draw', this.$el.empty(), baton)
    return this
  },

  edit () {
    if (_.isFunction(this.options.edit)) {
      this.options.edit(this.model, this)
    }
  },

  onEdit (e) {
    e.preventDefault()
    this.edit()
  },

  onToggle (e) {
    e.preventDefault()

    const enabled = this.model.get('enabled')
    const type = this.model.get('type')
    const requires = manifests.manager.getRequirements('pe/portal/plugins/' + type + '/register')

    // upsell check
    if (!enabled && ![].concat(requires).some(upsell.has)) {
      // trigger global upsell event
      upsell.trigger({
        type: 'portal-widget',
        id: type,
        missing: upsell.missing(requires)
      })
    } else {
      // toggle widget
      this.model.set('enabled', !enabled, { validate: true })
      this.render()
    }
  },

  removeWidget () {
    this.model.collection.remove(this.model)
  },

  onDelete (e) {
    e.preventDefault()
    const self = this
    // do we have custom data that might be lost?
    if (!_.isEmpty(this.model.get('props'))) {
      const dialog = new ModalDialog({ title: gt('Delete widget'), description: gt('Do you really want to delete this widget?') })
        .addCancelButton()
        .on('delete', function () { self.removeWidget() })
      if (this.model.get('enabled')) {
        // #. Just disable portal widget - in contrast to delete
        dialog.on('disable', function () { self.onToggle(e) })
          .addButton({ label: gt('Just disable widget'), action: 'disable', placement: 'left', className: 'btn-default' })
      }
      // #. Really delete portal widget - in contrast to "just disable"
      dialog.addButton({ label: gt('Delete'), action: 'delete' })
      dialog.open()
    } else {
      this.removeWidget()
    }
  }
})

export default WidgetSettingsView
