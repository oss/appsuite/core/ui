import ui from '@/io.ox/core/desktop'
import icons from '@/io.ox/core/main/icons'
import manifests from '@/io.ox/core/manifests'
import gt from 'gettext'

// Portal
ui.createApp({
  id: 'io.ox/portal',
  name: 'io.ox/portal',
  path: 'pe/portal/main',
  title: gt.pgettext('app', 'Portal'),
  requires: 'portal',
  refreshable: true,
  settings: () => import('@/pe/portal/settings/pane.js'),
  icon: icons['io.ox/portal'],
  load: () => manifests.manager.loadPluginsFor('io.ox/portal').then(() => import('@/pe/portal/main'))
})
