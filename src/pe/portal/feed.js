import $ from '@/jquery'
import _ from '@/underscore'

function Feed (options) {
  this.options = _.extend({}, options || {})
}

Feed.prototype.load = function (count, offset) {
  let callback = 'feed_callback_' + _.now(); let url; const self = this

  if (count) { callback += '_' + count }
  if (offset) { callback += '_' + offset }

  url = this.options.url + callback

  if (offset || count) {
    url = this.appendLimitOffset(url, count, offset)
  }

  return $.ajax({
    url,
    dataType: 'jsonp',
    jsonp: false,
    jsonpCallback: callback
  })
    .then(function (data) {
      return self.process(data)
    })
}

Feed.prototype.process = function (response) {
  return response
}

Feed.prototype.appendLimitOffset = function (url) {
  return url
}

export default Feed
