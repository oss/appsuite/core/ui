import { Settings } from '@/io.ox/core/settings'

export const defaults = {
  widgets: {
  },
  mobile: {
    summaryView: false
  }
}

export const settings = new Settings('io.ox/portal', () => defaults)
