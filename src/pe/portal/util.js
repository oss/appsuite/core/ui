import _ from '@/underscore'
import { settings } from '@/pe/portal/settings'

// this util class was introduced to avoid using portal/widgets.js
// because this would load *all* portal plugins

export const getWidgets = function () {
  return _(settings.get('widgets/user', {})).map(function (obj) {
    // make sure we always have props
    obj.props = obj.props || {}
    return obj
  })
}

export const getWidgetsByType = function (type) {
  return _(getWidgets()).filter(function (obj) {
    return obj.type === type
  })
}
