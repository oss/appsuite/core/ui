// cSpell:ignore Speicherplatz

import _ from '@/underscore'
import ext from '@/io.ox/core/extensions'
import QuotaView from '@/pe/backbone/mini-views/quota'
import folderAPI from '@/io.ox/core/folder/api'
import filesAPI from '@/io.ox/files/api'
import mailAPI from '@/io.ox/mail/api'

import { settings as coreSettings } from '@/io.ox/core/settings'
import gt from 'gettext'

ext.point('io.ox/files/mediator').extend({
  id: 'files-quota',
  setup (app) {
    if (_.device('smartphone')) return

    const quota = new QuotaView({
      // #. Using "Storage" as a less technical term than "Quota"
      // #. In German "Speicherplatz", for example
      title: coreSettings.get('quotaMode', 'default') === 'unified' ? gt('Storage') : gt('File storage'),
      renderUnlimited: false,
      module: 'file',
      upsell: {
        title: gt('Need more space?'),
        requires: 'boxcom || google || microsoftgraph',
        id: 'files-folderview-quota',
        icon: ''
      },
      upsellLimit: 5 * 1024 * 1024 // default upsell limit of 5 mb
    })
    // add some listeners
    folderAPI.on('clear', function () {
      quota.getQuota(true)
    })

    filesAPI.on('add:file remove:file add:version remove:version', function () {
      quota.getQuota(true)
    })

    filesAPI.on('copy', function () {
      quota.getQuota(true)
    })

    app.treeView.$el.append(
      quota.render().$el
    )
  }
})

ext.point('io.ox/mail/mediator').extend({
  id: 'mail-quota',
  setup (app) {
    if (_.device('smartphone')) return

    const quotaView = new QuotaView({
      // #. Quota means a general quota for mail and files
      title: coreSettings.get('quotaMode', 'default') === 'unified' ? gt('Quota') : gt('Mail quota'),
      renderUnlimited: false,
      upsell: {
        title: gt('Need more space?'),
        requires: 'infostore',
        id: 'mail-folderview-quota',
        icon: ''
      },
      upsellLimit: 5 * 1024 * 1024 // default upsell limit of 5 mb
    })

    const countQuotaView = new QuotaView({
      title: gt('Mail count quota'),
      quotaField: 'countquota',
      usageField: 'countuse',
      renderUnlimited: false,
      renderThreshold: 0.9,
      name: 'mailcount',
      sizeFunction: undefined
    })

    // add some listeners
    folderAPI.on('cleared-trash', function () {
      // this will update countQuota as well
      quotaView.getQuota(true)
    })

    mailAPI.on('deleted-mails-from-trash', function () {
      // this will update countQuota as well
      quotaView.getQuota(true)
    })

    mailAPI.on('refresh.all', function () {
      // this will update countQuota as well
      quotaView.getQuota(true)
    })

    app.treeView.$el.append(
      quotaView.render().$el,
      countQuotaView.render().$el
    )
  }
})
