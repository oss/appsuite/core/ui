// cSpell:ignore Schnelleinwahl

import moment from '@/moment'
import $ from '@/jquery'
import _ from '@/underscore'

import ox from '@/ox'
import { ZoomView } from '@/pe/zoom/view/zoom'
import { getConference } from '@/io.ox/conference/util'
import zoomApi from '@/pe/zoom/api'
import switchboardApi from '@/io.ox/switchboard/api'
import ext from '@/io.ox/core/extensions'
import { createIcon } from '@/io.ox/core/components'

import { settings as zoomSettings } from '@/io.ox/conference/zoom-settings'
import gt from 'gettext'

import { longerThanOneYear } from '@/io.ox/conference/views/util'

const filterCountry = zoomSettings.get('dialin/filterCountry', '')

const ZoomMeetingView = ZoomView.extend({

  className: 'conference-view zoom',

  events: {
    'click [data-action="copy-to-location"]': 'copyToLocationHandler',
    'click [data-action="copy-to-description"]': 'copyToDescriptionHandler',
    'click [data-action="copy-to-clipboard"]': 'copyToClipboard',
    'click [data-action="recreate"]': 'recreateMeeting'
  },

  initialize (options) {
    this.appointment = options.appointment
    const conference = getConference(this.appointment.get('conferences'))
    this.model.set('joinURL', conference && conference.type === 'zoom' ? conference.joinURL : '')
    this.listenTo(this.appointment, 'change:rrule', this.onChangeRecurrence)
    this.listenTo(this.appointment, 'create update', this.changeMeeting)
    this.listenTo(this.appointment, 'discard', this.discardMeeting)
    this.on('dispose', this.discardMeeting)
    window.zoomMeeting = this
  },

  getExtendedProps () {
    return this.appointment.get('extendedProperties') || {}
  },

  renderDone () {
    this.renderPoint('done')
    // auto copy
    this.autoCopyToLocation()
    this.autoCopyToDescription()
    this.onChangeRecurrence()
  },

  copyToClipboard () {
    navigator.clipboard.writeText(this.getJoinURL())
  },

  copyToLocationHandler (e) {
    e.preventDefault()
    this.copyToLocation()
  },

  autoCopyToLocation () {
    if (!zoomSettings.get('autoCopyToLocation')) return
    if (this.appointment.get('location')) return
    this.copyToLocation()
  },

  copyToLocation () {
    // just add the plain link without a text prefix. Gmail will not
    // recognize the link if there is text prefix on th link
    this.appointment.set('location', this.getJoinURL())
  },

  copyToDescriptionHandler (e) {
    e.preventDefault()
    this.copyToDescription()
  },

  autoCopyToDescription () {
    if (!zoomSettings.get('autoCopyToDescription')) return
    if (this.appointment.get('description')) return
    this.copyToDescription()
  },

  copyToDescription () {
    let dialInInfo = getDialInInfo(this.model.get('meeting'))
    const existingDescription = this.appointment.get('description')
    if (existingDescription) dialInInfo = dialInInfo + '\n' + existingDescription
    this.appointment.set('description', dialInInfo)
  },

  isDone () {
    return this.getJoinURL() && this.model.get('meeting')
  },

  createMeeting () {
    // load or create meeting?
    if (this.getJoinURL()) return this.getMeeting()
    const data = this.appointment.toJSON()
    return zoomApi.createMeeting(translateMeetingData(data)).then(
      function success (result) {
        if (ox.debug) console.debug('createMeeting', result)
        this.appointment.set('conferences', [{
          uri: result.join_url,
          feature: 'VIDEO',
          label: gt('Zoom Meeting'),
          extendedParameters: {
            'X-OX-TYPE': 'zoom',
            'X-OX-ID': result.id,
            'X-OX-OWNER': switchboardApi.userId
          }
        }])
        this.model.set({ joinURL: result.join_url, meeting: result, state: 'done', created: true })
      }.bind(this),
      this.createMeetingFailed.bind(this)
    )
  },

  recreateMeeting () {
    this.model.set('joinURL', '')
    this.createMeeting()
  },

  getMeeting () {
    const url = this.getJoinURL()
    // careful here, url can be with or without password, depending on the setting. Make sure this works correctly in both cases
    const id = String(url).replace(/^.+\/j\/(\w+).*$/, '$1')
    zoomApi.getMeeting(id).then(
      function success (result) {
        if (ox.debug) console.debug('getMeeting', result)
        this.model.set({ meeting: result, state: 'done' })
      }.bind(this),
      this.createMeetingFailed.bind(this)
    )
  },

  changeMeeting () {
    const meeting = this.model.get('meeting')
    if (!meeting) return
    const data = this.appointment.toJSON()
    // This appointment is an exception of a series - do not change the zoom meeting
    if (data.seriesId && (data.seriesId !== data.id)) return
    // This appointment changed to an exception of a series - do not change the zoom meeting
    if (data.seriesId && (data.seriesId === data.id) && !data.rrule) return
    const changes = translateMeetingData(data)
    zoomApi.changeMeeting(meeting.id, changes)
    this.off('dispose', this.discardMeeting)
  },

  discardMeeting () {
    const meeting = this.model.get('meeting')
    const description = this.appointment.get('description')
    if (description) {
      const newDescripion = description.replace(getDialInInfo(meeting), '')
      if (description === newDescripion) {
        this.appointment.trigger('warnOldDialInInfo')
      } else {
        this.appointment.set('description', newDescripion.trim())
      }
    }
    if (!this.model.get('created')) return
    zoomApi.deleteMeeting(meeting.id)
    this.off('dispose', this.discardMeeting)
  },

  onChangeRecurrence () {
    if (!this.isDone()) return
    const rrule = this.appointment.get('rrule')
    this.$('.recurrence-warning').toggleClass('hidden', !longerThanOneYear(rrule))
  }
})

const points = {
  auth: {
    icon () {
      this.$el.append(
        createIcon('bi/exclamation.svg').addClass('conference-logo')
      )
    },
    hint () {
      this.$el.append(
        $('<p>').text(
          gt('You first need to connect %1$s with Zoom. To do so, you need a Zoom Account. If you don\'t have an account yet, it is sufficient to create a free one.', ox.serverConfig.productName)
        )
      )
    },
    button () {
      this.$el.append(
        $('<p>').append(
          $('<button type="button" class="btn btn-default" data-action="start-oauth">')
            .text(gt('Connect with Zoom'))
        )
      )
    }
  },
  pending: {
    default () {
      this.$el.append(
        $('<div class="pending">').append(
          $('<div class="conference-logo">').append(
            createIcon('bi/camera-video.svg')
          ),
          // #. Present progressive, indicating an ongoing or current action.
          $.txt(gt('Connecting to Zoom…')),
          createIcon('bi/arrow-clockwise.svg').addClass('animate-spin')
        )
      )
    }
  },
  done: {
    icon () {
      this.$el.append(
        $('<div class="conference-logo">').append(
          createIcon('bi/camera-video.svg')
        )
      )
    },
    link () {
      const url = this.getJoinURL() || 'https://...'
      this.$el.append(
        $('<div class="ellipsis">').append(
          $('<b>').text(gt('Link:')),
          $.txt(' '),
          $('<a target="_blank" rel="noopener">').attr('href', url).text(gt.noI18n(url))
        )
      )
    },
    actions () {
      this.$el.append(
        $('<div>').append(
          $('<a href="#" class="secondary-action" data-action="copy-to-location">')
            // #. Copy the meeting link into the appointment's location field
            .text(gt('Copy link to location')),
          $('<a href="#" class="secondary-action" data-action="copy-to-clipboard">')
            .text(gt('Copy link to clipboard')).attr('data-clipboard-text', this.getJoinURL()),
          $('<a href="#" class="secondary-action" data-action="copy-to-description">')
            .text(gt('Copy dial-in information to description'))
        )
      )
    },
    warning () {
      this.$el.append(
        $('<div class="alert alert-info hidden recurrence-warning">').text(
          gt('Zoom meetings expire after 365 days. We recommend to limit the series to one year. Alternatively, you can update the series before the Zoom meeting expires.')
        )
      )
    }
  },
  error: {
    load () {
      if (!this.getJoinURL()) return
      this.model.set('error', gt('A problem occurred while loading the Zoom meeting. Maybe the Zoom meeting has expired.'))
    },
    icon () {
      this.$el.append(
        createIcon('bi/exclamation.svg').addClass('conference-logo')
      )
    },
    message () {
      this.$el.append(
        $('<p class="alert alert-warning message">').append(
          $.txt(this.model.get('error') || gt('Something went wrong. Please try again.'))
        )
      )
    },
    recreate () {
      if (!this.getJoinURL()) return
      this.$el.append(
        $('<button type="button" class="btn btn-default" data-action="recreate">')
          .text(gt('Create new Zoom meeting'))
      )
    }
  },
  offline: {
    icon () {
      this.$el.append(
        createIcon('bi/exclamation.svg').addClass('conference-logo')
      )
    },
    default () {
      this.$el.append(
        $('<p class="alert alert-warning message">').append(
          gt('The Zoom integration service is currently unavailable. Please try again later.')
        )
      )
    }
  }
}

_(points).each(function (point, id) {
  let index = 0
  ext.point('io.ox/switchboard/views/zoom-meeting/' + id).extend(
    _(point).map(function (fn, id) {
      return { id, index: (index += 100), render: fn }
    })
  )
})

function translateMeetingData (data) {
  const isRecurring = !!data.rrule
  const timezone = getTimezone(data.startDate)
  // we always set time (to be safe)
  let start = data.startDate.value
  if (start.indexOf('T') === -1) start += 'T000000'
  let end = data.endDate.value
  if (end.indexOf('T') === -1) end += 'T000000'
  return {
    agenda: data.description,
    // get duration in minutes
    duration: moment(end).diff(start, 'minutes'),
    start_time: moment(start).format('YYYY-MM-DD[T]HH:mm:ss'),
    timezone,
    topic: data.summary || gt('New meeting'),
    // type=2 (scheduled) if no recurrence pattern
    // type=3 (recurring with no fixed time) otherwise
    type: isRecurring ? 3 : 2
  }
}

export function getDialInInfo (meeting) {
  let meetingId; let passcode; let onetap; let description
  if (!meeting || !meeting.settings) return
  const dialinNumbers = _(meeting.settings.global_dial_in_numbers).filter(function (dialin) {
    if (!filterCountry) return true
    return filterCountry === dialin.country
  })
  description = gt('Join Zoom meeting') + ': ' + meeting.join_url + '\n'
  if (meeting.password) {
    // #. %1$s contains a password
    description += gt('Meeting password: %1$s', meeting.password) + '\n'
  }
  if (dialinNumbers.length) {
    meetingId = String(meeting.id).replace(/^(\d{3})(\d{4})(\d+)$/, '$1 $2 $3')
    passcode = meeting.h323_password
    onetap = dialinNumbers[0].number + ',,' + meeting.id + '#,,,,,,0#' + (passcode ? ',,' + passcode + '#' : '')
    description += '\n' +
      // #. Zoom offers a special number to automatically provide the meeting ID and passcode
      // #. German: "Schnelleinwahl mobil"
      // #. %1$s is the country, %2$s contains the number
      gt('One tap mobile (%1$s): %2$s', dialinNumbers[0].country_name, onetap) + '\n\n' +
      // #. %1$s contains a numeric zoom meeting ID
      gt('Meeting-ID: %1$s', meetingId) + '\n' +
      // #. %1$s contains a numeric dialin passcode
      (passcode ? gt('Dial-in passcode: %1$d', passcode) + '\n' : '') +
      '\n' +
      gt('Dial by your location') + '\n' +
      dialinNumbers.map(function (dialin) {
        return '    ' + dialin.country_name + (dialin.city ? ' (' + dialin.city + ')' : '') + ': ' + dialin.number
      })
        .join('\n') + '\n'
  }

  return description
}

function getTimezone (date) {
  return date.timezone || (moment.defaultZone && moment.defaultZone.name) || 'utc'
}

export default ZoomMeetingView
