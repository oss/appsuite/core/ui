import Backbone from '@/backbone'
import $ from '@/jquery'

import ox from '@/ox'
import zoomApi from '@/pe/zoom/api'
import DisposableView from '@/io.ox/backbone/views/disposable'
import ext from '@/io.ox/core/extensions'

export const ZoomView = DisposableView.extend({

  constructor () {
    this.model = new Backbone.Model({ type: 'zoom', state: 'authorized', joinURL: '' })
    // the original constructor will call initialize()
    DisposableView.prototype.constructor.apply(this, arguments)
    // set initial state (now; otherwise we get into cyclic deps)
    this.model.set('state', this.getInitialState())
    this.listenTo(this.model, 'change:state', this.onStateChange)
    this.listenTo(ox, 'zoom:tokens:added', function () {
      if (this.model.get('state') !== 'unauthorized') return
      this.setState('authorized')
    })
    this.$el.on('click', '[data-action="start-oauth"]', $.proxy(zoomApi.startOAuthHandshake, zoomApi))
  },

  render () {
    this.onStateChange()
    return this
  },

  getInitialState () {
    if (!this.isDone()) return 'authorized'
    return 'done'
  },

  setState (state) {
    if (this.disposed) return
    this.model.set('state', state)
  },

  isDone () {
    return !!this.getJoinURL()
  },

  getJoinURL () {
    return this.model && this.model.get('joinURL')
  },

  onStateChange () {
    this.$el.empty().removeClass('error')
    switch (this.model.get('state')) {
      case 'unauthorized':
        this.renderAuthRequired()
        break
      case 'authorized':
        this.renderPending()
        this.createMeeting()
        break
      case 'done':
        this.renderDone()
        break
      case 'offline':
        this.renderOffline()
        break
      case 'error':
        this.renderError()
        this.model.unset('error')
        break
      // no default
    }
  },

  renderPoint (suffix) {
    ext.point('io.ox/switchboard/views/zoom-meeting/' + suffix).invoke('render', this, new ext.Baton())
  },

  // no OAuth token yet
  renderAuthRequired () {
    this.renderPoint('auth')
  },

  // shown while talking to the API
  renderPending () {
    this.renderPoint('pending')
  },

  renderError () {
    this.renderPoint('error')
  },

  renderOffline () {
    this.renderPoint('offline')
  },

  renderDone () {
    this.renderPoint('done')
  },

  createMeeting () {
    return $.when()
  },

  createMeetingFailed (error) {
    if (this.disposed) return
    if (error.status === 401) {
      // 401 equals no or invalid tokens
      this.model.set('state', 'unauthorized')
    } else {
      this.model.set('error', error.message)
      this.model.set('state', 'error')
    }
  }
})
