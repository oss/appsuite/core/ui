// Load conference view
import ext from '@/io.ox/core/extensions'
import $ from '@/jquery'
import gt from 'gettext'
import ZoomMeetingView from '@/pe/zoom/view/zoom-meeting'
import switchboardApi from '@/io.ox/switchboard/api'
import { getGabId } from '@/io.ox/contacts/util'
import * as actionsUtil from '@/io.ox/backbone/views/actions/util'
import { settings as zoomSettings } from '@/io.ox/conference/zoom-settings'

import zoomApi from '@/pe/zoom/api'
import ox from '@/ox'

export { getDialInInfo } from '@/pe/zoom/view/zoom-meeting'

(async () => {
  window.addEventListener('message', function (message) {
    if (message.origin.replace(/^https?:\/\//, '') !== zoomSettings.get('host')) return
    if (message.data === 'zoom:tokens:added') ox.trigger('zoom:tokens:added')
  })
})()

function isGAB (baton) {
  return baton.array().every(function (data) {
    return String(data.folder_id) === getGabId() && data.email1
  })
}

function isMyself (email) {
  return email === switchboardApi.userId
}

ext.point('io.ox/calendar/conference-solutions').extend({
  id: 'zoom',
  index: 200,
  value: 'zoom',
  label: gt('Zoom Meeting'),
  render (view) {
    this.append(
      new ZoomMeetingView({ appointment: view.appointment }).render().$el
    )
  }
})

ext.point('io.ox/contacts/detail/actions/call').extend({
  id: 'zoom',
  index: 100,
  draw (baton) {
    if (!switchboardApi.isOnline() || !isGAB(baton)) return
    const disabled = isMyself(baton.data.email1)
    this.append(
      $('<li role="presentation">').append(
        $('<a href="#">').text(gt('Call via Zoom'))
          .toggleClass('disabled', disabled)
          .on('click', baton.data, e => {
            e.preventDefault()
            if (!disabled) actionsUtil.invoke('io.ox/switchboard/call-user', ext.Baton({ type: 'zoom', data: [e.data] }))
          })
      ))
  }
})

export function getMeeting (id) {
  return zoomApi.getMeeting(id)
}
