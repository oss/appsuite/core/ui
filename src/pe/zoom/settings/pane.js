import $ from '@/jquery'

import '@/pe/zoom/settings/style.scss'

import ox from '@/ox'
import ext from '@/io.ox/core/extensions'
import * as util from '@/io.ox/core/settings/util'
import capabilities from '@/io.ox/core/capabilities'
import DisposableView from '@/io.ox/backbone/views/disposable'
import zoomApi from '@/pe/zoom/api'
import * as contactsUtil from '@/io.ox/contacts/util'
import { createIcon } from '@/io.ox/core/components'
import { setConfigurable, bulkAdd, st, isConfigurable, addExplanations } from '@/io.ox/settings/index'

import { settings as zoomSettings } from '@/io.ox/conference/zoom-settings'

import gt from 'gettext'

const switchboardSettings = ext.point('io.ox/switchboard/state').get('settings').instance()
const zoomAndCalendar = capabilities.has('calendar')

setConfigurable({
  ZOOM_CALENDAR: zoomAndCalendar,
  ZOOM_ADD_PASSWORD: zoomAndCalendar,
  ZOOM_COPY_LINK_LOCATION: zoomAndCalendar,
  ZOOM_COPY_TO_DESCRIPTION: zoomAndCalendar
})

bulkAdd(st.NOTIFICATIONS, '', {
  NOTIFICATIONS_ZOOM: [gt('Zoom'), 'io.ox/notifications [data-section="io.ox/notifications/settings/zoom"]']
})

bulkAdd(st.NOTIFICATIONS, st.NOTIFICATIONS_ZOOM, {
  NOTIFICATIONS_ZOOM_DESKTOP: [gt('Show desktop notifications'), 'io.ox/notifications [name="call/showNativeNotifications"]'],
  NOTIFICATIONS_ZOOM_USE_RINGTONE: [gt('Play ringtone'), 'io.ox/notifications [name="call/useRingtones"]']
})

bulkAdd(st.CALENDAR, '', {
  CALENDAR_ZOOM_MEETINGS: [gt('Zoom meetings'), 'io.ox/calendar [data-section="io.ox/calendar/settings/zoom"]']
})

bulkAdd(st.CALENDAR, st.CALENDAR_ZOOM_MEETINGS, {
  ZOOM_ADD_PASSWORD: [gt('Always add a random meeting password'), 'io.ox/calendar [name="addMeetingPassword"]'],
  ZOOM_COPY_LINK_LOCATION: [gt('Automatically copy the meeting join link to the appointment location field'), 'io.ox/calendar [name="autoCopyToLocation"]'],
  ZOOM_COPY_TO_DESCRIPTION: [gt('Automatically copy comprehensive dial-in information to the appointment description field'), 'io.ox/calendar [name="autoCopyToDescription"]']
})

addExplanations({
  NOTIFICATIONS_ZOOM: gt('Notifications for incoming calls'),
  CALENDAR_ZOOM_MEETINGS: gt('Set up and configure Zoom meetings')
})

//
// Notifications
//

ext.point('io.ox/settings/notifications').extend(
  {
    id: 'zoom',
    index: 1000,
    render (baton) {
      baton.view.listenTo(switchboardSettings, 'change', () => switchboardSettings.saveAndYell())
      return util.renderExpandableSection(st.NOTIFICATIONS_ZOOM, st.NOTIFICATIONS_ZOOM_EXPLANATION, 'io.ox/settings/notifications/zoom').call(this, baton)
    }
  }
)

ext.point('io.ox/settings/notifications/zoom').extend(
  {
    id: 'calls',
    index: 100,
    render () {
      this.append(
        util.fieldset(
          gt('Incoming calls'),
          util.checkbox('call/showNativeNotifications', st.NOTIFICATIONS_ZOOM_DESKTOP, switchboardSettings),
          util.checkbox('call/useRingtones', st.NOTIFICATIONS_ZOOM_USE_RINGTONE, switchboardSettings)
        )
      )
    }
  }
)

//
// Calendar
//

ext.point('io.ox/calendar/settings/detail/view').extend(
  {
    id: 'zoom',
    after: 'timezones',
    render (baton) {
      if (!isConfigurable.ZOOM_CALENDAR) return
      baton.view.listenTo(zoomSettings, 'change', () => zoomSettings.saveAndYell())
      util.renderExpandableSection(st.CALENDAR_ZOOM_MEETINGS, st.CALENDAR_ZOOM_MEETINGS_EXPLANATION, 'io.ox/calendar/settings/zoom').call(this, baton)
    }
  }
)

ext.point('io.ox/calendar/settings/zoom').extend(
  {
    id: 'account',
    index: 100,
    render () {
      this.append(
        new AccountView().render().$el
      )
    }
  },
  {
    id: 'appointments',
    index: 200,
    render () {
      this.append(
        util.fieldset(
          gt('Appointments'),
          util.checkbox('addMeetingPassword', st.ZOOM_ADD_PASSWORD, zoomSettings),
          // #. Automatically copies the meeting link into an appointment's location field
          util.checkbox('autoCopyToLocation', st.ZOOM_COPY_LINK_LOCATION, zoomSettings),
          util.checkbox('autoCopyToDescription', st.ZOOM_COPY_TO_DESCRIPTION, zoomSettings)
        )
      )
    }
  },
  {
    id: 'dialin',
    index: 300,
    render () {
      this.append(
        util.fieldset(
          gt('Dial-in numbers'),
          $('<p>').text(
            // #. pro and business are names
            gt('If you have a professional Zoom account, Zoom offers dial-in numbers per country and lets you choose which countries appear in meeting invitations.')
          ),
          $('<p>').text(
            gt('You can edit the list of countries in your Zoom profile.')
          ),
          $('<p>').append(
            $('<a href="" rel="noopener" target="_blank">')
              .attr('href', 'https://zoom.us/profile/setting?tab=telephony')
              .append(
                $.txt(gt('Open Zoom profile')),
                createIcon('bi/box-arrow-up-right.svg').addClass('ms-8')
              )
          )
        ).attr('id', 'io-ox-zoom-dial-in-section').addClass('last')
      )
    }
  }
)

const AccountView = DisposableView.extend({
  className: 'conference-account-view',
  events: {
    'click [data-action="add"]': 'onAdd',
    'click [data-action="remove"]': 'onRemove'
  },
  initialize () {
    this.listenTo(ox, 'zoom:tokens:added', this.render)
    this.listenTo(ox, 'switchboard:disconnect switchboard:reconnect', this.render)
  },
  render () {
    this.$el.empty().append(this.$account)

    zoomApi.getAccount().then(
      this.renderAccount.bind(this)
    ).catch(error => {
      if (error.status === 401) {
        this.renderMissingAccount()
      } else {
        this.renderSwitchboardOffline()
      }
    })

    return this
  },
  renderMissingAccount () {
    this.$el.empty().append(
      $('<p>').text(
        gt('You first need to connect %1$s with Zoom. To do so, you need a Zoom Account. If you don\'t have an account yet, it is sufficient to create a free one.', ox.serverConfig.productName)
      ),
      // add button
      $('<button type="button" class="btn btn-primary" data-action="add">').text(
        gt('Connect with Zoom')
      )
    )
  },
  renderAccount (data) {
    const name = contactsUtil.getFullName(data)
    const pmi = data.pmi && ('https://zoom.us/j/' + String(data.pmi).replace(/\D+/g, ''))
    this.$el.empty().append(
      $('<p>').text(
        gt('You have linked the following Zoom account:')
      ),
      $('<div class="conference-account">').append(
        // name
        name && $('<div class="name">').text(name),
        // email
        data.email && $('<div>').text(data.email),
        // personal meeting
        pmi && $('<div>').append(
          $.txt(gt('Personal Meeting ID:') + ' '),
          $('<a target="_blank" rel="noopener">')
            .attr('href', pmi)
            .text(data.pmi)
        ),
        // remove button
        $('<button type="button" class="btn btn-default" data-action="remove">').text(gt('Remove account'))
      )
    )
  },
  renderSwitchboardOffline () {
    this.$el.empty().append(
      $('<p class="alert alert-warning">').text(
        gt('The Zoom integration service is currently unavailable. Please try again later.')
      )
    )
  },
  async onAdd () {
    zoomApi.startOAuthHandshake()
  },
  async onRemove () {
    await zoomApi.removeAccount()
    this.renderMissingAccount()
  }
})
