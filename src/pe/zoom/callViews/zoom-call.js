import $ from '@/jquery'
import _ from '@/underscore'

import ox from '@/ox'
import { ZoomView } from '@/pe/zoom/view/zoom'
import zoomApi from '@/pe/zoom/api'
import { createIcon } from '@/io.ox/core/components'

import gt from 'gettext'

const ZoomCallView = ZoomView.extend({

  className: 'conference-view zoom',

  initialize () {
    window.zoomCall = this
    this.listenTo(this, 'connect', function () {
      zoomApi.startOAuthHandshake()
    })
  },

  renderAuthRequired () {
    this.$el.append(
      $('<div class="alert alert-info">').text(
        gt('You first need to connect %1$s with Zoom. To do so, you need a Zoom Account. If you don\'t have an account yet, it is sufficient to create a free one.', ox.serverConfig.productName)
      )
    )
  },

  renderPending () {
    this.$el.append(
      $('<div class="pending">').append(
        // #. Present progressive, indicating an ongoing or current action.
        $.txt(gt('Connecting to Zoom…')),
        createIcon('bi/arrow-clockwise.svg').addClass('animate-spin')
      )
    )
  },

  renderDone () {
    const url = this.getJoinURL()
    this.$el.append(
      $('<div>').append(
        $('<a target="_blank" rel="noopener">').attr('href', url).html(
          _.escape(url).replace(/([-/.?&=])/g, '$1<wbr>')
        )
      ),
      $('<div>').append(
        $(`<button type="button" class="btn btn-link copy-to-clipboard">${gt('Copy to clipboard')}</button>`)
          .on('click', () => navigator.clipboard.writeText(url))
      )
    )
  },

  createConnectButtons () {
    return $('<div class="action-button-rounded">').append(
      this.createButton('cancel', gt('Cancel'), 'bi/x.svg'),
      this.createButton('connect', gt('Connect with Zoom'), 'bi/plug.svg', 'btn-accent')
    )
  },

  createMeeting () {
    return zoomApi.createInstantMeeting().then(
      this.createMeetingSuccess.bind(this),
      this.createMeetingFailed.bind(this)
    )
  },

  createMeetingSuccess (result) {
    this.model.set({ joinURL: result.join_url, state: 'done' })
  }
})

export default ZoomCallView
