import _ from '@/underscore'

import ox from '@/ox'

import switchboardApi from '@/io.ox/switchboard/api'
import { settings as zoomSettings } from '@/io.ox/conference/zoom-settings'
import gt from 'gettext'

function isJsonResponse (response) {
  const contentType = response.headers.get('content-type')
  return contentType && contentType.startsWith('application/json')
}

async function getHeaders () {
  const token = await switchboardApi.getJwt()
  return new Headers({
    Authorization: `Bearer ${token}`,
    'Content-Type': 'application/json'
  })
}

const exports = {

  getCallbackURL () {
    const callBackHost = zoomSettings.get('host', switchboardApi.host)
    return `https://${callBackHost.replace(/^https?:\/\//, '')}/zoom/oauth-callback?state=${encodeURIComponent(switchboardApi.userId)}&origin=${encodeURIComponent(ox.abs)}`
  },

  startOAuthHandshake () {
    const top = (screen.availHeight - 768) / 2 >> 0
    const left = (screen.availWidth - 1024) / 2 >> 0
    return window.open(this.getCallbackURL(), 'zoom', 'width=1024,height=768,left=' + left + ',top=' + top + ',scrollbars=yes')
  },

  getAccount () {
    return this.api('GET', '/users/me')
  },

  async removeAccount () {
    if (zoomSettings.get('host')) {
      const zoomServiceUrl = zoomSettings.get('host')
      const headers = await getHeaders()
      return fetch(`https://${zoomServiceUrl}/api/v1/zoom/tokens`,
        { method: 'DELETE', headers })
    }
  },

  async api (method, url, data) {
    const zoomServiceUrl = zoomSettings.get('host')
    const headers = await getHeaders()
    const payload = { method, url, data: data || {} }
    const request = fetch(
          `https://${zoomServiceUrl}/api/v1/zoom/executions`,
          { method: 'POST', headers, body: JSON.stringify({ payload }) }
    ).then(async response => {
      if (!response.ok) return Promise.reject(response)
      if (!isJsonResponse(response)) return response.text()
      return (await response.json()).data
    })
      .catch(err => {
        if (err.status === 401) return Promise.reject(err)
        return Promise.reject(rejectWithUnexpectedError())
      })
    return request
  },

  getMeeting (id) { return this.api('GET', `/meetings/${id}`) },

  // data:
  // - <string> topic
  // - <int> startTime
  // - <string> timezone
  // - <int> duration (in minutes)
  // - <string> [agenda]
  createMeeting (data) {
    data = { settings: { join_before_host: true }, ...data }
    if (!data.password && zoomSettings.get('addMeetingPassword', true)) data.password = createPassword()
    return this.api('POST', '/users/me/meetings', data)
  },

  createInstantMeeting () {
    return this.createMeeting({ type: 1 })
  },

  changeMeeting (id, changes) {
    return this.api('PATCH', '/meetings/' + id, changes)
  },

  deleteMeeting (id) {
    return this.api('DELETE', '/meetings/' + id + '?schedule_for_reminder=false')
  }
}

function rejectWithUnexpectedError () {
  return { status: 500, internal: true, message: gt('Something went wrong. Please try again.') }
}

function createPassword () {
  // [API documentation]
  // Password may only contain the following characters:
  // [a-z A-Z 0-9 @ - _ *] and can have a maximum of 10 characters.
  //
  // The admin might set minimum password requirement settings:
  //   * Have a minimum password length
  //   * Have at least 1 letter (a,b,c...)
  //   * Have at least 1 number (1,2,3...)
  //   * Have at least 1 special character (!,@,#...)
  //   * Only allow numeric password
  //
  // Lets always generate 10 characters with letters, numbers, and special characters
  // to be on the safe side also configuration wise. users are expected to copy and send
  // links instead of manually typing passwords.
  //
  // Taking the verbose but short way:
  const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890@-_*'
  return _.range(10).map(function () { return chars[Math.random() * chars.length >> 0] }).join('')
}

export default exports
