import $ from '@/jquery'
import _ from '@/underscore'
import ox from '@/ox'

import '@/io.ox/core/extensions'
import '@/io.ox/core/upsell'

import { settings as mailSettings } from '@/io.ox/mail/settings'
import { settings } from '@/pe/upsell/plugins/simple-wizard/settings'
import gt from 'gettext'

/**
 * Used settings:
 * - url
 * - overlayOpacity
 * - overlayColor
 * - zeroPadding
 * - width
 * - height
 *
 * URL variables:
 * - $user
 * - $user_id
 * - $context_id
 * - $session
 * - $language
 * - $missing, $type, $id
 * - $hostname
 */

let instance = null

const that = {

  getVariables (options) {
    options = options || {}
    return {
      context_id: ox.context_id,
      hostname: location.hostname,
      id: options.id || '',
      // missing
      imap_login: '',
      language: ox.language,
      mail: mailSettings.get('defaultaddress', ''),
      missing: options.missing || '',
      session: ox.session,
      type: options.type || '',
      user: ox.user,
      user_id: ox.user_id,
      // missing
      user_login: ''
    }
  },

  // url is temporary. used when working with local copy of settings
  // see upsell:simple-wizard:init
  getURL (options, url) {
    const hash = that.getVariables(options)

    url = String(url || that.settings.url).replace(/\$(\w+)/g, function (all, key) {
      key = String(key).toLowerCase()
      return key in hash ? encodeURIComponent(hash[key]) : '$' + key
    })

    return url
  },

  getIFrame () {
    // add iframe but with blank file (to avoid delay)
    return $('<iframe src="blank.html" allowtransparency="true" border="0" frameborder="0" framespacing="0">')
  },

  addControls () {
    if (that.settings.closeButton === true) {
      this.addButton('cancel', gt('Close'))
    }
  },

  getPopup () {
    return instance
  },

  setSrc (src) {
    if (instance) {
      instance.$body.idle().find('iframe').attr('src', src)
    }
  },

  // allows client-side settings during development
  settings: _.extend({
  // defaults
    closeButton: true,
    zeroPadding: true,
    width: 750,
    height: 390,
    overlayOpacity: 0.5,
    overlayColor: '#000',
    url: 'blank.html?user=$user,user_id=$user_id,context_id=$context_id,' +
              'language=$language,type=$type,id=$id,missing=$missing,hostname=$hostname#session=$session'
  }, settings.get()),

  open (options) {
    if (instance) return

    // allow custom context-sensitive settings (e.g. set via UI plugin)
    const settings = _.deepClone(that.settings)
    ox.trigger('upsell:simple-wizard:init', that.getVariables(options), settings)

    import('@/io.ox/backbone/views/modal').then(function ({ default: ModalDialog }) {
      if (_.device('smartphone')) {
        settings.width = '95%'
      }
      instance = new ModalDialog({ width: settings.width })
        .build(function () {
          if (settings.zeroPadding) {
            this.$el.addClass('zero-padding')
          }
          this.$body
            .busy()
            .css({
              maxHeight: settings.height + 'px',
              overflow: 'hidden'
            })
            .append(
              // add iframe but with blank file (to avoid delay)
              that.getIFrame()
                .css({
                  width: '100%',
                  height: settings.height + 'px'
                })
            )
          that.addControls.call(this)
        })
        .on('beforeshow', function () {
          ox.trigger('upsell:simple-wizard:show:before', this)
        })
        .on('open', function () {
          ox.off('upsell:requires-upgrade', that.open)
          // style backdrop
          this.$el.next().css({
            opacity: settings.overlayOpacity,
            backgroundColor: settings.overlayColor
          })
          const self = this
          setTimeout(function () {
            that.setSrc(that.getURL(options, settings.url))
            ox.trigger('upsell:simple-wizard:show', self)
          }, 250)
        })
        .on('close', function () {
          ox.on('upsell:requires-upgrade', that.open)
          ox.trigger('upsell:simple-wizard:close', this)
          instance = null
        })
        .open()
    })
  },

  close () {
    if (instance) instance.close()
  },

  enable () {
    ox.on('upsell:requires-upgrade', that.open)
  },

  disable () {
    ox.off('upsell:requires-upgrade', that.open)
  }
}

// register for event
that.enable()
// DEBUGGING; useful during development
// upsell.demo(true)

export default that
