import $ from '@/jquery'

import Stage from '@/io.ox/core/extPatterns/stage'
import ext from '@/io.ox/core/extensions'
import { settings } from '@/pe/upsell/plugins/settings'

const adSettings = settings.get('driveAd')
const api = {}

if (!adSettings) console.error('The upsell bubbles app does not work without settings for plugins/upsell//driveAd.')

if (adSettings) {
  api.extendDrive = function () {
    ext.point('io.ox/files/details').extend({
      id: 'upsellad',
      // after the title, right?
      index: 201,
      draw () {
        this.append(
          $('<div>').html(adSettings)
        )
      }
    })
  }
  // eslint-disable-next-line no-new
  new Stage('io.ox/core/stages', {
    id: 'upsellbubbles',
    index: 1002,
    run () {
      api.extendDrive()
      return $.when()
    }
  })
}

export default api
