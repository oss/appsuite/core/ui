import { Settings } from '@/io.ox/core/settings'

export const defaults = {}

export const settings = new Settings('plugins/upsell', () => defaults)
