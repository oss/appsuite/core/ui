import ext from '@/io.ox/core/extensions'
import UpsellView from '@/pe/backbone/mini-views/upsell'
import gt from 'gettext'

ext.point('io.ox/calendar/sidepanel').extend({
  index: 50,
  id: 'upsell-calendar',
  draw (baton) {
    // if (baton.context !== 'app') return
    this.append(new UpsellView({
      id: 'folderview/calendar',
      illustration: true,
      className: 'links rocket-box',
      requires: 'guard infostore',
      title: gt('Add email and document encryption')
    }).render().$el)
  }
})

ext.point('io.ox/contacts/sidepanel').extend({
  index: 50,
  id: 'upsell-contacts',
  draw (baton) {
    this.append(
      new UpsellView({
        id: 'folderview/contacts',
        className: 'links rocket-box',
        illustration: true,
        requires: 'guard infostore',
        title: gt('Add email and document encryption')
      }).render().$el
    )
  }
})

ext.point('io.ox/mail/sidepanel').extend({
  id: 'upsell',
  index: 50,
  draw () {
    this.append(
      new UpsellView({
        id: 'folderview/mail',
        className: 'links rocket-box',
        illustration: true,
        requires: 'guard infostore',
        title: gt('Add email and document encryption')
      }).render().$el
    )
  }
})
