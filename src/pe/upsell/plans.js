/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '@/jquery'
import _ from '@/underscore'
import Backbone from 'backbone'
import ox from '@/ox'

import ModalDialog from '@/io.ox/backbone/views/modal'
import { createButton } from '@/io.ox/core/components'
import { isDark, getCurrent } from '@/io.ox/core/theming/main'
import locale from '@/io.ox/core/locale'
import '@/pe/upsell/plans.scss'

import { settings as coreSettings } from '@/io.ox/core/settings'

// avoid duplicate dialogs
let dialogOpen = false

export function showPlans (upsellOptions) {
  if (dialogOpen) return
  console.debug('upsell:requires-upgrade', upsellOptions)

  const plans = getPlans()
  if (!plans.entries) {
    console.error('Upsell/Plans: Missing entries', plans)
    return
  }

  const defaultCurrency = plans.defaultCurrency || (locale.current() === 'en_US' ? 'USD' : 'EUR')
  const currentPlan = getCurrentPlan()
  const currentLanguage = locale.getCurrentLanguage()
  const model = new Backbone.Model({ currency: defaultCurrency, period: 1, showUSD: defaultCurrency === 'USD' })

  new ModalDialog({ title: 'Upgrade your plan', width: 900, model })
    .inject({
      renderPlan (plan) {
        const details = plan.details[currentLanguage] || plan.details.en
        if (!details) return $()
        details.id = plan.id
        const $el = $('<div class="flex-grow border border-bright rounded p-24 relative">')
          .attr('data-plan-id', plan.id)
        if (plan.expires) {
          $el.append(
            $('<div class="rounded-t absolute width-100 top-0 left-0 border-bottom text-center text-xs leading-6 text-bold">')
              .css('background-color', 'var(--background-10)')
              .text('Expires')
          )
        } else if (plan.popular) {
          $el.addClass('border-accent').append(
            $('<div class="rounded-t absolute width-100 top-0 left-0 border-bottom border-accent text-center text-xs leading-6 text-accent-dark text-medium">')
              .css('background-color', 'var(--accent-100)')
              .text('Most popular')
          )
          if (!isDark()) {
            $el.css('background-color', 'var(--accent-10)')
          }
        }
        const price = getPrice({ plan, period: this.model.get('period'), currency: this.model.get('currency') })
        $el.append(
          this.renderTitle(details),
          this.renderPrice(plan.id, price),
          this.renderUpgradeButton(details, price),
          this.renderDescription(details),
          this.renderFeatures(details),
          this.renderWarning(details),
          this.renderFinePrint(details)
        )
        return $el
      },
      renderTitle (details) {
        const { isNew, text } = decompose(details.title)
        return $('<h2 class="text-center text-accent mb-16">').append(
          isNew ? this.renderNewBadge() : $(),
          $('<span>').text(text)
        )
      },
      renderPrice (id, price) {
        const number = isNaN(price.offer) ? 'N/A' : price.offer
        const hasDiscount = price.discount !== 0
        return $('<div class="text-center mb-32">').attr('data-price', id).append(
          $('<div>').append(
            hasDiscount ? $('<span class="text-2xl text-attention me-8">').text(`-${price.discount} %`) : $(),
            $('<span class="text-3xl text-bold">').text(locale.currency(number, price.currency, 'stripIfInteger'))
          ),
          $('<div class="text-sm text-gray">').append(
            hasDiscount ? $('<span class="line-through me-8">').text(locale.currency(price.base, price.currency, 'stripIfInteger')) : $(),
            $('<span>').text('per month')
          )
        )
      },
      renderUpgradeButton (details, price) {
        const isCurrent = details.id === currentPlan
        const disabled = isCurrent || isNaN(price.base)
        const variant = isCurrent ? 'default' : 'primary'
        const text = getButtonText(details, price.offer, isCurrent)
        return createButton({ text, variant })
          .addClass('leading-8 width-100 mb-32')
          .attr({ 'data-action': 'chose', 'data-plan': details.id })
          .prop('disabled', disabled)
      },
      renderDescription (details) {
        return $('<div class="text-sm text-bold">').text(details.description || '')
      },
      renderFeatures (details) {
        if (!details.features?.length) return $()
        return $('<ul class="checkmarks">').append(
          details.features.map(feature => {
            const { isNew, text } = decompose(feature)
            return $('<li class="text-sm">').append(
              isNew ? this.renderNewBadge() : $(),
              $('<span>').html(miniMarkdown(text))
            )
          })
        )
      },
      renderNewBadge () {
        return $('<span class="label label-accent uppercase me-8">').text('New')
      },
      renderWarning (details) {
        return details.warning
          ? $('<div>').append($('<div class="label label-subtle subtle-red text-base">Important</div>'), $('<br>'), $.txt(details.warning))
          : $()
      },
      renderFinePrint (details) {
        return details.fineprint ? $('<div class="text-gray text-xs">').text(details.fineprint) : $()
      },
      renderRadioButtonGroup (periods = [1]) {
        if (periods.length <= 1) return $()
        return $('<div class="btn-group btn-radio-group" role="group" aria-label="Subscription period">').append(
          periods
            .map((period, i, all) => {
              const label = period === 1 ? '1 month' : `${period} months`
              return [
                $(`<input type="radio" class="btn-check" name="period" value="${period}" id="radio-period-${i}" autocomplete="off">`),
                $(`<label class="btn btn-outline-primary" style="border-radius: ${getRadius(i, all.length - 1)}" for="radio-period-${i}">`).text(label)
              ]
            })
            .flat()
        )
        function getRadius (i, last) {
          if (i === 0) return '6px 0 0 6px'
          if (i === last) return '0 6px 6px 0'
          return '0'
        }
      },
      updatePeriod () {
        const value = String(this.model.get('period'))
        this.$('[name="period"]').each(function () {
          $(this).prop('checked', this.value === value)
        })
      }
    })
    .build(function build () {
      this.$header.addClass('flex-row border-0').append(
        this.$header.find('h1').addClass('flex-grow mt-8'),
        this.renderRadioButtonGroup(plans.periods)
      )

      this.$footer.addClass('border-0')

      this.$body.css('padding', '0 16px').append(
        $('<div class="plans flex-row" style="gap: 16px; min-height: 400px">').append(
          Object.keys(plans.entries).map(id => {
            plans.entries[id].id = id
            return this.renderPlan(plans.entries[id])
          })
        )
      )

      this.listenTo(this.model, 'change:showUSD', (model, value) => {
        this.model.set('currency', value ? 'USD' : 'EUR')
      })

      this.$el.on('change', '.btn-group', () => {
        this.model.set('period', parseInt(this.$('[name="period"]:checked').val(), 10))
      })

      this.listenTo(this.model, 'change:currency change:period', () => {
        this.updatePeriod()
        Object.values(plans.entries).forEach(plan => {
          this.$(`[data-plan-id="${plan.id}"]`).replaceWith(this.renderPlan(plan))
        })
      })

      this.$body.on('click', '[data-action="chose"]', (e) => {
        const $el = $(e.currentTarget)
        const plan = $el.attr('data-plan')
        const currency = this.model.get('currency')
        const period = this.model.get('period')
        const price = getPrice({ plan: plans.entries[plan], period, currency })
        const color = getCurrent()?.accentColor
        ox.trigger('upsell:upgrade', { ...upsellOptions, plan: { id: plan, currency, period, price: price.base, offer: price.offer, discount: price.discount, color } })
        this.close()
      })
    })
    .addAlternativeButton({ action: 'cancel', label: 'Close' })
    .on('open', function () {
      dialogOpen = true
      this.$el.next('.modal-backdrop.in:visible').css({ opacity: 0.20, backgroundColor: 'var(--accent-700)' })
      this.updatePeriod()
    })
    .on('close', () => { dialogOpen = false })
    .open()
}

export function getButtonText (details, price, isCurrent = false) {
  const { text } = decompose(details.title)
  if (isCurrent) return 'Your current plan'
  if (details.button) return details.button
  if (price === 0) return 'Try for free'
  return `Upgrade to ${text}`
}

export function decompose (text = '') {
  const isNew = /^!/.test(text)
  return { isNew, text: String(text).replace(/^!/, '') }
}

export function miniMarkdown (text = '') {
  return _.escape(text).replace(/\*([^*]+)\*/g, '<b>$1</b>')
}

export function getCurrentPlan () {
  return coreSettings.get('upsell/currentPlan', 'free')
}

export function getPlans () {
  return coreSettings.get('upsell/plans') || getDefaultPlans()
}

export function getDefaultPlans () {
  return {
    periods: [1, 12, 24],
    entries: {
      free: {
        details: {
          en: {
            title: 'Free',
            description: 'It is free',
            features: [
              '*1 GB* storage',
              'E-Mail',
              'Addressbook',
              'Calendar',
              'Tasks'
            ],
            fineprint: 'The free plan will end next month. If you do not subscribe to a paid plan by the end of this month, you will lose the ability to receive and send emails.'
          }
        },
        expires: '2024-12-01',
        prices: {
          '*': {
            EUR: [
              { price: 0 }
            ],
            USD: [
              { price: 0 }
            ]
          }
        }
      },
      pro: {
        details: {
          en: {
            title: 'Professional',
            description: 'Everything in Free and',
            features: [
              '*5 GB* storage',
              'Drive',
              'Connect mobile devices',
              'Shared calendars'
            ]
          }
        },
        popular: true,
        prices: {
          1: {
            EUR: [
              { price: 3.99 }
            ],
            USD: [
              { price: 4.00 }
            ]
          },
          12: {
            EUR: [
              { price: 3.99, discount: 10 }
            ],
            USD: [
              { price: 4.00, discount: 10 }
            ]
          },
          24: {
            EUR: [
              { price: 3.99, discount: 20 }
            ],
            USD: [
              { price: 4.00, discount: 20 }
            ]
          }
        }
      },
      business: {
        details: {
          en: {
            title: 'Business',
            description: 'Everything in Pro and',
            features: [
              '!*1TB* storage',
              '!*AI support* ✨',
              'Expert support',
              'Encryption'
            ]
          }
        },
        prices: {
          1: {
            EUR: [
              { price: 5.99 }
            ],
            USD: [
              { price: 7.00 }
            ]
          },
          12: {
            EUR: [
              { price: 5.99, discount: 10 }
            ],
            USD: [
              { price: 7.00, discount: 10 }
            ]
          },
          24: {
            EUR: [
              { price: 5.99, discount: 20 }
            ],
            USD: [
              { price: 7.00, discount: 20 }
            ]
          }
        }
      }
    }
  }
}

export function getPrice ({ plan, period = 1, currency = 'EUR' } = {}) {
  let result = { base: NaN, offer: NaN, currency, period, discount: 0 }
  const data = plan.prices?.[period]?.[currency] ?? plan.prices?.['*']?.[currency]
  if (!data?.length) return result
  const { price, offer, discount } = data[0]
  result = { base: price, currency, period, discount: 0, offer: price }
  if (discount) {
    result.discount = discount
    result.offer = Math.round(price * (100 - discount)) / 100
  } else if (offer) {
    result.discount = Math.round((price - offer) / price * 100)
    result.offer = offer
  }
  return result
}
