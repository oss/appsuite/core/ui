import $ from '@/jquery'
import ext from '@/io.ox/core/extensions'
import UpsellView from '@/pe/backbone/mini-views/upsell'

import { settings as coreSettings } from '@/io.ox/core/settings'
import gt from 'gettext'

ext.point('io.ox/core/appcontrol/right/account').extend({
  id: 'upsell',
  index: 50,
  extend () {
    const s = coreSettings.get('features/upsell/secondary-launcher', {})
    const view = new UpsellView({
      tagName: 'li',
      id: 'topbar-dropdown',
      attributes: { role: 'presentation' },
      requires: s.requires || 'active_sync || caldav || carddav',
      title: gt('Upgrade your account'),
      customize () {
        $('i', this.$el).css({ width: 'auto' })
      }
    })

    if (!view.visible) return
    this.$ul.append(view.render().$el)
    this.divider()
  }
})
