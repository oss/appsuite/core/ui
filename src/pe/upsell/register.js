import ext from '@/io.ox/core/extensions'
import upsell from '@/pe/core/upsell'

ext.point('io.ox/core/upsell').extend({
  id: 'pe/core/upsell',
  register (baton) {
    for (const key in upsell) {
      if (Object.prototype.hasOwnProperty.call(upsell, key)) {
        baton.data[key] = upsell[key]
      }
    }
  }
})
