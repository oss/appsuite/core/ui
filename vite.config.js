/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import { defineConfig, normalizePath } from 'vite'
import vitePluginOxBundle from '@open-xchange/vite-plugin-ox-bundle'
import vitePluginOxCss from '@open-xchange/vite-plugin-ox-css'
import vitePluginOxExternals from '@open-xchange/vite-plugin-ox-externals'
import vitePluginOxManifests from '@open-xchange/vite-plugin-ox-manifests'
import vitePluginProxy from '@open-xchange/vite-plugin-proxy'
import rollupPluginCopy from 'rollup-plugin-copy'
import gettextPlugin from '@open-xchange/rollup-plugin-po2json'
import dotenv from 'dotenv'
import fs from 'node:fs/promises'
import path from 'node:path'
import { fileURLToPath, URL } from 'node:url'
import vue from '@vitejs/plugin-vue'
import svg from 'vite-plugin-svgo'

dotenv.config({ path: '.env' })
dotenv.config({ path: '.env.defaults' })

let PROXY_URL
try {
  PROXY_URL = new URL(process.env.SERVER)
} catch (e) {
  PROXY_URL = new URL('https://0.0.0.0')
}
const PORT = process.env.PORT
const ENABLE_HMR = process.env.ENABLE_HMR === 'true'
const ENABLE_HTTP_PROXY = process.env.ENABLE_HTTP_PROXY === 'true'
const FRONTEND_URIS = process.env.FRONTEND_URIS || ''
const ENABLE_SECURE_PROXY = process.env.ENABLE_SECURE_PROXY === 'true'
const ENABLE_SECURE_FRONTENDS = process.env.ENABLE_SECURE_FRONTENDS === 'true'
const ENABLE_STRICT_FILESYSTEM = process.env.ENABLE_STRICT_FILESYSTEM === 'true'
// convert to absolute path (helps fixing a windows issue with case sensitive paths)
const root = await fs.realpath('./src')
const normalizedRoot = path.normalize(await fs.realpath('./'))

// only VITE_ prefixed variables will be statically replaced
// therefore remap APP_VERSION to VITE_VERSION
process.env.VITE_VERSION = process.env.APP_VERSION

function resolveModulePath (target) {
  return normalizePath(fileURLToPath(import.meta.resolve(target)))
}

export default defineConfig(({ mode, command }) => {
  const debugMode = mode === 'development' || command === 'serve'
  if (mode === 'development') process.env.VITE_DEBUG = 'true'
  return {
    root,
    publicDir: '../public',
    logLevel: process.env.LOG_LEVEL || (debugMode ? 'info' : 'warn'),
    base: debugMode ? PROXY_URL.pathname : './',
    build: {
      minify: 'esbuild',
      target: 'es2022',
      outDir: '../dist',
      emptyOutDir: true,
      polyfillDynamicImport: false,
      sourcemap: true,
      assetsInlineLimit: 0,
      reportCompressedSize: process.env.LOG_LEVEL === 'info',
      modulePreload: true,
      chunkSizeWarningLimit: 1400,
      dynamicImportVarsOptions: {
        warnOnError: true,
        exclude: [
          'src/io.ox/core/settings.js',
          'src/io.ox/core/manifests.js',
          // in the following files, the dynamic imports are already deprecated
          'src/io.ox/settings/main.js',
          'src/io.ox/backbone/views/actions/util.js',
          'src/io.ox/core/desktop.js',
          'src/io.ox/core/feature.js'
          // convert to absolute paths (helps fixing a windows issue with case sensitive paths)
        ].map((excludedPath) => `${normalizedRoot}/${excludedPath}`)
      },
      rollupOptions: {
        input: {
          'blank.html': `${normalizedRoot}/src/blank.html`,
          'busy.html': `${normalizedRoot}/src/busy.html`,
          'index.html': `${normalizedRoot}/src/index.html`,
          'print.html': `${normalizedRoot}/src/print.html`,
          'checkout.html': `${normalizedRoot}/src/pe/upsell/checkout.html`
        },
        preserveEntrySignatures: 'strict',
        cache: true,
        output: {
          minifyInternalExports: false,
          entryFileNames: '[name].js'
        }
      }
    },
    server: {
      port: PORT,
      hmr: ENABLE_HMR,
      https: {
        key: process.env.HOST_KEY || 'ssl/host.key',
        cert: process.env.HOST_CRT || 'ssl/host.crt'
      },
      fs: {
        strict: ENABLE_STRICT_FILESYSTEM
      },
      watch: {
        // fixes an issue that happens on windows since vite 5.2.11, caused by this commit https://github.com/vitejs/vite/commit/4d83eb58cdea0d2e4ec4f0da6e1dd6b72014e67e
        ignored: `${normalizedRoot}/public`
      }
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        public: fileURLToPath(new URL('./public', import.meta.url))
      }
    },
    optimizeDeps: {
      esbuildOptions: {
        target: 'es2022'
      },
      include: [
        'jquery',
        'underscore',
        'backbone',
        'backbone-validation',
        'ky',
        'dompurify',
        'velocity-animate',
        'jwt-decode',
        'chart.js',
        'mark.js',
        'croppie/croppie.min.js',
        'qrcode',
        'swiper',
        'bigscreen',
        'pdfjs-dist/build/pdf',
        'pdfjs-dist/build/pdf.worker',
        'color-rgba',
        'chart.js/auto',
        'socket.io-client',
        'tinymce',
        'tinymce/tinymce',
        'tinymce/models/dom',
        'tinymce/icons/default',
        'tinymce/themes/silver',
        'tinymce/plugins/autoresize',
        'tinymce/plugins/autolink',
        'tinymce/plugins/code',
        'tinymce/plugins/link',
        'tinymce/plugins/lists',
        '@tinymce/tinymce-jquery/dist/tinymce-jquery',
        'swiper/modules',
        'pdfjs-dist/web/pdf_viewer',
        'vue',
        'luxon',
        '@vueuse/core',
        '@vueuse/integrations/useFocusTrap',
        'swiper/element/bundle',
        'moment/dist/locale/*',
        'moment-timezone'
      ],
      exclude: [
        '@open-xchange/bootstrap',
        'pinia'
      ]
    },
    plugins: [
      {
        configureServer ({ middlewares, ws }) {
          // detect refresh event from vite and update the version
          const send = ws.send
          ws.send = function (...args) {
            if (args[0]?.type === 'full-reload') {
              version = +new Date()
            }
            return send.apply(this, args)
          }

          let version = +new Date()
          middlewares.use((req, res, next) => {
            if (req.originalUrl === '/updateVersion') {
              version = +new Date()
              res.statusCode = 200
              res.setHeader('new-version', version)
              return res.end()
            }

            // Serve empty file for bundles in dev mode
            if (req.originalUrl.match(/\/bundles\/[a-z]+.js/g)) {
              res.statusCode = 200
              return res.end()
            }

            res.setHeader('version', req.headers?.version || version)
            res.setHeader('latest-version', version)
            next()
          })
        }
      },
      vue({
        template: {
          compilerOptions: {
            isCustomElement: (tag) => ['swiper-container', 'swiper-slide'].includes(tag)
          }
        }
      }),
      vitePluginProxy({
        proxy: {
          [`${PROXY_URL.pathname}/api`.replace(/\/+/g, '/')]: {
            target: PROXY_URL.href,
            changeOrigin: true,
            secure: ENABLE_SECURE_PROXY
          },
          '/ajax': {
            target: PROXY_URL.href,
            changeOrigin: true,
            secure: ENABLE_SECURE_PROXY
          },
          '/help': {
            target: PROXY_URL.href,
            changeOrigin: true,
            secure: ENABLE_SECURE_PROXY
          },
          '/meta': {
            target: PROXY_URL.href,
            changeOrigin: true,
            secure: ENABLE_SECURE_PROXY
          },
          '/socket.io/appsuite': {
            target: `wss://${PROXY_URL.host}/socket.io/appsuite`,
            ws: true,
            changeOrigin: true,
            secure: ENABLE_SECURE_PROXY
          },
          '/pwa.json': {
            target: PROXY_URL.href,
            changeOrigin: true,
            secure: ENABLE_SECURE_PROXY
          }
        },
        httpProxy: ENABLE_HTTP_PROXY && {
          target: PROXY_URL.href,
          port: PROXY_URL.port || 8080
        },
        frontends: FRONTEND_URIS && FRONTEND_URIS.split(',').map(uri => ({ target: uri, secure: ENABLE_SECURE_FRONTENDS }))
      }),
      vitePluginOxManifests({
        watch: true,
        entryPoints: ['src/**/*.js'],
        manifestsAsEntryPoints: true,
        meta: {
          id: 'core-ui',
          name: 'Core UI',
          buildDate: new Date().toISOString(),
          commitSha: process.env.CI_COMMIT_SHA,
          version: String(process.env.APP_VERSION || '').split('-')[0],
          revision: String(process.env.APP_VERSION || '').split('-')[1]
        }
      }),
      vitePluginOxExternals({
        prefix: '$'
      }),
      gettextPlugin({
        poFiles: 'src/i18n/*.po',
        outFile: 'ox.pot',
        defaultDictionary: 'io.ox/core',
        defaultLanguage: 'en_US'
      }),
      rollupPluginCopy({
        targets: [
          { src: resolveModulePath('tinymce-i18n/langs6/*'), dest: 'public/tinymce/langs' },
          { src: ['./src/themes/default'], dest: 'public/themes' },
          { src: './src/pe/portal/plugins/oxdriveclients/img/', dest: 'public/pe/portal/plugins/oxdriveclients/' },
          { src: './src/pe/portal/plugins/oxdriveclients/img/', dest: 'public/pe/portal/plugins/oxdriveclients/' },
          { src: ['./src/themes/default/favicon.*'], dest: 'public' },
          { src: './src/pe/core/whatsnew/**/*', dest: 'public/whatsnew' },
          { src: resolveModulePath('pdfjs-dist/build/pdf.worker.min.mjs'), dest: 'public/pdfjs/' },
          { src: resolveModulePath('pdfjs-dist/cmaps'), dest: 'public/pdfjs' },
          { src: resolveModulePath('pdfjs-dist/web/images'), dest: 'public/pdfjs/web/' },
          { src: resolveModulePath('bootstrap-icons/icons/*'), dest: 'public/themes/default/icons/bi/' },
          { src: './src/themes/icons/*', dest: 'public/themes/default/icons/bi/' }
        ],
        hook: 'buildStart'
      }),
      vitePluginOxCss(),
      vitePluginOxBundle(
        {
          ignore: ['precore.js'],
          src: './bundles.json',
          htmlFiles: ['blank.html', 'busy.html', 'index.html', 'print.html']
        }
      ),
      svg({
        multipass: true,
        plugins: [
          {
            name: 'preset-default',
            params: {
              overrides: {
                convertColors: {
                  currentColor: true
                }
              }
            }
          }
        ]
      })
    ]
  }
})
