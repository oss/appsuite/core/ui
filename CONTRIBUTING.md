# Contributing

The following is a set of guidelines for contributing to App Suite UI core.  Feel free to propose changes to this document.

## Table Of Contents

[Style guides](#style-guides)

- [Local development](#local-development)
- [Style Guides](#style-guides)
  - [Git Commit Messages](#git-commit-messages)
  - [Changelog](#changelog)
  - [JavaScript Style Guide](#javascript-style-guide)
- [Security](#security)
  - [Scanners running with every pipeline](#scanners-running-with-every-pipeline)
  - [Scanners running in scheduled pipelines](#scanners-running-in-scheduled-pipelines)
  - [Container Security Measures](#container-security-measures)
  - [Other Code Quality Measures](#other-code-quality-measures)

## Local development

See [README.md](README.md)

## Style Guides

### Git Commit Messages

- Use the present tense ("Add: Cool feature" not "Added: Cool feature")
- Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
- Try to limit the first line to 72 characters or less
- Don't reference the files you changed in the commit message, as this information can be retrieved easily
- When only changing things which do not need a pipeline run, push with git option ci.skip: `git push -o ci.skip`
- Reference GitLab issues in the commit message body using `Closes #123` or `Relates to #123`. For issues in different projects, use the format `<namespace/project>#<issue-id>` (e.g., `appsuite/web-apps/gitlab-profile#9`).
- See also next section for how commits are being used for automatic changelog entries

**Full examples**

```text
Fix: Show laptop icon for desktop devices in "Your devices"

Closes #510
```

```text
Chore: Update the contributing guide

Closes appsuite/web-apps/gitlab-profile#9
```

### Changelog

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

If you want your commit message to appear in our [changelog](CHANGELOG.md) you can add one of these keywords that is appropriate for you:

- **Add:** for new features.
- **Change:** for changes in existing functionality.
- **Deprecate:** for soon-to-be removed features.
- **Remove:** for now removed features.
- **Fix:** for any bug fixes.
- **Security:** in case of vulnerabilities.

The prefixed keyword will removed from the changelog entry and the entry will be added to its corresponding section.

If you want your commit message **not** to appear in our [changelog](CHANGELOG.md), you can include one of the following keywords that best fits your change:

- **Defect:** A fix for an issue introduced within the current release cycle.
- **Chore:** General housekeeping tasks

**Extended Changelog entry**

In case you want to write an extended changelog entry in a commit you can add `Changelog:` to your commit message.

```text
Add: Cool new feature

- Only the first line will appear in the changelog
- so you can add technical details that won't appear in the changelog

Changelog:
  - This will appear under the first line in the changelog
  - This also
    - This one too
```

will produce following changelog entry (markdown):

```markdown
### Added

- Cool new feature
  - This will appear under the first line in the changelog
  - This also
    - This one too
```

**Changelog entry for breaking changes**

If you want your commit to be highlighted as a breaking change please add "This is a breaking change." to the body of your commit message.

```text
Remove: Old feature

This is a breaking change.
```

will produce following changelog entry (markdown):

```markdown

### Removed

- **Breaking change:** Old feature
```


### JavaScript Style Guide

All JavaScript code is linted with [ESLint] and follows the [JavaScript Standard Style].

## Security

### Scanners running with every pipeline

- [pnpm audit] - Performs a vulnerability audit against dependencies
- [ESLint] following JavaScript Standard Style
- Regression tests (Part of our e2e tests)

### Scanners running in scheduled pipelines

- [Renovate] - Automated dependency updates
- [Synopsys Detect (formerly known as Blackduck)] - reports are only sporadically checked
- [Coverity] - reports are only sporadically checked

### Container Security Measures

- "Distroless" images
- Use non-root user in containers
- [Spectral OPS]

### Other Code Quality Measures

- [Lint staged] - Runs linters against staged git files
- Code reviews - Before accepting merge requests we encourage code reviews

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[JavaScript Standard Style]: https://standardjs.com/rules.html
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[ESLint]: https://eslint.org/
[Synopsys Detect (formerly known as Blackduck)]: https://community.synopsys.com/s/document-item?bundleId=integrations-detect&topicId=introduction.html&_LANG=enus
[Coverity]: https://scan.coverity.com/
[Renovate]: https://docs.renovatebot.com/
[pnpm audit]: https://pnpm.io/cli/audit/

[Dependabot Dashboard]: https://dependabot.k3s.os2.oxui.de/
[Synopsys Detect (formerly known as Blackduck) Dashboard]: https://blackduck.open-xchange.com/
[Coverity Dashboard]: https://coverity.open-xchange.com/login/login.htm
[Lint staged]: https://www.npmjs.com/package/lint-staged
[Spectral OPS]: <https://spectralops.io/>
