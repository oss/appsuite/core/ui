---
title: Customize
icon: fa-plug
description: Getting ready to develop your own plugins / apps
---

# Customize

App Suite UI is designed to be extended and customized.
Custom plugin code can be registered using [manifests](./customize/manifests.html) and
will dynamically be loaded at runtime.
Usually this is used to register extension points in order to inject new functionality
or change existing behavior.

You can find some practical use-cases in [the example plugin project](https://gitlab.open-xchange.com/appsuite/web-foundation/frontend-plugin-example).
