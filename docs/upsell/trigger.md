---
title: Triggers
---

# Upsell triggers

This sections lists available Upsell triggers

## Menu bar / Top bar

| ID              | Configurable properties     | Capabilities                              | Shown on mobile | Description                                                                                     |
| --------------- | --------------------------- | ----------------------------------------- | --------------- | ----------------------------------------------------------------------------------------------- |
| quick-launchers | none                        |                                           | no              | Part of quick launch icon of an app.                                                            |
| app-launcher    | none                        |                                           | yes             | Part of an app icon within app launcher dropdown menu.                                          |
| upgrade-button  | enabled                     | default: active_sync or caldav or carddav | no              | Former "secondary launcher" An "Upgrade Button" is shown in the topbar next to the search field |
| topbar-dropdown | enabled                     | default: active_sync or caldav or carddav | yes             | Part of main toolbar's dropdown as first entry.                                                 |


## Folder view


| ID                    | Configurable properties           | Capabilities                              | Shown on mobile | Description                                                                                                                                                                            |
| --------------------- | --------------------------------- | ----------------------------------------- | --------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| mail-folderview-quota | enabled, upsellLimit | active_sync or caldav or carddav  | yes             | Located below the folder view without an icon by default. You can set the upsell limit in Bytes. If the maximum mail quota is larger than upsell limit, the trigger will not be shown. |
| folderview/mail       | enabled, icon, color, title       | active_sync                    | yes             | Located below the folder view of the mail app/module.                                                                                                                                  |
| folderview/contacts   | enabled, icon, color, title       | carddav                        | yes             | Located below the folder view of the addressbook app/module.                                                                                                                           |
| folderview/calendar   | enabled, icon, color, title       | caldav                         | yes             | Located below the folder view of the calendar app/module.                                                                                                                              |


## Misc

| ID                | Configurable properties            | Capabilities               | On mobile | Description                                                                                                                                                                                           |
| ----------------- | ---------------------------------- | -------------------------- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| client.onboarding | enabled, icon, color               |                            | shown     | Part of "Connect your device" wizard                                                                                                                                                                  |
| portal-widget     | enabled, icon, imageURL, removable | active_sync caldav carddav | shown     | A draggable portal widget. You can add a background image with 'imageURL'. If no image is used, the widget displays the text centered with a customizable space separated list of font-awesome icons. |



## Disable individual trigger

If you want to disable a custom trigger, you can add the following to the `.properties` file:

```js
io.ox/core//features/upsell/$id/enabled=false
```



# Customize appearance/strings

## Change default icon

All custom triggers have a 'bi-stars' as default icon. You can change the default icon to any icon from the [Bootstrap icons](https://icons.getbootstrap.com/).

```javascript
io.ox/core//upsell/defaultIcon="bi/stars.svg"
```

## Change single icon

You can replace the icon of individual trigger with

```javascript
io.ox/core//features/upsell/$id/icon="bi/stars.svg"
```
where '$id' is the id of the upsell trigger.

## Change color for individual trigger

You can change the color of some upsell trigger with

```javascript
io.ox/core//features/upsell/$id/color="#f00"
```
where '$id' is the id of the upsell trigger and the color can be any css color.

## Change text

Some of the custom triggers use a title (or other strings) which a hoster could customize. You can provide your own text via

```javascript
io.ox/core//features/upsell/$id/i18n/$lang/title="A custom title"
```
where '$lang' is the current language identifier (e.g. "en_US").
