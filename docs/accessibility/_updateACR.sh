#!/bin/zsh
version=$(git describe --abbrev=0)
date=$(date +"%Y-%m-%d")
last_modified_date=$(date +"%Y-%m-%d")

pnpx yaml-cli set acr.yaml report_date "$date" \
    | pnpx yaml-cli set - last_modified_date "$last_modified_date" \
    | pnpx yaml-cli set - product.version "$version" > acr-out.yaml
pnpm install
pnpm openacr
echo "---\ntitle: Accessibility Conformance Report\n---" | cat - acr.md > tmp && mv tmp acr.md
rm -rf acr-out.yaml
