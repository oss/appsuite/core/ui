---
title: Noteworthy changes
icon: fa-sticky-note
---

<!--
# General
# API
# Configuration
# Libraries/Dependencies
-->

# 8.33

[Full list of changes](https://gitlab.open-xchange.com/appsuite/web-apps/ui/-/compare/stable-8.32...stable-8.33)

## Consumed API

### Migrate to new Mail API endpoints

> Breaking Change

- [Issue 715](https://gitlab.open-xchange.com/appsuite/web-apps/ui/-/issues/715)
- Migrate to new Mail API endpoints for actions `move`, `flag`, `archive`and `copy`
- The new endpoints support multiple mail references in a single request, improving efficiency and performance.
- This change is also a prerequisite for enabling some of our upcoming features.
- Backward Compatibility
    - The old endpoints are used by default for middleware versions before version `8.33`.
    - If the old API is still needed, you can enable it by setting `io.ox/mail//useLegacyAPI` to `true`.


