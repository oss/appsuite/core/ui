---
title: Browser support
icon: fa-laptop
references:
  - title: iOS/ iPadOS / Apple Safari Versions
    url: https://support.apple.com/en-us/100100
    description: 'information'

  - title: Desktop Browser, Mobile OS & Browser Support
    url: https://gitlab.open-xchange.com/appsuite/operation-guides/-/blob/main/docs/requirements.md?ref_type=heads#mobile-os--browser-support-smartphone--tablet
    description: 'versioning'
---

# Browser support

To ensure a seamless experience with our product, it's crucial to be aware of the supported browsers and operating systems. This page serves as a comprehensive guide, outlining the platforms that are compatible with our software. Whether you're accessing our application from a desktop or mobile device, understanding the supported environments is key to optimizing performance and functionality. Please review the details below to ensure you're using a supported browser and operating system for the best user experience.

## Desktop

_Minimum display resolution: 1024 x 768_

| Supported Browser | Version                        |
|-------------------|--------------------------------|
| Google Chrome     | latest version                 |
| Mozilla Firefox   | latest & current ESR-Version   |
| Apple Safari      | 18.x                           | <!-- See references above for versioning. -->
| Microsoft Edge    | latest version, Chromium-based |

## Smartphone & Tablet

_Minimum display resolution: 640 x 360. The minimum screen size for mobile devices is ensured only for portrait (vertical) mode._

_Landscape (horizontal) mode is not explicitly supported, but we aim to ensure the basic functionality of most features whenever possible._

_Mobile network connection: Devices not connected a local wifi should at least use a 4G (LTE) internet connection for best results_

| Operating system | Version          | Supported Browser       |
|------------------|------------------|-------------------------|
| iOS / iPadOS     | >=17.7.1, >=18.x | Safari                  | <!-- See references above for versioning. -->
| Android          | >= 10            | Chrome (latest version) |

### Progressive Web App

A PWA, or Progressive Web App, is a modern web application that delivers a native app-like experience to users, right from their web browsers. It's designed to work seamlessly across various devices, providing a reliable, fast, and engaging user experience.

It's important to note that as of the current documentation, **Firefox does not fully support** Progressive Web Apps, and users may experience various limitations when utilizing our application on this browser.
