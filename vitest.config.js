/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

/// <reference types="vitest" />

import { defineConfig } from 'vite'
import { fileURLToPath, URL } from 'node:url'
import gettextPlugin from '@open-xchange/rollup-plugin-po2json'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      public: fileURLToPath(new URL('./public', import.meta.url)),
      'http://localhost:3000/': '/'
    }
  },
  test: {
    reporters: ['default', 'junit'],
    outputFile: 'output/junit.xml',
    environment: 'jsdom',
    include: ['spec/**/*_test.mjs'],
    setupFiles: ['spec/setup.js'],

    coverage: {
      provider: 'v8',
      reporter: process.env.CI ? ['cobertura', 'text-summary'] : ['text', 'cobertura', 'text-summary'],
      reportsDirectory: 'output/coverage',
      include: ['src/**/*.js']
    }
  },
  plugins: [
    vue(),
    gettextPlugin({
      poFiles: 'src/i18n/*.po',
      outFile: 'ox.pot',
      defaultDictionary: 'io.ox/core',
      defaultLanguage: 'en_US'
    })
  ]
})
