#!/usr/bin/env node

/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const fs = require('node:fs')
const util = require('node:util')
const dotenv = require('dotenv')
const contextService = require('@open-xchange/appsuite-codeceptjs/src/soap/services/context')
const userService = require('@open-xchange/appsuite-codeceptjs/src/soap/services/user')
dotenv.config({ path: '.env' })
dotenv.config({ path: '.env.defaults' })

const userPath = process.env.DEFAULT_USER_PATH_HC || process.env.DEFAULT_USER_PATH
const PMUserPath = process.env.PM_USER_PATH

const mxDomain = process.env.MX_DOMAIN || 'box.ox.io'
const additionalCaps = process.env.PROVISIONING_CAPS ? process.env.PROVISIONING_CAPS.split(',') : ['switchboard', 'ai-service', 'spreadsheet', 'text', 'document_preview', 'presentation', 'presenter', 'remote_presenter']
const userAttributes = {
  entries: [{
    key: 'config',
    value: {
      entries: [
        { key: 'io.ox/core//ai/hostname', value: 'ai-service-main.dev.oxui.de' },
        { key: 'io.ox/core//ai/openai/useAzure', value: 'false' }
      ]
    }
  }]
}

const admin = {
  login: 'oxadmin',
  password: process.env.E2E_ADMIN_PASSWORD || 'secret'
}

function provisionUsersIfPathExists (context, userPath) {
  try {
    if (fs.existsSync(userPath)) {
      provisionUsers(context, userPath)
    }
  } catch (error) {
    console.error(error)
  }
}

async function createContext (contextData) {
  let createdContext
  try {
    createdContext = await contextService.create(contextData, { password: admin.password })
    console.log('Context created', createdContext)

    await contextService.changeModuleAccessByName(createdContext.id, 'all').catch(err => {
      console.log('access failed')
      console.error(err)
    })
  } catch (error) {
    if (error.message.includes('already exists')) [createdContext] = await contextService.list(contextData.loginMappings)
    else console.error(error)
  }
  return createdContext
}

const provisionUsers = async (context, userPath) => {
  const readFile = util.promisify(fs.readFile)
  const users = JSON.parse(String(await readFile(userPath)))

  await Promise.all([
    users.map(user => {
      const specificUser = Object.assign({
        password: user.password,
        primaryEmail: `${user.name}@${mxDomain}`,
        email1: `${user.name}@${mxDomain}`,
        display_name: `${user.given_name} ${user.sur_name}`,
        imapLogin: user.name,
        imapServer: process.env.IMAP_SERVER,
        smtpServer: process.env.SMTP_SERVER
      }, user)
      return userService.create({ ...{ id: context.id }, ...{ admin } }, specificUser)
        .catch(err => {
          if (!err.message.includes('is already used') && !err.message.includes('already exists')) console.error(err)
        })
    })
  ])
}

const changeContext = async (contextId) => {
  await Promise.all([
    additionalCaps.map(capsToAdd => {
      return contextService.changeCapabilities(contextId, capsToAdd, undefined).catch(err => {
        console.error(err)
      })
    })
  ])

  await contextService.change({ id: contextId, userAttributes })
}

(async () => {
  const defaultContext = await createContext({ loginMappings: 'defaultcontext', maxQuota: -1 })
  const pmContext = await createContext({ loginMappings: 'pm', maxQuota: -1 })
  await changeContext(defaultContext.id)
  await changeContext(pmContext.id)

  provisionUsersIfPathExists(defaultContext, userPath)
  provisionUsersIfPathExists(pmContext, PMUserPath)
})()
