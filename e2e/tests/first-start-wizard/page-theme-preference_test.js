/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > First Start Wizard > Theme preference')
Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Page: Theme preference', async ({ I, users }) => {
  await I.haveSetting({
    'io.ox/core': { features: { firstStartWizard: true } }
  })

  await I.login('app=io.ox/mail')

  I.waitForText('Welcome to OX App Suite')
  I.click('Get started')
  I.waitForText('Select your language and time zone')
  I.click('Next')
  I.waitForText('Tell us about yourself')
  I.click('Next')
  I.waitForText('Theme preference')

  I.see('Dark')
  I.see('Light')
  I.see('System')
  I.dontSeeElement('html.dark')
  I.click('Dark')
  I.waitForElement('html.dark')
  I.click('~Yellow')
  const buttonColor1 = await I.grabCssPropertyFrom('.btn-primary', 'background-color')
  expect(buttonColor1).to.equal('rgb(153, 102, 0)')
  I.pressKey('Escape')
  I.waitForSetting({ 'io.ox/core': { theming: { current: { theme: 'dark' } } } })
  I.logout()
  await I.login('app=io.ox/mail')
  I.waitForElement('html.dark')
  const buttonColor2 = await I.grabCssPropertyFrom('.btn-primary', 'background-color')
  expect(buttonColor2).to.equal('rgb(153, 102, 0)')
})
