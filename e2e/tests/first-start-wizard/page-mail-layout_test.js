/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > First Start Wizard > Mail Layout')
Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Page: Mail layout', async ({ I, users }) => {
  await I.haveSetting({
    'io.ox/core': { features: { firstStartWizard: true } }
  })

  await I.login('app=io.ox/mail')

  I.waitForText('Welcome to OX App Suite')
  I.click('Get started')
  I.waitForText('Select your language and time zone')
  I.click('Next')
  I.waitForText('Tell us about yourself')
  I.click('Next')
  I.waitForText('Theme preference')
  I.click('Next')
  I.waitForText('Mail Layout')

  I.see('Vertical')
  I.see('Horizontal')
  I.see('List')
  I.waitForElement('.preview-right')

  I.click('Vertical')
  I.waitForElement('.framemaillistvertical')
  I.waitForElement('.preview-right')

  I.click('Horizontal')
  I.waitForElement('.framemaillisthorizontal')
  I.waitForElement('.preview-bottom')

  I.click('List')
  I.waitForElement('.preview-none')
  I.waitForElement('.framemaillistlist')
  I.waitForSetting({ 'io.ox/mail': { layout: 'list' } })

  I.pressKey('Escape')
  I.logout()
  await I.login('app=io.ox/mail')
  I.waitForElement('.preview-none')
})
