/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > First Start Wizard > Personal information')
Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Page: Personal information', async ({ I, users, mail }) => {
  await I.haveSetting({
    'io.ox/core': { features: { firstStartWizard: true } }
  })

  await I.login('app=io.ox/mail')

  I.waitForText('Welcome to OX App Suite')
  I.click('Get started')
  I.waitForText('Select your language and time zone')

  I.click('Next')
  I.waitForText('Tell us about yourself')

  const firstName = users[0].get('given_name')
  const lastName = users[0].get('sur_name')
  const email = users[0].get('email1')
  const abbr = `${firstName[0].toUpperCase()}${lastName[0].toUpperCase()}`

  I.see(`${firstName} ${lastName}`, '.wizard-side-illustration')
  I.see(email, '.wizard-side-illustration')
  I.see(abbr, '.wizard-side-illustration')

  I.seeInField('First name', firstName)
  I.seeInField('Last name', lastName)

  I.fillField('First name', 'John')
  I.fillField('Last name', 'Doe')
  I.seeInField('Email display name', '')
  const placeholder = await I.grabAttributeFrom('input.displayname', 'placeholder')
  expect(placeholder).to.equal('John Doe')

  I.see('John Doe', '.wizard-side-illustration')

  // I.waitForElement('.vue-avatar svg')
  // I.dontSeeElement('.vue-avatar span')
  // I.dontSeeElement('.wizard-side-illustration img')
  // I.click('~Change user photo')
  // I.waitForText('Change contact photo')
  // I.attachFile('input[type="file"]', 'media/images/ox_logo.png')
  // I.click('Apply')
  // I.waitForDetached('.edit-picture')
  // I.waitForElement('.vue-avatar span')
  // I.waitForElement('.wizard-side-illustration img')

  I.pressKey('Escape')
  I.waitForInvisible('.first-start-wizard-modal')

  await mail.newMail()

  I.seeInField('.mail-input', 'John Doe')
  I.pressKey('Tab')
  I.pressKey('Escape')
  I.waitForDetached('.io-ox-mail-compose')

  I.click('~Help')
  I.clickDropdown('Restart initial setup')

  I.waitForText('Welcome to OX App Suite')
  I.click('Get started')
  I.waitForText('Select your language and time zone')

  I.click('Next')
  I.waitForText('Tell us about yourself')
  I.fillField('Email display name', 'Email Master')
  I.see('Email Master', '.wizard-side-illustration')

  I.pressKey('Escape')

  I.waitForInvisible('.first-start-wizard-modal')

  await mail.newMail()
  I.seeInField('.mail-input', 'Email Master')
})
