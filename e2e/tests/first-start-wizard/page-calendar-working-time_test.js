/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > First Start Wizard > Calendar working time')
Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Page: Calendar working time', async ({ I, settings, calendar }) => {
  await I.haveSetting({
    'io.ox/core': { features: { firstStartWizard: true } }
  })

  await I.login('app=io.ox/calendar')

  I.waitForText('Welcome to OX App Suite')
  I.click('Get started')
  I.waitForText('Select your language and time zone')
  I.click('Next')
  I.waitForText('Tell us about yourself')
  I.click('Next')
  I.waitForText('Theme preference')
  I.click('Next')
  I.waitForText('Mail Layout')
  I.click('Next')
  I.waitForText('Working time')

  I.see('Start')
  I.see('End')
  I.see('First day')
  I.see('Length')

  I.selectOption('Start', '11:00 AM')
  I.selectOption('End', '1:00 PM')
  I.selectOption('First day', 'Thursday')
  I.selectOption('Length', '4 days')
  I.see('Thu', '.wizard-side-illustration')
  I.see('Fri', '.wizard-side-illustration')

  I.pressKey('Escape')

  calendar.switchView('Workweek')
  I.seeNumberOfElements('.weekview-toolbar button', 4)
  I.see('Thu', '.weekview-toolbar')
  I.see('Fri', '.weekview-toolbar')
  I.see('Sat', '.weekview-toolbar')
  I.see('Sun', '.weekview-toolbar')
  I.dontSee('Mon', '.weekview-toolbar')
  I.dontSee('Tue', '.weekview-toolbar')
  I.dontSee('Wed', '.weekview-toolbar')
  settings.open('io.ox/calendar', 'Your week')
  I.waitForText('Working Time')
  const startTime = await I.grabValueFrom('select[name="startTime"]')
  expect(startTime).to.equal('11')
  const endTime = await I.grabValueFrom('select[name="endTime"]')
  expect(endTime).to.equal('13')

  // make sure settings are saved
  I.logout()
  await I.login('app=io.ox/calendar')
  I.seeNumberOfElements('.weekview-toolbar button', 4)
})
