/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > First Start Wizard')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Show First Start Wizard on first start', async ({ I, dialogs }) => {
  // run disabled
  await I.haveSetting({ 'io.ox/core': { features: { firstStartWizard: false } } })
  await I.login('app=io.ox/mail')
  I.dontSee('Welcome to OX App Suite')
  I.logout()

  // run enabled (one remaining)
  await I.haveSetting({ 'io.ox/core': { features: { firstStartWizard: true } } })
  await I.login('app=io.ox/mail')
  I.waitForText('Welcome to OX App Suite')
  I.pressKey('Escape')
  I.waitForInvisible('.first-start-wizard-modal')
  I.waitForNetworkTraffic()
  I.waitForSetting({ 'io.ox/core': { autoopen: { 'first-start-wizard': { remaining: 0 } } } })
  await I.logout()

  // run enabled (none remaining)
  await I.login('app=io.ox/mail')
  I.dontSee('Welcome to OX App Suite')
})

Scenario('Open and close', async ({ I, users, topbar }) => {
  await I.haveSetting({
    'io.ox/core': { features: { firstStartWizard: true } }
  })

  await I.login('app=io.ox/mail')

  I.waitForText('Welcome to OX App Suite')
  I.waitForText('Get started')
  I.waitForText('No thanks')
  I.click('No thanks')
  I.waitForInvisible('.first-start-wizard-modal')

  I.click('~Help')
  I.clickDropdown('Restart initial setup')

  I.waitForText('Welcome to OX App Suite')
  I.pressKey('Escape')
  I.waitForInvisible('.first-start-wizard-modal')

  I.click('~Help')
  I.clickDropdown('Restart initial setup')

  I.waitForText('Welcome to OX App Suite')
  I.click('Get started')
  I.waitForText('1/')
  I.pressKey('Escape')
  I.waitForInvisible('.first-start-wizard-modal')
})
