/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Accessibility')

Before(async ({ users }) => {
  await users.create()
  await users[0].hasCapability('document_preview')
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Viewer - standard pdf document', async ({ I, drive }) => {
  const folder = await I.grabDefaultFolder('infostore')
  await I.haveFile(folder, 'media/files/generic/pdf_document_1.pdf')

  await I.login('app=io.ox/files')

  drive.selectFile('pdf_document_1.pdf')
  I.clickToolbar('View')

  I.waitForElement('.io-ox-viewer')
  I.waitForText('doc-1-608048216047687')

  expect(await I.grabAxeReport()).to.be.accessible
})

Scenario('Viewer - standalone pdf document', async ({ I, drive }) => {
  const disableRules = ['empty-heading']
  const folder = await I.grabDefaultFolder('infostore')
  await I.haveFile(folder, 'media/files/generic/pdf_document_1.pdf')

  await I.login('app=io.ox/files')

  drive.selectFile('pdf_document_1.pdf')
  I.clickToolbar('View')

  I.waitForElement('.io-ox-viewer')
  I.waitForText('doc-1-608048216047687')
  I.click('~Pop out standalone viewer')

  I.wait(0.5)
  I.switchToNextTab()
  I.waitForElement('.io-ox-viewer')
  I.waitForText('doc-1-608048216047687')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})
