/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Drive > Folder')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Show sidepanel for folders', async ({ I, settings, drive }) => {
  await I.haveSetting('io.ox/files//showDetails', true)
  await I.login('app=io.ox/files')
  await I.waitForApp()

  drive.selectFile('Documents')
  I.waitForText('Documents', undefined, '.viewer-sidebar-pane .filename')
  I.waitForText('General', undefined, '.viewer-sidebar-pane .sidebar-panel-heading')
  I.waitForText('Shares', undefined, '.viewer-sidebar-pane .sidebar-panel-heading')
})

Scenario('Show sidepanel for folders in search result', async ({ I, settings, drive }) => {
  await I.haveSetting('io.ox/files//showDetails', true)
  await I.login('app=io.ox/files')
  await I.waitForApp()

  I.fillField('.search-field', 'Documents')
  I.pressKey('Enter')
  I.waitForText('Search results')
  I.waitNumberOfVisibleElements('.list-item.file-type-folder', 1)

  drive.selectFile('Documents')
  I.waitForText('Documents', undefined, '.viewer-sidebar-pane .filename')
  I.waitForText('General', undefined, '.viewer-sidebar-pane .sidebar-panel-heading')
  I.waitForText('Shares', undefined, '.viewer-sidebar-pane .sidebar-panel-heading')
})
