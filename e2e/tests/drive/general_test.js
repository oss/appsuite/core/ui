/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const fs = require('node:fs')
const util = require('node:util')
const readdir = util.promisify(fs.readdir)

Feature('Drive > General')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C8362] Add note', async ({ I, drive }) => {
  await I.login('app=io.ox/files')

  drive.clickSecondary('New note')
  I.waitForElement({ css: 'input[type="text"].title' })
  I.fillField('Title', 'Button save')
  I.fillField('Note', 'Test body')
  I.click('Save')
  I.waitForNetworkTraffic()
  I.click('Close')
  I.waitForDetached('textarea.content')
  I.waitForElement('.filename[title="Button save.txt"]')

  drive.clickSecondary('New note')
  I.waitForElement({ css: 'input[type="text"].title' })
  I.fillField('Title', 'Shortcut save')
  I.fillField('Note', 'Test body')
  I.pressKey(['CommandOrControl', 'Enter'])
  I.waitForNetworkTraffic()
  I.click('Close')
  I.waitForDetached('textarea.content')
  I.waitForElement('.filename[title="Shortcut save.txt"]')
})

// Bug: File input is not selectable (display: none), which is also a pot. a11y bug
Scenario('[C8364] Upload new file @smoketest', async ({ I, drive }) => {
  await I.login('app=io.ox/files')

  drive.clickSecondary('Upload files')
  // the input field is created on demand when Upload files is clicked. This click also closes the dropdown
  I.attachFile('.primary-action .dropdown input[name=file]', 'media/files/0kb/document.txt')
  I.waitForElement('.filename[title="document.txt"]')
  I.waitForElement('.list-item.selected .filename[title="document.txt"]')
})

// Note: This is not accessible H4 and textarea does not have a label
Scenario('[C8366] Edit description', async ({ I, drive, dialogs }) => {
  await I.haveFile(await I.grabDefaultFolder('infostore'), 'media/files/0kb/document.txt')
  await I.login('app=io.ox/files')

  const sidebarDescription = locate('.viewer-sidebar-pane .sidebar-panel-body .description').as('Sidebar')
  const descriptionTextarea = dialogs.main.find('textarea.form-control')

  drive.selectFile('document.txt')

  I.say('Add description')
  I.waitForText('Description')
  I.click(locate('.sidebar-panel-title').withText('Description'))
  I.waitForElement('~Edit description')
  I.click('~Edit description')
  dialogs.waitForVisible()
  I.waitForElement(descriptionTextarea)
  I.fillField(descriptionTextarea, 'Test description')
  dialogs.clickButton('Save')
  I.waitForDetached('.modal-dialog')

  I.say('Check description #1')
  I.waitForElement({ xpath: '//div[contains(@class, "viewer-sidebar-pane")]//*[text()="Test description"]' })

  I.say('Edit description')
  drive.selectFile('document.txt')
  I.waitForElement('~Edit description')
  I.click('button[data-action="edit-description"]')
  dialogs.waitForVisible()
  I.fillField(descriptionTextarea, 'Test description changed')
  dialogs.clickButton('Save')
  I.waitForDetached('.modal-dialog')

  I.say('Check description #2')
  I.waitForVisible(sidebarDescription)
  I.waitForText('Test description changed', undefined, sidebarDescription)
})

Scenario('[C8368] View change @bug', async ({ I }) => {
  await I.haveFile(await I.grabDefaultFolder('infostore'), 'media/files/0kb/document.txt')
  await I.login('app=io.ox/files')

  I.see('Documents')
  I.see('Music')
  I.see('Pictures')
  I.see('Videos')
  I.waitForElement('.filename[title="document.txt"]')

  I.click('~Settings')
  I.waitForElement('.dropdown.open')
  I.clickDropdown('Icons')
  I.waitForElement('.file-list-view.complete.grid-layout')
  I.see('Documents', '.grid-layout')
  I.see('Music', '.grid-layout')
  I.see('Pictures', '.grid-layout')
  I.see('Videos', '.grid-layout')
  I.waitForElement('.filename[title="document.txt"]')

  I.click('~Settings')
  I.waitForElement('.dropdown.open')
  I.clickDropdown('Tiles')
  I.waitForElement('.file-list-view.complete.tile-layout')
  I.see('Documents', '.tile-layout')
  I.see('Music', '.tile-layout')
  I.see('Pictures', '.tile-layout')
  I.see('Videos', '.tile-layout')

  I.click('~Settings')
  I.waitForElement('.dropdown.open')
  I.clickDropdown('List')
  I.waitForElement('.file-list-view.complete')
  I.see('Documents')
  I.see('Music')
  I.see('Pictures')
  I.see('Videos')
  I.waitForElement('.filename[title="document.txt"]')
})

Scenario('[C8369] Search', async ({ I }) => {
  const searchFor = async (query) => {
    I.click('.search-field')
    await I.waitForFocus('.search-field')
    I.fillField('.search-field', query)
    I.pressKey('Enter')
    I.waitForDetached('.busy-indicator.io-ox-busy')
  }

  const folder = await I.grabDefaultFolder('infostore')
  const testFolder = await I.haveFolder({ title: 'Testfolder', module: 'infostore', parent: folder })
  await Promise.all([
    I.haveFile(testFolder, 'media/files/0kb/document.txt'),
    I.haveFile(testFolder, 'media/files/generic/testdocument.rtf'),
    I.haveFile(testFolder, 'media/files/generic/testdocument.odt'),
    I.haveFile(testFolder, 'media/files/generic/testpresentation.ppsm')
  ])
  await I.login('app=io.ox/files')

  I.selectFolder('Testfolder')
  // search with file name
  await searchFor('document.txt')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 1)
  // search with noneexisting
  I.clearField('.search-field')
  await searchFor('noneexisting')
  I.dontSee('.file-list-view .list-item')
  I.waitForText('No search results', undefined, '.file-list-view')
  // search with d
  I.clearField('.search-field')
  await searchFor('d')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 7)
  // search with do
  I.clearField('.search-field')
  await searchFor('do')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 4)
})

Scenario('[C8371] Delete file', async ({ I, drive }) => {
  const folder = await I.grabDefaultFolder('infostore')
  await I.haveFile(folder, 'media/files/0kb/document.txt')
  await I.login('app=io.ox/files')

  drive.selectFile('document.txt')
  I.clickToolbar('Delete')
  I.waitForText('Do you really want to delete this item?')
  I.click('Delete')
  I.selectFolder('Trash')
  drive.selectFile('document.txt')
  I.clickToolbar('Delete forever')
  I.waitForText('Do you really want to delete this item?')
  I.click('Delete')
  I.waitForDetached('.filename[title="document.txt"]')
})

Scenario('[C45039] Breadcrumb navigation', async ({ I }) => {
  const parent = await I.haveFolder({ title: 'Folders', module: 'infostore', parent: await I.grabDefaultFolder('infostore') })
  await Promise.all([
    I.haveFolder({ title: 'subfolder1', module: 'infostore', parent }),
    I.haveFolder({ title: 'subfolder2', module: 'infostore', parent })
  ])
  const subFolder = await I.haveFolder({ title: 'subfolder3', module: 'infostore', parent })
  const subsubFolder = await I.haveFolder({ title: 'subsubfolder1', module: 'infostore', parent: subFolder })
  await I.haveFolder({ title: 'subsubfolder2', module: 'infostore', parent: subFolder })
  await I.login('app=io.ox/files&folder=' + subsubFolder)

  I.waitForNetworkTraffic()
  I.waitForElement({ xpath: '//a[text()="subfolder3"][@role="button"]' })
  I.click({ xpath: '//a[text()="subfolder3"][@role="button"]' })
  await I.waitForApp()
  I.waitForText('subsubfolder1', undefined, '.list-view')
  I.waitForText('subsubfolder2', undefined, '.list-view')
  I.waitForElement({ xpath: '//div[text()="subsubfolder2"]' })
  I.click({ xpath: '//div[text()="subsubfolder2"]' })
  I.click('Drive', '.breadcrumb-view')
  await I.waitForApp()
  I.waitForElement({ xpath: '//div[text()="Public files"]' })
  I.doubleClick({ xpath: '//div[text()="Public files"]' })
})

Scenario('[C45040] Sort files', async ({ I }) => {
  const folder = await I.grabDefaultFolder('infostore')
  const testFolder = await I.haveFolder({ title: 'Testfolder', module: 'infostore', parent: folder })
  await Promise.all([
    I.haveFile(testFolder, 'media/files/0kb/document.txt'),
    I.haveFile(testFolder, 'media/files/generic/testdocument.rtf'),
    I.haveFile(testFolder, 'media/files/generic/testdocument.odt'),
    I.haveFile(testFolder, 'media/files/generic/testpresentation.ppsm')
  ])
  await I.login('app=io.ox/files')

  I.wait(1)
  I.selectFolder('Testfolder')
  I.waitForDetached('.io-ox-busy')
  I.waitForElement('.filename[title="document.txt"]')

  // Begins with Name ascending order
  I.say('Name > Ascending')
  I.wait(1) // Wait for the sorting to be done
  I.waitForElement(locate('.list-item').at(1).withText('document.txt').as('document.txt at 1'))
  I.waitForElement(locate('.list-item').at(2).withText('testdocument.odt').as('testdocument.odt at 2'))
  I.waitForElement(locate('.list-item').at(3).withText('testdocument.rtf').as('testdocument.rtf at 3'))
  I.waitForElement(locate('.list-item').at(4).withText('testpresentation.ppsm').as('testpresentation.ppsm at 4'))
  I.wait(0.2)
  I.click('Sort by')
  I.click('Descending')
  I.waitForDetached('.io-ox-busy')
  I.say('Name > Descending')
  I.wait(1) // Wait for the sorting to be done
  I.waitForElement(locate('.list-item').at(1).withText('testpresentation.ppsm').as('testpresentation.ppsm at 1'))
  I.waitForElement(locate('.list-item').at(2).withText('testdocument.rtf').as('testdocument.rtf at 2'))
  I.waitForElement(locate('.list-item').at(3).withText('testdocument.odt').as('testdocument.odt at 3'))
  I.waitForElement(locate('.list-item').at(4).withText('document.txt').as('document.txt at 4'))

  I.wait(0.2)
  I.click('Sort by')
  I.click('Date')
  I.waitForDetached('.io-ox-busy')
  I.say('Date > Descending')
  I.wait(1) // Wait for the sorting to be done
  I.waitForElement(locate('.list-item').at(1).withText('testpresentation.ppsm').as('testpresentation.ppsm at 1'))
  I.waitForElement(locate('.list-item').at(2).withText('testdocument.odt').as('testdocument.odt at 2'))
  I.waitForElement(locate('.list-item').at(3).withText('testdocument.rtf').as('testdocument.rtf at 3'))
  I.waitForElement(locate('.list-item').at(4).withText('document.txt').as('document.txt at 4'))

  I.wait(0.2)
  I.click('Sort by')
  I.click('Ascending')
  I.waitForDetached('.io-ox-busy')
  I.say('Date > Ascending')
  I.wait(1) // Wait for the sorting to be done
  I.waitForElement(locate('.list-item').at(1).withText('document.txt').as('document.txt at 1'))
  I.waitForElement(locate('.list-item').at(2).withText('testdocument.rtf').as('testdocument.rtf at 2'))
  I.waitForElement(locate('.list-item').at(3).withText('testdocument.odt').as('testdocument.odt at 3'))
  I.waitForElement(locate('.list-item').at(4).withText('testpresentation.ppsm').as('testpresentation.ppsm at 4'))

  I.wait(0.2)
  I.click('Sort by')
  I.click('Size')
  I.waitForDetached('.io-ox-busy')
  I.say('Size > Ascending')
  I.wait(1) // Wait for the sorting to be done
  I.waitForElement(locate('.list-item').at(1).withText('document.txt').as('document.txt at 1'))
  I.waitForElement(locate('.list-item').at(2).withText('testdocument.odt').as('testdocument.odt at 2'))
  I.waitForElement(locate('.list-item').at(3).withText('testpresentation.ppsm').as('testpresentation.ppsm at 3'))
  I.waitForElement(locate('.list-item').at(4).withText('testdocument.rtf').as('testdocument.rtf at 4'))
  I.wait(0.2)
  I.click('Sort by')
  I.click('Descending')
  I.waitForDetached('.io-ox-busy')
  I.say('Size > Descending')
  I.wait(1) // Wait for the sorting to be done
  I.waitForElement(locate('.list-item').at(1).withText('testdocument.rtf').as('testdocument.rtf at 1'))
  I.waitForElement(locate('.list-item').at(2).withText('testpresentation.ppsm').as('testpresentation.ppsm at 2'))
  I.waitForElement(locate('.list-item').at(3).withText('testdocument.odt').as('testdocument.odt at 3'))
  I.waitForElement(locate('.list-item').at(4).withText('document.txt').as('document.txt at 4'))
})

Scenario('[C45041] Select files', async ({ I }) => {
  const testFolder = await I.haveFolder({ title: 'Selecttest', module: 'infostore', parent: await I.grabDefaultFolder('infostore') })
  const filePath = 'media/files/0kb/'
  const files = await readdir(filePath)

  await I.haveFolder({ title: 'Subfolder', module: 'infostore', parent: testFolder })

  await Promise.all(files.filter(name => name !== '.DS_Store').map(name => I.haveFile(testFolder, filePath + name)))
  console.log('Files created', testFolder, files.length)

  await I.login(`app=io.ox/files&folder=${testFolder}`)
  I.waitForText('Selecttest', undefined, '.breadcrumb-view')
  I.click('Select')
  await I.clickDropdown('All')
  I.waitNumberOfVisibleElements('.file-list-view .list-item.selected', 23)

  I.click('Select')
  await I.clickDropdown('All files')
  I.waitNumberOfVisibleElements('.file-list-view .list-item.selected', 22)

  I.click('Select')
  await I.clickDropdown('None')
  I.dontSeeElementInDOM('.file-list-view .list-item.selected')
})

Scenario('[C45042] Filter files', async ({ I }) => {
  const testFolder = await I.haveFolder({ title: 'Filtertest', module: 'infostore', parent: await I.grabDefaultFolder('infostore') })
  const filePath = 'media/files/0kb/'
  const files = await readdir(filePath)

  await Promise.all(files.filter(name => name !== '.DS_Store').map(name => I.haveFile(testFolder, filePath + name)))

  await I.login('app=io.ox/files')

  I.selectFolder('Filtertest')
  I.click('Select')
  I.click('PDFs')
  I.waitForElement('.filename[title="document.pdf"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 1)
  I.click('Select')
  I.click('Text documents')
  I.waitForElement('.filename[title="document.doc"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 5)
  I.click('Select')
  I.click('Spreadsheets')
  I.waitForElement('.filename[title="spreadsheet.xls"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 2)
  I.click('Select')
  I.click('Presentations')
  I.waitForElement('.filename[title="presentation.ppsm"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 2)
  I.click('Select')
  I.click('Images')
  I.waitForElement('.filename[title="image.gif"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 3)
  I.click('Select')
  I.click('Music')
  I.waitForElement('.filename[title="music.mp3"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 5)
  I.click('Select')
  I.click('Videos')
  I.waitForElement('.filename[title="video.avi"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 4)
  I.click('Select')
  // Read comment at the beginning of the scenario to find out why
  // the following selector is so clunky
  I.click({ css: 'a[data-name="filter"][data-value="all"]' })
  I.waitForElement('.filename[title="document.doc"]')
  I.waitNumberOfVisibleElements('.file-list-view .list-item', 22)
})

// Bug: File input is not selectable (display: none), which is also a pot. a11y bug
Scenario('[Bug 63288] Cancel upload does not work in drive', async ({ I, drive }) => {
  await I.login('app=io.ox/files')

  drive.clickSecondary('Upload files')
  // slow down network so we can click the cancel upload button
  await I.throttleNetwork('2G')
  // the input field is created on demand when Upload files is clicked. This click also closes the dropdown
  await I.createGenericFile('2MB.dat', 2 * 1024 * 1024)
  I.attachFile('.primary-action .dropdown input[name=file]', 'media/files/generic/2MB.dat')
  I.waitForText('Cancel')
  I.click('Cancel')
  // reset network speed
  await I.throttleNetwork('ONLINE')
  I.waitForDetached('.upload-wrapper')
  I.dontSee('2MB.dat')
})

Scenario('Upload and view an image @smoketest', async ({ I, drive }) => {
  await I.login('app=io.ox/files')

  drive.clickSecondary('Upload files')
  // the input field is created on demand when Upload files is clicked. This click also closes the dropdown
  I.attachFile('.primary-action .dropdown input[name=file]', 'media/images/ox_logo.png')
  I.waitForElement('.filename[title="ox_logo.png"]')
  I.waitForElement('.list-item.selected .filename[title="ox_logo.png"]')

  // view the image
  I.clickToolbar('View')
  I.waitForVisible('.io-ox-viewer')
  I.see('ox_logo.png')
  I.waitForElement('.viewer-sidebar.open')
  I.waitForElement('.viewer-toolbar')
  I.waitForElement('.viewer-displayer-image')
  I.click('Close viewer')
  I.waitForDetached('.io-ox-viewer')
})
