/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Calendar > Create')

Before(async ({ users }) => {
  await Promise.all([users.create(), users.create()])
})

After(async ({ users }) => { await users.removeAll() })

Scenario('[C264519] Create appointments with colors in public folder', async ({ I, users, calendar, dialogs }) => {
  await I.haveSetting('io.ox/calendar//categoryColorAppointments', false)
  await I.login('app=io.ox/calendar')

  I.say('Create public calendar')
  I.waitForElement('~Folder-specific actions')
  I.click('~Folder-specific actions')
  I.clickDropdown('Add new calendar')

  dialogs.waitForVisible()
  I.waitForText('Add as public calendar', undefined, dialogs.body)
  I.checkOption('Add as public calendar', dialogs.body)
  dialogs.clickButton('Add')
  I.waitForDetached('.modal-dialog')

  I.say('Grant permission to user b')
  I.click(locate('.folder-arrow').inside('~Public calendars').as('.folder-arrow'))
  I.rightClick('~New calendar')

  I.clickDropdown('Share / Permissions')
  dialogs.waitForVisible()
  I.waitForElement('.form-control.tt-input')
  I.fillField('.form-control.tt-input', users[1].get('primaryEmail'))
  I.pressKey('Enter')
  dialogs.clickButton('Save')
  I.waitForDetached('.modal-dialog')

  // create 2 test appointments with different colors
  I.clickPrimary('New appointment')
  I.waitForText('Appointments in public calendar')
  I.click('Create in public calendar')
  I.waitForVisible(calendar.editWindow)
  await I.waitForFocus('.io-ox-calendar-edit-window input[type="text"][name="summary"]')
  await within(calendar.editWindow, () => {
    I.wait(1)
    I.fillField('Title', 'testing is fun')
    calendar.startNextMonday()
    I.see('New calendar', '.folder-selection')
    I.click('~Start time')
    I.click('8:00 AM')
    I.click('Appointment color', '.color-picker-dropdown')
    I.waitForElement('.color-picker-dropdown.open')
    I.click(locate('a').inside('.color-picker-dropdown.open').withAttr({ title: 'Green' }).as('Green'))
    I.click('Create')
  })
  I.waitForDetached('.io-ox-calendar-edit-window')

  I.clickPrimary('New appointment')
  I.waitForText('Appointments in public calendar')
  I.click('Create in public calendar')
  I.waitForVisible(calendar.editWindow)
  await I.waitForFocus('.io-ox-calendar-edit-window input[type="text"][name="summary"]')
  await within(calendar.editWindow, () => {
    I.fillField('Title', 'testing is awesome')
    calendar.startNextMonday()
    I.see('New calendar', '.folder-selection')
    I.click('~Start time')
    I.click('10:00 AM')
    I.click('Appointment color', '.color-picker-dropdown')
    I.waitForElement('.color-picker-dropdown.open')
    I.click(locate('a').inside('.color-picker-dropdown.open').withAttr({ title: 'Red' }).as('Red'))
    I.click('Create')
  })
  I.waitForDetached('.io-ox-calendar-edit-window')

  await I.logout()

  // Login user b
  await I.login('app=io.ox/calendar', { user: users[1] })

  calendar.moveCalendarViewToNextWeek()
  I.click(locate('.folder-arrow').inside('~Public calendars').as('.folder-arrow'))
  I.doubleClick('~New calendar')
  // check if public appointments are there
  I.waitForText('testing is fun', 10, '.workweek')
  I.waitForText('testing is awesome', 10, '.workweek')
  // see if appointment colors still drawn with customized color (See Bug 65410)
  const appointment1 = await I.grabCssPropertyFrom('~testing is fun', 'background-color')
  const appointment2 = await I.grabCssPropertyFrom('~testing is awesome', 'background-color')

  assert.equal(appointment1, 'rgb(221, 247, 212)')
  assert.equal(appointment2, 'rgb(255, 204, 219)')
})

Scenario('[OXUIB-1143] Keep contact preview when changing appointment color', async ({ I, users, mail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]

  await I.haveMail({ from: sender, to: sender, subject: 'Test subject!' })

  await I.login('app=io.ox/mail')

  await mail.selectMailByIndex(0)
  I.click('.person-link.person-from')
  I.waitForElement('[data-action="io.ox/contacts/actions/invite"]')
  I.click('[data-action="io.ox/contacts/actions/invite"]')
  I.waitForElement('.color-picker-dropdown.dropdown')
  I.click('.color-picker-dropdown.dropdown')
  I.click('[data-value="#ff2968"]')
  I.seeElement('.io-ox-halo')
})
