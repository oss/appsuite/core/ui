/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Calendar > Notification area')

Before(async ({ I, users }) => {
  await Promise.all([users.create(), users.create()])
})

After(async ({ I, users }) => { await users.removeAll() })

Scenario('Event can be removed from notification area', async ({ I, users, calendar }) => {
  const startTime = calendar.getNextMonday()
  await I.haveAppointment({
    summary: 'Test',
    startDate: { value: startTime },
    endDate: { value: startTime.add(1, 'hour') },
    organizer: {
      cn: users[1].get('display_name'),
      email: users[1].get('email1'),
      uri: `mailto:${users[1].get('email1')}`
    },
    attendees: [
      { partStat: 'NEEDS-ACTION', cuType: 'INDIVIDUAL', entity: users[0].get('id'), cn: users[0].get('display_name'), uri: `mailto:${users[0].get('email1')}` }
    ]
  })

  await I.login('app=io.ox/calendar')

  I.waitForElement('#io-ox-notifications-toggle')
  I.click('#io-ox-notifications-toggle')
  I.waitForElement('.io-ox-notifications.visible')
  I.waitForText('Test', 10, '.io-ox-notifications')
  I.click('Test', '.io-ox-notifications')

  I.waitForElement('.detail-popup-appointment')
  I.click('~Delete', '.detail-popup-appointment')
  I.waitForElement('.delete-dialog')
  I.click('Delete appointment', '.delete-dialog')

  I.waitForDetached('.item[data-type="appointment:invitation"]')
  I.dontSeeElement('.item[data-type="appointment:invitation"]')
})
