/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Calendar > forward')

Before(async ({ users }) => { await Promise.all([users.create(), users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('User can forward appointments', async ({ I, mail, users, calendar, dialogs }) => {
  const startTime = calendar.getNextMonday()
  // no promise all because of concurrent modification issues
  await I.haveAppointment({
    summary: 'Another appointment',
    startDate: { value: startTime },
    endDate: { value: startTime.add(1, 'hour') },
    organizer: {
      cn: users[0].get('display_name'),
      email: users[0].get('email1'),
      entity: users[0].get('id'),
      uri: `mailto:${users[0].get('email1')}`
    },
    attendees: [
      { partStat: 'ACCEPTED', entity: users[0].get('id'), cn: users[0].get('display_name'), uri: `mailto:${users[0].get('email1')}` }
    ]
  })
  await I.haveAppointment({
    summary: 'Best appointment',
    startDate: { value: startTime.add(2, 'hour') },
    endDate: { value: startTime.add(1, 'hour') },
    organizer: {
      cn: users[0].get('display_name'),
      email: users[0].get('email1'),
      entity: users[0].get('id'),
      uri: `mailto:${users[0].get('email1')}`
    },
    attendees: [
      { partStat: 'ACCEPTED', entity: users[0].get('id'), cn: users[0].get('display_name'), uri: `mailto:${users[0].get('email1')}` },
      { partStat: 'ACCEPTED', entity: users[1].get('id'), cn: users[1].get('display_name'), uri: `mailto:${users[1].get('email1')}` }
    ]
  })

  // User 1: Attendee forwards the appointment
  await I.login('app=io.ox/calendar', { user: users[1] })

  calendar.moveCalendarViewToNextWeek()

  I.waitForText('Best appointment', undefined, '.appointment')
  I.click('Best appointment', '.appointment')
  I.waitForVisible('.detail-popup')
  I.waitForText('Best appointment', undefined, '.detail-popup')

  I.click('~More actions', '.detail-popup .more-dropdown')
  I.waitForText('Forward appointment')
  I.click('Forward appointment')
  I.waitForElement('.modal-dialog')
  // add user
  I.fillField('.add-participant.tt-input', users[2].get('email1'))
  I.waitForElement('.tt-suggestions .participant-name')
  I.pressKey('Enter')
  I.waitForText(`${users[2].get('sur_name')}, ${users[2].get('given_name')}`, undefined, '.modal-dialog')
  // filter existing participants
  I.fillField('.add-participant.tt-input', users[1].get('email1'))
  I.pressKey('Enter')
  I.waitForText('This email address cannot be used')
  I.dontSee(`${users[1].get('sur_name')}, ${users[1].get('given_name')}`, '.modal-dialog')
  I.clearField('.add-participant.tt-input')
  // add external
  I.fillField('.add-participant.tt-input', 'testy@mctest.face')
  I.pressKey('Enter')
  I.waitForText('testy <testy@mctest.face>', undefined, '.modal-dialog')
  // add message
  I.fillField('comment', 'You need to be there too.')

  dialogs.clickButton('Send invitations')
  I.waitForDetached('.modal-dialog')
  await I.logout()

  // User 2: Forwarded attendee accepts the invitation
  await I.login('app=io.ox/mail', { user: users[2] })

  await mail.selectMail('Fwd: Best appointment')
  I.waitForElement('.mail-detail-frame')
  I.waitForText('Accept', undefined, '.itip-actions')
  I.see('Maybe', '.itip-actions')
  I.see('Decline', '.itip-actions')
  I.waitForText('You need to be there too.')
  I.click('Accept')
  await I.logout()

  // User 0: Organizer accepts the new attendee
  await I.login('app=io.ox/mail', { user: users[0] })

  await mail.selectMail(`${users[2].get('display_name')} accepted the invitation: Best appointment`)
  I.waitForElement('.mail-detail-frame')
  I.waitForText('Add participant')
  I.waitForText('Reject participant')
  I.click('Add participant')
  I.waitForText('Accepted the new participant')
  I.openApp('Calendar')
  I.wait(0.5)
  I.refreshPage()
  await I.waitForApp()
  calendar.moveCalendarViewToNextWeek()

  I.waitForText('Best appointment')
  I.click('~Best appointment')
  I.waitForVisible('.detail-popup')
  I.waitForText('Best appointment', undefined, '.detail-popup')
  I.waitForElement(`.detail-popup .participant.accepted a[title="${users[2].get('primaryEmail')}"]`)

  // check if forward appointment is only offered for group scheduled appointments
  I.pressKey('Escape')
  I.waitForInvisible('.detail-popup')

  I.waitForText('Another appointment')
  I.click('~Another appointment')
  I.waitForVisible('.detail-popup')
  I.waitForText('Another appointment', undefined, '.detail-popup')

  I.click('~More actions', '.detail-popup .more-dropdown')
  I.dontSee('Forward appointment')
})
