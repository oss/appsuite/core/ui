/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Delete')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C7405] - Delete E-Mail', async ({ I, users, mail }) => {
  const [alice, bob] = users
  const mailText = `C7405 - ${Math.round(+new Date() / 1000)}`
  await I.haveSetting('io.ox/mail//messageFormat', 'text')
  await I.login('app=io.ox/mail', { user: alice })

  await mail.newMail()
  I.fillField(mail.to, bob.get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, mailText)
  I.fillField(mail.editorText, mailText)
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: bob })
  I.selectFolder('Inbox')
  I.waitForText(mailText, undefined, '.subject')
  I.click(`.list-item[aria-label*="${mailText}"]`)
  I.waitForEnabled('~Delete')
  I.click('~Delete')
  I.waitForDetached(`.list-item[aria-label*="${mailText}"]`)
  I.dontSee(mailText)
  I.selectFolder('Trash')
  I.waitForText(mailText, undefined, '.list-item')
  I.see(mailText)
})

const getCheckboxBackgroundImage = async () => {
  const { I } = inject()
  return await I.executeScript(() => {
    const node = document.querySelector('.list-item.selected .list-item-checkmark')
    return window.getComputedStyle(node, ':before').getPropertyValue('background-image')
  })
}

Scenario('[C7406] - Delete several E-Mails', async ({ I, users, mail }) => {
  await Promise.all([
    I.haveSetting({
      'io.ox/mail': { messageFormat: 'text', listViewLayout: 'checkboxes' },
      'io.ox/core': { selectionMode: 'alternative' }
    }),
    I.haveMail({ subject: 'Monday', from: users[0], to: users[0] }),
    I.haveMail({ subject: 'Tuesday', from: users[0], to: users[0] }),
    I.haveMail({ subject: 'Wednesday', from: users[0], to: users[0] }),
    I.haveMail({ subject: 'Thursday', from: users[0], to: users[0] })
  ])

  await I.login('app=io.ox/mail')
  I.selectFolder('Inbox')
  await I.waitForApp()
  I.waitForElement('.list-item')

  // select, check 'alternative' selection, delete
  await mail.selectMail('Monday')
  I.waitForElement('.list-item.selected.no-checkbox .list-item-checkmark')
  expect(await getCheckboxBackgroundImage()).to.match(/^none/)
  I.click('~Delete')
  I.waitForDetached('[title="Monday"]')

  // check 'alternative' selection (autoselect after delete)
  I.waitForElement('.list-item.selected.no-checkbox .list-item-checkmark')
  expect(await getCheckboxBackgroundImage()).to.match(/^none/)

  // delete second mail
  await mail.selectMail('Tuesday')
  I.click('~Delete')
  I.waitForDetached('[title="Tuesday"]')
  // check trash
  I.selectFolder('Trash')
  I.waitForText('Trash', undefined, '.folder-name')

  I.waitForElement('[title="Monday"]', 10)
  I.wait(5)
  I.refreshPage() // reload trash folder
  I.waitForElement('[title="Tuesday"]', 10)

  await I.haveSetting({ 'io.ox/core': { selectionMode: 'normal' } })
  I.refreshPage()
  await I.waitForApp()
  I.selectFolder('Inbox')

  // select, check 'normal' selection, delete
  await mail.selectMail('Wednesday')
  I.waitForElement('.list-item.selected .list-item-checkmark')
  expect(await getCheckboxBackgroundImage()).to.include('data:image/svg+xml')
  I.click('~Delete')
  I.waitForDetached('[title="Wednesday"]')

  // check 'normal' selection (autoselect after delete)
  I.waitForElement('.list-item.selected .list-item-checkmark')
  expect(await getCheckboxBackgroundImage()).to.include('data:image/svg+xml')
})

Scenario('[C265146] Delete with setting selectBeforeDelete=false', async ({ I, mail, users }) => {
  await Promise.all([
    I.haveMail({
      subject: 'Test E-Mail 1',
      attachments: [{ content: 'C265146\r\n', content_type: 'text/html', disp: 'inline' }],
      from: users[0],
      to: users[0],
      sendtype: 0
    }),
    I.haveMail({
      subject: 'Test E-Mail 2',
      attachments: [{ content: 'C265146\r\n', content_type: 'text/html', disp: 'inline' }],
      from: users[0],
      to: users[0],
      sendtype: 0
    })
  ])

  await I.haveSetting('io.ox/mail//features/selectBeforeDelete', false)

  await I.login('app=io.ox/mail')

  I.waitForText('Test E-Mail 1')
  await mail.selectMail('Test E-Mail 1')
  I.click('~Delete')
  I.wait(1)
  I.waitForText('No message selected')
})
