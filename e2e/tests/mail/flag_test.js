/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Flags')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C114336] Flag an E-Mail with a color flag', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/core//autoStart', 'none')
  await I.haveMail({
    attachments: [{
      content: 'Lorem ipsum',
      content_type: 'text/plain',
      disp: 'inline'
    }],
    flags: 0,
    from: users[0],
    subject: 'Flag mails',
    to: users[0]
  })

  await I.login(undefined, { wait: false })
  I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    mailSettings.set('features/flagging', { mode: 'color' })
    mailSettings.flagByColor = true
    mailSettings.flagByStar = false
  })
  I.waitForInvisible('#background-loader')
  I.openApp('Mail')
  await I.waitForApp()
  I.waitForText('No message selected')

  I.click('.list-view .list-item')
  I.waitForVisible('.thread-view.list-view .list-item')
  // set
  I.click('~Set color')
  I.wait(0.5)
  I.waitForText('Yellow')
  I.click('[data-action="color-yellow"]', '.smart-dropdown-container.flag-picker')
  I.waitForVisible('.list-item .color-flag.flag_10')

  // persistency
  I.waitForDetached('#io-ox-refresh-icon .animate-spin')
  I.click('#io-ox-refresh-icon')
  I.waitForNetworkTraffic()
  I.waitForVisible('.list-item .color-flag.flag_10')

  // unset
  I.click('~Set color')
  I.wait(0.5)
  I.waitForText('Yellow')
  I.click('[data-action="color-none"]', '.smart-dropdown-container.flag-picker')
  I.waitForInvisible('.list-item .color-flag')
})

Scenario('[C114337] Flag an E-Mail with a star', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/core//autoStart', 'none')
  await I.haveMail({
    attachments: [{
      content: 'Lorem ipsum',
      content_type: 'text/plain',
      disp: 'inline'
    }],
    flags: 0,
    from: users[0],
    subject: 'Flag mails',
    to: users[0]
  })

  await I.login(undefined, { wait: false })
  I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    mailSettings.set('features/flagging', { mode: 'star' })
    mailSettings.flagByColor = false
    mailSettings.flagByStar = true
  })
  I.waitForInvisible('#background-loader')
  I.openApp('Mail')
  await I.waitForApp()
  I.waitForText('No message selected')

  I.click('.list-view .list-item')
  I.waitForVisible('.thread-view.list-view .list-item')

  I.click('[data-action="io.ox/mail/actions/flag"]')
  I.dontSeeElement('[data-action="io.ox/mail/actions/color"]')
  I.waitForElement('.mail-item .list-item .flag [title="Flagged"]')
})

Scenario('[C114339a] Flag an E-Mail on an alternative client (mode "color")', async ({ I, users }) => {
  await I.haveSetting('io.ox/core//autoStart', 'none')
  await I.haveMail({
    attachments: [{
      content: 'Lorem ipsum',
      content_type: 'text/plain',
      disp: 'inline'
    }],
    flags: 8,
    from: users[0],
    subject: 'Flag mails',
    to: users[0]
  })

  await I.login(undefined, { wait: false })
  I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    mailSettings.set('features/flagging', { mode: 'color' })
    mailSettings.flagByColor = true
    mailSettings.flagByStar = false
  })
  I.waitForInvisible('#background-loader')
  I.openApp('Mail')
  await I.waitForApp()
  I.waitForText('No message selected')

  await I.executeScript(async function () {
    const { default: http } = await import(String(new URL('io.ox/core/http.js', location.href)))
    return http.PUT({
      module: 'mail',
      params: { action: 'update', id: 1, folder: 'default0/INBOX' },
      data: { color_label: 0, flags: 8, value: true },
      appendColumns: false
    })
      // @ts-ignore
      .always(() => { window.list.reload() })
  })

  I.click('.list-view .list-item')
  I.waitForVisible('.thread-view.list-view .list-item')

  I.waitForElement('.list-item .color-flag.flag_1')
  I.dontSee('.mail-item .list-item .flag [title="Flagged"]')
})

Scenario('[C114339b] Flag an E-Mail on an alternative client (mode "star")', async ({ I, users }) => {
  await I.haveSetting('io.ox/core//autoStart', 'none')
  await I.haveMail({
    attachments: [{
      content: 'Lorem ipsum',
      content_type: 'text/plain',
      disp: 'inline'
    }],
    flags: 8,
    from: users[0],
    subject: 'Flag mails',
    to: users[0]
  })

  await I.login(undefined, { wait: false })
  I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    mailSettings.set('features/flagging', { mode: 'star' })
    mailSettings.flagByColor = false
    mailSettings.flagByStar = true
  })
  I.waitForInvisible('#background-loader')
  I.openApp('Mail')
  await I.waitForApp()
  I.waitForText('No message selected')

  await I.executeScript(async function () {
    const { default: http } = await import(String(new URL('io.ox/core/http.js', location.href)))
    return http.PUT({
      module: 'mail',
      params: { action: 'update', id: 1, folder: 'default0/INBOX' },
      data: { color_label: 0, flags: 8, value: true },
      appendColumns: false
    })
      // @ts-ignore
      .always(() => { window.list.reload() })
  })

  I.click('.list-view .list-item')
  I.waitForVisible('.thread-view.list-view .list-item')

  I.waitForElement('.mail-item .list-item .flag [title="Flagged"]')
  I.dontSee('.list-item .color-flag.flag_1')
})
