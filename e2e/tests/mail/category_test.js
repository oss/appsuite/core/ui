/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Categories')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Use default categories', async ({ I, mail, users }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await Promise.all([
    I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } }),
    I.haveMail({ from: sender, to: sender, subject: 'Some random mail' })
  ])

  await I.login('app=io.ox/mail')

  // check default categories in dropdown
  await mail.selectMail('Some random mail')
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.waitNumberOfVisibleElements('.category-dropdown [aria-checked="false"]', 4)
  I.waitNumberOfVisibleElements('.category-dropdown [data-type]', 4)
})

Scenario('List, add, remove and search', async ({ I, mail, users, settings }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  const userID = users[0].get('id')
  await Promise.all([
    I.haveSetting({
      'io.ox/mail': {
        listViewLayout: 'checkboxes',
        showCategories: true,
        viewOptions: {
          'default0/INBOX': { thread: true }
        }
      },
      'io.ox/core': {
        features: { categories: true },
        categories: {
          userCategories: [
            { id: `$ct_user_0011_${userID}`, name: 'Confidential' },
            { id: `$ct_user_0012_${userID}`, name: 'Delayed' }
          ]
        }
      }
    }),
    I.haveMail({ from: sender, to: sender, subject: 'One predefined, one user' }),
    I.haveMail({ from: sender, to: sender, subject: 'One predefined, one shared, one user' }),
    I.haveMail({ path: 'media/mails/thread-part-1.eml' }),
    I.haveMail({ path: 'media/mails/thread-part-2.eml' })

  ])

  await I.login('app=io.ox/contacts')
  await Promise.all([
    I.setMailCategories({ mailId: 1, folder: 'default0/INBOX', categories: ['$ct_predefined_0003', `$ct_user_0011_${userID}`] }),
    I.setMailCategories({ mailId: 2, folder: 'default0/INBOX', categories: ['$ct_predefined_0001', `$ct_user_0012_${userID}`, `$ct_user_9999_${userID + 99}`] }),
    I.setMailCategories({ mailId: 3, folder: 'default0/INBOX', categories: [`$ct_user_0011_${userID}`] }),
    I.setMailCategories({ mailId: 4, folder: 'default0/INBOX', categories: [`$ct_user_0012_${userID}`] })
  ])

  I.openApp('Mail')
  await I.waitForApp()

  // check list and detail view for first mail
  await mail.selectMail('One predefined, one user')
  I.waitForText('Private', undefined, '.mail-detail .categories-badges')
  I.waitForText('Private', undefined, '.list-item.selected .categories-badges')
  I.see('Confidential', '.mail-detail .categories-badges')
  I.see('Confidential', '.list-item.selected .categories-badges')

  // check list and detail view for threaded mail
  await mail.selectMail('My thread')
  I.waitForText('Delayed', undefined, '.list-item.selected .categories-badges')
  I.waitForText('Confidential', undefined, '.list-item.selected .categories-badges')
  I.see('Delayed', '.thread-view article:nth-child(1) .categories-badges')
  I.dontSee('Confidential', '.thread-view article:nth-child(1) .categories-badges')
  I.see('Confidential', '.thread-view article:nth-child(2) .categories-badges')
  I.dontSee('Delayed', '.thread-view article:nth-child(2) .categories-badges')

  // check list and detail view for second mail
  await mail.selectMail('One predefined, one shared, one user')
  I.waitForText('Important', undefined, '.mail-detail .categories-badges')
  I.waitForText('Important', undefined, '.list-item.selected .categories-badges')
  I.see('Delayed', '.mail-detail .categories-badges')
  I.see('Delayed', '.list-item.selected .categories-badges')
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.waitNumberOfVisibleElements('.category-dropdown [aria-checked="true"]', 2)
  I.waitNumberOfVisibleElements('.category-dropdown [data-type]', 6)
  I.pressKey('Escape')

  // check listed categories and their states in category dropdown (single select)
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.see('Important', '.dropdown-menu [data-type="predefined"]')
  I.see('Business', '.dropdown-menu [data-type="predefined"]')
  I.see('Private', '.dropdown-menu [data-type="predefined"]')
  I.see('Meeting', '.dropdown-menu [data-type="predefined"]')
  I.see('Confidential', '.dropdown-menu [data-type="user"]')
  I.see('Delayed', '.dropdown-menu [data-type="user"]')
  I.waitNumberOfVisibleElements('.category-dropdown [aria-checked="true"]', 2)
  I.waitNumberOfVisibleElements('.category-dropdown [data-type]', 6)
  I.dontSee('User Categories', '.category-dropdown')
  I.dontSee('Shared Categories', '.category-dropdown')

  // add new category to single mail
  I.clickDropdown('Private')
  I.waitForText('Private', undefined, '.mail-detail .categories-badges')
  I.waitForText('+1', undefined, '.list-item.selected .categories-badges')

  // check listed categories and theirs states in category dropdown (multiselect)
  I.click('.list-view .selectable:not(.selected) .list-item-checkmark')
  I.click('Set categories')

  I.waitForVisible('.category-dropdown')
  I.see('Important', '.dropdown-menu [aria-checked="true"] [data-type="predefined"]')
  I.see('Business', '.dropdown-menu [aria-checked="false"] [data-type="predefined"]')
  I.see('Private', '.dropdown-menu [aria-checked="true"]  [data-type="predefined"]')
  I.see('Meeting', '.dropdown-menu  [aria-checked="false"] [data-type="predefined"]')
  I.see('Confidential', '.dropdown-menu [aria-checked="true"] [data-type="user"]')
  I.see('Delayed', '.dropdown-menu [aria-checked="true"] [data-type="user"]')
  I.waitNumberOfVisibleElements('.category-dropdown [aria-checked="true"]', 4)
  I.waitNumberOfVisibleElements('.category-dropdown [data-type]', 6)

  // remove category from two mails (multiselect)
  I.clickDropdown('Private')
  I.waitForNetworkTraffic()
  I.dontSee('Private', '.list-item.selected .categories-badges')
  I.dontSee('+1', '.list-item.selected .categories-badges')

  // check list and detail view after refresh (persistence)
  I.refreshPage()
  await I.waitForApp()
  await mail.selectMail('One predefined, one user')
  I.waitForText('Confidential', undefined, '.list-item.selected .categories-badges')

  await mail.selectMail('One predefined, one shared, one user')
  I.waitForText('Important', undefined, '.list-item.selected .categories-badges')
  I.waitForText('Delayed', undefined, '.list-item.selected .categories-badges')

  // use autocomplete in search dropdown
  I.click('~More search options')
  I.waitForVisible('.dropdown input[name="user_flags"]')
  I.fillField('.dropdown input[name="user_flags"]', 'Del')
  I.waitForText('Delayed', undefined, '.autocomplete .category-view')
  I.click('Delayed', '.autocomplete .category-view')
  I.seeInField('.dropdown input[name="user_flags"]', 'Delayed')
  I.click('~Search')
  I.waitForText('Search results')
  I.waitToHide('.list-view .busy-indicator.io-ox-busy')
  I.waitForText('One predefined, one shared, one user', undefined, '.list-view .list-item')
  I.dontSee('One predefined, one user', '.list-view .list-item')

  // check search dropdown (show label, not prefixed id)
  I.click('~More search options')
  I.waitForVisible('.dropdown input[name="user_flags"]')
  I.seeInField('.dropdown input[name="user_flags"]', 'Delayed')
  I.click('~Cancel search')

  // toggle category badges in list view
  await settings.open('Mail', 'Reading')
  I.waitForText('Show categories')
  I.seeCheckboxIsChecked('Show categories')
  I.uncheckOption('Show categories')
  settings.close()
  I.dontSeeElement('.categories-badges')
})

Scenario('Two users work on a shared mail', async ({ I, mail, users, dialogs, autocomplete }) => {
  await users.create()

  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  const userID0 = users[0].get('id')
  const userID1 = users[1].get('id')
  await Promise.all([
    I.haveSetting({
      'io.ox/mail': { listViewLayout: 'checkboxes', showCategories: true },
      'io.ox/core': {
        features: { categories: true },
        categories: {
          predefined: [
            { id: '$ct_pre_0001', name: 'Team', color: '#16adf8', icon: 'bi/people-fill.svg' },
            { id: '$ct_pre_0002', name: 'ToDo', color: '#707070', icon: 'bi/list-ol.svg' }
          ],
          userCategories: [
            { id: `$ct_user_0011_${userID0}`, name: 'Confidential' }
          ]
        }
      }
    }, { user: users[0] }),
    I.haveSetting({
      'io.ox/mail': { listViewLayout: 'checkboxes', showCategories: true },
      'io.ox/core': {
        features: { categories: true },
        categories: {
          predefined: [
            { id: '$ct_pre_0001', name: 'Team', color: '#16adf8', icon: 'bi/people-fill.svg' },
            { id: '$ct_pre_0002', name: 'ToDo', color: '#707070', icon: 'bi/list-ol.svg' }
          ],
          userCategories: [
            { id: `$ct_user_0011_${userID1}`, name: 'Confidential' }
          ]
        }
      }
    }, { user: users[1] }),
    I.haveMail({ from: sender, to: sender, subject: 'One predefined, one user' }),
    I.haveMail({ from: sender, to: sender, subject: 'One predefined, one shared, one user' })
  ])

  // set predefined category 'Team'
  await I.login('app=io.ox/mail')

  await mail.selectMail('One predefined, one user')
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.clickDropdown('Team')
  I.waitForText('Team', undefined, '.mail-detail .categories-badges')
  I.waitForText('Team', undefined, '.list-item.selected .categories-badges')

  // set predefined category 'ToDo'
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.clickDropdown('ToDo')
  I.waitForText('ToDo', undefined, '.mail-detail .categories-badges')
  I.waitForText('ToDo', undefined, '.list-item.selected .categories-badges')

  // set user category 'Confidential'
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.clickDropdown('Confidential')
  I.waitForText('Confidential', undefined, '.mail-detail .categories-badges')
  I.waitForText('+1', undefined, '.list-item.selected .categories-badges')

  // share folder for user[1]
  I.rightClick('~Inbox')
  I.clickDropdown('Share / Permissions')
  dialogs.waitForVisible()
  I.waitForElement('.form-control.tt-input')
  I.fillField('.form-control.tt-input', users[1].get('primaryEmail'))
  autocomplete.selectFirst()
  I.waitForText(users[1].get('name'), undefined, '.permissions-view')
  I.click('Current role')
  I.clickDropdown('Reviewer')
  dialogs.clickButton('Save')
  I.waitForDetached('.modal-dialog')
  await I.logout()

  // switch to user[1]
  await I.login('app=io.ox/mail', { user: users[1] })

  I.waitForText('Shared folders')
  I.click('.folder-arrow', '~Shared folders')
  I.click(`~${users[0].get('name')}`)

  // check categories (only predefined, not the user category)
  await mail.selectMail('One predefined, one user')
  I.waitForText('Team', undefined, '.list-item.selected .categories-badges')
  I.waitForText('ToDo', undefined, '.list-item.selected .categories-badges')
  I.dontSee('Confidential', '.list-item.selected .categories-badges')
  I.waitForText('Team', undefined, '.mail-detail .categories-badges')
  I.waitForText('ToDo', undefined, '.mail-detail .categories-badges')
  I.dontSee('Confidential', '.mail-detail .categories-badges')

  // remove predefined category 'ToDo'
  I.click('Set categories')
  I.waitForVisible('.category-dropdown')
  I.clickDropdown('ToDo')
  I.waitForNetworkTraffic()
  I.dontSee('ToDo', '.mail-detail .categories-badges')
  I.dontSee('ToDo', '.list-item.selected .categories-badges')

  // ensure change is persistent
  I.refreshPage()
  await I.waitForApp()
  await mail.selectMail('One predefined, one user')
  I.dontSee('ToDo', '.mail-detail .categories-badges')
  I.dontSee('ToDo', '.list-item.selected .categories-badges')
  await I.logout()

  // check removed category 'ToDo'
  await I.login('app=io.ox/mail')

  await mail.selectMail('One predefined, one user')
  I.waitForText('Team', undefined, '.list-item.selected .categories-badges')
  I.waitForText('Confidential', undefined, '.list-item.selected .categories-badges')
  I.dontSee('ToDo', '.list-item.selected .categories-badges')
  I.waitForText('Team', undefined, '.mail-detail .categories-badges')
  I.waitForText('Confidential', undefined, '.mail-detail .categories-badges')
  I.dontSee('ToDo', '.mail-detail .categories-badges')
})
