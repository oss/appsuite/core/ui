/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail Compose')

Before(async function ({ users }) {
  await users.create()
  await users.create()
  await users.create()
  await users.create()
})

After(async ({ users }) => { await users.removeAll() })

Scenario('[C7380] Send saved draft mail', async ({ I, users, dialogs, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')
  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.fillField(mail.subject, 'Test mail subject')
  I.fillField(mail.editorText, 'Body of mail')
  I.pressKey('Escape')

  dialogs.waitForVisible()
  dialogs.clickButton('Save draft')
  I.waitForDetached('.io-ox-mail-compose')

  I.selectFolder('Drafts')
  await mail.selectMail('Test mail subject')
  I.doubleClick('.list-item[aria-label*="Test mail subject"]')
  await I.waitForFocus('.io-ox-mail-compose textarea.plain-text')
  await within('.io-ox-mail-compose', () => {
    I.seeInField('Subject', 'Test mail subject')
    I.seeInField(mail.editorText, 'Body of mail')
  })
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('Test mail subject')
  I.doubleClick('[title="Test mail subject"]')
  I.waitForText('Test mail subject', undefined, '.io-ox-mail-detail-window')
})

Scenario('[C7381] Send email to multiple recipients', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.to, users[2].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.to, users[3].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'Test mail to multiple recipients')
  I.fillField(mail.editorText, 'Test mail to multiple recipients')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('Test mail to multiple recipients')
  I.doubleClick('[title="Test mail to multiple recipients"]')
  I.waitForElement('.io-ox-mail-detail-window')
  I.see('Test mail to multiple recipients', '.io-ox-mail-detail-window')
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[2] })
  I.selectFolder('Inbox')
  await mail.selectMail('Test mail to multiple recipients')
  I.doubleClick('[title="Test mail to multiple recipients"]')
  I.waitForElement('.io-ox-mail-detail-window')
  I.see('Test mail to multiple recipients', '.io-ox-mail-detail-window')
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[3] })
  I.selectFolder('Inbox')
  await mail.selectMail('Test mail to multiple recipients')
  I.doubleClick('[title="Test mail to multiple recipients"]')
  I.waitForText('Test mail to multiple recipients', undefined, '.io-ox-mail-detail-window')
})

Scenario('[C7382] Compose plain text mail', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.fillField(mail.subject, 'Test plain text mail')
  I.fillField(mail.editorText, 'Test plain text mail')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })

  await mail.selectMail('Test plain text mail')
  I.doubleClick('[title="Test plain text mail"]')
  I.waitForText('Test plain text mail', undefined, '.io-ox-mail-detail-window')
})

Scenario('[C7384] Save draft', async ({ I, users, mail, dialogs }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'C7384 - Test draft subject')
  I.fillField(mail.editorText, 'Test draft body')
  I.pressKey('Escape')

  dialogs.waitForVisible()
  dialogs.clickButton('Save draft')
  I.waitForDetached('.modal-dialog')
  I.selectFolder('Drafts')
  await mail.selectMail('C7384 - Test draft subject')
  I.doubleClick('.list-item[aria-label*="C7384 - Test draft subject"]')
  await I.waitForFocus('.io-ox-mail-compose textarea.plain-text')
  await within('.io-ox-mail-compose', () => {
    I.seeInField('Subject', 'C7384 - Test draft subject')
    I.seeInField(mail.editorText, 'Test draft body')
  })
})

Scenario('[C7385] Write mail to BCC recipients', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.click('BCC')
  I.fillField('BCC', users[2].get('primaryEmail'))
  I.fillField(mail.subject, 'C7385 - Test subject')
  I.fillField(mail.editorText, 'C7385 - Test body')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7385 - Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForElement(`[title="${users[1].get('primaryEmail')}"]`)
  I.waitForText('C7385 - Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[2] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7385 - Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForElement(`[title="${users[1].get('primaryEmail')}"]`)
  I.waitForText('C7385 - Test subject', undefined, '.mail-detail-pane .subject')
})

Scenario('[C7386] Write mail to CC recipients', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.click('CC')
  I.fillField('CC', users[2].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField('CC', users[3].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C7386 - Test subject')
  I.fillField(mail.editorText, 'C7386 - Test body')
  mail.send()
  await I.logout()

  // login and check with user1
  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7386')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForElement(`[title="${users[1].get('primaryEmail')}"]`)
  I.waitForElement(`[title="${users[2].get('primaryEmail')}"]`)
  I.waitForElement(`[title="${users[3].get('primaryEmail')}"]`)
  I.waitForText('C7386 - Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  // login and check with user2
  await I.login('app=io.ox/mail', { user: users[2] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7386')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForElement(`[title="${users[1].get('primaryEmail')}"]`)
  I.waitForElement(`[title="${users[2].get('primaryEmail')}"]`)
  I.waitForElement(`[title="${users[3].get('primaryEmail')}"]`)
  I.waitForText('C7386 - Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  // login and check with user3
  await I.login('app=io.ox/mail', { user: users[3] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7386')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForElement(`[title="${users[1].get('primaryEmail')}"]`)
  I.waitForElement(`[title="${users[2].get('primaryEmail')}"]`)
  I.waitForElement(`[title="${users[3].get('primaryEmail')}"]`)
  I.waitForText('C7386 - Test subject', undefined, '.mail-detail-pane .subject')
})

Scenario('[C7387] Send mail with attachment from upload', async ({ I, users, mail }) => {
  await Promise.all([
    users[0].doesntHaveCapability('document_preview'),
    I.haveSetting('io.ox/mail//messageFormat', 'text')
  ])

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C7387 - Test subject')
  I.pressKey('Enter')
  I.fillField(mail.editorText, 'C7387 - Test subject')
  mail.addAttachment('media/files/generic/testdocument.odt')
  mail.addAttachment('media/files/generic/testdocument.rtf')
  mail.addAttachment('media/files/generic/testpresentation.ppsm')
  mail.addAttachment('media/files/generic/testspreadsheed.xlsm')
  // Send mail and logout
  I.click('Send')

  I.waitForDetached('.io-ox-mail-compose')
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')

  await mail.selectMail('C7387 - Test subject')
  // Show attachments as list
  I.click('4 attachments')
  I.waitForText('testdocument.')
  I.waitForText('odt')
  I.waitForText('rtf')
  I.waitForText('ppsm')
  I.waitForText('xlsm')
})

Scenario('[C7388] Send mail with different priorities', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')

  await mail.newMail()

  I.click('~Mail compose actions')
  I.clickDropdown('High')
  I.waitForDetached('.dropup.open .dropdown-menu')
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C7388 - Test subject Priority: High')
  I.fillField(mail.editorText, 'C7388 - Test body')
  mail.send()

  await mail.newMail()
  I.click('~Mail compose actions')
  I.clickDropdown('Normal')
  I.waitForDetached('.dropup.open .dropdown-menu')
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C7388 - Test subject Priority: Normal')
  I.fillField(mail.editorText, 'C7388 - Test body')
  mail.send()

  await mail.newMail()
  I.click('~Mail compose actions')
  I.clickDropdown('Low')
  I.waitForDetached('.dropup.open .dropdown-menu')
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C7388 - Test subject Priority: Low')
  I.fillField(mail.editorText, 'C7388 - Test body')
  mail.send()

  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })

  I.selectFolder('Inbox')
  I.waitNumberOfVisibleElements('.list-view .list-item', 3)

  await mail.selectMail('C7388 - Test subject Priority: High')
  I.waitForText('C7388 - Test subject Priority: High', undefined, '.detail-view-header .subject')
  I.waitForElement('.mail-detail-pane.selection-one .priority .high')

  await mail.selectMail('C7388 - Test subject Priority: Normal')
  I.waitForText('C7388 - Test subject Priority: Normal', undefined, '.detail-view-header .subject')

  await mail.selectMail('C7388 - Test subject Priority: Low')
  I.waitForText('C7388 - Test subject Priority: Low', undefined, '.detail-view-header .subject')
  I.waitForElement('.mail-detail-pane.selection-one .priority .low')
})

Scenario('[C7389] Send mail with attached vCard', async ({ I, users, mail, dialogs, contacts }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.click('~Mail compose actions')
  I.clickDropdown('Attach Vcard')
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.fillField(mail.subject, 'C7389 - Test subject')
  I.fillField(mail.editorText, 'C7389 - Test body')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7389 - Test subject')
  I.click('1 attachment')
  I.click(`${users[0].get('display_name')}.vcf`)
  I.waitForElement('.dropdown.open')
  I.click('Add to address book', '.dropdown.open .dropdown-menu')
  I.waitForElement(contacts.editWindow)

  // confirm dirtycheck is working properly
  I.click('~Close', '.floating-header')
  dialogs.waitForVisible()
  I.waitForText('Do you really want to discard your changes?', undefined, dialogs.body)
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')

  I.click('Save')
  I.waitForDetached(contacts.editWindow)
  I.openApp('Address Book')
  await I.waitForApp()
  I.waitForVisible('.io-ox-contacts-window')
  I.selectFolder('Contacts')
  I.waitForElement(`[href="mailto:${users[0].get('primaryEmail')}"]`)
  I.waitForText(`${users[0].get('sur_name')}, ${users[0].get('given_name')}`, undefined, '.contact-detail.view .contact-header .fullname')
})

Scenario('[C7403] Forward a single mail', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C7403 - Test subject')
  I.fillField(mail.editorText, 'C7403 - Test body')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7403 - Test subject')
  I.click('~Forward')
  await I.waitForFocus(mail.to)
  I.fillField(mail.to, users[2].get('primaryEmail'))
  I.pressKey('Enter')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[2] })
  I.selectFolder('Inbox')
  await mail.selectMail('Fwd: C7403 - Test subject')
  I.see('Fwd: C7403 - Test subject', '.detail-view-header .subject')
})

Scenario('[C7404] Reply to single mail', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.fillField(mail.subject, 'C7404 - Test subject')
  I.fillField(mail.editorText, 'C7404 - Test subject')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('C7404 - Test subject')
  I.click('~Reply to sender')
  await I.waitForFocus('.io-ox-mail-compose textarea.plain-text')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail')
  I.selectFolder('Inbox')
  await mail.selectMail('Re: C7404 - Test subject')
  I.see('Re: C7404 - Test subject', '.mail-detail-pane .subject')
})

Scenario('[C8816] Cancel mail compose', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')
  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C8816 - Test subject')
  I.fillField(mail.editorText, 'C8816 - Test body')
  I.pressKey('Escape')
  I.see('This email has not been sent. You can save the draft to work on later.')
  I.click('Delete draft')
})

Scenario('[C8820] Forward attachments', async ({ I, users, mail }) => {
  await I.haveSetting({
    'io.ox/mail': {
      'attachments/layout/detail/open': true,
      messageFormat: 'text'
    }
  })

  await I.login('app=io.ox/mail')

  // login user 1 and send mail with attachments
  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.pressKey('Enter')
  I.fillField(mail.subject, 'C8820 - Test subject')
  I.fillField(mail.editorText, 'C8820 - Test subject')
  I.attachFile('.io-ox-mail-compose-window .composetoolbar input[type=file]', 'media/files/generic/testdocument.odt')
  I.attachFile('.io-ox-mail-compose-window .composetoolbar input[type=file]', 'media/files/generic/testdocument.rtf')
  I.attachFile('.io-ox-mail-compose-window .composetoolbar input[type=file]', 'media/files/generic/testpresentation.ppsm')
  I.attachFile('.io-ox-mail-compose-window .composetoolbar input[type=file]', 'media/files/generic/testspreadsheed.xlsm')
  mail.send()
  I.selectFolder('Sent')
  I.waitForVisible({ css: `div[title="${users[1].get('primaryEmail')}"]` })
  await I.logout()

  // login user 2, check mail and forward to user 3
  await I.login('app=io.ox/mail', { user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('C8820 - Test subject')
  I.waitForElement('.mail-attachment-list')
  I.click('4 attachments')
  I.waitForElement('.mail-attachment-list.open')
  I.waitForElement('.mail-attachment-list.open [title="testdocument.odt"]')
  I.waitForElement('.mail-attachment-list.open [title="testdocument.rtf"]')
  I.waitForElement('.mail-attachment-list.open [title="testpresentation.ppsm"]')
  I.waitForElement('.mail-attachment-list.open [title="testspreadsheed.xlsm"]')
  I.see('C8820 - Test subject', '.mail-detail-pane .subject')
  I.click('~Forward')
  await I.waitForFocus(mail.to)
  I.fillField(mail.to, users[2].get('primaryEmail'))
  I.pressKey('Enter')
  mail.send()
  await I.logout()

  // login user 3 and check mail
  await I.login('app=io.ox/mail', { user: users[2] })
  I.selectFolder('Inbox')
  await mail.selectMail('Fwd: C8820 - Test subject')
  I.click('4 attachments')
  I.waitForElement('.mail-attachment-list.open')
  I.waitForElement('.mail-attachment-list.open [title="testdocument.odt"]')
  I.waitForElement('.mail-attachment-list.open [title="testdocument.rtf"]')
  I.waitForElement('.mail-attachment-list.open [title="testpresentation.ppsm"]')
  I.waitForElement('.mail-attachment-list.open [title="testspreadsheed.xlsm"]')
  I.see('Fwd: C8820 - Test subject', '.mail-detail-pane .subject')
})

Scenario('[C8829] Recipients autocomplete', async ({ I, users, mail }) => {
  const contact = {
    display_name: 'C7382, C7382',
    folder_id: await I.grabDefaultFolder('contacts'),
    first_name: 'fnameC7382',
    last_name: 'lnameC7382',
    email1: 'mail1C7382@e2e.de',
    email2: 'mail2C7382@e2e.de',
    state_home: 'stateC7382',
    street_home: 'streetC7382',
    city_home: 'cityC7382'
  }
  await Promise.all([
    I.haveContact(contact),
    I.haveSetting('io.ox/mail//messageFormat', 'text')
  ])
  await I.login('app=io.ox/mail')
  await mail.newMail()
  await within(mail.composeWindow, () => {
    I.click('CC')
    I.waitForElement('.cc .tt-input')
    I.click('BCC')
    I.waitForElement('.bcc .tt-input')
    const receivers = ['To', 'CC', 'BCC']
    const fields = [contact.email1.substring(0, 7), contact.email2.substring(0, 7), contact.first_name.substring(0, 7), contact.last_name.substring(0, 7)]
    receivers.forEach(function (receiver) {
      fields.forEach(function (field) {
        I.fillField(receiver, field)
        I.waitForText(contact.email1, undefined, '.tt-suggestions')
        I.waitForText(contact.email2, undefined, '.tt-suggestions')
        I.waitForText(contact.first_name + ' ' + contact.last_name, undefined, '.tt-suggestions')
        I.clearField(receiver)
      })
    })
  })
})

Scenario('[C8830] Manually add multiple recipients via comma', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, 'foo@bar.de, lol@ox.io, bla@trash.com,')
  I.waitForElement('.io-ox-mail-compose div.token')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 3)
})

Scenario('[C8831] Add recipient manually', async ({ I, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.click({ css: 'button[data-action="maximize"]' })
  I.fillField(mail.to, 'super01@ox.com')
  I.pressKey('Enter')
  I.fillField(mail.to, 'super02@ox.com')
  I.pressKey('Enter')
  I.seeNumberOfVisibleElements('.io-ox-mail-compose div[data-extension-id="to"] div.token', 2)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
  I.waitForText('super02@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
})

Scenario('[C12118] Remove recipients', async ({ I, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  await I.waitForFocus(mail.to)
  I.click('CC')
  I.waitForVisible('.io-ox-mail-compose .cc .tt-input')
  I.click('BCC')
  I.waitForVisible('.io-ox-mail-compose .bcc .tt-input')

  I.fillField(mail.to, 'super01@ox.com')
  I.pressKey('Enter')
  I.fillField(mail.to, 'super02@ox.com')
  I.pressKey('Enter')
  I.fillField(mail.to, 'super03@ox.com')
  I.pressKey('Enter')
  I.seeNumberOfElements('.io-ox-mail-compose div[data-extension-id="to"] div.token', 3)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
  I.waitForText('super02@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
  I.waitForText('super03@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
  I.click('.io-ox-mail-compose [aria-label="super02@ox.com"] .close')
  I.seeNumberOfElements('.io-ox-mail-compose div[data-extension-id="to"] div.token', 2)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
  I.waitForText('super03@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="to"]')
  I.dontSeeElement('.io-ox-mail-compose div[data-extension-id="to"] [title="super02@ox.com"]')

  I.fillField('.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input', 'super01@ox.com')
  I.pressKey('Enter')
  I.fillField('.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input', 'super02@ox.com')
  I.pressKey('Enter')
  I.fillField('.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input', 'super03@ox.com')
  I.pressKey('Enter')
  I.seeNumberOfElements('.io-ox-mail-compose div[data-extension-id="cc"] div.token', 3)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="cc"]')
  I.waitForText('super02@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="cc"]')
  I.waitForText('super03@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="cc"]')
  I.click('.io-ox-mail-compose [aria-label="super02@ox.com"] .close')
  I.seeNumberOfElements('.io-ox-mail-compose div[data-extension-id="cc"] div.token', 2)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="cc"]')
  I.waitForText('super03@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="cc"]')
  I.dontSeeElement('.io-ox-mail-compose div[data-extension-id="cc"] [title="super02@ox.com"]')

  I.fillField('.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input', 'super01@ox.com')
  I.pressKey('Enter')
  I.fillField('.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input', 'super02@ox.com')
  I.pressKey('Enter')
  I.fillField('.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input', 'super03@ox.com')
  I.pressKey('Enter')
  I.seeNumberOfElements('.io-ox-mail-compose div[data-extension-id="bcc"] div.token', 3)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="bcc"]')
  I.waitForText('super02@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="bcc"]')
  I.waitForText('super03@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="bcc"]')
  I.click('.io-ox-mail-compose [aria-label="super02@ox.com"] .close')
  I.seeNumberOfElements('.io-ox-mail-compose div[data-extension-id="bcc"] div.token', 2)
  I.waitForText('super01@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="bcc"]')
  I.waitForText('super03@ox.com', undefined, '.io-ox-mail-compose div[data-extension-id="bcc"]')
  I.dontSeeElement('.io-ox-mail-compose div[data-extension-id="bcc"] [title="super02@ox.com"]')
})

Scenario('[C12119] Edit recipients', async ({ I, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.click('CC')
  I.waitForElement('.io-ox-mail-compose .cc .tt-input')
  I.click('BCC')
  I.waitForElement('.io-ox-mail-compose .bcc .tt-input')

  I.click(mail.to)
  I.fillField(mail.to, 'foo@bar.de, lol@ox.io, bla@trash.com,')
  I.pressKey('Enter')
  I.waitForElement('.io-ox-mail-compose div.token')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 3)
  I.waitForText('foo@bar.de', undefined, '.io-ox-mail-compose [data-extension-id="to"]')
  I.waitForText('lol@ox.io', undefined, '.io-ox-mail-compose [data-extension-id="to"]')
  I.waitForText('bla@trash.com', undefined, '.io-ox-mail-compose [data-extension-id="to"]')
  // nth-of-type index 5, as there are two div elements (aria-description and live region) in front
  I.doubleClick('.io-ox-mail-compose div:nth-of-type(5).token')
  I.fillField(mail.to, 'super@ox.com,')
  I.pressKey('Enter')
  I.wait(0.5) // The test is shaky otherwise
  I.dontSee('bla@trash.com', '.io-ox-mail-compose [data-extension-id="to"]')
  I.waitForText('foo@bar.de', undefined, '.io-ox-mail-compose [data-extension-id="to"]')
  I.waitForText('lol@ox.io', undefined, '.io-ox-mail-compose [data-extension-id="to"]')
  I.waitForText('super@ox.com', undefined, '.io-ox-mail-compose [data-extension-id="to"]')
  I.click('.io-ox-mail-compose [aria-label="foo@bar.de"] .close')
  I.click('.io-ox-mail-compose [aria-label="lol@ox.io"] .close')
  I.click('.io-ox-mail-compose [aria-label="super@ox.com"] .close')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 0)

  I.click('.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input')
  I.fillField('.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input', 'foo@bar.de, lol@ox.io, bla@trash.com,')
  I.pressKey('Enter')
  I.waitForElement('.io-ox-mail-compose div.token')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 3)
  I.waitForText('foo@bar.de', undefined, '.io-ox-mail-compose [data-extension-id="cc"]')
  I.waitForText('lol@ox.io', undefined, '.io-ox-mail-compose [data-extension-id="cc"]')
  I.waitForText('bla@trash.com', undefined, '.io-ox-mail-compose [data-extension-id="cc"]')
  // nth-of-type index 5, as there are two div elements (aria-description and live region) in front
  I.doubleClick('.io-ox-mail-compose div:nth-of-type(5).token')
  I.fillField('.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input', 'super@ox.com,')
  I.pressKey('Enter')
  I.wait(0.5) // The test is shaky otherwise
  I.dontSee('bla@trash.com', '.io-ox-mail-compose [data-extension-id="cc"]')
  I.waitForText('foo@bar.de', undefined, '.io-ox-mail-compose [data-extension-id="cc"]')
  I.waitForText('lol@ox.io', undefined, '.io-ox-mail-compose [data-extension-id="cc"]')
  I.waitForText('super@ox.com', undefined, '.io-ox-mail-compose [data-extension-id="cc"]')
  I.click('.io-ox-mail-compose [aria-label="foo@bar.de"] .close')
  I.click('.io-ox-mail-compose [aria-label="lol@ox.io"] .close')
  I.click('.io-ox-mail-compose [aria-label="super@ox.com"] .close')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 0)

  I.click('.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input')
  I.fillField('.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input', 'foo@bar.de, lol@ox.io, bla@trash.com,')
  I.pressKey('Enter')
  I.waitForElement('.io-ox-mail-compose div.token')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 3)
  I.waitForText('foo@bar.de', undefined, '.io-ox-mail-compose [data-extension-id="bcc"]')
  I.waitForText('lol@ox.io', undefined, '.io-ox-mail-compose [data-extension-id="bcc"]')
  I.waitForText('bla@trash.com', undefined, '.io-ox-mail-compose [data-extension-id="bcc"]')
  // nth-of-type index 5, as there are two div elements (aria-description and live region) in front
  I.doubleClick('.io-ox-mail-compose div:nth-of-type(5).token')
  I.fillField('.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input', 'super@ox.com,')
  I.pressKey('Enter')
  I.wait(0.5) // The test is shaky otherwise
  I.dontSee('bla@trash.com', '.io-ox-mail-compose [data-extension-id="bcc"]')
  I.waitForText('foo@bar.de', undefined, '.io-ox-mail-compose [data-extension-id="bcc"]')
  I.waitForText('lol@ox.io', undefined, '.io-ox-mail-compose [data-extension-id="bcc"]')
  I.waitForText('super@ox.com', undefined, '.io-ox-mail-compose [data-extension-id="bcc"]')
  I.click('.io-ox-mail-compose [aria-label="foo@bar.de"] .close')
  I.click('.io-ox-mail-compose [aria-label="lol@ox.io"] .close')
  I.click('.io-ox-mail-compose [aria-label="super@ox.com"] .close')
  I.seeNumberOfElements('.io-ox-mail-compose div.token', 0)
})

Scenario('[C12120] Recipient cartridge', async ({ I, users, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')
  await I.login('app=io.ox/mail')
  await mail.newMail()

  I.say('Init tokenfield/typehead')
  I.click('CC')
  I.waitForElement('.io-ox-mail-compose .cc .tt-input')
  I.click('BCC')
  I.waitForElement('.io-ox-mail-compose .bcc .tt-input')
  const fields = ['to', 'cc', 'bcc']

  fields.forEach(function (field) {
    I.say(`Enter #1 in ${field}`)
    I.fillField(`.io-ox-mail-compose div[data-extension-id="${field}"] input.tt-input`, users[1].get('primaryEmail'))
    I.waitForVisible('.tt-dropdown-menu .tt-suggestions')
    I.pressKey('Enter')
    I.waitForInvisible('.tt-dropdown-menu .tt-suggestions')

    I.say(`Enter #2 in ${field}`)
    I.fillField(`.io-ox-mail-compose div[data-extension-id="${field}"] input.tt-input`, 'super@ox.com')
    I.pressKey('Enter')
    I.seeNumberOfElements({ css: `.io-ox-mail-compose div[data-extension-id="${field}"] div.token` }, 2)

    I.say(`Check in ${field}`)
    I.waitForText(users[1].get('given_name') + ' ' + users[1].get('sur_name'), undefined, `.io-ox-mail-compose [data-extension-id="${field}"] .token-label`)
  })
})

Scenario('[C12121] Display and hide recipient fields', async ({ I, mail }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')
  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.click('CC')
  I.waitForVisible('.io-ox-mail-compose .cc .tt-input')
  I.click('BCC')
  I.waitForVisible('.io-ox-mail-compose .bcc .tt-input')
  I.click('CC')
  I.waitForInvisible('.io-ox-mail-compose .cc .tt-input')
  I.click('BCC')
  I.waitForInvisible('.io-ox-mail-compose .bcc .tt-input')
})

Scenario('[C83384] Automatically bcc all messages', async ({ I, mail, users, settings }) => {
  await Promise.all([
    I.haveSetting('io.ox/mail//messageFormat', 'text'),
    users.create()
  ])
  await I.login('app=io.ox/mail&settings=virtual/settings/io.ox/mail')
  settings.expandSection('Advanced settings')
  I.scrollTo('[data-section="io.ox/mail/settings/advanced"]')
  I.waitForText('The given address is always added to BCC when replying or composing new emails.', undefined, '.settings-detail-pane')
  I.fillField('Recipient', users[1].get('primaryEmail'))
  settings.close()

  await mail.newMail()
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`, undefined, '.bcc div.token')
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Forever alone')
  I.fillField(mail.editorText, 'Sending this (not only) to myself')
  mail.send()
  I.waitForText('Forever alone', 30, '.list-view.mail-item')
  await mail.selectMail('Forever alone')
  await within({ frame: '.mail-detail-pane .mail-detail-frame' }, () => {
    I.waitForText('Sending this (not only) to myself')
  })
  I.dontSee(users[1].get('primaryEmail'))
  await I.logout()

  await I.login({ user: users[1] })
  I.selectFolder('Inbox')
  await mail.selectMail('Forever alone')
  await within({ frame: '.mail-detail-pane .mail-detail-frame' }, () => {
    I.waitForText('Sending this (not only) to myself')
  })
})

Scenario('[C101615] Emojis', async ({ I, users, mail }) => {
  await I.haveMail({ path: 'media/mails/C101615.eml' })
  await I.login('app=io.ox/mail')
  I.selectFolder('Inbox')
  await mail.selectMail('😉✌️❤️')
  I.waitForText('😉✌️❤️', undefined, '.mail-detail-pane .subject')
  await within({ frame: '.mail-detail-pane .mail-detail-frame' }, () => {
    I.waitForText('😉✌️❤️', undefined, '.mail-detail-content p')
  })
})

Scenario('[C101620] Very long TO field', async ({ I, users, mail }) => {
  await I.haveMail({ path: 'media/mails/C101620.eml' })
  await I.login('app=io.ox/mail')
  I.selectFolder('Inbox')
  await mail.selectMail('Very long TO field')
  I.seeCssPropertiesOnElements('.mail-detail-pane .recipients', { overflow: 'hidden' })
  I.seeCssPropertiesOnElements('.mail-detail-pane .recipients', { 'text-overflow': 'ellipsis' })
  // TODO: Width is not 100% when get css property?
  I.doubleClick('[title="Very long TO field"]')
  I.waitForElement('.io-ox-mail-detail-window')
  await within('.io-ox-mail-detail-window', () => {
    I.seeCssPropertiesOnElements('.floating-window-content .recipients', { overflow: 'hidden' })
    I.seeCssPropertiesOnElements('.floating-window-content .recipients', { 'text-overflow': 'ellipsis' })
  })
})

Scenario('[C207507] Forgot mail attachment hint', async ({ I, users, mail, dialogs }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')
  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.fillField(mail.to, 'super01@ox.de')
  I.fillField(mail.subject, 'C207507')
  I.fillField('.plain-text', 'see attachment')
  I.click('Send')

  // Test if attachment is mentioned in mail
  dialogs.waitForVisible()
  I.waitForText('Forgot attachment?', undefined, '.modal-dialog .modal-header')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
  I.fillField(mail.subject, 'see attachment')
  I.fillField('.plain-text', 'C207507')
  I.click('Send')

  // Test if attachment is mentioned in subject
  dialogs.waitForVisible()
  I.waitForText('Forgot attachment?', undefined, '.modal-dialog .modal-header')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
})

Scenario('[C274142]- Disable autoselect in mail list layout', async ({ I, users }) => {
  const mailcount = 10
  const promises = [I.haveSetting('io.ox/mail//layout', 'list')]
  let i
  for (i = 0; i < mailcount; i++) {
    promises.push(I.haveMail({
      attachments: [{ content: 'C274142\r\n', content_type: 'text/plain', raw: true, disp: 'inline' }],
      from: users[0],
      sendtype: 0,
      subject: 'C274142',
      to: users[0]
    }))
  }
  await Promise.all(promises)
  await I.login('app=io.ox/mail')
  I.waitForVisible('.io-ox-mail-window')
  I.see(String(mailcount), '[data-contextmenu-id="default0/INBOX"][data-model="default0/INBOX"] .folder-counter')
  I.dontSeeElement('[data-ref="io.ox/mail/listview"] [aria-selected="true"]')
})
