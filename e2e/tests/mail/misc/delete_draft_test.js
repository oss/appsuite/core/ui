/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Misc')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C114958] Delete draft when closing composer', async ({ I, users, mail, dialogs }) => {
  const mailListView = '.list-view.visible-selection.mail-item'
  const subject = 'Testsubject Draft'

  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.fillField(mail.to, users[1].get('primaryEmail'))
  I.fillField(mail.subject, subject)
  await within({ frame: mail.editorIframe }, () => {
    I.fillField('body', 'Testcontent')
  })
  I.click('~Close', mail.composeWindow)

  dialogs.clickButton('Save draft')
  I.waitForDetached(mail.composeWindow)
  I.selectFolder('Drafts')
  await I.waitForApp()
  I.waitForElement(mailListView)
  I.waitForVisible('.list-view li.list-item')
  await mail.selectMail(subject)
  await I.waitForFocus('.list-view li.list-item.selected')

  I.waitForElement('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, () => {
    I.waitForElement(locate('div.default-style').withText('Testcontent'))
  })

  I.waitForElement('.detail-view-header')
  I.click('Delete', '.detail-view-header')

  await within(mailListView, () => {
    I.waitForDetached('.list-item.selectable')
    I.dontSee(subject)
  })
})
