/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Listview')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C114381] Sender address is shown in tooltip', async ({ I, users, mail }) => {
  await I.haveMail({
    from: users[0],
    to: users[1],
    sendtype: 0,
    subject: 'C114381: sent',
    attachments: [{
      content: '<p style="background-color:#ccc">[C114381] Sender address is shown in draft tooltip</p>',
      content_type: 'text/html',
      disp: 'inline'
    }]
  })
  await I.haveMail({
    from: users[0],
    to: users[1],
    sendtype: 0,
    subject: 'C114381: draft',
    flags: 4,
    attachments: [{
      content: '<p style="background-color:#ccc">[C114381] Sender address is shown in draft tooltip</p>',
      content_type: 'text/html',
      disp: 'inline'
    }]
  })

  await I.login('app=io.ox/mail')

  I.selectFolder('Sent')
  I.waitForVisible('.leftside .list-view .list-item .from')
  I.waitForText('C114381: sent', undefined, '.list-view .list-item')
  expect(await I.grabAttributeFrom('.leftside .list-view .list-item .from', 'title')).to.be.equal(users[1].get('primaryEmail'))
  I.selectFolder('Drafts')
  I.waitForText('C114381: draft', undefined, '.leftside .list-view .list-item .subject')
  expect(await I.grabAttributeFrom('.leftside .list-view .list-item .from', 'title')).to.be.equal(users[1].get('primaryEmail'))
  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })

  I.waitForText('C114381: sent')
  expect(await I.grabAttributeFrom('.leftside .list-view .list-item .from', 'title')).to.be.equal(users[0].get('primaryEmail'))
})

Scenario('Remove mail from thread', async ({ I, users, mail }) => {
  await Promise.all([
    I.haveSetting('io.ox/mail//viewOptions', {
      'default0/INBOX': {
        order: 'desc',
        thread: true
      }
    }),
    I.haveMail({
      attachments: [{
        content: 'Hello world!',
        content_type: 'text/html',
        disp: 'inline'
      }],
      from: [[users[1].get('display_name'), users[1].get('primaryEmail')]],
      sendtype: 0,
      subject: 'Test subject',
      to: users[0]
    }, { user: users[1] })
  ])

  await I.haveMail({
    attachments: [{
      content: 'Hello world!',
      content_type: 'text/html',
      disp: 'inline'
    }],
    from: [[users[1].get('display_name'), users[1].get('primaryEmail')]],
    sendtype: 0,
    subject: 'You should see this!',
    to: users[0]
  }, { user: users[1] })

  await I.login('', { user: users[1] })

  I.waitForText('Sent')
  I.selectFolder('Sent')
  I.waitForText('Test subject')
  await mail.selectMail('Test subject')

  I.waitForElement('~Reply to sender')
  I.click('~Reply to sender')
  await I.waitForFocus(mail.editorIframe)
  await within(mail.composeWindow, () => {
    I.waitForVisible('.active .io-ox-mail-compose', 30)
    I.waitForInvisible('.window-blocker.io-ox-busy', 15)
    I.waitForText(`${users[0].get('given_name')} ${users[0].get('sur_name')}`, undefined, '[data-extension-id=to]')
  })
  mail.send()
  await I.logout()

  await I.login()

  I.waitForText('Test subject', undefined, '.subject')
  await mail.selectMail('Test subject')
  // wait for 2 mails rendered in thread list
  I.waitForFunction(() => document.querySelectorAll('.mail-detail').length === 2)

  I.dontSeeElement('[data-action="io.ox/mail/actions/reply"].disabled')
  I.waitForElement('.mail-detail.expanded [data-toolbar]')
  I.click('Delete', '.mail-detail.expanded [data-toolbar]')

  // wait for refresh here, because the middleware needs to send new data
  // should really happen within 1s
  I.waitForNetworkTraffic()
  // give listview a moment to update
  I.wait(0.5)

  // this should even be in the '.list-view .selected' context, but it needs more logic for threads
  I.waitForText('Test subject', undefined, '.list-view')

  I.waitNumberOfVisibleElements('.list-view .selectable', 2)
  I.click('~Delete')
  I.seeNumberOfVisibleElements('.list-view .selectable', 1)
})

Scenario('Select all messages via "More message options" context menu', async ({ I, users }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]

  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'First mail' }),
    I.haveMail({ from: sender, to: sender, subject: 'Second mail' })
  ])

  await I.login()

  I.click('~More message options')
  I.clickDropdown('Select all messages')
  I.waitNumberOfVisibleElements('.list-view .selected', 2)
  I.waitForText('2 messages selected')
})

Scenario('Text preview is shown', async ({ I, settings }) => {
  // enabled
  await Promise.all([
    I.haveMail({ path: 'media/mails/types/text_plain.eml' }),
    I.haveSetting({ 'io.ox/mail//showTextPreview': true })
  ])
  await I.login('app=io.ox/mail')
  I.waitForText('Characteristic of this mail')
  await settings.open('Mail', 'Reading')
  I.seeCheckboxIsChecked('Show text preview')
  await I.logout()

  // disabled
  await I.haveSetting({ 'io.ox/mail': { features: { textPreview: false } } })
  await I.login('app=io.ox/mail')
  I.waitForText('Plain text')
  I.dontSee('Characteristic of this mail')
  await settings.open('Mail', 'Reading')
  I.waitForText('Checkboxes')
  I.dontSee('Show text preview')
})
