/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Settings > Mail > IMAP subscriptions')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[OXUI-1157] Returns focus from Change IMAP Subscription Modal', async ({ I, settings }) => {
  await I.login()
  await settings.open('Mail', 'Folders')

  I.waitForText('Change IMAP subscriptions …')
  I.click('Change IMAP subscriptions …')
  I.waitForVisible('.modal.subscribe-imap-folder')
  I.pressKey('Tab')
  I.pressKey('Tab')
  I.pressKey('Escape')
  I.waitForDetached('.modal.subscribe-imap-folder')
  await I.waitForFocus('[data-action="change-image-supscriptions"]')
})

Scenario('IMAP folder subscription roundtrip', async ({ I, settings }) => {
  await Promise.all([
    I.haveFolder({ title: 'First Folder', module: 'mail', parent: 'default0' }),
    I.haveFolder({ title: 'Second Folder', module: 'mail', parent: 'default0' })
  ])

  // check current state
  await I.login()
  await I.waitForApp()
  I.click('.tree-container [data-model="virtual/myfolders"] .folder-arrow')
  I.waitForText('First Folder')
  I.waitForText('Second Folder')

  // unsubscribe "First Folder"
  await settings.open('Mail', 'Folders')
  I.waitForText('Change IMAP subscriptions …')
  I.click('Change IMAP subscriptions …')
  I.waitForVisible('.modal.subscribe-imap-folder')
  I.waitForText('E-Mail')
  I.waitForText('First Folder')
  I.waitForElement('.folder [value="default0/First Folder"]')
  I.click('.folder [value="default0/First Folder"]')
  I.click('Save')
  I.waitForDetached('.modal.subscribe-imap-folder')
  settings.close()
  await I.waitForApp()
  I.waitForText('Second Folder')
  I.dontSee('First Folder')
  I.logout()

  // check current state
  await I.login()
  await I.waitForApp()
  I.click('.tree-container [data-model="virtual/myfolders"] .folder-arrow')
  I.waitForText('Second Folder')
  I.dontSee('First Folder')

  // subscribe "First Folder" again
  await settings.open('Mail', 'Folders')
  I.waitForText('Change IMAP subscriptions …')
  I.click('Change IMAP subscriptions …')
  I.waitForVisible('.modal.subscribe-imap-folder')
  I.waitForText('E-Mail')
  I.waitForText('First Folder')
  I.waitForElement('.folder [value="default0/First Folder"]')
  I.click('.folder [value="default0/First Folder"]')
  I.click('Save')
  settings.close()
  I.waitForDetached('.modal.subscribe-imap-folder')
  I.waitForText('Second Folder')
  I.see('First Folder')
})
