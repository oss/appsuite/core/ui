/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Scenario('Mails with inline SVG encoded as BASE64 do not break during reply/forward', async ({ I, mail }) => {
  await I.haveMail({ path: 'media/mails/inline-svg-base64.eml' })
  await I.haveSetting({ 'io.ox/mail': { didYouKnow: { saveOnCloseDontShowAgain: true } } })
  await I.login('app=io.ox/mail')
  I.waitForText('Base64 SVG')
  await mail.selectMail('Base64 SVG')
  I.waitForElement('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    I.waitForElement('img[data-ref="svg-fallback"]')
  })
  I.click('~Reply to sender')
  I.waitForVisible(mail.composeWindow)
  I.waitForElement('iframe[title*="Rich Text Area"]')
  I.click('~Mail compose actions')
  I.clickDropdown('Save draft and close')
  I.waitForDetached('.io-ox-mail-compose')
  I.dontSee('Upload file is invalid or illegal.')
})
