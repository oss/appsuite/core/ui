/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > External Accounts')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

async function createExampleDraft () {
  const { I, dialogs, mail } = inject()
  await mail.newMail()
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.fillField(mail.subject, 'Test subject draft')
  I.click('~Close', mail.composeWindow)
  dialogs.clickButton('Save draft')
  I.waitForDetached(mail.composeWindow)
}

Scenario('[C125352] No mail oauth service available', async ({ I }) => {
  await I.login('app=io.ox/mail')

  I.click('~More actions', '.primary-action')
  I.clickDropdown('Add email account', '.primary-action')

  /// /Check to see whether mail account wizard is shown up
  I.waitForElement('.add-mail-account-address', 30)
  I.seeElement('.add-mail-account-password')
})

Scenario('[OXUIB-225] Password recovery for account passwords after password change', async ({ I, dialogs, users }) => {
  await I.haveMailAccount({ additionalAccount: users[1], name: 'My External', extension: 'ext' })

  await I.login()
  // Check for the external account being registered
  I.waitForText('My External')
  I.dontSeeElement('.modal-dialog')
  await I.logout()

  // Change password using external system
  await users[0].change({ id: users[0].get('id'), password: 'secret2' })
  users[0].userdata.password = 'secret2'
  await I.login()
  I.waitForText('My External')
  dialogs.waitForVisible()
  dialogs.clickButton('Remind me again')
  I.waitToHide('.modal-dialog')

  I.refreshPage()
  I.waitForText('My External', 10)
  dialogs.waitForVisible()
  dialogs.clickButton('Remove passwords')
  I.waitToHide('.modal-dialog')

  I.refreshPage()
  I.waitForText('My External', 10)
  I.dontSeeElement('.modal-dialog')
})

Scenario('[OXUIB-1966] Permissions dialog is disabled for external and secondary accounts', async ({ I, mail, users, dialogs }) => {
  const additionalAccount = await users.create()
  await I.haveMailAccount({ additionalAccount, name: 'My External', extension: 'ext' })
  await I.login()

  I.waitForText('My External')
  I.rightClick('My External')
  I.waitForElement('.dropdown.open .dropdown-menu')
  I.dontSee('Permission', '.dropdown.open .dropdown-menu')
  I.pressKey('Escape')
  I.waitForDetached('.dropdown.open .dropdown-menu')
  I.click('.folder-arrow', '~My External')
  I.waitForText('Inbox', undefined, '~My External')
  I.rightClick('Inbox', '~My External')
  I.waitForElement('.dropdown.open .dropdown-menu')
  I.dontSee('Permission', '.dropdown.open .dropdown-menu')
})

Scenario.skip('[OXUI-1026] Testing with external SMTP enabled', async ({ I, mail, users, settings, dialogs }) => {
  await users[0].hasConfig('com.openexchange.mail.smtp.allowExternal', true)
  const defaultAddress = users[0].get('primaryEmail')
  const externalUser = users[1]
  const externalAddress = externalUser.get('primaryEmail')

  // Preparing external account
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  const recipient = [[externalUser.get('display_name'), externalAddress]]
  await I.haveMail({ from: sender, to: recipient, subject: 'Test subject mail' })

  await I.login('app=io.ox/mail', { user: externalUser })
  await createExampleDraft()
  await I.logout()

  await I.login()

  // Checking absence of disabled external SMTP note when adding accounts
  I.click('~More actions', '.primary-action')
  I.waitForElement('.btn-group.open')
  I.clickDropdown('Add email account')
  I.waitForText('Your credentials will be sent over a secure connection only', undefined, '.modal .help-block')
  I.dontSee('External email accounts are read-only')

  // Checking for `Outgoing server (SMTP)` in `Add mail account` form
  I.fillField('.modal #add-mail-account-address', externalAddress)
  dialogs.clickButton('Add')
  I.waitForText('Auto-configuration failed', 10, '.modal')
  I.click('Configure manually', '.modal')
  I.waitForText('Outgoing server (SMTP)', 10, '.modal')
  I.fillField('.modal #name', 'External Mail Account')
  I.fillField('.modal #mail_server', externalUser.get('imapServer'))
  I.fillField('.modal #password', externalUser.get('password'))
  I.fillField('.modal #transport_server', externalUser.get('smtpServer'))
  dialogs.clickButton('Save')
  I.waitForDetached('.modal')
  I.waitForText('External Mail Account', 10, '.window-sidepanel')

  // Checking for selectable external mail account in mail compose
  await mail.newMail()
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.waitForText(externalAddress)
  I.clickDropdown('Edit names')
  I.waitForText('Edit real names')
  I.waitForText(externalAddress, 10, '.modal-dialog')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
  I.click('~Close', mail.composeWindow)
  I.waitForDetached(mail.composeWindow)

  // Checking external sender when composing mail from external account folder
  I.doubleClick('External Mail Account', '.window-sidepanel')
  I.waitForText('Inbox', 10, '.window-sidepanel .virtual.remote-folders')
  I.waitForText('External Mail Account', 10, '.list-view-control')
  await mail.newMail()
  I.waitForText(externalAddress, 10, '.io-ox-mail-compose-window .sender')
  I.click('~Close', mail.composeWindow)
  I.waitForDetached(mail.composeWindow)

  // Checking external sender when replying to external mail
  I.click('~Inbox', '.window-sidepanel .virtual.remote-folders')
  await mail.selectMail('Test subject mail')
  I.click('~Reply to sender', '.rightside')
  I.waitForText(externalAddress, 10, '.io-ox-mail-compose-window .sender')
  I.waitForElement('.recipient .mail-input')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.click('.recipient .mail-input')
  I.fillField(mail.to, defaultAddress)
  mail.send()

  // Checking external sender when opening external draft
  I.click('div[title*="Drafts"]', '.window-sidepanel .virtual.remote-folders')
  await mail.selectMail('Test subject draft')
  I.click('~Edit draft', '.rightside')
  I.waitForText(externalAddress, 10, '.io-ox-mail-compose-window .sender')
  I.waitForElement('.recipient .mail-input')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.click('.recipient .mail-input')
  I.fillField(mail.to, defaultAddress)
  mail.send()

  // Checking for `Outgoing server (SMTP)` in account settings form
  await settings.open('Accounts')
  I.waitForText('External Mail Account', 10)
  I.click('~Edit ' + 'External Mail Account')
  I.waitForText('Outgoing server (SMTP)', 10)

  // Checking if the form can be submitted
  dialogs.clickButton('Save')
  I.waitForElement('.io-ox-alert svg.bi-check-lg', 10)
})

Scenario.skip('[OXUI-1026] Testing with external SMTP disabled', async ({ I, mail, users, dialogs, settings }) => {
  await users[0].hasConfig('com.openexchange.mail.smtp.allowExternal', false)
  const externalUser = users[1]
  const externalAddress = externalUser.get('primaryEmail')

  // Preparing external account
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  const recipient = [[externalUser.get('display_name'), externalAddress]]
  await I.waitForSetting({ 'io.ox/mail': { features: { allowExternalSMTP: false } } }, 60)
  await I.haveMail({ from: sender, to: recipient, subject: 'Test subject mail' })

  await I.login('app=io.ox/mail', { user: externalUser })
  await createExampleDraft()
  await I.logout()

  await I.login()

  // Checking for disabled external SMTP note when adding accounts
  I.click('~More actions', '.primary-action')
  I.waitForElement('.btn-group.open')
  I.clickDropdown('Add email account')
  I.waitForText('External email accounts are read-only', 10, '.modal .smtp-disabled')

  // Checking absence of `Outgoing server (SMTP)` in `Add mail account` form
  I.fillField('.modal #add-mail-account-address', externalAddress)
  dialogs.clickButton('Add')
  I.waitForText('Auto-configuration failed', 10, '.modal')
  I.click('Configure manually', '.modal')
  I.waitForText('Incoming server', 10, '.modal')
  I.dontSee('Outgoing server (SMTP)', '.modal')
  I.fillField('.modal #name', 'External Mail Account')
  I.fillField('.modal #mail_server', externalUser.get('imapServer'))
  I.fillField('.modal #password', externalUser.get('password'))
  dialogs.clickButton('Save')
  I.waitForElement('.io-ox-alert svg.bi-check-lg', 10)

  // Checking for missing external mail account mail compose
  await mail.newMail()
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.dontSee(externalAddress)
  I.clickDropdown('Edit names')
  I.waitForText('Edit real names')
  I.dontSee(externalAddress, '.modal-dialog')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
  I.click('~Close', mail.composeWindow)

  // Checking for default address when composing mail from external account folder
  I.doubleClick('External Mail Account', '.window-sidepanel')
  I.waitForText('Inbox', 10, '.window-sidepanel .virtual.remote-folders')
  I.click('~Inbox', '.window-sidepanel .virtual.remote-folders')
  await mail.newMail()
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.dontSee(externalAddress)
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.dropdown.open')
  I.dontSee(externalAddress)
  I.pressKey('Escape')
  I.waitForDetached('.dropdown.open')
  I.pressKey('Escape')
  I.waitForDetached(mail.composeWindow, 10)
  await mail.selectMail('Test subject mail')

  // Reply
  I.click('~Reply to sender', '.rightside')
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.dontSee(externalAddress)
  I.click('~From')
  I.waitForVisible('.dropdown.open')

  I.waitForText(users[0].get('primaryEmail'), undefined, '.dropdown.open')
  I.dontSee(externalAddress)
  I.pressKey('Escape')
  I.waitForDetached('.dropdown.open')
  I.pressKey('Escape')
  I.waitForDetached(mail.composeWindow)

  // Reply all
  I.click('~Reply to all recipients', '.rightside')
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.dontSee(externalAddress)
  I.waitForElement('iframe[title^="Rich Text Area"]')
  I.click('~From')
  I.waitForVisible('.dropdown.open')

  I.waitForText(users[0].get('primaryEmail'), undefined, '.dropdown.open')
  I.dontSee(externalAddress)
  I.pressKey('Escape')
  I.waitForDetached('.dropdown.open')
  I.pressKey('Escape')
  I.waitForDetached(mail.composeWindow)

  // Forward
  I.click('~Forward', '.rightside')
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  await I.waitForFocus('[placeholder="To"]')
  I.dontSee(externalAddress)
  I.click('~From')
  I.waitForVisible('.dropdown.open')

  I.dontSee(externalAddress)
  I.waitForText(users[0].get('primaryEmail'), undefined, '.dropdown.open')
  I.pressKey('Escape')
  I.waitForDetached('.dropdown.open')
  I.pressKey('Escape')
  I.waitForDetached(mail.composeWindow)

  // Checking sender replacement for external drafts
  I.click('div[title*="Drafts"]', '.window-sidepanel .virtual.remote-folders')
  await mail.selectMail('Test subject draft')
  I.click('~Edit draft', '.rightside')
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.dontSee(externalAddress)
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.waitForVisible('.dropdown.open')

  I.dontSee(externalAddress)
  I.waitForText(users[0].get('primaryEmail'), undefined, '.dropdown.open')
  I.pressKey('Escape')
  I.waitForDetached('.dropdown.open')
  I.pressKey('Escape')

  // Checking absence of `Outgoing server (SMTP)` in account settings form
  await settings.open('Accounts')
  I.waitForText('External Mail Account', 10)
  I.click('~Edit External Mail Account')
  I.waitForText('Incoming server', 10)
  I.dontSee('Outgoing server (SMTP)')

  // Checking if the form can be submitted
  dialogs.clickButton('Save')
  I.waitForElement('.io-ox-alert svg.bi-check-lg', 10)
})

Scenario.skip('[OXUI-1026] Testing when SMTP gets disabled for existing external account', async ({ I, users, mail, dialogs }) => {
  await users[0].hasConfig('com.openexchange.mail.smtp.allowExternal', true)
  const externalUser = users[1]
  const externalAddress = externalUser.get('primaryEmail')

  // Preparing external account
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  const recipient = [[externalUser.get('display_name'), externalAddress]]
  await I.waitForSetting({ 'io.ox/mail': { features: { allowExternalSMTP: true } } }, 10)
  await I.haveMail({ from: sender, to: recipient, subject: 'Test subject mail' })

  await I.login('app=io.ox/mail', { user: externalUser })
  await createExampleDraft()
  await I.logout()

  await I.login()

  // Checking absence of disabled external SMTP note when adding accounts
  I.click('~More actions', '.primary-action')
  I.waitForElement('.btn-group.open')
  I.clickDropdown('Add email account')
  I.waitForText('Your credentials will be sent over a secure connection only', undefined, '.modal .help-block')
  I.dontSee('External email accounts are read-only')

  // Checking for `Outgoing server (SMTP)` in `Add mail account` form
  I.fillField('.modal #add-mail-account-address', externalAddress)
  dialogs.clickButton('Add')
  await within(dialogs.body, () => {
    I.waitForText('Auto-configuration failed', 10)
    I.click('Configure manually')
    I.waitForText('Outgoing server (SMTP)', 10)
    I.fillField('#name', 'External Mail Account')
    I.fillField('#mail_server', externalUser.get('imapServer'))
    I.fillField('#password', externalUser.get('password'))
    I.fillField('#transport_server', externalUser.get('smtpServer'))
  })
  dialogs.clickButton('Save')
  I.waitForDetached('.modal-dialog')
  I.waitForText('External Mail Account', 10, '.window-sidepanel')

  await I.logout()

  await users[0].hasConfig('com.openexchange.mail.smtp.allowExternal', false)
  await I.waitForSetting({ 'io.ox/mail': { features: { allowExternalSMTP: false } } }, 30)

  await I.login()

  // Checking for selectable external mail account in mail compose
  await mail.newMail()
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.dontSee(externalAddress)
  I.pressKey('Escape')
  I.click('~Close', mail.composeWindow)
  I.waitForDetached(mail.composeWindow)
  I.waitForText('External Mail Account', 10, '.window-sidepanel')

  I.doubleClick('External Mail Account', '.window-sidepanel')
  I.waitForText('Inbox', 10, '.window-sidepanel .virtual.remote-folders')
  I.click('~Inbox', '.window-sidepanel .virtual.remote-folders')
  await mail.newMail()
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  I.dontSee(externalAddress)
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.waitForElement('.dropdown.open')
  I.dontSee(externalAddress)
  I.pressKey('Escape')
  I.click('~Close', mail.composeWindow)
  I.waitForDetached(mail.composeWindow, 10)
  await mail.selectMail('Test subject mail')
  I.click('~Reply to sender', '.rightside')
  I.waitForVisible('.io-ox-mail-compose iframe')
  I.waitForDetached('.io-ox-mail-compose .io-ox-busy')
  I.waitForVisible({ css: 'li[data-extension-id="composetoolbar-menu"]' })
  I.waitForText('From', 10, '.io-ox-mail-compose-window .sender')
  I.dontSee(externalAddress)
  I.waitForElement(mail.editorIframe)
  await within(mail.composeWindow, async () => {
    await I.waitForFocus(mail.editorIframe)
  })
  I.click('.io-ox-mail-compose-window .sender-dropdown-link')
  I.waitForElement('.dropdown.open')
  I.dontSee(externalAddress)
  I.pressKey('Escape')
  I.click('~Close', mail.composeWindow)
  I.waitForDetached(mail.composeWindow)
})
