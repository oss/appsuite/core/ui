/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail Send')

Before(async ({ users }) => {
  await users.create() // Sender
  await users.create() // Recipient
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Send and receive mail @smoketest', async ({ I, users, mail }) => {
  const [sender, recipient] = users

  await I.login('app=io.ox/mail', { user: sender })
  await mail.newMail()
  I.fillField(mail.to, recipient.get('primaryEmail'))
  I.fillField(mail.subject, 'Test mail')
  mail.send()
  await I.logout()

  await I.login('app=io.ox/mail', { user: recipient })

  let element = await I.grabNumberOfVisibleElements('.list-item.selectable')
  let retries = 6
  while (!element && retries) {
    retries--
    I.waitForNetworkTraffic()
    I.triggerRefresh()
    I.waitForNetworkTraffic()
    I.wait(10.5)
    element = await I.grabNumberOfVisibleElements('.list-item.selectable')
    if (!retries) assert.fail('Timeout waiting for element')
  }
  I.waitForText('Test mail')
})

Scenario('Send email with keyboard shortcut', async ({ I, users, mail }) => {
  await I.login('app=io.ox/mail')
  await mail.newMail()

  I.fillField(mail.to, users[0].get('primaryEmail'))
  await within({ frame: '.editor iframe' }, () => {
    I.fillField('body', 'Editing draft')
  })
  I.fillField(mail.subject, 'Testsubject')

  I.pressKey(['CommandOrControl', 'Enter'])
  I.waitForDetached('.io-ox-mail-compose')
})

Scenario('Send email with keyboard shortcut from To: field', async ({ I, users, mail }) => {
  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.fillField(mail.subject, 'Testsubject')
  await within({ frame: '.editor iframe' }, () => {
    I.fillField('body', 'Editing draft')
  })
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.pressKey('Enter') // confirm recipient
  I.pressKey(['CommandOrControl', 'Enter'])
  I.waitForDetached('.io-ox-mail-compose')
})
