/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail Compose')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C8821] Send mail with Hyperlink', async ({ I, mail }) => {
  const hyperLink = 'https://foo.bar'
  const linkText = 'appsuite link'
  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, 'foo@bar.com')
  I.fillField(mail.subject, 'test subject')
  I.click('~Insert/edit link')
  I.waitForVisible('.tox-dialog')
  I.fillField('URL', hyperLink)
  I.fillField('Text to display', linkText)
  I.click('Save')
  I.waitForVisible('#mce_0_ifr')
  await within({ frame: '#mce_0_ifr' }, () => {
    I.waitForText(linkText)
    I.click(linkText)
  })
  I.click('~Insert/edit link')
  I.waitForVisible('.tox-dialog')
  I.seeInField('URL', hyperLink)
  I.seeInField('Text to display', linkText)
  I.click('Save')
  mail.send()
  I.selectFolder('Sent')
  await mail.selectMail('test subject')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, () => {
    I.waitForText(linkText)
    I.click(linkText)
  })
  // sometimes it takes a little while to open the link
  let times = 0
  for (let i = 1; i <= 10 && (await I.grabNumberOfOpenTabs()) < 2; i++) {
    I.wait(0.1)
    times = i
  }
  expect(times, 'number of open tabs is 2 within 1s').to.be.below(10)
})

Scenario('[C8822] Send Mail with Hyperlink from existing text', async ({ I, mail }) => {
  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, 'foo@bar.com')
  I.fillField(mail.subject, 'test subject')
  await within({ frame: '#mce_0_ifr' }, () => {
    I.fillField('.mce-content-body', 'testlink')
    I.doubleClick({ css: 'div.default-style' })
  })
  I.click('~Insert/edit link')
  I.waitForVisible('.tox-dialog')
  I.fillField('URL', 'http://foo.bar')
  I.click('Save')
  await within({ frame: '#mce_0_ifr' }, () => {
    I.seeElement('a')
  })
  mail.send()
  I.selectFolder('Sent')
  I.waitForText('test subject', 30, '.list-view li[data-index="0"]')
  I.click('.list-view li[data-index="0"]')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, () => {
    I.waitForText('testlink')
  })
})

Scenario('[C8823] Send Mail with Hyperlink by typing the link', async ({ I, mail }) => {
  // test String has to contain whitespace at the end for URL converting to work
  const testText = 'Some test text https://foo.bar  '
  await I.login('app=io.ox/mail')
  await mail.newMail()
  I.fillField(mail.to, 'foo@bar.com')
  I.fillField(mail.subject, 'test subject')
  I.wait(0.5)
  await within({ frame: '#mce_0_ifr' }, () => {
    I.fillField('.mce-content-body', testText)
    I.seeElement('a')
  })
  mail.send()
  I.selectFolder('Sent')
  I.waitForText('test subject', 30, '.list-view li[data-index="0"]')
  I.click('.list-view li[data-index="0"]')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, () => {
    I.waitForText(testText.trim())
    I.seeElement({ css: 'a[href="https://foo.bar"]' })
  })
})

Scenario('[C8824] Remove hyperlinks', async ({ I, mail }) => {
  const iframeLocator = mail.editorIframe
  const defaultText = 'Dies ist ein testlink http://example.com.'

  await I.login('app=io.ox/mail')
  await mail.newMail()

  I.click('~Maximize')

  // Write some text with the default settings
  await within({ frame: iframeLocator }, async () => {
    I.click('.default-style')
    I.fillField({ css: 'body' }, defaultText)
    I.pressKey('Enter')
    I.see('http://example.com', 'a')
    I.pressKey('ArrowLeft')
    I.pressKey('ArrowLeft')
    I.pressKey('ArrowLeft')
  })

  I.click('~Insert/edit link')
  I.waitForVisible('.tox-dialog')
  I.fillField('URL', '')
  I.pressKey('Enter')

  await within({ frame: iframeLocator }, async () => {
    I.dontSeeElement('a')
  })
})
