/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail Compose > Attachment quota')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('I can not send too large files as mail attachments', async ({ I, users, mail }) => {
  await I.login('app=io.ox/mail')

  await I.executeScript(async function () {
    const { settings } = await import(String(new URL('io.ox/core/settings.js', location.href)))
    return settings.set('properties', { attachmentQuotaPerFile: 2 * 1024 * 1024 })
  })

  // compose mail with receiver and subject
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test')

  // add a too large file as attachment
  await I.createGenericFile('16MB.dat', 16 * 1024 * 1024)
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/16MB.dat')
  I.waitForText('The file "16MB.dat" cannot be uploaded because it exceeds the maximum file size of 2 MB', undefined, '.io-ox-alert.io-ox-alert-error')
  I.waitForElement('[data-action="close"]')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error')

  // change attachment and send successfully
  await I.createGenericFile('2MB.dat', 2 * 1024 * 1024)
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/2MB.dat')
  I.waitForVisible(locate('.attachment-list.preview').withText('dat').as('DAT file'))
  I.waitForDetached('.progress-container', 15)

  // send mail successfully
  mail.send()
  I.waitForElement('.list-item.selectable.unread', 30)
})

Scenario('I can not send too large accumulated mail attachments', async ({ I, users, mail }) => {
  await I.login('app=io.ox/mail')

  await I.executeScript(async function () {
    const { settings } = await import(String(new URL('io.ox/core/settings.js', location.href)))
    settings.set('properties', { attachmentQuotaPerFile: 16 * 1024 * 1024 })
    return settings.set('properties', { attachmentQuota: 5 * 1024 * 1024 })
  })

  // compose mail with receiver and subject
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test')

  // add attachments
  await I.createGenericFile('2MB.dat', 2 * 1024 * 1024)
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/2MB.dat')
  I.waitForVisible(locate('.attachment-list.preview').withText('dat').as('DAT file'))
  I.waitForDetached('.progress-container', 15)
  await I.createGenericFile('16MB.dat', 16 * 1024 * 1024)
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/16MB.dat')

  I.waitForText('The file "16MB.dat" cannot be uploaded because it exceeds the total attachment size limit of 5 MB', undefined, '.io-ox-alert.io-ox-alert-error')
})

Scenario('I can send multiple files up to maximum attachment limit', async ({ I, users, mail }) => {
  await Promise.all([
    I.createGenericFile('2MB.dat', 2 * 1024 * 1024),
    I.createGenericFile('16MB.dat', 16 * 1024 * 1024),
    I.haveSetting('io.ox/mail//attachments/layout/compose/large', 'list')
  ])
  await I.login('app=io.ox/mail')

  await I.executeScript(async function () {
    const { settings } = await import(String(new URL('io.ox/core/settings.js', location.href)))
    return settings.set('properties', { attachmentQuota: 6 * 1024 * 1024 })
  })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test')

  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/2MB.dat')

  // try adding a too large file as attachment to make sure it will not be accepted.
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/16MB.dat')
  I.waitForText('The file "16MB.dat" cannot be uploaded because it exceeds the total attachment size limit of 6 MB', 5, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/2MB.dat')

  // After reaching the maximum quota, we try to upload a large file again to check that the file is still not accepted
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/16MB.dat')
  I.waitForText('The file "16MB.dat" cannot be uploaded because it exceeds the total attachment size limit of 6 MB', 5, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error', 5)

  // change attachment and send successfully
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/2MB.dat')
  I.waitForDetached('.progress-container', 20, '.share-attachments')

  // send mail successfully
  mail.send()
  I.waitForElement('.list-item.selectable.unread', 30, '.list-view.mail-item')
})

Scenario('I can send multiple inline images up to maximum attachment limit', async ({ I, users, mail, tinymce }) => {
  await I.haveSetting('io.ox/mail//attachments/layout/compose/large', 'list')
  await I.login('app=io.ox/mail')

  await I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    return mailSettings.set('compose/maxMailSize', 5 * 1024)
  })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test')

  await tinymce.attachInlineImage('media/placeholder/800x600.png')
  await within({ frame: '.tox-edit-area iframe' }, async () => {
    I.waitForElement('#tinymce img')
    I.dontSee('The file cannot be uploaded because it exceeds the maximum email size of 5 kB')
  })

  await tinymce.attachInlineImage('media/placeholder/800x600-mango.png')
  await within({ frame: '.tox-edit-area iframe' }, async () => {
    I.waitForElement('#tinymce img')
    I.dontSee('The file cannot be uploaded because it exceeds the maximum email size of 5 kB')
  })

  await tinymce.attachInlineImage('media/placeholder/800x600-limegreen.png')
  I.waitForText('The file cannot be uploaded because it exceeds the maximum email size of 5 kB', undefined, '.io-ox-alert.io-ox-alert-error')
})

// no file input field in tinymce 5
Scenario('I can not send an email that exceeds the mail max size', async ({ I, users, mail, tinymce }) => {
  await I.login('app=io.ox/mail')

  await I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    return mailSettings.set('compose/maxMailSize', 3 * 1024)
  })

  // compose mail with receiver and subject
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test')

  // first attached, second inline
  I.attachFile('.composetoolbar input[type="file"]', 'media/placeholder/800x600-limegreen.png')
  I.waitForDetached('.progress-container', 15)
  tinymce.attachInlineImage('media/placeholder/800x600.png')

  I.waitForText('The file cannot be uploaded because it exceeds the maximum email size of 3 kB', undefined, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error')
  await within({ frame: '.tox-edit-area iframe' }, async () => {
    I.dontSee('#tinymce img')
    expect(await I.grabNumberOfVisibleElements('#tinymce img')).to.equal(0)
  })

  // remove attached
  I.click('.mail-attachment-list a[title="Toggle preview"]')
  I.waitForElement('.mail-attachment-list button[title=\'Remove attachment "800x600-limegreen.png"\']')
  I.click('.mail-attachment-list button[title=\'Remove attachment "800x600-limegreen.png"\']')
  I.waitForDetached('.mail-attachment-list span[title="2MB.dat"]')

  // first inline, second attached
  tinymce.attachInlineImage('media/placeholder/800x600-limegreen.png')
  await within({ frame: '.tox-edit-area iframe' }, async () => {
    I.waitForElement('#tinymce img')
    expect(await I.grabNumberOfVisibleElements('#tinymce img')).to.equal(1)
  })
  tinymce.attachInlineImage('media/placeholder/800x600.png')

  I.waitForText('The file cannot be uploaded because it exceeds the maximum email size of 3 kB', undefined, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error')

  // try multiple inline images
  tinymce.attachInlineImage('media/placeholder/800x600-mango.png')
  I.waitForText('The file cannot be uploaded because it exceeds the maximum email size of 3 kB', undefined, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error')

  await within({ frame: '.tox-edit-area iframe' }, async () => {
    I.waitForElement('#tinymce img')
    expect(await I.grabNumberOfVisibleElements('#tinymce img')).to.equal(1)
  })
})

// TODO: not able to adjust quota with `I.executeScript` approach 😞
Scenario.skip('[OXUIB-1089] I can send a quota exceeding file out of drive by mail', async ({ I, drive }) => {
  // file size 9462 (4731 * 2)
  await I.haveFile(await I.grabDefaultFolder('infostore'), 'media/files/generic/testdocument.odt')

  await I.login('app=io.ox/files')

  // adjust quota
  await I.executeScript(async function () {
    const { default: quotaAPI } = await import(String(new URL('io.ox/core/api/quota.js', location.href)))
    quotaAPI.mailQuota.set('quota', 1024)
  })

  drive.selectFile('testdocument.odt')
  I.waitForVisible('~Details')
  I.clickToolbar('More actions')
  I.clickDropdown('Send by email')

  I.waitForText('Mail quota limit reached.')

  I.waitForVisible('.active .io-ox-mail-compose [placeholder="To"]', 30)
  await I.waitForFocus('.active .io-ox-mail-compose [placeholder="To"]')
  I.see('ODT')
  I.seeCheckboxIsChecked('.share-attachments [type="checkbox"]')
})

// TODO: not able to adjust quota with `I.executeScript` approach 😞
Scenario.skip('[OXUIB-1089] I can send file out of drive by mail', async ({ I, drive }) => {
  // file size 9462 (4731 * 2)
  await I.haveFile(await I.grabDefaultFolder('infostore'), 'media/files/generic/testdocument.odt')

  await I.login('app=io.ox/files')

  // adjust quota
  await I.executeScript(async function () {
    const { default: quotaAPI } = await import(String(new URL('io.ox/core/api/quota.js', location.href)))
    quotaAPI.mailQuota.set('quota', 16 * 1024)
  })

  drive.selectFile('testdocument.odt')
  I.waitForVisible('~Details')
  I.clickToolbar('More actions')
  I.clickDropdown('Send by email')

  I.waitForVisible('.active .io-ox-mail-compose [placeholder="To"]', 30)
  await I.waitForFocus('.active .io-ox-mail-compose [placeholder="To"]')
  I.see('ODT')
  I.dontSeeCheckboxIsChecked('.share-attachments [type="checkbox"]')
})

// FIXME: Test is too shaky
Scenario.skip('I can not use drive if the infoStore limits get exceeded', async ({ I, users, mail, tinymce }) => {
  await I.login('app=io.ox/mail')
  await I.executeScript(async function () {
    const { settings: coreSettings } = await import(String(new URL('io.ox/core/settings.js', location.href)))
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    mailSettings.set('compose/maxMailSize', 3 * 1024)
    return coreSettings.set('properties', { infostoreQuota: 7 * 1024 })
  })

  // compose mail with receiver and subject
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test')

  // attach
  I.attachFile('.composetoolbar input[type="file"]', 'media/placeholder/800x600.png')
  I.waitForDetached('.progress-container', 30)

  // error for attachment and inline
  I.attachFile('.composetoolbar input[type="file"]', 'media/placeholder/800x600-limegreen.png')
  I.waitForText('The file cannot be uploaded because it exceeds the maximum email size of 3 kB', undefined, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error')
  tinymce.attachInlineImage('media/placeholder/800x600-limegreen.png')
  I.waitForText('The file cannot be uploaded because it exceeds the maximum email size of 3 kB', undefined, '.io-ox-alert.io-ox-alert-error')
  I.click('[data-action="close"]', '.io-ox-alert.io-ox-alert-error')
  I.waitForDetached('.io-ox-alert.io-ox-alert-error')

  // use drive
  I.checkOption('Use Drive Mail', '.share-attachments')

  // attach another
  I.attachFile('.composetoolbar input[type="file"]', 'media/placeholder/800x600-mango.png')
  I.waitForDetached('.progress-container', 30)
  tinymce.attachInlineImage('media/placeholder/800x600.png')
  I.wait(2)
  I.waitForResponse(response => response.url().includes('attachments?clientToken='))
  await within({ frame: '.tox-edit-area iframe' }, async () => {
    I.waitForElement('#tinymce img')
    expect(await I.grabNumberOfVisibleElements('#tinymce img')).to.equal(1)
  })
  I.wait(0.5)
  I.uncheckOption('Use Drive Mail', '.share-attachments')
  I.waitForElement('.io-ox-alert.io-ox-alert-error')
  I.waitForText('The uploaded attachment exceeds the maximum email size of 3 kB', undefined, '.io-ox-alert.io-ox-alert-error')
})
