/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Scheduled Mail')

Before(async ({ I, users }) => {
  await users.create()
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Scheduled mail roundtrip', async ({ I, users, mail }) => {
  await I.haveSetting({ 'io.ox/core': { features: { scheduleSend: true } } })

  await I.login()
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Scheduled mail roundtrip test')
  await within({ frame: '.io-ox-mail-compose-window .editor iframe' }, async () => {
    I.fillField('body', 'Scheduled mail roundtrip test')
  })

  // schedule mail with time picker
  I.click('~Schedule email')
  I.click('Pick a date and time …')
  I.waitForElement('input[name="date"]')
  I.fillField('input[name="date"]', '2000-01-01')
  I.click('Schedule email')
  I.see('Date must be in the future.')
  I.fillField('input[name="date"]', '2100-01-01')
  I.fillField('input[name="time"]', '14:00')
  I.click('Schedule email')
  I.waitForText('Sending planned for', undefined, '#io-ox-message-container')
  I.seeTextEquals(
    'Sending planned for 01/01/2100 2:00 PM',
    '#io-ox-message-container .state'
  )
  I.seeTextEquals(
    'Scheduled mail roundtrip test',
    '#io-ox-message-container .mail-send-progress-subject'
  )
  I.click('Close', '#io-ox-message-container')

  // wait for folder become available
  I.waitForText('Scheduled', 10, '.folder-tree')
  I.selectFolder('Scheduled')
  I.waitForText('01/01/2100 2:00 PM', undefined, '.list-view .date.scheduled')
  await mail.selectMail('Scheduled mail roundtrip test')
  I.waitForText('This email is scheduled to be sent on')
  I.seeTextEquals(
    'This email is scheduled to be sent on 01/01/2100 2:00 PM.',
    '.notification-banner span:nth-child(2)'
  )
  I.seeTextEquals('Cancel send', '.notification-banner button')

  // begin editing mail
  I.seeElement('.classic-toolbar .bi.bi-pencil')
  I.click('~Edit draft')
  I.seeTextEquals('Schedule will be discarded', '.modal-dialog .modal-header')
  I.seeTextEquals('Editing this email will discard the sending schedule.', '.modal-dialog .modal-body')
  I.click('Edit')

  // Check that editing the mail moved it to drafts
  I.waitForElement('[title="Scheduled (Total: 0, Unread: 0)"]', 10)
  I.waitForElement('[title="Drafts (Total: 1, Unread: 0)"]', 10)
  I.selectFolder('Drafts')
  I.waitForElement('.list-view .date:not(.scheduled)') // right date?

  // edit and reschedule mail
  await within({ frame: '.io-ox-mail-compose-window .editor iframe' }, async () => {
    I.appendField('body', ' EDITED')
  })
  I.click('~Schedule email')
  I.clickDropdown('Send Monday morning')
  I.waitForElement('[title="Scheduled (Total: 1, Unread: 0)"]', 10)
  I.waitForElement('[title="Drafts (Total: 0, Unread: 0)"]', 10)
  I.selectFolder('Scheduled')
  await mail.selectMail('Scheduled mail roundtrip test')

  // Cancel send
  I.click('Cancel send')
  I.waitForText('Sending has been canceled. The email has been moved to the drafts folder.')
  I.waitForElement('[title="Scheduled (Total: 0, Unread: 0)"]', 10)
  I.waitForElement('[title="Drafts (Total: 1, Unread: 0)"]', 10)
  I.selectFolder('Drafts')
  I.waitForElement('.list-view .date:not(.scheduled)') // right date?
})
