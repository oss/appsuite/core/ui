/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Undo actions')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('Archive single mail', async ({ I, users, mail }) => {
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login()

  await mail.selectMail('Plain text')
  I.waitForVisible('~Archive')
  I.click('~Archive', '.classic-toolbar-container')

  I.dontSee('Plain text', '.list-view')
  I.waitForText("Email 'Plain text' archived", 5, '.io-ox-toast')
  I.waitForElement(locate('[aria-label="Archive"] .folder-arrow').inside('.folder-tree'))

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitForText('Plain text', 5, '.list-view')
})

Scenario('Archive multiple mails at once', async ({ I, users, mail }) => {
  await Promise.all([
    I.haveMail({ path: 'media/mails/sequence/plain_text_1.eml' }),
    I.haveMail({ path: 'media/mails/sequence/plain_text_2.eml' })
  ])
  await I.login()

  await mail.selectMail('Number one of a sequence')
  I.pressKey(['Shift', 'ArrowUp'])
  I.waitForVisible('~Archive')
  I.click('~Archive', '.classic-toolbar-container')

  I.dontSee('Number one of a sequence', '.list-view')
  I.dontSee('Number two of a sequence', '.list-view')
  I.waitForText('2 emails archived', 5, '.io-ox-toast')
  I.waitForElement(locate('[aria-label="Archive"] .folder-arrow').inside('.folder-tree'))

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitForText('Number one of a sequence', 5, '.list-view')
  I.waitForText('Number two of a sequence', 5, '.list-view')
})

Scenario('Delete single mail', async ({ I, users, mail }) => {
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login()

  await mail.selectMail('Plain text')
  I.waitForVisible('~Delete')
  I.click('~Delete', '.classic-toolbar-container')

  I.dontSee('Plain text', '.list-view')
  I.waitForText("Email 'Plain text' deleted", 5, '.io-ox-toast')

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitForText('Plain text', 5, '.list-view')
})

Scenario('Delete multiple mails at once', async ({ I, users, mail }) => {
  await Promise.all([
    I.haveMail({ path: 'media/mails/sequence/plain_text_1.eml' }),
    I.haveMail({ path: 'media/mails/sequence/plain_text_2.eml' })
  ])
  await I.login()

  await mail.selectMail('Number one of a sequence')
  I.pressKey(['Shift', 'ArrowUp'])
  I.waitForVisible('~Delete')
  I.click('~Delete', '.classic-toolbar-container')
  I.waitForDetached('.list-view .list-item')
  I.dontSee('Number one of a sequence', '.list-view')
  I.dontSee('Number two of a sequence', '.list-view')

  I.waitForText('2 emails deleted', 5, '.io-ox-toast')
  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')

  I.waitForText('Number one of a sequence', 5, '.list-view')
  I.waitForText('Number two of a sequence', 5, '.list-view')
})

Scenario('Delete multiple mails one after the other', async ({ I, users, mail }) => {
  await Promise.all([
    I.haveMail({ path: 'media/mails/sequence/plain_text_1.eml' }),
    I.haveMail({ path: 'media/mails/sequence/plain_text_2.eml' }),
    I.haveMail({ path: 'media/mails/sequence/plain_text_3.eml' })
  ])

  await I.login()

  await mail.selectMail('Number one of a sequence')
  I.waitForVisible('~Delete')
  I.click('~Delete', '.classic-toolbar-container')
  I.waitForVisible('~Delete')
  I.dontSee('Number one of a sequence', '.list-view')
  I.see('Number two of a sequence', '.list-view')
  I.see('Number three of a sequence', '.list-view')
  I.waitForText("Email 'Number one of a sequ…' deleted", 5, '.io-ox-toast')

  I.waitForVisible('~Delete')
  I.click('~Delete', '.classic-toolbar-container')
  I.waitForVisible('~Delete')
  I.dontSee('Number one of a sequence', '.list-view')
  I.dontSee('Number two of a sequence', '.list-view')
  I.see('Number three of a sequence', '.list-view')
  I.waitForText("Email 'Number two of a sequ…' deleted", 5, '.io-ox-toast')
  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')

  I.waitForText('Number two of a sequence', 5, '.list-view')
  I.dontSee('Number one of a sequence', '.list-view')
  I.see('Number three of a sequence', '.list-view')
})

Scenario('Move single mail', async ({ I, users, mail, dialogs }) => {
  const folderName = 'Spam'
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login()

  await mail.selectMail('Plain text')
  I.waitForVisible('~Move')
  I.click('~Move', '.classic-toolbar-container')
  dialogs.waitForVisible()

  I.click(`.folder-picker-dialog .folder[aria-label="${folderName}"]`)
  I.waitForElement(`.folder-picker-dialog .folder[aria-label="${folderName}"].selected`)
  dialogs.clickButton('Move')
  I.waitForDetached('.modal-dialog')
  I.waitForText(folderName, 5, '.folder-tree')

  I.dontSee('Plain text')
  I.waitForText(`Email moved to folder '${folderName}'`, 5, '.io-ox-toast')

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitForText('Plain text', 5, '.list-view')
})

Scenario('Move multiple mails at once', async ({ I, users, mail, dialogs }) => {
  const folderName = 'Spam'
  await Promise.all([
    I.haveMail({ path: 'media/mails/sequence/plain_text_1.eml' }),
    I.haveMail({ path: 'media/mails/sequence/plain_text_2.eml' })
  ])
  await I.login()

  await mail.selectMail('Number one of a sequence')
  I.pressKey(['Shift', 'ArrowUp'])
  I.waitForVisible('~Move')
  I.click('~Move', '.classic-toolbar-container')
  dialogs.waitForVisible()

  I.click(`.folder-picker-dialog .folder[aria-label="${folderName}"]`)
  I.waitForElement(`.folder-picker-dialog .folder[aria-label="${folderName}"].selected`)
  dialogs.clickButton('Move')
  I.waitForDetached('.modal-dialog')
  I.waitForText(folderName, 5, '.folder-tree')

  I.dontSee('Number one of a sequence', '.list-view')
  I.dontSee('Number two of a sequence', '.list-view')
  I.waitForText(`2 emails moved to folder '${folderName}'`, 5, '.io-ox-toast')

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitForText('Number one of a sequence', 5, '.list-view')
  I.waitForText('Number two of a sequence', 5, '.list-view')
})

Scenario('Copy single mail', async ({ I, users, mail, dialogs }) => {
  const folderName = 'Inbox'
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login()

  await mail.selectMail('Plain text')

  I.waitForVisible('~More actions', 5)
  I.click('~More actions', '.mail-header-actions')
  I.clickDropdown('Copy')
  I.waitForElement('.folder-picker-dialog')

  dialogs.clickButton('Copy')
  I.waitForDetached('.modal-dialog')

  I.waitForText(`Email copied to folder '${folderName}'`, 5, '.io-ox-toast')
  I.waitNumberOfVisibleElements('.mail-item .list-item', 2)

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitNumberOfVisibleElements('.mail-item .list-item', 1)
})

Scenario('Copy multiple mails at once', async ({ I, users, mail, dialogs }) => {
  const folderName = 'Inbox'
  await Promise.all([
    I.haveMail({ path: 'media/mails/sequence/plain_text_1.eml' }),
    I.haveMail({ path: 'media/mails/sequence/plain_text_2.eml' })
  ])
  await I.login()

  await mail.selectMail('Number one of a sequence')
  I.pressKey(['Shift', 'ArrowUp'])
  I.waitForVisible('~More actions', 5)
  I.click('~More actions', '.mail-detail-pane .classic-toolbar ')
  I.clickDropdown('Copy')
  I.waitForElement('.folder-picker-dialog')

  dialogs.clickButton('Copy')
  I.waitForDetached('.modal-dialog')

  I.waitForText(`2 emails copied to folder '${folderName}'`, 5, '.io-ox-toast')
  I.waitNumberOfVisibleElements('.mail-item .list-item', 4)

  I.click('Undo', '.io-ox-toast')
  I.waitForInvisible('.io-ox-toast')
  I.waitNumberOfVisibleElements('.mail-item .list-item', 2)
})
