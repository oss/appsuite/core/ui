/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Search')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C8405] Find mails based on a date range', async ({ I, users }) => {
  // Precondition: Have some emails in the Inbox with timestamps from 2016 and 2017

  await Promise.all([
    I.haveMail({ path: 'media/mails/c8405_2016.eml' }),
    I.haveMail({ path: 'media/mails/c8405_2017.eml' })
  ])
  // Switch to Mail
  await I.login('app=io.ox/mail')
  I.waitForVisible('#io-ox-topsearch .search-field')
  I.click('#io-ox-topsearch .search-field')

  // Start typing a date rage (e.g. "01.01.2016 - 31.12.2016") into the search field
  I.fillField('#io-ox-topsearch .search-field', 'after:01.01.2016 before:31.12.2016')
  I.pressKey('Enter')
  // results
  I.waitForText('2016', undefined, '.list-view')
  I.dontSee('2017', '.list-view')

  // cancel search
  I.click('~Cancel search')
  I.pressKey('Enter')
  // results
  I.waitForText('2016', undefined, '.list-view')
  I.waitForText('2017', undefined, '.list-view')

  // second round
  I.fillField('#io-ox-topsearch .search-field', 'after:01.01.2017 before:31.12.2017')
  I.pressKey('Enter')
  I.waitForText('2017', undefined, '.list-view')
  I.dontSee('2016', '.list-view')

  // cancel search
  I.click('~Cancel search')
  I.pressKey('Enter')

  // Wrong order of the date range [OXUIB-2615]
  I.fillField('#io-ox-topsearch .search-field', 'before:01.01.2016 after:31.12.2016')
  I.pressKey('Enter')

  I.waitForElement('span[title="After 1/1/2016"]')
  I.waitForElement('span[title="Before 12/31/2016"]')

  // results
  I.waitForText('2016', undefined, '.list-view')
  I.dontSee('2017', '.list-view')
})

Scenario('[C8402] Search in different folders', async ({ I, users, mail }) => {
  // Precondition: Some emails are in the inbox- and in a subfolder and have the subject "test".
  const subFolder = await I.haveFolder({ title: 'Subfolder', module: 'mail', parent: 'default0/INBOX' })

  await Promise.all([
    I.haveMail({ path: 'media/mails/types/text_plain.eml' }),
    I.haveMail({ path: 'media/mails/types/text_plain.eml', folder: subFolder })
  ])

  await I.login('app=io.ox/mail')

  // Enter "test" in the inputfield, than hit enter.
  I.waitForVisible('#io-ox-topsearch .search-field')
  I.fillField('#io-ox-topsearch .search-field', 'plain')
  // UI will perform a reload if we do not wait here ...
  I.pressKey('Enter')
  I.waitForText('Plain text', undefined, '.list-view')
  I.waitForText('Inbox', undefined, '.list-view')
  I.waitForText('Plain text', undefined, '.list-view')
  I.waitForText('Subfolder', undefined, '.list-view')

  // Change the search folder to the subfolder
  I.click('#io-ox-topsearch .dropdown-toggle')
  I.waitForVisible('.search-view .dropdown')
  I.selectOption('Search in', 'Current folder')

  I.click('Search')
  // Checking result
  I.waitForText('Plain text', undefined, '.list-view')
  I.waitForText('Inbox', undefined, '.list-view')
  I.dontSee('Subfolder', '.list-view')
})

Scenario('[C8404] Find mails based on from/to', async ({ I, users }) => {
  await Promise.all([
    I.haveMail({ path: 'media/mails/sender-receiver/from-flamingo-to-lemur.eml' }),
    I.haveMail({ path: 'media/mails/sender-receiver/from-lemur-to-flamingo.eml' })
  ])

  // Start a new search in mail
  await I.login('app=io.ox/mail')
  I.waitForText('Flamingo to Lemur')
  I.waitForText('Lemur to Flamingo')

  I.waitForVisible('#io-ox-topsearch .search-field')
  // Click into the input field.
  I.click('.search-field')

  // Start typing some user name
  I.fillField('#io-ox-topsearch .search-field', 'from:funky.flamingo@zoo.ox.io')
  I.pressKey('Enter')
  // result
  I.waitForText('Funky Flamingo', undefined, '.list-view')
  I.waitForText('Flamingo to Lemur', undefined, '.list-view')
  I.dontSee('Loopy Lemur', '.list-view')
  I.dontSee('Lemur to Flamingo', '.list-view')

  // second round
  I.click('~Cancel search')
  I.fillField('#io-ox-topsearch .search-field', 'to:funky.flamingo@zoo.ox.io')
  I.pressKey('Enter')
  // result
  I.waitForText('Loopy Lemur', 5, '.list-view')
  I.waitForText('Lemur to Flamingo', 5, '.list-view')
  I.dontSee('Funky Flamingo', '.list-view')
  I.dontSee('Flamingo to Lemur', '.list-view')

  // third round
  I.click('~Cancel search')
  I.fillField('#io-ox-topsearch .search-field', 'funky.flamingo@zoo.ox.io')
  I.pressKey('Enter')
  // result
  I.waitForText('Funky Flamingo', undefined, '.list-view')
  I.waitForText('Flamingo to Lemur', undefined, '.list-view')
  I.waitForText('Loopy Lemur', 5, '.list-view')
  I.waitForText('Lemur to Flamingo', 5, '.list-view')
})

Scenario('[OXUIB-1800] Find mails via autocomplete in search', async ({ I }) => {
  await Promise.all([
    I.haveMail({ path: 'media/mails/types/text_plain.eml' }),
    I.haveContact({
      email1: 'funky.flamingo@zoo.ox.io',
      folder_id: await I.grabDefaultFolder('contacts'),
      first_name: 'Funky',
      last_name: 'Flamingo'
    })
  ])

  await I.login('app=io.ox/mail')
  I.waitForVisible('#io-ox-topsearch .search-field')
  I.click('.search-field')

  // Click
  I.fillField('#io-ox-topsearch .search-field', 'from:funky')
  I.waitForText('funky.flamingo@zoo.ox.io', undefined, '.email')
  I.click('.list-item[data-cid]')
  I.seeNumberOfElements('.filter', 1)
  I.dontSee('From/To funky.flamingo@zoo.ox.io', '.filter')
  I.seeTextEquals('From funky.flamingo@zoo.ox.io', '.filter')
  I.click('~Cancel search')

  // Click: arrow down
  I.fillField('#io-ox-topsearch .search-field', 'from:funky')
  I.waitForText('funky.flamingo@zoo.ox.io', undefined, '.email')
  I.pressKey('ArrowDown')
  I.click('.list-item[data-cid]')
  I.seeNumberOfElements('.filter', 1)
  I.dontSee('From/To funky.flamingo@zoo.ox.io', '.filter')
  I.seeTextEquals('From funky.flamingo@zoo.ox.io', '.filter')
  I.click('~Cancel search')

  // Enter: arrow down
  I.fillField('#io-ox-topsearch .search-field', 'from:funky')
  I.waitForText('funky.flamingo@zoo.ox.io', undefined, '.email')
  I.pressKey('ArrowDown')
  I.pressKey('Enter')
  I.seeNumberOfElements('.filter', 1)
  I.dontSee('From/To funky.flamingo@zoo.ox.io', '.filter')
  I.seeTextEquals('From funky.flamingo@zoo.ox.io', '.filter')
  I.click('~Cancel search')

  // Enter: tab
  I.fillField('#io-ox-topsearch .search-field', 'from:funky')
  I.waitForText('funky.flamingo@zoo.ox.io', undefined, '.email')
  I.pressKey('Tab')
  I.pressKey('Enter')
  I.seeNumberOfElements('.filter', 1)
  I.dontSee('From/To funky.flamingo@zoo.ox.io', '.filter')
  I.seeTextEquals('From funky.flamingo@zoo.ox.io', '.filter')
  I.click('~Cancel search')

  // Enter: arrow down, tab
  I.fillField('#io-ox-topsearch .search-field', 'from:funky')
  I.waitForText('funky.flamingo@zoo.ox.io', undefined, '.email')
  I.pressKey('ArrowDown')
  I.pressKey('Tab')
  I.pressKey('Enter')
  I.seeNumberOfElements('.filter', 1)
  I.dontSee('From/To funky.flamingo@zoo.ox.io', '.filter')
  I.seeTextEquals('From funky.flamingo@zoo.ox.io', '.filter')
  I.click('~Cancel search')

  I.waitForVisible('~More search options')
  I.click('~More search options')
  I.waitForVisible('.dropdown input[name="from"]')
  I.fillField('.dropdown input[name="from"]', 'funky')
  I.waitForText('funky.flamingo@zoo.ox.io', undefined, '.email')
  I.click('.list-item[data-cid]')
  I.pressKey('Enter')
  I.waitForInvisible('.dropdown input[name="from"]')
  I.seeNumberOfElements('.filter', 1)
  I.dontSee('From/To funky.flamingo@zoo.ox.io', '.filter')
  I.seeTextEquals('From funky.flamingo@zoo.ox.io', '.filter')
})

Scenario('[C8408] Try to run a script in search', async ({ I, mail, search }) => {
  await I.login()

  search.doSearch('<script>document.body.innerHTML=\'I am a hacker\'</script>')
  I.dontSee('I am a hacker')
})

Scenario('Mail search has no further options except "Select All"', async ({ I, search, mail }) => {
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login('app=io.ox/mail')

  I.waitForElement('~More message options')
  I.click('~More message options')
  I.see('Select all messages')
  I.see('Delete all messages')
  I.see('Sort by')
  I.click('.smart-dropdown-container')
  search.doSearch('Plain text')
  await mail.selectMail('Plain text')
  I.click('~More message options')
  I.see('Select all messages')
  I.dontSee('Delete all messages')
  I.dontSee('Sort by')
  I.click('.smart-dropdown-container')
  search.cancel()
  I.click('~More message options')
  I.see('Select all messages')
  I.see('Select all messages')
  I.see('Delete all messages')
  I.see('Sort by')
})

Scenario('[OXUIB-1990] search with quotes that should give results', async ({ I, search, mail }) => {
  await I.haveMail({ path: 'media/mails/OXUIB-1990.eml' })

  await I.login('app=io.ox/mail')

  I.fillField('#io-ox-topsearch .search-field', '"the hecklers pelted the discombobulated speaker with anything that came to hand"')
  I.pressKey('Enter')
  I.waitForText('Gedoehns')

  I.fillField('#io-ox-topsearch .search-field', '"the hecklers pelted"')
  I.pressKey('Enter')
  I.waitForText('Gedoehns')

  I.fillField('#io-ox-topsearch .search-field', '"the hecklers"')
  I.pressKey('Enter')
  I.waitForText('Gedoehns')

  I.fillField('#io-ox-topsearch .search-field', '"hecklers pelted"')
  I.pressKey('Enter')
  I.waitForText('Gedoehns')
})

Scenario('[OXUIB-1990] search with quotes that should not give results', async ({ I, search, mail }) => {
  await I.haveMail({ path: 'media/mails/OXUIB-1990.eml' })

  await I.login('app=io.ox/mail')

  I.fillField('#io-ox-topsearch .search-field', '"hecklers pelted discombobulated speaker anything came hand"')
  I.pressKey('Enter')
  I.waitForText('Search results')
  I.dontSee('Gedoehns')
})

Scenario('Search by translated "to:" keyword', async ({ I, search, mail }) => {
  await Promise.all([
    I.haveSetting('io.ox/core//language', 'es_ES'),
    I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  ])
  await I.login('app=io.ox/mail')
  I.waitForVisible('.search-field')
  I.fillField('.search-field', 'para:' + 'loopy.lemur@zoo.ox.io')
  I.pressKey('Enter')
  I.waitForVisible('.filter')
  I.see('Para ' + 'loopy.lemur@zoo.ox.io')
  I.waitForText('Funky Flamingo')
})

Scenario('Search by "to:" keyword for non-english users', async ({ I, search, mail }) => {
  await Promise.all([
    I.haveSetting('io.ox/core//language', 'es_ES'),
    I.haveMail({ path: 'media/mails/sender-receiver/from-flamingo-to-lemur.eml' })
  ])
  await I.login('app=io.ox/mail')
  I.waitForVisible('.search-field')
  I.fillField('.search-field', 'to:' + 'loopy.lemur@zoo.ox.io')
  I.pressKey('Enter')
  I.waitForVisible('.filter')
  I.see('Para ' + 'loopy.lemur@zoo.ox.io')
  I.waitForText('Funky Flamingo')
})

Scenario('[OXUIB-1991] search for mail address in mail body', async ({ I, mail, users }) => {
  await I.haveMail({ from: users[0], to: users[0], subject: 'testmail', content: 'test@test.io' })

  await I.login()

  I.fillField('#io-ox-topsearch .search-field', 'test@test.io')
  I.pressKey('Enter')

  I.waitForText('Search results')
  I.waitForText('testmail', undefined, '.list-view.mail-item')
  I.seeNumberOfVisibleElements('.list-view.mail-item .list-item', 2)
})
