/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mail > Folderview')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[OXUIB-64] Mark folders as favorites', async ({ I, mail }) => {
  await I.haveSetting('io.ox/core//favorites/mail', ['default0'])
  await I.login('app=io.ox/mail')

  I.waitForVisible('.folder.virtual.favorites .folder-arrow')
  I.click('.folder.virtual.favorites .folder-arrow')
  I.rightClick('.folder.virtual.favorites .folder[data-id="default0"] .folder-node')
  I.click('Remove from favorites')
  I.dontSee('Favorites')
})

Scenario('[OXUIB-1574] Do not display statistics for virtual folders', async ({ I, mail, dialogs }) => {
  await I.login('app=io.ox/mail')

  I.click('li[data-model="virtual/myfolders"]')
  I.click('button[aria-label="Settings"]')
  I.waitForElement('#topbar-settings-dropdown')
  I.dontSee('a[data-name="statistics"]')
})

Scenario('Enabled fullMailAddress feature displays the full email address', async ({ I, users }) => {
  // disabled (default)
  await I.login('app=io.ox/mail')
  const [localPart, domainPart] = String(users[0].get('primaryEmail')).split('@')
  I.waitForElement('[data-id="virtual/standard"] > .folder-node > .folder-label')
  I.see(domainPart, '[data-id="virtual/standard"] > .folder-node > .folder-label')
  I.dontSee(`${localPart}@${domainPart}`, '[data-id="virtual/standard"] > .folder-node > .folder-label')
  await I.logout()

  // enabled
  await I.haveSetting('io.ox/mail//features/fullMailaddress', true)
  await I.login('app=io.ox/mail')
  I.waitForElement('[data-id="virtual/standard"] > .folder-node > .folder-label')
  I.see(`${localPart}@${domainPart}`, '[data-id="virtual/standard"] > .folder-node > .folder-label')
})
