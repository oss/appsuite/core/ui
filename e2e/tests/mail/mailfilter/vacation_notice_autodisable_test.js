/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const moment = require('moment')

Feature('Mailfilter > Vacation notice autodisable')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('Auto-disable for past timeframe (mail)', async ({ I, users }) => {
  const lastWeek = moment().utc().subtract(8, 'day').startOf('day').valueOf()
  const today = moment().utc().startOf('day').valueOf()
  const yesterday = moment().utc().subtract(1, 'day').startOf('day').valueOf()

  const [userA, userB] = users

  // should remain active (still within the timeframe)
  await I.haveMailFilterRule({
    rulename: 'vacation notice',
    active: true,
    flags: ['vacation'],
    test: {
      id: 'allof',
      tests: [
        { id: 'currentdate', comparison: 'ge', datepart: 'date', datevalue: [lastWeek], zone: '+0200' },
        { id: 'currentdate', comparison: 'le', datepart: 'date', datevalue: [today], zone: '+0200' }
      ]
    },
    actioncmds: [{
      days: '7', subject: 'Test Subject', text: 'Test Text', id: 'vacation', addresses: [userA.get('primaryEmail')]
    }]
  }, { user: userA })
  await I.login('app=io.ox/mail', { user: userA })
  I.click('~Settings')
  I.waitForVisible({ css: 'a[data-name="vacation-notice"]' })
  I.clickDropdown('Vacation notice …')
  I.waitForText('Send vacation notice during this time only')
  I.waitForVisible('.modal-body')
  I.dontSeeElement('.modal-body.disabled')
  await I.logout()

  // should be set to inactive (outside of the timeframe)
  await I.haveMailFilterRule({
    rulename: 'vacation notice',
    active: true,
    flags: ['vacation'],
    test: {
      id: 'allof',
      tests: [
        { id: 'currentdate', comparison: 'ge', datepart: 'date', datevalue: [lastWeek], zone: '+0200' },
        { id: 'currentdate', comparison: 'le', datepart: 'date', datevalue: [yesterday], zone: '+0200' }
      ]
    },
    actioncmds: [{
      days: '7', subject: 'Test Subject', text: 'Test Text', id: 'vacation', addresses: [userB.get('primaryEmail')]
    }]
  }, { user: userB })
  await I.login('app=io.ox/mail', { user: userB })
  I.click('~Settings')
  I.waitForVisible({ css: 'a[data-name="vacation-notice"]' })
  I.clickDropdown('Vacation notice …')
  I.waitForText('Send vacation notice during this time only')
  I.waitForVisible('.modal-body.disabled')
})

Scenario('Auto-disable for past timeframe (settings)', async ({ I, users }) => {
  const lastWeek = moment().utc().subtract(8, 'day').startOf('day').valueOf()
  const today = moment().utc().startOf('day').valueOf()
  const yesterday = moment().utc().subtract(1, 'day').startOf('day').valueOf()

  const [userA, userB] = users

  // should remain active (still within the timeframe)
  await I.haveMailFilterRule({
    rulename: 'vacation notice',
    active: true,
    flags: ['vacation'],
    test: {
      id: 'allof',
      tests: [
        { id: 'currentdate', comparison: 'ge', datepart: 'date', datevalue: [lastWeek], zone: '+0200' },
        { id: 'currentdate', comparison: 'le', datepart: 'date', datevalue: [today], zone: '+0200' }
      ]
    },
    actioncmds: [{
      days: '7', subject: 'Test Subject', text: 'Test Text', id: 'vacation', addresses: [userA.get('primaryEmail')]
    }]
  }, { user: userA })
  await I.login('settings=virtual/settings/io.ox/mail&section=io.ox/mail/settings/rules', { user: userA })

  I.waitForText('Vacation notice')
  I.waitForVisible('[data-action="edit-vacation-notice"] svg.mini-toggle')
  I.waitForText('vacation notice', 5, '.settings-list-item')
  I.dontSee('vacation notice (Disabled)', '.settings-list-item')
  I.click('Edit')
  I.waitForText('Send vacation notice during this time only')
  I.waitForVisible('.modal-body')
  I.dontSeeElement('.modal-body.disabled')
  await I.logout()

  // should be set to inactive (outside of the timeframe)
  await I.haveMailFilterRule({
    rulename: 'vacation notice',
    active: true,
    flags: ['vacation'],
    test: {
      id: 'allof',
      tests: [
        { id: 'currentdate', comparison: 'ge', datepart: 'date', datevalue: [lastWeek], zone: '+0200' },
        { id: 'currentdate', comparison: 'le', datepart: 'date', datevalue: [yesterday], zone: '+0200' }
      ]
    },
    actioncmds: [{
      days: '7', subject: 'Test Subject', text: 'Test Text', id: 'vacation', addresses: [userB.get('primaryEmail')]
    }]
  }, { user: userB })
  await I.login('settings=virtual/settings/io.ox/mail&section=io.ox/mail/settings/rules', { user: userB })

  I.waitForText('Vacation notice')
  I.dontSeeElement('[data-action="edit-vacation-notice"] svg.mini-toggle')
  I.waitForText('vacation notice (Disabled)', 5, '.settings-list-item')
  I.click('Edit')
  I.waitForText('Send vacation notice during this time only')
  I.waitForVisible('.modal-body.disabled')
})
