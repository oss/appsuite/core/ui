/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Mailfilter > Autoforward')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('Single user roundtrip with add and remove', async ({ I, dialogs, settings }) => {
  await I.login('settings=virtual/settings/io.ox/mail&section=io.ox/mail/settings/rules')

  I.waitForElement('[data-action="edit-auto-forward"]')
  I.click('Auto forward …', '[data-action="edit-auto-forward"]')

  // check for all expected elements
  dialogs.waitForVisible()
  I.waitForElement('.modal-header input[name="active"]')

  // buttons
  I.see('Cancel', dialogs.footer)
  I.see('Apply changes', dialogs.footer)

  await within(dialogs.body, () => {
    // form elements
    I.seeElement({ css: 'input[name="to"][disabled]' })
    I.seeElement({ css: 'input[name="copy"][disabled]' })
    I.seeElement({ css: 'input[name="processSub"][disabled]' })

    // enable
    I.waitForVisible('.checkbox.switch.large')
    I.click('.checkbox.switch.large')
    I.seeElement({ css: 'input[name="to"]:not([disabled])' })
    I.seeElement({ css: 'input[name="copy"]:not([disabled])' })
    I.seeElement({ css: 'input[name="processSub"]:not([disabled])' })
  })
  // button disabled?
  I.seeElement('.modal-footer [data-action="save"][disabled]')
  I.fillField({ css: '.modal-body input[name="to"]' }, 'test@oxtest.com')

  // button enabled?
  dialogs.clickButton('Apply changes')
  I.waitForDetached('.modal:not(.io-ox-settings-main)')
  I.see('Auto forward …', '[data-action="edit-auto-forward"]')
  I.waitForElement('[data-action="edit-auto-forward"] > svg')
  I.waitForInvisible('[data-point="io.ox/mail/auto-forward/edit"]')
  settings.close()

  // check notification in mail
  I.waitForText('Auto forwarding is active', undefined, '[data-action="edit-auto-forward-notice"]')
  I.click('Auto forwarding is active', '[data-action="edit-auto-forward-notice"]')
  dialogs.waitForVisible()
  await within(dialogs.body, () => {
    I.seeInField({ css: 'input[name="to"]' }, 'test@oxtest.com')
    I.seeElement({ css: 'input[name="processSub"]:checked' })
  })

  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
})

Scenario('[C7786] Multi user roundtrip', async ({ I, users, mail, dialogs, settings }) => {
  await I.haveSetting({ 'io.ox/mail': { messageFormat: 'text' } })

  await I.login('app=io.ox/mail')

  await settings.open('Mail', 'Rules')
  I.waitForText('Auto forward …')
  I.click('Auto forward …', '[data-action="edit-auto-forward"]')
  dialogs.waitForVisible()

  // check for all expected elements
  I.waitForElement('.modal-header input[name="active"]')

  // buttons
  I.see('Cancel', dialogs.footer)
  I.see('Apply changes', dialogs.footer)

  // form elements
  I.waitForElement({ css: 'input[name="to"][disabled]' })
  I.waitForElement({ css: 'input[name="copy"][disabled]' })
  I.waitForElement({ css: 'input[name="processSub"][disabled]' })

  // enable
  I.waitForVisible('.checkbox.switch.large')
  I.click('.checkbox.switch.large')
  I.waitForElement({ css: 'input[name="to"]:not([disabled])' })
  I.waitForElement({ css: 'input[name="copy"]:not([disabled])' })
  I.waitForElement({ css: 'input[name="processSub"]:not([disabled])' })

  // button disabled?
  I.waitForElement('.modal-footer [data-action="save"][disabled]')
  I.fillField({ css: '.modal-body input[name="to"]' }, users[1].get('primaryEmail'))

  // button enabled?
  dialogs.clickButton('Apply changes')

  I.see('Auto forward …', '[data-action="edit-auto-forward"]')
  I.waitForElement('[data-action="edit-auto-forward"] > svg')
  I.waitForInvisible('[data-point="io.ox/mail/auto-forward/edit"]')

  settings.close()

  // compose mail for user 0
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Test subject')
  I.fillField(mail.editorText, 'Test text')
  I.seeInField(mail.editorText, 'Test text')

  mail.send()
  I.waitForDetached('.io-ox-mail-compose')

  await I.logout()

  await I.login('app=io.ox/mail', { user: users[1] })

  // check for mail
  I.waitForVisible('.io-ox-mail-window .leftside ul li.unread')
  I.click('.io-ox-mail-window .leftside ul li.unread')
  I.waitForVisible('.io-ox-mail-window .mail-detail-pane .subject')
  I.see('Test subject', '.mail-detail-pane')
})

Scenario('[OXUIB-1783] Reset', async ({ I, users, dialogs, settings }) => {
  await I.haveSetting({ 'io.ox/mail': { messageFormat: 'text' } })

  await I.login('app=io.ox/mail')

  await settings.open('Mail', 'Rules')
  I.waitForVisible('[data-action="edit-auto-forward"]')
  I.click('Auto forward …', '[data-action="edit-auto-forward"]')
  dialogs.waitForVisible()

  // check for all expected elements
  I.waitForElement('.modal-header input[name="active"]')

  // buttons
  I.waitForText('Cancel', undefined, dialogs.footer)
  I.waitForText('Apply changes', undefined, dialogs.footer)

  await within(dialogs.body, () => {
    // form elements
    I.seeElement({ css: 'input[name="to"][disabled]' })
    I.seeElement({ css: 'input[name="copy"][disabled]' })
    I.seeElement({ css: 'input[name="processSub"][disabled]' })

    // enable
    I.waitForVisible('.checkbox.switch.large')
    I.click('.checkbox.switch.large')
    I.waitForElement({ css: 'input[name="to"]:not([disabled])' })
    I.waitForElement({ css: 'input[name="copy"]:not([disabled])' })
    I.waitForElement({ css: 'input[name="processSub"]:not([disabled])' })
  })

  // button disabled?
  I.waitForElement('.modal-footer [data-action="save"][disabled]')
  I.fillField({ css: '.modal-body input[name="to"]' }, users[1].get('primaryEmail'))

  // button enabled?
  dialogs.clickButton('Apply changes')
  I.waitForDetached('.modal.rule-dialog')
  I.waitForText('Auto forward …', undefined, '[data-action="edit-auto-forward"]')
  I.waitForElement('[data-action="edit-auto-forward"] > svg')
  I.waitForInvisible('[data-point="io.ox/mail/auto-forward/edit"]')
  I.click('Auto forward …', '[data-action="edit-auto-forward"]')
  dialogs.waitForVisible()

  // check for reset button
  I.waitForText('Reset', undefined, dialogs.footer)
  dialogs.clickButton('Reset')
  dialogs.clickButton('Apply changes')
  I.waitForDetached('.modal.rule-dialog')

  I.waitForText('Auto forward …', undefined, '[data-action="edit-auto-forward"]')
  await I.waitForApp()

  I.click('Auto forward …', '[data-action="edit-auto-forward"]')
  dialogs.waitForVisible()
  // check for all expected elements
  I.waitForElement('.modal-header input[name="active"]')

  // buttons
  I.waitForText('Cancel', undefined, dialogs.footer)
  I.waitForText('Apply changes', undefined, dialogs.footer)

  // form elements
  I.seeElement({ css: 'input[name="to"][disabled]' })
  I.seeElement({ css: 'input[name="copy"][disabled]' })
  I.seeElement({ css: 'input[name="processSub"][disabled]' })
})

Scenario('Rule is correctly listed after a status update', async ({ I, dialogs }) => {
  await I.haveMailFilterRule({
    rulename: 'autoforward',
    actioncmds: [
      { id: 'redirect', to: 'test@tester.com', copy: true }
    ],
    active: true,
    flags: ['autoforward'],
    test: { id: 'true' }
  })

  await I.login('settings=virtual/settings/io.ox/mail&section=io.ox/mail/settings/rules')

  I.waitForElement('[data-action="edit-auto-forward"] > svg')
  I.click('Auto forward …', '.io-ox-settings-main')

  dialogs.waitForVisible()
  I.waitForVisible('.checkbox.switch.large')
  I.click('.checkbox.switch.large')
  dialogs.clickButton('Apply changes')
  I.waitForDetached('.modal:not(.io-ox-settings-main)')

  I.waitForElement('.io-ox-mailfilter-settings .settings-list-item.disabled')
})
