/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Portal')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Create new appointment and check display in portal widget', async ({ I, calendar, dialogs }) => {
  // make sure portal widget is empty
  await I.login('app=io.ox/portal')

  I.waitForText('You don\'t have any appointments in the near future.', 10, '[data-widget-id="calendar_0"] li.line')

  I.openApp('Calendar')
  await I.waitForApp()
  calendar.moveCalendarViewToNextWeek()
  I.dontSeeElement('.appointment')

  I.selectFolder('Calendar')
  await I.waitForApp()
  await calendar.newAppointment()
  await within(calendar.editWindow, async () => {
    I.fillField('Title', 'test portal widget')
    I.fillField('Location', 'portal widget location')
    calendar.startNextMonday()
    I.click('Create')
  })
  I.waitForDetached('.io-ox-calendar-edit-window')

  // check in week view'
  I.waitForText('test portal widget', undefined, '.weekview-container')

  // check in portal
  I.openApp('Portal')
  await I.waitForApp()
  I.waitForElement('[data-widget-id="calendar_0"]')
  I.waitForText('test portal widget', undefined, '[data-widget-id="calendar_0"]')
})
