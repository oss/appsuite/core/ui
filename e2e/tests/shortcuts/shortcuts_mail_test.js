/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Shortcuts > Mail')

Before(async ({ users }) => {
  await Promise.all([
    users.create(),
    users.create(),
    users.create()
  ])
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Open compose mail dialog', async ({ I, users, settings, mail }) => {
  I.haveSetting({
    'io.ox/core': { features: { shortcuts: true } }
  })
  await I.login('app=io.ox/mail')

  await settings.open('General', 'Keyboard shortcuts')
  I.waitForText('Outlook.com')
  I.checkOption('Outlook.com')
  settings.close()

  I.pressKey('n')
  I.click('~Close', mail.composeWindow)
  I.waitForDetached(mail.composeWindow)

  await settings.open('General', 'Keyboard shortcuts')
  I.waitForText('Gmail')
  I.checkOption('Gmail')
  settings.close()

  I.pressKey('c')
  I.waitForVisible(mail.composeWindow)
})

Scenario('Shortcut to archive mail', async ({ I, mail, users }) => {
  const subjects = ['Click Archive me', 'Key Archive me']
  await I.haveSetting({
    'io.ox/core': { features: { shortcuts: true } }
  })

  await Promise.all([
    I.haveMail({
      from: users[0],
      subject: subjects[0],
      to: users[0]
    }),
    I.haveMail({
      from: users[0],
      subject: subjects[1],
      to: users[0]
    })
  ])

  await I.login('app=io.ox/mail')

  I.see(subjects[0])
  await mail.selectMail(subjects[0])
  I.click('~Archive')
  I.dontSee(subjects[0], '.io-ox-mail-window .list-view.mail-item')

  I.see(subjects[1])
  await mail.selectMail(subjects[1])
  I.pressKey('a')
  I.waitForDetached(subjects[1])
  I.dontSee(subjects[1], '.io-ox-mail-window .list-view.mail-item')
})

Scenario('Shortcut to reply to mail', async ({ I, mail, users }) => {
  await I.haveSetting({
    'io.ox/core': { features: { shortcuts: true } }
  })
  await I.haveMail({
    from: users[1],
    subject: 'Reply me',
    to: [
      [users[0].get('display_name'), users[0].get('email1')],
      [users[2].get('display_name'), users[2].get('email1')]
    ]
  }, { user: users[1] })

  await I.login('app=io.ox/mail')

  I.see('Reply me')
  await mail.selectMail('Reply me')
  I.pressKey('r')
  I.waitForVisible(mail.composeWindow)
  I.waitForInvisible('.io-ox-busy') // wait for loading icon to disappear
  await within(mail.composeWindow, () => {
    I.wait(1) // wait for the to field to be filled
    I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`, undefined, '.token-label')
    I.dontSee(`${users[0].get('given_name')} ${users[0].get('sur_name')}`, '.token-label')
    I.dontSee(`${users[2].get('given_name')} ${users[2].get('sur_name')}`, '.token-label')
    I.seeInField('subject', 'Re: Reply me')
  })
})

Scenario('Shortcut to reply all', async ({ I, mail, users }) => {
  const subject = 'Reply me'
  await I.haveSetting({
    'io.ox/core': { features: { shortcuts: true } }
  })
  const to = [
    [users[0].get('display_name'), users[0].get('email1')],
    [users[1].get('display_name'), users[1].get('email1')],
    [users[2].get('display_name'), users[2].get('email1')]
  ]
  await I.haveMail({ from: users[1], subject, to }, { user: users[1] })

  await I.login('app=io.ox/mail')

  I.see(subject)
  await mail.selectMail(subject)
  I.pressKey(['Shift', 'r'])
  I.waitForVisible(mail.composeWindow)
  I.waitForInvisible('.io-ox-busy') // wait for loading icon to disappear
  await within(mail.composeWindow, () => {
    I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`, undefined, '.to')
    I.waitForText(users[2].get('display_name'), undefined, '.to')
    I.seeInField('subject', `Re: ${subject}`)
  })
})

Scenario('Shortcut to forward', async ({ I, mail, users }) => {
  const subject = 'Forward me'
  // Needs to be enabled due to timing issues with mail loading
  await users[0].hasCapability('switchboard')
  I.haveSetting({
    'io.ox/core': { features: { shortcuts: true } }
  })
  const to = [
    [users[0].get('display_name'), users[0].get('email1')],
    [users[1].get('display_name'), users[1].get('email1')],
    [users[2].get('display_name'), users[2].get('email1')]
  ]
  await I.haveMail({ from: users[1], subject, to }, { user: users[1] })

  await I.login('app=io.ox/mail')
  await mail.selectMail(subject)
  I.pressKey('f')
  I.waitForVisible(mail.composeWindow)
  I.waitForInvisible('.io-ox-busy') // wait for loading icon to disappear
  await within(mail.composeWindow, () => {
    I.seeInField('subject', `Fwd: ${subject}`)
  })
})

Scenario('Shortcut to set mail read/unread', async ({ I, mail, users }) => {
  const subject = 'Read me'
  await I.haveSetting({
    'io.ox/core': { features: { shortcuts: true } }
  })

  await Promise.all([
    I.haveMail({
      from: users[0],
      subject,
      to: users[0]
    })
  ])

  await I.login('app=io.ox/mail')

  I.see(subject, '.unread')
  await mail.selectMail(subject)
  I.waitForDetached('.unread')
  I.waitForElement('~Mark as unread')
  I.dontSee('.seen-unseen-indicator')

  I.pressKey('u')

  I.waitForElement('~Mark as read')
  I.see(subject, '.unread')
  I.waitForElement('.seen-unseen-indicator')

  I.pressKey('i')
  I.waitForDetached('.unread')
  I.waitForElement('~Mark as unread')
  I.dontSee('.seen-unseen-indicator')
})
