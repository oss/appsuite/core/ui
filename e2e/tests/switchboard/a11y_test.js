/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const moment = require('moment')

Feature('Accessibility > Switchboard')

Before(async function ({ users }) {
  await users.create()
  await users[0].context.hasCapability('switchboard')
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Switchboard - Calendar', async ({ I, calendar }) => {
  await I.haveSetting('io.ox/core//features/presence', true)

  const time = moment().startOf('day').add(10, 'hours')

  await I.haveAppointment({
    summary: 'test invite accept/decline/accept tentative',
    startDate: { value: time },
    endDate: { value: time.add(1, 'hour') },
    flags: ['conferences', 'organizer', 'accepted'],
    conferences: [{
      id: 456,
      uri: 'https://localhost',
      label: 'Zoom Meeting',
      features: ['AUDIO', 'VIDEO'],
      extendedParameters: {
        'X-OX-TYPE': 'zoom',
        'X-OX-ID': '65498713',
        'X-OX-OWNER': 'test.user@schmalzgollum.com'
      }
    }]
  })

  await I.haveSetting('io.ox/calendar//layout', 'month')
  await I.login('app=io.ox/calendar')

  I.waitForVisible('.appointment')
  I.click('.appointment')
  I.waitForVisible('.detail-popup')
  I.waitForElement(locate('.btn-default').withText('Join Zoom meeting'))
  expect(await I.grabAxeReport()).to.be.accessible
})
