/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Settings > Security > 2-step verification')
const OTPAuth = require('otpauth')
const { I, dialogs } = inject()

function getTotp (secret) {
  secret = secret.replace(/(\r\n|\n|\r)/, '')
  const totp = new OTPAuth.TOTP({
    algorithm: 'SHA1',
    digits: 6,
    period: 30,
    secret: secret.replace(/ /g, '')
  })
  return totp.generate()
}

Before(async ({ users }) => {
  await users.create()
  await users[0].context.hasCapability('multifactor')
})

After(async ({ users }) => {
  await users[0].context.doesntHaveCapability('multifactor')
  await users.removeAll()
})

function enterCode (code) {
  I.waitForVisible('.multifactorAuthDiv .mfInput')
  I.fillField('.mfInput', code)
  I.click('Next')
}

function addSMS () {
  I.waitForText('Add verification option')
  I.wait(0.3)
  I.click('Add verification option')
  I.waitForText('Code via text message')
  I.click('Code via text message')

  I.waitForVisible('#deviceNumber')
  I.waitForVisible('.countryCodes')
  I.fillField('#deviceNumber', '5555551212')
  dialogs.clickButton('Ok')

  I.waitForText('Confirm Code')
  I.fillField('Confirm Code', '0815')
  dialogs.clickButton('Ok')
}

async function addTOTP () {
  I.waitForText('Add verification option')
  I.wait(0.3)
  I.click('Add verification option')
  I.waitForText('Google Authenticator or compatible')
  I.click('Google Authenticator or compatible')

  I.waitForVisible('#code')
  const secret = await I.grabTextFrom('#code')
  const code = getTotp(secret)
  I.fillField('Authentication code', code)
  dialogs.clickButton('Ok')
  I.waitForDetached('.modal[data-point="multifactor/views/totpProvider"]')
  return secret
}

Scenario('Add TOTP multifactor and login using', async ({ I, settings }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  const secret = await addTOTP()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')

  I.waitForText('Authenticator 1') // Listed in active list

  settings.close()
  await I.logout()

  await I.login(undefined, { wait: false })
  enterCode(getTotp(secret))
  I.waitForInvisible('#background-loader.busy', 20)
  await I.waitForApp()
})

Scenario('TOTP multifactor bad entry', async ({ I, users, settings }) => {
  await users[0].hasConfig('com.openexchange.multifactor.maxBadAttempts', 4)
  // Login to settings
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  const secret = await addTOTP()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')

  I.waitForText('Authenticator 1') // Listed in active list

  settings.close()
  await I.logout()

  await I.login('app=io.ox/mail', { wait: false }) // Log back in
  const badCode = Number(getTotp(secret)) + 123

  I.waitForVisible('.multifactorAuthDiv .mfInput')
  I.fillField('.mfInput', badCode)
  I.click('Next')
  I.waitForText('The authentication failed.')
  I.wait(0.5)
  I.waitForVisible('.multifactorAuthDiv .mfInput')
  I.fillField('.mfInput', badCode)
  I.click('Next')
  I.waitForText('The authentication failed.')
  I.wait(0.5)
  I.waitForVisible('.multifactorAuthDiv .mfInput')
  I.fillField('.mfInput', badCode)
  I.click('Next')
  I.waitForText('The authentication failed.')
  I.wait(0.5)
  I.waitForVisible('.multifactorAuthDiv .mfInput')
  I.fillField('.mfInput', badCode)
  I.wait(0.5)
  I.click('Next')
  await I.executeScript('')
  I.waitForVisible('.io-ox-alert')
  I.see('locked', '.io-ox-alert')
  await I.login('app=io.ox/mail', { wait: false })
  I.waitForVisible('.io-ox-alert')
  I.see('locked', '.io-ox-alert')
})

Scenario('Add SMS multifactor and login using', async ({ I, settings }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  addSMS()

  I.waitForText('Code via text message')
  I.waitForText('Backup code to access your account')
  I.click('Close', '.modal.verification-options')

  I.waitForText('My Phone') // Listed in active list
  I.waitForText('SMS Code')

  settings.close()
  await I.logout()

  await I.login(undefined, { wait: false })

  enterCode('0815')
  I.waitForInvisible('#background-loader.busy', 20)
  await I.waitForApp()
})

Scenario('Add SMS multifactor, then lost device', async ({ I, settings }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  addSMS()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  const recovery = await I.grabTextFrom('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')

  I.waitForText('My Phone') // Listed in active list

  settings.close()
  await I.logout()

  await I.login(undefined, { wait: false })

  I.waitForText('You secured your account with 2-step verification.')
  I.click('I lost my device')

  I.waitForText('Please enter the recovery code')
  I.fillField('Recovery code', recovery)
  I.click('Next')
  await I.waitForApp()
})

Scenario('Add TOTP multifactor, then lost device', async ({ I, settings, dialogs }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  const secret = await addTOTP()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  const recovery = await I.grabTextFrom('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')

  I.waitForText('Authenticator 1') // Listed in active list

  settings.close()
  await I.logout()
  await I.login(undefined, { wait: false })
  enterCode(getTotp(secret))
  I.waitForInvisible('#background-loader.busy', 20)
  await I.waitForApp()

  // leave happy path
  // force re-authentication while adding a new device
  I.refreshPage()

  I.waitForNetworkTraffic()
  await I.waitForApp()
  await settings.open('Security', 'Two-step verification')
  I.waitForText('Add verification option')
  I.click('Add verification option')
  I.waitForText('Google Authenticator or compatible')
  I.click('Google Authenticator or compatible')

  I.waitForText('Reauthentication required for this action')
  dialogs.clickButton('I lost my device')
  I.wait(1)
  I.fillField('Recovery code', recovery)
  dialogs.clickButton('Next')

  I.waitForText('Scan the QR code with your authenticator')
})

Scenario('Add multiple multifactors, then login', async ({ I, settings }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  addSMS()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')

  I.waitForText('My Phone') // Listed in active list

  await addTOTP()

  settings.close()
  await I.logout()

  await I.login(undefined, { wait: false })

  I.waitForText('Please select a device to use for additional authentication')
  I.click('My Phone')

  enterCode('0815')
  await I.waitForApp()
})

Scenario('Add second device and force re-authentication', async ({ I, settings, dialogs }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  await addSMS()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')
  I.waitForText('My Phone') // Listed in active list
  settings.close()
  await I.logout()

  await I.login(undefined, { wait: false })
  // fail once and then succeed
  enterCode('0816')
  I.waitForText('The authentication failed.')
  enterCode('0815')
  I.waitForInvisible('#background-loader.busy', 20)
  await I.waitForApp()

  // leave happy path
  // force re-authentication while adding a new device
  I.refreshPage()
  I.waitForNetworkTraffic()
  await I.waitForApp()
  await settings.open('Security', 'Two-step verification')
  I.waitForText('Add verification option')
  I.click('Add verification option')
  I.waitForVisible('.verification-options .mfIcon.bi-key')
  // cancel and reopen
  I.click('.verification-options .mfIcon.bi-key')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal[data-point="multifactor/views/smsProvider"]')
  // now add
  I.waitForText('Add verification option')
  I.click('Add verification option')

  I.waitForText('Google Authenticator or compatible')
  I.click('Google Authenticator or compatible')
  enterCode('0815')
  I.waitForVisible('#code')

  const secret = await I.grabTextFrom('#code')
  const code = getTotp(secret)
  I.fillField('Authentication code', code)
  dialogs.clickButton('Ok')
  I.waitForText('Authenticator 1')
})

Scenario('Add second TOTP and force re-authentication', async ({ I, settings, dialogs }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  let secret = await addTOTP()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')
  I.waitForText('Authenticator 1') // Listed in active list
  settings.close()
  await I.logout()

  await I.login(undefined, { wait: false })
  // fail once and then succeed
  enterCode(`${getTotp(secret)}_false`)
  I.waitForText('The authentication failed.')
  enterCode(getTotp(secret))
  I.waitForInvisible('#background-loader.busy', 20)
  await I.waitForApp()

  // leave happy path
  // force re-authentication while adding a new device
  I.refreshPage()
  I.waitForNetworkTraffic()
  await I.waitForApp()
  await settings.open('Security', 'Two-step verification')
  I.waitForText('Add verification option')
  I.click('Add verification option')
  I.waitForVisible('.verification-options .mfIcon.bi-key')
  // cancel and reopen
  I.click('.verification-options .mfIcon.bi-key')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal[data-point="multifactor/views/totpProvider"]')
  // now add
  I.waitForText('Add verification option')
  I.click('Add verification option')

  I.waitForText('Google Authenticator or compatible')
  I.click('Google Authenticator or compatible')
  enterCode(getTotp(secret))
  I.waitForVisible('#code')

  secret = await I.grabTextFrom('#code')
  const code = getTotp(secret)
  I.fillField('Authentication code', code)
  dialogs.clickButton('Ok')
  I.waitForText('Authenticator 2')
})

Scenario('Add second TOTP, delete and rename', async ({ I, settings, dialogs }) => {
  await I.login('section=io.ox/settings/security/multifactor&settings=virtual/settings/security')

  await addTOTP()

  I.waitForText('Backup code to access your account.')
  I.click('Backup code to access your account.')
  I.waitForText('Recovery Code')
  I.waitForElement('.multifactorRecoveryCodeDiv')
  dialogs.clickButton('Ok')
  I.waitForText('Authenticator 1')

  await addTOTP()
  I.say('Remove Authenticator 1')
  I.waitForElement('~Delete Authenticator 1')
  I.waitForVisible('~Delete Authenticator 1')
  I.click('~Delete Authenticator 1')
  I.waitForText('This will delete the device named Authenticator 1.')
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal[data-point="multifactor/settings/deleteMultifactor"]')
  I.waitForDetached('li[data-devicename="Authenticator 1"]')

  I.say('Remove My Backup')
  I.waitForVisible('~Delete My Backup')
  I.click('~Delete My Backup')
  I.waitForText('This will delete the device named My Backup.')
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal[data-point="multifactor/settings/deleteMultifactor"]')
  I.waitForDetached('li[data-devicename="My Backup"]')

  I.say('Edit Authenticator 2')
  I.waitForElement('~Edit Authenticator 2')
  I.waitForVisible('~Edit Authenticator 2')
  I.click('~Edit Authenticator 2')
  I.waitForText('This will edit the name for device (Authenticator 2)')
  I.fillField('Name', 'New Authenticator Name')
  dialogs.clickButton('Save')
  I.waitForDetached('.modal[data-point="multifactor/settings/editMultifactor"]')
  I.waitForDetached('li[data-devicename="Authenticator 2"]')
  I.waitForText('New Authenticator Name')
})
