/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('General > Mobile App Launcher').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ I, users }) => {
  await users.create()

  // open mobile app launcher but don't load any app
  I.haveSetting({ 'io.ox/core': { autoStart: 'none' } })
  await I.login(undefined, { wait: false })
  I.waitForNetworkTraffic()
  I.waitForElement('#io-ox-launcher', 30)
  I.click('#io-ox-launcher button')
  I.waitForElement('.launcher-dropdown.complete')
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Launch "Change contact photo" application @mobile @smoketest', ({ I }) => {
  I.click('.launcher-dropdown .user-picture-container')
  I.waitForText('Change contact photo', undefined, '.modal.edit-picture .modal-title')
})

Scenario('Launch "E-Mail" application @mobile @smoketest', async ({ I }) => {
  I.click('Mail', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForElement('.io-ox-mail-window')
  I.waitForText('Inbox', undefined, '.io-ox-mail-window .current .app-header-container')
})

Scenario('Launch "Calendar" application @mobile @smoketest', async ({ I }) => {
  I.click('Calendar', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForText('Today', undefined, '.io-ox-calendar-window .bottom-toolbar')
})

Scenario('Launch "Address Book" application @mobile @smoketest', async ({ I }) => {
  I.click('Address Book', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForElement('.io-ox-contacts-window')
  I.waitForText('Contacts', undefined, '.io-ox-contacts-window .current .app-header-container')
})

Scenario('Launch "Tasks" application @mobile @smoketest', async ({ I }) => {
  I.click('Tasks', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForText('This task list is empty', undefined, '.io-ox-tasks-window')
})

Scenario('Launch "Drive" application @mobile @smoketest', async ({ I }) => {
  I.click('Drive', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForElement('.io-ox-files-window')
  I.waitForText('My files', undefined, '.io-ox-files-window .current .app-header-container')
})

Scenario('Launch "Portal" application @mobile @smoketest', async ({ I }) => {
  I.click('Portal', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForElement('.window-content.io-ox-portal .greeting-phrase')
})

Scenario.skip('Launch "Install OX App Suite like an app" modal @mobile @smoketest', async ({ I }) => {
  I.click('Install OX App Suite like an app', '.launcher-dropdown')
  I.waitForText('Install OX App Suite', undefined, '.modal-dialog')
})

Scenario('Launch "Edit personal information" application @mobile @smoketest', ({ I, contacts }) => {
  I.click('Edit personal data', '.launcher-dropdown')
  I.waitForText('First name', undefined, contacts.editWindow)
})

Scenario('Launch "Settings" application @mobile @smoketest', ({ I }) => {
  I.click('Settings', '.launcher-dropdown')
  I.waitForText('Settings', undefined, '~Settings Folders')
})

Scenario('Launch "Connect this device" application @mobile @smoketest', ({ I }) => {
  I.click('Connect this device', '.launcher-dropdown')
  I.waitForText('Connect your device', undefined, '.wizard-content')
  I.waitForText('Choose an application', undefined, '.wizard-content')
})

Scenario('Launch "Help" application @mobile @smoketest', async ({ I }) => {
  I.click('Help', '.launcher-dropdown')
  I.waitForElement('.inline-help-iframe')
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.waitForText('User Guide')
  })
})

Scenario('Launch "Updates" modal @mobile @smoketest', ({ I }) => {
  I.forceClick('Updates', '.launcher-dropdown')
  I.waitForText('Updates', undefined, '.modal.mobile-dialog.whats-new-dialog')
})

Scenario('Launch "Give feedback" modal @mobile @smoketest', ({ I }) => {
  I.forceClick('Give feedback', '.launcher-dropdown')
  I.waitForText('We appreciate your feedback', undefined, '.modal.mobile-dialog .modal-header')
})

Scenario('Launch "About" modal @mobile @smoketest', ({ I }) => {
  I.forceClick('About', '.launcher-dropdown')
  I.waitForText('OX App Suite', undefined, '.modal.mobile-dialog.about-dialog .modal-header')
})

Scenario('Sign out @mobile @smoketest', ({ I }) => {
  I.forceClick('Sign out', '.launcher-dropdown')
  I.waitForText('Sign in', undefined, '#io-ox-login-form')
})

Scenario('Close app launcher @mobile @smoketest', ({ I }) => {
  I.click('.btn-close')
  I.waitForDetached('.smart-dropdown-container .launcher-dropdown')
})

Scenario('Don\'t close app launcher when not clicking on a menu item @mobile @smoketest', ({ I }) => {
  // regression test to prevent the default dropdown behaviour, where the dropdown closes when clicking anywhere
  I.click('.user .text-container')
  I.wait(0.5)
  I.seeElement('.smart-dropdown-container .launcher-dropdown')
})
