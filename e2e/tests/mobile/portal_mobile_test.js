/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')
const { DateTime } = require('luxon')

Feature('Mobile > Portal').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Open portal on mobile', async ({ I }) => {
  await I.login('app=io.ox/portal')

  I.waitForElement('.greeting-phrase')
  I.seeNumberOfElements('.widget', 5)
})

Scenario('Remove widgets on mobile', async ({ I, dialogs }) => {
  const firstWidgetDisable = locate('.disable-widget').at(1)
  await I.login('app=io.ox/portal')

  I.waitForElement('.greeting-phrase')
  I.seeNumberOfElements('.widget', 5)

  // first one is inbox that has special delete behavior
  I.click(firstWidgetDisable)
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal-dialog')
  I.seeNumberOfElements('.widget', 4)

  I.click(firstWidgetDisable)
  I.seeNumberOfElements('.widget', 3)

  I.click(firstWidgetDisable)
  I.seeNumberOfElements('.widget', 2)

  I.click(firstWidgetDisable)
  I.seeNumberOfElements('.widget', 1)

  I.click(firstWidgetDisable)
  I.dontSeeElement('.widget')
})

Scenario('Portal with widget summary: Inbox', async ({ I, users }) => {
  await Promise.all([
    I.haveSetting({ 'io.ox/portal': { mobile: { summaryView: 'true' } } }),
    I.haveMail({
      from: users[0],
      subject: 'Mark me as unread',
      to: users[0]
    })
  ])

  await I.login('app=io.ox/portal')
  I.waitForElement('.greeting-phrase')

  // mark mail as unread
  I.waitForText('You have 1 unread message')
  I.click('.title', '~Inbox')
  I.seeNumberOfVisibleElements('.mailwidget li', 1)
  I.click('.mailwidget li')
  I.waitForVisible('.detail-popup')
  I.click('~Close')
  I.waitForDetached('.detail-popup')

  // check summary
  I.click('.title', '~Inbox')
  I.see('You have no unread messages')
})

Scenario('Portal with widget summary: Appointments', async ({ I, users }) => {
  const time = DateTime.local().startOf('day').plus({ hours: 8 })
  await I.haveSetting({ 'io.ox/portal': { mobile: { summaryView: 'true' } } })

  await I.login('app=io.ox/portal')
  I.waitForElement('.greeting-phrase')
  I.waitForText('You don\'t have any appointments in the near future.')

  await Promise.all([
    I.haveAppointment({
      summary: 'Test appointment 1',
      startDate: { value: time.plus({ days: 1 }) },
      endDate: { value: time.plus({ days: 1, hours: 2 }) }
    }),
    I.haveAppointment({
      summary: 'Test appointment 2',
      startDate: { value: time.plus({ days: 2 }) },
      endDate: { value: time.plus({ days: 2, hours: 2 }) }
    })
  ])

  I.refreshPage()
  I.waitForElement('.greeting-phrase')
  I.waitForText('Tomorrow')
  I.see('Test appointment 1')
  I.click('.title', '~Appointments')
  I.see('Test appointment 1')
  I.see('Test appointment 2')
  I.click('.title', '~Appointments')
  I.waitForText('Tomorrow')
  I.see('Test appointment 1')
  I.dontSee('Test appointment 2')
})
