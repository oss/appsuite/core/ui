/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')
const { DateTime } = require('luxon')

Feature('Mobile > Closable windows').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('I can open and collapse windows @mobile', async ({ I, mail, mobileMail, mobileCalendar }) => {
  await I.login()

  mobileMail.newMail()
  I.click('~Close window', mail.composeWindow)
  I.waitForInvisible(mail.composeWindow)

  I.click('~Navigate to:')
  I.waitForElement('.launcher-dropdown.complete')
  I.click('Calendar', '.launcher-dropdown.complete')
  await I.waitForApp()
  await mobileCalendar.newAppointment()
  mobileCalendar.setDate('startDate', DateTime.local().set({ hour: 14, minutes: 0 }))
  I.waitForElement('.io-ox-calendar-edit-window.complete')
  I.click('~Close window', '.io-ox-calendar-edit-window')
  I.waitForInvisible('.io-ox-calendar-edit-window')

  I.click('~Navigate to:')
  I.waitForElement('.launcher-dropdown.complete')
  I.click('New email', '.launcher-dropdown.complete')
  I.waitForVisible('.io-ox-mail-compose-window.complete')
  I.click('Discard')
  I.waitForDetached(mail.composeWindow)

  I.click('~Create appointment')
  I.waitForVisible('.io-ox-calendar-edit-window.complete')
  await within('.io-ox-calendar-edit-window.complete', () => {
    I.fillField('Title', 'Appointment name')
    I.click('Create')
  })
  I.waitForDetached('.io-ox-calendar-edit-window.complete')
})

Scenario('Closing windows restores focus on primary button (Mail) @mobile', async ({ I, mail, mobileMail }) => {
  await I.login()

  mobileMail.newMail()
  I.click('Discard', mail.composeWindow)
  I.waitForInvisible(mail.composeWindow)

  await I.waitForFocus('.inline-toolbar [aria-label="New email"]')
})

Scenario('Closing windows restores focus on primary button (Calendar) @mobile', async ({ I, mobileCalendar }) => {
  await I.login('app=io.ox/calendar')

  await mobileCalendar.newAppointment()
  I.click('Discard', '.io-ox-calendar-edit-window')
  I.waitForInvisible('.io-ox-calendar-edit-window')

  await I.waitForFocus('.inline-toolbar [aria-label="Create appointment"]')
})

Scenario('Closing windows restores focus on primary button (Address book) @mobile', async ({ I, contacts, mobileContacts }) => {
  await I.login('app=io.ox/contacts')

  mobileContacts.newContact()
  I.click('Discard', contacts.editWindow)
  I.waitForInvisible(contacts.editWindow)

  await I.waitForFocus('.inline-toolbar [aria-label="New contact"]')
})

Scenario('Closing windows restores focus on primary button (Tasks) @mobile', async ({ I }) => {
  await I.login('app=io.ox/tasks')

  I.click('~New task')
  I.waitForVisible('.io-ox-tasks-edit-window.complete')

  I.click('Discard', '.io-ox-tasks-edit-window')
  I.waitForInvisible('.io-ox-tasks-edit-window')

  await I.waitForFocus('.inline-toolbar [aria-label="New task"]')
})

Scenario('Collapsing windows restores focus on primary button @mobile', async ({ I, mail, mobileMail }) => {
  await I.login()

  mobileMail.newMail()
  I.click('~Close window', mail.composeWindow)
  I.waitForInvisible(mail.composeWindow)
  await I.waitForFocus('.inline-toolbar [aria-label="New email"]')
})
