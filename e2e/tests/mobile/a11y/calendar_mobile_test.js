/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')
const { DateTime } = require('luxon')

Feature('Mobile > Accessibility > Calendar').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

const disableRules = [
  'scrollable-region-focusable', // [MODERATE] Ensure that scrollable region has keyboard access
  'aria-required-children', //  [CRITICAL] Certain ARIA roles must contain particular children
  'meta-viewport' // [CRITICAL] Zooming and scaling must not be disabled
]

Scenario('Day view w/o appointments @mobile', async ({ I }) => {
  await I.haveSetting({
    'io.ox/calendar': { showCheckboxes: true, layout: 'week:day' }
  })

  await I.login('app=io.ox/calendar')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Year view @mobile', async ({ I }) => {
  const date = DateTime.local()
  await I.haveSetting({
    'io.ox/calendar': { showCheckboxes: true, layout: 'year' }
  })
  await I.login('app=io.ox/calendar')

  I.waitForText(date.toFormat('MMMM'))
  I.click(date.toFormat('MMMM'))
  I.waitForVisible('.month-container')
  I.waitForText(date.toFormat('yyyy'), 5, '#io-ox-appcontrol')
  I.click(date.toFormat('yyyy'), '#io-ox-appcontrol')
  I.waitForText('January', 5, '.year-view-container')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Month view w/o appointments @mobile', async ({ I }) => {
  const month = DateTime.local().toFormat('MMMM')
  await I.login('app=io.ox/calendar')

  I.waitForText(month)
  I.click(month)
  I.waitForVisible('.month-container')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Month view with one appointment @mobile', async ({ I }) => {
  const now = DateTime.local().set({ hour: 8, minute: 0 })
  const month = now.toFormat('MMMM')

  await I.haveAppointment({
    summary: 'Test title',
    startDate: { value: now.toFormat("yyyyMMdd'T'HHmmss'Z'") },
    endDate: { value: now.plus({ hours: 1 }).toFormat("yyyyMMdd'T'HHmmss'Z'") }
  })

  await I.login('app=io.ox/calendar')

  I.waitForText(month)
  I.click(month)
  I.waitForVisible('.month-container .today .appointment')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('List view w/o appointments @mobile', async ({ I }) => {
  await I.login('app=io.ox/calendar')

  I.click('~List view')
  I.waitForVisible('.list-view.complete')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Day view with one short appointment @mobile', async ({ I }) => {
  const time = DateTime.local().startOf('day').set({ hour: 10, minutes: 0 })

  await Promise.all([
    I.haveAppointment({
      summary: 'test invite accept/decline/accept tentative',
      startDate: { value: time.toFormat("yyyyMMdd'T'HHmmss'Z'") },
      endDate: { value: time.plus({ hours: 1 }).toFormat("yyyyMMdd'T'HHmmss'Z'") }
    }),
    I.haveSetting({ 'io.ox/calendar': { selectedFolders: [`cal://0/${await I.grabDefaultFolder('calendar')}`] } })
  ])

  await I.login('app=io.ox/calendar')

  I.waitForText('test invite', undefined, '.appointment-container .appointment')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Day view with one all day appointment @mobile', async ({ I }) => {
  const time = DateTime.local().startOf('day').set({ hour: 10, minutes: 0 })

  await Promise.all([
    I.haveAppointment({
      summary: 'test invite accept/decline/accept tentative',
      startDate: { value: time.toFormat('yyyyMMdd') },
      endDate: { value: time.plus({ days: 1 }).toFormat('yyyyMMdd') }
    }),
    I.haveSetting({ 'io.ox/calendar': { selectedFolders: [`cal://0/${await I.grabDefaultFolder('calendar')}`] } })
  ])

  await I.login('app=io.ox/calendar')

  I.waitForText('test invite', undefined, '.appointment-container .appointment')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Day view with appointment clicked @mobile', async ({ I }) => {
  const time = DateTime.local().startOf('day').set({ hour: 10, minutes: 0 })

  await Promise.all([
    I.haveAppointment({
      summary: 'test invite accept/decline/accept tentative',
      startDate: { value: time.toFormat("yyyyMMdd'T'HHmmss'Z'") },
      endDate: { value: time.plus({ hours: 1 }).toFormat("yyyyMMdd'T'HHmmss'Z'") }
    }),
    I.haveSetting({ 'io.ox/calendar': { selectedFolders: [`cal://0/${await I.grabDefaultFolder('calendar')}`] } })
  ])

  await I.login('app=io.ox/calendar')

  I.waitForText('test invite', undefined, '.appointment-container .appointment')
  I.click('.appointment-container .appointment')
  I.waitForText('test invite', undefined, '.io-ox-calendar-window')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Calendar folder tree @mobile', async ({ I }) => {
  const month = DateTime.local().toFormat('MMMM')
  const year = DateTime.local().toFormat('yyyy')

  await I.login('app=io.ox/calendar')
  I.click(month)
  I.waitForVisible('.month-container')
  I.click(year)
  I.waitForVisible('.year-view-container')
  I.click('Calendars')
  I.waitForText('My calendars')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

// FIXME: There is a backdrop where forceClick is used, this should not be the case
Scenario('Notification area toggle with notifications open @mobile', async ({ I }) => {
  await I.login('app=io.ox/calendar')

  I.waitForElement('#io-ox-notifications-toggle')
  I.waitForElement('~There are no notifications')
  I.dontSeeElement('.io-ox-notifications')

  I.click('~There are no notifications')
  I.waitForVisible('.io-ox-notifications.visible')

  I.forceClick('~There are no notifications')
  I.waitForInvisible('.io-ox-notifications.visible')

  I.click('~There are no notifications')
  I.waitForVisible('.io-ox-notifications')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('Window: Create appointment @mobile', async ({ I, mobileCalendar }) => {
  await I.login('app=io.ox/calendar')

  mobileCalendar.newAppointment()

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})
