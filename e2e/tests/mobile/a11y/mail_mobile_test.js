/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('Mobile > Accessibility > Mail').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

const disableRules = ['meta-viewport'] // [CRITICAL] Zooming and scaling must not be disabled

Scenario('Vertical view w/o mail @mobile', async ({ I, mail }) => {
  await I.login('app=io.ox/mail')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('List view unified mail w/o mail @mobile', async ({ I, mail }) => {
  await I.login('app=io.ox/mail')
  // enable unified mail
  I.executeScript(async () => {
    const api = await import(String(new URL('io.ox/core/api/account.js', location.href)))
    return api.update({ id: 0, personal: null, unified_inbox_enabled: true })
  })
  I.waitForResponse(response => response.url().includes('api/jslob?action=set') && response.request().method() === 'PUT', 10)
  I.waitForResponse(response => response.url().includes('api/jslob?action=set') && response.request().method() === 'PUT', 10)

  I.refreshPage()
  I.waitForInvisible('#background-loader.busy', 20)
  await I.waitForApp()
  I.waitForText('Folders', undefined, '#io-ox-appcontrol .left-action')
  I.click('Folders', '#io-ox-appcontrol .left-action')
  I.waitForText('Unified mail', 10)

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible

  // disable unified mail
  I.executeScript(async () => {
    const api = await import(String(new URL('io.ox/core/api/account.js', location.href)))
    return api.update({ id: 0, personal: null, unified_inbox_enabled: false })
  })
})

Scenario('Compose window (with exceptions) @mobile', async ({ I, mobileMail }) => {
  // Exceptions:
  // Typeahead missing label (critical), TinyMCE toolbar invalid role (minor issue)
  const exclude = ['.to', '.mce-open', '.mce-toolbar']

  await I.login('app=io.ox/mail')

  mobileMail.newMail()
  I.waitForElement('iframe[title*="Rich Text Area"]')

  expect(await I.grabAxeReport({ disableRules, exclude })).to.be.accessible
})

Scenario('Vacation notice (with exceptions) @mobile', async ({ I, dialogs, settings, mail }) => {
  // Exceptions:
  // Checkbox has no visible label (critical)
  const exclude = ['.checkbox.switch.large']

  const rules = [...disableRules, 'scrollable-region-focusable']

  await I.login('app=io.ox/mail&section=io.ox/mail/settings/rules&settings=virtual/settings/io.ox/mail')

  I.wait(0.5)

  I.waitForText('Vacation notice')
  I.click('Vacation notice …')
  dialogs.waitForVisible()

  expect(await I.grabAxeReport({ disableRules: rules, exclude })).to.be.accessible
})

Scenario('Auto forward (with exceptions) @mobile', async ({ I, dialogs, settings, mail }) => {
  await I.login('app=io.ox/mail&section=io.ox/mail/settings/rules&settings=virtual/settings/io.ox/mail')

  I.wait(0.5)

  I.waitForText('Auto forward')
  I.click('Auto forward …')
  dialogs.waitForVisible()

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})

Scenario('New folder modal (with exceptions) @mobile', async ({ I, dialogs, mail }) => {
  // Exceptions:
  // Input has no visible label (critical)
  const exclude = ['*[placeholder="New folder"]']

  await I.login('app=io.ox/mail')

  // navigate to folder view and enter edit mode
  I.waitForText('Folders', undefined, '#io-ox-appcontrol .left-action')
  I.click('Folders', '#io-ox-appcontrol .left-action')
  I.waitForDetached('.io-ox-core-animation')

  I.waitForVisible('~Actions for Inbox')
  I.click('~Actions for Inbox')
  I.clickDropdown('Add new folder')
  dialogs.waitForVisible()

  expect(await I.grabAxeReport({ disableRules, exclude })).to.be.accessible
})

Scenario('Mail folder tree @mobile', async ({ I }) => {
  await I.login('app=io.ox/mail')

  I.waitForText('Folders')
  I.click('Folder')
  I.waitForText('Inbox', undefined, '.folder-tree')

  expect(await I.grabAxeReport({ disableRules })).to.be.accessible
})
