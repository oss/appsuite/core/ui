/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('Mobile > Accessibility > Login').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

const disableRules = ['meta-viewport'] // [CRITICAL] Zooming and scaling must not be disabled

// Exceptions:
// Login form does not have a visible label
const exclude = ['#io-ox-login-username', '#io-ox-login-password']

Scenario('Login page (with exceptions) @mobile', async ({ I }) => {
  I.amOnLoginPage()
  I.waitForInvisible('#background-loader')

  expect(await I.grabAxeReport({ disableRules, exclude })).to.be.accessible
})
