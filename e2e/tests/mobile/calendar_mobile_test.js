/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')
const { DateTime } = require('luxon')

Feature('Mobile > Calendar').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Create and delete appointment with all fields @mobile', async ({ I, users, calendar, dialogs, mobileCalendar }) => {
  await users.create()
  await I.login('app=io.ox/calendar')

  await mobileCalendar.newAppointment()
  await within(calendar.editWindow, () => {
    I.fillField('Title', 'Test title')
    I.fillField('Location', 'test location')
    I.fillField('Description', 'test description')
    mobileCalendar.setDate('startDate', DateTime.local().set({ hour: 12, minutes: 0 }))
    I.click('Expand form')
    I.fillField('Participants and resources', users[1].get('primaryEmail'))
    I.pressKey('Enter')
    I.selectOption('Visibility', 'Private')
    I.checkOption('All day')
    I.checkOption('Show as free')
    I.click('No reminder')
  })

  dialogs.waitForVisible()
  I.waitForText('Add reminder', undefined, dialogs.body)
  I.click('Add reminder')
  I.selectOption({ css: '.alarm-time' }, '15 minutes')
  dialogs.clickButton('Apply')

  I.waitForDetached('.modal-dialog')
  I.waitForText('Notify 15 minutes before start')
  I.click('Create', '.io-ox-calendar-edit-window')

  I.waitForDetached('.io-ox-calendar-edit-window', 15)
  I.waitForVisible('.page.current .appointment-panel .appointment.private.free')
  I.waitForText('Test title', undefined, '.day')
  I.dontSee('test location', '.day')
  I.seeElement('.page.current .appointment-panel .appointment.private.free')
  I.seeElement('.confidential-flag')

  I.click('Test title', '.fulltime-container .title')
  I.waitForElement('~Delete')
  I.click('~Delete')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal-dialog')
  I.waitForDetached('.appointment')
})

Scenario('Create and discard appointment @mobile', async ({ I, dialogs, calendar, mobileCalendar }) => {
  await I.login('app=io.ox/calendar')

  await mobileCalendar.newAppointment()
  await within(calendar.editWindow, () => {
    I.fillField('Title', 'Test title')
    I.click('Discard')
  })
  dialogs.waitForVisible()
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
  await within(calendar.editWindow, () => {
    I.clearField('Title')
    I.click('Discard')
  })
  I.waitForDetached('.io-ox-calendar-edit')
})

Scenario('Edit an appointment in list view @mobile', async ({ I, calendar, mobileCalendar }) => {
  const time = DateTime.local().startOf('day').plus({ hours: 8 })
  await I.haveAppointment({
    summary: 'Test appointment',
    location: 'Test location',
    description: 'Test description',
    startDate: { value: time },
    endDate: { value: time.plus({ hours: 1 }) }
  })

  await I.login('app=io.ox/calendar')

  I.click('~List view')
  I.waitForText('Test appointment', undefined, '.list-view .appointment')
  I.waitForText('Test location', undefined, '.list-view .appointment')
  I.waitForText('Appointments', undefined, '.current .app-header-container')
  I.click('.list-view .appointment')
  await within('.calendar-detail', () => {
    I.waitForText('Test location')
    I.waitForText('Test description')
  })
  I.waitForVisible('~Edit')
  I.click('~Edit')
  I.waitForVisible('.io-ox-calendar-edit-window')
  I.waitForVisible('.io-ox-calendar-edit-window input[name="summary"]')

  await within(calendar.editWindow, () => {
    I.fillField('Title', 'edited title')
    I.fillField('Location', 'edited location')
    I.fillField('Description', 'edited description')

    I.click('Save')
  })
  I.waitForDetached('.io-ox-calendar-edit-window')

  await within('.calendar-detail', () => {
  // validate edited appointment in detail view
    I.waitForText('edited title')
    I.waitForText('edited location')
    I.waitForText('edited description')
    I.dontSee('Test')
  })
  // validate edited appointment in list view
  I.click('Back')
  await within('.list-view', () => {
    I.waitForText('edited title')
    I.waitForText('edited location')
  })
})

Scenario('Create an appointment in day view and delete it in list view @mobile', async ({ I, dialogs, calendar, mobileCalendar }) => {
  await I.login('app=io.ox/calendar')

  await mobileCalendar.newAppointment()

  await within(calendar.editWindow, () => {
    I.fillField('Title', 'Test title')
    I.click('Create')
  })
  I.waitForDetached(mobileCalendar.editWindow, 15)

  I.waitForText('Test title', 5, '.weekview-container .appointment')
  I.click('~List view')
  I.waitForNetworkTraffic()
  I.waitForText('Test title', undefined, '.list-view .appointment')
  I.click('.list-view .appointment')

  I.waitForVisible('~Delete')
  I.click('~Delete')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete appointment')
  I.waitForDetached('.modal-dialog')
  I.waitForText('No appointments found')
})

Scenario('Folder handling @mobile', async ({ I, calendar, mobileCalendar, dialogs, users }) => {
  const now = DateTime.local().set({ hour: 8, minute: 0 })
  const month = now.toFormat('MMMM')
  await I.haveAppointment({
    summary: 'Test title',
    startDate: { value: now },
    endDate: { value: now.plus({ hours: 1 }) }
  })
  await I.login('app=io.ox/calendar')

  // month view > calendar selection view > uncheck calendar
  I.wait(0.5)
  I.click(month, '#io-ox-appcontrol')
  I.waitForText('Test title', undefined, `#${now.toFormat("'slide-month-'yyyy-MM'-day-'yyyy-MM-dd")}.today .appointment .title`)
  I.wait(0.5)
  I.click('Calendars', '.current .monthview-container .bottom-toolbar')
  I.waitForVisible('#calendar-tree-mobile .window-container.open.complete')

  // add new calendar
  I.click('~Add new calendar')
  dialogs.waitForVisible()
  I.fillField('Calendar name', 'Calendar name')
  dialogs.clickButton('Add')
  I.waitForDetached('.modal-dialog')
  I.waitForText('Calendar name', undefined, '.folder-tree')

  // rename calendar
  I.click('~Actions for Calendar name')
  I.clickDropdown('Rename')
  dialogs.waitForVisible()
  I.fillField('Folder name', 'Renamed calendar')
  dialogs.clickButton('Rename')
  I.waitForDetached('.modal-dialog')
  I.waitForText('Renamed calendar', undefined, '.folder-tree')
  I.click(locate('.color-label').first())

  // navigate to day view of calendar "Renamed calendar"
  I.click('~Close window')
  I.waitForVisible('#calendar-tree-mobile .window-container.closed.complete')
  I.dontSee('Test title', '.monthview-container')
  I.click('.month .today')
  I.waitForVisible('.appointment-container')
  I.dontSee('Test title', '.weekview-container')

  // create appointment
  await mobileCalendar.newAppointment()
  await within(calendar.editWindow, async () => {
    I.fillField('Title', 'Appointment name')
    await mobileCalendar.setDate('startDate', DateTime.local().set({ hour: 14, minute: 0 }))
    I.click('Create')
  })
  I.waitForDetached(mobileCalendar.editWindow, 15)
  I.waitForText('Appointment name', 10, '.weekview-container')

  // navigate to folder view
  I.wait(0.5)
  I.click(month, '#io-ox-appcontrol')
  I.waitForText('Appointment name', undefined, `#${now.toFormat("'slide-month-'yyyy-MM'-day-'yyyy-MM-dd")}.today .appointment .title`)
  I.wait(0.5)
  I.click('Calendars', '.current .monthview-container .bottom-toolbar')
  I.waitForVisible('#calendar-tree-mobile .window-container.open.complete')

  // delete calendar
  I.click('~Actions for Renamed calendar')
  I.clickDropdown('Delete')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal-dialog')
  I.dontSee('Renamed calendar')

  // no contextmenu for public calendars
  I.click('Public calendars', '.folder-tree')
  I.waitForText('All my public appointments')
  I.dontSeeElement('[aria-label="All my public appointments"] .contextmenu-control')

  // don't see appointment of deleted calendar
  I.click('~Close window')
  I.waitForVisible('#calendar-tree-mobile .window-container.closed.complete')
  I.waitForInvisible(`#${now.toFormat("'slide-month-'yyyy-MM'-day-'yyyy-MM-dd")}.today .appointment .title`)
  I.click('.month .today')
  I.dontSee('Test title', '.weekview-container')
})

Scenario('Toggling existing calendars shows correct appointments across all views @mobile', async ({ I, calendar, mobileCalendar, dialogs, users }) => {
  const date = DateTime.now().set({ hour: 12, minute: 0 })
  const defaultFolder = await I.grabDefaultFolder('calendar')
  await Promise.all([
    I.haveSetting({ 'io.ox/calendar': { selectedFolders: [`cal://0/${defaultFolder}`] } }),
    I.haveAppointment({
      summary: 'Appointment 0',
      startDate: { value: date.plus({ hours: 2 }) },
      endDate: { value: date.plus({ hours: 4 }) }
    }),
    I.haveAppointment({
      folder: await I.haveFolder({ title: 'Calendar A', module: 'event', parent: `cal://0/${defaultFolder}` }),
      summary: 'Appointment A',
      startDate: { value: date.plus({ hours: 2 }) },
      endDate: { value: date.plus({ hours: 4 }) }
    }),
    I.haveAppointment({
      folder: await I.haveFolder({ title: 'Calendar B', module: 'event', parent: `cal://0/${defaultFolder}` }),
      summary: 'Appointment B',
      startDate: { value: date.plus({ hours: 2 }) },
      endDate: { value: date.plus({ hours: 4 }) }
    })
  ])

  await I.login('app=io.ox/calendar')

  I.waitForText('Appointment 0', 5, '.weekview-container')
  I.dontSee('Appointment A', '.weekview-container')
  I.dontSee('Appointment B', '.weekview-container')

  // navigate to month view and check
  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol .left-action')
  I.waitForText('Appointment 0', 5, '.month-container')
  I.dontSee('Appointment A', '.month-container')
  I.dontSee('Appointment B', '.month-container')

  I.wait(0.5)
  I.click('Calendars', '.current .monthview-container .bottom-toolbar')
  I.waitForVisible('#calendar-tree-mobile .window-container.open.complete')
  I.waitForText('My calendars', 5, '.folder-tree')
  I.click('My calendars')

  I.waitForVisible(locate('.folder-label').withText('Calendar A'))
  I.click(locate('.color-label').inside('~My calendars').at(1).as('Checkbox: Default folder'))
  I.click(locate('.color-label').inside('~My calendars').at(2).as('Checkbox: Calendar A'))
  I.click(locate('.color-label').inside('~My calendars').at(3).as('Checkbox: Calendar B'))
  I.click(locate('.color-label').inside('~My calendars').at(3).as('Checkbox: Calendar B'))
  I.click('~Close window')
  I.waitForVisible('#calendar-tree-mobile .window-container.closed.complete')

  I.waitForText('Appointment A', 5, '.month-container')
  I.dontSee('Appointment B', '.month-container')
  I.dontSee('Appointment 0', '.month-container')

  I.click('.today.day')
  I.waitForText('Appointment A', 5, '.weekview-container')
  I.dontSee('Appointment B', '.weekview-container')
  I.dontSee('Appointment 0', '.weekview-container')
})

Scenario('Navigating closes opened notification area @mobile', async ({ I }) => {
  await I.login('app=io.ox/calendar')

  // when using back navigation
  I.waitForElement('~There are no notifications')
  I.click('~There are no notifications')
  I.waitForVisible('.io-ox-notifications')
  I.forceClick('#io-ox-appcontrol .left-action button')
  I.waitForInvisible('.io-ox-notifications.visible')

  I.click('~There are no notifications')
  I.waitForVisible('.io-ox-notifications')

  // when using search
  I.forceClick('~Search')
  I.waitForInvisible('.io-ox-notifications')

  // re-opening should close search area
  I.click('~There are no notifications')
  I.waitForVisible('.io-ox-notifications')
  I.waitForInvisible('.search-container')
})

Scenario('Check appointment reactivity in week:day and month view @mobile', async ({ I, mobileCalendar, calendar }) => {
  const date = DateTime.now().set({ hour: 12, minute: 0 })
  const id = date.toFormat("'slide-month-'yyyy-MM'-day-'yyyy-MM-dd")
  await I.login('app=io.ox/calendar')

  // create appointment and check in week:day
  await mobileCalendar.newAppointment()
  await within(calendar.editWindow, async () => {
    I.fillField('Title', 'Test title')
    await mobileCalendar.setDate('startDate', date)
    I.click('Create')
  })
  I.waitForDetached('.io-ox-calendar-edit-window', 15)
  I.waitForVisible('.weekview-container .appointment')
  I.waitForText('Test title', 5, '.day')

  // navigate to month view and check
  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol .left-action')
  I.waitForVisible('.month-container')
  I.waitForText('Test title', 10, `#${id}.today.day .appointment`)

  // navigate back and edit appointment
  I.click(`#${id}`)
  I.waitForVisible('.weekview-container .appointment')
  I.click('.weekview-container .appointment')
  I.waitForVisible('~Edit')
  I.click('~Edit')
  await within('.calendar-detail', () => {
    I.waitForText('Test title')
  })
  I.waitForVisible('.io-ox-calendar-edit-window')
  I.waitForVisible('.io-ox-calendar-edit-window input[name="summary"]')
  await within(calendar.editWindow, () => {
    I.fillField('Title', 'edited title')
    I.click('Save')
  })
  I.waitForDetached('.io-ox-calendar-edit-window')

  // validate edited appointment in week:day and month view
  I.click('#io-ox-appcontrol .left-action')
  I.waitForVisible('.weekview-container .appointment')
  I.waitForText('edited title', 5, '.weekview-container .day .appointment')
  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol .left-action')
  I.waitForText('edited title', 10, `#${id}.today.day .appointment`)
})

Scenario('Show limited number of appointments in month view @mobile', async ({ I, mobileCalendar, calendar }) => {
  const date = DateTime.now().set({ hour: 12, minute: 0 })
  const id = date.toFormat("'slide-month-'yyyy-MM'-day-'yyyy-MM-dd")
  const appointment = (i) => ({
    summary: i,
    startDate: { value: date },
    endDate: { value: date.plus({ hours: 1 }) }
  })
  // create appointment and check in week:day
  Promise.all([
    await I.haveAppointment(appointment(0)),
    await I.haveAppointment(appointment(1))
  ])
  await I.login('app=io.ox/calendar')

  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol .left-action')
  I.waitForVisible('.month-container')
  I.waitNumberOfVisibleElements(`#${id}.today.day .appointment`, 2)
  I.dontSee('+', `#${id}`)

  await mobileCalendar.newAppointment()
  await within(calendar.editWindow, async () => {
    I.fillField('Title', 'Test')
    await mobileCalendar.setDate('startDate', date.plus({ hours: 4 }))
    I.click('Create')
  })
  I.waitForDetached('.io-ox-calendar-edit-window')

  // show 1 appointment, and +2
  I.waitNumberOfVisibleElements(`#${id}.today.day .appointment`, 1)
  I.see('+2', `#${id}`)
})

Scenario('Appointment spanning midnight is drawn for both days @mobile', async ({ I }) => {
  const date = DateTime.now().set({ hour: 23, minute: 0 })

  await I.haveAppointment({
    summary: 'Multiple day appointment',
    startDate: { value: date },
    endDate: { value: date.plus({ hours: 2 }) }
  })

  await I.login('app=io.ox/calendar')

  I.waitForText('Multiple day appointment', 5, `#slide-day-${date.toISODate()}`)
  I.click('~Next day')
  I.waitForText('Multiple day appointment', 5, `#slide-day-${date.plus({ days: 1 }).toISODate()}`)
})

Scenario('Create a recurring appointment @mobile', async ({ I, calendar, mobileCalendar, dialogs }) => {
  const date = DateTime.local().set({ hour: 12, minute: 0 })

  await I.login('app=io.ox/calendar')

  // create appointment with reccuring rule (daily, repeat 3 times)
  await mobileCalendar.newAppointment()
  await within(calendar.editWindow, () => {
    I.fillField('Title', 'Test title')
    mobileCalendar.setDate('startDate', date)
    I.checkOption('Repeat')
    I.click(`Every ${date.toFormat('cccc')}.`)
  })
  dialogs.waitForVisible()
  I.selectOption('.modal-dialog [name="recurrence_type"]', 'Daily')
  I.selectOption('.modal-dialog [name="until"]', 'After a number of occurrences')
  I.waitForElement('.modal-dialog [name="occurrences"]')
  I.fillField('.modal-dialog [name="occurrences"]', '3')
  dialogs.clickButton('Apply')
  I.waitForDetached('.modal-dialog')
  I.click('Create', calendar.editWindow)
  I.waitForDetached('.io-ox-calendar-edit-window', 15)

  I.waitForElement(`#slide-day-${date.toISODate()} .appointment .recurrence-flag`)
  I.waitForElement(`#slide-day-${date.plus({ days: 1 }).toISODate()} .appointment .recurrence-flag`)
  I.waitForElement(`#slide-day-${date.plus({ days: 2 }).toISODate()} .appointment .recurrence-flag`)
  I.dontSeeElement(`#slide-day-${date.plus({ days: 3 }).toISODate()} .appointment .recurrence-flag`)
})

Scenario('Update and delete a whole recurring appointment series @mobile', async ({ I, dialogs, calendar }) => {
  const date = DateTime.local().startOf('month').set({ hour: 12, minute: 0 })
  const dayId = (day) => day.toFormat("#'slide-day-'yyyy-MM-dd")
  const monthId = date.toFormat("#'slide-month-'yyyy-MM")
  const monthCellId = (day) => day.toFormat("#'slide-month-'yyyy-MM'-day-'yyyy-MM-dd")

  await I.haveAppointment({
    summary: 'Test appointment',
    startDate: { value: date },
    endDate: { value: date.plus({ hours: 2 }) },
    rrule: 'FREQ=DAILY;COUNT=5'
  })

  await I.login('app=io.ox/calendar')

  // check month view
  I.wait(0.5)
  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol')
  I.waitForText('Test appointment', undefined, `${monthId} .appointment .title`)
  I.waitNumberOfVisibleElements(`${monthId} .appointment`, 5)
  I.seeNumberOfVisibleElements(locate(`${monthId} .appointment`).withText('Test appointment'), 5)

  // navigate back to day view, check this and next day, edit series
  I.click(monthCellId(date))
  I.waitForText('Test appointment', 5, dayId(date))
  I.waitForText('Test appointment', 5, dayId(date.plus({ days: 1 })))
  I.click('Test appointment', dayId(date))
  I.waitForElement('~Edit')
  I.click('~Edit')

  // edit series
  dialogs.waitForVisible()
  dialogs.clickButton('Edit series')
  I.waitForVisible('.io-ox-calendar-edit-window.complete')
  I.waitForVisible('.io-ox-calendar-edit-window.complete input[name="summary"]')
  await within(calendar.editWindow, () => {
    I.fillField('Title', 'Test appointment updated')
    I.click('Save')
  })
  I.waitForDetached('.io-ox-calendar-edit-window', 15)
  I.waitForVisible('html.complete')

  // check current and next day for updated appointment title
  I.waitForText('Test appointment updated', 5, '.calendar-detail')
  I.click('Back', '#io-ox-appcontrol')
  I.waitForText('Test appointment updated', 5, `#slide-day-${date.toISODate()}`)
  I.waitForText('Test appointment updated', 5, `#slide-day-${date.plus({ days: 1 }).toISODate()}`)

  // check month view again
  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol')
  I.waitForText('Test appointment updated', undefined, `${monthId} .appointment .title`)
  I.waitNumberOfVisibleElements(`${monthId} .appointment`, 5)
  I.seeNumberOfVisibleElements(locate(`${monthId} .appointment`).withText('Test appointment updated'), 5)

  // delete series (days 2-5)
  I.click(monthCellId(date.plus({ days: 1 })))
  I.wait(0.5)
  I.click('Test appointment updated', dayId(date.plus({ days: 1 })))
  I.waitForElement('~Delete')
  I.click('~Delete')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete all future appointments')
  I.waitForDetached('.modal-dialog')
  I.waitForDetached(`${monthCellId(date.plus({ days: 1 }))} .appointment`)
  I.waitForDetached(`${monthCellId(date.plus({ days: 2 }))} .appointment`)
  I.waitForDetached(`${monthCellId(date.plus({ days: 3 }))} .appointment`)
  I.waitForDetached(`${monthCellId(date.plus({ days: 4 }))} .appointment`)

  // check month view
  I.wait(0.5)
  I.click(date.toFormat('MMMM'), '#io-ox-appcontrol')
  I.waitNumberOfVisibleElements(`${monthId} .appointment`, 1)
})

Scenario('Add a recurrence rule to an existing appointment @mobile', async ({ I, calendar, dialogs }) => {
  const date = DateTime.local().set({ hour: 12, minute: 0 })

  await I.haveAppointment({
    summary: 'Test appointment',
    startDate: { value: date },
    endDate: { value: date.plus({ hours: 2 }) }
  })

  await I.login('app=io.ox/calendar')

  I.waitForText('Test appointment', 5, `#slide-day-${date.toISODate()}`)
  I.click('Test appointment')
  I.waitForElement('~Edit')
  I.click('~Edit')

  I.waitForVisible('.io-ox-calendar-edit-window.complete input[name="summary"]')

  await within(calendar.editWindow, () => {
    I.checkOption('Repeat')
    I.click(`Every ${date.toFormat('cccc')}.`)
  })
  dialogs.waitForVisible()
  I.selectOption('.modal-dialog [name="recurrence_type"]', 'Daily')
  dialogs.clickButton('Apply')
  I.waitForDetached('.modal-dialog')

  I.click('Save', calendar.editWindow)
  I.waitForDetached('.io-ox-calendar-edit-window', 15)
  I.waitForVisible('.appointment .recurrence-flag')
  I.click('~Next day')
  I.waitForDetached(`#slide-day-${date.toISODate()}.swiper-slide-fully-visible`)

  I.waitForText('Test appointment', 5, `#slide-day-${date.plus({ days: 1 }).toISODate()}`)
})

Scenario('Move appointment to next day @mobile', async ({ I, mobileCalendar, calendar }) => {
  const date = DateTime.local().startOf('day').plus({ hours: 8 })
  await I.haveAppointment({
    summary: 'Test appointment',
    startDate: { value: date },
    endDate: { value: date.plus({ hours: 1 }) }
  })

  await I.login('app=io.ox/calendar')

  I.waitForVisible(`#slide-day-${date.toISODate()} .appointment`)
  I.click('Test appointment', `#slide-day-${date.toISODate()}`)
  I.waitForElement('~Edit')
  I.click('~Edit')

  I.waitForVisible('.io-ox-calendar-edit-window.complete')
  await within(calendar.editWindow, () => {
    mobileCalendar.setDate('startDate', date.plus({ days: 1 }))
    I.click('Save')
  })

  I.waitForDetached('.io-ox-calendar-edit-window', 15)
  I.click('Back', '#io-ox-appcontrol')
  I.waitForDetached(`#slide-day-${date.toISODate()} .appointment`)

  I.click('~Next day')
  I.waitForVisible(`#slide-day-${date.plus({ days: 1 }).toISODate()} .appointment`)
})
