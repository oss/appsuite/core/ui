/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

/**
 * Missing test cases:
 * - check number of apps with less than 3 default apps
 */

Feature('General > Mobile App Navigation').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

const activeAppLocator = (app) => locate('#app-navigation button.active .icon-label').withText(app).as(`Active app: ${app}`)
const appAtLocator = (app, at) => locate('#app-navigation button .icon-label').at(at).withText(app).as(`${app} at position #${at}`)

Scenario('Default Quicklaunch: Launch without autostart @mobile @smoketest', async ({ I }) => {
  I.haveSetting({ 'io.ox/core': { autoStart: 'none' } })
  await I.login(undefined, { wait: false })
  I.waitForNetworkTraffic()
  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.dontSeeElement('#app-navigation > li > button.active')
})

Scenario('Default Quicklaunch: Launch with autostart in "Mail"  @mobile @smoketest', async ({ I }) => {
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)
  I.waitForVisible(activeAppLocator('Mail'))
})

Scenario('Default Quicklaunch: Launch with autostart in "Calendar"  @mobile @smoketest', async ({ I }) => {
  await I.login('app=io.ox/calendar')

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)
  I.waitForVisible(activeAppLocator('Calendar'))
})

Scenario('Default Quicklaunch: Launch with autostart in "Address Book"  @mobile @smoketest', async ({ I }) => {
  await I.login('app=io.ox/contacts')

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)
  I.waitForVisible(activeAppLocator('Address Book'))
})

Scenario('Default Quicklaunch: Launch with autostart in "Tasks"  @mobile @smoketest', async ({ I }) => {
  await I.login('app=io.ox/tasks')

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.dontSeeElement('#app-navigation > li > button.active')
  I.dontSeeElement(activeAppLocator('Tasks'))
})

Scenario('Custom Quicklaunch: Drive, Tasks, Portal @mobile @smoketest', async ({ I }) => {
  I.haveSetting('io.ox/core//apps/quickLaunch', 'io.ox/files,io.ox/tasks,io.ox/portal')
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.seeElement(appAtLocator('Drive', 1))
  I.seeElement(appAtLocator('Tasks', 2))
  I.seeElement(appAtLocator('Portal', 3))
})

Scenario('Custom Quicklaunch: None configured @mobile @smoketest', async ({ I }) => {
  I.haveSetting('io.ox/core//apps/quickLaunch', 'none,none,none')
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.seeElement(appAtLocator('Mail', 1))
  I.seeElement(appAtLocator('Calendar', 2))
  I.seeElement(appAtLocator('Address Book', 3))
})

Scenario('Custom Quicklaunch: Drive configured @mobile @smoketest', async ({ I }) => {
  I.haveSetting('io.ox/core//apps/quickLaunch', 'io.ox/files')
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.seeElement(appAtLocator('Drive', 1))
  I.seeElement(appAtLocator('Mail', 2))
  I.seeElement(appAtLocator('Calendar', 3))
})

Scenario('Custom Quicklaunch: Drive, Portal configured @mobile @smoketest', async ({ I }) => {
  I.haveSetting('io.ox/core//apps/quickLaunch', 'io.ox/files, io.ox/portal')
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li', 4)
  I.seeElement(appAtLocator('Drive', 1))
  I.seeElement(appAtLocator('Portal', 2))
  I.seeElement(appAtLocator('Mail', 3))
})

Scenario('App Navigation through bottom bar @mobile', async ({ I }) => {
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)

  // navigate between apps, check .active class
  I.waitForVisible(appAtLocator('Calendar', 2))
  I.click(appAtLocator('Calendar', 2))
  await I.waitForApp()
  I.waitForVisible('.io-ox-calendar-window')
  I.waitForVisible(activeAppLocator('Calendar'))
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)

  // navigate between apps, check .active class
  I.waitForVisible('#app-navigation')
  I.waitForVisible(appAtLocator('Address Book', 3))
  I.click(appAtLocator('Address Book', 3))
  await I.waitForApp()
  I.waitForVisible('.io-ox-contacts-window')
  I.waitForVisible(activeAppLocator('Address Book'))
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)
})

Scenario('App Navigation through app launcher @mobile', async ({ I }) => {
  await I.login()

  I.waitForVisible('#app-navigation')
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)

  // app launcher: navigate so that none of the bottom apps is opened
  I.click('#io-ox-launcher button')
  I.waitForElement('.launcher-dropdown.complete')
  I.click('Tasks', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForVisible('.io-ox-tasks-window')
  I.dontSeeElement('#app-navigation > li > button.active')

  // app launcher: back to mail, .active state on mail
  I.click('#io-ox-launcher button')
  I.waitForElement('.launcher-dropdown.complete')
  I.click('Mail', '.launcher-dropdown')
  await I.waitForApp()
  I.waitForVisible('.io-ox-mail-window')
  I.waitForVisible(activeAppLocator('Mail'))
  I.waitNumberOfVisibleElements('#app-navigation > li > button.active', 1)
})
