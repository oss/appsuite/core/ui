/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('Mobile > Settings').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Open settings manually @mobile', async ({ I }) => {
  await I.login()

  I.click('~Navigate to:')
  I.waitForVisible('.launcher-dropdown.complete')
  I.click('Settings', '.menu-actions')
  await I.waitForApp()
  I.waitForElement('.folder-tree.complete')
})

Scenario('Basic navigation @mobile', async ({ I, settings }) => {
  await I.login('app=io.ox/mail&settings=virtual/settings/')

  I.waitForText('General')
  I.waitForElement('.folder-tree.complete')
  I.wait(1)
  I.click('General')
  I.waitForVisible(locate('h1').withText('General').as('"General"'))
  I.waitForElement('.settings-detail-pane .expandable-section[open], .settings-detail-pane .settings-section')

  I.click('Back', '.settings-navbar')
  I.waitForText('Settings', undefined, '.page.current h1')
  I.click('~Close', '.settings-navbar')
  I.waitForDetached('modal.mobile-dialog.io-ox-settings-main')
})

Scenario('Searching settings and selecting search results @mobile', async ({ I, users }) => {
  await I.login('settings=virtual/settings/')

  // I.fillField('Search for setting', 'langu') doesn't work with the suggestions
  I.appendField('Search', 'l')
  I.appendField('Search', 'a')
  I.appendField('Search', 'n')
  I.appendField('Search', 'g')
  I.appendField('Search', 'u')
  I.waitForText('Language')

  I.seeNumberOfElements('.search-results li', 2)
  I.click('Language')

  I.waitForText('General')
  I.waitForElement({ css: '[data-section="io.ox/settings/general/language"]' })
})
