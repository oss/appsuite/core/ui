/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('Mobile > Mail > Detail View').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await Promise.all([users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('Delete only mail @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  const trashFolderLocator = locate('.folder-label').withText('Trash').as('Trash folder')
  await I.haveMail({ from: sender, to: sender, subject: 'Delete me!' })

  await I.login('app=io.ox/mail')

  mobileMail.selectMail('Delete me!')
  I.waitForInvisible('.list-view.complete')
  I.click('~Delete')
  I.waitForInvisible('.mail-detail-pane')
  I.waitForText('This folder is empty')
  I.dontSee('Delete me!', '.io-ox-mail-window .list-view.mail-item')

  I.click('Folders', '#io-ox-appcontrol')
  I.waitForVisible('.folder-tree')
  I.waitForVisible(trashFolderLocator)
  // wait, otherwise click does nothing
  I.wait(1)
  I.click(trashFolderLocator)
  I.waitForText('Delete me!')
})

Scenario('Delete one mail @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 1!' }),
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 2!' })
  ])

  await I.login('app=io.ox/mail')

  mobileMail.selectMail('Delete me 1!')
  I.waitForInvisible('.list-view.complete')
  I.click('~Delete')
  I.waitForInvisible('.mail-detail-pane')
  I.waitForVisible('.list-view.complete')

  I.dontSee('Delete me 1!', '.io-ox-mail-window .list-view.mail-item')
  I.see('Delete me 2!', '.io-ox-mail-window .list-view.mail-item')
})

Scenario('Individual font sizes are not applied on @mobile', async ({ I, mobileMail, settings }) => {
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login('app=io.ox/mail')

  // check initial value
  await mobileMail.selectMail('Plain text')
  I.waitForInvisible('.list-view.complete')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    // check for not applied font size class
    I.waitForElement('.mail-detail-content')
    I.dontSeeElement('.mail-detail-content.text-very-small')

    // check mobile default size specified via $sm-mail-detail-font-size
    const basefontSize = await I.grabCssPropertyFrom('.mail-detail-content', 'font-size')
    assert.equal(basefontSize, '15px')
  })

  // check that setting block is not drawn
  await settings.open('Mail', 'Reading')
  I.waitForText('Mark as read')
  I.dontSeeElement('#fontSize')
})
