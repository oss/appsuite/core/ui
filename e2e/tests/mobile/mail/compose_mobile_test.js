/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('Mobile > Mail > Compose').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => {
  await Promise.all([users.create(), users.create(), users.create(), users.create()])
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Send mail @mobile', async ({ I, users, mail, mobileMail }) => {
  await I.login('app=io.ox/mail')

  mobileMail.newMail()
  await within(mail.composeWindow, () => {
    I.fillField(mail.to, users[0].get('primaryEmail'))
    I.fillField('Subject', 'This is a mail')
  })
  await within({ frame: mail.editorIframe }, () => {
    I.appendField('body', 'Some mail text')
  })
  mobileMail.send()

  I.waitForInvisible('.abs.notification', 30)
  I.waitForText('This is a mail')
  I.waitForText('Some mail text')
  I.waitForText(`${users[0].get('given_name')} ${users[0].get('sur_name')}`)
})

Scenario('Send mail to CC recipients @mobile', async ({ I, users, mail, mobileMail }) => {
  await I.login('app=io.ox/mail')

  mobileMail.newMail()
  await within(mail.composeWindow, () => {
    I.fillField(mail.to, users[1].get('primaryEmail'))
    I.click('[data-action="add"]')
    I.fillField('CC', users[2].get('primaryEmail'))
    I.waitForVisible('.cc .tt-dropdown-menu .tt-suggestion')
    I.click('.cc .tt-dropdown-menu .tt-suggestion')
    I.fillField('CC', users[3].get('primaryEmail'))
    I.waitForVisible('.cc .tt-dropdown-menu .tt-suggestion')
    I.click('.cc .tt-dropdown-menu .tt-suggestion')
    I.fillField('Subject', 'Test subject')
  })
  await within({ frame: mail.editorIframe }, () => {
    I.appendField('body', 'Some mail text')
  })
  mobileMail.send()
  await I.logout()

  // login and check with user1
  await I.login('app=io.ox/mail', { user: users[1] })

  mobileMail.selectMail('Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`)
  I.waitForDetached('.io-ox-core-animation')
  I.click('.toggle-details')
  I.waitForText(`${users[2].get('given_name')} ${users[2].get('sur_name')}`)
  I.waitForText(`${users[3].get('given_name')} ${users[3].get('sur_name')}`)
  I.waitForText('Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  // login and check with user2
  await I.login('app=io.ox/mail', { user: users[2] })

  mobileMail.selectMail('Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`)
  I.waitForDetached('.io-ox-core-animation')
  I.click('.toggle-details')
  I.waitForText(`${users[2].get('given_name')} ${users[2].get('sur_name')}`)
  I.waitForText(`${users[3].get('given_name')} ${users[3].get('sur_name')}`)
  I.waitForText('Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  // login and check with user3
  await I.login('app=io.ox/mail', { user: users[3] })

  mobileMail.selectMail('Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`)
  I.waitForDetached('.io-ox-core-animation')
  I.click('.toggle-details')
  I.waitForText(`${users[2].get('given_name')} ${users[2].get('sur_name')}`)
  I.waitForText(`${users[3].get('given_name')} ${users[3].get('sur_name')}`)
  I.waitForText('Test subject', undefined, '.mail-detail-pane .subject')
})

Scenario('Send mail to BCC recipients @mobile', async ({ I, users, mail, mobileMail, autocomplete }) => {
  await I.login('app=io.ox/mail')

  mobileMail.newMail()
  await within(mail.composeWindow, () => {
    I.fillField(mail.to, users[1].get('primaryEmail'))
    autocomplete.selectFirst()
    I.waitForInvisible('.autocomplete')
    I.click('~Show all')
    I.fillField('BCC', users[2].get('primaryEmail'))
    I.waitForVisible('.bcc .tt-dropdown-menu .tt-suggestion')
    I.click('.bcc .tt-dropdown-menu .tt-suggestion')
    I.waitForInvisible('.autocomplete')
    I.fillField('BCC', users[3].get('primaryEmail'))
    I.waitForVisible('.bcc .tt-dropdown-menu .tt-suggestion')
    I.click('.bcc .tt-dropdown-menu .tt-suggestion')
    I.waitForInvisible('.autocomplete')
    I.fillField('Subject', 'Test subject')
  })
  await within({ frame: mail.editorIframe }, () => {
    I.appendField('body', 'Some mail text')
  })
  mobileMail.send()
  await I.logout()

  // login and check with second user
  await I.login('app=io.ox/mail', { user: users[1] })

  mobileMail.selectMail('Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`)
  I.dontSee(`${users[2].get('given_name')} ${users[2].get('sur_name')}`)
  I.dontSee(`${users[3].get('given_name')} ${users[3].get('sur_name')}`)
  I.dontSee(`${users[2].get('primaryEmail')}`)
  I.dontSee(`${users[3].get('primaryEmail')}`)
  I.waitForText('Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  // login and check with third user
  await I.login('app=io.ox/mail', { user: users[2] })

  mobileMail.selectMail('Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`)
  I.dontSee(`${users[2].get('given_name')} ${users[2].get('sur_name')}`)
  I.dontSee(`${users[3].get('given_name')} ${users[3].get('sur_name')}`)
  I.dontSee(`${users[2].get('primaryEmail')}`)
  I.dontSee(`${users[3].get('primaryEmail')}`)
  I.waitForText('Test subject', undefined, '.mail-detail-pane .subject')
  await I.logout()

  // login and check with fourth user
  await I.login('app=io.ox/mail', { user: users[3] })

  mobileMail.selectMail('Test subject')
  I.waitForText(users[0].get('primaryEmail'), undefined, '.detail-view-header')
  I.waitForText(`${users[1].get('given_name')} ${users[1].get('sur_name')}`)
  I.dontSee(`${users[2].get('given_name')} ${users[2].get('sur_name')}`)
  I.dontSee(`${users[3].get('given_name')} ${users[3].get('sur_name')}`)
  I.dontSee(`${users[2].get('primaryEmail')}`)
  I.dontSee(`${users[3].get('primaryEmail')}`)
  I.waitForText('Test subject', undefined, '.mail-detail-pane .subject')
})

Scenario('Compose and discard empty mail @mobile', async ({ I, users, dialogs, mail, mobileMail }) => {
  await I.login('app=io.ox/mail')

  mobileMail.newMail()
  await within(mail.composeWindow, () => {
    // show modal dialog when dirty, otherwise don't
    I.fillField('Subject', 'This is a mail')
    I.click('Discard')
  })
  dialogs.waitForVisible()
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal-dialog')
  await within(mail.composeWindow, () => {
    I.clearField('Subject')
    I.click('Discard')
  })

  I.waitForText('This folder is empty')
})

Scenario('Compose and discard draft @mobile', async ({ I, users, dialogs, mail, mobileMail }) => {
  await I.login('app=io.ox/mail')

  mobileMail.newMail()

  await within(mail.composeWindow, () => {
    I.fillField('Subject', 'This is a mail')
    I.click('Discard')
  })
  dialogs.waitForVisible()
  dialogs.clickButton('Delete draft')

  I.waitForText('This folder is empty')
})

Scenario('Compose and save draft @mobile', async ({ I, users, dialogs, mail, mobileMail }) => {
  const draftFolderLocator = locate('.folder-label').withText('Drafts').as('Drafts folder')
  await I.login('app=io.ox/mail')

  mobileMail.newMail()

  await within(mail.composeWindow, () => {
    I.fillField('Subject', 'A draft')
    I.click('Discard')
  })
  dialogs.waitForVisible()
  dialogs.clickButton('Save draft')
  I.waitForDetached('.modal-dialog')
  I.waitForText('This folder is empty')

  I.waitForDetached('.io-ox-mail-compose-window.complete')
  I.click('Folders', '#io-ox-appcontrol')
  I.waitForVisible('.folder-tree')
  I.waitForVisible(draftFolderLocator)
  I.waitForText('1', undefined, '~Drafts, 1 total.')
  I.click(draftFolderLocator)
  I.waitForText('A draft')
  // Note: This test fails, because of an actual bug. The message count is not updated correctly.
  I.waitForText('1 message', 10)
})

Scenario('Flag an e-Mail with a color flag @mobile', async ({ I, mobileMail, users }) => {
  await Promise.all([
    I.haveSetting('io.ox/core//autoStart', 'none'),
    I.haveMail({
      attachments: [{
        content: 'Lorem ipsum',
        content_type: 'text/plain',
        disp: 'inline'
      }],
      flags: 0,
      from: [['Icke', users[0].get('email1')]],
      subject: 'Flag mails',
      to: [['Icke', users[0].get('email1')]]
    })
  ])

  await I.login(undefined, { wait: false })
  I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    mailSettings.set('features/flagging', { mode: 'color' })
    mailSettings.flagByColor = true
  })
  I.waitForInvisible('#background-loader')
  I.openApp('Mail')
  await I.waitForApp()
  mobileMail.selectMail('Flag mails')

  // set
  I.click('~Actions')
  I.clickDropdown('Set color')
  I.clickDropdown('Yellow')
  I.waitForVisible('.mail-detail-pane .list-item .flags.color-flag.colored.flag_10')

  // check in list view
  I.click('Inbox', '#io-ox-appcontrol .left-action')
  I.waitForVisible('.list-view .list-item .color-flag.flag_10')
  mobileMail.selectMail('Flag mails')

  // go back to detail view and unset
  I.click('~Actions')
  I.clickDropdown('Set color')
  I.clickDropdown('None')
  I.waitForDetached('.list-view .list-item .color-flag')
  I.dontSeeElement('.mail-detail-pane .list-item .flags.color-flag.colored')
})

Scenario('Send drive-mail with an attachment above the threshold @mobile', async ({ I, users, mail, mobileMail, dialogs }) => {
  await I.haveSetting('io.ox/core//autoStart', 'none')
  await I.login(undefined, { wait: false })

  I.executeScript(async function () {
    const { settings: mailSettings } = await import(String(new URL('io.ox/mail/settings.js', location.href)))
    const { settings } = await import(String(new URL('io.ox/core/settings.js', location.href)))
    mailSettings.set('compose/shareAttachments/threshold', 1)
    const { default: filesApi } = await import(String(new URL('io.ox/files/api.js', location.href)))
    const blob = new window.Blob(['someBlob'], { type: 'text/plain' })
    await filesApi.upload({ folder: settings.get('folder/infostore'), file: blob, filename: 'Principia.txt', params: {} }
    )
  })
  I.waitForInvisible('#background-loader')
  I.openApp('Mail')
  await I.waitForApp()

  mobileMail.newMail()
  await within(mail.composeWindow, () => I.click('~Add attachments'))

  I.clickDropdown('Add from Drive')
  dialogs.waitForVisible()
  dialogs.clickButton('Add')
  I.waitForDetached('.modal-dialog')

  I.waitForText('Attachment file size too large.', 10, '.io-ox-alert')
  await within(mail.composeWindow, () => {
    I.waitForText('Use Drive Mail')
    I.seeCheckboxIsChecked('.share-attachments input[type="checkbox"]')
  })
})
