/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

Feature('Mobile > Mail > List View').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await Promise.all([users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('Delete the only mail @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await I.haveMail({ from: sender, to: sender, subject: 'Delete me!' })

  await I.login('app=io.ox/mail')

  I.waitForText('Select', undefined, '.edit')
  I.click('Select', '.edit')
  I.waitForVisible('.list-item-checkmark')
  I.click('.list-item-checkmark')
  I.see('1 selected')
  I.click('~Delete')
  I.waitForText('This folder is empty')
  I.dontSee('Delete me!', '.io-ox-mail-window .list-view.mail-item')
})

Scenario('Delete one mail @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]

  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 1!' }),
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 2!' })
  ])

  await I.login('app=io.ox/mail')

  I.waitForText('2 messages')
  I.waitForText('Select', undefined, '.edit')
  I.click('Select', '.edit')
  I.waitForVisible('.list-item-checkmark')
  I.click('[data-cid="default0/INBOX.1"] .list-item-checkmark')
  I.waitForText('1 selected')
  I.click('~Delete')
  I.waitForText('1 message')
  I.dontSee('Delete me 1!', '.io-ox-mail-window .list-view.mail-item')
  I.see('Delete me 2!', '.io-ox-mail-window .list-view.mail-item')
})

Scenario('Delete all mails @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 1!' }),
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 2!' })
  ])

  await I.login('app=io.ox/mail')

  I.waitForText('Select', undefined, '.edit')
  I.waitForText('2 messages')
  I.click('Select', '.edit')
  I.waitForVisible('.list-item-checkmark')
  I.click(locate('.list-item-checkmark').at(1))
  I.click(locate('.list-item-checkmark').at(2))
  I.see('2 selected')
  I.click('~Delete')
  I.waitForText('This folder is empty')
  I.waitForText('0 messages')
  I.dontSee('Delete me 1!', '.io-ox-mail-window .list-view.mail-item')
  I.dontSee('Delete me 2!', '.io-ox-mail-window .list-view.mail-item')
})

Scenario('Move all mails to another folder @mobile', async ({ I, users, mobileMail, dialogs }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 1!' }),
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 2!' })
  ])
  const draftFolder = locate('.folder-label').withText('Drafts').as('Drafts folder')
  const inboxFolder = locate('.folder-label').withText('Inbox').as('Inbox folder')

  await I.login('app=io.ox/mail')

  // select mails
  I.waitForVisible('~Email actions')
  I.waitForText('Select', 10, '.edit')
  I.waitForText('2 messages')
  I.click('Select', '.edit')
  I.waitForVisible('.list-item-checkmark')
  I.click(locate('.list-item-checkmark').at(1))
  I.click(locate('.list-item-checkmark').at(2))
  I.see('2 selected')

  // move mails to drafts
  I.click('~Actions')
  I.clickDropdown('Move')
  dialogs.waitForVisible()
  I.waitForText('Drafts', undefined, dialogs.body)
  I.click('Drafts', dialogs.body)
  dialogs.clickButton('Move')
  I.waitForDetached(dialogs.main)
  I.waitForText('0 messages')
  I.waitForInvisible('~Email actions')

  // navigate to drafts folder
  I.click('Folders')
  I.waitForDetached('.io-ox-core-animation')
  I.waitForElement(draftFolder)
  I.click(draftFolder)
  I.waitNumberOfVisibleElements('.list-view .list-item', 2)
  I.waitNumberOfVisibleElements('.seen-unseen-indicator', 2)
  I.waitForText('2 messages')
  I.waitForText('Select')
  I.waitForVisible('~Email actions')

  // navigate back and check available ui elements
  I.click('Folders')
  I.waitForDetached('.io-ox-core-animation')
  I.waitForElement(inboxFolder)
  I.click(inboxFolder)
  I.waitForText('0 messages')
  I.waitForText('This folder is empty')
  I.waitForInvisible('~Email actions')
  I.dontSeeElement('.list-view .list-item')
  I.dontSeeElement('.seen-unseen-indicator')
  I.dontSee('Select')
})

Scenario('Mark all messages as read @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 1!' }),
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 2!' })
  ])

  await I.login('app=io.ox/mail')

  I.waitForText('Updated just now')

  I.waitNumberOfVisibleElements('.seen-unseen-indicator', 2)
  I.waitForText('Select', undefined, '.edit')
  I.click('Select', '.edit')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 2)
  I.click('~Email actions')

  I.waitForText('Mark all messages as read')
  I.click('Mark all messages as read')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 0)
})

Scenario('Mark selected message as read / unread @mobile', async ({ I, users, mobileMail }) => {
  const sender = [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  await Promise.all([
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 1!' }),
    I.haveMail({ from: sender, to: sender, subject: 'Delete me 2!' })
  ])

  await I.login('app=io.ox/mail')

  I.waitNumberOfVisibleElements('.seen-unseen-indicator', 2)
  I.waitForText('Select', undefined, '.edit')
  I.click('Select', '.edit')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 2)
  I.waitForVisible('.list-item-checkmark')
  I.click(locate('.list-item-checkmark').at(2))
  I.see('1 selected')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 2)

  I.click('~Mark as read')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 1)

  I.click('~Mark as unread')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 2)

  I.click(locate('.list-item-checkmark').at(1).as('First checkmark'))
  I.see('2 selected')

  I.click('~Mark as read')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 0)

  I.click('~Mark as unread')
  I.seeNumberOfVisibleElements('.seen-unseen-indicator', 2)
})

Scenario('Empty folder @mobile', async ({ I, users, mobileMail }) => {
  await I.login('app=io.ox/mail')

  I.waitForVisible('.current .app-header-container .right-side')
  I.dontSee('Select', '.app-header-container .right-side')
  I.see('0 messages', '.current .app-header-container .context')

  I.waitForText('Updated just now')
  I.waitForInvisible('~Email actions')
})

Scenario('Reactive mail counter when switching folders @mobile', async ({ I, mobileMail }) => {
  const defaultFolder = await I.grabDefaultFolder('mail')
  I.haveSetting('io.ox/mail//folderview/open/small', ['default0', 'default0/INBOX', 'virtual/standard'])

  await Promise.all([
    I.haveFolder({ title: 'Folder1', module: 'mail', parent: defaultFolder }),
    I.haveFolder({ title: 'Folder2', module: 'mail', parent: defaultFolder })
  ])

  await Promise.all([
    I.haveMail({ folder: defaultFolder + '/Folder1', path: 'media/mails/c44997.eml' }),
    I.haveMail({ folder: defaultFolder + '/Folder1', path: 'media/mails/c44997.eml' }),
    I.haveMail({ folder: defaultFolder + '/Folder2', path: 'media/mails/c44997.eml' }),
    I.haveMail({ folder: defaultFolder + '/Folder2', path: 'media/mails/c44997.eml' })
  ])

  await I.login('app=io.ox/mail')

  I.waitForText('Folders')
  I.click('Folders')
  // prevents clicking during animation
  I.waitForText('Folder1')
  I.click('~Folder1')

  I.waitForText('2 messages')
  I.waitForText('Testsubject')
  mobileMail.selectMail('Testsubject')
  I.waitForElement('button[data-action="io.ox/mail/actions/delete"]')
  I.click('button[data-action="io.ox/mail/actions/delete"]')

  I.waitForText('1 message')
  I.waitForText('Testsubject')
  mobileMail.selectMail('Testsubject')
  I.waitForElement('button[data-action="io.ox/mail/actions/delete"]')
  I.click('button[data-action="io.ox/mail/actions/delete"]')

  I.waitForText('0 messages')
  I.waitForText('Folders')

  I.click('Folders')
  I.waitForText('Folder2')
  I.click('~Folder2')

  I.waitForText('2 messages')
  I.waitForText('Testsubject')
  mobileMail.selectMail('Testsubject')
  I.waitForElement('button[data-action="io.ox/mail/actions/delete"]')
  I.click('button[data-action="io.ox/mail/actions/delete"]')

  I.waitForText('1 message')
  I.waitForText('Testsubject')
  mobileMail.selectMail('Testsubject')
  I.waitForElement('button[data-action="io.ox/mail/actions/delete"]')
  I.click('button[data-action="io.ox/mail/actions/delete"]')

  I.waitForText('0 messages')
  I.waitForText('Folders')
})
