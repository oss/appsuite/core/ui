/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')

const util = require('@open-xchange/appsuite-codeceptjs/src/util.js')
const rootUrl = util.getURLRoot()

// This test is run in chromium despite the mobile tag, because this specific tests does not work headless in webkit
Feature('Mobile > Mail > Scheduled Mail').config({ browser: 'chromium', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Scheduled mail roundtrip @mobile', async ({ I, mail, mobileMail, users }) => {
  await Promise.all([
    I.haveSetting({ 'io.ox/core': { features: { scheduleSend: true } } }),
    I.haveSetting('io.ox/mail//autoSaveAfter', 1000)
  ])

  await I.login()

  // schedule mail with time picker
  mobileMail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Scheduled mail roundtrip test')
  await within({ frame: '.io-ox-mail-compose-window .editor iframe' }, async () => {
    I.fillField('body', 'Scheduled mail roundtrip test')
  })
  I.wait(3) // autosave
  I.click('~Mail compose actions')
  I.clickDropdown('Schedule send')
  // date/time picker with validation
  I.clickDropdown('Pick a date and time …')
  I.waitForElement('.modal-dialog .modal-body input[name="date"]')
  I.fillField('Date', '01.01.2000')
  I.click('Schedule email')
  I.see('Date must be in the future.')
  I.wait(0.5)
  await within('.modal-dialog .modal-body', () => {
    I.fillField('Date', '01.01.2100')
    I.fillField('Time', '14:00')
  })
  I.click('Schedule email')
  I.waitForText('Sending planned for', 10, '#io-ox-message-container')
  I.seeTextEquals(
    'Sending planned for 01/01/2100 2:00 PM',
    '#io-ox-message-container .state'
  )
  I.seeTextEquals(
    'Scheduled mail roundtrip test',
    '#io-ox-message-container .mail-send-progress-subject'
  )
  I.click('Close', '#io-ox-message-container')

  // locate mail in `Scheduled` folder
  I.waitForText('Folders')
  I.click('Folders')
  I.waitForText('Scheduled', 10, '.tree-container')
  I.click(locate('.folder-label').withText('Scheduled').as('Scheduled'))
  I.wait(1) // animation
  I.seeTextEquals('01/01/2100 2:00 PM', '.page.current .date')
  mobileMail.selectMail('Scheduled mail roundtrip test')
  I.waitForText('This email is scheduled to be sent on')
  I.seeTextEquals(
    'This email is scheduled to be sent on 01/01/2100 2:00 PM.',
    '.notification-banner span:nth-child(2)'
  )
  I.seeTextEquals('Cancel send', '.notification-banner button')

  // Begin editing mail
  I.click('~Edit draft')
  I.waitForElement('.modal-dialog')
  I.waitForText('Schedule will be discarded', undefined, '.modal-dialog .modal-header')
  I.waitForText('Editing this email will discard the sending schedule.', undefined, '.modal-dialog .modal-body')
  I.click('Edit', '.modal-dialog .modal-footer')
  I.wait(1) // wait for the .io-ox-mail-compose-window context
  I.click('~Close window')
  I.wait(1) // animation

  // Check that editing the mail moved it to drafts
  I.dontSee('Scheduled mail roundtrip test')
  I.amOnPage(rootUrl + '#!!&app=io.ox/mail&folder=default0/Drafts')
  I.refreshPage()
  await I.waitForApp()
  I.waitForElement('.list-view .date:not(.scheduled)') // right date?
  I.click('Scheduled mail roundtrip test')
  I.wait(1) // animation

  // Edit and reschedule mail
  await within({ frame: '.io-ox-mail-compose-window .editor iframe' }, async () => {
    I.appendField('body', ' EDITED')
  })
  I.wait(3) // autosave
  I.click('~Mail compose actions')
  I.clickDropdown('Schedule send')
  I.wait(1) // animation of second dropdown
  I.clickDropdown('Send Monday morning')
  I.click('Close', '#io-ox-message-container')
  I.waitForText('0 messages', 10)
  I.dontSee('Scheduled mail roundtrip test')
  I.amOnPage(rootUrl + '#!!&app=io.ox/mail&folder=default0/Scheduled')
  I.refreshPage()
  await I.waitForApp()
  I.waitForText('Scheduled mail roundtrip test', undefined, '.text-preview')
  mobileMail.selectMail('Scheduled mail roundtrip test')

  // Cancel send
  I.click('Cancel send')
  I.waitForText('Sending has been canceled. The email has been moved to the drafts folder.')
  I.waitForText('0 messages', 10)
  I.wait(1) // otherwise the Text is still in the DOM...
  I.dontSee('Scheduled mail roundtrip test', '.page.current')
  I.amOnPage(rootUrl + '#!!&app=io.ox/mail&folder=default0/Drafts')
  I.refreshPage()
  await I.waitForApp()
  I.waitForElement('.list-view .date:not(.scheduled)') // right date?
  I.waitForText('Scheduled mail roundtrip test')
})
