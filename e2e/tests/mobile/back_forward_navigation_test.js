/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { devices } = require('playwright-core')
Feature('Mobile > Back/Forward Navigation').config({ browser: 'webkit', emulate: devices['iPhone 14 Pro'] })

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('App switching @mobile', async ({ I }) => {
  // fill history
  await I.login('app=io.ox/mail')

  I.click('Calendar')
  await I.waitForApp()
  I.click('Address Book')
  await I.waitForApp()

  async function getAppId () {
    return await I.executeScript(async (app) => {
      const { default: ox } = await import(String(new URL('ox.js', location.href)))
      return ox.ui.App.getCurrentApp().id
    })
  }
  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  expect(await getAppId()).to.equal('io.ox/calendar')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  expect(await getAppId()).to.equal('io.ox/mail')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  expect(await getAppId()).to.equal('io.ox/calendar')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  expect(await getAppId()).to.equal('io.ox/mail')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  expect(await getAppId()).to.equal('io.ox/calendar')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  expect(await getAppId()).to.equal('io.ox/contacts')
})

Scenario('Notification area toggle @mobile', async ({ I }) => {
  await I.login('app=io.ox/calendar')

  // fill history
  I.waitForElement('~There are no notifications')
  I.click('~There are no notifications')
  I.waitForElement('.io-ox-notifications.visible')

  I.forceClick('~There are no notifications')
  I.waitForElement('.io-ox-notifications:not(.visible)')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-notifications.visible')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-notifications:not(.visible)')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-notifications.visible')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-notifications:not(.visible)')
})

Scenario('Settings toggle @mobile', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.click('#io-ox-launcher button')
  I.waitForElement('.launcher-dropdown.complete')
  I.waitForText('Settings')
  I.click('Settings')
  I.waitForElement('.io-ox-settings-main')
  I.waitForElement('~Close')
  I.click('~Close', '.io-ox-settings-main')
  I.waitForInvisible('.io-ox-settings-main')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-settings-main')
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())

  I.waitForVisible('.dropdown.open.launcher')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForInvisible('.dropdown.open.launcher')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForInvisible('.io-ox-settings-main')
})

Scenario('Settings toggle with modal dialogs @mobile', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.click('#io-ox-launcher button')
  I.waitForElement('.launcher-dropdown.complete')
  I.waitForText('Settings')
  I.click('Settings')
  I.waitForElement('.io-ox-settings-main')
  // fill history (bad style but it just doesn't work without wait, probably because of animations)
  I.waitForText('General')
  I.waitForElement('.folder-tree.complete')
  I.wait(1)
  I.click('General')
  I.waitForText('General', undefined, '.settings-header')
  I.waitForElement('~Close')
  I.click('~Close', '.io-ox-settings-main')
  I.waitForInvisible('.io-ox-settings-main')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.wait(1)
  I.waitForElement('.io-ox-settings-main')

  // disrupt navigation by opening a modal dialog
  I.waitForText('Language & Time zone')
  I.click('details[data-section="io.ox/settings/general/language"]')
  I.waitForText('Customize date and time formats …')
  I.click('Customize date and time formats …')
  I.waitForText('Regional settings', 5, '.modal-title')

  // dialog closes, settings should still be open
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.wait(1)
  I.waitForElement('.io-ox-settings-main:not(.modal-paused)')
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/detailView"]')
  I.dontSee('Regional settings', '.modal-title')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.wait(1)
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/folderTree"]')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.wait(1)
  I.waitForVisible('.dropdown.open.launcher')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.wait(1)
  I.waitForElement('.io-ox-settings-main')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.wait(1)
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/detailView"]')

  // wait for animation
  I.wait(1)
  // disrupt navigation by opening a modal dialog
  I.waitForText('Language & Time zone')
  I.click('details[data-section="io.ox/settings/general/language"]')
  I.waitForText('Customize date and time formats …')
  I.click('Customize date and time formats …')
  I.waitForText('Regional settings', 5, '.modal-title')

  // dialog closes, settings should still be open
  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.wait(1)
  I.waitForElement('.io-ox-settings-main:not(.modal-paused)')
  I.dontSee('Regional settings', '.modal-title')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.wait(1)
  I.waitForInvisible('.io-ox-settings-main')
})

// TODO: add or remove waiting for "Search results"
Scenario('Search navigation @mobile', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.click('#io-ox-topbar-mobile-search-container button')
  I.waitForVisible('.search-field')
  I.fillField('.search-field', 'Meine tolle Suche')
  I.click('~Search', '.search-view')
  // I.waitForText('Search results')
  I.waitForText('No search results')
  I.click('#io-ox-topbar-mobile-search-container button')
  I.waitForText('Inbox')
  I.waitForText('This folder is empty')
  I.dontSeeElement('.smartphone-search-container')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  // I.waitForText('Search results')
  I.seeInField('.search-field', 'Meine tolle Suche')
  I.waitForText('No search results')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForText('Inbox')
  I.waitForText('This folder is empty')
  I.dontSeeElement('.smartphone-search-container')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  // I.waitForText('Search results')
  I.seeInField('.search-field', 'Meine tolle Suche')
  I.waitForText('No search results')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForText('Inbox')
  I.waitForText('This folder is empty')
  I.dontSeeElement('.smartphone-search-container')
})

Scenario('Mail navigation @mobile', async ({ I, users, mobileMail }) => {
  await I.haveMail({ subject: 'Monday', from: users[0], to: users[0], attachments: [{ content: 'Received on a monday.' }] })
  await I.login('app=io.ox/mail')

  // fill history
  mobileMail.selectMail('Monday')
  I.waitForText('Received on a monday.')
  I.click('Inbox', '#io-ox-appcontrol')
  I.waitForText('Folders')
  I.click('Folders')
  I.click('[data-id="default0/INBOX"] div')
  I.waitForText('Folders')
  I.waitForText('Monday', 10, '.list-item')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="io.ox/mail/folderTree"]')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="io.ox/mail/listView"]')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="io.ox/mail/folderTree"]')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="io.ox/mail/listView"]')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="io.ox/mail/detailView"]')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="io.ox/mail/listView"]')
})

Scenario('Settings navigation @mobile', async ({ I, users }) => {
  await I.login('app=io.ox/mail&settings=virtual/settings/')

  // fill history (bad style but it just doesn't work without wait, probably because of animations)
  I.waitForText('General')
  I.waitForElement('.folder-tree.complete')
  I.wait(1)
  I.click('General')
  I.waitForText('General', undefined, '.settings-header')
  I.wait(1)
  I.click('Back', '.io-ox-settings-main .settings-navbar')
  I.waitForVisible('.folder-tree.complete')
  I.wait(1)
  I.click('Accounts')
  I.waitForText('Accounts', undefined, '.settings-header')
  I.wait(1)
  I.click('Back', '.io-ox-settings-main .settings-navbar')
  I.waitForVisible('.folder-tree.complete')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/detailView"]')
  I.waitForText('Accounts', undefined, '.settings-header')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/folderTree"]')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/detailView"]')
  I.waitForText('Accounts', undefined, '.settings-header')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/folderTree"]')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/detailView"]')
  I.waitForText('General', undefined, '.settings-header')
  I.wait(1)
  I.dontSee('Accounts')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-pagecontroller.page.current[data-page-id="settings/folderTree"]')
})

Scenario('List view edit toggle @mobile', async ({ I, users }) => {
  await I.haveMail({ subject: 'Monday', from: users[0], to: users[0] })
  await I.login('app=io.ox/mail')

  // fill history
  I.waitForText('Select')
  I.click('Select')
  I.waitForText('Cancel')
  I.click('Cancel')
  I.waitForText('Select')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForText('Cancel')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForText('Select')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForText('Cancel')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForText('Select')
})

Scenario('Edit/Create/Compose dialog navigation @mobile', async ({ I, users, mail, mobileMail }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.click('~New email')
  I.waitForVisible(mail.composeWindow, 10)
  I.click('~Close window')
  I.waitForInvisible(mail.composeWindow, 10)

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForVisible(mail.composeWindow, 10)

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForInvisible(mail.composeWindow, 10)

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForVisible(mail.composeWindow, 10)

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForInvisible(mail.composeWindow, 10)
})

Scenario('Reload with contacts draft @mobile', async ({ I, users, contacts, mobileContacts }) => {
  async function getAppId () {
    return await I.executeScript(async app => {
      const { default: ox } = await import(String(new URL('ox.js', location.href)))
      return ox.ui.App.getCurrentApp().id
    })
  }

  await I.login('app=io.ox/contacts')

  mobileContacts.newContact()

  I.fillField('First name', 'Peter')
  I.click('~Close window', contacts.editWindow)
  I.waitForInvisible('.io-ox-contacts-edit-window.complete')

  // manually trigger draft caching
  await I.executeScript(async (app) => {
    const { default: ox } = await import(String(new URL('ox.js', location.href)))
    await ox.ui.App.getCurrentFloatingApp().saveRestorePoint()
  })

  I.waitForNetworkTraffic()

  I.refreshPage()
  await I.waitForApp()

  expect(await getAppId()).to.equal('io.ox/mail')
  I.waitForVisible('~New email')

  I.click('Address Book', '#app-navigation')
  await I.waitForApp()

  expect(await getAppId()).to.equal('io.ox/contacts')

  I.waitForVisible('~New contact')
  I.waitForText('Folders', undefined, '#io-ox-appcontrol')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  await I.waitForApp()

  expect(await getAppId()).to.equal('io.ox/mail')

  I.waitForElement('#io-ox-launcher', 30)
  I.click('#io-ox-launcher button')
  I.waitForElement('.launcher-dropdown.complete')

  I.see('Drafts', '.launcher-dropdown')
  I.see('New contact', '.launcher-dropdown')
})
