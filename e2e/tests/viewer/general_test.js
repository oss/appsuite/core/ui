/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Viewer')

Before(async ({ users }) => {
  await users.create()
  await users[0].hasCapability('document_preview')
})

After(async ({ users }) => { await users.removeAll() })

// Viewer:        The viewer has different layers for the content (canvas, text-layer).
//                Just checking the existence of text via CodeceptJS has limited informative value.
//                You have to test how the these layer interact and also the layout for a real e2e test.
//
// Positions:     All positions are in device coordinates. Add a pause() before the point you want
//                to update and messure inside the browser window.
//
// Clipboard:     Text copied from the PDF contains line breaks and similar symbols. Make sure to escape
//                text strings correctly when you want to update them.
Scenario('Check content and interactive elements of a pdf document', async function ({ I, drive, viewer }) {
  const folder = await I.grabDefaultFolder('infostore')
  await I.haveFile(folder, 'media/files/generic/text_document_viewer_test_v1.pdf')

  await I.login('app=io.ox/files')

  drive.selectFile('text_document_viewer_test_v1.pdf')
  I.clickToolbar('View')

  I.waitForElement('.io-ox-viewer')
  I.waitForText('START_PAGE_1')
  I.waitForText('START_PAGE_2')

  // 1) verify a 100% congruent text and canvas layer so that text selection and displayed content matches
  const textBoxPage1 = await I.grabElementBoundingRect('.page[data-page-number="1"] .textLayer')
  const canvasBoxPage1 = await I.grabElementBoundingRect('.page[data-page-number="1"] .canvasWrapper')
  const textBoxPage2 = await I.grabElementBoundingRect('.page[data-page-number="2"] .textLayer')
  const canvasBoxPage2 = await I.grabElementBoundingRect('.page[data-page-number="2"] .canvasWrapper')
  assert.deepEqual(textBoxPage1, canvasBoxPage1, 'Page1: canvas and text-wrapper must be congruent with each other')
  assert.deepEqual(textBoxPage2, canvasBoxPage2, 'Page2: canvas and text-wrapper must be congruent with each other')
  expect(textBoxPage1.height).to.be.equal(1122, 'The height of the pdf is different than expected. The viewer might show the pdf with a new zoom factor. In this case all following positions must probably be updated.')
  expect(textBoxPage1.width).to.be.equal(793, 'The width of the pdf is different than expected. The viewer might show the pdf with a new zoom factor. In this case all following positions must probably be updated.')

  viewer.scrollTo(0)

  // 2) verify positions of "page start markers" in device coordinates (just messure the screen) to check layout & to verify the correct text-layer position below the canvas
  expect(await viewer.grabScrollPosition()).to.be.equal(0, 'PDF should not be scrolled initially')
  // check the text at the given position in the text-layer

  expect(await viewer.grabTextAtPoint(225, 160)).to.be.equal('START_PAGE_1', 'Text is not at the expected viewport position. If the text appears to be correct, be aware that the log might not show non-printable characters like line breaks.')
  // scroll down by exactly the pdf page height
  const pageBox = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  viewer.scrollTo(0, pageBox.height)
  // START_PAGE_2 must be at the same device coordinates as START_PAGE_1
  expect(await viewer.grabTextAtPoint(225, 160)).to.be.equal('START_PAGE_2', 'Text is not at the expected viewport position. If the text appears to be correct, be aware that the log might not show non-printable characters like line breaks.')

  // 3) verify pdf content via clipboard
  viewer.setBrowserSelection('START_PAGE_1', 'START_PAGE_2', '.textLayer')
  I.copyToClipboard()
  const encodedClipboardContentReference = 'START_PAGE_1%0ATable%20of%20contents%0AHeadline1...............................................................................................................................%201%0AHeadline2............................................................................................................................%201%0AHeadline3........................................................................................................................%201%0AHeadline1_page_2.................................................................................................................2%0AHeadline1%0AHeadline2%0AHeadline3%0AThis%20is%20a%20normal%20text.%0AThis%20text%20has%20a%20comment.%0Aexternal%20web%20link%3A%20open-xchange.com%0ASTART_PAGE_2%0A'
  const encodedClipboardContent = encodeURIComponent(await I.getClipboardContent())
  expect(encodedClipboardContent).to.be.equal(encodedClipboardContentReference, 'Clipboard has not the expected content. Use encoded strings to not overlook non-printable characters like line breaks')

  // 4) verify working internal link, clicking the link should scroll to the correct position
  viewer.scrollTo(0, 0)
  // it's the position of "Headline1_page_2" in table of contents

  // after updating pdfjs-dist to v4.6.82 links within the selection from point 3) are not
  // clickable. After pressing a key the problem is solved.
  I.pressKey('ArrowRight')

  viewer.clickAtPosition(245, 330)
  expect(await viewer.grabScrollPosition()).to.be.equal(1305, 'Scroll position is not as expected after clicking the last entry "Headline1_page_2" in table of contents')

  // 5) verify comment layer
  I.scrollTo('.textAnnotation')
  I.moveCursorTo('.textAnnotation')
  I.waitForText('This_is_a_test_comment.')

  // 6) verify working external link
  I.click('a[title="https://open-xchange.com/"]')
  I.wait(2)
  I.switchToNextTab()
  I.waitInUrl('open-xchange.com')
})

Scenario('Check viewer zoom', async function ({ I, drive, users }) {
  const folder = await I.grabDefaultFolder('infostore')
  await Promise.all([
    I.haveFile(folder, 'media/files/generic/pdf_document_1.pdf'),
    users[0].hasCapability('document_preview')
  ])
  await I.login('app=io.ox/files')

  drive.selectFile('pdf_document_1.pdf')
  I.clickToolbar('View')
  I.waitForElement('.io-ox-viewer')
  I.waitForText('doc-1-608048216047687', 5, '.io-ox-viewer .swiper-slide-active')

  // initial zoom for the given test window size
  let zoomedPageSize = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  // locally the values are equal, in the pipeline there is a very small difference...
  const allowedDelta = 0.01
  expect(zoomedPageSize.width).to.be.closeTo(817, allowedDelta, 'Page width at 100% zoom is wrong.')
  expect(zoomedPageSize.height).to.be.closeTo(1146, allowedDelta, 'Page height at 100% zoom is wrong.')

  I.click('~Zoom in')

  I.waitForText('125 %', 5)
  // the browser needs a moment to scale
  I.wait(0.5)
  zoomedPageSize = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  expect(zoomedPageSize.width).to.be.closeTo(1022, allowedDelta, 'Page width at 125% zoom is wrong.')
  expect(zoomedPageSize.height).to.be.closeTo(1433, allowedDelta, 'Page height at 125% zoom is wrong.')

  I.click('~Zoom in')
  I.waitForText('150 %', 5)
  // the browser needs a moment to scale
  I.wait(0.5)
  zoomedPageSize = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  expect(zoomedPageSize.width).to.be.closeTo(1226, allowedDelta, 'Page width at 150% zoom is wrong.')
  expect(zoomedPageSize.height).to.be.closeTo(1719, allowedDelta, 'Page height at 150% zoom is wrong.')

  I.click('~Zoom out')
  I.waitForText('125 %', 5)
  // the browser needs a moment to scale
  I.wait(0.5)
  zoomedPageSize = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  expect(zoomedPageSize.width).to.be.closeTo(1022, allowedDelta, 'Page width at 125% zoom is wrong.')
  expect(zoomedPageSize.height).to.be.closeTo(1433, allowedDelta, 'Page height at 125% zoom is wrong.')

  I.click('~Zoom out')
  I.waitForText('100 %', 5)
  // the browser needs a moment to scale
  I.wait(0.5)
  zoomedPageSize = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  expect(zoomedPageSize.width).to.be.closeTo(817, allowedDelta, 'Page width at 100% zoom is wrong.')
  expect(zoomedPageSize.height).to.be.closeTo(1146, allowedDelta, 'Page height at 100% zoom is wrong.')
  I.click('~Zoom out')
  I.waitForText('75 %', 5)
  // the browser needs a moment to scale
  I.wait(0.5)

  zoomedPageSize = await I.grabElementBoundingRect('.page[data-page-number="1"]')
  expect(zoomedPageSize.width).to.be.closeTo(613, allowedDelta, 'Page width at 75% zoom is wrong.')
  expect(zoomedPageSize.height).to.be.closeTo(859, allowedDelta, 'Page height at 75% zoom is wrong.')
})

Scenario('Check swiper carousel', async function ({ I, drive, users }) {
  const folder = await I.grabDefaultFolder('infostore')

  await Promise.all([
    I.haveFile(folder, 'media/files/generic/pdf_document_1.pdf'),
    I.haveFile(folder, 'media/files/generic/pdf_document_2.pdf'),
    I.haveFile(folder, 'media/files/generic/pdf_document_3.pdf'),
    users[0].hasCapability('document_preview')
  ])

  await I.login('app=io.ox/files')

  // 1) select single file
  drive.selectFile('pdf_document_1.pdf')
  I.clickToolbar('View')
  I.waitForText('pdf_document_1.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-1-608048216047687', undefined, '.io-ox-viewer .swiper-slide-active')
  I.waitForText('File 1 of 3')

  // test forward loop, test via keyboard and mouse
  I.pressKey('ArrowRight')
  I.waitForText('File 2 of 3')
  I.waitForText('pdf_document_2.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-2-592871146255686', undefined, '.io-ox-viewer .swiper-slide-active')
  I.click('~Next')
  I.waitForText('File 3 of 3')
  I.waitForText('pdf_document_3.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-3-17686886748919695', undefined, '.io-ox-viewer .swiper-slide-active')
  I.click('~Next')
  I.waitForText('File 1 of 3')
  I.waitForText('pdf_document_1.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-1-608048216047687', undefined, '.io-ox-viewer .swiper-slide-active')

  // test backwards loop, test via keyboard and mouse
  I.pressKey('ArrowLeft')
  I.waitForText('File 3 of 3')
  I.waitForText('pdf_document_3.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-3-17686886748919695', undefined, '.io-ox-viewer .swiper-slide-active')
  I.click('~Previous')
  I.waitForText('File 2 of 3')
  I.waitForText('pdf_document_2.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-2-592871146255686', undefined, '.io-ox-viewer .swiper-slide-active')
  I.click('~Previous')
  I.waitForText('File 1 of 3')
  I.waitForText('pdf_document_1.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-1-608048216047687', undefined, '.io-ox-viewer .swiper-slide-active')
  I.pressKey('Escape')

  // 2) loop is limited to selected files if more than one file is selected
  I.pressKeyDown('CommandOrControl')
  drive.selectFile('pdf_document_3.pdf')
  I.pressKeyUp('CommandOrControl')
  I.clickToolbar('View')
  I.waitForElement('.io-ox-viewer')
  I.waitForText('pdf_document_1.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-1-608048216047687', undefined, '.io-ox-viewer .swiper-slide-active')
  I.waitForText('File 1 of 2')

  I.click('~Next')
  I.waitForText('File 2 of 2')
  I.waitForText('pdf_document_3.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-3-17686886748919695', undefined, '.io-ox-viewer .swiper-slide-active')

  I.click('~Next')
  I.waitForText('File 1 of 2')
  I.waitForText('pdf_document_1.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-1-608048216047687', undefined, '.io-ox-viewer .swiper-slide-active')

  I.click('~Previous')
  I.waitForText('File 2 of 2')
  I.waitForText('pdf_document_3.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-3-17686886748919695', undefined, '.io-ox-viewer .swiper-slide-active')

  I.click('~Previous')
  I.waitForText('File 1 of 2')
  I.waitForText('pdf_document_1.pdf', 10, '.io-ox-viewer .viewer-toolbar')
  I.waitForText('doc-1-608048216047687', undefined, '.io-ox-viewer .swiper-slide-active')
})

Scenario('Check viewer download progress-bar', async function ({ I, drive, users }) {
  const folder = await I.grabDefaultFolder('infostore')
  await Promise.all([
    I.haveFile(folder, 'media/files/generic/OX-OXDrive-User-Guide-English-v8.9.0.pdf'),
    I.haveFile(folder, 'media/files/generic/pdf_document_1.pdf'),
    users[0].hasCapability('document_preview')
  ])
  await I.login('app=io.ox/files')

  // 1) load time below 1 second, no progress-bar visible
  drive.selectFile('pdf_document_1.pdf')
  I.clickToolbar('View')
  // the element needs to be in DOM to have a stable point for testing the behavior
  I.retry(3).seeElementInDOM('.io-ox-viewer .swiper-slide .progress-bar')
  I.dontSeeElement('.io-ox-viewer .swiper-slide .progress-bar')
  I.waitForText('doc-1-608048216047687', 5, '.io-ox-viewer .swiper-slide-active')
  I.dontSeeElement('.io-ox-viewer .swiper-slide .progress-bar')
  I.pressKey('Escape')

  // 2) load time above 1 second, the progress-bar is visible
  drive.selectFile('OX-OXDrive-User-Guide-English-v8.9.0.pdf')
  await I.throttleNetwork('3G')
  I.clickToolbar('View')
  // the element needs to be in DOM to have a stable point for testing the behavior
  I.retry(3).seeElementInDOM('.io-ox-viewer .swiper-slide .progress-bar')
  I.dontSeeElement('.io-ox-viewer .swiper-slide .progress-bar')
  I.waitForElement('.io-ox-viewer .swiper-slide .progress-bar[aria-valuenow="0"]', 10)
  I.waitForElement('.io-ox-viewer .swiper-slide .progress-bar[aria-valuenow="100"]', 20)
  I.waitForText('User Guide', 5, '.io-ox-viewer .swiper-slide-active')
  I.waitForInvisible('.io-ox-viewer .swiper-slide .progress-bar')
})

Scenario('Check standalone Viewer app toolbar', async function ({ I, drive, users }) {
  const folder = await I.grabDefaultFolder('infostore')
  await Promise.all([
    I.haveFile(folder, 'media/files/generic/pdf_document_1.pdf'),
    users[0].hasCapability('document_preview'),

    I.haveSetting('io.ox/core//logoAction', 'autoStart'), // AutoStart defaults to mail app
    I.haveSetting('io.ox/core//apps/quickLaunchCount', 5),
    I.haveSetting('io.ox/core//apps/quickLaunch', 'io.ox/calendar,io.ox/mail,io.ox/files')
  ])

  await I.login('app=io.ox/files')

  drive.selectFile('pdf_document_1.pdf')
  I.clickToolbar('View')
  I.waitForElement('.io-ox-viewer')
  I.waitForText('doc-1-608048216047687', 5, '.io-ox-viewer .swiper-slide-active')

  I.click('~Pop out standalone viewer')

  I.wait(0.3)
  I.switchToNextTab()

  I.waitForElement('.io-ox-viewer')
  I.waitForText('doc-1-608048216047687', 5, '.io-ox-viewer .swiper-slide-active')

  // app launcher & quick launcher icons must not be visible
  I.dontSeeElement('#io-ox-launcher')
  I.dontSeeElement('#io-ox-quicklaunch')
  // a click on the logo must not change to another App (mail)
  I.click('#io-ox-top-logo')
  I.wait(1)
  I.see('doc-1-608048216047687', '.io-ox-viewer .swiper-slide-active')
  I.dontSee('New email')
})
