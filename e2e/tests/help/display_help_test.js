/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > Help')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Hide and show Help topics based on user capabilities', async ({ I, users, contacts, topbar }) => {
  const checkIfDisplayNone = async (capability) => {
    I.waitForElement(`.listitem.cap-${capability}`)
    const displayProperties = await I.grabCssPropertyFromAll(locate(`.listitem.cap-${capability}`), 'display')
    const result = displayProperties.every(displayProperty => displayProperty === 'none')
    expect(result, `expected ${capability} section to be hidden`).to.be.true
  }

  // Disable calendar
  await users[0].hasModuleAccess({ calendar: false })

  await I.login('app=io.ox/contacts')

  // open help window
  topbar.help()

  // Check if help shows info about disabled capability
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.wait(1)
    I.waitForText('Start Page')
    I.click('Start Page')
    I.waitForElement('li.listitem.cap-tasks')
    I.scrollTo('.listitem.cap-tasks')
    I.waitForText('Tasks')
    await checkIfDisplayNone('calendar')
  })

  await I.logout()
  // additionally disable tasks
  await users[0].hasModuleAccess({ tasks: false })
  await I.login('app=io.ox/contacts')

  // open help window
  topbar.help()

  // Check if help shows info about disabled capability
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.wait(1)
    I.waitForText('Start Page')
    I.click('Start Page')
    I.waitForElement('li.listitem.cap-infostore')
    I.scrollTo('li.listitem.cap-infostore')
    I.waitForText('Drive')

    await checkIfDisplayNone('calendar')
    await checkIfDisplayNone('tasks')
  })

  await I.logout()
  // additionally disable drive
  await users[0].hasModuleAccess({ infostore: false })
  // close help
  await I.login('app=io.ox/contacts')

  // open help window
  topbar.help()

  // Check if help shows info about disabled capability
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.wait(1)
    I.waitForText('Start Page')
    I.click('Start Page')

    await checkIfDisplayNone('calendar')
    await checkIfDisplayNone('tasks')
    await checkIfDisplayNone('infostore')
  })
})

Scenario('Do not add indicator class when feature is disabled', async ({ I, users, topbar }) => {
  await Promise.all([
    I.haveSetting('io.ox/core//features/attachFromDrive', false),
    users[0].hasModuleAccess({ infostore: true })
  ])

  await I.login()
  topbar.help()
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.waitForText('Start Page')
    const hasClass = await I.executeScript(() => { return document.documentElement.classList.contains('feat-io-ox-core-attachFromDrive') })
    expect(hasClass).to.be.false
  })
})

Scenario('Do not add indicator class when requirement is not met', async ({ I, users, topbar }) => {
  await Promise.all([
    I.haveSetting('io.ox/core//features/attachFromDrive', true),
    users[0].hasModuleAccess({ infostore: false })
  ])

  await I.login()
  topbar.help()
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.waitForText('Start Page')
    const hasClass = await I.executeScript(() => { return document.documentElement.classList.contains('feat-io-ox-core-attachFromDrive') })
    expect(hasClass).to.be.false
  })
})

Scenario('Add indicator class when feature is enabled requirement is met', async ({ I, users, topbar }) => {
  await Promise.all([
    I.haveSetting('io.ox/core//features/attachFromDrive', true),
    users[0].hasModuleAccess({ infostore: true })
  ])

  await I.login()
  topbar.help()
  await within({ frame: '.inline-help-iframe' }, async () => {
    I.waitForText('Start Page')
    const hasClass = await I.executeScript(() => { return document.documentElement.classList.contains('feat-io-ox-core-attachFromDrive') })
    expect(hasClass).to.be.true
  })
})
