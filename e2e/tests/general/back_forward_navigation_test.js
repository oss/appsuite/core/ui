/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > Back/Forward Navigation')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('App switching', async ({ I }) => {
  // fill history
  await I.login('app=io.ox/mail')

  I.openApp('Calendar')
  await I.waitForApp()
  I.openApp('Address Book')
  await I.waitForApp()

  async function getAppId () {
    return await I.executeScript(async (app) => {
      const { default: ox } = await import(String(new URL('ox.js', location.href)))
      return ox.ui.App.getCurrentApp().id
    })
  }

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  expect(await getAppId()).to.equal('io.ox/calendar')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  expect(await getAppId()).to.equal('io.ox/mail')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  expect(await getAppId()).to.equal('io.ox/calendar')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  expect(await getAppId()).to.equal('io.ox/mail')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  expect(await getAppId()).to.equal('io.ox/calendar')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  expect(await getAppId()).to.equal('io.ox/contacts')
})

Scenario('Notification area toggle', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.waitForElement('#io-ox-notifications-toggle')
  I.click('#io-ox-notifications-toggle button')
  I.waitForElement('.io-ox-notifications.visible')
  I.pressKey('Escape')
  I.waitForInvisible('.io-ox-notifications.visible')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-notifications.visible')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-notifications:not(.visible)')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-notifications.visible')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-notifications:not(.visible)')
})

Scenario('Settings toggle', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.click('#io-ox-topbar-settings-dropdown-icon button')
  I.waitForText('All settings …')
  I.click('All settings …')
  I.waitForElement('.io-ox-settings-main')
  I.waitForElement('~Close settings')
  I.click('~Close settings')
  I.waitForInvisible('.io-ox-settings-main')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-settings-main')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForInvisible('.io-ox-settings-main')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-settings-main')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForInvisible('.io-ox-settings-main')
})

// FIXME: The back navigation in settings modal opens general settings instead of mail settings
Scenario.skip('Settings toggle with modal dialogs', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.click('#io-ox-topbar-settings-dropdown-icon button')
  I.waitForText('All settings …')
  I.click('All settings …')
  I.waitForElement('.io-ox-settings-main')
  I.waitForElement('~Close settings')
  I.click('~Close settings')
  I.waitForInvisible('.io-ox-settings-main')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-settings-main')
  // disrupt navigation by opening a modal dialog
  I.click('Configure inbox categories')
  I.waitForText('Configure categories')

  // dialog closes, settings should still be open
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForElement('.io-ox-settings-main:not(.modal-paused)')
  I.dontSee('Configure categories')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForInvisible('.io-ox-settings-main')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-settings-main')

  // disrupt navigation by opening a modal dialog
  I.click('Configure inbox categories')
  I.waitForText('Configure categories')

  // dialog closes, settings should still be open
  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForElement('.io-ox-settings-main:not(.modal-paused)')
  I.dontSee('Configure categories')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForInvisible('.io-ox-settings-main')
})

Scenario('Search navigation', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // fill history
  I.fillField('.search-field', 'Meine tolle Suche')
  I.pressKey('Enter')
  I.waitForText('Search results')
  I.waitForText('No search results')
  I.waitForElement('~Cancel search')
  I.click('~Cancel search')
  I.waitForText('Inbox')
  I.waitForText('This folder is empty')
  I.dontSeeInField('.search-field', 'Meine tolle Suche')

  // try navigation
  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForText('Search results')
  I.seeInField('.search-field', 'Meine tolle Suche')
  I.waitForText('No search results')

  I.usePlaywrightTo('press back button', async ({ page }) => await page.goBack())
  I.waitForText('Inbox')
  I.waitForText('This folder is empty')
  I.dontSeeInField('.search-field', 'Meine tolle Suche')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForText('Search results')
  I.seeInField('.search-field', 'Meine tolle Suche')
  I.waitForText('No search results')

  I.usePlaywrightTo('press forward button', async ({ page }) => await page.goForward())
  I.waitForText('Inbox')
  I.waitForText('This folder is empty')
  I.dontSeeInField('.search-field', 'Meine tolle Suche')
})
