/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > Whats New Dialog')

Before(async ({ users }) => {
  const user = await users.create()
  await user.hasConfig('io.ox/core//whatsNew/autoStart', true)
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Does not show up for new users by default', async ({ I }) => {
  await I.login()

  I.wait(2)
  I.dontSee('What\'s new')
  await I.logout()

  // unless baselineVersion is configured…
  I.haveSetting('io.ox/core//whatsNew/baselineVersion', '8.1')
  await I.login()

  I.waitForElement('.modal[data-point="tours/whatsnew/dialog"]')
  I.see('What\'s new', '.modal')
})

Scenario('Featurelist is customizable', async ({ I, users }) => {
  await users[0].hasConfig('io.ox/core//autoStart', 'io.ox/mail')
  // @ts-ignore
  I.setCookie({ name: 'locale', value: 'en_US', url: process.env.LAUNCH_URL })
  I.refreshPage()
  await I.haveSetting('io.ox/core//whatsNew/baselineVersion', '8.1')
  I.amOnLoginPage()
  await I.waitForFocus('#io-ox-login-username')
  await I.executeScript(async () => {
    const { default: ext } = await import(String(new URL('io.ox/core/extensions.js', location.href)))
    ext.point('pe/core/whatsnew/dialog/featurelist').extend({
      id: 'customwhatsnew',
      customize: function (features) {
        features.unshift({
          version: '8.99',
          priority: 1,
          device: '!smartphone',
          title: 'Alea iacta est',
          description: 'This is what Iulius Caesar said when crossing the Rubicon river with his legions',
          illustration: 'pe/core/whatsnew/8.4/ic_notifications.svg',
          tag: 'Mytag',
          date: '2024-12-03',
          month: 'December 2024'
        })
        return features
      }
    })
  })
  I.fillField('Username', `${users[0].get('name')}@${users[0].context.id}`)
  I.fillField('Password', users[0].get('password'))
  I.click('Sign in')
  I.waitForText('No message selected', 30)
  I.see('What\'s new', '.modal')

  const customizedFeature = locate('li').withChild('.feature-illustration').first()
  I.waitForElement(customizedFeature.find('img').withAttr({ src: 'pe/core/whatsnew/8.4/ic_notifications.svg' }))
  I.waitForElement(customizedFeature.find('h3').withText('Alea iacta est'))
  I.waitForElement(customizedFeature.find('p').withText('This is what Iulius Caesar said when crossing the Rubicon river with his legions'))
  I.waitForElement(customizedFeature.find('span').withText('Mytag'))
})
