/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > Sidepanel')

Before(async ({ users }) => {
  await users.create()
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Should add and remove and navigate widgets properly', async ({ I }) => {
  await I.login('app=io.ox/mail')
  I.waitForApp()

  await I.executeScript(async () => {
    const { sidepanelView } = await import('./io.ox/core/sidepanel.js')
    const first = sidepanelView.add('example', 'Example Sidepanel')
    first.$content.text('First')
    sidepanelView.makeActive('example')
  })

  I.waitForText('Example Sidepanel', 10, '#io-ox-sidepanel')
  I.dontSeeElement('#io-ox-sidepanel .back')
  I.dontSeeElement('#io-ox-sidepanel .help-button')

  await I.executeScript(async () => {
    const { sidepanelView } = await import('./io.ox/core/sidepanel.js')
    const second = sidepanelView.add('second', 'Best Sidepanel App', { helpUrl: 'BestHelpEver.html' })
    second.$content.text('Second sidepanel')
    const third = sidepanelView.add('third', 'Dritte App', { back: 'second' })
    third.$content.text('Third sidepanel')
    sidepanelView.makeActive('third')
  })

  I.waitForText('Dritte App', 10, '#io-ox-sidepanel')
  I.waitForText('Third sidepanel', 3, '#io-ox-sidepanel')
  I.seeElement('#io-ox-sidepanel .back')
  I.dontSeeElement('#io-ox-sidepanel .help-button')

  I.click('#io-ox-sidepanel .back')

  I.waitForText('Best Sidepanel App', 10, '#io-ox-sidepanel')
  I.waitForText('Second sidepanel', 3, '#io-ox-sidepanel')
  I.dontSeeElement('#io-ox-sidepanel .back')
  I.seeElement('#io-ox-sidepanel .help-button')

  await I.executeScript(async () => {
    const { sidepanelView } = await import('./io.ox/core/sidepanel.js')
    sidepanelView.remove('second')
    sidepanelView.remove('third')
  })

  I.dontSeeElement('#io-ox-sidepanel')
})
