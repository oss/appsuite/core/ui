/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > Floating windows')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C237267] Check if specific apps open as floating windows @contentReview', async ({ I, tasks }) => {
  await I.login(['app=io.ox/tasks'])
  I.waitForVisible('.io-ox-tasks-window', 30)

  tasks.newTask()
  I.waitForVisible('.floating-window')

  await within('.floating-window', async () => {
    I.click('[data-action=close]')
  })
})

Scenario('[C237268] Open floating apps and verify the taskbar', async ({ I, mail, tasks, contacts }) => {
  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.dontSeeElement('#io-ox-taskbar-container')
  I.openApp('Address Book')
  I.waitForNetworkTraffic()
  contacts.newContact()
  I.waitForVisible(contacts.editWindow)
  I.seeElement(mail.composeWindow)
  I.openApp('Tasks')
  tasks.newTask()
  I.seeElement('.io-ox-mail-compose')
  I.seeElement(contacts.editWindow)
  I.click('~Minimize', contacts.editWindow)
  I.waitForVisible('#io-ox-taskbar-container')
  I.waitForVisible('#io-ox-taskbar-container button[aria-label="New contact"]')
})

Scenario('[C237269] Toggle display styles of floating windows @contentReview', async ({ I, tasks }) => {
  await I.login(['app=io.ox/tasks'])
  I.waitForVisible('.io-ox-tasks-window', 30)

  tasks.newTask()
  I.waitForVisible('.floating-window')

  I.click('[data-action=maximize]')
  I.waitForVisible('.floating-window.maximized')

  I.click('[data-action=normalize]')
  I.waitForVisible('.floating-window.normal')

  I.click('[data-action=minimize]')
  I.wait(1)
  I.dontSeeElement('.floating-window')
  I.waitForVisible('[data-action=restore]')
})

Scenario('Closing a floating window focusses the next open floating window', async ({ I, tasks }) => {
  await I.login(['app=io.ox/tasks'])
  I.waitForVisible('.io-ox-tasks-window', 30)

  tasks.newTask()
  tasks.newTask()
  tasks.newTask()
  I.waitForVisible('.floating-window')

  I.seeNumberOfVisibleElements('.floating-window', 3)
  await I.waitForFocus('.floating-window:last-of-type input.title-field')
  I.pressKey('Escape')

  I.seeNumberOfVisibleElements('.floating-window', 2)
  await I.waitForFocus('.floating-window:last-of-type input.title-field')
  I.pressKey('Escape')

  I.seeNumberOfVisibleElements('.floating-window', 1)
  await I.waitForFocus('.floating-window:last-of-type input.title-field')
  I.pressKey('Escape')

  I.waitForDetached('.floating-window')
  await I.waitForFocus('.primary-action > div > button:nth-child(1)')
})

Scenario('Closing dirty-model check modal returns focus to floating window', async ({ I, tasks }) => {
  await I.login(['app=io.ox/tasks'])
  I.waitForVisible('.io-ox-tasks-window', 30)

  tasks.newTask()
  I.waitForVisible('.floating-window')

  I.fillField('.active input.title-field', 'test')
  I.pressKey('Escape')

  I.waitForVisible('.modal-dialog')
  I.pressKey('Escape')

  I.waitForVisible('.floating-window')
  await I.waitForFocus('.floating-window input.title-field')
})

Scenario('Navigate between floating windows', async ({ I, tasks }) => {
  await I.login(['app=io.ox/tasks'])
  I.waitForVisible('.io-ox-tasks-window', 30)

  tasks.newTask()
  I.fillField('.active input.title-field', 'window 1')
  tasks.newTask()
  I.fillField('.active input.title-field', 'window 2')
  tasks.newTask()
  I.fillField('.active input.title-field', 'window 3')
  I.waitForVisible('.floating-window')
  I.seeNumberOfVisibleElements('.floating-window', 3)

  I.pressKey(['Control', '.'])
  expect(await I.grabValueFrom('.active input.title-field')).to.equal('window 1')

  I.pressKey(['Control', '.'])
  expect(await I.grabValueFrom('.active input.title-field')).to.equal('window 2')

  I.pressKey(['Control', '.'])
  expect(await I.grabValueFrom('.active input.title-field')).to.equal('window 3')

  I.pressKey(['Control', ','])
  expect(await I.grabValueFrom('.active input.title-field')).to.equal('window 2')
})
