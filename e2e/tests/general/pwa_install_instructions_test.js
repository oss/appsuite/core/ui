/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > PWA Install Instructions')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Only show action when enabled', async ({ I, dialogs }) => {
  await I.haveSetting({
    'io.ox/core': { features: { pwaInstructionsWizard: false } }
  })

  await I.login('app=io.ox/mail')
  I.click('~Settings')
  I.dontSee('Install OX App Suite like an app …')
  I.logout()

  await I.haveSetting({
    'io.ox/core': {
      features: { pwaInstructionsWizard: true },
      autoopen: { 'pwa-install-instructions': { remaining: '3' } }
    }
  })

  await I.login('app=io.ox/mail')
  I.waitForVisible('.pwa-install-instructions')
  I.waitForText('Install OX App Suite')
  I.click('OK')
  I.waitForInvisible('.pwa-install-instructions')
  I.click('~Settings')
  I.see('Install OX App Suite like an app …')
  I.clickDropdown('Install OX App Suite like an app …')
  I.waitForText('Install OX App Suite')
})

Scenario('Show "Install" when beforeinstallprompt event is present', async ({ I, dialogs }) => {
  await I.haveSetting({
    'io.ox/core': { features: { pwaInstructionsWizard: true }, autoopen: { 'pwa-install-instructions': { remaining: 0 } } }
  })
  await I.login('app=io.ox/mail')

  // mock beforeinstallprompt event
  I.executeScript(async () => {
    const { default: ox } = await import(String(new URL('ox.js', location.href)))
    const mockInstallPromptEvent = {
      prompt () {
        document.body.classList.add('beforeinstallprompt')
        return Promise.resolve()
      }
    }

    ox.deferredInstallPrompt = mockInstallPromptEvent
  })

  // open instructions
  I.click('~Settings')
  I.see('Install OX App Suite like an app …')
  I.clickDropdown('Install OX App Suite like an app …')
  I.waitForText('Install OX App Suite')

  // check functionality of Install
  I.waitForVisible('.pwa-install-instructions')
  I.waitForText('Install OX App Suite')
  I.click('Install')
  I.waitForDetached('.pwa-install-instructions')
  I.waitForElement('body.beforeinstallprompt')
})
