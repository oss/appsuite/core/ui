/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

/* eslint-disable camelcase */
Feature('General > Category Browser')

Before(async ({ users }) => {
  await users.create()
})

After(async ({ users }) => { await users.removeAll() })

Scenario('Category browser is showing just INBOX mails when "allMessagesFolder" is not existing (undefined)', async ({ I, users }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  const defaultData = {
    from: users[0],
    to: users[0],
    sendtype: 0,
    content: 'Sehr tolle Mail'
  }
  await Promise.all([
    I.haveMail({ subject: 'Mail 1', ...defaultData }),
    I.haveMail({ subject: 'Mail 2', ...defaultData }),
    I.haveMail({ subject: 'Mail 3', ...defaultData }),
    I.haveMail({ subject: 'Mail 4', ...defaultData })
  ])
  await Promise.all([
    I.setMailCategories({ mailId: 1, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 2, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 3, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 4, folder: 'default0/Sent', categories: ['$ct_predefined_0001'] }),
    users[0].hasConfig('com.openexchange.find.basic.mail.allMessagesFolder', '')
  ])

  await I.login('app=io.ox/contacts')
  I.waitForApp()
  I.click('~Settings', '#io-ox-topbar-settings-dropdown-icon')
  I.waitForVisible('#topbar-settings-dropdown')
  I.click('Browse categories …', '#topbar-settings-dropdown')
  I.waitForText('No category selected')
  I.click('Add category')
  I.waitForVisible('.category-dropdown a[data-name="Important"]')
  I.click('.category-dropdown a[data-name="Important"]')
  I.waitForText('Emails (Inbox)', 5, '.category-label')

  I.waitForText('Mail 1', 5, '.category-browser-mail-item')
  I.waitForText('Mail 2', 5, '.category-browser-mail-item')
  I.waitForText('Mail 3', 5, '.category-browser-mail-item')
  I.dontSee('Mail 4', '.category-browser-mail-item')
})

Scenario('Category browser is showing all filtered mails when "allMessagesFolder" is existing (default0/virtual/all)', async ({ I, users }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  const defaultData = {
    from: users[0],
    to: users[0],
    sendtype: 0,
    content: 'Sehr tolle Mail'
  }
  await Promise.all([
    I.haveMail({ subject: 'Mail 1', ...defaultData }),
    I.haveMail({ subject: 'Mail 2', ...defaultData }),
    I.haveMail({ subject: 'Mail 3', ...defaultData }),
    I.haveMail({ subject: 'Mail 4', ...defaultData })
  ])
  await Promise.all([
    I.setMailCategories({ mailId: 1, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 2, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 3, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 4, folder: 'default0/Sent', categories: ['$ct_predefined_0001'] }),
    users[0].hasConfig('com.openexchange.find.basic.mail.allMessagesFolder', 'default0/virtual/all')
  ])

  await I.login('app=io.ox/contacts')
  I.waitForApp()
  I.click('~Settings', '#io-ox-topbar-settings-dropdown-icon')
  I.waitForVisible('#topbar-settings-dropdown')
  I.click('Browse categories …', '#topbar-settings-dropdown')
  I.waitForText('No category selected')
  I.click('Add category')
  I.waitForVisible('.category-dropdown a[data-name="Important"]')
  I.click('.category-dropdown a[data-name="Important"]')
  I.waitForText('Emails', 5, '.category-label')

  I.waitForText('Mail 1', 5, '.category-browser-mail-item')
  I.waitForText('Mail 2', 5, '.category-browser-mail-item')
  I.waitForText('Mail 3', 5, '.category-browser-mail-item')
  I.waitForText('Mail 4', 5, '.category-browser-mail-item')
})

Scenario('Check recent categories and emtpy messages', async ({ I }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  await I.login()
  I.waitForApp()

  // open category browser
  I.click('~Settings', '#io-ox-topbar-settings-dropdown-icon')
  I.waitForVisible('#topbar-settings-dropdown')
  I.dontSeeElement('#io-ox-category-browser')
  I.click('Browse categories …', '#topbar-settings-dropdown')
  I.waitForElement('#io-ox-category-browser')
  I.waitForText('No category selected')

  // add categories
  I.click('Add category')
  I.waitForVisible('.category-dropdown a[data-name="Important"]')
  I.click('.category-dropdown a[data-name="Important"]')
  I.waitForText('No items with this category found')
  I.click('Add category')
  I.waitForVisible('.category-dropdown a[data-name="Private"]')
  I.click('.category-dropdown a[data-name="Private"]')
  I.waitForText('No items with these categories found')
  I.seeNumberOfElements('#io-ox-category-browser .selected-categories li', 2)

  // remove categories
  I.click('~Remove category Important', '#io-ox-category-browser')
  I.seeNumberOfElements('#io-ox-category-browser .selected-categories li', 1)
  I.click('~Remove category Private', '#io-ox-category-browser')
  I.seeNumberOfElements('#io-ox-category-browser .selected-categories li', 0)

  // check recently used categories and select one
  I.waitForText('Recently used')
  I.seeNumberOfElements('#io-ox-category-browser .recent-categories li', 2)
  I.click('#io-ox-category-browser .recent-categories [data-name="Important"]')
  I.waitForInvisible('#io-ox-category-browser .recent-categories')
  I.waitForText('No items with this category found')
  I.seeNumberOfVisibleElements('#io-ox-category-browser .selected-categories li', 1)

  // close category browser
  I.click('#io-ox-sidepanel [title="Close"]')
  I.waitForInvisible('#io-ox-category-browser')
})

Scenario('Mail roundtrip', async ({ I, users, mail }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  // create 6 mails
  const defaultData = {
    from: [[users[0].get('display_name'), users[0].get('primaryEmail')]],
    to: users[0],
    sendtype: 0,
    content: 'Sehr tolle Mail'
  }

  await Promise.all([
    I.haveMail({ subject: 'Mail 1', ...defaultData }),
    I.haveMail({ subject: 'Mail 2', ...defaultData }),
    I.haveMail({ subject: 'Mail 3', ...defaultData }),
    I.haveMail({ subject: 'Mail 4', ...defaultData }),
    I.haveMail({ subject: 'Mail 5', ...defaultData }),
    I.haveMail({ subject: 'Mail 6', ...defaultData })
  ])

  // bit ugly but I.haveMail does not support creating mails with categories (yet?)
  await I.login('app=io.ox/contacts')
  await Promise.all([
    I.setMailCategories({ mailId: 1, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 2, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 3, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 4, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 5, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] }),
    I.setMailCategories({ mailId: 6, folder: 'default0/INBOX', categories: ['$ct_predefined_0001'] })
  ])

  I.openApp('Mail')
  await I.waitForApp()

  I.waitForText('Mail 1', 5, '.list-view.mail-item')
  I.click('Mail 1', '.list-view.mail-item')
  I.waitForElement('.mail-detail .categories-badges-container [data-name="Important"]')
  I.click('.mail-detail .categories-badges-container [data-name="Important"]')
  I.waitForElement('#io-ox-category-browser .selected-categories [data-name="Important"]')
  I.waitForText('Show all', 5, '#io-ox-category-browser')
  I.seeNumberOfElements('#io-ox-category-browser .category-browser-mail-item', 5)

  I.click('.category>button')
  I.waitForDetached('.category .items')
  I.click('.category>button')
  I.waitForElement('.category .items')

  I.click('Show all', '#io-ox-category-browser')
  I.waitForText('Search results')
  I.waitForText('Mail 1', 5, '.list-view.mail-item')
  I.waitForText('Mail 2', 5, '.list-view.mail-item')
  I.waitForText('Mail 3', 5, '.list-view.mail-item')
  I.waitForText('Mail 4', 5, '.list-view.mail-item')
  I.waitForText('Mail 5', 5, '.list-view.mail-item')
  I.waitForText('Mail 6', 5, '.list-view.mail-item')

  I.click('#io-ox-category-browser .category-browser-mail-item:first-child')
  I.waitForVisible('.detail-popup [data-action="io.ox/mail/actions/delete"]')
  I.click('~Delete', '.detail-popup')
  I.waitForDetached('.detail-popup')

  I.waitForInvisible('#io-ox-category-browser .footer')
  I.dontSee('Show all', '')
  I.seeNumberOfElements('#io-ox-category-browser .category-browser-mail-item', 5)
})

Scenario('Appointments roundtrip', async ({ I, users, calendar, dialogs }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  // create 6 appointments
  const startTime = calendar.getNextMonday()
  const defaultData = {
    organizer: {
      cn: users[0].get('display_name'),
      email: users[0].get('email1'),
      entity: users[0].get('id'),
      uri: `mailto:${users[0].get('email1')}`
    },
    categories: ['Important'],
    attendees: [
      { partStat: 'ACCEPTED', entity: users[0].get('id'), cn: users[0].get('display_name'), uri: `mailto:${users[0].get('email1')}` }
    ]
  }
  // no promise all because of concurrent modification issues
  await I.haveAppointment({ summary: 'Appointment 1', startDate: { value: startTime }, endDate: { value: startTime.add(1, 'hour') }, ...defaultData })
  await I.haveAppointment({ summary: 'Appointment 2', startDate: { value: startTime }, endDate: { value: startTime.add(1, 'hour') }, ...defaultData })
  await I.haveAppointment({ summary: 'Appointment 3', startDate: { value: startTime }, endDate: { value: startTime.add(1, 'hour') }, ...defaultData })
  await I.haveAppointment({ summary: 'Appointment 4', startDate: { value: startTime }, endDate: { value: startTime.add(1, 'hour') }, ...defaultData })
  await I.haveAppointment({ summary: 'Appointment 5', startDate: { value: startTime }, endDate: { value: startTime.add(1, 'hour') }, ...defaultData })
  await I.haveAppointment({ summary: 'Appointment 6', startDate: { value: startTime }, endDate: { value: startTime.add(1, 'hour') }, ...defaultData })

  await I.login('app=io.ox/calendar')
  I.waitForApp()
  calendar.moveCalendarViewToNextWeek()

  I.waitForText('Appointment 1', 5, '.appointment')
  I.click('Appointment 1', '.appointment')
  I.waitForVisible('.detail-popup')
  I.waitForText('Appointment 1', 5, '.detail-popup')
  I.waitForElement('.calendar-detail .categories-badges [data-name="Important"]')
  I.click('.calendar-detail .categories-badges [data-name="Important"]')

  I.waitForElement('#io-ox-category-browser .selected-categories [data-name="Important"]')
  I.click('~Close', '.detail-popup')
  I.waitForDetached('.detail-popup')

  I.waitForText('Appointment 1', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 2', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 3', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 4', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 5', 5, '#io-ox-category-browser')
  I.waitForText('Show all', 5, '#io-ox-category-browser')

  I.click('.category>button')
  I.waitForDetached('.category .items')
  I.click('.category>button')
  I.waitForElement('.category .items')

  I.click('Show all', '#io-ox-category-browser')
  I.waitForText('Search results')
  I.waitForText('Appointment 1', 5, '.calendar-list-view')
  I.waitForText('Appointment 2', 5, '.calendar-list-view')
  I.waitForText('Appointment 3', 5, '.calendar-list-view')
  I.waitForText('Appointment 4', 5, '.calendar-list-view')
  I.waitForText('Appointment 5', 5, '.calendar-list-view')
  I.waitForText('Appointment 6', 5, '.calendar-list-view')

  I.click('Appointment 1', '#io-ox-category-browser')
  I.waitForVisible('.detail-popup')
  I.waitForText('Appointment 1', 5, '.detail-popup')
  I.click('~Delete appointment', '.detail-popup')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete appointment')
  I.waitForDetached('.detail-popup')

  I.waitForText('Appointment 6', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 2', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 3', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 4', 5, '#io-ox-category-browser')
  I.waitForText('Appointment 5', 5, '#io-ox-category-browser')
  I.dontSee('Show all', '#io-ox-category-browser')
})

Scenario('Contacts roundtrip', async ({ I, users, calendar, dialogs }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  // create 6 contacts
  const folder_id = await I.grabDefaultFolder('contacts')
  await Promise.all([
    I.haveContact({ display_name: 'Scott, Jason', first_name: 'Jason', last_name: 'Scott', folder_id, categories: 'Important' }),
    I.haveContact({ display_name: 'Kwan, Trini', first_name: 'Trini', last_name: 'Kwan', folder_id, categories: 'Important' }),
    I.haveContact({ display_name: 'Hart, Kimberly', first_name: 'Kimberly', last_name: 'Hart', folder_id, categories: 'Important' }),
    I.haveContact({ display_name: 'Cranston, Billy', first_name: 'Billy', last_name: 'Cranston', folder_id, categories: 'Important' }),
    I.haveContact({ display_name: 'Taylor, Zack', first_name: 'Zack', last_name: 'Taylor', folder_id, categories: 'Important' }),
    I.haveContact({ display_name: 'Oliver, Tommy', first_name: 'Tommy', last_name: 'Oliver', folder_id, categories: 'Important' })
  ])

  await I.login('app=io.ox/contacts')
  I.waitForApp()

  I.waitForText('Cranston, Billy', 5, '.contact-detail')
  I.click('Important', '.contact-detail')

  I.waitForElement('#io-ox-category-browser .selected-categories [data-name="Important"]')

  I.waitForText('Cranston, Billy', 5, '#io-ox-category-browser')
  I.waitForText('Hart, Kimberly', 5, '#io-ox-category-browser')
  I.waitForText('Kwan, Trini', 5, '#io-ox-category-browser')
  I.waitForText('Oliver, Tommy', 5, '#io-ox-category-browser')
  I.waitForText('Scott, Jason', 5, '#io-ox-category-browser')
  I.waitForText('Show all', 5, '#io-ox-category-browser')

  I.click('.category>button')
  I.waitForDetached('.category .items')
  I.click('.category>button')
  I.waitForElement('.category .items')

  I.click('Show all', '#io-ox-category-browser')
  I.waitForText('Search results')
  I.waitForText('Cranston, Billy', 5, '.contact-grid-container')
  I.waitForText('Hart, Kimberly', 5, '.contact-grid-container')
  I.waitForText('Kwan, Trini', 5, '.contact-grid-container')
  I.waitForText('Oliver, Tommy', 5, '.contact-grid-container')
  I.waitForText('Scott, Jason', 5, '.contact-grid-container')
  I.waitForText('Taylor, Zack', 5, '.contact-grid-container')

  I.click('Cranston, Billy', '#io-ox-category-browser')
  I.waitForVisible('.detail-popup')
  I.waitForText('Cranston, Billy', 5, '.detail-popup')
  I.click('~Delete', '.detail-popup')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.detail-popup')

  I.waitForText('Taylor, Zack', 5, '#io-ox-category-browser')
  I.waitForText('Hart, Kimberly', 5, '#io-ox-category-browser')
  I.waitForText('Kwan, Trini', 5, '#io-ox-category-browser')
  I.waitForText('Oliver, Tommy', 5, '#io-ox-category-browser')
  I.waitForText('Scott, Jason', 5, '#io-ox-category-browser')
  I.dontSee('Show all', '#io-ox-category-browser')
})

Scenario('Tasks roundtrip', async ({ I, users, tasks, calendar, dialogs }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  const defaultFolder = await I.grabDefaultFolder('tasks')
  const dueTime = calendar.getNextMonday()
  const defaultData = {
    full_time: false,
    folder_id: defaultFolder,
    categories: 'Important'
  }
  await Promise.all([
    I.haveTask({ title: 'Task 1', end_time: dueTime.add(1, 'hour').valueOf(), ...defaultData }),
    I.haveTask({ title: 'Task 2', end_time: dueTime.add(1, 'hour').valueOf(), ...defaultData }),
    I.haveTask({ title: 'Task 3', end_time: dueTime.add(1, 'hour').valueOf(), ...defaultData }),
    I.haveTask({ title: 'Task 4', end_time: dueTime.add(1, 'hour').valueOf(), ...defaultData }),
    I.haveTask({ title: 'Task 5', end_time: dueTime.add(1, 'hour').valueOf(), ...defaultData }),
    I.haveTask({ title: 'Task 6', end_time: dueTime.add(1, 'hour').valueOf(), ...defaultData })
  ])

  await I.login('app=io.ox/tasks')
  I.waitForApp()

  I.waitForText('Task 1', 5, '.task-detail-container')
  I.waitForElement('.task-detail-container .categories-badges [data-name="Important"]')
  I.click('.task-detail-container .categories-badges [data-name="Important"]')

  I.waitForElement('#io-ox-category-browser .selected-categories [data-name="Important"]')

  I.waitForText('Task 1', 5, '#io-ox-category-browser')
  I.waitForText('Task 2', 5, '#io-ox-category-browser')
  I.waitForText('Task 3', 5, '#io-ox-category-browser')
  I.waitForText('Task 4', 5, '#io-ox-category-browser')
  I.waitForText('Task 5', 5, '#io-ox-category-browser')
  I.waitForText('Show all', 5, '#io-ox-category-browser')

  I.click('.category>button')
  I.waitForDetached('.category .items')
  I.click('.category>button')
  I.waitForElement('.category .items')

  I.click('Show all', '#io-ox-category-browser')
  I.waitForText('Search results')
  I.waitForText('Task 1', 5, '.io-ox-tasks-window .vgrid-scrollpane-container')
  I.waitForText('Task 2', 5, '.io-ox-tasks-window .vgrid-scrollpane-container')
  I.waitForText('Task 3', 5, '.io-ox-tasks-window .vgrid-scrollpane-container')
  I.waitForText('Task 4', 5, '.io-ox-tasks-window .vgrid-scrollpane-container')
  I.waitForText('Task 5', 5, '.io-ox-tasks-window .vgrid-scrollpane-container')
  I.waitForText('Task 6', 5, '.io-ox-tasks-window .vgrid-scrollpane-container')

  I.click('Task 1', '#io-ox-category-browser')
  I.waitForVisible('.detail-popup')
  I.waitForText('Task 1', 5, '.detail-popup')
  I.click('~Delete', '.detail-popup')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.detail-popup')

  I.waitForText('Task 6', 5, '#io-ox-category-browser')
  I.waitForText('Task 2', 5, '#io-ox-category-browser')
  I.waitForText('Task 3', 5, '#io-ox-category-browser')
  I.waitForText('Task 4', 5, '#io-ox-category-browser')
  I.waitForText('Task 5', 5, '#io-ox-category-browser')
  I.dontSee('Show all', '#io-ox-category-browser')
})

Scenario('Adheres to feature toggle', async ({ I, users }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: false }, categories: { userCategories: [] } } })
  await I.login()
  I.waitForApp()
  I.click('~Settings')
  I.dontSee('Browse categories …', '.dropdown.open')
  I.logout()
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories: [] } } })
  await I.login()
  I.waitForApp()
  I.click('~Settings')
  I.see('Browse categories …', '.dropdown.open')
})
