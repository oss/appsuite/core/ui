/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Contacts > Delete')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Single, multiple and in detail app', async ({ I, search, contacts, dialogs }) => {
  const defaultFolder = await I.grabDefaultFolder('contacts')
  await Promise.all([
    I.haveSetting('io.ox/contacts//listViewLayout', 'checkboxes'),
    I.haveContact({ first_name: 'Brian', last_name: 'Johnson', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Phil', last_name: 'Rudd', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Cliff', last_name: 'Williams', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Angus', last_name: 'Young', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Stevie', last_name: 'Young', folder_id: defaultFolder })

  ])

  await I.login('app=io.ox/contacts')

  // aspect: single delete
  contacts.selectContact('Johnson, Brian')
  I.clickToolbar('Delete')
  I.waitForText('Do you really want to delete this contact?')
  I.click('Delete')
  I.waitForNetworkTraffic()
  I.dontSee('Johnson, Brian')

  // aspect: multi delete
  I.click(locate('[aria-label="Williams, Cliff"] .vgrid-cell-checkbox').as('[aria-label="Williams, Cliff"] .vgrid-cell-checkbox'))

  I.see('2 items selected')
  I.clickToolbar('Delete')
  I.waitForText('Do you really want to delete these contacts?')
  I.click('Delete')
  I.waitForNetworkTraffic()
  I.dontSee('Rudd, Phil')
  I.dontSee('Williams, Cliff')

  // aspect: detail window delete
  I.doubleClick('Young, Angus')
  I.waitForVisible('.io-ox-contacts-detail-window[data-window-nr="1"]')
  I.click('Minimize')
  I.waitForInvisible('.io-ox-contacts-detail-window[data-window-nr="1"]')

  I.doubleClick('Young, Stevie')
  I.see('Saved in', '.io-ox-contacts-detail-window')
  I.seeElement('.io-ox-contacts-detail-window[data-window-nr="2"] button[aria-label="Delete"]')
  I.click('.io-ox-contacts-detail-window[data-window-nr="2"] button[aria-label="Delete"]')
  I.waitForText('Do you really want to delete this contact?')
  I.click('Delete')
  I.waitForNetworkTraffic()
  I.waitForDetached('.io-ox-contacts-detail-window[data-window-nr="2"]')
  I.dontSee('Young, Stevie')

  I.click('~Young, Angus', '#io-ox-taskbar')
  I.see('Saved in', '.io-ox-contacts-detail-window')
  I.seeElement('.io-ox-contacts-detail-window[data-window-nr="1"] button[aria-label="Delete"]')
  I.click('.io-ox-contacts-detail-window[data-window-nr="1"] button[aria-label="Delete"]')
  I.waitForText('Do you really want to delete this contact?')
  I.click('Delete')
  I.waitForNetworkTraffic()
  I.waitForDetached('.io-ox-contacts-detail-window[data-window-nr="1"]')
  I.dontSee('Young, Stevie')
})

Scenario('[C7366] Multiple contacts', async ({ I, search, contacts, dialogs }) => {
  const defaultFolder = await I.grabDefaultFolder('contacts')
  await Promise.all([
    I.haveContact({
      display_name: 'C7366, C7366',
      folder_id: defaultFolder,
      first_name: 'C7366',
      last_name: 'C7366'
    }),
    I.haveContact({
      display_name: 'C7366, C7366',
      folder_id: defaultFolder,
      first_name: 'C7366',
      last_name: 'C7366'
    })
  ])

  await I.login('app=io.ox/contacts')

  search.doSearch('C7366 C7366')
  I.click('~C7366, C7366')
  I.waitForNetworkTraffic()
  I.click('~More contact options')
  I.clickDropdown('Select all')
  I.waitForNetworkTraffic()
  I.clickToolbar('Delete')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal-dialog')
  I.waitForInvisible('.io-ox-busy')
  I.dontSee('C7367, C7367')
})

Scenario('[C7367] Single Contact', async ({ I, contacts, dialogs }) => {
  await I.haveContact({
    display_name: 'C7367, C7367',
    folder_id: await I.grabDefaultFolder('contacts'),
    first_name: 'C7367',
    last_name: 'C7367'
  })

  await I.login('app=io.ox/contacts')

  contacts.selectContact('C7367, C7367')

  I.clickToolbar('Delete')
  dialogs.waitForVisible()
  dialogs.clickButton('Delete')
  I.waitForDetached('.modal-dialog')
  I.waitForDetached('~C7367, C7367')
})
