/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Contacts > Sorting')

Before(async ({ users, contacts }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Contacts can be sorted', async ({ I, search, contacts }) => {
  const defaultFolder = await I.grabDefaultFolder('contacts')
  await Promise.all([
    I.haveSetting('io.ox/contacts//listViewLayout', 'checkboxes'),
    I.haveContact({ first_name: 'Angus', last_name: 'Franciscus', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Berta', last_name: 'Schmidt', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Cliff', last_name: 'Weber', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Dieter', last_name: 'Becker', folder_id: defaultFolder }),
    I.haveContact({ first_name: 'Eric', last_name: 'Wolf', folder_id: defaultFolder })
  ])
  await I.login('app=io.ox/contacts')
  I.click('~More contact options')
  I.waitForText('First name')
  I.click('First name')
  I.click('~More contact options')
  I.waitForText('Ascending')
  I.click('Ascending')
  I.waitForDetached('.smart-dropdown-container')

  I.wait(1)
  I.see('Franciscus , Angus', '.vgrid-cell[aria-posinset="1"]')
  I.see('Schmidt , Berta', '.vgrid-cell[aria-posinset="2"]')
  I.see('Weber , Cliff', '.vgrid-cell[aria-posinset="3"]')
  I.see('Becker , Dieter', '.vgrid-cell[aria-posinset="4"]')
  I.see('Wolf , Eric', '.vgrid-cell[aria-posinset="5"]')

  I.click('~More contact options')
  I.waitForText('Descending')
  I.click('Descending')
  I.waitForDetached('.smart-dropdown-container')

  I.wait(1)
  I.see('Franciscus , Angus', '.vgrid-cell[aria-posinset="5"]')
  I.see('Schmidt , Berta', '.vgrid-cell[aria-posinset="4"]')
  I.see('Weber , Cliff', '.vgrid-cell[aria-posinset="3"]')
  I.see('Becker , Dieter', '.vgrid-cell[aria-posinset="2"]')
  I.see('Wolf , Eric', '.vgrid-cell[aria-posinset="1"]')

  I.click('~More contact options')
  I.waitForText('Last name')
  I.click('Last name')
  I.click('~More contact options')
  I.waitForText('Ascending')
  I.click('Ascending')
  I.waitForDetached('.smart-dropdown-container')

  I.wait(1)
  I.see('Becker , Dieter', '.vgrid-cell[aria-posinset="1"]')
  I.see('Franciscus , Angus', '.vgrid-cell[aria-posinset="2"]')
  I.see('Schmidt , Berta', '.vgrid-cell[aria-posinset="3"]')
  I.see('Weber , Cliff', '.vgrid-cell[aria-posinset="4"]')
  I.see('Wolf , Eric', '.vgrid-cell[aria-posinset="5"]')

  I.click('~More contact options')
  I.waitForText('Descending')
  I.click('Descending')
  I.waitForDetached('.smart-dropdown-container')

  I.wait(1)
  I.see('Becker , Dieter', '.vgrid-cell[aria-posinset="5"]')
  I.see('Franciscus , Angus', '.vgrid-cell[aria-posinset="4"]')
  I.see('Schmidt , Berta', '.vgrid-cell[aria-posinset="3"]')
  I.see('Weber , Cliff', '.vgrid-cell[aria-posinset="2"]')
  I.see('Wolf , Eric', '.vgrid-cell[aria-posinset="1"]')
})
