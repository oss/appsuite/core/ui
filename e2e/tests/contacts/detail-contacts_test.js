/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Contacts > Detail')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Displays only fields with content @smoketest', async ({ I, contacts }) => {
  // alwaysVisible and optional fields
  const first = 'Phil'
  const last = 'Dunphy'
  const displayName = `${last}, ${first}`
  await I.haveContact({
    display_name: displayName,
    folder_id: await I.grabDefaultFolder('contacts'),
    first_name: first,
    last_name: last,
    street_home: 'STREET_HOME'
  })

  await I.login('app=io.ox/contacts')

  // initial value
  I.waitForElement(`~${displayName}`)
  I.click(`~${displayName}`)
  I.waitForText('Home address', undefined, '.contact-detail')
  I.waitForText('STREET_HOME', undefined, '.contact-detail')

  // remove value
  I.click('~Edit')
  I.waitForVisible(contacts.editWindow)
  I.fillField('Email 1', 'email1@example.org')
  I.fillField('street_home', '')
  I.click('Save')
  I.waitForDetached(contacts.editWindow)
  I.waitForElement('~Refresh')

  // check detail view
  I.click('.io-ox-contacts-window .leftside')
  I.pressKey('End')
  I.pressKey('Enter')
  I.waitForVisible('.io-ox-contacts-window .leftside .vgrid-cell.selected')
  I.waitForText(displayName, undefined, '.contact-detail')
  I.waitForText('email1@example.org', undefined, '.contact-detail')
  I.dontSee('Home address', '.contact-detail')
  I.dontSee('street_home', '.contact-detail')
})
