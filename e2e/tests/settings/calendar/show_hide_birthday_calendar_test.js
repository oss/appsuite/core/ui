/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const moment = require('moment')

Feature('Settings > Calendar')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C248441] Configure to show/hide birthday calendar', async ({ I, calendar, settings }) => {
  /* eslint-disable camelcase */
  const folder_id = await I.grabDefaultFolder('contacts')

  await Promise.all([
    I.haveSetting('io.ox/calendar//layout', 'week:week'),
    I.haveContact({ first_name: 'User1', folder_id, birthday: moment().valueOf() })
  ])

  await I.login('app=io.ox/calendar&settings=virtual/settings/io.ox/calendar')

  settings.expandSection('Advanced settings')

  // Check whether birthday calendar is shown on Calendar App
  I.waitForText('Show birthday calendar')
  I.waitForVisible('#settings-birthday')
  I.seeCheckboxIsChecked('birthday')
  settings.close()
  I.waitForText('Birthdays')

  // Check whether options are hidden when appointment has no organizer
  I.waitForText('🎂 User1')
  I.click('🎂 User1 ')
  I.waitForVisible('.detail-popup [aria-label="More actions"]')
  I.click('~More actions', '.detail-popup')
  I.waitForText('Move', 5, '.smart-dropdown-container.dropdown.open')
  I.see('Export', '.smart-dropdown-container.dropdown.open')
  I.see('Print', '.smart-dropdown-container.dropdown.open')
  I.dontSee('Participant related actions', '.dropdown-header')
  I.dontSee('Send e-mail to all participants', '.smart-dropdown-container.dropdown.open')
  I.dontSee('Invite to new appointment', '.smart-dropdown-container.dropdown.open')
  I.pressKey('Escape')

  // Check whether birthday calendar is not shown on Calendar App
  await settings.open()
  settings.expandSection('Advanced settings')
  I.uncheckOption('Show birthday calendar')
  settings.close()
  I.waitForText('My calendars')
  I.waitForInvisible('Birthdays')
})
