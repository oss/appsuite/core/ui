/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Custom mail account')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C7840] Delete external mail account', async ({ I, users, mail, settings, dialogs }) => {
  await I.haveSetting('io.ox/mail//messageFormat', 'text')

  const additionalAccount = await users.create()
  await I.haveMailAccount({ additionalAccount, name: 'MyExternalAccount', extension: 'ext' })

  await I.login('settings=virtual/settings/io.ox/settings/accounts')

  I.waitForText('MyExternalAccount', 10, '.io-ox-mail-window .folder-tree')

  // this is important to trigger OXUIB-2404
  I.click('Accounts')

  I.waitForVisible('.io-ox-accounts-settings')
  I.waitForNetworkTraffic()

  I.waitNumberOfVisibleElements('.settings-list-item', 2)
  I.waitForVisible('~Delete MyExternalAccount')
  I.waitForVisible('.io-ox-accounts-settings .remove .bi-x-lg')
  I.waitForEnabled('~Delete MyExternalAccount')
  I.click('~Delete MyExternalAccount')

  dialogs.waitForVisible()
  dialogs.clickButton('Delete account')
  I.waitForInvisible('~Delete account')

  I.click('Accounts')
  I.waitForVisible('.io-ox-accounts-settings')
  I.dontSee('MyExternalAccount', '.io-ox-accounts-settings')

  settings.close()
  I.waitForInvisible(locate('.folder-tree li').withText('MyExternalAccount'))
})

Scenario('Remove file storage', async ({ I, settings, mail, users }) => {
  await I.login('io.ox/mail')

  I.executeScript(async () => {
    const oauth = await import(String(new URL('io.ox/oauth/keychain.js', location.href)))
    const { OAuthAccountDetailExtension } = await import(String(new URL('io.ox/oauth/settings.js', location.href)))
    const { accounts, services } = await oauth.getAPI()
    const { default: ext } = await import(String(new URL('io.ox/core/extensions.js', location.href)))

    ext.point('io.ox/settings/accounts/dropbox/settings/detail').extend(new OAuthAccountDetailExtension('dropbox'))

    services.add({
      id: 'com.openexchange.oauth.dropbox',
      displayName: 'Dropbox',
      availableScopes: [
        'drive'
      ]
    })

    accounts.add({
      id: 4177,
      displayName: 'My Dropbox account',
      serviceId: 'com.openexchange.oauth.dropbox',
      enabledScopes: [
        'drive'
      ],
      availableScopes: [
        'drive'
      ],
      associations: [
        {
          id: '2022',
          name: 'My Dropbox',
          scopes: [
            'drive'
          ],
          module: 'infostore',
          folder: 'dropbox://0000/'
        }
      ]
    })
  })

  await settings.open('Accounts')
  I.waitForElement('button[aria-label="Edit My Dropbox account"]')
  I.click('button[aria-label="Edit My Dropbox account"]')
  I.waitForElement('button[aria-label="Delete My Dropbox"]')
  I.click('button[aria-label="Delete My Dropbox"]')
  I.waitForText('Delete account')
  I.click('Delete account')
  I.waitForEnabled('.close-settings')
})
