/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Settings > Mail')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Changes on individual font sizes are changed at runtime', async ({ I, mail, settings }) => {
  await I.haveMail({ path: 'media/mails/types/text_plain.eml' })
  await I.login('app=io.ox/mail')

  // check initial value
  await mail.selectMail('Plain text')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    // default value
    I.waitForElement('.text-very-small')
  })

  // open settings pane
  await settings.open('Mail', 'Reading')
  I.waitForText('Font size')
  I.scrollTo('#fontSize')

  // see default value and change to 'text very large'
  I.seeElement('#reading-pane-preview .text-very-small')
  I.selectOption('#fontSize', 'Very large')
  I.waitForResponse(response => response.url().includes('api/jslob?action=set') && response.request().method() === 'PUT', 10)
  I.waitForElement('#reading-pane-preview .text-very-large')
  settings.close()
  await within({ frame: '.mail-detail-frame' }, async () => { I.waitForElement('.mail-detail-content.text-very-large') })
  await I.logout()

  // check persistence
  await I.login('app=io.ox/mail')
  await mail.selectMail('Plain text')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    I.waitForElement('.mail-detail-content.text-very-large')
  })
})

Scenario('Individual font sizes support different types of emails', async ({ I, mail, settings }) => {
  await Promise.all([
    I.haveSetting('io.ox/mail//fontSize', 'text-very-large'),
    I.haveMail({ path: 'media/mails/types/text_plain.eml' }),
    I.haveMail({ path: 'media/mails/types/html_tinymce-default.eml' }),
    I.haveMail({ path: 'media/mails/types/html_glossy.eml' })
  ])
  await I.login('app=io.ox/mail')

  // check plain text mail
  await mail.selectMail('Plain text')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    // default value
    I.waitForElement('.mail-detail-content.text-very-large')
    const basefontSize = await I.grabCssPropertyFrom('.mail-detail-content', 'font-size')
    assert.equal(basefontSize, '18px')
  })

  // check html mails send by App Suite with a default style
  await mail.selectMail('HTML TinyMCE default')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    I.waitForElement('.mail-detail-content.text-very-large')
    const basefontSize = await I.grabCssPropertyFrom('.mail-detail-content', 'font-size')
    assert.equal(basefontSize, '18px')

    const styledFontSize = await I.grabCssPropertyFrom('.mail-detail-content .styled-string', 'font-size')
    assert.equal(styledFontSize, '18px')
  })

  // check glossy mail with inner stylings like newsletters
  await mail.selectMail('HTML Glossy')
  I.waitForVisible('.mail-detail-frame')
  await within({ frame: '.mail-detail-frame' }, async () => {
    // default value
    I.waitForElement('.mail-detail-content.text-very-large')
    const basefontSize = await I.grabCssPropertyFrom('.mail-detail-content', 'font-size')
    assert.equal(basefontSize, '18px')

    const styledFontSize = await I.grabCssPropertyFrom('.mail-detail-content .styled-string', 'font-size')
    assert.equal(styledFontSize, '10px')
  })
})
