/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Settings > Mail > undoSend')

Before(async ({ users }) => { await users.create() })
After(async ({ users }) => { await users.removeAll() })

Scenario('Feature Flag undoSend=true', async ({ I, settings }) => {
  await I.haveSetting('io.ox/core//features/undoSend', true)

  await I.login('app=io.ox/mail&section=io.ox/mail/settings/compose&settings=virtual/settings/io.ox/mail')

  I.waitForText('Undo send')
  I.see('Without delay')
  I.see('5 seconds')
  I.see('10 seconds')
})

Scenario('Feature Flag undoSend=false', async ({ I, settings }) => {
  await I.haveSetting('io.ox/core//features/undoSend', false)

  await I.login('app=io.ox/mail&section=io.ox/mail/settings/compose&settings=virtual/settings/io.ox/mail')

  I.waitForText('Message Format')
  I.dontSee('Undo send')
})

Scenario('After the user sends a mail in mail compose the mail is not send until the delay has passed', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 1, undoSendDoneDelay: 1 },
    'io.ox/core': { features: { undoSend: true } }
  })
  await I.login('app=io.ox/mail&section=io.ox/mail/settings/compose&settings=virtual/settings/io.ox/mail')

  I.checkOption('Without delay')
  settings.close()

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject')
  I.click('Send')

  I.waitForVisible('.mail-send-progress')
  I.waitForInvisible('.mail-send-progress-action', 0.1)

  await settings.open('Mail', 'Compose & Reply')
  I.checkOption('5 seconds', '#undoSendDelay') // "5 Seconds" clashes with "Mark as read"
  await I.waitForSetting({ 'io.ox/mail': { undoSendDelay: 5 } })
  settings.close()
  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')
  I.click('Send')

  I.waitForText('Sending…', undefined, '.mail-send-progress .state')
  I.see('Subject of Mail', '.mail-send-progress-subject')
  I.see('Undo', '.mail-send-progress button')
  I.waitForText('Send', undefined, '.mail-send-progress .state')
  I.see('Subject of Mail', '.mail-send-progress-subject')
  I.dontSee('.mail-send-progress button')
  I.waitForInvisible('.mail-send-progress', 20)
})

Scenario('If the user presses the "undo" button, the mail is not send. Mail compose is re-opened with the mail', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 5 },
    'io.ox/core': { features: { undoSend: true } }
  })

  await I.login('app=io.ox/mail', { user: users[0] })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')
  I.click('Send')

  I.waitForText('Sending…', undefined, '.mail-send-progress .state')
  I.click('.mail-send-progress button')
  I.dontSee('.mail-send-progress')
  I.see('Subject of Mail', '.io-ox-mail-compose-window .title')
  I.see('Subject of Mail', '.mail-send-progress-subject')
})

Scenario('If multiple mails are canceled, all mails are re-opened', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 15 },
    'io.ox/core': { features: { undoSend: true } }
  })

  await I.login('app=io.ox/mail', { user: users[0] })
  I.resizeWindow(800, 800)

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')
  I.click('Send')

  await mail.newMail()
  I.fillField('.active .to .tt-input', users[0].get('primaryEmail'))
  I.fillField('.active .mail-input [name="subject"]', 'Subject of Mail 2')
  I.click('.active .btn-primary')

  I.waitForText('Sending…', undefined, '.mail-send-progress .state')
  I.see('Subject of Mail 2', '.mail-send-progress-subject')
  I.click('Undo')
  I.dontSee('.mail-send-progress')
  I.waitForVisible(mail.composeWindow)

  I.seeNumberOfElements(mail.composeWindow, 2)
})

Scenario('If attachments are added, sending mail can be undone during attachment upload', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 5 },
    'io.ox/core': { features: { undoSend: true } }
  })

  await I.login('app=io.ox/mail', { user: users[0] })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')
  await I.createGenericFile('16MB.dat', 16 * 1024 * 1024)
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/16MB.dat')
  I.click('Send')

  I.waitForText('Uploading Attachment…', undefined, '.mail-send-progress .state')
  I.see('Subject of Mail', '.mail-send-progress-subject')
  I.waitForElement('.progress')
  I.click('Undo')
  I.waitForInvisible('.progress')
  I.waitForText('Subject of Mail', undefined, '.io-ox-mail-compose-window .title')

  // remove attachment and try to send again
  I.click('.toggle-mode')
  I.click('.remove-attachment')
  I.click('Send')
  I.waitForText('Subject of Mail', undefined, '.mail-send-progress-subject')
  I.waitToHide('.mail-send-progress', 10)
})

Scenario('Send one mail, abort the other', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 5 },
    'io.ox/core': { features: { undoSend: true } }
  })

  await I.login('app=io.ox/mail', { user: users[0] })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')

  await mail.newMail()
  I.fillField('.active .token-input.tt-input', users[0].get('primaryEmail'))
  I.fillField('.active [name="subject"]', 'Subject of Mail 2')

  // send both mails
  I.click('Send', '.active .window-footer')
  I.click('Send')

  // abort both mails
  I.waitForElement('.mail-send-progress')
  I.click('Undo')
  I.waitToHide('.mail-send-progress')

  // send one mail
  I.click('Send')

  I.waitToHide('.mail-send-progress', 20)
})

Scenario('When attachment upload is interrupted, mail window reopens', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 5 },
    'io.ox/core': { features: { undoSend: true } }
  })

  await I.login('app=io.ox/mail', { user: users[0] })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')

  I.mockRoute('**/attachments*', route => route.abort('failed'))
  await I.createGenericFile('2MB.dat', 2 * 1024 * 1024)
  I.attachFile('.composetoolbar input[type="file"]', 'media/files/generic/2MB.dat')

  I.click('Send')

  I.waitForElement('.mail-send-progress')
  I.waitForText('Error', 30)
  I.waitForVisible(mail.composeWindow)
  I.waitForText('Subject of Mail', undefined, '.io-ox-mail-compose-window .title')
})

Scenario('When logging out during mail send users are warned of potential data loss', async ({ I, dialogs, mail, users }) => {
  I.haveSetting({
    'io.ox/mail': { undoSendDelay: 5 },
    'io.ox/core': { features: { undoSend: true } }
  })

  await I.login('app=io.ox/mail', { user: users[0] })

  I.executeScript(async function () {
    // @ts-ignore
    const { default: ox } = await import(String(new URL('ox.js', location.href)))

    ox.logoutLocation = 'https://www.open-xchange.com'
  })

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject of Mail')
  I.click('Send')

  I.waitForElement('.mail-send-progress')

  I.click('~My account')
  I.waitForText('Sign out')
  I.clickDropdown('Sign out')

  dialogs.waitForVisible()
  I.waitForText('Sending email in progress', undefined, dialogs.header)
  I.click('Cancel')

  I.waitForElement('.mail-send-progress')

  I.click('~My account')
  I.waitForText('Sign out')
  I.clickDropdown('Sign out')

  dialogs.waitForVisible()
  I.waitForText('Sending email in progress', undefined, dialogs.header)
  I.click('Continue sign out')
  I.waitInUrl('open-xchange.com')
})

Scenario('The user can dismiss the popup or click to navigate to Scheduled folder', async ({ I, settings, mail, users }) => {
  await I.haveSetting({
    'io.ox/mail': { undoSendDelay: 5 },
    'io.ox/core': { features: { undoSend: true, scheduleSend: true } }
  })
  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.fillField(mail.to, users[0].get('primaryEmail'))
  I.fillField(mail.subject, 'Subject')
  I.click('~Schedule email')
  I.click('[data-name="tomorrowMorning"]')

  I.waitForVisible('.mail-send-progress')
  I.waitForInvisible('.mail-send-progress-action', 0.1)

  I.waitForText('Sending…', undefined, '.mail-send-progress .state')
  I.see('Subject', '.mail-send-progress-subject')
  I.see('Undo', '.mail-send-progress button')
  I.wait(2)
  I.waitForText('Sending planned for', undefined, '.mail-send-progress .state')
  I.see('Subject', '.mail-send-progress-subject')
  I.see('Close', '.mail-send-progress button')

  I.click('Sending planned for')
  I.waitForText('Scheduled', undefined, '.folder-name')

  I.waitForInvisible('.mail-send-progress', 20)
})
