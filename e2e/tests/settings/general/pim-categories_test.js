/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Core > Categories')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Settings page', async ({ I }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true, '.user': { categories: true } } } })
  await I.login('settings=virtual/settings/io.ox/core&section=io.ox/settings/general/advanced')

  I.waitForText('Manage categories …')
  I.click('Manage categories …')
  I.waitForText('Manage categories')
  I.see('Close')
  I.see('New category')
  I.seeNumberOfVisibleElements('.category-list-item', 4)
})

Scenario('Supports default and predefined categories', async ({ I }) => {
  await I.haveSetting({
    'io.ox/core': {
      features: { categories: true },
      categories: {
        userCategories: [{
          name: 'Confidential'
        }]
      }
    }
  })

  await I.login('settings=virtual/settings/io.ox/core&section=io.ox/settings/general/advanced')

  I.waitForText('Manage categories …')
  I.click('Manage categories …')
  I.waitForText('Manage categories')

  // number and type
  I.seeNumberOfVisibleElements('.category-list-item', 5)
  I.seeNumberOfVisibleElements('.category-view[data-type="user"]', 1)
  I.seeNumberOfVisibleElements('.category-view[data-type="predefined"]', 4)

  // actions
  I.seeNumberOfVisibleElements('.category-list-item .delete', 1)
  I.seeNumberOfVisibleElements('.category-list-item .edit', 1)

  // check predefined (immutable)
  I.see('Important')
  I.seeElement('.category-view .bi-exclamation-circle')
  I.dontSeeElement('.category-list-item:nth-of-type(1) .list-item-controls .edit')
  I.dontSeeElement('.category-list-item:nth-of-type(1) .list-item-controls .delete')
  I.see('Business')
  I.seeElement('.category-view .bi-briefcase')
  I.dontSeeElement('.category-list-item:nth-of-type(2) .list-item-controls .edit')
  I.dontSeeElement('.category-list-item:nth-of-type(2) .list-item-controls .delete')
  I.see('Private')
  I.seeElement('.category-view .bi-house-door')
  I.dontSeeElement('.category-list-item:nth-of-type(3) .list-item-controls .edit')
  I.dontSeeElement('.category-list-item:nth-of-type(3) .list-item-controls .delete')
  I.see('Meeting')
  I.seeElement('.category-view .bi-people')
  I.dontSeeElement('.category-list-item:nth-of-type(4) .list-item-controls .edit')
  I.dontSeeElement('.category-list-item:nth-of-type(4) .list-item-controls .delete')

  // check user category
  I.see('Confidential')
  I.seeElement('.category-list-item:nth-of-type(5) .list-item-controls .edit')
  I.seeElement('.category-list-item:nth-of-type(5) .list-item-controls .delete')
})

Scenario('Add, edit and remove', async ({ I }) => {
  const userCategories = [{
    name: 'user:icon',
    color: '#ff2968',
    icon: 'bi/exclamation-circle.svg'
  }, {
    name: 'user:delete',
    color: '#16adf8'
  },
  {
    name: 'user:edit',
    color: '#707070'
  }]
  await I.haveSetting({ 'io.ox/core': { features: { categories: true }, categories: { userCategories } } })

  await I.login('settings=virtual/settings/io.ox/core&section=io.ox/settings/general/advanced')

  I.waitForText('Manage categories …')
  I.click('Manage categories …')
  I.waitForText('Manage categories')

  I.seeNumberOfVisibleElements('.category-list-item', 7)
  I.seeNumberOfVisibleElements('.category-list-item .delete', 3)
  I.seeNumberOfVisibleElements('.category-list-item .edit', 3)

  I.see('user:icon')
  I.see('user:delete')
  I.see('user:edit')

  // single one with item
  I.seeElement('.category-list-item:nth-of-type(1) .bi-exclamation-circle')

  // delete
  I.click('~Delete category user:delete')
  I.waitForText('Delete category')
  I.click('Delete')
  I.waitForText('Manage categories')

  // set icon
  I.click('~Edit category user:edit')
  I.waitForText('Edit category')
  I.fillField('Name', 'user:edit:renamed')
  I.checkOption('[title="Alarm"]')
  I.click('Save')
  I.waitForText('New category')

  // create
  I.click('New category')
  I.waitForText('Create category')
  I.fillField('Name', 'user:new')
  I.click('Create')
  I.waitForText('Manage categories')
  I.waitForResponse(response => response.url().includes('api/jslob?action=set') && response.request().method() === 'PUT', 10)
  I.wait(1)
  await I.logout()

  // check restore
  await I.login('settings=virtual/settings/io.ox/core&section=io.ox/settings/general/advanced')

  I.waitForText('Manage categories …')
  I.click('Manage categories …')
  I.waitForText('Manage categories')

  I.waitForText('user:new')
  I.see('user:icon')
  I.dontSee('user:delete')
  I.see('user:edit:renamed')
  I.seeElement('.category-list-item:nth-of-type(6) .bi-alarm')
  I.see('user:new', '.category-list-item')
})

Scenario('Validate categories', async ({ I }) => {
  await I.haveSetting({
    'io.ox/core': {
      features: { categories: true },
      categories: {
        userCategories: [{
          name: 'Confidential'
        }]
      }
    }
  })

  await I.login('settings=virtual/settings/io.ox/core&section=io.ox/settings/general/advanced')

  I.waitForText('Manage categories …')
  I.click('Manage categories …')
  I.waitForElement('.modal-dialog')
  I.see('Manage categories', '.modal-dialog')
  I.see('Confidential', '.modal-dialog')
  // predefined categories
  I.seeNumberOfVisibleElements('.category-list-item', 5)

  // validate duplicate: edit (we use a name with trailing whitespace to ensure validation can handle this)
  I.click('~Edit category Confidential')
  I.waitForText('Edit category')
  I.fillField('Name', ' Business ')
  I.click('Save')
  I.see('Name is already taken.')
  I.pressKey('Tab')
  I.click('.category-modal-update button[data-action="cancel"]')
  I.waitForText('Manage categories')

  // validate duplicate: new
  I.click('New category')
  I.waitForText('Create category')
  I.fillField('Name', 'Meeting')
  I.click('Create')
  I.see('Name is already taken.')
  I.pressKey('Tab')
  I.click('.category-modal-update button[data-action="cancel"]')
  I.waitForText('Manage categories')

  // validate empty name
  I.click('New category')
  I.waitForText('Create category')
  I.fillField('Name', '')
  I.click('Create')
  I.see('Please enter a name.')
})

Scenario('Search space separated categories', async ({ I, tasks }) => {
  await I.haveSetting({
    'io.ox/core': {
      features: { categories: true, '.user': { categories: true } },
      categories: { userCategories: [{ name: 'space separated' }] }
    }
  })
  await I.login('app=io.ox/tasks')

  tasks.newTask()

  I.waitForElement('.io-ox-tasks-edit')
  I.fillField('Subject', 'Task title')
  I.click('Expand form')
  I.click('Add category')
  I.click('space separated')
  I.click('Create')
  I.waitForDetached('.io-ox-tasks-edit-window')
  I.waitForNetworkTraffic()

  I.click('[data-point="io.ox/tasks/search/dropdown"] .search-field')
  await I.waitForFocus('[data-point="io.ox/tasks/search/dropdown"] .search-field')
  I.fillField('Search tasks', 'space')
  I.pressKey('Enter')
  I.waitForText('Search results')
  I.waitForText('Task title', undefined, '.vgrid-cell')

  I.fillField('Search tasks', 'separated')
  I.pressKey('Enter')
  I.waitForText('Search results')
  I.waitForText('Task title', undefined, '.vgrid-cell')

  I.fillField('Search tasks', 'space separated')
  I.pressKey('Enter')
  I.waitForText('Search results')
  I.waitForText('Task title', undefined, '.vgrid-cell')
})

Scenario('Change icon of a category', async ({ I, calendar }) => {
  await I.haveSetting({ 'io.ox/core': { features: { categories: true, '.user': { categories: true } } } })
  await I.login('app=io.ox/calendar')

  await calendar.newAppointment()
  I.scrollTo('.category-dropdown .dropdown-label')
  I.click('.category-dropdown .dropdown-label')
  I.clickDropdown('Manage categories …')
  I.waitForText('New category')
  I.click('New category')
  I.fillField('Name', 'Custom')
  I.click(locate('.item-picker-item').withText('Meeting'))
  I.click('Create', '.category-modal-update')
  I.waitToHide('.category-modal-update')

  I.seeElement('.category-view[data-name="Custom"] svg.bi-people')
  I.click('~Edit category Custom')
  I.waitForElement('.category-modal-update')
  I.click(locate('.item-picker-item').withText('Important'))
  I.click('Save', '.category-modal-update')
  I.waitToHide('.category-modal-update')
  I.seeElement('.category-view[data-name="Custom"] svg.bi-exclamation-circle')

  I.click('~Edit category Custom')
  I.waitForElement('.category-modal-update')
  I.click(locate('.item-picker-item').withText('Meeting'))
  I.click('Save', '.category-modal-update')
  I.waitToHide('.category-modal-update')
  I.seeElement('.category-view[data-name="Custom"] svg.bi-people')

  I.click('~Edit category Custom')
  I.waitForElement('.category-modal-update')
  I.click(locate('.item-picker-item').withText('Important'))
  I.click('Save', '.category-modal-update')
  I.waitToHide('.category-modal-update')
  I.seeElement('.category-view[data-name="Custom"] svg.bi-exclamation-circle')
})

Scenario('Display shared categories', async ({ I, contacts, users }) => {
  const folder = await I.grabDefaultFolder('contacts')
  await Promise.all([
    I.haveSetting({ 'io.ox/core': { features: { categories: true, '.user': { categories: true } } } }),
    I.haveContact({ categories: 'Meeting,Male', folder_id: folder, first_name: 'Bernhard', last_name: 'Blocksberg' }),
    I.haveContact({ categories: 'Business,Ginger,Witch', folder_id: folder, first_name: 'Barbara', last_name: 'Blocksberg' })
  ])

  await I.login('app=io.ox/contacts')

  contacts.selectContact('Blocksberg, Bernhard')
  I.waitForText('Meeting', undefined, '.contact-detail .categories-badges')
  I.waitForText('Male', undefined, '.contact-detail .categories-badges')

  contacts.selectContact('Blocksberg, Barbara')
  I.waitForText('Business', undefined, '.contact-detail .categories-badges')
  I.waitForText('Ginger', undefined, '.contact-detail .categories-badges')
  I.waitForText('Witch', undefined, '.contact-detail .categories-badges')

  // check edit
  I.clickToolbar('Edit')
  I.waitForVisible(contacts.editWindow)
  I.scrollTo('[data-field="categories"]')
  I.waitForText('Business', undefined, '.io-ox-contacts-edit-window .categories-badges')
  I.waitForText('Ginger', undefined, '.io-ox-contacts-edit-window .categories-badges')
  I.waitForText('Witch', undefined, '.io-ox-contacts-edit-window .categories-badges')

  // dropdown
  I.click('Add category')

  I.see('User Categories', '.category-dropdown')
  I.see('Important', '.dropdown-menu [data-type="predefined"]')
  I.see('Business', '.dropdown-menu [data-type="predefined"]')
  I.see('Private', '.dropdown-menu [data-type="predefined"]')
  I.see('Meeting', '.dropdown-menu [data-type="predefined"]')

  I.see('Shared Categories', '.category-dropdown')
  I.see('Ginger', '.dropdown-menu [data-type="shared-item"]')
  I.see('Witch', '.dropdown-menu [data-type="shared-item"]')
  I.dontSee('Male', '.dropdown-menu [data-type="shared-item"]')

  // add category
  I.clickDropdown('Manage categories …')
  I.waitForText('Manage categories')

  I.waitForText('Important', undefined, '.category-modal')
  I.waitForText('Business', undefined, '.category-modal')
  I.waitForText('Private', undefined, '.category-modal')
  I.waitForText('Meeting', undefined, '.category-modal')

  I.dontSeeElement('.category-modal [data-type="shared-item"]')
})
