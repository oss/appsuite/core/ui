/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Settings > Basic')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C7758] Set timezone', { timeout: 120 }, async ({ I, users, settings }) => {
  // create appointment
  // let's use a fixed date to avoid problems
  // with daylight saving time around the globe.
  // time is 14:00 CEST, so 12:00 UTC.
  await I.haveAppointment({
    summary: 'Timezone test',
    startDate: { value: '20190403T140000' },
    endDate: { value: '20190403T150000' },
    attendees: [{ entity: users[0].get('id') }]
  })
  await I.haveSetting('io.ox/calendar//layout', 'month')
  // check major timezones
  const timezones = {
    'Australia/Adelaide': '10:30 PM',
    'Asia/Hong_Kong': '8:00 PM',
    'America/New_York': '8:00 AM',
    'America/Los_Angeles': '5:00 AM',
    'Europe/Berlin': '2:00 PM'
  }

  for (const id in timezones) {
    await I.login('settings=virtual/settings/io.ox/core')
    settings.expandSection('Language & Time zone')
    I.waitForText('Time zone')
    I.selectOption('#settings-timezone', id) // retry here, as the options are in optgroups which takes longer
    I.waitForVisible('.settings-hint.reload-page')
    settings.close()
    await I.logout()

    await I.login('app=io.ox/calendar')
    I.waitForText('New appointment')
    I.waitForText('Today')
    await I.executeScript(async function () {
      const { default: ox } = await import(String(new URL('ox.js', location.href)))
      // go to 2019-04-06 (not 1) to ensure we land in April
      ox.ui.App.getCurrentApp().setDate(1554595200000)
    })
    I.waitForText('Timezone test')
    I.waitForText(timezones[id])
    await I.logout()
  }
})
