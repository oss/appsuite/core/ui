/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

const { I, contacts } = inject()

Feature('Settings > Basic > User picture')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

function editPhoto () {
  contacts.editMyAccount()
  I.waitForElement('.contact-edit .contact-photo')
  I.click('.contact-edit .contact-photo')
  I.waitForVisible('.edit-picture')
}

function editPhotoStandalone () {
  I.waitForVisible('.dropdown-toggle[aria-label="My account"]')
  I.waitForVisible('.contact-picture')
  I.click('.contact-picture')
  I.waitForVisible('.user-picture-container')
  I.click('[data-name="user-picture"]', '.dropdown.open .dropdown-menu')
  I.waitForVisible('.modal.edit-picture')
}

Scenario('User starts without picture', async ({ I, dialogs, contacts, mail }) => {
  await I.login('app=io.ox/mail')

  I.waitForElement('#io-ox-toprightbar .contact-picture')
  I.waitForElement('.user-picture')
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).is.equal('none')

  // via click on user image
  editPhotoStandalone()
  dialogs.waitForVisible()

  dialogs.clickButton('Remove photo')
  I.waitForVisible('.modal.edit-picture.empty')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal.edit-picture')
  I.waitForDetached('.contact-photo-upload.hidden')

  // via "My user data"
  contacts.editMyAccount()

  // check if empty
  I.seeElementInDOM('.empty')
  I.waitForText('Update photo', 20, '.contact-photo label')
  I.click('.contact-edit .contact-photo')
  I.waitForVisible('.edit-picture.empty')
  dialogs.clickButton('Cancel')
  I.click('Discard')
})

Scenario('User can upload/remove picture', async ({ I, mail, dialogs }) => {
  await I.login('app=io.ox/mail')

  // 0. start with unset user picture
  I.waitForElement('#io-ox-toprightbar .contact-picture')
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  editPhoto()
  dialogs.waitForVisible()

  // 1. add user image (2.2 MB)
  I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', 'media/placeholder/800x600.png')
  I.waitForInvisible('.edit-picture.empty')
  dialogs.clickButton('Apply')
  I.waitForDetached('.edit-picture')
  I.waitForInvisible('.empty')
  I.click('Save')
  I.waitForDetached('.contact-edit')

  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}[style]`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).to.have.string('uniq')

  // 2. remove user image
  editPhoto()
  // TODO: BUG
  dialogs.waitForVisible()
  dialogs.clickButton('Remove photo')
  I.waitForVisible('.edit-picture.empty')
  dialogs.clickButton('Apply')
  I.waitForDetached('.modal-dialog')
  I.click('Save')
  I.waitForDetached('.contact-edit')

  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}:not([style])`)
  I.waitForElement(`${'.user-picture'}:not([style])`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).is.equal('none')

  // 3. check edit view again
  editPhoto()
  dialogs.waitForVisible()
  I.waitForVisible('.edit-picture.in.empty')
})

Scenario('Uploading a malformed image generates an error message and resets to previous', async ({ I, mail, dialogs }) => {
  await I.login('app=io.ox/mail')

  // 0. start with unset user picture
  I.waitForElement('#io-ox-toprightbar .contact-picture')
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  editPhoto()
  dialogs.waitForVisible()

  // 1. add user image (2.2 MB)
  I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', 'media/placeholder/800x600.png')
  I.waitForInvisible('.edit-picture.empty')
  dialogs.clickButton('Apply')
  I.waitForDetached('.edit-picture')
  I.waitForInvisible('.empty')
  I.click('Save')
  I.waitForDetached('.contact-edit')

  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}[style]`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).to.have.string('uniq')

  // 2. try to replace user image with malformed one
  editPhoto()
  dialogs.waitForVisible()

  I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', 'media/images/malformed.png')
  I.waitForElement('.alert')
  I.see('Unable to set image. Image might be too large or malformed.')
})

Scenario('User can upload/remove picture (standalone version)', async ({ I, mail, dialogs }) => {
  await I.login('app=io.ox/mail')

  // 0. start with unset user picture
  I.waitForElement('#io-ox-toprightbar .contact-picture')
  I.waitForElement('.user-picture')
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).is.equal('none')

  // 1. add user image (2.2 MB)
  editPhotoStandalone()
  dialogs.waitForVisible()
  I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', 'media/placeholder/800x600.png')
  I.waitForInvisible('.edit-picture.empty')
  dialogs.clickButton('Apply')

  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}[style]`)
  I.waitForElement(`${'.user-picture'}[style]`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).to.have.string('uniq')
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).to.have.string('uniq')
  const uniq = (await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).slice(-15, -2)

  // 2. update user image (2.2 MB)
  editPhotoStandalone()
  dialogs.waitForVisible()
  I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', 'media/placeholder/800x600-limegreen.png')
  I.waitForInvisible('.edit-picture.empty')
  dialogs.clickButton('Apply')
  I.wait(2)

  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}[style]`)
  I.waitForElement(`${'.user-picture'}[style]`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).to.have.string('uniq').and.not.have.string(uniq)
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).to.have.string('uniq').and.not.have.string(uniq)

  // 3. remove user image (but cancel)
  editPhotoStandalone()
  dialogs.waitForVisible()
  dialogs.clickButton('Remove photo')
  I.waitForVisible('.edit-picture.empty')
  dialogs.clickButton('Cancel')
  I.waitForDetached('.modal.edit-picture')

  I.wait(2)
  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}[style]`)
  I.waitForElement(`${'.user-picture'}[style]`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).to.have.string('uniq')
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).to.have.string('uniq')

  // 4. remove user image
  editPhotoStandalone()
  dialogs.waitForVisible()
  dialogs.clickButton('Remove photo')
  I.waitForVisible('.edit-picture.empty')
  dialogs.clickButton('Apply')
  I.waitForDetached('.modal.edit-picture')

  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}:not([style])`)
  I.waitForElement(`${'.user-picture'}:not([style])`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  expect(await I.grabCssPropertyFrom('.user-picture', 'background-image')).is.equal('none')

  // 5. check edit view again
  editPhotoStandalone()
  dialogs.waitForVisible()
  I.waitForVisible('.edit-picture.empty')
})

Scenario('User can rotate her/his picture', async ({ I, mail, dialogs }) => {
  await I.login('app=io.ox/mail')

  // 0. start with unset user picture
  I.waitForElement('#io-ox-toprightbar .contact-picture')
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).is.equal('none')
  editPhoto()
  dialogs.waitForVisible()

  // 1. add user image (2.2 MB)
  I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', 'media/placeholder/800x600.png')
  I.waitForInvisible('.edit-picture.empty')
  expect(await I.grabCssPropertyFrom('.contact-photo-upload .contact-photo', 'background-image')).to.not.be.empty
  // rotate (portrait to landscape)
  const height = await I.grabAttributeFrom('.cr-image', 'height')
  I.click('.inline-actions button[data-action="rotate-right"]')
  const width = await I.grabAttributeFrom('.cr-image', 'width')
  expect(Array.isArray(height) ? height[0] : height).to.be.equal(Array.isArray(width) ? width[0] : width)

  dialogs.clickButton('Apply')
  I.waitForDetached('.modal-dialog')
  I.waitForInvisible('.edit-picture')

  // 2. Discard
  I.click('Discard')
  // "Discard change"
  dialogs.waitForVisible()
  dialogs.clickButton('Cancel')

  I.click('Save')
  I.waitForDetached('.contact-edit')
  I.waitForElement(`${'#io-ox-toprightbar .contact-picture'}[style]`)
  expect(await I.grabCssPropertyFrom('#io-ox-toprightbar .contact-picture', 'background-image')).to.have.string('uniq')
})
