/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('General > Floating windows')

Before(async ({ users }) => { await Promise.all([users.create(), users.create()]) })

After(async ({ users }) => { await users.removeAll() })

Scenario('Opening multiple windows', async ({ I, users, calendar, dialogs, mail }) => {
  await I.haveSetting('io.ox/calendar//layout', 'week:workweek')
  await I.login('app=io.ox/calendar')

  await calendar.newAppointment()

  await within(calendar.editWindow, () => {
    calendar.startNextMonday()
    I.fillField('Title', 'Participants test')
    I.click('~Select contacts')
  })
  dialogs.waitForVisible()
  I.waitForEnabled('.modal-content .search-field')
  I.fillField('.modal-content .search-field', users[1].get('primaryEmail'))
  I.waitForEnabled('.modal-content .list-item-content')
  I.click(users[1].get('sur_name'), '.modal-content .list-item-content')
  dialogs.clickButton('Select')
  I.waitForDetached('.modal-dialog')
  I.click('Create')
  I.waitForDetached(calendar.editWindow)
  calendar.moveCalendarViewToNextWeek()
  I.waitForText('Participants test', undefined, '.page.current .appointment')
  I.click('Participants test', '.page.current .appointment')
  I.waitForText(users[1].get('sur_name'), undefined, '.participants-view')
  I.click(users[1].get('sur_name'), '.participants-view')

  I.waitForVisible('[data-block="communication"]')
  I.click(users[1].get('primaryEmail'), '[data-block="communication"]')

  // wait until compose window is active
  I.waitForVisible('.io-ox-mail-compose-window.active')

  const composeIndex = await I.grabCssPropertyFromAll(mail.composeWindow, 'z-index')
  const sidePopupIndex = await I.grabCssPropertyFromAll('.detail-popup', 'z-index')
  sidePopupIndex.map(s => Number.parseInt(s, 10)).forEach((index) => {
    expect(Number.parseInt(String(composeIndex), 10)).to.be.above(index)
  })
})

Scenario('[OXUIB-987] Tabbing into header toolbar after changing window size', async ({ I, mail }) => {
  await I.login('app=io.ox/mail')

  await mail.newMail()
  I.waitForElement('.io-ox-mail-compose-window.normal')

  I.pressKey(['Shift', 'Tab'])
  I.pressKey(['Shift', 'Tab'])
  I.pressKey(['Shift', 'Tab'])
  I.pressKey(['Shift', 'Tab'])
  I.pressKey(['Shift', 'Tab'])
  I.pressKey(['Shift', 'Tab'])

  I.pressKey('ArrowRight')
  I.pressKey('Enter')
  I.seeElement('.io-ox-mail-compose-window.maximized')
  I.pressKey('ArrowRight')
  I.pressKey('Enter')
  I.waitForDetached(mail.composeWindow)
})
