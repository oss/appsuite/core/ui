/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Login > Switch translations')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C7338] on form login page', async ({ I }) => {
  I.amOnLoginPage()
  // @ts-ignore
  I.setCookie({ name: 'locale', value: 'en_US', url: process.env.LAUNCH_URL })
  I.refreshPage()
  await I.waitForFocus('#io-ox-login-username', 30)
  I.click('English')
  I.clickDropdown('Italiano (Italia)')
  I.waitForText('Accedi') // cSpell:disable-line
})

Scenario.skip('[OXUI-700] for guest users with password', async ({ I, users, dialogs }) => {
  await users.create()
  await I.login('app=io.ox/files')
  I.waitForElement(locate('.folder-tree .folder-label').withText('My files'))
  I.selectFolder('Music')
  I.clickToolbar('Share')

  dialogs.waitForVisible()
  I.selectOption('Who can access this folder?', 'Anyone with the public link and invited people')
  I.waitForText('Anyone with the public link and invited people')
  I.waitForText('Copy link')
  I.wait(0.2)
  I.waitForElement('button[aria-label="Copy to clipboard"]:not([data-clipboard-text=""])')
  let link = await I.grabAttributeFrom('button[aria-label="Copy to clipboard"]', 'data-clipboard-text')
  I.waitForEnabled('.settings-button')
  I.click('.settings-button')
  dialogs.waitForVisible()
  I.fillField('Password', 'CorrectHorseBatteryStaple')
  dialogs.clickButton('Save')

  I.waitForText('Copy link')
  I.click('Copy link')
  I.waitForText('The link has been copied to the clipboard', undefined, '.io-ox-alert')
  link = Array.isArray(link) ? link[0] : link
  dialogs.clickButton('Share')
  I.waitForDetached('.modal-dialog')
  await I.logout()

  I.amOnPage(link)
  I.waitForVisible('#io-ox-login-password')
  I.waitForVisible('#io-ox-languages .dropdown > a')

  // german
  I.click('#io-ox-languages .dropdown > a')
  I.clickDropdown('Deutsch (Deutschland)')
  I.waitForDetached('.dropdown.open')
  I.waitForText('Passwort', undefined, '#io-ox-login-password')
  const german = await I.grabTextFrom('#io-ox-login-help')
  expect(german).contains('hat den Ordner')
  expect(german).contains('Musik')

  // english
  I.click('#io-ox-languages .dropdown > a')
  I.clickDropdown('English (United States)')
  I.waitForDetached('.dropdown.open')
  I.waitForText('Password', undefined, '#io-ox-login-password')
  const english = await I.grabTextFrom('#io-ox-login-help')
  expect(english).contains('has shared the folder')
  expect(english).contains('Music')
})
